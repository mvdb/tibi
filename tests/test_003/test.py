""" Basic single-point calculation tests for a small periodic structure.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase.build import bulk
from ase.units import Hartree, Bohr
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = bulk('Si', 'diamond', a=5.43)

calc = Tibi(basis_set={'Si': 'sp'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=True,
            include_tightbinding=False,
            kpts=[3, 3, 3],
            kpts_gamma_shift=False,
            output_hamiltonian=False,
            output_mulliken_subshell_chg=True,
            output_overlap=False,
            repulsion_2c_form='spline',
            smearing_method='none',
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/',
            )
atoms.set_calculator(calc)

# First, the repulsive terms
erep = atoms.get_potential_energy()
tester.check_float('Repulsive energy', erep, -0.1018594387 * Hartree, 1e-5)

frep = atoms.get_forces()
tester.check_array('Repulsive forces', frep, 0., 1e-5)

srep = atoms.get_stress(voigt=False)
tester.check_array('Repulsive stress', srep,
                   np.identity(3) * 0.000475066519 * Hartree / Bohr**3, 1e-5)


# Next, the band terms and k-point info
calc.set(include_repulsion=False,
         include_tightbinding=True)

eband = atoms.get_potential_energy()
tester.check_float('Band energy', eband, -2.4688290654 * Hartree, 5e-5)

efermi = calc.get_fermi_level()
tester.check_bounds('Fermi energy', efermi, [-3.703, -0.310])

nspin = calc.get_number_of_spins()
tester.check_int('Number of spins', nspin, 1)

kptcoords = calc.get_ibz_k_points()
tester.check_int('Number of k-points', len(kptcoords), 14)

igamma, iother = None, None
for ikpt, coord in enumerate(kptcoords):
    if np.allclose(coord, [0., 0., 0.]):
        igamma = ikpt
    elif np.allclose(coord, [0, 0, 1./3]):
        iother = ikpt
assert igamma is not None and iother is not None

kptweights = calc.get_k_point_weights()
kptweights_ref = [1./13.5] * 14
kptweights_ref[igamma] /= 2.
tester.check_array('K-point weights', kptweights, kptweights_ref, 1e-5)


# And also the band structure and H and S matrices
calc.set(output_bands=True,
         output_hamiltonian=True,
         output_overlap=True)
calc.calculate(atoms)

bands_ref = {igamma: [-14.783, -3.703, -3.703, -3.703,
                      -2.415, -1.401, -1.401, -1.401],
             iother: [-13.633, -8.684, -4.778, -4.778,
                      -1.875, 0.350, 0.350, 3.648]}

nkpt = len(calc.get_ibz_k_points())
print('Bands (ispin, ikpt, [...])')
for ikpt in [igamma, iother]:
    bands = calc.get_eigenvalues(kpt=ikpt, spin=0)
    tester.check_array('Bands 0 %s' % ikpt, bands, bands_ref[ikpt], 1e-3)


hamiltonian_ref = {igamma: np.array([
    [-4.620351E-01, 1.058791E-22, -5.293956E-23, -1.058791E-22, 0, 0, 0, 0],
    [-1.058791E-22, -9.455253E-02, 0.000000E+00, 0.000000E+00, 0, 0, 0, 0],
    [5.293956E-23, 0.000000E+00, -9.455253E-02, 0.000000E+00, 0, 0, 0, 0],
    [1.058791E-22, 0.000000E+00, 0.000000E+00, -9.455253E-02, 0, 0, 0, 0],
    [-4.177728E-01, -3.595651E-22, -3.601079E-22, -2.067952E-25,
     -4.620351E-01, -2.949009E-17, -3.035729E-17, -2.948998E-17],
    [3.595651E-22, 5.746504E-02, -5.169879E-26, -2.168844E-19,
     2.949009E-17, -9.455253E-02, 0.000000E+00, 0.000000E+00],
    [3.601079E-22, -5.169879E-26, 5.746504E-02, -2.151903E-19,
     3.035729E-17, 0.000000E+00, -9.455253E-02, 0.000000E+00],
    [2.067952E-25, -2.168844E-19, -2.151903E-19, 5.746504E-02,
     2.948998E-17, 0.000000E+00, 0.000000E+00, -9.455253E-02]])}

hamiltonian_ref[iother] = np.array([
    [-4.136601E-01, -8.673617E-19, -8.671500E-19, -2.601873E-18, 0, 0, 0, 0],
    [8.673617E-19, -1.390060E-01, -3.186333E-02, 3.186333E-02, 0, 0, 0, 0],
    [8.671500E-19, -3.186333E-02, -1.390060E-01, 3.186333E-02, 0, 0, 0, 0],
    [2.601873E-18, 3.186333E-02, 3.186333E-02, -1.390060E-01, 0, 0, 0, 0],
    [-2.528282E-01, 1.121282E-01, 1.121282E-01, -1.121282E-01,
     -4.136601E-01, -6.939026E-18, -8.673670E-18, -3.122518E-17],
    [-1.121282E-01, 2.434914E-02, -8.614603E-02, 8.614603E-02,
     6.939026E-18, -1.390060E-01, -3.186333E-02, 3.186333E-02],
    [-1.121282E-01, -8.614603E-02, 2.434914E-02, 8.614603E-02,
     8.673670E-18, -3.186333E-02, -1.390060E-01, 3.186333E-02],
    [1.121282E-01, 8.614603E-02, 8.614603E-02, 2.434914E-02,
     3.122518E-17, 3.186333E-02, 3.186333E-02, -1.390060E-01]]) \
    + 1j * np.array([
    [0.000000E+00, -2.633940E-02, -2.633940E-02, 2.633940E-02, 0, 0, 0, 0],
    [2.633940E-02, 8.672029E-19, -1.058791E-22, -1.058791E-22, 0, 0, 0, 0],
    [2.633940E-02, -1.058791E-22, -2.117582E-22, 2.117582E-22, 0, 0, 0, 0],
    [-2.633940E-02, -1.058791E-22, 2.117582E-22, -6.507331E-19, 0, 0, 0, 0],
    [-9.038211E-02, -7.029400E-02, -7.029400E-02, 7.029400E-02,
     -1.039479E-17, -2.633940E-02, -2.633940E-02, 2.633940E-02],
    [7.029400E-02, 1.226033E-02, 4.411538E-02, -4.411538E-02,
     2.633940E-02, 1.452836E-17, 2.081658E-17, 0.000000E+00],
    [7.029400E-02, 4.411538E-02, 1.226033E-02, -4.411538E-02,
     2.633940E-02, 2.081658E-17, 6.071638E-18, 0.000000E+00],
    [-7.029400E-02, -4.411538E-02, -4.411538E-02, 1.226033E-02,
     -2.633940E-02, 0.000000E+00, 0.000000E+00, -8.669382E-19]])

print('Hamiltonian (ispin, ikpt, iband, [...])')
for ikpt in [igamma, iother]:
    hamiltonian = calc.get_hamiltonian_matrix(kpt=ikpt, spin=0)
    for iband in range(hamiltonian.shape[0]):
        tester.check_array('Hamiltonian 0 %s %s' % (ikpt, iband),
                            hamiltonian[iband, :],
                            hamiltonian_ref[ikpt][iband, :], 5e-5)


overlap_ref = {igamma: np.array([
    [1.059137E+00, -5.293956E-23, 2.646978E-23, 5.293956E-23, 0, 0, 0, 0],
    [5.293956E-23, 9.186951E-01, 2.646978E-23, 5.293956E-23, 0, 0, 0, 0],
    [-2.646978E-23, 2.646978E-23, 9.186951E-01, 5.293956E-23, 0, 0, 0, 0],
    [-5.293956E-23, 5.293956E-23, 5.293956E-23, 9.186951E-01, 0, 0, 0, 0],
    [5.603023E-01, 1.867102E-22, 1.870591E-22, -2.964822E-21,
     1.059137E+00, 3.469436E-17, 3.295956E-17, 3.642903E-17],
    [-1.867102E-22, -1.983444E-01, 7.754818E-26, 2.403994E-23,
     -3.469436E-17, 9.186951E-01, 2.646978E-23, 3.705769E-22],
    [-1.870591E-22, 7.754818E-26, -1.983444E-01, -2.159694E-19,
     -3.295956E-17, 2.646978E-23, 9.186951E-01, -1.058791E-22],
    [2.964822E-21, 2.403994E-23, -2.159694E-19, -1.983444E-01,
     -3.642903E-17, 3.705769E-22, -1.058791E-22, 9.186951E-01]])}

overlap_ref[iother] = np.array([
    [1.014656E+00, 1.058791E-22, 0.000000E+00, -1.058791E-22, 0, 0, 0, 0],
    [-1.058791E-22, 9.800190E-01, 4.171676E-02, -4.171676E-02, 0, 0, 0, 0],
    [0.000000E+00, 4.171676E-02, 9.800190E-01, -4.171676E-02, 0, 0, 0, 0],
    [1.058791E-22, -4.171676E-02, -4.171676E-02, 9.800190E-01, 0, 0, 0, 0],
    [3.439812E-01, -1.944442E-01, -1.944442E-01, 1.944442E-01,
     1.014656E+00, 1.040841E-17, 6.938920E-18, 3.989872E-17],
    [1.944442E-01, -1.122374E-01, 2.077444E-01, -2.077444E-01,
     -1.040841E-17, 9.800190E-01, 4.171676E-02, -4.171676E-02],
    [1.944442E-01, 2.077444E-01, -1.122374E-01, -2.077444E-01,
     -6.938920E-18, 4.171676E-02, 9.800190E-01, -4.171676E-02],
    [-1.944442E-01, -2.077444E-01, -2.077444E-01, -1.122374E-01,
     -3.989872E-17, -4.171676E-02, -4.171676E-02, 9.800190E-01]]) \
    + 1j * np.array([
    [-2.646978E-23, 2.830092E-02, 2.830092E-02, -2.830092E-02, 0, 0, 0, 0],
    [-2.830092E-02, 1.734697E-18, -5.293956E-23, -5.293956E-23, 0, 0, 0, 0],
    [-2.830092E-02, -5.293956E-23, -5.188077E-21, -1.058791E-22, 0, 0, 0, 0],
    [2.830092E-02, -5.293956E-23, -1.058791E-22, -8.622795E-19, 0, 0, 0, 0],
    [1.212753E-01, 1.169925E-01, 1.169925E-01, -1.169925E-01,
     1.128248E-17, 2.830092E-02, 2.830092E-02, -2.830092E-02],
    [-1.169925E-01, -4.283639E-02, -1.144677E-01, 1.144677E-01,
     -2.830092E-02, -1.864833E-17, -3.295969E-17, 1.058791E-22],
    [-1.169925E-01, -1.144677E-01, -4.283639E-02, 1.144677E-01,
     -2.830092E-02, -3.295969E-17, -8.668985E-18, 0.000000E+00],
    [1.169925E-01, 1.144677E-01, 1.144677E-01, -4.283639E-02,
     2.830092E-02, 1.058791E-22, 0.000000E+00, -8.663241E-18]])

print('Overlap (ispin, ikpt, iband, [...])')
for ikpt in [igamma, iother]:
    overlap = calc.get_overlap_matrix(kpt=ikpt, spin=0)
    for iband in range(overlap.shape[0]):
        tester.check_array('Overlap 0 %s %s' % (ikpt, iband),
                            overlap[iband, :],
                            overlap_ref[ikpt][iband, :], 1e-5)

# And also the charges
charges_ref = {False: [0., 0.], True: [[0.50221947, -0.50221947]]*2}
for ldep in [False, True]:
    if ldep:
        charges = calc.get_mulliken_subshell_chg()
    else:
        charges = calc.get_mulliken_atom_chg()

    tester.check_array('Charges (ldep=%s):' % ldep,
                       charges, charges_ref[ldep], 1e-4)


# Add distortion to reduce the symmetry
atoms.positions += np.array([[0.4, 0.1, -0.3], [1.1, -0.2, 0.]])
cell = atoms.get_cell()
cell *= np.array([[1.03, 0.9, 0.7], [0.8, 0.99, 1.05], [1., 1., 1.05]])
atoms.set_cell(cell, scale_atoms=True)
atoms.wrap()

# Check the band energy, forces and stress
atoms.set_calculator(calc)

eband = atoms.get_potential_energy()
eband_ref = -2.3794603047 * Hartree
tester.check_float('Band energy (distorted)', eband, eband_ref, 5e-5)

fband = atoms.get_forces()
fband_ref = [[0.001882449966, -0.013662644968, 0.007898523160],
             [-0.001882449966, 0.013662644968, -0.007898523160]]
fband_ref = np.array(fband_ref) * Hartree / Bohr
for i in range(len(atoms)):
    tester.check_array('Band forces (distorted, atom %d)' % i, fband[i, :],
                       fband_ref[i, :], 5e-3)

stress = atoms.get_stress(voigt=False)
stress_ref = [[-0.001464826229, 0.000197247339, -0.00047381842],
              [0.000197247296, -0.001017789485, 0.000300957442],
              [-0.000473818408, 0.00030095743, -0.001735216041]]
stress_ref = np.array(stress_ref) * Hartree / Bohr**3
tester.check_array('Stress tensor', stress, stress_ref, 5e-4)

tester.print_summary()
