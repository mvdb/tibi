""" Single-point calculation test, involving two different elements
with s- and p-states, with and without periodic boundary conditions.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase.build import bulk
from ase.units import Hartree, Bohr
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

# NOTE: in this structure, there happen to be C-O distances
# lying in the last spline segment for repulsion, as well as
# in the switching function interval for the SlaKo integrals
atoms = bulk('CO', 'rocksalt', a=2.5)
atoms.positions[1] += [0.1, -0.2, 0.3]
N = len(atoms)

calc = Tibi(basis_set={'O': 'sp', 'C': 'sp'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            kpts=[1, 1, 1],
            output_mulliken_orbital_pop=True,
            output_mulliken_subshell_chg=True,
            overlap_2c_switching_degree=5,
            overlap_2c_switching_length=0.529,
            repulsion_2c_form='spline',
            smearing_method='fermi-dirac',
            smearing_width=0.1,
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/chno/',
            veff_2c_switching_degree=5,
            veff_2c_switching_length=0.529,
            )

erep_ref = {
    True: -0.0755457265 * Hartree,
    False: -0.0746594264 * Hartree,
}
frep_ref = {
    True: Hartree/Bohr * np.array([
            [0.053308163042, -0.326119614006, 1.210776142585],
            [-0.053308163042, 0.326119614006, -1.210776142585]]),
    False: Hartree/Bohr * np.array([
            [0.000127671314, -1.89142690000012e-05, 2.83714030000007e-05],
            [-0.000127671314, 1.89142690000012e-05, -2.83714030000007e-05]]),
}
srep_ref = {
    True: Hartree / Bohr**3 * np.array([
            [ 0.000095509233, -0.008153084382,  0.016360620244],
            [-0.008153084382, -0.019756266731, -0.035358042207],
            [ 0.016360620245, -0.035358042208, -0.061565789422]]),
     False: None,
}

eband_ref = {
    True: -1.9924635582 * Hartree,
    False: -4.8226028951 * Hartree,
}
fband_ref = {
    True: Hartree/Bohr * np.array([
            [-0.908673801074, 0.563189421876, -0.186626721861],
            [0.908673801074, -0.563189421876, 0.186626721861]]),
     False: Hartree/Bohr * np.array([
            [0.154142669285, -0.022835950562, 0.034253925862],
            [-0.154142669285, 0.022835950562, -0.034253925862]]),
}
populations_ref = {
    True: np.array([[0.00868926, 0.84906367, 1.11542948, 1.07367902],
                    [2.10835005, 1.72617240, 1.64142401, 1.47719210]]),
    False: np.array([[1.71519197, 0.81769560, 0.30745357, 0.32176611],
                     [1.89640112, 1.54485067, 1.70050355, 1.69613741]]),
}
sband_ref = {
    True: Hartree / Bohr**3 * np.array([
            [-0.155234444004,  0.062627357523, -0.066869850247],
            [ 0.062627358160, -0.161101717088,  0.078664083688],
            [-0.066869850053,  0.078664083892, -0.157145110265]]),
     False: None,
}

for pbc in [True, False]:
    atoms.set_pbc(pbc)

    ## Repulsion terms
    calc.set(include_repulsion=True,
             include_tightbinding=False)
    atoms.set_calculator(calc)

    erep = atoms.get_potential_energy()
    tag = 'Repulsive energy (PBC=%s)' % pbc
    tester.check_float(tag, erep, erep_ref[pbc], 1e-5)

    frep = atoms.get_forces()
    for i in range(N):
        tag = 'Repulsive forces on atom %d (PBC=%s)' % (i, pbc)
        tester.check_array(tag, frep[i], frep_ref[pbc][i], 1e-4)

    if pbc:
        srep = atoms.get_stress(voigt=False)
        tag = 'Repulsive stress (PBC=%s)' % pbc
        tester.check_array(tag, srep, srep_ref[pbc], 1e-4)

    ## Band properties
    ## NOTE: it would be nice to get closer agreement for the band energy /
    ## forces / stress, but this seems to be quite sensitive to details
    ## of the tail switching procedure for the current Slater-Koster tables.
    calc.set(include_repulsion=False,
             include_tightbinding=True)
    atoms.set_calculator(calc)

    eband = atoms.get_potential_energy()
    tester.check_float('Band energy (PBC=%s)' % pbc, eband, eband_ref[pbc],
                       1e-1)

    fband = atoms.get_forces()
    for i in range(N):
        tag = 'Band forces on atom %d (PBC=%s)' % (i, pbc)
        tester.check_array(tag, fband[i], fband_ref[pbc][i], 1e-1)

    if pbc:
        sband = atoms.get_stress(voigt=False)
        tag = 'Band stress (PBC=%s)' % pbc
        tester.check_array(tag, sband, sband_ref[pbc], 2e-1)

    charges_l_ref = [[2. - populations_ref[pbc][0][0],
                      2. - sum(populations_ref[pbc][0][1:4])],
                     [2. - populations_ref[pbc][1][0],
                      4. - sum(populations_ref[pbc][1][1:4])]]
    charges_atom_ref = [sum(chg) for chg in charges_l_ref]

    charges_l = calc.get_mulliken_subshell_chg()
    charges_atom = calc.get_mulliken_atom_chg()
    populations = calc.get_mulliken_orbital_pop()

    for i in range(N):
        tag = 'charges_l for atom %d (PBC=%s)' % (i, pbc)
        tester.check_array(tag, charges_l[i], charges_l_ref[i], 1e-3)
        tag = 'Atom charges for atom %d (PBC=%s)' % (i, pbc)
        tester.check_array(tag, charges_atom[i], charges_atom_ref[i], 1e-3)
        tag = 'Populations for atom %d (PBC=%s)' % (i, pbc)
        tester.check_array(tag, populations[i], populations_ref[pbc][i], 1e-3)

# Final check that populations are correctly written
# also if charges are not requested
calc.set(output_mulliken_subshell_chg=False)
calc.calculate(atoms)
populations = calc.get_mulliken_orbital_pop()
for i in range(N):
    tag = 'Populations for atom %d (PBC=%s, check)' % (i, pbc)
    tester.check_array(tag, populations[i], populations_ref[pbc][i], 1e-3)

tester.print_summary()
