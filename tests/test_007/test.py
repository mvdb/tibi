""" Single-point calculation test, now involving two different elements,
checking that energies are invariant w.r.t. atom permutations
and that forces and charges permute with the atoms.
Reference values have been obtained with DFTB+ v19.1.
"""
from itertools import permutations
import numpy as np
from ase import Atoms
from ase.units import Hartree, Bohr
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = Atoms('OH2', pbc=False,
              positions=[[-0.21533721,  0.08580833,  0.04468449],
                         [ 0.45457559,  0.63501123, -0.38857202],
                         [-0.23923839, -0.72081956, -0.49094447]])
N = len(atoms)

calc = Tibi(basis_set={'O': 'sp', 'H': 's'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            kpts=[1, 1, 1],
            kpts_gamma_shift=False,
            output_bands=False,
            repulsion_2c_form='spline',
            smearing_method='none',
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/chno/',
            )

# First, the repulsive terms
calc.set(include_repulsion=True,
         include_tightbinding=False)

erep_ref = 0.0184725565 * Hartree
frep_ref = np.array([[-0.08433594469, 0.033606476788, 0.126486684192],
                     [0.08745621122, 0.071697696813, -0.056561052202],
                     [-0.003120266531, -0.105304173601, -0.069925631991]])
frep_ref *= Hartree / Bohr

for indices in permutations(range(N)):
    a = atoms[indices]
    a.set_calculator(calc)

    erep = a.get_potential_energy()
    tag = 'Repulsive energy for permut.' % list(indices)
    tester.check_float(tag, erep, erep_ref, 1e-5)

    frep = a.get_forces()
    for i in range(N):
        tag = 'Repulsive forces on atom %d for permut. %s' % (i, list(indices))
        tester.check_array(tag, frep[i], frep_ref[indices[i]], 1e-3)


# Next, the band terms
calc.set(include_repulsion=False,
         include_tightbinding=True,
         output_mulliken_orbital_pop=True,
         output_mulliken_subshell_chg=True)

eband_ref = -3.9793338361 * Hartree
fband_ref = np.array([[0.067402721298, -0.026858873048, -0.101090312331],
                      [-0.068969165906, -0.055489866309, 0.045341381947],
                      [0.001566444608, 0.082348739357, 0.055748930384]])
fband_ref *= Hartree / Bohr

populations_ref = [[1.88532619, 1.80285811, 1.50001212, 1.84181105],
                   [0.48499626],
                   [0.48499626]]
charges_l_ref = [[2. - populations_ref[0][0],
                  4. - sum(populations_ref[0][1:4])],
                 [1. - populations_ref[1][0]],
                 [1. - populations_ref[2][0]]]

charges_atom_ref = [sum(chg) for chg in charges_l_ref]

for indices in permutations(range(N)):
    a = atoms[indices]
    a.set_calculator(calc)

    eband = a.get_potential_energy()
    tag = 'Band energy for permut. %s' % list(indices)
    tester.check_float(tag, eband, eband_ref, 1e-3)

    fband = a.get_forces()
    for i in range(N):
        tag = 'Forces on atom %d for permut. %s' % (i, list(indices))
        tester.check_array(tag, fband[i], fband_ref[indices[i]], 1e-3)

    charges_l = calc.get_mulliken_subshell_chg()
    charges_atom = calc.get_mulliken_atom_chg()
    populations = calc.get_mulliken_orbital_pop()

    for i in range(N):
        tag = 'charges_l for atom %d for permut. %s' % (i, list(indices))
        tester.check_array(tag, charges_l[i], charges_l_ref[indices[i]],
                           1e-3)
        tag = 'Atom charges for atom %d for permut. %s' % (i, list(indices))
        tester.check_array(tag, charges_atom[i], charges_atom_ref[indices[i]],
                           1e-3)
        tag = 'Populations for atom %d for permut. %s' % (i, list(indices))
        tester.check_array(tag, populations[i], populations_ref[indices[i]],
                           1e-4)

tester.print_summary()
