""" Tests for (Mulliken) self-consistent tight-binding (with and without
periodicity, with atom- and subshell-dependent auxiliary basis).
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase import Atoms
from ase.units import Bohr, Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

positions = [[-0.740273308080763, 0.666649653991325, 0.159416494587587],
             [0.006891486298212, -0.006206095648781, -0.531735097642277],
             [0.697047663527725, 0.447111938577178, -1.264187748314973],
             [0.036334158254826, -1.107555496919721, -0.464934648630337]]
cell = [[3.75, 0., 0.], [1.5, 4.5, 0.], [0.45, 1.05, 3.75]]
atoms = Atoms('OCH2', cell=cell, positions=positions)

calc = Tibi(basis_set={'O': 'sp', 'H': 's', 'C': 'sp'},
            diag_algo=tester.get_diag_algo(),
            ewald_tolerance=1e-6,
            include_repulsion=False,
            include_scf=True,
            include_tightbinding=True,
            mixing_fraction=0.25,
            mixing_history=1,
            mixing_scheme='linear',
            output_mulliken_subshell_chg=True,
            overlap_2c_switching_degree=5,
            overlap_2c_switching_length=0.529,
            scf_maxiter=100,
            scf_must_converge=True,
            scf_tolerance=1e-3,
            smearing_method='none',
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/chno/',
            veff_2c_switching_degree=5,
            veff_2c_switching_length=0.529,
            )

# Settings which depend on whether the periodicity is turned on
extra_kwargs = {
    True: dict(
        complex_wfn=True,
        kpts=[1, 1, 1],
        kpts_gamma_shift=False,
        ewald_alpha=0.3,
    ),
    False: dict(
        complex_wfn=False,
        ewald_alpha=0.,
    ),
}

# Reference values for different combinations of periodicity
# and subshell dependence of the Mulliken auxiliary basis
etot_ref = {
    (False, 'atom_dependent'): -5.4588556335 * Hartree,
    (False, 'subshell_dependent'): -5.4613679962 * Hartree,
    (True, 'atom_dependent'): -5.4428150460 * Hartree,
    (True, 'subshell_dependent'): -5.4476142302 * Hartree,
}

charges_atom_ref = {
    (False, 'atom_dependent'): [
        -0.32746986, 0.19609886, 0.06568550, 0.06568550,
    ],
    (False, 'subshell_dependent'): [
        -0.37223393, 0.21626596, 0.07798398, 0.07798398,
    ],
    (True, 'atom_dependent'): [
        -0.40851783, 0.23833539, 0.09830414, 0.07187830,
    ],
    (True, 'subshell_dependent'): [
        -0.47595501, 0.28025322, 0.11150727, 0.08419452,
    ],
}

forces_ref = {
    (False, 'atom_dependent'): np.array([
            [ 0.158254367051, -0.142515227866, -0.146390406063],
            [-0.131169305885,  0.118123902640,  0.121335848717],
            [-0.025492034431, -0.015921674867,  0.026982375949],
            [-0.001593026734,  0.040313000094, -0.001927818603],
            ]) * Hartree / Bohr,
    (False, 'subshell_dependent'): np.array([
            [ 0.156896852113, -0.141292724165, -0.145134660857],
            [-0.128157846185,  0.115411946742,  0.118550151130],
            [-0.026327400152, -0.015196698406,  0.027757505410],
            [-0.002411605775,  0.041077475829, -0.001172995683],
            ]) * Hartree / Bohr,
    (True, 'atom_dependent'): np.array([
            [ 0.169738613787, -0.141880528904, -0.166121263632],
            [-0.142562058053,  0.126745783586,  0.134792799799],
            [-0.026194089107, -0.021574390023,  0.030321865621],
            [-0.000982466627,  0.036709135342,  0.001006598212],
            ]) * Hartree / Bohr,
    (True, 'subshell_dependent'): np.array([
            [ 0.166413899047, -0.139391440701, -0.162919122944],
            [-0.137412972646,  0.122045637234,  0.130014836128],
            [-0.027180419122, -0.020676874238,  0.031038546978],
            [-0.001820507279,  0.038022677706,  0.001865739838],
            ]) * Hartree / Bohr,
}

stress_ref = {
    (False, 'atom_dependent'): None,
    (False, 'subshell_dependent'): None,
    (True, 'atom_dependent'): np.array([
            [ 0.000585019802, -0.000538947038, -0.000668505639],
            [-0.000538947036,  0.000597262061,  0.000396455480],
            [-0.000668505639,  0.000396455482,  0.000507186881],
            ]) * Hartree / Bohr**3,
    (True, 'subshell_dependent'): np.array([
            [ 0.000587189754, -0.000531181398, -0.000659054179],
            [-0.000531181391,  0.000595429891,  0.000391760332],
            [-0.000659054180,  0.000391760342,  0.000510496345],
            ]) * Hartree / Bohr**3,
}

for pbc in [False, True]:
    atoms.set_pbc(pbc)

    # Note: the tolerances indicate good agreement, but it would
    # presumably be closer if the "redundant" Slater-Koster integral pairs
    # would be identical. In the current parameter files, there are
    # some small but significant differences between e.g. the C-O s-s-sigma
    # integrals in C-O.skf and the O-C s-s-sigma integrals in O-C.skf
    # (presumably caused by insufficiently tight integration grids).
    # Results are hence partially implementation dependent.
    etol = 3e-3 if pbc else 1e-3
    chgtol = 1e-3 if pbc else 5e-4
    ftol = 1e-2 if pbc else 5e-3
    stol = 1e-3

    for ldep in ['atom_dependent', 'subshell_dependent']:
        suffix = 'pbc=%s, ldep=%s' % (pbc, ldep)

        calc.set(aux_basis_mulliken=ldep, **extra_kwargs[pbc])
        atoms.set_calculator(calc)

        etot = atoms.get_potential_energy()
        tester.check_float('Total (electronic) energy (%s)' % suffix, etot,
                           etot_ref[(pbc, ldep)], etol)

        charges_atom = calc.get_mulliken_atom_chg()
        tester.check_array('Atom charges (%s)' % suffix, charges_atom,
                           charges_atom_ref[(pbc, ldep)], chgtol)

        forces = atoms.get_forces()
        tester.check_array('Forces (%s)' % suffix, forces,
                           forces_ref[(pbc, ldep)], ftol)

        if pbc:
            stress = atoms.get_stress(voigt=False)
            tester.check_array('Stress (%s)' % suffix, stress,
                               stress_ref[(pbc, ldep)], stol)

        # Testing the restart capability
        suffix = 'pbc=%s, ldep=%s, restart' % (pbc, ldep)

        charges_l = calc.get_mulliken_subshell_chg()
        with open('scf_restart.in', 'w') as f:
            for chg in charges_l:
                f.write(' '.join(map(str, chg)) + '\n')

        calc.set(scf_initial_guess='restart',
                 scf_maxiter=1,
                 scf_must_converge=False,
                 scf_restart_input_file='scf_restart.in')
        atoms.set_calculator(calc)

        etot = atoms.get_potential_energy()
        tester.check_float('Total (electronic) energy (%s)' % suffix, etot,
                           etot_ref[(pbc, ldep)], etol)

        calc.set(scf_initial_guess='zero',
                 scf_maxiter=100,
                 scf_must_converge=True)


# Additional test case in which the Pulay mixer encounters (nearly) linearly
# dependent error vectors

calc = Tibi(aux_basis_mulliken='atom_dependent',
            basis_set={'O': 'sp', 'H': 's', 'C': 'sp'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            ewald_tolerance=1e-9,
            include_repulsion=False,
            include_scf=True,
            include_tightbinding=True,
            kpts=[1, 1, 1],
            kpts_gamma_shift=False,
            mixing_fraction=0.25,
            mixing_history=20,
            mixing_scheme='pulay',
            repulsion_2c_form='spline',
            scf_initial_guess='zero',
            scf_extrapolation='use_previous',
            scf_maxiter=50,
            scf_must_converge=True,
            scf_tolerance=1e-3,
            smearing_method='none',
            task='energy',
            tbpar_dir='../skf_files/chno/',
            )

positions = [[0., 0., 0.119262],
             [0., 0.763239, -0.477047],
             [0.,-0.763239, -0.477047]]
atoms = Atoms('OH2', pbc=False, positions=positions)
atoms.set_calculator(calc)

etot_ref = -3.9280621643 * Hartree
etot = atoms.get_potential_energy()
key = 'H2O-lindep'
tester.check_float('Total (electronic) energy (%s)' % key, etot, etot_ref, 1e-3)

tester.print_summary()
