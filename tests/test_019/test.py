""" Tests for geometry optimization with tight-binding models,
on a structure with no band gap.
Reference values have been obtained using Tibi and the LBFGS optimizer in ASE.
"""
import os
import numpy as np
from ase.build import bulk
from ase.io import read
from ase.units import Hartree
from ase.optimize import LBFGS
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = bulk('Au', 'fcc', a=4.08).repeat((2, 2, 1))
atoms.positions[0, 1:] += 0.4

kwargs = dict(basis_set={'Au': 'spd'},
              complex_wfn=True,
              diag_algo=tester.get_diag_algo(),
              ewald_tolerance=1e-6,
              include_repulsion=True,
              include_tightbinding=True,
              kpts=[1, 1, 2],
              kpts_gamma_shift=True,
              mixing_fraction=0.05,
              mixing_history=50,
              mixing_scheme='pulay',
              repulsion_2c_form='polynomial',
              scf_maxiter=50,
              scf_must_converge=True,
              scf_tolerance=1e-5,
              smearing_method='fermi-dirac',
              smearing_width=0.1,
              spin_polarized=False,
              tbpar_dir='../skf_files/',
              )

# Models to be tested
models = {
    'model001': dict(include_scf=False, aux_basis_mulliken='atom_dependent'),
    'model002': dict(include_scf=True, aux_basis_mulliken='atom_dependent'),
    'model003': dict(include_scf=True, aux_basis_mulliken='subshell_dependent'),
}

# Reference energies for each model
etot_ref = {
    'model001': -267.4325791093812,
    'model002': -267.4325785651535,
    'model003': -266.8485596348827,
}

# Reference iteration count for each model
maxiter_ref = {
    'model001': 11,
    'model002': 11,
    'model003': 12,
}

fmax = 1e-2
relax_history = 20
relax_initial_curvature = 70.
relax_maxstep = 0.2

for model in sorted(models):
    key = model
    maxiter = int(np.ceil((maxiter_ref[model] * 1.05)))

    calc = Tibi(relax_algo='lbfgs',
                relax_fmax=fmax,
                relax_history=relax_history,
                relax_initial_curvature=relax_initial_curvature,
                relax_maxiter=maxiter,
                relax_maxstep=relax_maxstep,
                relax_must_converge=True,
                relax_trajectory_append=False,
                scf_extrapolation='use_previous',
                scf_initial_guess='zero',
                scf_write_restart_file=False,
                task='relaxation',
                **kwargs, **models[model])
    atoms.set_calculator(calc)

    etot = atoms.get_potential_energy()
    tester.check_float('Total energy (%s)' % key, etot, etot_ref[model], 5e-3)

    forces = atoms.get_forces()
    fmag = np.sqrt(np.sum(forces**2, axis=1))
    tester.check_array('Force magnitudes (%s)' % key, fmag, 0., fmax)


# Additional check using the last settings to make sure
# that the 'relax.xyz' file can be read
etot = read('relax.xyz').get_potential_energy()
tester.check_float('Total energy (%s, xyz)' % key, etot, etot_ref[model], 5e-3)

# And that using ASE as a driver also still works
restart_file = 'scf_restart.out'
if os.path.exists(restart_file):
    os.remove(restart_file)

calc.set(scf_extrapolation='use_guess',
         scf_initial_guess='restart',
         scf_restart_input_file=restart_file,
         scf_write_restart_file=True,
         task='energy_forces')
atoms.set_calculator(calc)

dyn = LBFGS(atoms, maxstep=relax_maxstep, alpha=relax_initial_curvature,
            memory=relax_history, logfile='-')
dyn.run(fmax=fmax, steps=maxiter-1)

etot = atoms.get_potential_energy()
tester.check_float('Total energy (%s, ASE)' % key, etot, etot_ref[model], 1e-3)

forces = atoms.get_forces()
fmag = np.sqrt(np.sum(forces**2, axis=1))
tester.check_array('Force magnitudes (%s, ASE)' % key, fmag, 0., fmax)

tester.print_summary()
