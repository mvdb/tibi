""" Single-point calculation test for the wave function coefficients
and the (energy weighted) density matrix.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase import Atoms
from ase.units import Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = Atoms('OH2', pbc=False,
              positions=[[-0.21533721,  0.08580833,  0.04468449],
                         [ 0.45457559,  0.63501123, -0.38857202],
                         [-0.23923839, -0.72081956, -0.49094447]])

calc = Tibi(basis_set={'O': 'sp', 'H': 's'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_tightbinding=True,
            kpts=[1, 1, 1],
            kpts_gamma_shift=False,
            output_bands=True,
            output_densmat=True,
            output_ew_densmat=True,
            output_wfncoeff=True,
            smearing_method='none',
            spin_polarized=False,
            task='energy',
            tbpar_dir='../skf_files/chno/',
            )

# Get the band structure related properties from Tibi
atoms.set_calculator(calc)
eband = atoms.get_potential_energy()
tester.check_float('Band energy', eband, -3.9793338361 * Hartree, 1e-3)

bands = calc.get_eigenvalues(kpt=0, spin=0)
occupations = calc.get_occupation_numbers(kpt=0, spin=0)
wfncoeff = calc.get_wfncoeff_matrix(kpt=0, spin=0)
densmat = calc.get_density_matrix(kpt=0, spin=0)
ew_densmat = calc.get_energy_weighted_density_matrix(kpt=0, spin=0)

# Set up the corresponding reference values
wfncoeff_ref = np.array([
        [-0.939479, 0, 0.179503, 0, 0, 0.91008],
        [-0.008307, 0.330396, 0.485627, -0.707107, -0.448161, -0.365024],
        [0.00331, 0.645651, -0.193514, 0.40558, -0.875782, 0.145456],
        [0.012459, 0.04875, -0.728341, -0.579228, -0.066126, 0.547462],
        [0.060897, 0.309173, 0.156435, 0, 0.933745, 0.888254],
        [0.060897, -0.309173, 0.156435, 0, -0.933745, 0.888254]]).T

densmat_ref = np.zeros_like(wfncoeff_ref)
ew_densmat_ref = np.zeros_like(wfncoeff_ref)
nband = 6

for inu in range(nband):
    for imu in range(nband):
        for iband in range(nband):
            term = wfncoeff_ref[iband, inu] * wfncoeff_ref[iband, imu]
            term *= occupations[iband]
            densmat_ref[inu, imu] += term
            ew_densmat_ref[inu, imu] += term * bands[iband] / Hartree

densmat_ref = np.tril(densmat_ref)
ew_densmat_ref = np.tril(ew_densmat_ref)

# Check that the two sets are in agreement
for iband in range(nband):
    # Wave function coefficients are allowed
    # to differ in sign from the reference
    maxdiff = min([np.max(np.abs(wfncoeff[iband, :] - wfncoeff_ref[iband, :])),
                   np.max(np.abs(wfncoeff[iband, :] + wfncoeff_ref[iband, :]))])
    tester.check_float('Band %d wfncoeff maxdiff' % iband, maxdiff, 0, 1e-4)

    # Now the density matrix
    maxdiff = np.max(np.abs(densmat[:, iband] - densmat_ref[:, iband]))
    tester.check_float('Band %d densmat maxdiff' % iband, maxdiff, 0, 5e-4)

    # And the energy-weighted density matrix
    maxdiff = np.max(np.abs(ew_densmat[:, iband] - ew_densmat_ref[:, iband]))
    tester.check_float('Band %d ew_densmat maxdiff' % iband, maxdiff, 0, 5e-4)

tester.print_summary()
