""" Tests for Giese-York self-consistency (periodic structure). """
import numpy as np
import os
from ase import Atoms
from ase.calculators.socketio import SocketIOCalculator
from ase.units import Bohr
from tibi import Tibi
from tibi.test import Tester


tester = Tester()

unixsocket = 'ase_tibi_test038'

ewald_alpha_ref = 0.5 / Bohr

lmax_mp = dict(P=1, O=2)
nzeta_mp = dict(P=2, O=3)
naux_mp = {
    sym: nzeta_mp[sym] * (lmax_mp[sym] + 1)**2
    for sym in ['P', 'O']
}
aux_basis_giese_york = {
    sym: '{0}{1}'.format(nzeta_mp[sym], 'SPD'[lmax_mp[sym]])
    for sym in ['P', 'O']
}

tbpar_dir = '../skf_files/p_o'

if not os.path.exists(os.path.join(tbpar_dir, 'O-O.skf')):
    tester.reason_for_skipping = 'integral tables have not been generated'
    tester.print_summary()
    exit()

calc = Tibi(aux_basis_giese_york=aux_basis_giese_york,
            basis_set={'P': 'spd_sp', 'O': 'spd_sp'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_scf=True,
            include_tightbinding=True,
            kpts=(5, 4, 3),
            kpts_gamma_shift=True,
            mixing_fraction=0.1,
            mixing_pulay_restart=True,
            output_densmat=False,
            output_giese_york_chg=True,
            output_giese_york_mag=True,
            output_mulliken_orbital_mag=False,
            output_mulliken_orbital_pop=False,
            output_mulliken_subshell_chg=True,
            output_mulliken_subshell_mag=True,
            scf_aux_mapping='giese_york',
            scf_aux_use_delta=True,
            scf_initial_guess='restart',
            scf_maxiter=300,
            scf_must_converge=True,
            scf_tolerance=1e-4,
            scf_ukernel_expansion_offsite=2,
            scf_ukernel_expansion_onsite=2,
            scf_ukernel_off2c_model='tabulated',
            scf_wkernel_expansion_offsite=2,
            scf_wkernel_expansion_onsite=2,
            scf_write_verbose=False,
            smearing_method='fermi-dirac',
            smearing_width=0.002,
            spin_fix_nupdown=True,
            spin_nupdown=2,
            task='energy_forces',
            tbpar_dir=tbpar_dir,
            veff_expansion_offsite=2,
            veff_expansion_onsite=1,
            )

positions = np.array([
        [-0.02083789, -0.00281334, -0.10680980],
        [ 0.08201354,  1.35370389,  0.46822299],
        [ 0.02514407,  1.38111127,  1.98834384],
        [-0.04545038,  0.02757270,  2.66616222],
        ])
cell = np.array([
        [ 2.6, 0.0, 0.0],
        [-1.0, 2.0, 0.0],
        [ 0.3, 0.0, 4.2],
        ])
atoms = Atoms('POPO', positions=positions, cell=cell, pbc=True)
N = len(atoms)

models = {}
for spinpol in [True, False]:
    models[spinpol] = dict(spin_polarized=spinpol)

e_ref = {  # regression values for every model
    False: -190.50885672167107,
    True: -189.00902771019582,
}

chg_ref = {  # regression values for every model
    False: np.array([
        [-7.008353e-01, -1.376838e-04, -6.703115e-02, -7.636089e-02,
          4.999329e-01, -5.234582e-02, -2.746614e-02,  2.888471e-02,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00],
        [ 2.295795e-01,  3.392134e-02,  8.830665e-04,  1.982741e-02,
          1.711003e-02, -1.612522e-02, -8.532938e-03,  9.368527e-03,
         -1.814711e-03,  6.327164e-02, -9.586645e-02, -2.049193e-02,
         -4.972842e-02, -5.040631e-02,  1.778802e-02,  1.915564e-02,
         -3.426574e-02, -2.540116e-03, -1.261321e-01,  6.502607e-02,
          1.818528e-02,  2.560169e-02,  2.253454e-02, -1.384756e-02,
         -8.830705e-03,  2.011027e-02, -5.271526e-03],
        [-6.441124e-01,  8.259721e-02,  6.480381e-02, -1.320716e-01,
          4.994134e-01,  6.642861e-02,  1.240781e-02,  3.700148e-02,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00],
        [ 2.502653e-01, -5.968735e-02, -8.454530e-03,  7.039549e-02,
          2.969527e-02,  1.365254e-02,  2.155886e-03,  5.466393e-03,
          7.736234e-03,  4.337396e-02,  1.302182e-01,  2.926167e-02,
         -5.910536e-02, -6.780525e-02, -1.040049e-02, -1.749165e-02,
         -3.632016e-02,  9.816366e-03, -1.148241e-01, -8.874462e-02,
         -2.225296e-02,  9.161273e-03,  3.827945e-02,  6.679390e-03,
          3.458296e-03,  2.512450e-02, -1.871243e-02],
    ]),
    True: np.array([
        [-7.095003e-01, -4.745707e-03, -6.676627e-02, -6.505847e-02,
          5.062436e-01, -5.012868e-02, -2.220626e-02,  3.234834e-02,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00],
        [ 2.323384e-01,  2.836277e-02,  8.558271e-04,  1.792096e-02,
          1.505474e-02, -1.647254e-02, -1.271043e-02,  9.501275e-03,
          4.501909e-04,  5.486356e-02, -9.021245e-02, -1.925045e-02,
         -5.230783e-02, -5.045311e-02,  1.671852e-02,  2.481228e-02,
         -3.576674e-02, -2.579424e-03, -1.195858e-01,  6.347124e-02,
          1.698389e-02,  2.945450e-02,  2.210726e-02, -1.005266e-02,
         -1.024612e-02,  1.858642e-02, -5.382771e-03],
        [-6.551754e-01,  8.354083e-02,  6.758183e-02, -1.335814e-01,
          5.090921e-01,  6.380322e-02,  1.109938e-02,  4.318987e-02,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00],
        [ 2.587818e-01, -5.566644e-02, -8.461451e-03,  7.255067e-02,
          2.823603e-02,  1.552924e-02,  4.511665e-03,  5.019134e-03,
          8.402366e-03,  2.728292e-02,  1.272108e-01,  2.696262e-02,
         -5.754016e-02, -6.893491e-02, -1.417884e-02, -2.130630e-02,
         -3.635838e-02,  8.746276e-03, -1.044080e-01, -8.531897e-02,
         -2.118368e-02,  6.393252e-03,  3.948419e-02,  9.298669e-03,
          3.756024e-03,  2.350011e-02, -1.650825e-02],
    ]),
}

mag_ref = {  # regression values for every model
    True: np.array([
        [ 2.387790e-01,  4.039129e-03, -1.243772e-02,  9.974805e-03,
         -4.082022e-02, -2.193398e-02, -6.413346e-03,  5.799923e-03,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00],
        [ 4.577453e-02, -3.693389e-03, -2.480108e-03, -1.137824e-03,
          6.253742e-03, -3.369547e-03, -4.198707e-03,  2.664740e-03,
          2.743137e-03,  1.124181e-02,  6.414916e-03,  6.723862e-03,
         -5.364069e-03, -1.385780e-02,  4.311032e-03,  6.108190e-03,
         -1.053964e-02, -4.137843e-03,  2.266855e-02, -6.229730e-03,
         -6.077542e-03,  1.070827e-02,  1.354706e-02, -1.473012e-03,
         -3.006626e-04,  1.186610e-02,  2.318203e-03],
        [ 2.162533e-01, -9.414365e-03, -7.540080e-05, -1.090873e-02,
         -5.259853e-03,  3.196479e-02,  1.095290e-03, -5.059408e-03,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00,  0.000000e+00,
          0.000000e+00,  0.000000e+00,  0.000000e+00],
        [ 4.431906e-02, -6.666184e-03,  5.824302e-04,  3.312129e-03,
          5.643396e-03,  2.483450e-03,  1.420234e-04,  6.019634e-04,
          2.740518e-03,  9.636528e-03,  7.993626e-03,  5.408245e-04,
          5.253316e-03, -1.124455e-02, -1.780609e-03, -3.934875e-03,
         -7.580899e-03, -3.960786e-03,  2.159329e-02,  1.131836e-03,
          2.621626e-03, -1.938385e-03,  1.364944e-02, -1.692121e-03,
          3.982486e-03,  1.154060e-02,  5.884371e-04],
    ]),
}

f_ref = {  # obtained with calc.calculate_numerical_forces (d=1e-4)
    False: np.array([
        [ 7.40265554,  1.85967487, -4.86439631],
        [-8.38795566, -1.99971092,  0.24068638],
        [-7.35255855, -1.85341046, -4.23626312],
        [ 8.33677468,  1.9929358 ,  8.86093525]
    ]),
    True: np.array([
        [ 7.86002901,  1.8818079 , -3.71255627],
        [-9.01538165, -1.87266245, -0.75232655],
        [-8.23349477, -1.47813346, -4.79802887],
        [ 9.38931444,  1.46895616,  9.26408656],
    ]),
}

s_ref = {  # obtained with calc.calculate_numerical_stress (d=1e-4)
    False: np.array([
        -1.76397461, -0.32892231, -1.27425893,
         0.03931503, -0.17794005, -0.69441997,
    ]),
    True: np.array([
        -1.82703946, -0.52032442, -1.26652049,
        -0.03560961, -0.12506453, -0.74828933,
    ]),
}


def write_restart_file(spinpol):
    with open('scf_restart.in', 'w') as f:
        for atom in atoms:
            sym = atom.symbol
            f.write(' '.join(['0'] * naux_mp[sym]))
            if spinpol:
                m = 0 if sym == 'P' else 1. / np.sqrt(4 * np.pi)
                f.write(' '.join([' ' + str(m)] + ['0']*(naux_mp[sym]-1)))
            f.write('\n')
    return


def lists_to_ndarray(lists):
    arr = np.zeros((N, max(naux_mp.values())))
    for i, lst in enumerate(lists):
        arr[i, :len(lst)] = lst
    return arr


if False:
    # For regenerating the references when necessary

    spinpol = False
    write_restart_file(spinpol)

    model = spinpol
    calc.set(ewald_alpha=ewald_alpha_ref, **models[model])
    atoms.set_calculator(calc)

    e = atoms.get_potential_energy()
    print('Reference energy:', e)

    chg = lists_to_ndarray(calc.get_giese_york_chg())
    print('Reference Giese-York charges:', np.array2string(chg, separator=', '))

    if spinpol:
        mag = lists_to_ndarray(calc.get_giese_york_mag())
        print('Reference Giese-York magnetizations:',
              np.array2string(mag, separator=', '))

    calc.set(socket_type='unix',
             socket_unix_suffix=unixsocket,
             task='socket')

    with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:
        atoms.calc = socketcalc

        f = calc.calculate_numerical_forces(atoms, d=1e-4)
        print('Reference forces:', np.array2string(f, separator=', '))

        s = calc.calculate_numerical_stress(atoms, d=1e-4)
        print('Reference stress:', np.array2string(s, separator=', '))

        atoms.calc.server.protocol.end()

    exit()


atoms.set_calculator(calc)


for model, param in models.items():
    spinpol = model
    write_restart_file(spinpol)

    f_tol = 3e-3
    s_tol = 3e-3

    ewald_alphas = [ewald_alpha_ref]
    e_tols = [5e-5]
    q_tols = [5e-6]

    if not spinpol:
        ewald_alphas.append(0.1)
        e_tols.append(5e-5)
        q_tols.append(5e-5)

    for ewald_alpha, e_tol, q_tol in zip(ewald_alphas, e_tols, q_tols):
        tag = 'spinpol={0}, alpha={1:.3f}'.format(spinpol, ewald_alpha)
        calc.reset()
        calc.set(ewald_alpha=ewald_alpha, **param)

        e = atoms.get_potential_energy()
        tester.check_float('Total energy ({0})'.format(tag), e, e_ref[model],
                           e_tol)

        chg = lists_to_ndarray(calc.get_giese_york_chg())
        tester.check_array('Giese-York charges ({0})'.format(tag), chg,
                           chg_ref[model], q_tol)

        if spinpol:
            mag = lists_to_ndarray(calc.get_giese_york_mag())
            tester.check_array('Giese-York magnetizations ({0})'.format(tag),
                               mag, mag_ref[model], q_tol)

        f = atoms.get_forces()
        tester.check_array('Forces ({0})'.format(tag), f, f_ref[model], f_tol)

        s = atoms.get_stress()
        tester.check_array('Stress ({0})'.format(tag), s, s_ref[model], s_tol)

tester.print_summary()
