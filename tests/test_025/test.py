""" Tests for variable-cell relaxation with (Mulliken) self-consistent
tight-binding. Reference values have been obtained using Tibi and
the LBFGS optimizer in ASE.
"""
import numpy as np
from ase.build import bulk
from ase.io import read
from ase.io.trajectory import Trajectory
from ase.units import Hartree
from ase.constraints import FixAtoms, StrainFilter, UnitCellFilter
from ase.calculators.socketio import SocketIOCalculator
from ase.optimize import LBFGS
from tibi import Tibi
from tibi.test import Tester


def get_atoms():
    atoms = bulk('C', 'diamond', a=3.6).repeat((2, 2, 1))
    cell = atoms.get_cell()
    atoms.numbers[1] = 8
    atoms.positions[1] += [0.2, -0.2, 0.]
    cell[0, :] *= 1.1
    cell[1, 1:] *= 0.9
    atoms.set_cell(cell, scale_atoms=True)
    return atoms


tester = Tester()


kwargs = dict(aux_basis_mulliken='subshell_dependent',
              basis_set={'C': 'sp', 'O': 'sp'},
              complex_wfn=True,
              diag_algo=tester.get_diag_algo(),
              ewald_tolerance=1e-6,
              include_repulsion=True,
              include_scf=True,
              include_tightbinding=True,
              kpts=[1, 1, 2],
              kpts_gamma_shift=True,
              mixing_fraction=0.25,
              mixing_history=8,
              mixing_scheme='pulay',
              repulsion_2c_form='spline',
              scf_extrapolation='use_previous',
              scf_initial_guess='zero',
              scf_maxiter=50,
              scf_must_converge=True,
              scf_tolerance=1e-3,
              scf_write_restart_file=False,
              smearing_method='fermi-dirac',
              smearing_width=0.05,
              spin_polarized=False,
              tbpar_dir='../skf_files/chno/',
              )

# Reference values without and with fixed scaled positions
etot_ref = {False: -383.517707, True: -380.239162}
maxiter_ref = {False: 30, True: 15}

etol = 1e-4
fmax = 1e-2
relax_history = 12
relax_initial_curvature = 70.
relax_maxstep = 0.2
ase_lbfgs_kwargs = dict(alpha=relax_initial_curvature, logfile='-',
                        maxstep=relax_maxstep, memory=relax_history)

for fix_spos in [False, True]:
    key = 'fix_spos=%s' % fix_spos
    maxiter = maxiter_ref[fix_spos]
    eref = etot_ref[fix_spos]

    atoms = get_atoms()
    if fix_spos:
        c = FixAtoms(indices=range(len(atoms)))
        atoms.set_constraint(c)

    calc = Tibi(relax_algo='lbfgs',
                relax_fmax=fmax,
                relax_history=relax_history,
                relax_initial_curvature=relax_initial_curvature,
                relax_maxiter=maxiter,
                relax_maxstep=relax_maxstep,
                relax_must_converge=True,
                relax_vc=True,
                relax_vc_maxstep=relax_maxstep,
                relax_vc_scale=0.,
                relax_vc_smax=fmax / len(atoms),
                relax_trajectory_append=False,
                task='relaxation',
                **kwargs)
    atoms.set_calculator(calc)

    etot = atoms.get_potential_energy()
    tester.check_float('Total energy (%s)' % key, etot, eref, etol)

    if not fix_spos:
        forces = atoms.get_forces()
        fmag = np.sqrt(np.sum(forces**2, axis=1))
        tester.check_array('Force magnitudes (%s)' % key, fmag, 0., fmax)

    stress = atoms.get_stress()
    smag = np.max(np.abs(stress))
    tester.check_array('Stress magnitudes (%s)' % key, smag, 0., fmax)

    # Make sure the 'relax.xyz' file can be read
    a = read('relax.xyz')
    etot = a.get_potential_energy()
    tester.check_float('Total energy (%s, xyz)' % key, etot, eref, etol)

    if fix_spos:
        spos = a.get_scaled_positions(wrap=False)
        spos_ref = atoms.get_scaled_positions(wrap=False)
        tester.check_array('Scaled positions (%s, xyz)' % key, spos - spos_ref,
                           0., 1e-8)

    # And that a (unix) socket connection also works
    atoms = get_atoms()
    filter = StrainFilter(atoms) if fix_spos else UnitCellFilter(atoms)

    trajfile = 'opt.traj'
    trajectory = Trajectory(trajfile, mode='w', atoms=atoms)

    unixsocket = 'ase_tibi_test025'
    calc.set(socket_type='unix',
             socket_unix_suffix=unixsocket,
             task='socket')

    with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:
        atoms.calc = socketcalc

        dyn = LBFGS(filter, **ase_lbfgs_kwargs)
        dyn.attach(trajectory)
        dyn.run(fmax=fmax, steps=maxiter-1)

        etot = atoms.get_potential_energy()
        tester.check_float('Total energy (%s, unix)' % key,
                           etot, etot_ref[fix_spos], etol)

        if not fix_spos:
            forces = atoms.get_forces()
            fmag = np.sqrt(np.sum(forces**2, axis=1))
            tester.check_array('Force magnitudes (%s, unix)' % key,
                               fmag, 0., fmax)

        stress = atoms.get_stress()
        smag = np.max(np.abs(stress))
        tester.check_array('Stress magnitudes (%s, unix)' % key, smag, 0., fmax)

        # Needed for graceful exit:
        atoms.calc.server.protocol.end()

    trajectory.close()

    # And also a few steps via the FileIO interface
    atoms = read(trajfile + '@-5')
    filter = StrainFilter(atoms) if fix_spos else UnitCellFilter(atoms)

    calc.set(task='energy_forces')
    atoms.set_calculator(calc)

    dyn = LBFGS(filter, **ase_lbfgs_kwargs)
    dyn.run(fmax=fmax, steps=maxiter)

    etot = atoms.get_potential_energy()
    tester.check_float('Total energy (%s, ASE)' % key, etot, eref, etol)

    if not fix_spos:
        forces = atoms.get_forces()
        fmag = np.sqrt(np.sum(forces**2, axis=1))
        tester.check_array('Force magnitudes (%s, ASE)' % key, fmag, 0., fmax)

    stress = atoms.get_stress()
    smag = np.max(np.abs(stress))
    tester.check_array('Stress magnitudes (%s, ASE)' % key, smag, 0., fmax)

tester.print_summary()
