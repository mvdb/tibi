""" Tests for geometry optimization with tight-binding models,
on a structure with a band gap.
Reference values have been obtained using Tibi and the LBFGS optimizer in ASE.
"""
import os
import numpy as np
from ase import Atoms
from ase.io import read
from ase.units import Hartree
from ase.constraints import FixAtoms, FixCartesian
from ase.calculators.socketio import SocketIOCalculator
from ase.optimize import LBFGS
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

cell = [3., 3., 3.]
positions = np.array([[-0.21533721,  0.08580833,  0.04468449],
                      [ 0.45457559,  0.63501123, -0.38857202],
                      [-0.23923839, -0.72081956, -0.49094447]])
atoms = Atoms('OH2', cell=cell, positions=positions)

kwargs = dict(basis_set={'O': 'sp', 'H': 's'},
              complex_wfn=False,
              diag_algo=tester.get_diag_algo(),
              include_repulsion=True,
              include_tightbinding=True,
              kpts=[1, 1, 1],
              kpts_gamma_shift=False,
              mixing_fraction=0.25,
              mixing_history=20,
              mixing_scheme='pulay',
              repulsion_2c_form='spline',
              scf_maxiter=50,
              scf_must_converge=True,
              scf_tolerance=1e-3,
              smearing_method='none',
              spin_polarized=False,
              tbpar_dir='../skf_files/chno/',
              )

# Models to be tested, for both pbc=False and pbc=True
models = {
    'model001': dict(include_scf=False, aux_basis_mulliken='atom_dependent'),
    'model002': dict(include_scf=True, aux_basis_mulliken='atom_dependent'),
    'model003': dict(include_scf=True, aux_basis_mulliken='subshell_dependent'),
}

# Reference energies for each model and periodicity
etot_ref = {
    'model001': {False: -107.82214210072115, True: -105.71393942425559},
    'model002': {False: -106.39290132939013, True: -104.40907482348156},
    'model003': {False: -106.73231221263838, True: -104.79628277839548},
}

# Reference iteration count for each model and periodicity
maxiter_ref = {
    'model001': {False: 8, True: 28},
    'model002': {False: 4, True: 26},
    'model003': {False: 6, True: 27},
}

fmax = 1e-2
relax_history = 20
relax_initial_curvature = 70.
relax_maxstep = 0.2

for model in sorted(models):
    for pbc in [False, True]:
        key = '%s, pbc=%s' % (model, pbc)
        maxiter = int(np.ceil((maxiter_ref[model][pbc] * 1.05)))

        calc = Tibi(ewald_tolerance=1e-6 if pbc else 1e-9,
                    relax_algo='lbfgs',
                    relax_fmax=fmax,
                    relax_history=relax_history,
                    relax_initial_curvature=relax_initial_curvature,
                    relax_maxiter=maxiter,
                    relax_maxstep=relax_maxstep,
                    relax_must_converge=True,
                    relax_trajectory_append=False,
                    scf_extrapolation='use_previous',
                    scf_initial_guess='zero',
                    scf_write_restart_file=False,
                    task='relaxation',
                    **kwargs, **models[model])

        atoms.set_pbc(pbc)
        atoms.set_calculator(calc)

        etot = atoms.get_potential_energy()
        tester.check_float('Total energy (%s)' % key, etot,
                           etot_ref[model][pbc], 1e-3)

        forces = atoms.get_forces()
        fmag = np.sqrt(np.sum(forces**2, axis=1))
        tester.check_array('Force magnitudes (%s)' % key, fmag, 0., fmax)


# Additional check using the last settings to make sure
# that the 'relax.xyz' file can be read
etot = read('relax.xyz').get_potential_energy()
tester.check_float('Total energy (%s, xyz)' % key, etot,
                   etot_ref[model][pbc], 5e-3)


# And that atoms can be constrained
constraints_masks = [
   (FixAtoms(indices=[0]), np.array([[1]*3, [0]*3, [0]*3])),
   (FixCartesian(1, mask=(1, 0, 1)), np.array([[0]*3, [1, 0, 1], [0]*3])),
]

for constraint, mask in constraints_masks:
    calc.set(relax_maxiter=2*maxiter)
    calc.reset()  # to force recalculation
    atoms.set_constraint(constraint)

    etot = atoms.get_potential_energy()
    tester.check_float('Total energy (%s, %s)' % (key, constraint), etot,
                       etot_ref[model][pbc], 1e-3)

    pos = read('relax.xyz').get_positions()

    select = np.where(mask == 1)
    tester.check_array('Fixed coordinates (%s, %s)' % (key, constraint),
                       pos[select], positions[select], 1e-8)

    select = np.where(mask == 0)
    tester.check_array('Free coordinates (%s, %s)' % (key, constraint),
                       1. / (pos[select] - positions[select]), 0., 1000.)

    forces = atoms.get_forces()
    fmag = np.sqrt(np.sum(forces**2, axis=1))
    tester.check_array('Force magnitudes (%s, %s)' % (key, constraint),
                       fmag, 0., fmax)

    del atoms.constraints


# And that using ASE as a driver also still works
restart_file = 'scf_restart.out'
if os.path.exists(restart_file):
    os.remove(restart_file)

calc.set(scf_extrapolation='use_guess',
         scf_initial_guess='restart',
         scf_restart_input_file=restart_file,
         scf_write_restart_file=True,
         task='energy_forces')
atoms.set_calculator(calc)

ase_lbfgs_kwargs = dict(alpha=relax_initial_curvature, logfile='-',
                        maxstep=relax_maxstep, memory=relax_history)
dyn = LBFGS(atoms, **ase_lbfgs_kwargs)
dyn.run(fmax=fmax, steps=maxiter-1)

etot = atoms.get_potential_energy()
tester.check_float('Total energy (%s, ASE)' % key, etot,
                   etot_ref[model][pbc], 1e-3)

forces = atoms.get_forces()
fmag = np.sqrt(np.sum(forces**2, axis=1))
tester.check_array('Force magnitudes (%s, ASE)' % key, fmag, 0., fmax)


# And also via a socket connection
socket_kwargs = {'unix': {'unixsocket': 'ase_tibi'},
                 'inet': {'port': 31415}}

calc.set(scf_extrapolation='use_previous',
         scf_initial_guess='zero',
         scf_write_restart_file=False,
         socket_inet_host='localhost',
         socket_inet_port=socket_kwargs['inet']['port'],
         socket_unix_suffix=socket_kwargs['unix']['unixsocket'],
         task='socket')

restart_file = 'scf_restart.out'
if os.path.exists(restart_file):
    os.remove(restart_file)

for socket_type in sorted(socket_kwargs):
    calc.set(socket_type=socket_type)
    atoms.set_positions(positions)

    with SocketIOCalculator(calc, **socket_kwargs[socket_type]) as socketcalc:
        atoms.calc = socketcalc

        dyn = LBFGS(atoms, **ase_lbfgs_kwargs)
        dyn.run(fmax=fmax, steps=maxiter-1)

        etot = atoms.get_potential_energy()
        tester.check_float('Total energy (%s, %s)' % (key, socket_type),
                           etot, etot_ref[model][pbc], 1e-3)

        forces = atoms.get_forces()
        fmag = np.sqrt(np.sum(forces**2, axis=1))
        tester.check_array('Force magnitudes (%s, %s)' % (key, socket_type),
                           fmag, 0., fmax)

        # Needed for graceful exit:
        atoms.calc.server.protocol.end()

tester.print_summary()
