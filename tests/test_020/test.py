""" Tests for (Mulliken) self-consistent tight-binding with
more than one subshell of the same angular momentum in the main basis.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase import Atoms
from ase.units import Bohr, Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

calc = Tibi(aux_basis_mulliken='atom_dependent',
            basis_set={'O': 'sp', 'H':'s_s'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            ewald_tolerance=1e-9,
            include_repulsion=False,
            include_scf=True,
            include_tightbinding=True,
            kpts=[1, 1, 1],
            kpts_gamma_shift=False,
            mixing_fraction=0.25,
            mixing_history=20,
            mixing_scheme='pulay',
            output_mulliken_subshell_chg=True,
            repulsion_2c_form='spline',
            scf_extrapolation='use_previous',
            scf_initial_guess='zero',
            scf_maxiter=50,
            scf_must_converge=True,
            scf_tolerance=1e-4,
            smearing_method='none',
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/ohh+/',
            )

positions = [[0., 0.763239, -0.477047],
             [0., 0., 0.119262],
             [0.,-0.763239, -0.477047]]
atoms = Atoms('HOH', pbc=False, positions=positions)
atoms.set_calculator(calc)

etot_ref = -4.2021057627 * Hartree
etot = atoms.get_potential_energy()
tester.check_float('Electronic energy', etot, etot_ref, 1e-3)

forces_ref = np.array([[0., -0.179295078347, 0.107309936100],
                       [0., 0.000081068675, -0.214556532429],
                       [0., 0.179214009673, 0.107246596329]]) * Hartree / Bohr
forces = atoms.get_forces()
tester.check_array('Forces', forces, forces_ref, 1e-2)

charges_ref = [[0.27893350, -0.03409933],
               [0.22465472, -0.71417258],
               [0.27888078, -0.03419708]]
charges = calc.get_mulliken_subshell_chg()
tester.check_array('Subshell charges', charges, charges_ref, 5e-4)

tester.print_summary()
