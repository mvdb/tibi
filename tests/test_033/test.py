""" Tests for 3-center repulsion, with multiple elements. """
import os
from itertools import permutations
import numpy as np
from ase import Atoms
from ase.units import Hartree
from ase.calculators.socketio import SocketIOCalculator
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

unixsocket = 'ase_tibi_test033'
tbpar_dir = './'

calc = Tibi(basis_set={'O': 'sp', 'C': 'sp'},
            diag_algo=tester.get_diag_algo(),
            include_repulsion=True,
            include_tightbinding=False,
            repulsion_2c_form='spline',
            repulsion_3c_eps_inner=1e-6,
            repulsion_3c_eps_outer=1e-8,
            repulsion_3c_interpolation=3,
            repulsion_3c_upsampling=(1, 1, 1),
            repulsion_expansion=3,
            socket_type='unix',
            socket_unix_suffix=unixsocket,
            task='socket',
            tbpar_dir=tbpar_dir,
            )

d = 1.2
atoms = Atoms('COO', cell=[12.]*3, pbc=False,
              positions=[[0.]*3, [d, 0., 0.], [0.5*d, np.sqrt(3)/2.*d, 0.]])
atoms.euler_rotate(phi=20.0, theta=-32.0, psi=83.0, center='COP')
atoms.center()
N = len(atoms)

e_ref = 2.5975676 * Hartree  # 3c terms contributing by -0.017831 Hartree
e_tol, f_tol = 7.5e-3, 2.5e-3
# Note: deviations seem to mainly come from the (much larger) 2-center terms

for indices in permutations(range(N)):
    a = atoms[indices]
    tag = list(indices)

    with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:
        a.calc = socketcalc

        e = a.get_potential_energy()
        tester.check_float('Repulsive energy (%s)' % tag, e, e_ref, e_tol)

        f = a.get_forces()
        f_ref = calc.calculate_numerical_forces(a, d=1e-4)
        tester.check_array('Repulsive forces (%s)' % tag, f, f_ref, f_tol)

        a.calc.server.protocol.end()

# Final check with binary IO
assert not any([f.endswith('.3cb') for f in os.listdir(tbpar_dir)])
calc.reset()
calc.set(task='energy', repulsion_3c_binary_io=True)
atoms.set_calculator(calc)
e_ref = atoms.get_potential_energy()

calc.reset()
atoms.set_calculator(calc)
e = atoms.get_potential_energy()
tester.check_float('Total energy (binary IO)', e, e_ref, 1e-8)
os.system('rm {0}/*.3cb'.format(tbpar_dir))

tester.print_summary()
