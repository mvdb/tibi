""" Test for diag_parallel_ks (to verify thread safety and parallel activity)
and for diag_nband.
"""
import numpy as np
import os
from ase.build import bulk
from ase.calculators.socketio import SocketIOCalculator
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

if tester.get_diag_algo() == 'elpa':
    # needed for correct results with OMP_NUM_THREADS > 1
    os.environ.update(ELPA_DEFAULT_omp_threads='1')
    # needed to avoid segfaults with OMP_NUM_THREADS > 1 and Intel OpenMP RTL
    os.environ.update(OMP_STACKSIZE='8M')

atoms = bulk('Au', 'fcc', a=4.08, cubic=True).repeat((4, 4, 1))

scf_wkernel_on1c = {
    'Au': [-0.4, -0.325, -0.2, -0.325, -0.35, -0.25, -0.2, -0.25, -0.3],
}

unixsocket = 'ase_tibi_test041'

kwargs = dict(aux_basis_mulliken='subshell_dependent',
              basis_set={'Au': 'spd'},
              diag_algo=tester.get_diag_algo(),
              diag_parallel_ks=True,
              include_repulsion=False,
              include_scf=True,
              include_tightbinding=True,
              kpts=[1, 1, 2],
              kpts_gamma_shift=True,
              output_wfncoeff=True,
              scf_aux_mapping='mulliken',
              scf_extrapolation='use_guess',
              scf_maxiter=10,
              scf_must_converge=True,
              scf_tolerance=1e-3,
              scf_wkernel_on1c=scf_wkernel_on1c,
              scf_write_restart_file=False,
              smearing_method='fermi-dirac',
              smearing_width=0.1,
              socket_unix_suffix=unixsocket,
              spin_fix_nupdown=True,
              spin_nupdown=len(atoms)*0.1,
              spin_polarized=True,
              task='socket',
              tbpar_dir='../skf_files/',
              )

etot_ref = {  # obtained with diag_nband=0
    True: -4806.1242721748895,
    False: -4819.6678385058485,
}

for complex_wfn, diag_nband in [(True, 1.08), (False, 381)]:
    calc = Tibi(complex_wfn=complex_wfn, diag_nband=diag_nband, **kwargs)
    atoms.set_calculator(calc)

    with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:
        atoms.calc = socketcalc

        for i in range(2):
            tag = 'complex={0}, nband={1}, i={2}'.format(complex_wfn,
                                                         diag_nband, i)

            etot = atoms.get_potential_energy()
            eref = etot_ref[complex_wfn]
            tester.check_float('Total energy (%s)' % tag, etot, eref, 1e-6)

            forces = atoms.get_forces()
            fmag = np.sqrt(np.sum(forces**2, axis=1))
            tester.check_array('Force magnitudes (%s)' % tag, fmag, 0., 1e-6)

            atoms.calc.reset()

        atoms.calc.server.protocol.end()

tester.print_summary()
