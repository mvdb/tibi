""" Tests with non-neutral molecules in non- and (Mulliken) self-consistent
tight-binding. Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase import Atoms
from ase.units import Bohr, Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

positions = np.array([
    [ 0.34847233,  0.88871489, -0.58972745],
    [-0.13788365,  0.15921433,  0.25266583],
    [-0.21058867, -1.14792922,  0.37859461],
    ])

atoms = Atoms('ONO', positions=positions, pbc=False)

calc = Tibi(basis_set={'O': 'sp', 'N': 'sp'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            ewald_tolerance=1e-9,
            include_repulsion=False,
            include_tightbinding=True,
            mixing_history=8,
            mixing_scheme='pulay',
            net_charge=1.,
            scf_maxiter=50,
            scf_must_converge=True,
            scf_tolerance=1e-3,
            smearing_method='none',
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/chno/',
            )

# Reference values for different combinations of "include_scf"
# and "aux_basis_mulliken"
etot_ref = {
    (False, 'atom_dependent'): -8.6081573088 * Hartree,
    (True, 'atom_dependent'): -8.3521101582 * Hartree,
    (True, 'subshell_dependent'): -8.3565371695 * Hartree,
}

forces_ref = {
    (False, 'atom_dependent'): np.array([
        [-0.147891407495, -0.171657061646,  0.256155437788],
        [ 0.155774273108, -0.022227620665, -0.269808963534],
        [-0.007882865613,  0.193884682311,  0.013653525746],
        ]) * Hartree / Bohr,
    (True, 'atom_dependent'): np.array([
        [-0.123963130316, -0.139082541067,  0.214710445262],
        [ 0.130566281653, -0.055625899746, -0.226147440823],
        [-0.006603151337,  0.194708440813,  0.011436995561],
        ]) * Hartree / Bohr,
    (True, 'subshell_dependent'): np.array([
        [-0.120901783803, -0.135777531765,  0.209408037470],
        [ 0.127396354507, -0.052274856408, -0.220656965796],
        [-0.006494570703,  0.188052388173,  0.011248928325],
        ]) * Hartree / Bohr,
}

keys = [
    (False, 'atom_dependent'),
    (True, 'atom_dependent'),
    (True, 'subshell_dependent'),
]

for key in keys:
    suffix = 'scf=%s, ldep=%s' % key

    calc.set(aux_basis_mulliken=key[1],
             include_scf=key[0])
    atoms.set_calculator(calc)

    etot = atoms.get_potential_energy()
    tester.check_float('Total (electronic) energy (%s)' % suffix,
                       etot, etot_ref[key], 5e-4)

    forces = atoms.get_forces()
    tester.check_array('Forces (%s)' % suffix, forces, forces_ref[key], 5e-3)

tester.print_summary()
