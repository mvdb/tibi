""" A basic single-point calculation test.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase import Atoms
from ase.units import Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = Atoms('Au2', pbc=False,
              positions=[[4.24707929324, 4.84322981472, 5.36094696310],
                         [3.75292070675, 3.15677018527, 7.13905303689]])

calc = Tibi(basis_set={'Au': 'spd'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_tightbinding=True,
            kpts=[1, 1, 1],
            kpts_gamma_shift=False,
            output_bands=True,
            output_hamiltonian=True,
            output_overlap=True,
            smearing_method='none',
            spin_polarized=False,
            tbpar_dir='../skf_files',
            )
atoms.set_calculator(calc)


eband = atoms.get_potential_energy()
tester.check_float('Band energy', eband, -5.5835084077 * Hartree, 1e-5)

bands_ref = [-8.241, -7.615, -7.615, -7.030, -7.030, -6.744, -6.744,
             -6.460, -6.210, -6.140, -6.140, -4.152, -0.853, -0.853,
             0.159, 1.029, 1.029, 6.761]
bands = calc.get_eigenvalues(kpt=0, spin=0)
tester.check_array('Bands', bands, bands_ref, 1e-3)

hamiltonian_ref = np.diagflat([[-0.21275600] + [-0.009343] * 3 \
                                + [-0.253157] * 5 ] * 2)
hamiltonian_ref[9:, :9] = np.array([
    [-1.003350E-01, -1.734641E-02, -5.919966E-02, 6.241672E-02, 1.324380E-02,
     -4.765450E-02, -1.396350E-02, -2.065884E-02, 1.484061E-02],
    [1.734641E-02, -3.090408E-02, 6.100994E-03, -6.432537E-03, 2.337353E-02,
     1.725338E-02, -2.464371E-02, 1.573335E-02, -1.013839E-02],
    [5.919966E-02, 6.100994E-03, -1.187035E-02, -2.195290E-02, -8.110317E-03,
     2.918295E-02, 1.725338E-02, -2.642297E-03, -3.460021E-02],
    [-6.241672E-02, -6.432537E-03, -2.195290E-02, -9.545885E-03, 1.725338E-02,
     -3.391350E-02, -9.937176E-03, -2.691333E-02, -1.496007E-02],
    [1.324380E-02, -2.337353E-02, 8.110317E-03, -1.725338E-02, 1.070227E-02,
     1.678278E-02, -1.812976E-02, 1.047813E-02, -1.561783E-02],
    [-4.765450E-02, -1.725338E-02, -2.918295E-02, 3.391350E-02, 1.678278E-02,
     -4.502223E-02, -1.847702E-02, -1.249091E-02, 1.252835E-02],
    [-1.396350E-02, 2.464371E-02, -1.725338E-02, 9.937176E-03, -1.812976E-02,
     -1.847702E-02, 1.262192E-02, -1.843505E-02, 3.670998E-03],
    [-2.065884E-02, -1.573335E-02, 2.642297E-03, 2.691333E-02, 1.047813E-02,
     -1.249091E-02, -1.843505E-02, 1.074794E-03, 2.436206E-02],
    [1.484061E-02, 1.013839E-02, 3.460021E-02, 1.496007E-02, -1.561783E-02,
     1.252835E-02, 3.670998E-03, 2.436206E-02, 2.711720E-02]])

hamiltonian = calc.get_hamiltonian_matrix(kpt=0, spin=0)
for iband in range(hamiltonian.shape[0]):
    tester.check_array('Hamiltonian %s' % iband, hamiltonian[iband, :],
                       hamiltonian_ref[iband, :], 1e-6)

overlap_ref = np.diagflat([1.] * 18)
overlap_ref[9:, :9] = np.array([
    [2.450181E-01, 6.436271E-02, 2.196564E-01, -2.315931E-01, -2.412880E-02,
     8.682145E-02, 2.544002E-02, 3.763821E-02, -2.703802E-02],
    [-6.436271E-02, 1.377175E-01, -7.486689E-02, 7.893535E-02, -5.324882E-02,
     -4.258367E-02, 5.614250E-02, -3.753099E-02, 2.427176E-02],
    [-2.196564E-01, -7.486689E-02, -9.585036E-02, 2.693898E-01, 2.131841E-02,
     -7.670895E-02, -4.258367E-02, 2.081335E-03, 8.283444E-02],
    [2.315931E-01, 7.893535E-02, 2.693898E-01, -1.243745E-01, -4.258367E-02,
     8.814330E-02, 2.582734E-02, 6.642573E-02, 3.151775E-02],
    [-2.412880E-02, 5.324882E-02, -2.131841E-02, 4.258367E-02, -1.614135E-02,
     -2.242885E-02, 2.747519E-02, -1.445427E-02, 2.233560E-02],
    [8.682145E-02, 4.258367E-02, 7.670895E-02, -8.814330E-02, -2.242885E-02,
     5.832999E-02, 2.480324E-02, 1.476511E-02, -1.585889E-02],
    [2.544002E-02, -5.614250E-02, 4.258367E-02, -2.582734E-02, 2.747519E-02,
     2.480324E-02, -1.905053E-02, 2.615310E-02, -4.646898E-03],
    [3.763821E-02, 3.753099E-02, -2.081335E-03, -6.642573E-02, -1.445427E-02,
     1.476511E-02, 2.615310E-02, -2.860538E-03, -3.484102E-02],
    [-2.703802E-02, -2.427176E-02, -8.283444E-02, -3.151775E-02, 2.233560E-02,
     -1.585889E-02, -4.646898E-03, -3.484102E-02, -4.055860E-02]])

overlap = calc.get_overlap_matrix(kpt=0, spin=0)
for iband in range(overlap.shape[0]):
    tester.check_array('Overlap %s' % iband, overlap[iband, :],
                       overlap_ref[iband, :], 1e-6)

tester.print_summary()
