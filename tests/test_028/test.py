""" Basic tests for models beyond the 1- and 2-center approximations,
this time with a periodic structure.
"""
import numpy as np
from ase.build import bulk
from ase.units import Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = bulk('C', 'diamond', a=3.6)

calc = Tibi(basis_set={'C': 'sp'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_scf=False,
            include_tightbinding=True,
            kpts=[3, 3, 3],
            kpts_gamma_shift=True,
            output_bands=False,
            output_hamiltonian=False,
            output_overlap=False,
            smearing_method='none',
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='./',
            )

# Regression values (in eV)
eband_ref = {
    (2, 2): -146.95080307072718,
    (2, 3): -129.1224226850348,
    (3, 2): -132.8275103119642,
    (3, 3): -116.04865648628622,
}

# Obtained with calc.calculate_numerical_stress(atoms, d=1e-4, voigt=True)
sband_ref = {
    (2, 2): [11.65675681]*3 + [0.]*3,
    (2, 3): [6.66975241]*3 + [0.]*3,
    (3, 2): [7.61039291]*3 + [0.]*3,
    (3, 3): [3.36677913]*3 + [0.]*3,
}

models = {
    (i, j): {'veff_expansion_onsite': i, 'veff_expansion_offsite': j}
    for i in [2, 3] for j in [2, 3]
}

etol, ftol, stol = 1e-6, 1e-8, 2e-4

for model, param in models.items():
    calc.reset()
    calc.set(**param)
    atoms.set_calculator(calc)

    eband = atoms.get_potential_energy()
    tester.check_array('Band energy %s' % str(model), eband, eband_ref[model],
                       etol)

    fband = atoms.get_forces()
    tester.check_array('Band forces %s' % str(model), fband, 0., ftol)

    sband = atoms.get_stress(voigt=True)
    tester.check_array('Band stress %s' % str(model), sband, sband_ref[model],
                       stol)

# Additional check with linear interpolation
model = (3, 3)
calc.set(veff_3c_eps_inner=1e-7,
         veff_3c_eps_outer=1e-7,
         veff_3c_interpolation=1,
         veff_3c_upsampling=(4, 6, 5),
         **models[model])
atoms.set_calculator(calc)

eband = atoms.get_potential_energy()
tester.check_array('Band energy %s (linear)' % str(model), eband,
                   eband_ref[model], 2e-2)

fband = atoms.get_forces()
tester.check_array('Band forces %s (linear)' % str(model), fband, 0., ftol)

sband = atoms.get_stress(voigt=True)
# Obtained with calc.calculate_numerical_stress(atoms, d=1e-4, voigt=True)
sband_ref = [3.37086177]*3 + [0.]*3
tester.check_array('Band stress1 %s (linear)' % str(model), sband, sband_ref,
                   2e-2)
# Obtained with calc.calculate_numerical_stress(atoms, d=1e-4, voigt=True)
# using the same linear interpolation scheme and veff_3c_eps* = 1e-14
sband_ref = [3.37272812]*3 + [0.]*3
tester.check_array('Band stress2 %s (linear)' % str(model), sband, sband_ref,
                   5e-2)

tester.print_summary()
