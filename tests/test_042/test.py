""" Sanity checks for the Eiger program.
"""
from collections import OrderedDict
from itertools import product
from ase import Atoms
from tibi import Eiger
from tibi.test import Tester

tester = Tester()

atoms = Atoms()

common_kwargs = dict(
    # Tibi parameters:
    diag_nband=66,
    # Eiger parameters:
    nbasis=89,
    niter=3,
    random_seed=42,
)

# We cannot expect different compilers to use the same RNG implementation,
# so we need to determine the reference energies on the fly
etot_ref = {}
for complex_wfn in [True, False]:
    calc = Eiger(complex_wfn=complex_wfn,
                 diag_algo='divide&conquer',
                 diag_gen2std='LAPACK',
                 **common_kwargs)
    atoms.set_calculator(calc)
    etot = atoms.get_potential_energy()

    lbound = 2 * common_kwargs['nbasis']
    ubound = lbound + 1
    assert lbound < etot < ubound, \
           f'Reference energy {etot} lies outside of ]{lbound}, {ubound}['

    etot_ref[complex_wfn] = etot


model_space = OrderedDict(
    # Tibi parameters:
    complex_wfn=[True, False],
    diag_algo=[tester.get_diag_algo()],
    diag_gen2std=['LAPACK', 'eigensolver'],
    diag_parallel_ks=[True, False],
    kpts=[(1, 1, 1), (2, 1, 1)],
    spin_polarized=[True, False],
    # Eiger parameters:
    decompose_overlap=[True, False],
)


def generate(space):
    # Yields all possible combinations from an (ordered) dict
    # with iterable values
    keys = space.keys()
    for vals in product(*space.values()):
        yield OrderedDict(zip(keys, vals))


for model_kwargs in generate(model_space):
    calc = Eiger(**common_kwargs, **model_kwargs)
    atoms.set_calculator(calc)
    etot = atoms.get_potential_energy()

    tag = ', '.join([f'{key}={val}' for key, val in model_kwargs.items()])
    eref = etot_ref[model_kwargs['complex_wfn']]
    tester.check_float(f'Total energy ({tag})', etot, eref, 1e-6)

tester.print_summary()
