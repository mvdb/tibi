""" Tests for (Mulliken) self-consistent tight-binding
in a periodic structure with k-points, f-electrons and spin polarization.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase import Atoms
from ase.units import Bohr, Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

scaled_positions = [(0.65, 0.35,  0.75), (0.35, 0.65,  0.25),
                    (0.65, 0.35,  0.35), (0.35, 0.65,  0.65), (0., 0., 0.)]
atoms = Atoms('Ce2O3', cell=(3.83, 3.83, 6.07, 90., 90., 120.), pbc=True,
              scaled_positions=scaled_positions)

scf_wkernel_on1c = {
    'Ce': [-0.23, -0.17, -0.14, -0.02, -0.17, -0.18,
           -0.08, -0.01, -0.14, -0.08, -0.21, -0.06,
           -0.02, -0.01, -0.06, -0.31],
    'O': [-0.88, -0.77, -0.77, -0.08],
}

calc = Tibi(aux_basis_mulliken='subshell_dependent',
            basis_set={'O': 'sp', 'Ce': 'spdf'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            ewald_tolerance=1e-6,
            include_repulsion=False,
            include_scf=True,
            include_tightbinding=True,
            kpts=[3, 3, 2],
            kpts_gamma_shift=False,
            mixing_fraction=0.4,
            mixing_history=8,
            mixing_scheme='pulay',
            output_mulliken_subshell_chg=True,
            output_mulliken_subshell_mag=True,
            scf_initial_guess='restart',
            scf_maxiter=50,
            scf_must_converge=True,
            scf_tolerance=1e-3,
            scf_wkernel_on1c=scf_wkernel_on1c,
            smearing_method='fermi-dirac',
            smearing_width=0.1,
            spin_polarized=True,
            task='energy_forces',
            tbpar_dir='../skf_files/ceo/',
            )
atoms.set_calculator(calc)

with open('scf_restart.in', 'w') as f:
    # Aiming for anti-ferromagnetic ordering
    for i in range(2):
        f.write('0.0 0.0 0.0 0.0 0.0 0.0 0.0 %.2f\n' % (-2. if i == 0 else 2.))
    for i in range(3):
        f.write('0.0 0.0 0.0 0.0\n')

etot_ref = -12.5091770422 * Hartree

charges_ref = [[1.6894841, -0.52617567, -0.17358386, -0.33796338],
               [1.6894841, -0.52617566, -0.17358386, -0.33796319],
               [0.16784613, -0.55917205],
               [0.16784613, -0.55917203],
               [0.11277344, -0.63364424]]

mag_ref = [[-0.0018491, -0.00246981, -0.01621282, -0.97515164],
           [0.0018491, 0.0024698, 0.01621282, 0.97515147],
           [-0.00099283, -0.01902857],
           [0.00099283, 0.01902857],
           [0., 0.]]

forces_ref = Hartree / Bohr * np.array([
        [-0.059153978903,  0.034152570999, -0.058582757471],
        [ 0.059153980384, -0.034152572293,  0.058582759249],
        [-0.049939817686,  0.028832770407, -0.019099619705],
        [ 0.049939816335, -0.028832769520,  0.019099611608],
        [-0.000000000131,  0.000000000407,  0.000000006319],
        ])

stress_ref = Hartree / Bohr**3 * np.array([
        [ 0.002578697705, -0.000446237313,  0.000241089829],
        [-0.000446237329,  0.002063427244, -0.000139193277],
        [ 0.000241089830, -0.000139193268,  0.001554740956],
])

etot = atoms.get_potential_energy()
tester.check_float('Total (electronic) energy', etot, etot_ref, 5e-3)

charges = calc.get_mulliken_subshell_chg()
mag = calc.get_mulliken_subshell_mag()
forces = atoms.get_forces()
for i in range(len(atoms)):
    tester.check_array('Subshell charges on atom %d' % i,
                       charges[i], charges_ref[i], 5e-4)
    tester.check_array('Subshell magnetizations on atom %d' % i,
                       mag[i], mag_ref[i], 5e-4)
    tester.check_array('Force on atom %d' % i,
                       forces[i, :], forces_ref[i, :], 5e-3)

stress = atoms.get_stress(voigt=False)
tester.check_array('Stress tensor', stress, stress_ref, 2e-3)

tester.print_summary()
