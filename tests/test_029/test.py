""" Basic tests for models beyond the 1- and 2-center approximations to the
on- and off-site Hamiltonian integrals, respectively (with an sp basis set),
here with 3 different elements.
"""
import os
import numpy as np
from ase import Atoms
from ase.units import Hartree
from ase.calculators.socketio import SocketIOCalculator
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = Atoms('OCH', pbc=False,
              positions=[[5.813, 6.235, 6.044],
                         [6.856, 6.927, 6.692],
                         [6.323, 8.008, 6.573]])

unixsocket = 'ase_tibi_test029'
tbpar_dir = './'

calc = Tibi(basis_set={'O': 'sp', 'H': 's', 'C': 'sp'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_scf=False,
            include_tightbinding=True,
            kpts=[1, 1, 1],
            kpts_gamma_shift=False,
            output_bands=True,
            output_hamiltonian=True,
            output_overlap=False,
            smearing_method='fermi-dirac',
            smearing_width=0.001,
            spin_polarized=False,
            tbpar_dir=tbpar_dir,
            )

# Reference values for each (veff_expansion_onsite, veff_expansion_offsite)
ham_ref = {
    (2, 2): np.array([
    [-8.884822e-01, 5.928429e-02, 4.301092e-02, 3.737607e-02, 0.000000e+00,
     0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [5.928429e-02, -3.791048e-01, -3.285107e-02, -2.954962e-02, 0.000000e+00,
     0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [4.301092e-02, -3.285107e-02, -3.590501e-01, -2.119986e-02, 0.000000e+00,
     0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [3.737607e-02, -2.954962e-02, -2.119986e-02, -3.505033e-01, 0.000000e+00,
     0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [-3.445271e-01, 2.203676e-01, 1.462039e-01, 1.369101e-01, -5.876685e-01,
     -1.044816e-01, 6.394401e-03, -5.371620e-02, 0.000000e+00],
    [-3.374286e-01, 6.711997e-02, 1.409801e-01, 1.320135e-01, -1.044816e-01,
     -3.084381e-01, -1.980030e-02, -5.015585e-02, 0.000000e+00],
    [-2.238830e-01, 1.409865e-01, -5.182057e-02, 8.759312e-02, 6.394401e-03,
     -1.980030e-02, -3.126136e-01, -2.432591e-02, 0.000000e+00],
    [-2.096490e-01, 1.320153e-01, 8.759037e-02, -6.334505e-02, -5.371620e-02,
     -5.015585e-02, -2.432591e-02, -2.480030e-01, 0.000000e+00],
    [1.421672e-01, -2.868022e-02, -9.970183e-02, -2.975503e-02, 3.585655e-01,
     1.197508e-01, -2.428618e-01, 2.673508e-02, -3.994729e-01],
    ]),
    (2, 3): np.array([
    [-8.884822e-01, 5.928429e-02, 4.301092e-02, 3.737607e-02, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [5.928429e-02, -3.791048e-01, -3.285107e-02, -2.954962e-02, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [4.301092e-02, -3.285107e-02, -3.590501e-01, -2.119986e-02, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [3.737607e-02, -2.954962e-02, -2.119986e-02, -3.505033e-01, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [-3.491061e-01, 2.250037e-01, 1.571846e-01, 1.409593e-01, -5.876685e-01,
    -1.044816e-01, 6.394401e-03, -5.371620e-02, 0.000000e+00],
    [-3.424084e-01, 7.007402e-02, 1.518751e-01, 1.364197e-01, -1.044816e-01,
    -3.084381e-01, -1.980030e-02, -5.015585e-02, 0.000000e+00],
    [-2.212799e-01, 1.375331e-01, -6.236308e-02, 8.458068e-02, 6.394401e-03,
    -1.980030e-02, -3.126136e-01, -2.432591e-02, 0.000000e+00],
    [-2.118694e-01, 1.342998e-01, 9.208464e-02, -6.387423e-02, -5.371620e-02,
    -5.015585e-02, -2.432591e-02, -2.480030e-01, 0.000000e+00],
    [1.687605e-01, -6.060485e-02, -1.383729e-01, -5.217549e-02, 3.852383e-01,
    1.539961e-01, -2.302787e-01, 4.651219e-02, -3.994729e-01],
    ]),
    (3, 2): np.array([
    [-8.880728e-01, 5.890887e-02, 4.228692e-02, 3.707263e-02, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [5.890887e-02, -3.784550e-01, -3.220506e-02, -2.929970e-02, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [4.228692e-02, -3.220506e-02, -3.574487e-01, -2.067762e-02, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [3.707263e-02, -2.929970e-02, -2.067762e-02, -3.499406e-01, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [-3.445271e-01, 2.203676e-01, 1.462039e-01, 1.369101e-01, -5.870872e-01,
    -1.037146e-01, 6.252827e-03, -5.333583e-02, 0.000000e+00],
    [-3.374286e-01, 6.711997e-02, 1.409801e-01, 1.320135e-01, -1.037146e-01,
    -3.072316e-01, -1.999224e-02, -4.972471e-02, 0.000000e+00],
    [-2.238830e-01, 1.409865e-01, -5.182057e-02, 8.759312e-02, 6.252827e-03,
    -1.999224e-02, -3.122869e-01, -2.442661e-02, 0.000000e+00],
    [-2.096490e-01, 1.320153e-01, 8.759037e-02, -6.334505e-02, -5.333583e-02,
    -4.972471e-02, -2.442661e-02, -2.474640e-01, 0.000000e+00],
    [1.421672e-01, -2.868022e-02, -9.970183e-02, -2.975503e-02, 3.585655e-01,
    1.197508e-01, -2.428618e-01, 2.673508e-02, -3.931298e-01],
    ]),
    (3, 3): np.array([
    [-8.880728e-01, 5.890887e-02, 4.228692e-02, 3.707263e-02, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [5.890887e-02, -3.784550e-01, -3.220506e-02, -2.929970e-02, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [4.228692e-02, -3.220506e-02, -3.574487e-01, -2.067762e-02, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [3.707263e-02, -2.929970e-02, -2.067762e-02, -3.499406e-01, 0.000000e+00,
    0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00],
    [-3.491061e-01, 2.250037e-01, 1.571846e-01, 1.409593e-01, -5.870872e-01,
    -1.037146e-01, 6.252827e-03, -5.333583e-02, 0.000000e+00],
    [-3.424084e-01, 7.007402e-02, 1.518751e-01, 1.364197e-01, -1.037146e-01,
    -3.072316e-01, -1.999224e-02, -4.972471e-02, 0.000000e+00],
    [-2.212799e-01, 1.375331e-01, -6.236308e-02, 8.458068e-02, 6.252827e-03,
    -1.999224e-02, -3.122869e-01, -2.442661e-02, 0.000000e+00],
    [-2.118694e-01, 1.342998e-01, 9.208464e-02, -6.387423e-02, -5.333583e-02,
    -4.972471e-02, -2.442661e-02, -2.474640e-01, 0.000000e+00],
    [1.687605e-01, -6.060485e-02, -1.383729e-01, -5.217549e-02, 3.852383e-01,
    1.539961e-01, -2.302787e-01, 4.651219e-02, -3.931298e-01],
    ]),
}
eigval_ref = {
    (2, 2): [-9.378994e-01, -5.473912e-01, -4.416552e-01, -3.628965e-01,
             -3.589379e-01, -2.279711e-01, -1.482299e-01, 4.918669e-02,
             1.343943e-01],
    (2, 3): [-9.401822e-01, -5.606441e-01, -4.155852e-01, -3.644683e-01,
             -3.524575e-01, -2.252395e-01, -1.455753e-01, 8.077307e-02,
             1.747402e-01],
    (3, 2): [-9.372673e-01, -5.467408e-01, -4.398240e-01, -3.625959e-01,
             -3.578720e-01, -2.268066e-01, -1.478140e-01, 5.964106e-02,
             1.396641e-01],
    (3, 3): [-9.396003e-01, -5.599001e-01, -4.146814e-01, -3.641684e-01,
             -3.513522e-01, -2.238147e-01, -1.451587e-01, 8.940791e-02,
             1.824139e-01],
}

models = {
    (i, j): {'veff_expansion_onsite': i, 'veff_expansion_offsite': j}
    for i in [2, 3] for j in [2, 3]
}

htol, etol, ftol = 5e-4, 1e-3, 1e-3

for model, param in models.items():
    calc.reset()
    calc.set(task='energy_forces', **param)
    atoms.set_calculator(calc)
    e = atoms.get_potential_energy()

    ham = calc.get_hamiltonian_matrix(kpt=0, spin=0)
    tester.check_array('Hamiltonian %s' % str(model), ham, ham_ref[model],
                       htol)

    eigval = calc.get_eigenvalues(kpt=0, spin=0)
    tester.check_array('Eigenvalues %s' % str(model), eigval / Hartree,
                       eigval_ref[model], etol)

    # Use socket to speed up the numerical force calculation,
    # as this avoids re-reading the parameter files
    calc.set(socket_type='unix',
             socket_unix_suffix=unixsocket,
             task='socket')

    with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:
        atoms.calc = socketcalc

        fband = atoms.get_forces()
        fband_ref = calc.calculate_numerical_forces(atoms, d=5e-4)
        tester.check_array('Band forces %s' % str(model), fband, fband_ref,
                           ftol)

        atoms.calc.server.protocol.end()

# Final check with binary IO
assert not any([f.endswith('.3cb') for f in os.listdir(tbpar_dir)])
calc.reset()
calc.set(task='energy', veff_3c_binary_io=True, **models[(3, 3)])
atoms.set_calculator(calc)
e_ref = atoms.get_potential_energy()

calc.reset()
atoms.set_calculator(calc)
e = atoms.get_potential_energy()
tester.check_float('Total energy (binary IO)', e, e_ref, 1e-8)
os.system('rm {0}/*.3cb'.format(tbpar_dir))

tester.print_summary()
