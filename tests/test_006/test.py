""" A basic force calculation test.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase.build import bulk
from ase.units import Hartree, Bohr
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = bulk('Si', 'diamond', a=5.43)
atoms.set_pbc(False)

calc = Tibi(basis_set={'Si': 'sp'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=True,
            include_tightbinding=False,
            kpts=[1, 1, 1],
            kpts_gamma_shift=False,
            output_bands=False,
            output_hamiltonian=False,
            output_overlap=False,
            repulsion_2c_form='spline',
            smearing_method='none',
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/',
            )
atoms.set_calculator(calc)

# First, the repulsive terms
erep = atoms.get_potential_energy()
tester.check_float('Repulsive energy', erep, -0.0254648597 * Hartree, 1e-5)

frep = atoms.get_forces()
frep_ref = np.array([[0.012505232276, 0.012505232276, 0.012505232276],
                     [-0.012505232276, -0.012505232276, -0.012505232276]])
tester.check_array('Repulsive forces', frep, frep_ref * Hartree / Bohr, 1e-5)

# Next, the band terms
calc.set(include_repulsion=False,
         include_tightbinding=True)

eband = atoms.get_potential_energy()
tester.check_float('Band energy', eband, -2.3346740342 * Hartree, 1e-4)

fband = atoms.get_forces()
fband_ref =  np.array([[0.001068868101, 0.001068868101, 0.001068868101],
                      [-0.001068868101, -0.001068868101, -0.001068868101]])
tester.check_array('Band forces', fband, fband_ref * Hartree / Bohr, 5e-3)

tester.print_summary()
