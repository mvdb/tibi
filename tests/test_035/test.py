""" Tests for more advanced treatments of (Mulliken) self-consistency
(nonperiodic structure).
"""
from itertools import permutations
import numpy as np
from ase import Atoms
from ase.calculators.socketio import SocketIOCalculator
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

unixsocket = 'ase_tibi_test035'

scf_wkernel_on1c = {
    'H': [-1.883, -1.642, -2.481, -1.642, -2.025, -1.716, -2.481,
          -1.716, -3.847],
    'C': [-0.763, -0.717, -0.607, -0.908, -0.877, -0.717, -0.699,
          -0.556, -0.849, -0.852, -0.607, -0.556, -0.762, -0.548,
          -0.529, -0.908, -0.849, -0.548, -1.206, -1.151, -0.877,
          -0.852, -0.529, -1.151, -1.155],
}

calc = Tibi(aux_basis_mulliken='subshell_dependent',
            basis_set={'H': 'sp_s', 'C': 'spd_sp'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_scf=True,
            include_tightbinding=True,
            kpts=(1, 1, 1),
            output_mulliken_subshell_chg=True,
            output_mulliken_subshell_mag=True,
            scf_initial_guess='restart',
            scf_maxiter=30,
            scf_must_converge=True,
            scf_tolerance=1e-3,
            scf_ukernel_expansion_offsite=2,
            scf_ukernel_expansion_onsite=2,
            scf_ukernel_off2c_model='tabulated',
            scf_wkernel_expansion_offsite=2,
            scf_wkernel_expansion_onsite=2,
            scf_wkernel_on1c=scf_wkernel_on1c,
            scf_write_verbose=True,
            smearing_method='fermi-dirac',
            smearing_width=0.001,
            socket_type='unix',
            socket_unix_suffix=unixsocket,
            spin_fix_nupdown=True,
            spin_nupdown=2,
            spin_polarized=True,
            task='socket',
            tbpar_dir='../skf_files/ch_dzp',
            veff_expansion_offsite=2,
            veff_expansion_onsite=1,
            )

positions = np.array([
    [6.50242848, 7.50829635, 5.47009356],
    [5.96283680, 7.19450780, 6.34808337],
    [5.53473472, 6.24506185, 6.62334607],
    ])

atoms = Atoms('HCH', positions=positions, pbc=False)
N = len(atoms)

e_ref = -53.08209498578383  # regression value
k_ref = {  # regression values (confirmed by preliminary BeckeHarris runs)
    'U': np.array([
            [5.780190e-01, 5.459703e-01, 6.562971e-01, -7.002866e-02,
             -7.784807e-02, -8.984212e-02, -4.968289e-02, -5.253891e-02,
             -4.285396e-03, -3.653809e-03, -1.219565e-03],
            [5.459703e-01, 5.264041e-01, 6.083511e-01, -7.128632e-02,
             -7.887076e-02, -9.288595e-02, -4.987277e-02, -5.257678e-02,
             -3.654124e-03, -2.722835e-03, -6.343843e-04],
            [6.562971e-01, 6.083511e-01, 7.780388e-01, -4.576693e-02,
             -5.439939e-02, -6.564055e-02, -2.468366e-02, -2.799339e-02,
             -1.219913e-03, -6.344524e-04, -1.031414e-04],
            [-7.003063e-02, -7.128920e-02, -4.576411e-02, 5.680082e-01,
             5.540776e-01, 5.069741e-01, 6.237866e-01, 6.202872e-01,
             -7.003063e-02, -7.128920e-02, -4.576411e-02],
            [-7.784950e-02, -7.887307e-02, -5.439609e-02, 5.540776e-01,
             5.411692e-01, 4.963918e-01, 6.061712e-01, 6.030241e-01,
             -7.784950e-02, -7.887307e-02, -5.439609e-02],
            [-8.984222e-02, -9.288711e-02, -6.563629e-02, 5.069741e-01,
             4.963918e-01, 4.617810e-01, 5.471155e-01, 5.448565e-01,
             -8.984222e-02, -9.288710e-02, -6.563629e-02],
            [-4.968664e-02, -4.987736e-02, -2.468227e-02, 6.237866e-01,
             6.061712e-01, 5.471155e-01, 6.962249e-01, 6.914034e-01,
             -4.968664e-02, -4.987736e-02, -2.468227e-02],
            [-5.254237e-02, -5.258111e-02, -2.799164e-02, 6.202872e-01,
             6.030241e-01, 5.448565e-01, 6.914034e-01, 6.868704e-01,
             -5.254237e-02, -5.258110e-02, -2.799164e-02],
            [-4.285396e-03, -3.653809e-03, -1.219565e-03, -7.002866e-02,
             -7.784807e-02, -8.984212e-02, -4.968289e-02, -5.253891e-02,
             5.780190e-01, 5.459703e-01, 6.562971e-01],
            [-3.654124e-03, -2.722835e-03, -6.343843e-04, -7.128632e-02,
             -7.887076e-02, -9.288595e-02, -4.987277e-02, -5.257678e-02,
             5.459703e-01, 5.264041e-01, 6.083511e-01],
            [-1.219913e-03, -6.344524e-04, -1.031414e-04, -4.576693e-02,
             -5.439939e-02, -6.564055e-02, -2.468366e-02, -2.799339e-02,
             6.562971e-01, 6.083511e-01, 7.780388e-01],
    ]),
    'W': np.array([
            [-5.367800e-02, -4.385879e-02, -7.446191e-02, -8.984892e-03,
             -8.899913e-03, -1.103957e-02, -7.621683e-03, -7.741286e-03,
             -1.657946e-03, -1.559328e-03, -5.575918e-04],
            [-4.385879e-02, -5.263508e-02, -4.793746e-02, -9.863478e-03,
             -9.799998e-03, -1.126118e-02, -8.881479e-03, -8.915346e-03,
             -1.559328e-03, -1.329955e-03, -3.308886e-04],
            [-7.446191e-02, -4.793746e-02, -1.183210e-01, -8.730735e-03,
             -8.394253e-03, -1.165388e-02, -7.029673e-03, -7.157068e-03,
             -5.575918e-04, -3.308886e-04, -5.590031e-05],
            [-8.984892e-03, -9.863478e-03, -8.730735e-03, -2.562238e-02,
             -2.406719e-02, -1.950583e-02, -3.110409e-02, -3.001067e-02,
             -8.984892e-03, -9.863478e-03, -8.730735e-03],
            [-8.899913e-03, -9.799998e-03, -8.394253e-03, -2.406719e-02,
             -2.342908e-02, -1.774795e-02, -2.917597e-02, -2.925335e-02,
             -8.899913e-03, -9.799998e-03, -8.394253e-03],
            [-1.103957e-02, -1.126118e-02, -1.165388e-02, -1.950583e-02,
             -1.774795e-02, -2.379952e-02, -1.817474e-02, -1.735551e-02,
             -1.103957e-02, -1.126118e-02, -1.165388e-02],
            [-7.621683e-03, -8.881479e-03, -7.029673e-03, -3.110409e-02,
             -2.917597e-02, -1.817474e-02, -4.164765e-02, -3.983062e-02,
             -7.621683e-03, -8.881478e-03, -7.029673e-03],
            [-7.741286e-03, -8.915346e-03, -7.157068e-03, -3.001067e-02,
             -2.925335e-02, -1.735551e-02, -3.983062e-02, -4.004376e-02,
             -7.741286e-03, -8.915346e-03, -7.157068e-03],
            [-1.657946e-03, -1.559328e-03, -5.575918e-04, -8.984892e-03,
             -8.899913e-03, -1.103957e-02, -7.621683e-03, -7.741286e-03,
             -5.367800e-02, -4.385879e-02, -7.446191e-02],
            [-1.559328e-03, -1.329955e-03, -3.308886e-04, -9.863478e-03,
             -9.799998e-03, -1.126118e-02, -8.881478e-03, -8.915346e-03,
             -4.385879e-02, -5.263508e-02, -4.793746e-02],
            [-5.575918e-04, -3.308886e-04, -5.590031e-05, -8.730735e-03,
             -8.394253e-03, -1.165388e-02, -7.029673e-03, -7.157068e-03,
             -7.446191e-02, -4.793746e-02, -1.183210e-01],
    ]),
}


def write_restart_file(atoms):
    with open('scf_restart.in', 'w') as f:
        for atom in atoms:
            if atom.symbol == 'H':
                f.write('0 0 0    0 0 0\n')
            elif atom.symbol == 'C':
                f.write('0 0 0 0 0    1 1 0 0 0\n')
    return


e_tol, f_tol, k_tol = 1e-5, 5e-4, 1e-6

for i, indices in enumerate(permutations(range(N))):
    a = atoms[indices]
    write_restart_file(a)

    with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:
        a.calc = socketcalc

        e = a.get_potential_energy()
        if i == 0:
            k = np.loadtxt('scf_kernel_U.dat')[:, 1:]
            tag = 'scf_kernel_U (permutation %s)' % list(indices)
            tester.check_array(tag, k, k_ref['U'], k_tol)

            k = np.loadtxt('scf_kernel_W.dat')[:, 1:]
            tag = 'scf_kernel_W (permutation %s)' % list(indices)
            tester.check_array(tag, k, k_ref['W'], k_tol)

        tag = 'Total energy (permutation %s)' % list(indices)
        tester.check_array(tag, e, e_ref, e_tol)

        f = a.get_forces()
        f_ref = calc.calculate_numerical_forces(a, d=1e-4)
        tag = 'Forces (permutation %s)' % list(indices)
        tester.check_array(tag, f, f_ref, f_tol)

        a.calc.server.protocol.end()


# Verify the 'tabulated' option for scf_ukernel_on1c_model and scf_wkernel_on1c
# (the integrals in the 1cl files are chosen so that the results should
# be the same).
e_tol, f_tol = 1e-9, 1e-9
calc.set(task='energy_forces')
atoms.set_calculator(calc)
write_restart_file(atoms)

for subshell_dependent in [True, False]:
    if subshell_dependent:
        kwargs = dict(
            aux_basis_mulliken='subshell_dependent',
            scf_ukernel_expansion_onsite=2,
            scf_ukernel_off2c_model='tabulated',
            scf_ukernel_on1c_model='elstner',
            scf_wkernel_expansion_offsite=2,
            scf_wkernel_expansion_onsite=2,
            scf_wkernel_on1c=scf_wkernel_on1c,
        )
    else:
        kwargs = dict(
            aux_basis_mulliken='atom_dependent',
            scf_ukernel_expansion_onsite=1,
            scf_ukernel_off2c_model='elstner',
            scf_ukernel_on1c_model='elstner',
            scf_wkernel_expansion_offsite=0,
            scf_wkernel_expansion_onsite=1,
            scf_wkernel_on1c={
                key: val[0] for key, val in scf_wkernel_on1c.items()
            },
        )
    calc.set(**kwargs)
    calc.reset()

    e_ref = atoms.get_potential_energy()
    f_ref = atoms.get_forces()

    calc.set(scf_ukernel_on1c_model='tabulated',
             scf_wkernel_on1c='tabulated')
    calc.reset()

    e = atoms.get_potential_energy()
    tester.check_array('Total energy (1cl, %s)' % subshell_dependent,
                       e, e_ref, e_tol)

    f = atoms.get_forces()
    tester.check_array('Forces (1cl, %s)' % subshell_dependent,
                       f, f_ref, f_tol)

tester.print_summary()
