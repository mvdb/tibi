""" Tests for (Mulliken) spin-polarized tight-binding (nonperiodic structure).
Reference values have been obtained with DFTB+ v19.1.
"""
from itertools import product
import numpy as np
from ase import Atoms
from ase.io import read
from ase.units import Bohr, Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

positions = np.array([
    [ 0.34847233,  0.88871489, -0.58972745],
    [-0.13788365,  0.15921433,  0.25266583],
    [-0.21058867, -1.14792922,  0.37859461],
    ])

atoms = Atoms('ONO', positions=positions, pbc=False)

calc = Tibi(basis_set={'O': 'sp', 'N': 'sp'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            ewald_tolerance=1e-9,
            include_repulsion=False,
            include_tightbinding=True,
            mixing_history=8,
            mixing_scheme='pulay',
            output_mulliken_orbital_mag=True,
            output_mulliken_subshell_chg=True,
            output_mulliken_subshell_mag=True,
            scf_tolerance=1e-3,
            smearing_method='fermi-dirac',
            smearing_width=0.01,
            spin_fix_nupdown=True,
            spin_polarized=True,
            task='energy_forces',
            tbpar_dir='../skf_files/chno/',
            )

# Reference values for different combinations of "include_scf",
# "aux_basis_mulliken" and "net_charge"
etot_ref = {
    (False, 'atom_dependent', 0): -8.7991681679 * Hartree,
    (True, 'atom_dependent', 0): -8.7577535176 * Hartree,
    (True, 'subshell_dependent', 0): -8.7589607862 * Hartree,
    (False, 'atom_dependent', 1): -8.6081573088 * Hartree,
    (True, 'atom_dependent', 1): -8.3521101582 * Hartree,
    (True, 'subshell_dependent', 1): -8.3565371695 * Hartree,
}

forces_ref = {
    (False, 'atom_dependent', 0): np.array([
        [-0.078967398577, -0.140117167114,  0.136775549836],
        [ 0.065705054997,  0.046647321013, -0.113804497216],
        [ 0.013262343580,  0.093469846101, -0.022971052620],
        ]) * Hartree / Bohr,
    (True, 'atom_dependent', 0): np.array([
        [-0.069485472061, -0.119181852086,  0.120352371124],
        [ 0.057400164092,  0.001966835544, -0.099420004234],
        [ 0.012085307969,  0.117215016542, -0.020932366890],
        ]) * Hartree / Bohr,
    (True, 'subshell_dependent', 0): np.array([
        [-0.069433530675, -0.120053315380,  0.120262405996],
        [ 0.056972211178,  0.002428867594, -0.098678768024],
        [ 0.012461319497,  0.117624447785, -0.021583637972],
        ]) * Hartree / Bohr,
    (False, 'atom_dependent', 1): np.array([
        [-0.147891407495, -0.171657061646,  0.256155437788],
        [ 0.155774273108, -0.022227620665, -0.269808963534],
        [-0.007882865613,  0.193884682311,  0.013653525746],
        ]) * Hartree / Bohr,
    (True, 'atom_dependent', 1): np.array([
        [-0.123963130316, -0.139082541067,  0.214710445262],
        [ 0.130566281653, -0.055625899746, -0.226147440823],
        [-0.006603151337,  0.194708440813,  0.011436995561],
        ]) * Hartree / Bohr,
    (True, 'subshell_dependent', 1): np.array([
        [-0.120901783803, -0.135777531765,  0.209408037470],
        [ 0.127396354507, -0.052274856408, -0.220656965796],
        [-0.006494570703,  0.188052388173,  0.011248928325],
        ]) * Hartree / Bohr,
}

mag_ref = {
    (True, 'atom_dependent', 0): np.array([
        [0.00046876, 0.05806962, 0.08031728, 0.17584779],
        [0.03023483, 0.05196806, 0.07286552, 0.17117129],
        [0.00100899, 0.06323582, 0.12201058, 0.17280146],
        ]),
    (True, 'subshell_dependent', 0): np.array([
        [0.00003609, 0.05664445, 0.07421944, 0.17413174],
        [0.03469571, 0.06085480, 0.07855941, 0.18318309],
        [0.00051656, 0.05502547, 0.12187391, 0.16025934],
        ]),
    (False, 'atom_dependent', 1): np.zeros((3, 4)),
    (True, 'atom_dependent', 1): np.zeros((3, 4)),
    (True, 'subshell_dependent', 1): np.zeros((3, 4)),
}

for key in product([False, True], ['atom_dependent', 'subshell_dependent'], [1, 0]):
    include_scf, ldep, charge = key
    suffix = 'include_scf=%s, ldep=%s, charge=%d' % key
    if key not in etot_ref:
        continue

    spin_nupdown = {0: 1, 1: 0}[charge]
    scf_wkernel_on1c = {
        (False, 'atom_dependent'): {},
        (True, 'atom_dependent'): {
            'N': -0.82, 'O': -0.95,
        },
        (True, 'subshell_dependent'): {
            'N': [-0.82, -0.66, -0.66, -0.60],
            'O': [-0.95, -0.72, -0.72, -0.68],
        },
    }
    calc.set(aux_basis_mulliken=ldep,
             include_scf=include_scf,
             net_charge=charge,
             scf_initial_guess='zero',
             scf_maxiter=50,
             scf_must_converge=True,
             scf_wkernel_on1c=scf_wkernel_on1c[(include_scf, ldep)],
             scf_write_restart_file=True,
             spin_nupdown=spin_nupdown)
    atoms.set_calculator(calc)

    etot = atoms.get_potential_energy()
    tester.check_float('Total (electronic) energy (%s)' % suffix,
                       etot, etot_ref[key], 5e-4)

    forces = atoms.get_forces()
    tester.check_array('Forces (%s)' % suffix, forces, forces_ref[key], 5e-3)

    if include_scf:
        mag_tot = calc.get_magnetization()
        tester.check_float('Total magnetization (%s)' % suffix,
                            mag_tot, spin_nupdown, 1e-6)

        mag = calc.get_mulliken_orbital_mag()
        tester.check_array('Magnetizations (%s)' % suffix, mag, mag_ref[key],
                           1e-3)

        mag = calc.get_mulliken_atom_mag()
        tester.check_float('Sum of atom magnetizations (%s)' % suffix,
                           sum(mag), mag_tot, 2e-4)

        mag = calc.get_mulliken_subshell_mag()
        tester.check_float('Sum of subshell magnetizations (%s)' % suffix,
                           sum([sum(x) for x in mag]), mag_tot, 2e-4)

        # Check that restarting works
        calc.set(scf_initial_guess='restart',
                 scf_maxiter=1,
                 scf_must_converge=False,
                 scf_restart_input_file='scf_restart.out')
        atoms.set_calculator(calc)

        etot = atoms.get_potential_energy()
        tester.check_float('Total (electronic) energy (%s, restart)' % suffix,
                           etot, etot_ref[key], 5e-4)

# Check that relaxation works
trajectory = 'relax.xyz'
calc.set(include_repulsion=True,
         relax_algo='lbfgs',
         relax_fmax=5e-2,
         relax_maxiter=10,
         relax_maxstep=0.2,
         relax_must_converge=True,
         relax_trajectory_append=False,
         relax_trajectory_file=trajectory,
         repulsion_2c_form='spline',
         scf_extrapolation='use_previous',
         scf_initial_guess='zero',
         scf_maxiter=50,
         scf_must_converge=True,
         task='relaxation')
atoms.set_calculator(calc)
atoms.get_potential_energy()
atoms = read(trajectory)
dr = atoms.get_distance(0, 1) - atoms.get_distance(1, 2)
tester.check_float('N-O bond length difference (%s)' % suffix, dr, 0., 1e-3)

tester.print_summary()
