""" Mainly for performance and parallelization testing.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase.build import bulk
from ase.units import Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = bulk('Au', 'fcc', a=4.0, orthorhombic=True).repeat((3, 3, 3))

calc = Tibi(basis_set={'Au': 'spd'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_tightbinding=True,
            kpts=[2, 2, 2],
            kpts_gamma_shift=False,
            output_bands=False,
            output_hamiltonian=False,
            output_overlap=False,
            smearing_method='fermi-dirac',
            smearing_width=0.1,
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/',
            )
atoms.set_calculator(calc)

eband = atoms.get_potential_energy()
tester.check_float('Band energy', eband, -148.9355594594 * Hartree, 1e-2)

fband = atoms.get_forces()
tester.check_float('Max band force', np.max(np.abs(fband)), 0., 1e-5)

tester.print_summary()
