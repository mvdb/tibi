""" Tests for Giese-York self-consistency (periodic structure, d-metal). """
import numpy as np
import os
from ase.build import bulk
from ase.calculators.socketio import SocketIOCalculator
from ase.units import Bohr
from tibi import Tibi
from tibi.test import Tester


tester = Tester()

unixsocket = 'ase_tibi_test040'

ewald_alpha_ref = 0.25 / Bohr

lmax_mp = dict(Fe=2)
nzeta_mp = dict(Fe=3)
naux_mp = {
    sym: nzeta_mp[sym] * (lmax_mp[sym] + 1)**2
    for sym in ['Fe']
}
aux_basis_giese_york = {
    sym: '{0}{1}'.format(nzeta_mp[sym], 'SPD'[lmax_mp[sym]])
    for sym in ['Fe']
}

tbpar_dir = '../skf_files/fe_dzp_mp'

if not os.path.exists(os.path.join(tbpar_dir, 'Fe-Fe.skf')):
    tester.reason_for_skipping = 'integral tables have not been generated'
    tester.print_summary()
    exit()

calc = Tibi(aux_basis_giese_york=aux_basis_giese_york,
            basis_set={'Fe': 'spd_sd'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_scf=True,
            include_tightbinding=True,
            kpts=(5, 4, 3),
            kpts_gamma_shift=True,
            mixing_fraction=0.05,
            mixing_history=4,
            mixing_pulay_restart=True,
            mixing_pulay_restart_history=1,
            output_giese_york_chg=True,
            output_giese_york_mag=True,
            scf_aux_mapping='giese_york',
            scf_aux_use_delta=True,
            scf_ukernel_off2c_model='tabulated',
            scf_initial_guess='restart',
            scf_maxiter=300,
            scf_must_converge=True,
            scf_tolerance=1e-4,
            scf_ukernel_expansion_offsite=2,
            scf_ukernel_expansion_onsite=2,
            scf_wkernel_expansion_offsite=2,
            scf_wkernel_expansion_onsite=2,
            scf_write_verbose=False,
            smearing_method='fermi-dirac',
            smearing_width=0.001,
            spin_polarized=True,
            task='energy_forces',
            tbpar_dir=tbpar_dir,
            veff_expansion_offsite=2,
            veff_expansion_onsite=1,
            )

lc = 2.856
atoms = bulk('Fe', 'bcc', a=lc, cubic=True)
atoms.set_positions([[0., 0.1*lc, -0.1*lc], [0.45*lc, 0.5*lc, 0.55*lc]])
N = len(atoms)

e_ref = -75.17660691248594  # regression value
m_ref = 5.3334  # regression value

chg_ref =  np.array([  # regression values
        [-6.406770e-01, -2.715142e-02, -5.673206e-02,  7.011480e-02,
         -4.682569e-03,  2.652413e-02,  1.192072e-02, -6.564006e-03,
          2.451571e-02,  9.754261e-01,  4.206584e-02,  8.298551e-02,
         -1.053686e-01,  1.205434e-02, -3.133473e-02, -1.729369e-02,
          1.256325e-02, -2.405035e-02, -3.347649e-01, -1.660970e-02,
         -3.184936e-02,  4.173826e-02,  2.563214e-03, -7.126479e-04,
         -1.214615e-03, -5.743461e-04,  6.644281e-03],
        [-6.406798e-01,  2.715161e-02,  5.673238e-02, -7.011521e-02,
         -4.682647e-03,  2.652425e-02,  1.192081e-02, -6.564046e-03,
          2.451580e-02,  9.754291e-01, -4.206600e-02, -8.298578e-02,
          1.053689e-01,  1.205439e-02, -3.133480e-02, -1.729374e-02,
          1.256328e-02, -2.405042e-02, -3.347655e-01,  1.660973e-02,
          3.184942e-02, -4.173833e-02,  2.563200e-03, -7.126265e-04,
         -1.214600e-03, -5.743525e-04,  6.644301e-03],
])

mag_ref =  np.array([  # regression values
        [ 9.396283e-02, -4.620469e-02, -9.001679e-02,  6.611312e-02,
         -8.990229e-03,  8.700715e-03,  7.563506e-03, -3.856735e-03,
          1.157274e-02,  8.275102e-01,  2.208253e-02,  4.719568e-02,
         -2.371818e-02, -6.988214e-03,  1.248445e-02,  6.815330e-03,
         -9.213665e-04,  2.864234e-03, -1.692165e-01, -9.134637e-04,
         -3.615971e-03, -2.506765e-03, -2.792301e-03,  1.524119e-03,
          2.199973e-03, -1.432356e-04, -3.530240e-05],
        [ 9.396315e-02,  4.620466e-02,  9.001675e-02, -6.611309e-02,
         -8.990207e-03,  8.700684e-03,  7.563486e-03, -3.856734e-03,
          1.157272e-02,  8.275103e-01, -2.208250e-02, -4.719564e-02,
          2.371814e-02, -6.988221e-03,  1.248446e-02,  6.815337e-03,
         -9.213792e-04,  2.864240e-03, -1.692165e-01,  9.134573e-04,
          3.615959e-03,  2.506777e-03, -2.792292e-03,  1.524108e-03,
          2.199967e-03, -1.432352e-04, -3.531743e-05],
])

f_ref = np.array([  # obtained with calc.calculate_numerical_forces (d=1e-4)
        [ 0.83074592,  0.51809716,  0.2330778 ],
        [-0.8307404 , -0.51810332, -0.23306972],
])

s_ref = np.array([  # obtained with calc.calculate_numerical_stress (d=1e-4)
         0.21243605,  0.20930307,  0.20414841,
        -0.08382052, -0.05948625,  0.05977098,
])


def write_restart_file(magmom=3):
    with open('scf_restart.in', 'w') as f:
        for atom in atoms:
            sym = atom.symbol
            f.write(' '.join(['0'] * naux_mp[sym]))
            m = magmom / np.sqrt(4 * np.pi)
            f.write(' '.join(['', str(m)] + ['0']*(naux_mp[sym]-1)))
            f.write('\n')
    return


if False:
    # For regenerating the references when necessary

    write_restart_file()

    calc.set(ewald_alpha=ewald_alpha_ref)
    atoms.set_calculator(calc)

    e = atoms.get_potential_energy()
    print('Reference energy:', e)

    m = calc.get_magnetization()
    print('Reference total magnetization:', m)

    chg = np.array(calc.get_giese_york_chg())
    print('Reference Giese-York charges:', np.array2string(chg, separator=', '))

    mag = np.array(calc.get_giese_york_mag())
    print('Reference Giese-York magnetizations:',
          np.array2string(mag, separator=', '))

    calc.set(socket_type='unix',
             socket_unix_suffix=unixsocket,
             task='socket')

    with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:
        atoms.calc = socketcalc

        f = calc.calculate_numerical_forces(atoms, d=1e-4)
        print('Reference forces:', np.array2string(f, separator=', '))

        s = calc.calculate_numerical_stress(atoms, d=1e-4)
        print('Reference stress:', np.array2string(s, separator=', '))

        atoms.calc.server.protocol.end()

    exit()


atoms.set_calculator(calc)
write_restart_file()

e_tol = 5e-6
m_tol = 1e-5
q_tol = 2e-5
f_tol = 1e-4
s_tol = 3e-4

for ewald_alpha in [ewald_alpha_ref, 0.1]:
    tag = 'alpha={0:.3f}'.format(ewald_alpha)
    calc.reset()
    calc.set(ewald_alpha=ewald_alpha)

    e = atoms.get_potential_energy()
    tester.check_float('Total energy ({0})'.format(tag), e, e_ref, e_tol)

    m = calc.get_magnetization()
    tester.check_float('Total magnetization ({0})'.format(tag), m, m_ref, m_tol)

    chg = calc.get_giese_york_chg()
    tester.check_array('Giese-York charges ({0})'.format(tag), chg, chg_ref,
                       q_tol)

    mag = calc.get_giese_york_mag()
    tester.check_array('Giese-York magnetizations ({0})'.format(tag), mag,
                       mag_ref, q_tol)

    f = atoms.get_forces()
    tester.check_array('Forces ({0})'.format(tag), f, f_ref, f_tol)

    s = atoms.get_stress()
    tester.check_array('Stress ({0})'.format(tag), s, s_ref, s_tol)

tester.print_summary()
