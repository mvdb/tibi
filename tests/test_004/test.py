""" Tests the Fermi-Dirac smearing.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase.build import bulk
from ase.units import Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = bulk('Au', 'fcc', a=4.0)

calc = Tibi(basis_set={'Au': 'spd'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_tightbinding=True,
            kpts=[3, 3, 3],
            kpts_gamma_shift=False,
            output_bands=True,
            output_hamiltonian=False,
            output_overlap=False,
            smearing_method='fermi-dirac',
            smearing_width=0.1,
            spin_polarized=False,
            tbpar_dir='../skf_files/',
            )
atoms.set_calculator(calc)

eband = atoms.get_potential_energy()
tester.check_float('Band energy', eband, -2.7700698862 * Hartree, 5e-5)

efermi = calc.get_fermi_level()
tester.check_float('Fermi energy', efermi, -0.1766609862 * Hartree, 5e-5)

kptcoords = calc.get_ibz_k_points()
igamma, iother = None, None
for ikpt, coord in enumerate(kptcoords):
    if np.allclose(coord, [0., 0., 0.]):
        igamma = ikpt
    elif np.allclose(coord, [0, 1./3, 1./3]):
        iother = ikpt
assert igamma is not None and iother is not None

occup = calc.get_occupation_numbers(kpt=igamma, spin=0)
occup_ref = [2.] * 6 + [0.] * 3
tester.check_array('Occupations (kpt=%d)' % igamma, occup, occup_ref, 5e-5)

occup = calc.get_occupation_numbers(kpt=iother, spin=0)
occup_ref = [2., 2., 1.99508, 1.99508, 1.92053, 1.81406, 0., 0., 0.]
tester.check_array('Occupations (kpt=%d)' % iother, occup, occup_ref, 5e-5)

# Additional checks using shifted k-point grids
refs = {(2, 2, 2): -2.6311579050 * Hartree,
        (4, 2, 3): -2.7351860009 * Hartree}

for mp_grid, eband_ref in refs.items():
    key = 'shift=True, ' + str(mp_grid)

    calc.set(kpts=mp_grid,
             kpts_gamma_shift=True)
    atoms.set_calculator(calc)

    eband = atoms.get_potential_energy()
    tester.check_float('Band energy (%s)' % key, eband, eband_ref, 5e-5)

tester.print_summary()
