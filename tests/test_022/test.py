""" Tests for (Mulliken) spin-polarized tight-binding (periodic structure).
Reference values have been obtained with DFTB+ v19.1.
"""
from itertools import product
import numpy as np
from ase.build import bulk
from ase.units import Bohr, Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()


a = 2.856
atoms = bulk('Fe', 'bcc', a=a, cubic=True)
atoms.set_positions([[0., 0.1*a, -0.1*a], [0.45*a, 0.5*a, 0.55*a]])

mag_per_atom = 1.

calc = Tibi(basis_set={'Fe': 'spd'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            ewald_tolerance=1e-9,
            include_repulsion=False,
            include_tightbinding=True,
            kpts=[3, 3, 3],
            mixing_fraction=0.4,
            mixing_history=8,
            mixing_scheme='pulay',
            output_mulliken_subshell_mag=True,
            scf_initial_guess='restart',
            scf_maxiter=50,
            scf_must_converge=True,
            scf_tolerance=1e-3,
            smearing_method='fermi-dirac',
            smearing_width=0.05,
            spin_nupdown=len(atoms)*mag_per_atom,
            spin_polarized=True,
            task='energy_forces',
            tbpar_dir='../skf_files/',
            )

# Reference values for different combinations of "include_scf",
# "aux_basis_mulliken" and "spin_fix_nupdown"
etot_ref = {
    (False, 'atom_dependent', True): -4.7387775875 * Hartree,
    (True, 'atom_dependent', False): -4.7694379610 * Hartree,
    (True, 'atom_dependent', True): -4.7538448097 * Hartree,
    (True, 'subshell_dependent', False): -4.7528524041 * Hartree,
    (True, 'subshell_dependent', True): -4.7450352794 * Hartree,
}

forces_ref = {
    (False, 'atom_dependent', True): np.array([
        [ 0.236278925885,  0.283457072100, -0.282805373034],
        [-0.236278925885, -0.283457072100,  0.282805373034],
        ]) * Hartree / Bohr,
    (True, 'atom_dependent', False): np.array([
        [ 0.210120971716,  0.251723822501, -0.246817410018],
        [-0.210120971716, -0.251723822501,  0.246817410018],
        ]) * Hartree / Bohr,
    (True, 'atom_dependent', True): np.array([
        [ 0.236278925885,  0.283457072100, -0.282805373034],
        [-0.236278925885, -0.283457072100,  0.282805373034],
        ]) * Hartree / Bohr,
    (True, 'subshell_dependent', False): np.array([
        [ 0.224872506680,  0.265868392575, -0.259202809819],
        [-0.224872506680, -0.265868392575,  0.259202809819],
        ]) * Hartree / Bohr,
    (True, 'subshell_dependent', True): np.array([
        [ 0.238494309345,  0.285541622626, -0.286219220140],
        [-0.238494309345, -0.285541622626,  0.286219220140],
        ]) * Hartree / Bohr,
}

mag_tot_ref = {
    (True, 'atom_dependent', False): 5.10545136,
    (True, 'atom_dependent', True): len(atoms)*mag_per_atom,
    (True, 'subshell_dependent', False): 3.72114140,
    (True, 'subshell_dependent', True): len(atoms)*mag_per_atom,
}

stress_ref = {
    (False, 'atom_dependent', True): np.array([
        [ 0.008158425038,  0.003218515241, -0.002905207693],
        [ 0.003218515212,  0.006912541917, -0.003527057711],
        [-0.002905207638, -0.003527057694,  0.005677440974],
        ]) * Hartree / Bohr**3,
    (True, 'atom_dependent', False): np.array([
        [ 0.007158892730,  0.002727646488, -0.002542422500],
        [ 0.002727646458,  0.005885787309, -0.003199016021],
        [-0.002542422451, -0.003199016002,  0.004862204083],
        ]) * Hartree / Bohr**3,
    (True, 'atom_dependent', True): np.array([
        [ 0.008158425038,  0.003218515241, -0.002905207693],
        [ 0.003218515212,  0.006912541917, -0.003527057711],
        [-0.002905207638, -0.003527057694,  0.005677440974],
        ]) * Hartree / Bohr**3,
    (True, 'subshell_dependent', False): np.array([
        [ 0.007812745787,  0.003096711906, -0.002771709456],
        [ 0.003096711876,  0.006431763519, -0.003366326739],
        [-0.002771709404, -0.003366326720,  0.005322688525],
        ]) * Hartree / Bohr**3,
    (True, 'subshell_dependent', True): np.array([
        [ 0.008374777820,  0.003241387378, -0.002924581966],
        [ 0.003241387350,  0.006959920993, -0.003551072441],
        [-0.002924581910, -0.003551072427,  0.005838777851],
        ]) * Hartree / Bohr**3,
}

with open('scf_restart.in', 'w') as f:
    for i in range(len(atoms)):
        f.write('0.0 0.0 0.0 0.0 0.0 %.2f\n' % mag_per_atom)

keys = product([False, True], ['atom_dependent', 'subshell_dependent'],
               [False, True])

for key in keys:
    include_scf, ldep, fix_nupdown = key
    suffix = 'include_scf=%s, ldep=%s, fix_nupdown=%s' % key
    if key not in etot_ref:
        continue

    scf_wkernel_on1c = {
        (False, 'atom_dependent'): {},
        (True, 'atom_dependent'): {
            'Fe': -0.41,
        },
        (True, 'subshell_dependent'): {
            'Fe': [-0.44, -0.33, -0.08, -0.33, -0.79,
                   -0.03, -0.08, -0.03, -0.41],
        },
    }
    calc.set(aux_basis_mulliken=ldep,
             include_scf=include_scf,
             scf_wkernel_on1c=scf_wkernel_on1c[(include_scf, ldep)],
             spin_fix_nupdown=fix_nupdown)
    atoms.set_calculator(calc)

    etot = atoms.get_potential_energy()
    tester.check_float('Total (electronic) energy (%s)' % suffix,
                       etot, etot_ref[key], 5e-4)

    forces = atoms.get_forces()
    tester.check_array('Forces (%s)' % suffix, forces, forces_ref[key], 5e-2)

    stress = atoms.get_stress(voigt=False)
    tester.check_array('Stress (%s)' % suffix, stress, stress_ref[key], 2e-3)

    if include_scf:
        mag_tot = calc.get_magnetization()
        tester.check_float('Total magnetization (%s)' % suffix, mag_tot,
                            mag_tot_ref[key], 1e-6 if fix_nupdown else 5e-3)

        mag = calc.get_mulliken_atom_mag()
        tester.check_float('Sum of atom magnetizations (%s)' % suffix,
                           sum(mag), mag_tot, 2e-4)

        mag = calc.get_mulliken_subshell_mag()
        tester.check_float('Sum of subshell magnetizations (%s)' % suffix,
                           sum([sum(x) for x in mag]), mag_tot, 2e-4)

tester.print_summary()
