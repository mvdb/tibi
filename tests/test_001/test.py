""" A basic single-point calculation test.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase import Atoms
from ase.units import Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = Atoms('C2', cell=(8., 8., 10.), pbc=True,
              positions=[[3.5005268, 4.2935255, 5.85873465],
                         [4.4994731, 3.7064744, 5.48924534]])

calc = Tibi(basis_set={'C': 'sp'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=True,
            include_tightbinding=False,
            kpts=[1, 1, 1],
            kpts_gamma_shift=False,
            output_bands=False,
            output_hamiltonian=False,
            output_overlap=False,
            repulsion_2c_form='polynomial',
            smearing_method='none',
            spin_polarized=False,
            tbpar_dir='../skf_files',
            )
atoms.set_calculator(calc)

# First, the repulsive terms
erep = atoms.get_potential_energy()
tester.check_float('Repulsive energy', erep, 0.5360364097 * Hartree, 1e-5)


# Next, the band terms and k-point info
calc.set(include_repulsion=False,
         include_tightbinding=True)

eband = atoms.get_potential_energy()
tester.check_float('Band energy', eband, -3.3957581192 * Hartree, 1e-5)

efermi = calc.get_fermi_level()
tester.check_bounds('Fermi energy', efermi, [-8.62277121, -7.83179065])

nspin = calc.get_number_of_spins()
tester.check_int('Number of spins', nspin, 1)

kptcoords = calc.get_ibz_k_points()
tester.check_int('Number of k-points', len(kptcoords), 1)
tester.check_array('K-point coordinates', kptcoords[0], [0., 0., 0.], 1e-12)

kptweights = calc.get_k_point_weights()
tester.check_array('K-point weights', kptweights, [1.], 1e-12)


# And also the band structure and H and S matrices
calc.set(output_bands=True,
         output_hamiltonian=True,
         output_overlap=True)
calc.calculate(atoms)

bands_ref = np.array([[[-18.413, -10.543, -8.623, -8.623,
                        -7.832, 0.057, 0.057, 46.866]]])
nkpt = len(calc.get_ibz_k_points())
print('Bands (ispin, ikpt, [...])')
for ispin in range(nspin):
    for ikpt in range(nkpt):
        bands = calc.get_eigenvalues(kpt=ikpt, spin=ispin)
        tester.check_array('Bands %s %s' % (ispin, ikpt), bands,
                           bands_ref[ispin, ikpt, :], 5e-4)


hamiltonian_ref = np.diagflat([[-0.505228] + [-0.194173] * 3] * 2)
hamiltonian_ref[4:, :4] = np.array([
                            [-3.944618000218350E-01, 3.142021074096793E-01,
                             -1.846472560799163E-01, -1.162167777938943E-01],
                            [-3.142021074096793E-01, 1.631116850051939E-01,
                             -2.109209116975940E-01, -1.327533874439797E-01],
                            [1.846472560799163E-01, -2.109209116975940E-01,
                             -7.184661815195226E-02, 7.801522684332893E-02],
                            [1.162167777938943E-01, -1.327533874439797E-01,
                             7.801522684332893E-02, -1.466958825883305E-01]])

print('Hamiltonian (ispin, ikpt, iband, [...])')
for ispin in range(nspin):
    for ikpt in range(nkpt):
        hamiltonian = calc.get_hamiltonian_matrix(kpt=ikpt, spin=ispin)
        for iband in range(hamiltonian.shape[0]):
            tester.check_array('Hamiltonian %s %s %s' % (ispin, ikpt, iband),
                                hamiltonian[iband, :],
                                hamiltonian_ref[iband, :], 1e-6)


overlap_ref = np.diagflat([1.] * 8)
overlap_ref[4:, :4] = np.array([
                            [3.766189813964029E-01, -3.301492708395870E-01,
                             1.940189304434967E-01, 1.221152992243864E-01],
                            [3.301492708395870E-01, -1.619714416741464E-01,
                             2.307359001381755E-01, 1.452249191497689E-01],
                            [-1.940189304434967E-01, 2.307359001381755E-01,
                             9.506004332905270E-02, -8.534437591574620E-02],
                            [-1.221152992243864E-01, 1.452249191497689E-01,
                             -8.534437591574620E-02, 1.769410301510706E-01]])

print('Overlap (ispin, ikpt, iband, [...])')
for ispin in range(nspin):
    for ikpt in range(nkpt):
        overlap = calc.get_overlap_matrix(kpt=ikpt, spin=ispin)
        for iband in range(overlap.shape[0]):
            tester.check_array('Overlap %s %s %s' % (ispin, ikpt, iband),
                                overlap[iband, :],
                                overlap_ref[iband, :], 1e-6)

tester.print_summary()
