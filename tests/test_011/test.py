""" Test with a one-dimensional system (nanotube-like).
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase import Atoms
from ase.units import Hartree, Bohr
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

positions = [[9.269532884509735, 5.874716020382699, 1.331955090603167],
             [8.162363424509735, 6.333320630382699, 0.621955090603166],
             [7.055193974509725, 5.874716020382699, 1.331955090603167],
             [6.596589364509725, 4.767546560382700, 0.621955090603166],
             [7.055193974509725, 3.660377110382704, 1.331955090603167],
             [8.162363424509735, 3.201772500382702, 0.621955090603166],
             [9.269532884509735, 3.660377110382704, 1.331955090603167],
             [9.728137494509735, 4.767546560382700, 0.621955090603166]]

cell = [[11.131548, 0.000000, 0.00],
        [ 5.565774, 9.640203, 0.00],
        [ 0.000000, 0.000000, 2.13]]

atoms = Atoms('C8', pbc=True, cell=cell, positions=positions)

calc = Tibi(basis_set={'C': 'sp'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_tightbinding=True,
            kpts=[1, 1, 2],
            kpts_gamma_shift=False,
            output_bands=False,
            output_hamiltonian=False,
            output_overlap=False,
            smearing_method='fermi-dirac',
            smearing_width=0.01,
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/',
            )
atoms.set_calculator(calc)

eband = atoms.get_potential_energy()
tester.check_float('Band energy', eband, -13.8441245513 * Hartree, 1e-3)

fband_ref = np.array([
    [-0.117297940926, -0.117297940927, -0.249735029573],
    [-0.000000005453, -0.165884346716,  0.249735031330],
    [ 0.117297948786, -0.117297937342, -0.249735031619],
    [ 0.165884341901, -0.000000005452,  0.249735031909],
    [ 0.117297945204,  0.117297945202, -0.249735033667],
    [-0.000000005454,  0.165884341902,  0.249735031910],
    [-0.117297937342,  0.117297948787, -0.249735031619],
    [-0.165884346716, -0.000000005453,  0.249735031330]]) * Hartree / Bohr

fband = atoms.get_forces()
maxdiff = np.max(np.abs(fband - fband_ref))
tester.check_float('Band forces maxdiff', maxdiff, 0., 1e-3)

sband_ref = np.identity(3) * [0.001272844838, 0.001272844838, -0.001002620886]
sband_ref *= Hartree / Bohr**3

sband = atoms.get_stress(voigt=False)
maxdiff = np.max(np.abs(sband - sband_ref))
tester.check_float('Band stress maxdiff', maxdiff, 0., 5e-5)

tester.print_summary()
