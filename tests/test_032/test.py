""" Tests for 3-center repulsion, with one element. """
import numpy as np
from ase import Atoms
from ase.build import bulk
from ase.units import Hartree
from ase.calculators.socketio import SocketIOCalculator
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

calc = Tibi(basis_set={'C': 'sp'},
            diag_algo=tester.get_diag_algo(),
            include_repulsion=True,
            include_tightbinding=False,
            repulsion_2c_form='spline',
            repulsion_3c_eps_inner=1e-6,
            repulsion_3c_eps_outer=1e-8,
            repulsion_3c_interpolation=3,
            repulsion_3c_upsampling=(1, 1, 1),
            repulsion_expansion=3,
            task='energy_forces',
            tbpar_dir='./',
            )


# Part I: an equilateral triangle with three C atoms
e_tol, f_tol = 1e-3, 1e-3

d = 1.2
atoms = Atoms('C3', cell=[12.]*3, pbc=False,
              positions=[[0.]*3, [d, 0., 0.], [0.5*d, np.sqrt(3)/2.*d, 0.]])
atoms.euler_rotate(phi=20.0, theta=-32.0, psi=83.0, center='COP')
atoms.center()

atoms.set_calculator(calc)
e = atoms.get_potential_energy()
e_ref = 2.7773419 * Hartree  # 3c terms contributing by -0.0208236 Hartree
tester.check_float('Repulsive energy (C3)', e, e_ref, e_tol)

f = atoms.get_forces()
f_ref = calc.calculate_numerical_forces(atoms, d=1e-4)
tester.check_array('Repulsive forces (C3)', f, f_ref, f_tol)

## Additional checks with linear interpolation
calc.set(repulsion_3c_interpolation=1,
         repulsion_3c_upsampling=(5, 5, 5))
atoms.set_calculator(calc)

e = atoms.get_potential_energy()
tester.check_float('Repulsive energy (C3, linear)', e, e_ref, e_tol)

f = atoms.get_forces()
tester.check_array('Repulsive forces (C3, linear)', f, f_ref, f_tol)


# Part II: bulk diamond
e_tol, f_tol, s_tol = 5e-4, 1e-6, 1e-4

calc.set(repulsion_3c_interpolation=3,
         repulsion_3c_upsampling=(1, 1, 1),
         task='energy')

lc = 3.6
atoms = bulk('C', 'diamond', a=lc, cubic=True)
atoms.set_calculator(calc)
e_ref = atoms.get_potential_energy() * 2. / len(atoms)

atoms = bulk('C', 'diamond', a=lc, cubic=False)

unixsocket = 'ase_tibi_test032'
calc.set(socket_type='unix',
         socket_unix_suffix=unixsocket,
         task='socket')

with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:
    atoms.calc = socketcalc

    e = atoms.get_potential_energy()
    tester.check_float('Repulsive energy (diamond)', e, e_ref, e_tol)

    f = atoms.get_forces()
    tester.check_array('Repulsive forces (diamond)', f, 0., f_tol)

    s = atoms.get_stress()
    s_ref = socketcalc.calculate_numerical_stress(atoms, d=1e-4)
    tester.check_array('Repulsive stress (diamond)', s, s_ref, s_tol)

    atoms.calc.server.protocol.end()

tester.print_summary()
