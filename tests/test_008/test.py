""" Single-point test to check that omitting certain subshells,
or distributing them over separate SKF files, leads to the expected
(lack of) changes in the H and S matrices and the atom-and subshell-
dependent charges and the Mulliken populations.
"""
import os
import numpy as np
from itertools import combinations
from ase import Atoms
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = Atoms('Au2', pbc=False,
              positions=[[4.24707929324, 4.84322981472, 5.36094696310],
                         [3.75292070675, 3.15677018527, 7.13905303689]])

skf_lines = open('../skf_files/Au-Au.skf', 'r').readlines()
skf_files = []
for i in range(3):
    for j in range(3):
        filename = 'Au{0}-Au{1}.skf'.format('+'*i, '+'*j)
        skf_files.append(filename)
        with open(filename, 'w') as f:
            if i == j:
                f.writelines(skf_lines)
            else:
                offdiag = [' '.join(['0.0']*3 + ['1.0']*3) + '\n']
                f.writelines(skf_lines[:1] + offdiag + skf_lines[2:])

kwargs = dict(complex_wfn=False,
              diag_algo=tester.get_diag_algo(),
              include_repulsion=False,
              include_tightbinding=True,
              kpts=[1, 1, 1],
              kpts_gamma_shift=False,
              output_bands=True,
              output_hamiltonian=True,
              output_mulliken_orbital_pop=True,
              output_mulliken_subshell_chg=True,
              output_overlap=True,
              smearing_method='fermi-dirac',
              smearing_width=0.1,
              spin_polarized=False,
              task='energy',
              tbpar_dir='./',
              )

# Full H and S matrices, with all basis functions included (s, p, d):
calc = Tibi(basis_set={'Au': 'spd'}, **kwargs)
atoms.set_calculator(calc)
e_full = atoms.get_potential_energy()
hamiltonian_full = calc.get_hamiltonian_matrix(kpt=0, spin=0)
overlap_full = calc.get_overlap_matrix(kpt=0, spin=0)

# Tests with only a subset of these basis functions:
for nchoice in [1, 2, 3]:
    for selection in combinations(range(3), nchoice):
        inc = ''.join(['spd'[i] for i in range(3) if i in selection])

        # Get the submatrix indices for the 'full' reference matrices
        indices = []
        counter = 0
        for il in range(3):
            numlm = 2*il + 1
            if il in selection:
                indices.extend(range(counter, counter + numlm))
            counter += numlm

        maxnumlm = sum([2*il + 1 for il in range(3)])
        indices.extend([maxnumlm + i for i in indices])
        submatrix = np.ix_(indices, indices)
        nband = len(indices)

        # If more than 1 subshell, also test the case where they
        # are given in separate SKF files
        basis_sets = [inc]
        if nchoice > 1:
            basis_sets.append('_'.join(inc))

        for bs in basis_sets:
            calc = Tibi(basis_set={'Au': bs}, **kwargs)
            atoms.set_calculator(calc)
            e = atoms.get_potential_energy()
            if nchoice == 3:
                tester.check_float('Total energy %s' % bs, e, e_full, 1e-6)

            # Check that the common subblocks are indeed equal
            hamiltonian = calc.get_hamiltonian_matrix(kpt=0, spin=0)
            shape = np.shape(hamiltonian)
            assert shape == (nband, nband), ('H', shape, nband)

            maxdiff = np.max(np.abs(hamiltonian - hamiltonian_full[submatrix]))
            tester.check_float('Hamiltonian %s maxdiff' % bs, maxdiff, 0., 1e-9)

            overlap = calc.get_overlap_matrix(kpt=0, spin=0)
            shape = np.shape(overlap)
            assert shape == (nband, nband), ('S', shape, nband)

            maxdiff = np.max(np.abs(overlap - overlap_full[submatrix]))
            tester.check_float('Overlap %s maxdiff' % bs, maxdiff, 0., 1e-9)

            # Sanity checks on the charges and Mulliken populations
            # Note: no interatomic electron transfer due to symmetry

            charges_atom = calc.get_mulliken_atom_chg()
            charges_l = calc.get_mulliken_subshell_chg()
            populations = calc.get_mulliken_orbital_pop()

            eps = 5e-6

            tester.check_array('charges_atom %s' % bs, charges_atom, 0., eps)
            tester.check_array('charges_l_sum %s' % bs,
                               np.sum(charges_l, axis=1), 0., eps)
            numl = len(selection)
            for i in range(len(atoms)):
                tester.check_int('charges_l_len %s (atom=%d)' % (bs, i),
                                 len(charges_l[i]), numl)

            nval = np.sum(np.array([1., 0., 10.])[np.array(selection)])
            numlm = sum([2*il+1 for il in range(3) if il in selection])
            for i in range(len(atoms)):
                tester.check_float('populations_sum %s (atom=%d)' % (bs, i),
                                   np.sum(populations[i]), nval, eps)
                tester.check_int('populations_len %s (atom=%d)' % (bs, i),
                                 len(populations[i]), numlm)

for f in skf_files:
    os.system('rm %s' % f)

tester.print_summary()
