""" Basic tests for models beyond the 1- and 2-center approximations to the
on- and off-site Hamiltonian integrals, respectively (with more-than-one
subshell of the same angular momentum).
"""
import numpy as np
from ase import Atoms
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

atoms = Atoms('H3', pbc=False,
              positions=[[5.813, 6.235, 6.044],
                         [6.856, 6.927, 6.692],
                         [6.323, 8.008, 6.573]])

calc = Tibi(basis_set={'H': 's_s'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_scf=False,
            include_tightbinding=True,
            kpts=[1, 1, 1],
            kpts_gamma_shift=False,
            output_bands=True,
            output_hamiltonian=True,
            output_overlap=False,
            smearing_method='fermi-dirac',
            smearing_width=0.01,
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='./',
            )

# Reference values for each (veff_expansion_onsite, veff_expansion_offsite)
# NOTE: the (non-negligible) one-center contributions for the on-site s-s*
# integrals are not included, as these are currently neglected in Tibi (& DFTB+)
# Also the eigenvalues need to be ignored for now, as the corresponding overlap
# integrals are assumed to be zero.
ham_ref = {
    (2, 2): np.array([
                [-2.708207e-01, 2.328717e-02, 0.000000e+00,
                 0.000000e+00, 0.000000e+00, 0.000000e+00],
                [2.328717e-02, 4.971989e-02, 0.000000e+00,
                 0.000000e+00, 0.000000e+00, 0.000000e+00],
                [-2.026328e-01, 5.301527e-02, -3.172524e-01,
                 1.806072e-02, 0.000000e+00, 0.000000e+00],
                [5.297474e-02, 4.286277e-02, 1.806072e-02,
                 4.947531e-02, 0.000000e+00, 0.000000e+00],
                [-1.111867e-01, 7.638929e-02, -2.514432e-01,
                 3.327774e-02, -2.914255e-01, 1.768090e-02],
                [7.635917e-02, 3.874729e-02, 3.324163e-02,
                 4.230115e-02, 1.768090e-02, 5.040560e-02],
    ]),
    (2, 3): np.array([
                [-2.708207e-01, 2.328717e-02, 0.000000e+00,
                 0.000000e+00, 0.000000e+00, 0.000000e+00],
                [2.328717e-02, 4.971989e-02, 0.000000e+00,
                 0.000000e+00, 0.000000e+00, 0.000000e+00],
                [-2.193166e-01, 8.020512e-02, -3.172524e-01,
                 1.806072e-02, 0.000000e+00, 0.000000e+00],
                [5.733404e-02, 2.434757e-02, 1.806072e-02,
                 4.947531e-02, 0.000000e+00, 0.000000e+00],
                [-1.368415e-01, 9.984012e-02, -2.658867e-01,
                 3.979041e-02, -2.914255e-01, 1.768090e-02],
                [9.122425e-02, 2.407034e-02, 5.467368e-02,
                 1.870274e-02, 1.768090e-02, 5.040560e-02],
    ]),
    (3, 2): np.array([
                [-2.683493e-01, 2.037112e-02, 0.000000e+00,
                 0.000000e+00, 0.000000e+00, 0.000000e+00],
                [2.037112e-02, 5.823019e-02, 0.000000e+00,
                 0.000000e+00, 0.000000e+00, 0.000000e+00],
                [-2.026328e-01, 5.301527e-02, -3.156679e-01,
                 1.712706e-02, 0.000000e+00, 0.000000e+00],
                [5.297474e-02, 4.286277e-02, 1.712706e-02,
                 5.201961e-02, 0.000000e+00, 0.000000e+00],
                [-1.111867e-01, 7.638929e-02, -2.514432e-01,
                 3.327774e-02, -2.890867e-01, 1.520299e-02],
                [7.635917e-02, 3.874729e-02, 3.324163e-02,
                 4.230115e-02, 1.520299e-02, 5.743652e-02],
    ]),
    (3, 3): np.array([
                [-2.683493e-01, 2.037112e-02, 0.000000e+00,
                 0.000000e+00, 0.000000e+00, 0.000000e+00],
                [2.037112e-02, 5.823019e-02, 0.000000e+00,
                 0.000000e+00, 0.000000e+00, 0.000000e+00],
                [-2.193166e-01, 8.020512e-02, -3.156679e-01,
                 1.712706e-02, 0.000000e+00, 0.000000e+00],
                [5.733404e-02, 2.434757e-02, 1.712706e-02,
                 5.201961e-02, 0.000000e+00, 0.000000e+00],
                [-1.368415e-01, 9.984012e-02, -2.658867e-01,
                 3.979041e-02, -2.890867e-01, 1.520299e-02],
                [9.122425e-02, 2.407034e-02, 5.467368e-02,
                 1.870274e-02, 1.520299e-02, 5.743652e-02],
    ]),
}

models = {
    (i, j): {'veff_expansion_onsite': i, 'veff_expansion_offsite': j}
    for i in [2, 3] for j in [2, 3]
}

for model, param in models.items():
    calc.reset()
    calc.set(**param)
    atoms.set_calculator(calc)
    e = atoms.get_potential_energy()

    htol, ftol = 5e-4, 5e-4

    ham = calc.get_hamiltonian_matrix(kpt=0, spin=0)
    tester.check_array('Hamiltonian %s' % str(model), ham, ham_ref[model], htol)

    fband = atoms.get_forces()
    fband_ref = calc.calculate_numerical_forces(atoms, d=5e-4)
    tester.check_array('Band forces %s' % str(model), fband, fband_ref, ftol)

tester.print_summary()
