label='lda'
extra_options=${@}
xc='LDA'
basis='dzp'
zeta_method='--zeta-method=split_valence --aux-zeta-method=split_valence'

el='Fe'
configuration='[Ar],3d6,4s2'
valence='3d,4s'
rcut='4.716796875,6.11187744140625'
aux_basis='3D'
aux_subshell='3d'

hotcent-basis ${el} --pseudo-label=${label} --label=${label} -f ${xc} \
              --basis=${basis} -c ${configuration} -v ${valence} \
              --rcut-approach=user --rcut=${rcut} \
              --aux-basis=${aux_basis} --aux-subshell=${aux_subshell} \
              ${zeta_method} ${extra_options} 1>"${el}.${label}.out" 2>&1
