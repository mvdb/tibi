hotcent-tables Fe \
               --aux-mappings=giese_york \
               --giese-york-constraint-method=original \
               --tasks=all,^on2c,^off3c,^on3c,^rep3c \
               --label=lda \
               ${@}
