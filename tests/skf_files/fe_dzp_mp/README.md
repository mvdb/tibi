# Generating the parameters with Hotcent

- First download the https://nninc.cnf.cornell.edu/psp_files/Fe.psf
  pseudopotential and rename it to Fe.lda.psf.

- Generate the parameters from the included YAML file with

  $ bash generate_tables.sh --pseudo-path=${PWD}

  followed by

  $ mv tables_Fe/* .
  $ rmdir tables_Fe
  $ hotcent-concat -b dzp Fe


# Regenerating the basis with Hotcent

With this command the basis can be regenerated if needed:

$ bash generate_basis.sh --pseudo-path=${PWD} --plot
