# Generating the parameters with Hotcent

- First download the https://nninc.cnf.cornell.edu/psp_files/O.psf
  and https://nninc.cnf.cornell.edu/psp_files/P.psf pseudopotentials
  and rename them to O.lda.psf and P.lda.psf, respectively.

- Generate the parameters from the included YAML files with

  $ bash generate_tables.sh --pseudo-path=${PWD}

  followed by

  $ mv tables_*/* .
  $ rmdir tables_*
  $ hotcent-concat -b dzp P O


# Regenerating the basis with Hotcent

With this command the basis can be regenerated if needed:

$ bash generate_basis.sh --pseudo-path=${PWD} --plot
