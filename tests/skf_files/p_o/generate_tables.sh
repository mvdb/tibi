hotcent-tables P,O \
               --aux-mappings=giese_york \
               --giese-york-constraint-method=reduced \
               --tasks=all,^on2c,^off3c,^on3c,^rep3c \
               --label=lda \
               ${@}
