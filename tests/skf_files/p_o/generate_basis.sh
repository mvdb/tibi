label='lda'
extra_options=${@}
xc='LDA'
basis='dzp'
zeta_method='--zeta-method=split_valence --aux-zeta-method=split_valence'

el='P'
configuration='[Ne],3s2,3p3'
valence='3s,3p'
rcut='4.775390625,5.89453125'
aux_basis='2P'
hotcent-basis ${el} --pseudo-label=${label} --label=${label} -f ${xc} \
              --basis=${basis} -c ${configuration} -v ${valence} \
              --rcut-approach=user --rcut=${rcut} --aux-basis=${aux_basis} \
              ${zeta_method} ${extra_options} 1>"${el}.${label}.out" 2>&1

el='O'
configuration='[He],2s2,2p4'
valence='2s,2p'
rcut='3.896484375,4.283203125'
aux_basis='3D'
hotcent-basis ${el} --pseudo-label=${label} --label=${label} -f ${xc} \
              --basis=${basis} -c ${configuration} -v ${valence} \
              --rcut-approach=user --rcut=${rcut} --aux-basis=${aux_basis} \
              ${zeta_method} ${extra_options} 1>"${el}.${label}.out" 2>&1
