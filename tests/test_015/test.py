""" Tests for (Mulliken) self-consistent tight-binding
in a periodic structure with k-points and d-electrons.
Reference values have been obtained with DFTB+ v19.1.
"""
import numpy as np
from ase import Atoms
from ase.units import Bohr, Hartree
from tibi import Tibi
from tibi.test import Tester

tester = Tester()

positions = np.array([[1.96374659, 1.28206628, 4.0],
                      [5.14874241, 1.38206628, 4.5],
                      [3.60624458, 3.68054582, 4.5],
                      [6.59124025, 3.88054582, 5.1],
                      [4.74874241, 6.17902535, 4.5],
                      [7.93373808, 6.17902535, 4.5],
                      [3.80624458, 2.21489279, 6.4]])

cell = np.array([[5.76999133, 0., 0.],
                 [4.32749350, 7.49543861, 0.],
                 [0., 0., 14.35558909]])

atoms = Atoms('Au7', positions=positions, cell=cell, pbc=True)

calc = Tibi(basis_set={'Au': 'spd'},
            complex_wfn=True,
            diag_algo=tester.get_diag_algo(),
            ewald_tolerance=1e-6,
            include_repulsion=False,
            include_scf=True,
            include_tightbinding=True,
            kpts=[3, 2, 1],
            kpts_gamma_shift=False,
            mixing_fraction=0.25,
            mixing_history=8,
            mixing_scheme='pulay',
            output_mulliken_subshell_chg=True,
            scf_maxiter=50,
            scf_must_converge=True,
            scf_tolerance=1e-3,
            smearing_method='fermi-dirac',
            smearing_width=0.1,
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir='../skf_files/',
            )

etot_ref = {
    'atom_dependent': -19.6093972154 * Hartree,
    'subshell_dependent': -19.5262388659 * Hartree,
}

forces_ref = {
    'atom_dependent': Hartree / Bohr * np.array([
        [-0.004353195131, -0.010149658711, -0.009412667393],
        [ 0.010600175450, -0.008816331325, -0.007580424605],
        [ 0.000930907396,  0.015143886906, -0.004541897416],
        [-0.001163149670,  0.007470136699,  0.002396148095],
        [ 0.004436009251,  0.001262234047, -0.005679871113],
        [-0.007213669644,  0.001787929596, -0.002912399669],
        [-0.003237077653, -0.006698197213,  0.027731112101]]),
    'subshell_dependent': Hartree / Bohr * np.array([
        [ 0.001412906745, -0.010832537811, -0.013517602587],
        [ 0.011631567704, -0.015358837079, -0.016648030580],
        [-0.001395653291,  0.021676335818, -0.019736719490],
        [-0.003858574014,  0.005672726583,  0.008002753622],
        [ 0.014726706602,  0.003787972008, -0.004784482719],
        [-0.013855868994,  0.006312855464, -0.003556146100],
        [-0.008661084753, -0.011258514983,  0.050240227855]]),
}

stress_ref = {
    'atom_dependent': Hartree / Bohr**3 * np.array([
        [-0.000074482311, -0.000000517094,  0.000002107148],
        [-0.000000517088, -0.000086676241,  0.000001429745],
        [ 0.000002107149,  0.000001429747, -0.000026516953]]),
    'subshell_dependent': Hartree / Bohr**3 * np.array([
        [-0.000121724135,  0.000001814241,  0.000008785124],
        [ 0.000001814246, -0.000137293931,  0.000005670098],
        [ 0.000008785125,  0.000005670100, -0.000048268151]]),
}

for ldep in ['atom_dependent', 'subshell_dependent']:
    calc.set(aux_basis_mulliken=ldep)
    atoms.set_calculator(calc)

    etot = atoms.get_potential_energy()
    tester.check_float('Total (electronic) energy (ldep=%s)' % ldep,
                       etot, etot_ref[ldep], 1e-3)

    forces = atoms.get_forces()
    for i in range(len(atoms)):
        tester.check_array('Force on atom %d (ldep=%s)' % (i, ldep),
                           forces[i, :], forces_ref[ldep][i, :], 5e-3)

    stress = atoms.get_stress(voigt=False)
    tester.check_array('Stress tensor (ldep=%s)' % ldep,
                       stress, stress_ref[ldep], 5e-5)

tester.print_summary()
