""" Test for performance evaluations of Giese-York self-consistency
using a medium-sized periodic structure. """
import numpy as np
import os
from ase.build import bulk
from ase.calculators.socketio import SocketIOCalculator
from tibi import Tibi
from tibi.test import Tester


tester = Tester()

tbpar_dir = '../skf_files/p_o'

if not os.path.exists(os.path.join(tbpar_dir, 'O-O.skf')):
    tester.reason_for_skipping = 'integral tables have not been generated'
    tester.print_summary()
    exit()

calc = Tibi(aux_basis_giese_york={'P': '2P', 'O': '3D'},
            basis_set={'P': 'spd_sp', 'O': 'spd_sp'},
            complex_wfn=False,
            diag_algo=tester.get_diag_algo(),
            include_repulsion=False,
            include_scf=True,
            include_tightbinding=True,
            kpts=(1, 1, 1),
            kpts_gamma_shift=True,
            mixing_fraction=0.15,
            output_giese_york_chg=False,
            scf_aux_mapping='giese_york',
            scf_aux_use_delta=True,
            scf_initial_guess='zero',
            scf_maxiter=15,
            scf_must_converge=True,
            scf_tolerance=1e-4,
            scf_ukernel_expansion_offsite=2,
            scf_ukernel_expansion_onsite=2,
            scf_ukernel_off2c_model='tabulated',
            scf_write_verbose=False,
            smearing_method='fermi-dirac',
            smearing_width=0.01,
            spin_polarized=False,
            task='energy_forces',
            tbpar_dir=tbpar_dir,
            veff_expansion_offsite=2,
            veff_expansion_onsite=1,
            )

atoms = bulk('PO', crystalstructure='rocksalt', a=3., cubic=True)
atoms = atoms.repeat((2, 2, 2))
atoms.center()
N = len(atoms)

e_ref = -2246.2875043820086
f_ref = np.zeros((N, 3))
s_ref = np.array([-7.20575441]*3 + [0]*3)


if False:
    # For regenerating the references when necessary

    atoms.set_calculator(calc)
    e = atoms.get_potential_energy()
    print('Reference energy:', e)

    unixsocket = 'ase_tibi_test039'
    calc.set(socket_type='unix',
             socket_unix_suffix=unixsocket,
             task='socket')

    with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:
        atoms.calc = socketcalc

        s = calc.calculate_numerical_stress(atoms, d=1e-4)
        print('Reference stress:', np.array2string(s, separator=', '))

        atoms.calc.server.protocol.end()

    exit()


atoms.set_calculator(calc)

e_tol, f_tol, s_tol = 5e-4, 2e-4, 1e-5

e = atoms.get_potential_energy()
tester.check_float('Total energy', e, e_ref, e_tol)

f = atoms.get_forces()
tester.check_array('Forces', f, f_ref, f_tol)

s = atoms.get_stress()
tester.check_array('Stress', s, s_ref, s_tol)

tester.print_summary()
