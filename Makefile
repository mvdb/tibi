# Directory locations:
SRCDIR = $(PWD)/src
BLDDIR = $(PWD)/build
TSTDIR = $(PWD)/tests
OBJDIR = $(BLDDIR)/obj
BINDIR = $(BLDDIR)/bin
LIBDIR = $(BLDDIR)/lib

# ELPA build defaults:
ELPA ?= 0

# Test option defaults:
PYTHON ?= python
TEST_DIAG ?= DC

# Set the ASE_{TIBI,EIGER}_COMMAND variables for the test suite
# This ignores the value of the corresponding environment variable
# (if defined) to e.g. ensure that the correct executable is used
ASE_TIBI_COMMAND = $(BINDIR)/tibi PREFIX.in PREFIX.xyz > PREFIX.out
ASE_EIGER_COMMAND = $(BINDIR)/eiger PREFIX.in > PREFIX.out

# Recursive make requires turning off parallelization at the parent level
# (sub-makes will still run in parallel when the '-j ...' option is used)
.NOTPARALLEL:

# All the available targets:
default: all

.PHONY: all
all: tibi eiger

.PHONY: clean
clean:
	$(RM) -r $(OBJDIR)
	$(RM) -r $(BINDIR)
	$(RM) -r $(LIBDIR)

$(OBJDIR):
	mkdir -p $(OBJDIR)

$(BINDIR):
	mkdir -p $(BINDIR)

$(LIBDIR):
	mkdir -p $(LIBDIR)

eiger: $(OBJDIR) $(BINDIR) libtibi
	$(MAKE) -C $(OBJDIR) -f $(PWD)/src/Makefile \
	  ROOT=$(PWD) SRC=$(SRCDIR) BLD=$(OBJDIR) BIN=$(BINDIR) LIB=$(LIBDIR) \
	  $(BINDIR)/eiger

tibi: external_fsockets $(OBJDIR) $(BINDIR)
	$(MAKE) -C $(OBJDIR) -f $(PWD)/src/Makefile \
	  ROOT=$(PWD) SRC=$(SRCDIR) BLD=$(OBJDIR) BIN=$(BINDIR) \
	  $(BINDIR)/tibi

libtibi: external_fsockets $(OBJDIR) $(LIBDIR)
	$(MAKE) -C $(OBJDIR) -f $(PWD)/src/Makefile \
	  ROOT=$(PWD) SRC=$(SRCDIR) BLD=$(OBJDIR) LIB=$(LIBDIR) \
	  $(LIBDIR)/libtibi.a

external_fsockets: $(OBJDIR)
	mkdir -p $(OBJDIR)/external/fsockets
	$(MAKE) -C $(OBJDIR)/external/fsockets \
	  -f $(PWD)/external/fsockets/Makefile \
	  ROOT=$(PWD) SRC=$(PWD)/external/fsockets BLD=$(OBJDIR)/external/fsockets

test: all
	@echo '# ----------- Running the test suite ----------- #'
	$(MAKE) -f $(PWD)/tests/Makefile TST=$(TSTDIR) \
      ASE_TIBI_COMMAND="$(ASE_TIBI_COMMAND)" \
      ASE_EIGER_COMMAND="$(ASE_EIGER_COMMAND)" \
      PYTHON=$(PYTHON)
	@echo '# ------------- Testing completed -------------- #'

test_%: all
	@echo '# -------------- Running test_$* -------------- #'
	$(MAKE) -f $(PWD)/tests/Makefile test_$* TST=$(TSTDIR) \
      ASE_TIBI_COMMAND="$(ASE_TIBI_COMMAND)" \
      ASE_EIGER_COMMAND="$(ASE_EIGER_COMMAND)" \
      PYTHON=$(PYTHON)
	@echo '# ------------ test_$* completed  ------------- #'

help:
	@echo
	@echo -e ' Usage: make [OPTIONS=...] [targets]\n'
	@echo -e ' Examples:\n'
	@echo '   make clean'
	@echo '   make DEBUG=1'
	@echo '   make OMP=1 clean tibi'
	@echo '   make OMP=1 OMP_NUM_THREADS=2 test'
	@echo
	@echo -e ' Options for the targets "tibi" and "libtibi":\n'
	@echo '  DEBUG              set to 1 to use compiler options suitable'
	@echo '                     for debugging (default: DEBUG=0, i.e.'
	@echo '                     options for production)'
	@echo '  OMP                set to 1 to produce an OpenMP build'
	@echo '                     (default: OMP=0, i.e. a serial build)'
	@echo '  ELPA               set to 1 to enable ELPA eigensolvers'
	@echo '                     (default: ELPA=0, i.e. no ELPA support)'
	@echo
	@echo -e ' Options for the target "test":\n'
	@echo '  PYTHON             name of the Python interpreter'
	@echo '                     (default: python)'
	@echo '  TEST_DIAG          diagonalization algorithm to use when'
	@echo '                     running the tests; set to ELPA to use'
	@echo '                     ELPA eigensolvers (default: DC, i.e.'
	@echo '                     LAPACK divide-and-conquer eigensolvers)'
	@echo
	@echo -e ' Available targets:\n'
	@echo '  help               print this help message'
	@echo '  clean              clean the build directories so that '
	@echo '                     recompiling will happen from scratch'
	@echo '  all (default)      build all executable targets (tibi, eiger)'
	@echo '  tibi               build the main Tibi executable'
	@echo '                     (./build/bin/tibi)'
	@echo '  libtibi            build the static Tibi library'
	@echo '                     (./build/lib/libtibi.a)'
	@echo '  eiger               build the Eiger executable'
	@echo '                     (./build/bin/eiger)'
	@echo '  test               run the test suite; individual tests can also'
	@echo '                     be selected by appending the test number,'
	@echo '                     e.g. "test_004"'
	@echo
