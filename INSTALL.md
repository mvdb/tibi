## Installation

<br>

### 1. Building from source

<br>

#### Getting the code

The **Tibi** repository is hosted on [gitlab](https://gitlab.com/mvdb/tibi).
A copy of the development version can be obtained either by [direct download](
https://gitlab.com/mvdb/tibi/-/archive/master/tibi-master.zip)
or by cloning with [git](https://git-scm.com/):

```bash
git clone https://gitlab.com/mvdb/tibi.git
```

<br>

#### Requirements

Building the *tibi* and *eiger* executables requires:

* [GNU Make](https://www.gnu.org/software/make)
* a Fortran compiler that complies with (most of) the 2003 and 2008 standards.
  <br>The following compilers ought to be able to compile Tibi:
    * [GNU Fortran (gfortran)](https://gcc.gnu.org/fortran): >= v8.3.0
    * [Intel Fortran Compiler (ifx)](https://software.intel.com/content/www/us/en/develop/tools/compilers/fortran-compilers.html): \>= v2023.2.0
    * [Intel Fortran Compiler Classic (ifort)](https://software.intel.com/content/www/us/en/develop/tools/compilers/fortran-compilers.html): \> v2021.2.0 (serial only; OpenMP build will fail)
* a C compiler (only used for the external socket interface)
* the BLAS and LAPACK libraries, e.g. from
  [MKL](https://software.intel.com/en-us/mkl) or
  [OpenBLAS](https://www.openblas.net), or the (slower)
  [Netlib](https://www.netlib.org) reference implementations
* the [LibXSMM](https://github.com/libxsmm/libxsmm) library (with its
  Fortran interface modules).
* (optional) an [ELPA](https://elpa.mpcdf.mpg.de/) installation
  (library and Fortran interface modules) with support for version 20180525
  of the ELPA API.
  <br>Note: only recent ELPA releases (v2022.05.001 and v2023.11.001)
  have been tested with Tibi so far.

In addition, the `tibi` Python module requires:

* [ASE](https://wiki.fysik.dtu.dk/ase/)

<br>

#### Compiling the Fortran code

Copy one of the `Makefile.include.*` files in `./arch`
to the root directory, e.g.:
```bash
cp arch/Makefile.include.gnu Makefile.include
```
and then modify `Makefile.include` as needed, e.g.:

* inserting the actual LibXSMM installation path
* selecting a different BLAS/LAPACK installation
* modifying the compiler optimization flags
* (optionally) inserting the actual ELPA installation path

Typing:
```bash
make help
```
will display a short description of the available targets and options.
Simply calling `make`, for example, should produce serial `tibi` and
`eiger` executables in the `./build/bin` folder. For an OpenMP build,
add the `OMP=1` option to `make`. For ELPA support, add `ELPA=1`.

<br>

#### Installing the Python module

The `tibi` Python module can be installed with e.g.
```shell
pip install .
```

When developing it can be more convenient to install in editable mode:
```shell
pip install -e .
```

You should then be able to e.g. import the `Tibi` and `Eiger`
calculator classes:
```python
from tibi import Tibi
from tibi import Eiger
```

For actual calculations (other than the test suite) it is also
needed to inform ASE about how to launch Tibi (and, if desired, Eiger).
Assuming that the executables are located in `/path/to/bin`
(e.g. `${PWD}/build/bin`) then you can either:

* set the `ASE_TIBI_COMMAND` environment variable to
  `"/path/to/bin/tibi PREFIX.in PREFIX.xyz > PREFIX.out"`
  and the `ASE_EIGER_COMMAND` environment variable to
  `"/path/to/bin/eiger PREFIX.in > PREFIX.out"`
* pass the same values to the `command` keyword argument
  when creating Tibi/Eiger calculator objects
* add the folder with the executable (`/path/to/bin` in this example)
  to your `${PATH}`

<br>

#### Running the test suite

After compiling the Fortran code and installing the Python module,
execute `make test` to run the tests.

**Note:** Tests 037 through 040 will be skipped unless the required parameters
have been generated first (see `tests/skf_files/p_o/README.md` and
`tests/skf_files/fe_dzp_mp/README.md`).

**Note:** For testing with `tibi` and `eiger` executables that are not located
in `build/bin`, the alternative location can be provided via the `BINDIR`
make option (e.g. `make BINDIR=/usr/bin/ test`).

<br>

### 2. Precompiled binaries

For selected platforms, Tibi \>= v1.1.0 can also be obtained via
the following [Conda](https://conda.org/) packages:

* [tibi](https://anaconda.org/conda-forge/tibi)
  for the precompiled, OpenMP-enabled `tibi` and `eiger` executables,
* [tibi-python](https://anaconda.org/conda-forge/tibi-python)
  for the Python interface.

For e.g. the linux-64 platform, the `tibi` package has two
[microarchitecture variants](
https://conda-forge.org/docs/maintainer/knowledge_base/#microarch):

* `microarch_level=1` (with build numbers 1xx):
  only uses instructions from the baseline x86-64-v1 level
  and so can run on any x86-64 CPU produced in the last few decades,
* `microarch_level=3` (with build numbers 3xx):
  also uses instructions from the x86-64-v3 level (FMA, AVX2, ...)
  and so can only run on x86-64 CPUs which implement these.

By default Conda will install the variant with the highest microarchitecture
level supported by the processor at hand. Keep in mind that building from
source may be required to reach optimal performance and that these Conda
packages are mostly intended as convenient ways to get started with Tibi.
