**Tibi** is an ab-initio tight-binding electronic structure code.
It is pronounced as **teebee**, with emphasis on the first syllable.


### Installation

Installation instructions can be found in the [online manual](
https://mvdb.gitlab.io/tibi/page/install.html) or the `INSTALL.md` file.


### Unleashing Tibi

The `tibi` executable requires two arguments: an input file and a geometry
file in *extxyz* format, e.g.:

```bash
./build/bin/tibi tibi.in tibi.xyz > tibi.out
```

After installing the `tibi` Python module (see the installation instructions),
also an [ASE](https://wiki.fysik.dtu.dk/ase) compatible FileIOCalculator
can be used, e.g.:

```python
from ase.build import bulk
from tibi import Tibi

atoms = bulk('Si')
calc = Tibi(basis_set={'Si': 'sp'},
            include_scf=True,
            kpts=[3, 3, 3],
            output_mulliken_subshell_chg=True,
            repulsion_2c_form='spline',
            tbpar_dir='./tests/skf_files/',
            )
atoms.set_calculator(calc)
print(atoms.get_potential_energy())
```

More examples can be found under `./tests` and in `./tutorials`.


### Background theory

A short theoretical background is given in the [online manual](
https://mvdb.gitlab.io/tibi/page/background.html).


### Current features

- [x] Up-to-three-center contributions to the zeroth-order Hamiltonian
      matrix elements
- [x] Up-to-three-center contributions to the repulsive energy
- [x] Up-to-two-center contributions to the U and W kernel matrix elements
- [x] Mulliken mapping to a (monopolar) auxiliary basis
- [x] Giese-York mapping to a (multipolar) auxiliary basis (up to quadrupoles)
- [x] Nonperiodic and fully periodic structures
- [x] LAPACK divide-and-conquer and [ELPA](https://elpa.mpcdf.mpg.de/)
      eigensolvers
- [x] Brillouin zone sampling
- [x] Spin polarization
- [x] Fermi-Dirac smearing
- [x] (r-)Pulay mixing
- [x] Analytical forces and stress tensor
- [x] Structural relaxation using LBFGS
- [x] Parallelization using OpenMP
- [x] Socket communication through the i-PI protocol
- [x] Python interface using [ASE](https://wiki.fysik.dtu.dk/ase)
- [x] Documentation using [FORD](
      https://github.com/Fortran-FOSS-Programmers/ford)
- [x] Tools for easier performance analysis (`Eiger`, `concur`)


### To-do list

- [ ] A more efficient and scalable approach to parameter file IO
      (improving on the current multitude of small text files)
