!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_spline) tibi_spline_trilinear
!! Submodule for trilinear interpolation for [[tibi_spline]]

implicit none


contains


pure module function interpolate_trilinear(x, y, z, flocal) result(fval)
    !! Returns the function value at the given (scaled) coordinates based
    !! on trilinear interpolation of the surrounding function values.
    real(dp), intent(in) :: x, y, z
        !! scaled x/y/z coordinates within the central voxel
    real(dp), dimension(2, 2, 2), intent(in) :: flocal
        !! set of surrounding function values
    real(dp) :: fval

    fval = (1._dp - x) * (1._dp - y) * (1._dp - z) * flocal(1, 1, 1) &
           + x * (1._dp - y) * (1._dp - z) * flocal(2, 1, 1) &
           + (1._dp - x) * y * (1._dp - z) * flocal(1, 2, 1) &
           + x * y * (1._dp - z) * flocal(2, 2, 1) &
           + (1._dp - x) * (1._dp - y) * z * flocal(1, 1, 2) &
           + x * (1._dp - y) * z * flocal(2, 1, 2) &
           + (1._dp - x) * y * z * flocal(1, 2, 2) &
           + x * y * z * flocal(2, 2, 2)
end function interpolate_trilinear


pure module function interpolate_trilinear_batch(x, y, z, n, flocal) &
                                                 result(fval)
    !! Variant of interpolate_trilinear for a multi-valued function.
    real(dp), intent(in) :: x, y, z
        !! scaled x/y/z coordinates within the central voxel
    integer, intent(in) :: n
        !! number of function values to evaluate
    real(dp), dimension(n, 2, 2, 2), intent(in) :: flocal
        !! set of surrounding function values
    real(dp), dimension(n) :: fval

    fval(:) = (1._dp - x) * (1._dp - y) * (1._dp - z) * flocal(:, 1, 1, 1) &
              + x * (1._dp - y) * (1._dp - z) * flocal(:, 2, 1, 1) &
              + (1._dp - x) * y * (1._dp - z) * flocal(:, 1, 2, 1) &
              + x * y * (1._dp - z) * flocal(:, 2, 2, 1) &
              + (1._dp - x) * (1._dp - y) * z * flocal(:, 1, 1, 2) &
              + x * (1._dp - y) * z * flocal(:, 2, 1, 2) &
              + (1._dp - x) * y * z * flocal(:, 1, 2, 2) &
              + x * y * z * flocal(:, 2, 2, 2)
end function interpolate_trilinear_batch


pure module function interpolate_trilinear_deriv(x, y, z, flocal) result(fder)
    !! Returns the function value derivatives at the given (scaled)
    !! coordinates based on trilinear interpolation of the surrounding
    !! function values.
    real(dp), intent(in) :: x, y, z
        !! scaled x/y/z coordinates within the central voxel
    real(dp), dimension(2, 2, 2), intent(in) :: flocal
        !! set of surrounding function values
    real(dp), dimension(3) :: fder

    fder(1) = - (1._dp - y) * (1._dp - z) * flocal(1, 1, 1) &
              + (1._dp - y) * (1._dp - z) * flocal(2, 1, 1) &
              - y * (1._dp - z) * flocal(1, 2, 1) &
              + y * (1._dp - z) * flocal(2, 2, 1) &
              - (1._dp - y) * z * flocal(1, 1, 2) &
              + (1._dp - y) * z * flocal(2, 1, 2) &
              - y * z * flocal(1, 2, 2) &
              + y * z * flocal(2, 2, 2)

    fder(2) = - (1._dp - x) * (1._dp - z) * flocal(1, 1, 1) &
              - x * (1._dp - z) * flocal(2, 1, 1) &
              + (1._dp - x) * (1._dp - z) * flocal(1, 2, 1) &
              + x * (1._dp - z) * flocal(2, 2, 1) &
              - (1._dp - x) * z * flocal(1, 1, 2) &
              - x * z * flocal(2, 1, 2) &
              + (1._dp - x) * z * flocal(1, 2, 2) &
              + x * z * flocal(2, 2, 2)

    fder(3) = - (1._dp - x) * (1._dp - y) * flocal(1, 1, 1) &
              - x * (1._dp - y) * flocal(2, 1, 1) &
              - (1._dp - x) * y * flocal(1, 2, 1) &
              - x * y * flocal(2, 2, 1) &
              + (1._dp - x) * (1._dp - y) * flocal(1, 1, 2) &
              + x * (1._dp - y) * flocal(2, 1, 2) &
              + (1._dp - x) * y * flocal(1, 2, 2) &
              + x * y * flocal(2, 2, 2)
end function interpolate_trilinear_deriv


pure module function interpolate_trilinear_deriv_batch(x, y, z, n, flocal) &
                                                       result(fder)
    !! Variant of interpolate_trilinear_deriv for a multi-valued function.
    real(dp), intent(in) :: x, y, z
        !! scaled x/y/z coordinates within the central voxel
    integer, intent(in) :: n
        !! number of function values
    real(dp), dimension(n, 2, 2, 2), intent(in) :: flocal
        !! set of surrounding function values
    real(dp), dimension(n, 3) :: fder

    fder(:, 3) = - (1._dp - y) * (1._dp - z) * flocal(:, 1, 1, 1) &
                 + (1._dp - y) * (1._dp - z) * flocal(:, 2, 1, 1) &
                 - y * (1._dp - z) * flocal(:, 1, 2, 1) &
                 + y * (1._dp - z) * flocal(:, 2, 2, 1) &
                 - (1._dp - y) * z * flocal(:, 1, 1, 2) &
                 + (1._dp - y) * z * flocal(:, 2, 1, 2) &
                 - y * z * flocal(:, 1, 2, 2) &
                 + y * z * flocal(:, 2, 2, 2)

    fder(:, 2) = - (1._dp - x) * (1._dp - z) * flocal(:, 1, 1, 1) &
                 - x * (1._dp - z) * flocal(:, 2, 1, 1) &
                 + (1._dp - x) * (1._dp - z) * flocal(:, 1, 2, 1) &
                 + x * (1._dp - z) * flocal(:, 2, 2, 1) &
                 - (1._dp - x) * z * flocal(:, 1, 1, 2) &
                 - x * z * flocal(:, 2, 1, 2) &
                 + (1._dp - x) * z * flocal(:, 1, 2, 2) &
                 + x * z * flocal(:, 2, 2, 2)

    fder(:, 1) = - (1._dp - x) * (1._dp - y) * flocal(:, 1, 1, 1) &
                 - x * (1._dp - y) * flocal(:, 2, 1, 1) &
                 - (1._dp - x) * y * flocal(:, 1, 2, 1) &
                 - x * y * flocal(:, 2, 2, 1) &
                 + (1._dp - x) * (1._dp - y) * flocal(:, 1, 1, 2) &
                 + x * (1._dp - y) * flocal(:, 2, 1, 2) &
                 + (1._dp - x) * y * flocal(:, 1, 2, 2) &
                 + x * y * flocal(:, 2, 2, 2)
end function interpolate_trilinear_deriv_batch

end submodule tibi_spline_trilinear
