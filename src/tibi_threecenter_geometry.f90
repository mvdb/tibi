 !-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_threecenter_geometry
!! Module for geometrical operations in three-center interactions

use tibi_constants, only: dp
use tibi_utils_math, only: cross
implicit none


real(dp), parameter :: EPS_RCM = 1e-8_dp
    !! threshold for normalization of CM vectors
    !! (only those of length above EPS_RCM will get normalized)


contains


pure function get_d_orbital_rotmat(transmat) result(dmat)
    !! Returns the rotation matrix for d-orbitals associated
    !! with the given coordinate transformation.
    !!
    !! Based on the [OpenMOPAC manual section on orbital rotations](
    !! http://openmopac.net/manual/rotate_atomic_orbitals_generic.html).
    real(dp), dimension(3, 3), intent(in) :: transmat
        !! Coordinate transformation matrix
    real(dp), dimension(5, 5) :: dmat

    associate(tm => transmat)
    dmat(1, 1) = tm(1, 1)*tm(2, 2) + tm(2, 1)*tm(1, 2)
    dmat(2, 1) = tm(2, 1)*tm(3, 2) + tm(3, 1)*tm(2, 2)
    dmat(3, 1) = tm(1, 1)*tm(3, 2) + tm(3, 1)*tm(1, 2)
    dmat(4, 1) = tm(1, 1)*tm(1, 2) - tm(2, 1)*tm(2, 2)
    dmat(5, 1) = (2._dp*tm(3, 1)*tm(3, 2) - tm(2, 2)*tm(2, 1) &
                 - tm(1, 1)*tm(1, 2))/sqrt(3._dp)

    dmat(1, 2) = tm(1, 2)*tm(2, 3) + tm(2, 2)*tm(1, 3)
    dmat(2, 2) = tm(2, 2)*tm(3, 3) + tm(3, 2)*tm(2, 3)
    dmat(3, 2) = tm(1, 2)*tm(3, 3) + tm(3, 2)*tm(1, 3)
    dmat(4, 2) = tm(1, 2)*tm(1, 3) - tm(2, 2)*tm(2, 3)
    dmat(5, 2) = (2._dp*tm(3, 3)*tm(3, 2) - tm(2, 2)*tm(2, 3) &
                 - tm(1, 2)*tm(1, 3))/sqrt(3._dp)

    dmat(1, 3) = tm(1, 1)*tm(2, 3) + tm(2, 1)*tm(1, 3)
    dmat(2, 3) = tm(2, 1)*tm(3, 3) + tm(3, 1)*tm(2, 3)
    dmat(3, 3) = tm(1, 1)*tm(3, 3) + tm(3, 1)*tm(1, 3)
    dmat(4, 3) = tm(1, 1)*tm(1, 3) - tm(2, 1)*tm(2, 3)
    dmat(5, 3) = (2._dp*tm(3, 1)*tm(3, 3) - tm(2, 1)*tm(2, 3) &
                 - tm(1, 1)*tm(1, 3))/sqrt(3._dp)

    dmat(1, 4) = (tm(1, 1)*tm(2, 1) - tm(1, 2)*tm(2, 2))
    dmat(2, 4) = (tm(2, 1)*tm(3, 1) - tm(2, 2)*tm(3, 2))
    dmat(3, 4) = (tm(1, 1)*tm(3, 1) - tm(1, 2)*tm(3, 2))
    dmat(4, 4) = (tm(1, 1)**2 - tm(1, 2)**2 - tm(2, 1)**2 + tm(2, 2)**2)/2._dp
    dmat(5, 4) = (2._dp*tm(3, 1)**2 - tm(2, 1)**2 - tm(1, 1)**2 &
                 - 2._dp*tm(3, 2)**2 + tm(2, 2)**2 + tm(1, 2)**2)/sqrt(12._dp)

    dmat(1, 5) = (2._dp*tm(1, 3)*tm(2, 3) - tm(2, 2)*tm(1, 2) &
                 - tm(1, 1)*tm(2, 1))/sqrt(3._dp)
    dmat(2, 5) = (2._dp*tm(2, 3)*tm(3, 3) - tm(2, 2)*tm(3, 2) &
                 - tm(3, 1)*tm(2, 1))/sqrt(3._dp)
    dmat(3, 5) = (2._dp*tm(1, 3)*tm(3, 3) - tm(1, 2)*tm(3, 2) &
                 - tm(1, 1)*tm(3, 1))/sqrt(3._dp)
    dmat(4, 5) = (2._dp*tm(1, 3)**2 - tm(1, 2)**2 - tm(1, 1)**2 &
                 - 2._dp*tm(2, 3)**2 + tm(2, 2)**2 + tm(2, 1)**2)/sqrt(12._dp)
    dmat(5, 5) = (4._dp*tm(3, 3)**2 - 2._dp*(tm(2, 3)**2 + tm(1, 3)**2 &
                 + tm(3, 2)**2 + tm(3, 1)**2) + tm(2, 2)**2 + tm(1, 2)**2 &
                 + tm(2, 1)**2 + tm(1, 1)**2)/6._dp
    end associate
end function get_d_orbital_rotmat


pure function get_d_orbital_rotmat_deriv(transmat, transmat_deriv) &
                                         result(dmat_deriv)
    !! Returns the derivative of the rotation matrix for d-orbitals associated
    !! with the given coordinate transformation and its derivatives.
    !!
    !! Generated with a little help from SymPy.
    real(dp), dimension(3, 3), intent(in) :: transmat
        !! coordinate transformation matrix
    real(dp), dimension(3, 3, 3), intent(in) :: transmat_deriv
        !! derivatives of the coordinate transformation matrix
    real(dp), dimension(5, 5, 3) :: dmat_deriv

    associate(tm => transmat)
    associate(dtm => transmat_deriv)
    dmat_deriv(1, 1, 1) = tm(1, 1)*dtm(2, 2, 1) + tm(2, 1)*dtm(1, 2, 1) &
                          + tm(1, 2)*dtm(2, 1, 1) + tm(2, 2)*dtm(1, 1, 1)
    dmat_deriv(1, 2, 1) = tm(1, 2)*dtm(2, 3, 1) + tm(2, 2)*dtm(1, 3, 1) &
                          + tm(1, 3)*dtm(2, 2, 1) + tm(2, 3)*dtm(1, 2, 1)
    dmat_deriv(1, 3, 1) = tm(1, 1)*dtm(2, 3, 1) + tm(2, 1)*dtm(1, 3, 1) &
                          + tm(1, 3)*dtm(2, 1, 1) + tm(2, 3)*dtm(1, 1, 1)
    dmat_deriv(1, 4, 1) = tm(1, 1)*dtm(2, 1, 1) + tm(2, 1)*dtm(1, 1, 1) &
                          - tm(1, 2)*dtm(2, 2, 1) - tm(2, 2)*dtm(1, 2, 1)
    dmat_deriv(1, 5, 1) = sqrt(3._dp)*(-tm(1, 1)*dtm(2, 1, 1) - tm(2, &
                          1)*dtm(1, 1, 1) - tm(1, 2)*dtm(2, 2, 1) - tm(2, &
                          2)*dtm(1, 2, 1) + 2*tm(1, 3)*dtm(2, 3, 1) + 2*tm(2, &
                          3)*dtm(1, 3, 1))/3
    dmat_deriv(2, 1, 1) = tm(2, 1)*dtm(3, 2, 1) + tm(3, 1)*dtm(2, 2, 1) &
                          + tm(2, 2)*dtm(3, 1, 1) + tm(3, 2)*dtm(2, 1, 1)
    dmat_deriv(2, 2, 1) = tm(2, 2)*dtm(3, 3, 1) + tm(3, 2)*dtm(2, 3, 1) &
                          + tm(2, 3)*dtm(3, 2, 1) + tm(3, 3)*dtm(2, 2, 1)
    dmat_deriv(2, 3, 1) = tm(2, 1)*dtm(3, 3, 1) + tm(3, 1)*dtm(2, 3, 1) &
                          + tm(2, 3)*dtm(3, 1, 1) + tm(3, 3)*dtm(2, 1, 1)
    dmat_deriv(2, 4, 1) = tm(2, 1)*dtm(3, 1, 1) + tm(3, 1)*dtm(2, 1, 1) &
                          - tm(2, 2)*dtm(3, 2, 1) - tm(3, 2)*dtm(2, 2, 1)
    dmat_deriv(2, 5, 1) = sqrt(3._dp)*(-tm(2, 1)*dtm(3, 1, 1) - tm(3, &
                          1)*dtm(2, 1, 1) - tm(2, 2)*dtm(3, 2, 1) - tm(3, &
                          2)*dtm(2, 2, 1) + 2*tm(2, 3)*dtm(3, 3, 1) + 2*tm(3, &
                          3)*dtm(2, 3, 1))/3
    dmat_deriv(3, 1, 1) = tm(1, 1)*dtm(3, 2, 1) + tm(3, 1)*dtm(1, 2, 1) &
                          + tm(1, 2)*dtm(3, 1, 1) + tm(3, 2)*dtm(1, 1, 1)
    dmat_deriv(3, 2, 1) = tm(1, 2)*dtm(3, 3, 1) + tm(3, 2)*dtm(1, 3, 1) &
                          + tm(1, 3)*dtm(3, 2, 1) + tm(3, 3)*dtm(1, 2, 1)
    dmat_deriv(3, 3, 1) = tm(1, 1)*dtm(3, 3, 1) + tm(3, 1)*dtm(1, 3, 1) &
                          + tm(1, 3)*dtm(3, 1, 1) + tm(3, 3)*dtm(1, 1, 1)
    dmat_deriv(3, 4, 1) = tm(1, 1)*dtm(3, 1, 1) + tm(3, 1)*dtm(1, 1, 1) &
                          - tm(1, 2)*dtm(3, 2, 1) - tm(3, 2)*dtm(1, 2, 1)
    dmat_deriv(3, 5, 1) = sqrt(3._dp)*(-tm(1, 1)*dtm(3, 1, 1) - tm(3, &
                          1)*dtm(1, 1, 1) - tm(1, 2)*dtm(3, 2, 1) - tm(3, &
                          2)*dtm(1, 2, 1) + 2*tm(1, 3)*dtm(3, 3, 1) + 2*tm(3, &
                          3)*dtm(1, 3, 1))/3
    dmat_deriv(4, 1, 1) = tm(1, 1)*dtm(1, 2, 1) - tm(2, 1)*dtm(2, 2, 1) &
                          + tm(1, 2)*dtm(1, 1, 1) - tm(2, 2)*dtm(2, 1, 1)
    dmat_deriv(4, 2, 1) = tm(1, 2)*dtm(1, 3, 1) - tm(2, 2)*dtm(2, 3, 1) &
                          + tm(1, 3)*dtm(1, 2, 1) - tm(2, 3)*dtm(2, 2, 1)
    dmat_deriv(4, 3, 1) = tm(1, 1)*dtm(1, 3, 1) - tm(2, 1)*dtm(2, 3, 1) &
                          + tm(1, 3)*dtm(1, 1, 1) - tm(2, 3)*dtm(2, 1, 1)
    dmat_deriv(4, 4, 1) = tm(1, 1)*dtm(1, 1, 1) - tm(2, 1)*dtm(2, 1, 1) &
                          - tm(1, 2)*dtm(1, 2, 1) + tm(2, 2)*dtm(2, 2, 1)
    dmat_deriv(4, 5, 1) = sqrt(3._dp)*(-2*tm(1, 1)*dtm(1, 1, 1) + 2*tm(2, &
                          1)*dtm(2, 1, 1) - 2*tm(1, 2)*dtm(1, 2, 1) + 2*tm(2, &
                          2)*dtm(2, 2, 1) + 4*tm(1, 3)*dtm(1, 3, 1) - 4*tm(2, &
                          3)*dtm(2, 3, 1))/6
    dmat_deriv(5, 1, 1) = sqrt(3._dp)*(-tm(1, 1)*dtm(1, 2, 1) - tm(2, &
                          1)*dtm(2, 2, 1) + 2*tm(3, 1)*dtm(3, 2, 1) - tm(1, &
                          2)*dtm(1, 1, 1) - tm(2, 2)*dtm(2, 1, 1) + 2*tm(3, &
                          2)*dtm(3, 1, 1))/3
    dmat_deriv(5, 2, 1) = sqrt(3._dp)*(-tm(1, 2)*dtm(1, 3, 1) - tm(2, &
                          2)*dtm(2, 3, 1) + 2*tm(3, 2)*dtm(3, 3, 1) - tm(1, &
                          3)*dtm(1, 2, 1) - tm(2, 3)*dtm(2, 2, 1) + 2*tm(3, &
                          3)*dtm(3, 2, 1))/3
    dmat_deriv(5, 3, 1) = sqrt(3._dp)*(-tm(1, 1)*dtm(1, 3, 1) - tm(2, &
                          1)*dtm(2, 3, 1) + 2*tm(3, 1)*dtm(3, 3, 1) - tm(1, &
                          3)*dtm(1, 1, 1) - tm(2, 3)*dtm(2, 1, 1) + 2*tm(3, &
                          3)*dtm(3, 1, 1))/3
    dmat_deriv(5, 4, 1) = sqrt(3._dp)*(-2*tm(1, 1)*dtm(1, 1, 1) - 2*tm(2, &
                          1)*dtm(2, 1, 1) + 4*tm(3, 1)*dtm(3, 1, 1) + 2*tm(1, &
                          2)*dtm(1, 2, 1) + 2*tm(2, 2)*dtm(2, 2, 1) - 4*tm(3, &
                          2)*dtm(3, 2, 1))/6
    dmat_deriv(5, 5, 1) = tm(1, 1)*dtm(1, 1, 1)/3 + tm(2, 1)*dtm(2, 1, 1)/3 &
                          - 2*tm(3, 1)*dtm(3, 1, 1)/3 + tm(1, 2)*dtm(1, 2, &
                          1)/3 + tm(2, 2)*dtm(2, 2, 1)/3 - 2*tm(3, 2)*dtm(3, &
                          2, 1)/3 - 2*tm(1, 3)*dtm(1, 3, 1)/3 - 2*tm(2, &
                          3)*dtm(2, 3, 1)/3 + 4*tm(3, 3)*dtm(3, 3, 1)/3
    dmat_deriv(1, 1, 2) = tm(1, 1)*dtm(2, 2, 2) + tm(2, 1)*dtm(1, 2, 2) &
                          + tm(1, 2)*dtm(2, 1, 2) + tm(2, 2)*dtm(1, 1, 2)
    dmat_deriv(1, 2, 2) = tm(1, 2)*dtm(2, 3, 2) + tm(2, 2)*dtm(1, 3, 2) &
                          + tm(1, 3)*dtm(2, 2, 2) + tm(2, 3)*dtm(1, 2, 2)
    dmat_deriv(1, 3, 2) = tm(1, 1)*dtm(2, 3, 2) + tm(2, 1)*dtm(1, 3, 2) &
                          + tm(1, 3)*dtm(2, 1, 2) + tm(2, 3)*dtm(1, 1, 2)
    dmat_deriv(1, 4, 2) = tm(1, 1)*dtm(2, 1, 2) + tm(2, 1)*dtm(1, 1, 2) &
                          - tm(1, 2)*dtm(2, 2, 2) - tm(2, 2)*dtm(1, 2, 2)
    dmat_deriv(1, 5, 2) = sqrt(3._dp)*(-tm(1, 1)*dtm(2, 1, 2) - tm(2, &
                          1)*dtm(1, 1, 2) - tm(1, 2)*dtm(2, 2, 2) - tm(2, &
                          2)*dtm(1, 2, 2) + 2*tm(1, 3)*dtm(2, 3, 2) + 2*tm(2, &
                          3)*dtm(1, 3, 2))/3
    dmat_deriv(2, 1, 2) = tm(2, 1)*dtm(3, 2, 2) + tm(3, 1)*dtm(2, 2, 2) &
                          + tm(2, 2)*dtm(3, 1, 2) + tm(3, 2)*dtm(2, 1, 2)
    dmat_deriv(2, 2, 2) = tm(2, 2)*dtm(3, 3, 2) + tm(3, 2)*dtm(2, 3, 2) &
                          + tm(2, 3)*dtm(3, 2, 2) + tm(3, 3)*dtm(2, 2, 2)
    dmat_deriv(2, 3, 2) = tm(2, 1)*dtm(3, 3, 2) + tm(3, 1)*dtm(2, 3, 2) &
                          + tm(2, 3)*dtm(3, 1, 2) + tm(3, 3)*dtm(2, 1, 2)
    dmat_deriv(2, 4, 2) = tm(2, 1)*dtm(3, 1, 2) + tm(3, 1)*dtm(2, 1, 2) &
                          - tm(2, 2)*dtm(3, 2, 2) - tm(3, 2)*dtm(2, 2, 2)
    dmat_deriv(2, 5, 2) = sqrt(3._dp)*(-tm(2, 1)*dtm(3, 1, 2) - tm(3, &
                          1)*dtm(2, 1, 2) - tm(2, 2)*dtm(3, 2, 2) - tm(3, &
                          2)*dtm(2, 2, 2) + 2*tm(2, 3)*dtm(3, 3, 2) + 2*tm(3, &
                          3)*dtm(2, 3, 2))/3
    dmat_deriv(3, 1, 2) = tm(1, 1)*dtm(3, 2, 2) + tm(3, 1)*dtm(1, 2, 2) &
                          + tm(1, 2)*dtm(3, 1, 2) + tm(3, 2)*dtm(1, 1, 2)
    dmat_deriv(3, 2, 2) = tm(1, 2)*dtm(3, 3, 2) + tm(3, 2)*dtm(1, 3, 2) &
                          + tm(1, 3)*dtm(3, 2, 2) + tm(3, 3)*dtm(1, 2, 2)
    dmat_deriv(3, 3, 2) = tm(1, 1)*dtm(3, 3, 2) + tm(3, 1)*dtm(1, 3, 2) &
                          + tm(1, 3)*dtm(3, 1, 2) + tm(3, 3)*dtm(1, 1, 2)
    dmat_deriv(3, 4, 2) = tm(1, 1)*dtm(3, 1, 2) + tm(3, 1)*dtm(1, 1, 2) &
                          - tm(1, 2)*dtm(3, 2, 2) - tm(3, 2)*dtm(1, 2, 2)
    dmat_deriv(3, 5, 2) = sqrt(3._dp)*(-tm(1, 1)*dtm(3, 1, 2) - tm(3, &
                          1)*dtm(1, 1, 2) - tm(1, 2)*dtm(3, 2, 2) - tm(3, &
                          2)*dtm(1, 2, 2) + 2*tm(1, 3)*dtm(3, 3, 2) + 2*tm(3, &
                          3)*dtm(1, 3, 2))/3
    dmat_deriv(4, 1, 2) = tm(1, 1)*dtm(1, 2, 2) - tm(2, 1)*dtm(2, 2, 2) &
                          + tm(1, 2)*dtm(1, 1, 2) - tm(2, 2)*dtm(2, 1, 2)
    dmat_deriv(4, 2, 2) = tm(1, 2)*dtm(1, 3, 2) - tm(2, 2)*dtm(2, 3, 2) &
                          + tm(1, 3)*dtm(1, 2, 2) - tm(2, 3)*dtm(2, 2, 2)
    dmat_deriv(4, 3, 2) = tm(1, 1)*dtm(1, 3, 2) - tm(2, 1)*dtm(2, 3, 2) &
                          + tm(1, 3)*dtm(1, 1, 2) - tm(2, 3)*dtm(2, 1, 2)
    dmat_deriv(4, 4, 2) = tm(1, 1)*dtm(1, 1, 2) - tm(2, 1)*dtm(2, 1, 2) &
                          - tm(1, 2)*dtm(1, 2, 2) + tm(2, 2)*dtm(2, 2, 2)
    dmat_deriv(4, 5, 2) = sqrt(3._dp)*(-2*tm(1, 1)*dtm(1, 1, 2) + 2*tm(2, &
                          1)*dtm(2, 1, 2) - 2*tm(1, 2)*dtm(1, 2, 2) + 2*tm(2, &
                          2)*dtm(2, 2, 2) + 4*tm(1, 3)*dtm(1, 3, 2) - 4*tm(2, &
                          3)*dtm(2, 3, 2))/6
    dmat_deriv(5, 1, 2) = sqrt(3._dp)*(-tm(1, 1)*dtm(1, 2, 2) - tm(2, &
                          1)*dtm(2, 2, 2) + 2*tm(3, 1)*dtm(3, 2, 2) - tm(1, &
                          2)*dtm(1, 1, 2) - tm(2, 2)*dtm(2, 1, 2) + 2*tm(3, &
                          2)*dtm(3, 1, 2))/3
    dmat_deriv(5, 2, 2) = sqrt(3._dp)*(-tm(1, 2)*dtm(1, 3, 2) - tm(2, &
                          2)*dtm(2, 3, 2) + 2*tm(3, 2)*dtm(3, 3, 2) - tm(1, &
                          3)*dtm(1, 2, 2) - tm(2, 3)*dtm(2, 2, 2) + 2*tm(3, &
                          3)*dtm(3, 2, 2))/3
    dmat_deriv(5, 3, 2) = sqrt(3._dp)*(-tm(1, 1)*dtm(1, 3, 2) - tm(2, &
                          1)*dtm(2, 3, 2) + 2*tm(3, 1)*dtm(3, 3, 2) - tm(1, &
                          3)*dtm(1, 1, 2) - tm(2, 3)*dtm(2, 1, 2) + 2*tm(3, &
                          3)*dtm(3, 1, 2))/3
    dmat_deriv(5, 4, 2) = sqrt(3._dp)*(-2*tm(1, 1)*dtm(1, 1, 2) - 2*tm(2, &
                          1)*dtm(2, 1, 2) + 4*tm(3, 1)*dtm(3, 1, 2) + 2*tm(1, &
                          2)*dtm(1, 2, 2) + 2*tm(2, 2)*dtm(2, 2, 2) - 4*tm(3, &
                          2)*dtm(3, 2, 2))/6
    dmat_deriv(5, 5, 2) = tm(1, 1)*dtm(1, 1, 2)/3 + tm(2, 1)*dtm(2, 1, 2)/3 &
                          - 2*tm(3, 1)*dtm(3, 1, 2)/3 + tm(1, 2)*dtm(1, 2, &
                          2)/3 + tm(2, 2)*dtm(2, 2, 2)/3 - 2*tm(3, 2)*dtm(3, &
                          2, 2)/3 - 2*tm(1, 3)*dtm(1, 3, 2)/3 - 2*tm(2, &
                          3)*dtm(2, 3, 2)/3 + 4*tm(3, 3)*dtm(3, 3, 2)/3
    dmat_deriv(1, 1, 3) = tm(1, 1)*dtm(2, 2, 3) + tm(2, 1)*dtm(1, 2, 3) &
                          + tm(1, 2)*dtm(2, 1, 3) + tm(2, 2)*dtm(1, 1, 3)
    dmat_deriv(1, 2, 3) = tm(1, 2)*dtm(2, 3, 3) + tm(2, 2)*dtm(1, 3, 3) &
                          + tm(1, 3)*dtm(2, 2, 3) + tm(2, 3)*dtm(1, 2, 3)
    dmat_deriv(1, 3, 3) = tm(1, 1)*dtm(2, 3, 3) + tm(2, 1)*dtm(1, 3, 3) &
                          + tm(1, 3)*dtm(2, 1, 3) + tm(2, 3)*dtm(1, 1, 3)
    dmat_deriv(1, 4, 3) = tm(1, 1)*dtm(2, 1, 3) + tm(2, 1)*dtm(1, 1, 3) &
                          - tm(1, 2)*dtm(2, 2, 3) - tm(2, 2)*dtm(1, 2, 3)
    dmat_deriv(1, 5, 3) = sqrt(3._dp)*(-tm(1, 1)*dtm(2, 1, 3) - tm(2, &
                          1)*dtm(1, 1, 3) - tm(1, 2)*dtm(2, 2, 3) - tm(2, &
                          2)*dtm(1, 2, 3) + 2*tm(1, 3)*dtm(2, 3, 3) + 2*tm(2, &
                          3)*dtm(1, 3, 3))/3
    dmat_deriv(2, 1, 3) = tm(2, 1)*dtm(3, 2, 3) + tm(3, 1)*dtm(2, 2, 3) &
                          + tm(2, 2)*dtm(3, 1, 3) + tm(3, 2)*dtm(2, 1, 3)
    dmat_deriv(2, 2, 3) = tm(2, 2)*dtm(3, 3, 3) + tm(3, 2)*dtm(2, 3, 3) &
                          + tm(2, 3)*dtm(3, 2, 3) + tm(3, 3)*dtm(2, 2, 3)
    dmat_deriv(2, 3, 3) = tm(2, 1)*dtm(3, 3, 3) + tm(3, 1)*dtm(2, 3, 3) &
                          + tm(2, 3)*dtm(3, 1, 3) + tm(3, 3)*dtm(2, 1, 3)
    dmat_deriv(2, 4, 3) = tm(2, 1)*dtm(3, 1, 3) + tm(3, 1)*dtm(2, 1, 3) &
                          - tm(2, 2)*dtm(3, 2, 3) - tm(3, 2)*dtm(2, 2, 3)
    dmat_deriv(2, 5, 3) = sqrt(3._dp)*(-tm(2, 1)*dtm(3, 1, 3) - tm(3, &
                          1)*dtm(2, 1, 3) - tm(2, 2)*dtm(3, 2, 3) - tm(3, &
                          2)*dtm(2, 2, 3) + 2*tm(2, 3)*dtm(3, 3, 3) + 2*tm(3, &
                          3)*dtm(2, 3, 3))/3
    dmat_deriv(3, 1, 3) = tm(1, 1)*dtm(3, 2, 3) + tm(3, 1)*dtm(1, 2, 3) &
                          + tm(1, 2)*dtm(3, 1, 3) + tm(3, 2)*dtm(1, 1, 3)
    dmat_deriv(3, 2, 3) = tm(1, 2)*dtm(3, 3, 3) + tm(3, 2)*dtm(1, 3, 3) &
                          + tm(1, 3)*dtm(3, 2, 3) + tm(3, 3)*dtm(1, 2, 3)
    dmat_deriv(3, 3, 3) = tm(1, 1)*dtm(3, 3, 3) + tm(3, 1)*dtm(1, 3, 3) &
                          + tm(1, 3)*dtm(3, 1, 3) + tm(3, 3)*dtm(1, 1, 3)
    dmat_deriv(3, 4, 3) = tm(1, 1)*dtm(3, 1, 3) + tm(3, 1)*dtm(1, 1, 3) &
                          - tm(1, 2)*dtm(3, 2, 3) - tm(3, 2)*dtm(1, 2, 3)
    dmat_deriv(3, 5, 3) = sqrt(3._dp)*(-tm(1, 1)*dtm(3, 1, 3) - tm(3, &
                          1)*dtm(1, 1, 3) - tm(1, 2)*dtm(3, 2, 3) - tm(3, &
                          2)*dtm(1, 2, 3) + 2*tm(1, 3)*dtm(3, 3, 3) + 2*tm(3, &
                          3)*dtm(1, 3, 3))/3
    dmat_deriv(4, 1, 3) = tm(1, 1)*dtm(1, 2, 3) - tm(2, 1)*dtm(2, 2, 3) &
                          + tm(1, 2)*dtm(1, 1, 3) - tm(2, 2)*dtm(2, 1, 3)
    dmat_deriv(4, 2, 3) = tm(1, 2)*dtm(1, 3, 3) - tm(2, 2)*dtm(2, 3, 3) &
                          + tm(1, 3)*dtm(1, 2, 3) - tm(2, 3)*dtm(2, 2, 3)
    dmat_deriv(4, 3, 3) = tm(1, 1)*dtm(1, 3, 3) - tm(2, 1)*dtm(2, 3, 3) &
                          + tm(1, 3)*dtm(1, 1, 3) - tm(2, 3)*dtm(2, 1, 3)
    dmat_deriv(4, 4, 3) = tm(1, 1)*dtm(1, 1, 3) - tm(2, 1)*dtm(2, 1, 3) &
                          - tm(1, 2)*dtm(1, 2, 3) + tm(2, 2)*dtm(2, 2, 3)
    dmat_deriv(4, 5, 3) = sqrt(3._dp)*(-2*tm(1, 1)*dtm(1, 1, 3) + 2*tm(2, &
                          1)*dtm(2, 1, 3) - 2*tm(1, 2)*dtm(1, 2, 3) + 2*tm(2, &
                          2)*dtm(2, 2, 3) + 4*tm(1, 3)*dtm(1, 3, 3) - 4*tm(2, &
                          3)*dtm(2, 3, 3))/6
    dmat_deriv(5, 1, 3) = sqrt(3._dp)*(-tm(1, 1)*dtm(1, 2, 3) - tm(2, &
                          1)*dtm(2, 2, 3) + 2*tm(3, 1)*dtm(3, 2, 3) - tm(1, &
                          2)*dtm(1, 1, 3) - tm(2, 2)*dtm(2, 1, 3) + 2*tm(3, &
                          2)*dtm(3, 1, 3))/3
    dmat_deriv(5, 2, 3) = sqrt(3._dp)*(-tm(1, 2)*dtm(1, 3, 3) - tm(2, &
                          2)*dtm(2, 3, 3) + 2*tm(3, 2)*dtm(3, 3, 3) - tm(1, &
                          3)*dtm(1, 2, 3) - tm(2, 3)*dtm(2, 2, 3) + 2*tm(3, &
                          3)*dtm(3, 2, 3))/3
    dmat_deriv(5, 3, 3) = sqrt(3._dp)*(-tm(1, 1)*dtm(1, 3, 3) - tm(2, &
                          1)*dtm(2, 3, 3) + 2*tm(3, 1)*dtm(3, 3, 3) - tm(1, &
                          3)*dtm(1, 1, 3) - tm(2, 3)*dtm(2, 1, 3) + 2*tm(3, &
                          3)*dtm(3, 1, 3))/3
    dmat_deriv(5, 4, 3) = sqrt(3._dp)*(-2*tm(1, 1)*dtm(1, 1, 3) - 2*tm(2, &
                          1)*dtm(2, 1, 3) + 4*tm(3, 1)*dtm(3, 1, 3) + 2*tm(1, &
                          2)*dtm(1, 2, 3) + 2*tm(2, 2)*dtm(2, 2, 3) - 4*tm(3, &
                          2)*dtm(3, 2, 3))/6
    dmat_deriv(5, 5, 3) = tm(1, 1)*dtm(1, 1, 3)/3 + tm(2, 1)*dtm(2, 1, 3)/3 &
                          - 2*tm(3, 1)*dtm(3, 1, 3)/3 + tm(1, 2)*dtm(1, 2, &
                          3)/3 + tm(2, 2)*dtm(2, 2, 3)/3 - 2*tm(3, 2)*dtm(3, &
                          2, 3)/3 - 2*tm(1, 3)*dtm(1, 3, 3)/3 - 2*tm(2, &
                          3)*dtm(2, 3, 3)/3 + 4*tm(3, 3)*dtm(3, 3, 3)/3
    end associate
    end associate
end function get_d_orbital_rotmat_deriv


pure function get_rAB_derivs(vec_AB) result(drAB)
    !! Returns the derivatives of the AB distance with respect
    !! to the positions of A and B.
    real(dp), dimension(3), intent(in) :: vec_AB
        !! AB unit vector
    real(dp), dimension(3, 2) :: drAB

    drAB(:, 1) = -vec_AB(:)
    drAB(:, 2) = vec_AB(:)
end function get_rAB_derivs


pure subroutine get_rCM_and_vec_CM(rAB, vec_AB, rAC, vec_AC, rCM, vec_CM)
    ! Calculates rCM and vec_CM from rAB, vec_AB, rAC and vec_AC.
    real(dp), intent(in) :: rAC, rAB
    real(dp), dimension(3), intent(in) :: vec_AC, vec_AB
    real(dp), intent(out) :: rCM
    real(dp), dimension(3), intent(out) :: vec_CM

    vec_CM = rAC * vec_AC(:) - 0.5_dp * rAB * vec_AB(:)
    rCM = norm2(vec_CM)
    if (rCM > EPS_RCM) vec_CM = vec_CM / rCM
end subroutine get_rCM_and_vec_CM


pure function get_rCM_derivs(vec_CM) result(drCM)
    !! Returns the derivatives of the CM distance with respect
    !! to the positions of A and B (M = middle of A and B).
    real(dp), dimension(3), intent(in) :: vec_CM
        !! CM unit vector
    real(dp), dimension(3, 2) :: drCM

    drCM(:, 1) = -0.5_dp * vec_CM(:)
    drCM(:, 2) = drCM(:, 1)
end function get_rCM_derivs


pure function get_rotation_matrix(vec_AB, vec_CM) result(rotmat)
    !! Returns the rotation matrix for the change to the new basis
    !! where AB lies along the z-axis and CM lies in the XZ-plane.
    real(dp), dimension(3), intent(in) :: vec_AB
        !! Normalized AB vector
    real(dp), dimension(3), intent(in) :: vec_CM
        !! Normalized CM vector
    real(dp), dimension(3, 3) :: rotmat

    integer :: i
    real(dp), parameter :: eps_norm = 1e-10_dp
    real(dp) :: n

    rotmat(:, 3) = vec_AB

    rotmat(:, 2) = cross(vec_CM, vec_AB)
    n = norm2(rotmat(:, 2))
    if (n < eps_norm) then
        ! AB and CM vectors are parallel
        ! Pick a vector that is perpendicular to them
        do i = 1, 3
            rotmat(:, 2) = 0._dp
            rotmat(i, 2) = 1._dp
            rotmat(:, 2) = cross(rotmat(:, 2), vec_AB)
            n = norm2(rotmat(:, 2))
            if (n > eps_norm) exit
        end do
    end if
    rotmat(:, 2) = rotmat(:, 2) / n

    rotmat(:, 1) = cross(rotmat(:, 2), rotmat(:, 3))
    rotmat(:, 1) = rotmat(:, 1) / norm2(rotmat(:, 1))

    rotmat = transpose(rotmat)
end function get_rotation_matrix


pure function get_rotation_matrix_deriv(vec_AB, dvAB, vec_CM, dvCM, index) &
                                        result(dmdx)
    !! Returns the derivatives of the rotation matrix for the change to the
    !! new basis where AB lies along the z-axis and CM lies in the XZ-plane.
    !! The derivatives are with respect to the same coordinates as the
    !! given derivatives of the AB and CM unit vectors.
    real(dp), dimension(3), intent(in) :: vec_AB
        !! Normalized AB vector
    real(dp), dimension(3, 3), intent(in) :: dvAB
        !! Derivatives of the AB unit vector
    real(dp), dimension(3), intent(in) :: vec_CM
        !! Normalized CM vector
    real(dp), dimension(3, 3), intent(in) :: dvCM
        !! Derivatives of the CM unit vector
    integer, intent(in) :: index
        !! index of the atom with respect to which the derivatives are being
        !! computed (1/2/3=A/B/C)
    real(dp), dimension(3, 3, 3) :: dmdx
        !! Derivative matrices (:, :, dx/dy/dz)

    integer :: i
    real(dp), parameter :: eps_norm = 1e-10_dp
    real(dp) :: n
    real(dp), dimension(3) :: dudx, dudy, dudz, vec

    dmdx(:, 3, :) = dvAB(:, :)

    vec = cross(vec_CM, vec_AB)
    n = norm2(vec)
    if (n < eps_norm) then
        ! AB and CM vectors are parallel
        ! Pick a vector that is perpendicular to them
        do i = 1, 3
            vec(:) = 0._dp
            vec(i) = 1._dp
            dudx = cross(vec, dmdx(:, 3, 1))
            dudy = cross(vec, dmdx(:, 3, 2))
            dudz = cross(vec, dmdx(:, 3, 3))

            vec = cross(vec, vec_AB)
            n = norm2(vec)
            if (n > eps_norm) then
                ! Found suitable vector; now try to get numerical derivatives
                dudx = dudx + get_dvec_x_vAB(i, 1)
                dudy = dudx + get_dvec_x_vAB(i, 2)
                dudz = dudx + get_dvec_x_vAB(i, 3)
                exit
            end if
        end do
    else
        dudx = cross(dvCM(:, 1), vec_AB) + cross(vec_CM, dmdx(:, 3, 1))
        dudy = cross(dvCM(:, 2), vec_AB) + cross(vec_CM, dmdx(:, 3, 2))
        dudz = cross(dvCM(:, 3), vec_AB) + cross(vec_CM, dmdx(:, 3, 3))
    end if

    vec(:) = vec(:) / n
    dmdx(:, 2, 1) = (dudx(:) - vec(:) * dot_product(vec, dudx)) / n
    dmdx(:, 2, 2) = (dudy(:) - vec(:) * dot_product(vec, dudy)) / n
    dmdx(:, 2, 3) = (dudz(:) - vec(:) * dot_product(vec, dudz)) / n

    dudx = cross(dmdx(:, 2, 1), vec_AB) + cross(vec, dmdx(:, 3, 1))
    dudy = cross(dmdx(:, 2, 2), vec_AB) + cross(vec, dmdx(:, 3, 2))
    dudz = cross(dmdx(:, 2, 3), vec_AB) + cross(vec, dmdx(:, 3, 3))
    vec = cross(vec, vec_AB)
    n = norm2(vec)

    vec(:) = vec(:) / n
    dmdx(:, 1, 1) = (dudx(:) - vec(:) * dot_product(vec, dudx)) / n
    dmdx(:, 1, 2) = (dudy(:) - vec(:) * dot_product(vec, dudy)) / n
    dmdx(:, 1, 3) = (dudz(:) - vec(:) * dot_product(vec, dudz)) / n

    do i = 1, 3
        dmdx(:, :, i) = transpose(dmdx(:, :, i))
    end do


    contains


    pure function get_dvec_x_vAB(i, j) result(prod)
        ! Returns d(vCM) x vAB in the case where vCM // rAB.
        integer, intent(in) :: i, j
        real(dp), dimension(3) :: prod

        real(dp), parameter :: eps_step = 1e-6_dp
        real(dp) :: n
        real(dp), dimension(3) :: dvec

        dvec(:) = vec_CM(:)
        if (index == 3) then
            dvec(j) = dvec(j) + eps_step
        else
            dvec(j) = dvec(j) - 0.5_dp * eps_step
        end if

        n = norm2(cross(dvec, vec_AB))
        if (n > eps_norm) then
            dvec(:) = dvec(:) / n
            dvec(i) = dvec(i) - 1._dp
            dvec(:) = dvec(:) / eps_step
            prod = cross(dvec, vec_AB)
        else
            prod = 0._dp
        end if
    end function get_dvec_x_vAB
end function get_rotation_matrix_deriv


pure function get_theta_angle(vec_CM, rotmat) result(theta)
    !! Returns the 'theta' angle (in the rotated basis) between the
    !! CM vector and the z-axis. Returns 0 if CM is a (near-)null vector.
    real(dp), dimension(3), intent(in) :: vec_CM
        !! Normalized CM vector
    real(dp), dimension(3, 3), intent(in) :: rotmat
        !! Rotation matrix describing the new basis
    real(dp) :: theta

    real(dp), parameter :: eps_norm = 1e-10_dp
    real(dp), dimension(3) :: normal

    if (norm2(vec_CM) < eps_norm) then
        theta = 0._dp
    else
        normal = matmul(rotmat, vec_CM)
        theta = atan2(normal(1), normal(3))
    end if
end function get_theta_angle


pure function get_vAB_derivA(vec_AB, rAB) result(dvABdA)
    !! Returns the derivatives of the AB vector with respect
    !! to the position of A.
    real(dp), dimension(3), intent(in) :: vec_AB
        !! AB unit vector
    real(dp), intent(in) :: rAB
        !! AB distance
    real(dp), dimension(3, 3) :: dvABdA

    integer :: i

    do i = 1, 3
        dvABdA(:, i) = vec_AB(i) * vec_AB(:) / rAB
        dvABdA(i, i) = dvABdA(i, i) - 1._dp / rAB
    end do
end function get_vAB_derivA


pure function get_vCM_derivA(vec_CM, rCM) result(dvCMdA)
    !! Returns the derivatives of the CM vector with respect
    !! to the position of A (M = middle of A and B).
    real(dp), dimension(3), intent(in) :: vec_CM
        !! CM unit vector
    real(dp), intent(in) :: rCM
        !! CM distance
    real(dp), dimension(3, 3) :: dvCMdA

    integer :: i

    if (rCM > EPS_RCM) then
        dvCMdA = 0.5_dp * get_vAB_derivA(vec_CM, rCM)
    else
        dvCMdA = 0._dp
        do i = 1, 3
            dvCMdA(i, i) = -1._dp
        end do
    end if
end function get_vCM_derivA

end module tibi_threecenter_geometry
