!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_coulomb_realspace_multipole
!! Module for Coulomb interactions in real space for (point) multipoles

use tibi_atoms, only: Atoms_type
use tibi_constants, only: dp, &
                          pi
use tibi_coulomb_realspace, only: RealSpaceCoulombCalculator_type
use tibi_utils_prog, only: assert
implicit none


type, extends(RealSpaceCoulombCalculator_type) :: &
              RealSpaceMultipoleCalculator_type
    !! Calculator for real-space Coulomb interactions between point multipoles
    logical, dimension(:), allocatable :: include_dipoles
        !! whether to also include point dipoles (and not just monopoles),
        !! for each atomic kind
    logical, dimension(:), allocatable :: include_quadrupoles
        !! whether to also include point quadrupoles (and not just monopoles
        !! and dipoles), for each atomic kind
    integer, dimension(:, :), allocatable :: indices
        !! start and end indices of the input moments on each atom
    integer, dimension(:, :), allocatable :: indices_nozeta
        !! start and end indices and zeta counts for each atom in self%moments
    real(dp), dimension(:), allocatable :: locpot
        !! array with, for each charge moment, the energy derivative w.r.t.
        !! that charge moment (e.g. the electrostatic potential in case
        !! of a charge monopole).
    real(dp), dimension(:), allocatable :: moments
        !! array of the charge moments on each atom (1 per atom in the case
        !! of monopoles, 4 per atom when also dipoles are included, etc.)
    integer, dimension(:), allocatable :: nmulti
        !! number of multipoles per atom, for each atomic kind
    contains
    procedure :: determine_cutoff_radii
    procedure :: initialize
    procedure :: add_local_potentials
    procedure :: calculate_energy
    procedure :: calculate_forces
    procedure :: calculate_local_potentials
    procedure :: check_energy
    procedure :: evaluate_pair
    procedure :: update_geometry
    procedure :: update_moments
end type RealSpaceMultipoleCalculator_type


contains


function determine_cutoff_radii(self, atoms, tolerance) result(rcuts)
    !! Returns the cutoff radii to be used in the neighbor list builds
    !! for periodic structures.
    class(RealSpaceMultipoleCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), intent(in) :: tolerance
        !! accuracy threshold
    real(dp), dimension(atoms%nkind, atoms%nkind) :: rcuts

    real(dp) :: rcut_delta

    call assert(atoms%is_periodic, 'determine_cutoff_radii should only ' // &
                'be called for periodic structures')

    if (self%ewald_alpha > epsilon(1._dp)) then
        rcut_delta = self%determine_cutoff_radius_delta(tolerance)
    else
        rcut_delta = 0._dp
    end if

    rcuts(:, :) = rcut_delta
end function determine_cutoff_radii


subroutine initialize(self, atoms, indices, distribution, alpha, tolerance, &
                      ilmax)
    !! Initialization of the real-space Ewald sum calculator.
    class(RealSpaceMultipoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    integer, dimension(2, atoms%natom), intent(in) :: indices
        !! start and end indices of the charges on each atom
    character(len=*), intent(in) :: distribution
        !! functional form of the charge distributions
    real(dp), intent(in) :: alpha
        !! Ewald screening parameter
    real(dp), intent(in) :: tolerance
        !! accuracy threshold
    integer, dimension(:), intent(in) :: ilmax
        !! maximum angular momentum indices in multipole expansions
        !! (one for each atomic kind)

    integer :: iat, ik, istart, nmom, nzeta

    ! Set empty description, to exclude results from output
    self%description = ''

    call assert(distribution == 'delta', 'Only point-multipoles are allowed')
    self%distribution = distribution

    if (atoms%is_periodic .and. alpha < epsilon(1._dp)) then
        self%ewald_alpha = 0.5_dp
    else
        self%ewald_alpha = alpha
    end if

    self%indices = indices
    call self%initialize_neighborlist(atoms, tolerance)

    ! Determine an appropriate value for the Ewald parameter (if undefined)
    if (atoms%is_periodic .and. self%ewald_alpha < epsilon(1._dp)) then
        call self%determine_ewald_alpha(tolerance)
    end if

    call assert(all(ilmax >= 1) .and. all(ilmax <= 3), 'ilmax must lie ' // &
                'between 1 (monopoles only) and 3 (up to quadrupoles)')
    self%include_dipoles = ilmax >= 2
    self%include_quadrupoles = ilmax >= 3
    self%nmulti = ilmax**2

    allocate(self%indices_nozeta(3, atoms%natom))

    nmom = 0
    istart = 1
    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        nmom = nmom + self%nmulti(ik)

        self%indices_nozeta(1, iat) = istart
        self%indices_nozeta(2, iat) = istart + self%nmulti(ik) - 1
        nzeta = (self%indices(2, iat) - self%indices(1, iat) + 1) &
                / self%nmulti(ik)
        self%indices_nozeta(3, iat) = nzeta

        istart = istart + self%nmulti(ik)
    end do

    allocate(self%moments(nmom))
    allocate(self%locpot(nmom))
end subroutine initialize


pure subroutine add_local_potentials(self, locpot)
    !! Adds, for each charge moment, the energy derivative w.r.t.
    !! that charge moment (e.g. the electrostatic potential in case
    !! of a charge monopole).
    class(RealSpaceMultipoleCalculator_type), intent(in) :: self
    real(dp), dimension(:), intent(inout) :: locpot

    integer :: iat, istart, iend, istart_z, iend_z, izeta, nzeta, nmulti

    do iat = 1, size(self%indices, dim=2)
        istart = self%indices_nozeta(1, iat)
        iend = self%indices_nozeta(2, iat)
        nzeta = self%indices_nozeta(3, iat)
        nmulti = iend - istart + 1

        istart_z = self%indices(1, iat)
        do izeta = 1, nzeta
            iend_z = istart_z + nmulti - 1
            locpot(istart_z:iend_z) = locpot(istart_z:iend_z) &
                                      + self%locpot(istart:iend)
            istart_z = iend_z + 1
        end do
    end do
end subroutine add_local_potentials


pure subroutine calculate_energy(self, atoms)
    !! Calculates the contribution to the total energy for the fluctuations
    !! associated with the given [[tibi_atoms:Atoms_type]] object, which is
    !! stored in atoms%energy.
    !!
    !! @NOTE Also updates self%locpot in the process.
    class(RealSpaceMultipoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms

    integer :: iat, jat, ik, jk, ineigh, ineighlist, istart, jstart
    real(dp) :: energy, factor, mi, mj, r, t1
    real(dp), dimension(6) :: rinv
    real(dp), dimension(3) :: t3, vec
    real(dp), dimension(3, 3) :: t5
    real(dp), dimension(3, 3, 3) :: t7
    real(dp), dimension(3, 3, 3, 3) :: t9

    atoms%energy = 0._dp
    self%locpot(:) = 0._dp

    istart = 1
    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)

        do ineigh = 1, self%nl%neighbors(iat)
            ineighlist = self%nl%start_indices(iat) + ineigh - 1
            jat = self%nl%neighbor_indices(ineighlist)
            jk = atoms%kinds(jat)
            jstart = self%indices_nozeta(1, jat)

            r = self%nl%r(ineighlist)
            vec = -r * self%nl%vec(:, ineighlist)

            if (r > self%nl%rcuts(ik, jk)) cycle

            call get_inverse_distances(rinv, r, self%ewald_alpha)

            if (iat == jat) then
                factor = 0.5_dp
            else
                factor = 1._dp
            end if

            t1 = get_tensor_r1(rinv)
            mi = self%moments(istart)
            mj = self%moments(jstart)
            associate(e0i => self%locpot(istart))
            associate(e0j => self%locpot(jstart))

            energy = factor * mi * t1 * mj
            e0i = e0i + factor * t1 * self%moments(jstart)
            e0j = e0j + factor * t1 * self%moments(istart)

            if (self%include_dipoles(ik) .or. self%include_dipoles(jk)) then
                t3 = get_tensor_r3(rinv, vec)
                t5 = get_tensor_r5(rinv, vec)
            end if

            if (self%include_quadrupoles(ik) .or. &
                self%include_quadrupoles(jk)) then
                t7 = get_tensor_r7(rinv, vec)
                t9 = get_tensor_r9(rinv, vec)
            end if

            if (self%include_dipoles(ik)) then
                associate(di => self%moments(istart+1:istart+3))
                associate(e1i => self%locpot(istart+1:istart+3))
                energy = energy + factor * mj * sum(t3 * di)
                e0j = e0j + factor * sum(t3 * di)
                e1i = e1i + factor *  t3 * mj
                end associate
                end associate

                if (self%include_quadrupoles(ik)) then
                    associate(qi => self%moments(istart+4:istart+8))
                    e0j = e0j + factor * sum(t5 * dense2square(qi)) / 3
                    end associate
                end if
            end if

            if (self%include_dipoles(jk)) then
                associate(dj => self%moments(jstart+1:jstart+3))
                associate(e1j => self%locpot(jstart+1:jstart+3))
                energy = energy - factor * mi * sum(t3 * dj)
                e0i = e0i - factor * sum(t3 * dj)
                e1j = e1j - factor *  t3 * mi
                end associate
                end associate

                if (self%include_quadrupoles(jk)) then
                    associate(qj => self%moments(jstart+4:jstart+8))
                    e0i = e0i + factor * sum(t5 * dense2square(qj)) / 3
                    end associate
                end if
            end if

            if (self%include_dipoles(ik) .and. self%include_dipoles(jk)) then
                associate(di => self%moments(istart+1:istart+3))
                associate(e1i => self%locpot(istart+1:istart+3))
                associate(dj => self%moments(jstart+1:jstart+3))
                associate(e1j => self%locpot(jstart+1:jstart+3))
                energy = energy - factor * sum(di * matmul(t5, dj))
                e1i = e1i - factor * matmul(t5, dj)
                e1j = e1j - factor * matmul(t5, di)
                end associate
                end associate
                end associate
                end associate
            end if

            if (self%include_quadrupoles(ik)) then
                associate(qi => self%moments(istart+4:istart+8))
                associate(e2i => self%locpot(istart+4:istart+8))
                energy = energy + factor &
                         * mj * sum(t5 * dense2square(qi)) / 3
                e2i = e2i + factor * square2dense(t5) * mj / 3

                if (self%include_dipoles(jk)) then
                    associate(dj => self%moments(jstart+1:jstart+3))
                    associate(e1j => self%locpot(jstart+1:jstart+3))
                    energy = energy - factor &
                             * sum(dj * tensordot322(t7, dense2square(qi))) / 3
                    e1j = e1j - factor * tensordot322(t7, dense2square(qi)) / 3
                    e2i = e2i - factor * square2dense(tensordot311(t7, dj)) / 3
                    end associate
                    end associate
                end if

                end associate
                end associate
            end if

            if (self%include_quadrupoles(jk)) then
                associate(qj => self%moments(jstart+4:jstart+8))
                associate(e2j => self%locpot(jstart+4:jstart+8))
                energy = energy + factor &
                         * mi * sum(t5 * dense2square(qj)) / 3
                e2j = e2j + factor * square2dense(t5) * mi / 3

                if (self%include_dipoles(ik)) then
                    associate(di => self%moments(istart+1:istart+3))
                    associate(e1i => self%locpot(istart+1:istart+3))
                    energy = energy + factor &
                             * sum(di * tensordot322(t7, dense2square(qj))) / 3
                    e1i = e1i + factor * tensordot322(t7, dense2square(qj)) / 3
                    e2j = e2j + factor * square2dense(tensordot311(t7, di)) / 3
                    end associate
                    end associate
                end if

                end associate
                end associate
            end if

            if (self%include_quadrupoles(ik) .and. &
                self%include_quadrupoles(jk)) then
                associate(qi => self%moments(istart+4:istart+8))
                associate(e2i => self%locpot(istart+4:istart+8))
                associate(qj => self%moments(jstart+4:jstart+8))
                associate(e2j => self%locpot(jstart+4:jstart+8))
                energy = energy + factor * sum(dense2square(qi) &
                                  * tensordot422(t9, dense2square(qj))) / 9
                e2i = e2i + factor * square2dense(tensordot422(t9, &
                                                        dense2square(qj))) / 9
                e2j = e2j + factor * square2dense(tensordot422(t9, &
                                                        dense2square(qi))) / 9
                end associate
                end associate
                end associate
                end associate
            end if

            end associate
            end associate

            atoms%energy = atoms%energy + energy
        end do

        istart = istart + self%nmulti(ik)
    end do
end subroutine calculate_energy


subroutine check_energy(self, atoms)
    !! Checks whether atoms%energy agrees with the value calculated
    !! from the prodcts of the multipole moments and the derivative
    !! of the total energy with respect to those moments.
    class(RealSpaceMultipoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: iat, ik, istart
    real(dp) :: energy
    real(dp), parameter :: tolerance = 1e-8_dp

    energy = 0._dp

    istart = 1
    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)

        associate(mi => self%moments(istart))
        associate(e0i => self%locpot(istart))
        energy = energy + mi * e0i

        if (self%include_dipoles(ik)) then
            associate(di => self%moments(istart+1:istart+3))
            associate(e1i => self%locpot(istart+1:istart+3))
            energy = energy + sum(di * e1i)

            if (self%include_quadrupoles(ik)) then
                associate(qi => self%moments(istart+4:istart+8))
                associate(e2i => self%locpot(istart+4:istart+8))
                energy = energy + sum(dense2square(qi) * dense2square(e2i))
                end associate
                end associate
            end if

            end associate
            end associate
        end if

        end associate
        end associate

        istart = istart + self%nmulti(ik)
    end do

    energy = energy / 2

    call assert(abs(atoms%energy - energy) < tolerance, 'Energy is not ' // &
                'consistent with the electrostatic potential and ' // &
                'electric field (gradient)')
end subroutine check_energy


pure subroutine calculate_forces(self, atoms, do_stress)
    !! Calculates the atomic forces for the given [[tibi_atoms:Atoms_type]]
    !! object, which are stored in atoms%forces.
    !!
    !! @NOTE This procedure is largely similar to the corresponding one
    !! in [[tibi_realspace_pair:RealSpacePairCalculator_type]].
    class(RealSpaceMultipoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    logical, intent(in) :: do_stress
        !! whether to also compute the stress tensor for periodic structures

    integer :: iat, jat, ik, jk, ineigh, ineighlist, istart, jstart
    integer :: i, j
    real(dp) :: factor
    real(dp), dimension(6) :: rinv
    real(dp), dimension(3) :: dedx, t3
    real(dp), dimension(3, 3) :: t5
    real(dp), dimension(3, 3, 3) :: t7
    real(dp), dimension(3, 3, 3, 3) :: t9
    real(dp), dimension(3, 3, 3, 3, 3) :: t11

    atoms%forces = 0._dp

    if (do_stress) then
        atoms%stress = 0._dp
    end if

    istart = 1
    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)

        do ineigh = 1, self%nl%neighbors(iat)
            ineighlist = self%nl%start_indices(iat) + ineigh - 1

            jat = self%nl%neighbor_indices(ineighlist)
            jk = atoms%kinds(jat)
            jstart = self%indices_nozeta(1, jat)

            associate(r => self%nl%r(ineighlist))
            associate(vec => -r * self%nl%vec(:, ineighlist))

            if (r > self%nl%rcuts(ik, jk)) cycle

            call get_inverse_distances(rinv, r, self%ewald_alpha)

            t3 = get_tensor_r3(rinv, vec)
            associate(mi => self%moments(istart))
            associate(mj => self%moments(jstart))

            ! monopole-monopole
            dedx = mi * t3 * mj

            if (self%include_dipoles(ik) .or. self%include_dipoles(jk)) then
                t5 = get_tensor_r5(rinv, vec)
                t7 = get_tensor_r7(rinv, vec)
            end if

            if (self%include_dipoles(ik)) then
                associate(di => self%moments(istart+1:istart+3))

                ! dipole-monopole
                dedx = dedx + mj * matmul(t5, di)

                end associate
            end if

            if (self%include_dipoles(jk)) then
                associate(dj => self%moments(jstart+1:jstart+3))

                ! monopole-dipole
                dedx = dedx - mi * matmul(t5, dj)

                end associate
            end if

            if (self%include_dipoles(ik) .and. self%include_dipoles(jk)) then
                associate(di => self%moments(istart+1:istart+3))
                associate(dj => self%moments(jstart+1:jstart+3))

                ! dipole-dipole
                dedx = dedx - tensordot121(di, tensordot311(t7, dj))

                end associate
                end associate
            end if

            if (self%include_quadrupoles(ik) .or. &
                self%include_quadrupoles(jk)) then
                t9 = get_tensor_r9(rinv, vec)
                t11 = get_tensor_r11(rinv, vec)
            end if

            if (self%include_quadrupoles(ik)) then
                associate(qi => self%moments(istart+4:istart+8))

                ! quadrupole-monopole
                dedx = dedx + mj * tensordot322(t7, dense2square(qi)) / 3

                if (self%include_dipoles(jk)) then
                    associate(dj => self%moments(jstart+1:jstart+3))

                    ! quadrupole-dipole
                    dedx = dedx - tensordot121(dj, &
                                        tensordot422(t9, dense2square(qi))) / 3

                    end associate
                end if

                end associate
            end if

            if (self%include_quadrupoles(jk)) then
                associate(qj => self%moments(jstart+4:jstart+8))

                ! monopole-quadrupole
                dedx = dedx + mi * tensordot322(t7, dense2square(qj)) / 3

                if (self%include_dipoles(ik)) then
                    associate(di => self%moments(istart+1:istart+3))

                    ! dipole-quadrupole
                    dedx = dedx + tensordot121(di, &
                                        tensordot422(t9, dense2square(qj))) / 3

                    end associate
                end if

                end associate
            end if

            if (self%include_quadrupoles(ik) .and. &
                self%include_quadrupoles(jk)) then
                associate(qi => self%moments(istart+4:istart+8))
                associate(qj => self%moments(jstart+4:jstart+8))

                ! quadrupole-quadrupole
                dedx = dedx + tensordot232(dense2square(qi), &
                                    tensordot522(t11, dense2square(qj))) / 9

                end associate
                end associate
            end if

            end associate
            end associate

            atoms%forces(:, iat) = atoms%forces(:, iat) - dedx
            atoms%forces(:, jat) = atoms%forces(:, jat) + dedx

            if (do_stress) then
                if (iat == jat) then
                    factor = 0.5_dp
                else
                    factor = 1._dp
                end if

                do i = 1, 3
                    do j = 1, 3
                        atoms%stress(i, j) = atoms%stress(i, j) &
                                + factor * (dedx(i) * vec(j) + dedx(j) * vec(i))
                    end do
                end do
            end if

            end associate
            end associate
        end do

        istart = istart + self%nmulti(ik)
    end do

    if (do_stress) then
        atoms%stress = atoms%stress / (2 * atoms%get_volume())
    end if
end subroutine calculate_forces


subroutine calculate_local_potentials(self)
    !! Calculates the energy derivatives w.r.t. the charge moments.
    class(RealSpaceMultipoleCalculator_type), intent(inout) :: self

    ! Do nothing (this will be handled by calculate_energy).
end subroutine calculate_local_potentials


elemental function evaluate_pair(self, iat, jat, r, der) result(fval)
    !! Evaluates the pairwise interaction between two charge distributions.
    !!
    !! @NOTE This function is not supposed to be used.
    class(RealSpaceMultipoleCalculator_type), intent(in) :: self
    integer, intent(in) :: iat, jat
        !! the atomic indices
    real(dp), intent(in) :: r
        !! the interatomic distance
    integer, intent(in) :: der
        !! the derivative index (only der=1 is allowed here)
    real(dp) :: fval

    fval = 0._dp
end function evaluate_pair


subroutine update_geometry(self, atoms)
    !! Updates the calculator parts which depend on the atomic positions
    !! and cell vectors.
    class(RealSpaceMultipoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    call self%nl%update(atoms)
end subroutine update_geometry


pure subroutine update_moments(self, moments)
    !! Updates the calculator with respect to a new set of charge moments.
    class(RealSpaceMultipoleCalculator_type), intent(inout) :: self
    real(dp), dimension(:), intent(in) :: moments

    integer :: iat, istart, iend, istart_z, iend_z, izeta, nzeta, nmulti

    do iat = 1, size(self%indices, dim=2)
        istart = self%indices_nozeta(1, iat)
        iend = self%indices_nozeta(2, iat)
        nzeta = self%indices_nozeta(3, iat)
        nmulti = iend - istart + 1

        self%moments(istart:iend) = 0._dp

        istart_z = self%indices(1, iat)
        do izeta = 1, nzeta
            iend_z = istart_z + nmulti - 1
            self%moments(istart:iend) = self%moments(istart:iend) &
                                        + moments(istart_z:iend_z)
            istart_z = iend_z + 1
        end do

        istart = iend + 1
    end do
end subroutine update_moments


pure subroutine get_inverse_distances(rinv, r, alpha)
    !! Returns the values of the (odd-powered) inverse distance
    !! 1/r^n (n = 1, 3, 5, ...), with applied screening if the
    !! Ewald alpha parameter is positive.
    real(dp), dimension(:), intent(out) :: rinv
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), intent(in) :: alpha
        !! Ewald alpha parameter

    integer :: i, factorial2

    if (alpha < epsilon(1._dp)) then
        do i = 1, size(rinv)
            rinv(i) = 1._dp / r**(2*i - 1)
        end do
    else
        rinv(1) = erfc(alpha * r) / r
        factorial2 = 1

        do i = 2, size(rinv)
            rinv(i) = (rinv(i-1) + 2**(i - 1) * alpha**(2*i - 3) &
                       * exp(-(alpha * r)**2) / (sqrt(pi) * factorial2)) / r**2
            factorial2 = factorial2 * (2*i - 1)
        end do
    end if
end subroutine get_inverse_distances


pure function get_tensor_r1(rinv) result(t)
    !! Returns the 0-dimensional interaction tensor
    real(dp), dimension(:), intent(in) :: rinv
        !! (screened) inverse distances
    real(dp) :: t

    t = rinv(1)
end function get_tensor_r1


pure function get_tensor_r3(rinv, vec) result(t)
    !! Returns the 1-dimensional interaction tensor
    real(dp), dimension(:), intent(in) :: rinv
        !! (screened) inverse distances
    real(dp), dimension(3), intent(in) :: vec
        !! vector pointing from the second to the first atom
    real(dp), dimension(3) :: t

    t = -rinv(2) * vec
end function get_tensor_r3


pure function get_tensor_r5(rinv, vec) result(t)
    !! Returns the 2-dimensional interaction tensor
    real(dp), dimension(:), intent(in) :: rinv
        !! (screened) inverse distances
    real(dp), dimension(3), intent(in) :: vec
        !! vector pointing from the second to the first atom
    real(dp), dimension(3, 3) :: t

    integer :: i, j

    do i = 1, 3
        do j = 1, 3
            t(i, j) = 3 * rinv(3) * vec(i) * vec(j)
        end do

        t(i, i) = t(i, i) - rinv(2)
    end do
end function get_tensor_r5


pure function get_tensor_r7(rinv, vec) result(t)
    !! Returns the 3-dimensional interaction tensor
    real(dp), dimension(:), intent(in) :: rinv
        !! (screened) inverse distances
    real(dp), dimension(3), intent(in) :: vec
        !! vector pointing from the second to the first atom
    real(dp), dimension(3, 3, 3) :: t

    integer :: i, j, k

    do i = 1, 3
        do j = 1, 3
            do k = 1, 3
                t(i, j, k) = -15 * rinv(4) * vec(i) * vec(j) * vec(k)
                if (j == k) then
                    t(i, j, k) = t(i, j, k) + 3 * rinv(3) * vec(i)
                end if
                if (i == k) then
                    t(i, j, k) = t(i, j, k) + 3 * rinv(3) * vec(j)
                end if
                if (i == j) then
                    t(i, j, k) = t(i, j, k) + 3 * rinv(3) * vec(k)
                end if
            end do
        end do
    end do
end function get_tensor_r7


pure function get_tensor_r9(rinv, vec) result(t)
    !! Returns the 4-dimensional interaction tensor
    real(dp), dimension(:), intent(in) :: rinv
        !! (screened) inverse distances
    real(dp), dimension(3), intent(in) :: vec
        !! vector pointing from the second to the first atom
    real(dp), dimension(3, 3, 3, 3) :: t

    integer :: i, j, k, l

    do i = 1, 3
        do j = 1, 3
            do k = 1, 3
                do l = 1, 3
                    t(i, j, k, l) = 105 * rinv(5) * vec(i) * vec(j) * vec(k) &
                                    * vec(l)

                    if (k == l) then
                        T(i, j, k, l) = T(i, j, k, l) &
                                        - 15 * rinv(4) * vec(i) * vec(j)
                    end if
                    if (j == l) then
                        T(i, j, k, l) = T(i, j, k, l) &
                                        - 15 * rinv(4) * vec(i) * vec(k)
                    end if
                    if (j == k) then
                        T(i, j, k, l) = T(i, j, k, l) &
                                        - 15 * rinv(4) * vec(i) * vec(l)
                    end if
                    if (i == l) then
                        T(i, j, k, l) = T(i, j, k, l) &
                                        - 15 * rinv(4) * vec(j) * vec(k)
                    end if
                    if (i == k) then
                        T(i, j, k, l) = T(i, j, k, l) &
                                        - 15 * rinv(4) * vec(j) * vec(l)
                    end if
                    if (i == j) then
                        T(i, j, k, l) = T(i, j, k, l) &
                                        - 15 * rinv(4) * vec(k) * vec(l)
                    end if

                    if (i == j .and. k == l) then
                        T(i, j, k, l) = T(i, j, k, l) + 3 * rinv(3)
                    end if
                    if (i == k .and. j == l) then
                        T(i, j, k, l) = T(i, j, k, l) + 3 * rinv(3)
                    end if
                    if (i == l .and. j == k) then
                        T(i, j, k, l) = T(i, j, k, l) + 3 * rinv(3)
                    end if
                end do
            end do
        end do
    end do
end function get_tensor_r9


pure function get_tensor_r11(rinv, vec) result(t)
    !! Returns the 5-dimensional interaction tensor
    real(dp), dimension(:), intent(in) :: rinv
        !! (screened) inverse distances
    real(dp), dimension(3), intent(in) :: vec
        !! vector pointing from the second to the first atom
    real(dp), dimension(3, 3, 3, 3, 3) :: t

    integer :: i, j, k, l, m

    do i = 1, 3
        do j = 1, 3
            do k = 1, 3
                do l = 1, 3
                    do m = 1, 3
                        t(i, j, k, l, m) = -945 * rinv(6) * vec(i) * vec(j) &
                                           * vec(k) * vec(l) * vec(m)

                        if (l == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(i) &
                                                 * vec(j) * vec(k)
                        end if
                        if (k == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(i) &
                                                 * vec(j) * vec(l)
                        end if
                        if (k == l) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(i) &
                                                 * vec(j) * vec(m)
                        end if
                        if (j == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(i) &
                                                 * vec(k) * vec(l)
                        end if
                        if (j == l) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(i) &
                                                 * vec(k) * vec(m)
                        end if
                        if (j == k) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(i) &
                                                 * vec(l) * vec(m)
                        end if
                        if (i == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(j) &
                                                 * vec(k) * vec(l)
                        end if
                        if (i == l) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(j) &
                                                 * vec(k) * vec(m)
                        end if
                        if (i == k) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(j) &
                                                 * vec(l) * vec(m)
                        end if
                        if (i == j) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               + 105 * rinv(5) * vec(k) &
                                                 * vec(l) * vec(m)
                        end if

                        if (i == j .and. l == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(k)
                        end if
                        if (i == j .and. k == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(l)
                        end if
                        if (i == k .and. l == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(j)
                        end if
                        if (i == k .and. j == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(l)
                        end if

                        if (i == l .and. k == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(j)
                        end if
                        if (i == l .and. j == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(k)
                        end if
                        if (j == k .and. l == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(i)
                        end if
                        if (j == k .and. i == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(l)
                        end if

                        if (j == l .and. k == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(i)
                        end if
                        if (j == l .and. i == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(k)
                        end if
                        if (k == l .and. j == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(i)
                        end if
                        if (k == l .and. i == m) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(j)
                        end if

                        if (i == j .and. k == l) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(m)
                        end if
                        if (i == k .and. j == l) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(m)
                        end if
                        if (j == k .and. i == l) then
                            t(i, j, k, l, m) = t(i, j, k, l, m) &
                                               - 15 * rinv(4) * vec(m)
                        end if
                    end do
                end do
            end do
        end do
    end do
end function get_tensor_r11


pure function square2dense(a) result(b)
    !! Conversion from a redundant square representation of a traceless
    !! 2D tensor to its Voigt form (with components xx yy xy xz yz).
    real(dp), dimension(3, 3), intent(in) :: a
    real(dp), dimension(5) :: b

    real(dp) :: x

    x = (a(1, 1) + a(2, 2) + a(3, 3)) / 3
    b = [a(1, 1) - x, a(2, 2) - x, a(1, 2), a(1, 3), a(2, 3)]
end function square2dense


pure function dense2square(a) result(b)
    !! Conversion from a Voigt representation of a traceless 2D tensor
    !! to its redundant square form.
    real(dp), dimension(5), intent(in) :: a
    real(dp), dimension(3, 3) :: b

    b(:, 1) = [a(1), a(3), a(4)]
    b(:, 2) = [a(3), a(2), a(5)]
    b(:, 3) = [a(4), a(5), -(a(1) + a(2))]
end function dense2square


pure function tensordot121(a, b) result(c)
    !! Dot product between a 1- and a 2-dimensional tensor (single contraction).
    real(dp), dimension(3), intent(in) :: a
    real(dp), dimension(3, 3), intent(in) :: b
    real(dp), dimension(3) :: c

    integer :: i

    do i = 1, 3
        c(i) = sum(a(:) * b(:, i))
    end do
end function tensordot121


pure function tensordot232(a, b) result(c)
    !! Dot product between a 2- and a 3-dimensional tensor (double contraction).
    real(dp), dimension(3, 3), intent(in) :: a
    real(dp), dimension(3, 3, 3), intent(in) :: b
    real(dp), dimension(3) :: c

    integer :: i

    do i = 1, 3
        c(i) = sum(a(:, :) * b(:, :, i))
    end do
end function tensordot232


pure function tensordot311(a, b) result(c)
    !! Dot product between a 3- and a 1-dimensional tensor (single contraction).
    real(dp), dimension(3, 3, 3), intent(in) :: a
    real(dp), dimension(3), intent(in) :: b
    real(dp), dimension(3, 3) :: c

    integer :: i

    do i = 1, 3
        c(i, :) = matmul(a(i, :, :), b)
    end do
end function tensordot311


pure function tensordot322(a, b) result(c)
    !! Dot product between a 3- and a 2-dimensional tensor (double contraction).
    real(dp), dimension(3, 3, 3), intent(in) :: a
    real(dp), dimension(3, 3), intent(in) :: b
    real(dp), dimension(3) :: c

    integer :: i

    do i = 1, 3
        c(i) = sum(a(i, :, :) * b)
    end do
end function tensordot322


pure function tensordot422(a, b) result(c)
    !! Dot product between a 4- and a 2-dimensional tensor (double contraction).
    real(dp), dimension(3, 3, 3, 3), intent(in) :: a
    real(dp), dimension(3, 3), intent(in) :: b
    real(dp), dimension(3, 3) :: c

    integer :: i, j

    do i = 1, 3
        do j = 1, 3
            c(i, j) = sum(a(i, j, :, :) * b)
        end do
    end do
end function tensordot422


pure function tensordot522(a, b) result(c)
    !! Dot product between a 5- and a 2-dimensional tensor (double contraction).
    real(dp), dimension(3, 3, 3, 3, 3), intent(in) :: a
    real(dp), dimension(3, 3), intent(in) :: b
    real(dp), dimension(3, 3, 3) :: c

    integer :: i, j, k

    do i = 1, 3
        do j = 1, 3
            do k = 1, 3
                c(i, j, k) = sum(a(i, j, k, :, :) * b)
            end do
        end do
    end do
end function tensordot522

end module tibi_coulomb_realspace_multipole
