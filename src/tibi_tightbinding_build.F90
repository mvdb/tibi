!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_tightbinding) tibi_tightbinding_build
!! Submodule for building the H and S matrices for [[tibi_tightbinding]]

use tibi_orbitals, only: MAXTAU
use tibi_threecenter_geometry, only: get_rCM_and_vec_CM
implicit none


contains


module subroutine build_H0_and_S(self, atoms)
    !! Builds the zeroth-order Hamiltonian (H0) and overlap (S) matrices.
    !!
    !! @NOTE Only the lower triangular blocks of the H0 and S matrices
    !! are constructed.
    class(TightBindingCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: iat, jat, kat, ik, jk, kk, ikpt, ispin
    integer :: ineigh, jneigh, ineighlist, jneighlist
    integer :: istart, jstart, iend, jend, inumlm, jnumlm
    complex(dp) :: phase_shift
    logical :: is_onsite, need_3c
    real(dp) :: rAB, rAC, rBC2, rCM
    real(dp), dimension(3) :: vec_AB, vec_AC, vec_CM
    real(dp), dimension(:, :), allocatable :: hoff3c, hon3c
    real(dp), dimension(:, :), allocatable :: h, hon2c, s

    call task_timer%start_timer('tightbinding_build_H0_and_S')

    allocate(h(self%basis_sets%maxnumlm, self%basis_sets%maxnumlm))
    allocate(s(self%basis_sets%maxnumlm, self%basis_sets%maxnumlm))

    if (self%h0s%veff_expansion_onsite >= 2) then
        allocate(hon2c(self%basis_sets%maxnumlm, self%basis_sets%maxnumlm))
    end if

    if (self%h0s%veff_expansion_onsite >= 3) then
        allocate(hon3c(self%basis_sets%maxnumlm, self%basis_sets%maxnumlm))
    end if

    if (self%h0s%veff_expansion_offsite >= 3) then
        allocate(hoff3c(self%basis_sets%maxnumlm, self%basis_sets%maxnumlm))
    end if

    if (self%complex_wfn) then
        self%hamiltonian_complex = (0._dp, 0._dp)
        self%overlap_complex = (0._dp, 0._dp)
    else
        self%hamiltonian_real = 0._dp
        self%overlap_real = 0._dp
    end if

    !$omp parallel do default(private), shared(self, atoms) schedule(runtime)
    iat_loop: do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        istart = self%indices_lm(1, iat)
        iend = self%indices_lm(2, iat)
        inumlm = self%basis_sets%get_nr_of_orbitals(ik)

        if (self%h0s%veff_expansion_onsite >= 2) then
            hon2c = 0._dp

            do ineigh = 1, self%nl%neighbors(iat)
                ineighlist = self%nl%start_indices(iat) + ineigh - 1
                jat = self%nl%neighbor_indices(ineighlist)
                jk = atoms%kinds(jat)

                vec_AB = self%nl%vec(:, ineighlist)
                rAB = self%nl%r(ineighlist)
                if (rAB > self%nl%rcuts(jk, ik) .or. rAB < self%nl%rsame) cycle

                call self%h0s%onsite2c(ik, jk)%add_integrals(vec_AB(1), &
                                               vec_AB(2), vec_AB(3), rAB, hon2c)
            end do
        end if

        ! TODO insert in the loop above?
        if (self%h0s%veff_expansion_onsite >= 3) then
            hon3c = 0._dp

            do ineigh = 1, self%nl%neighbors(iat)
                ineighlist = self%nl%start_indices(iat) + ineigh - 1
                jat = self%nl%neighbor_indices(ineighlist)
                jk = atoms%kinds(jat)

                vec_AB = self%nl%vec(:, ineighlist)
                rAB = self%nl%r(ineighlist)
                if (rAB > self%nl%rcuts(jk, ik) .or. rAB < self%nl%rsame) cycle

                do jneigh = 1, self%nl%neighbors(iat)
                    if (jneigh == ineigh) cycle
                    jneighlist = self%nl%start_indices(iat) + jneigh - 1
                    kat = self%nl%neighbor_indices(jneighlist)
                    kk = atoms%kinds(kat)

                    vec_AC = self%nl%vec(:, jneighlist)
                    rAC = self%nl%r(jneighlist)
                    if (rAC > self%nl%rcuts(kk, ik) .or. &
                        rAC < self%nl%rsame) cycle

                    call get_rCM_and_vec_CM(rAB, vec_AB, rAC, vec_AC, rCM, &
                                            vec_CM)

                    call self%h0s%onsite3c(ik, jk, kk)%add_integrals( &
                                            vec_AB, rAB, vec_CM, rCM, hon3c)
                end do
            end do
        end if

        ineigh_loop: do ineigh = 1, self%nl%neighbors(iat)
            ineighlist = self%nl%start_indices(iat) + ineigh - 1
            jat = self%nl%neighbor_indices(ineighlist)
            if (jat > iat) exit ineigh_loop

            jk = atoms%kinds(jat)
            jstart = self%indices_lm(1, jat)
            jend = self%indices_lm(2, jat)
            jnumlm = self%basis_sets%get_nr_of_orbitals(jk)

            ! Calculate all the hamiltonian and overlap integrals for iat & jat
            vec_AB = self%nl%vec(:, ineighlist)
            rAB = self%nl%r(ineighlist)
            if (rAB > self%nl%rcuts(jk, ik)) cycle ineigh_loop

            is_onsite = rAB < self%nl%rsame

            if (is_onsite) then
                call self%h0s%on1coff2c(ik, ik)%evaluate_onsite( &
                                h(:inumlm, :inumlm), s(:inumlm, :inumlm))
            else
                call self%h0s%on1coff2c(ik, jk)%evaluate_offsite( &
                                vec_AB(1), vec_AB(2), vec_AB(3), rAB, h, s)
            end if

            if (is_onsite) then
                if (self%h0s%veff_expansion_onsite >= 2) then
                    h = h + hon2c
                end if
                if (self%h0s%veff_expansion_onsite >= 3) then
                    h = h + 0.5_dp * hon3c
                end if
            end if

            need_3c = self%h0s%veff_expansion_offsite >= 3 .and. &
                      (.not. is_onsite)

            if (need_3c) then
                hoff3c = 0._dp

                jneigh_loop: do jneigh = 1, self%nl%neighbors(iat)
                    if (jneigh == ineigh) cycle
                    jneighlist = self%nl%start_indices(iat) + jneigh - 1
                    kat = self%nl%neighbor_indices(jneighlist)
                    kk = atoms%kinds(kat)

                    vec_AC = self%nl%vec(:, jneighlist)
                    rAC = self%nl%r(jneighlist)
                    if (rAC > self%nl%rcuts(kk, ik) .or. &
                        rAC < self%nl%rsame) cycle jneigh_loop

                    rBC2 = sum((rAC * vec_AC - rAB * vec_AB)**2)
                    if (rBC2 > self%nl%rcuts(kk, jk)**2) cycle jneigh_loop

                    call get_rCM_and_vec_CM(rAB, vec_AB, rAC, vec_AC, rCM, &
                                            vec_CM)

                    call self%h0s%offsite3c(ik, jk, kk)%add_integrals( &
                                            vec_AB, rAB, vec_CM, rCM, hoff3c)
                end do jneigh_loop

                h = h + hoff3c
            end if

            ! Now fill in the corresponding submatrices for iat and jat
            do ispin = 1, self%get_nr_of_spins()
                do ikpt = 1, self%get_nr_of_kpoints()
                    if (self%complex_wfn) then
                        phase_shift = &
                            self%occupations%calculate_phase_shift( &
                                            ikpt, atoms%invcell, &
                                            self%nl%translations(:, ineighlist))

                        !$omp critical
                        self%hamiltonian_complex(istart:iend, jstart:jend, &
                                                 ikpt, ispin) = &
                            self%hamiltonian_complex(istart:iend, jstart:jend, &
                                                     ikpt, ispin) &
                            + phase_shift * h(:inumlm, :jnumlm)
                        !$omp end critical

                        !$omp critical
                        self%overlap_complex(istart:iend, jstart:jend, &
                                             ikpt, ispin) = &
                            self%overlap_complex(istart:iend, jstart:jend, &
                                                 ikpt, ispin) &
                            + phase_shift * s(:inumlm, :jnumlm)
                        !$omp end critical
                else
                        !$omp critical
                        self%hamiltonian_real(istart:iend, jstart:jend, &
                                              ikpt, ispin) = &
                            self%hamiltonian_real(istart:iend, jstart:jend, &
                                                    ikpt, ispin) &
                            + h(:inumlm, :jnumlm)
                        !$omp end critical

                        !$omp critical
                        self%overlap_real(istart:iend, jstart:jend, &
                                          ikpt, ispin) = &
                            self%overlap_real(istart:iend, jstart:jend, &
                                                ikpt, ispin) &
                            + s(:inumlm, :jnumlm)
                        !$omp end critical
                    end if
                end do
            end do
        end do ineigh_loop
    end do iat_loop
    !$omp end parallel do

    call task_timer%stop_timer('tightbinding_build_H0_and_S')
end subroutine build_H0_and_S

end submodule tibi_tightbinding_build
