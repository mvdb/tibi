!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_timer_set
!! Module for sets of timers

use tibi_constants, only: dp
use tibi_timer, only: TIMER_NAME_LENGTH, &
                      Timer_type
use tibi_utils_prog, only: assert
implicit none


type TimerLink_type
    !! Class for handling links between a timer and a 'parent' timer
    !!
    !! Needed, in part, because Fortran does not support pointer arrays
    character(len=TIMER_NAME_LENGTH) :: name
        !! timer name
    type(Timer_type), pointer :: parent => null()
        !! parent timer pointer
    type(Timer_type), pointer :: ptr => null()
        !! timer pointer
end type TimerLink_type


type TimerSet_type
    !! Class for handling related [[tibi_timer:Timer_type]] objects
    type(TimerLink_type), dimension(:), allocatable :: timers
        !! timers with links to other timers
    contains
    procedure :: add_timer
    procedure :: print_timing_tables
end type TimerSet_type


type(TimerSet_type) :: global_timer
    !! shared timer set to which timers can be attached


contains


subroutine add_timer(self, timer, name, parent)
    !! Adds a (pointer to a) new timer to the set of registered timers.
    class(TimerSet_type), intent(inout) :: self
    type(Timer_type), target, intent(in) :: timer
        !! timer to be registered
    character(len=*), intent(in) :: name
        !! name of the new timer to be added
    character(len=*), intent(in), optional :: parent
        !! name of the parent timer

    integer :: i, n
    type(TimerLink_type), dimension(:), allocatable :: timers

    call assert(len_trim(name) > 0, 'Timer name cannot be empty string')

    if (allocated(self%timers)) then
        n = size(self%timers)
    else
        n = 0
    end if

    allocate(timers(n+1))
    if (n > 0) timers(1:n) = self%timers(1:n)

    timers(n+1)%name = name
    timers(n+1)%parent => null()
    timers(n+1)%ptr => timer

    if (present(parent)) then
        call assert(len_trim(parent) > 0, 'Parent name cannot be empty string')

        do i = 1, n
            if (trim(parent) == trim(timers(i)%name)) then
                timers(n+1)%parent => timers(i)%ptr
                exit
            end if
        end do
    end if

    call move_alloc(from=timers, to=self%timers)
end subroutine add_timer


subroutine print_timing_tables(self)
    !! Lets the timers print out their timings tables.
    class(TimerSet_type), intent(in) :: self

    character(len=:), allocatable :: name, title
    integer :: i, n
    real(dp) :: subtotal

    n = size(self%timers)

    do i = 1, n
        name = trim(self%timers(i)%name)

        if (associated(self%timers(i)%parent)) then
            title = name // ' subtimings'
            subtotal = self%timers(i)%parent%get_timing(name)
            call self%timers(i)%ptr%print_timing_table(title, subtotal)
        else
            title = name // ' timings'
            call self%timers(i)%ptr%print_timing_table(title)
        end if
    end do
end subroutine print_timing_tables

end module tibi_timer_set
