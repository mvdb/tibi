!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_utils_string
!! Module for small string-related utilities

implicit none


contains


pure function concatenate(arr) result(str)
    !! Pastes array items into one string.
    character(len=*), dimension(:), intent(in) :: arr
        !! string array to be concatenated
    character(len=:), allocatable :: str

    integer :: i

    str = ''
    do i = 1, size(arr)
        str = trim(str) // trim(arr(i))
    end do
end function concatenate


pure function count_items(str) result(num)
    !! Counts items in string that are whitespace or comma separated.
    character(len=*), intent(in) :: str
    integer :: num

    integer :: i
    logical :: flag

    num = 0
    if (str == ' ') then
        num = 0
    else
        num = 1
    end if

    do i = 1, len_trim(str) - 1
        flag = str(i: i) /= ' ' .and. str(i: i) /= ','
        if (flag) then
            flag = str(i+1: i+1) == ' ' .or. str(i+1: i+1) == ','
            if (flag) then
                num = num + 1
            end if
        end if
    end do
end function count_items


subroutine expand_multiplications(str)
    !! When a string contains e.g. "2*0.0", expands it to "0.0 0.0".
    character(len=*), intent(inout) :: str

    character(len=:), allocatable :: full_str
    integer :: i, j, iprev, isep1, isep2, nmul

    full_str = ''
    iprev = 0
    isep1 = 1

    associate(s => str)
    do i = 1, len_trim(s)
        if (s(i: i) == ' ') then
            isep1 = i
        elseif (s(i: i) == '*') then
            ! add the previous part
            if (i > 0 .and. isep1 > 1) then
                full_str = trim(full_str) // ' ' // s(iprev+1: isep1)
            end if

            ! find the next separator
            isep2 = scan(s(i+1: len_trim(s)), ' ,')
            if (isep2 == 0) then
                isep2 = len_trim(s)
            else
                isep2 = isep2 + i - 1
            end if

            ! add the new parts
            read(s(isep1: i-1), '(i3)') nmul
            do j = 1, nmul
                full_str = trim(full_str) // ' ' // s(i+1: isep2)
            end do

            ! advance our delimiter
            iprev = isep2
        end if
    end do

    if (trim(s(iprev+1: len_trim(s))) /= ',') then
        full_str = trim(full_str) // ' ' // s(iprev+1: len_trim(s))
    end if
    end associate

    str = full_str(1: len_trim(full_str))
end subroutine expand_multiplications


pure function get_file_extension(filename) result(ext)
    !! Returns the file extension (e.g. 'filename.xyz' -> '.xyz').
    character(len=*), intent(in) :: filename
    character(len=12) :: ext

    integer :: i

    i = index(filename, '.', back=.true.)
    if (i == 0) then
        ext = ''
    else
        associate(s => filename)
        ext = s(i: len_trim(filename))
        end associate
    end if
end function get_file_extension


pure function int2str(num) result(str)
    !! Converts an integer into a string.
    integer, intent(in) :: num
    character(len=:), allocatable :: str

    character(len=(1+abs(num/10))) :: tmp

    write(tmp, '(i0)') num
    str = trim(tmp)
end function int2str


subroutine lower_case(str)
    !! Converts a string to lower case.
    character(len=*), intent(inout) :: str

    integer :: i

    do i = 1, len(str)
        select case(str(i:i))
        case("A":"Z")
            str(i:i) = achar(iachar(str(i:i)) + 32)
        end select
    end do
end subroutine lower_case


pure function find_string_index(arr, str) result(index)
    !! Returns the first index where the (trimmed) array
    !! element is equal to the (trimmed) string.
    !! Result is 0 if a matching string was not found.
    character(len=*), dimension(:), intent(in) :: arr
        !! the string array
    character(len=*), intent(in) :: str
        !! the string to be searched for
    integer :: index

    logical :: found

    found = .false.
    do index = 1, size(arr)
        if (trim(arr(index)) == trim(str)) then
            found = .true.
            exit
        end if
    end do

    if (.not. found) then
        index = 0
    end if
end function find_string_index


pure subroutine replace_tabs_by_whitespace(str)
    !! Replaces tabs by a (single) whitespace.
    character(len=*), intent(inout) :: str

    integer :: i

    do i = 1, len_trim(str)
        if (ichar(str(i:i)) == 9) then
            str(i:i) = ' '
        end if
    end do
end subroutine replace_tabs_by_whitespace

end module tibi_utils_string
