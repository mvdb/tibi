!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_giese_york_kernel
!! Module for evaluating Giese-York mapping coefficient kernels

use libxsmm, only: libxsmm_dmmcall, &
                   libxsmm_dmmdispatch, &
                   libxsmm_dmmfunction
use tibi_basis_ao, only: BasisAO_type
use tibi_constants, only: dp
use tibi_orbitals, only: lm_labels, &
                         MAXL, &
                         orbital_index
use tibi_spline, only: BatchSpline_type
use tibi_threecenter_geometry, only: get_d_orbital_rotmat, &
                                     get_d_orbital_rotmat_deriv, &
                                     get_rotation_matrix, &
                                     get_rotation_matrix_deriv, &
                                     get_vAB_derivA
use tibi_utils_prog, only: assert, &
                           is_master
implicit none


integer, parameter :: MAXL_1CM = 3
    !! highest implemented subshell index for on-site mapping coefficients

integer, parameter :: MAXL_2CM = 3
    !! highest implemented subshell index for off-site mapping coefficients

integer, parameter :: MAXLM_1CM = MAXL_1CM**2
    !! highest implemented orbital index for on-site mapping coefficients

integer, parameter :: MAXLM_2CM = MAXL_2CM**2
    !! highest implemented orbital index for off-site mapping coefficients


type, extends(BatchSpline_type) :: MultipoleBatchSpline_type
    contains
    procedure :: initialize_batch
end type MultipoleBatchSpline_type


type GieseYorkKernel_type
    !! Evaluates the mapping coefficients for one atomic kind pair
    !! by interpolation.
    integer :: ilmax1
        !! maximum angular momentum index in the multipole expansion
        !! for the first kind
    integer :: ilmax2
        !! maximum angular momentum index in the multipole expansion
        !! for the second kind
    logical, dimension(:, :), allocatable :: inc1
        !! (MAXL, numset1) array with included subshells for the first kind
    logical, dimension(:, :), allocatable :: inc2
        !! (MAXL, numset2) array with included subshells for the second kind
    integer :: naux1
        !! number of auxiliary functions for the first kind
    integer :: naux2
        !! number of auxiliary functions for the second kind
    integer :: maxdorb
        !! maximum number of d-orbital rotations to consider
    integer, dimension(:), allocatable :: numlm1
        !! number of orbitals for the first kind
    integer, dimension(:), allocatable :: numlm2
        !! number of orbitals for the second kind
    integer :: numset1
        !! number of basis subsets for the first kind
    integer :: numset2
        !! number of basis subsets for the second kind
    integer, dimension(MAXL_2CM) :: nzeta
        !! total number of auxiliary radial functions per subshell
        !! (first and second kind)
    integer :: nzeta1
        !! number of auxiliary radial functions for the first kind
    integer :: nzeta2
        !! number of auxiliary radial functions for the second kind
    real(dp), dimension(:, :, :), allocatable :: onsiteM
        !! onsite mapping coefficient matrix (if homonuclear)
    integer, dimension(:), allocatable :: order
        !! indices used for sorting the auxiliary basis functions
    real(dp) :: rcut
        !! cutoff radius
    type(MultipoleBatchSpline_type), dimension(:, :, :, :), allocatable :: spl
        !! integral interpolators for every pair of main basis subsets
        !! and pair of main basis subshells
    type(libxsmm_dmmfunction) :: xsmmf_p
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_d
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_pps
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_pds
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_dds
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_spp
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_sdp
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_spd
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_sdd
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_ppp1
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_ppp2
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_ppd1
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_ppd2
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_pdp1
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_pdp2
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_pdd1
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_pdd2
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_ddp1
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_ddp2
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_ddd1
        !! LibXSMM matrix multiplicator
    type(libxsmm_dmmfunction) :: xsmmf_ddd2
        !! LibXSMM matrix multiplicator
    contains
    procedure :: evaluate_onsite
    procedure :: evaluate_offsite
    procedure :: evaluate_offsite_deriv
    procedure :: initialize
    procedure :: initialize_xsmmf
end type GieseYorkKernel_type


contains


subroutine evaluate_offsite(self, x, y, z, r, M)
    !! Returns the off-site mapping coefficients for the chosen orbitals
    !! and orientation.
    class(GieseYorkKernel_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :, :), intent(out) :: M
        !! output mapping coefficient matrix

    integer :: icount, jcount, kcount, naux12, iset, jset
    real(dp), dimension(3, 3) :: pmat, rmat_p
    real(dp), dimension(5, 5) :: dmat, rmat_d
    real(dp), dimension(9, 9) :: rmat_pp
    real(dp), dimension(15, 15) :: rmat_pd, rmat_dp
    real(dp), dimension(25, 25) :: rmat_dd
    real(dp), dimension(:), pointer :: ptr1
    real(dp), dimension(:, :), pointer :: ptr2
    real(dp), dimension(5*5*(self%naux1+self%naux2)), target :: reftmp

    pmat = get_rotation_matrix([x, y, z], [0._dp, 0._dp, 1._dp])
    rmat_p = transpose(pmat)
    rmat_pp = get_rmat_xx(3, rmat_p)

    if (self%maxdorb >= 1) then
        dmat = get_d_orbital_rotmat(pmat)
        rmat_d = transpose(dmat)
        rmat_pd = get_rmat_xy(3, 5, rmat_p, rmat_d)
        rmat_dp = get_rmat_xy(5, 3, rmat_d, rmat_p)

        if (self%maxdorb >= 2) then
            rmat_dd = get_rmat_xx(5, rmat_d)
        end if
    end if

    naux12 = self%naux1 + self%naux2

    icount = 1
    do iset = 1, self%numset1
        associate(iinc => self%inc1(:, iset))
        associate(inumlm => self%numlm1(iset))

        jcount = 1
        do jset = 1, self%numset2
            associate(jinc => self%inc2(:, jset))
            associate(jnumlm => self%numlm2(jset))

            if (iinc(1)) then
                if (jinc(1)) then
                    kcount = 1
                    call self%spl(1, 1, iset, jset)%evaluate_batch_nomask(r, &
                                                                        reftmp)
                    ptr1(1:naux12) => reftmp(1:naux12)

                    ! sss
                    kcount = kcount + self%nzeta(1)

                    ! ssp
                    if (self%nzeta(2) > 0) then
                        call rot_z(3, self%nzeta(2), rmat_p, self%xsmmf_p, &
                                   ptr1(kcount:kcount+3*self%nzeta(2)-1))
                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! ssd
                    if (self%nzeta(3) > 0) then
                        call rot_z(5, self%nzeta(3), rmat_d, self%xsmmf_d, &
                                   ptr1(kcount:kcount+5*self%nzeta(3)-1))
                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    M(icount, jcount, :) = ptr1

                    jcount = jcount + 1
                end if

                if (jinc(2)) then
                    kcount = 1
                    call self%spl(1, 2, iset, jset)%evaluate_batch_nomask(r, &
                                                                        reftmp)
                    ptr2(1:3, 1:naux12) => reftmp(1:3*naux12)

                    ! sps
                    if (self%nzeta(1) > 0) then
                        call rot_y(3, self%nzeta(1), rmat_p, &
                                   ptr2(:, kcount:kcount+self%nzeta(1)-1))
                        kcount = kcount + self%nzeta(1)
                    end if

                    ! spp
                    if (self%nzeta(2) > 0) then
                        call rot_yz(3, 3, self%nzeta(2), rmat_pp, &
                                    self%xsmmf_spp, &
                                    ptr2(:, kcount:kcount+3*self%nzeta(2)-1))
                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! spd
                    if (self%nzeta(3) > 0) then
                        call rot_yz(3, 5, self%nzeta(3), rmat_pd, &
                                    self%xsmmf_spd, &
                                    ptr2(:, kcount:kcount+5*self%nzeta(3)-1))
                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    M(icount, jcount:jcount+2, :) = ptr2

                    jcount = jcount + 3
                end if

                if (jinc(3)) then
                    kcount = 1
                    call self%spl(1, 3, iset, jset)%evaluate_batch_nomask(r, &
                                                                        reftmp)
                    ptr2(1:5, 1:naux12) => reftmp(1:5*naux12)

                    ! sds
                    if (self%nzeta(1) > 0) then
                        call rot_y(5, self%nzeta(1), rmat_d, &
                                   ptr2(:, kcount:kcount+self%nzeta(1)-1))
                        kcount = kcount + self%nzeta(1)
                    end if

                    ! sdp
                    if (self%nzeta(2) > 0) then
                        call rot_yz(5, 3, self%nzeta(2), rmat_dp, &
                                    self%xsmmf_sdp, &
                                    ptr2(:, kcount:kcount+3*self%nzeta(2)-1))
                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! sdd
                    if (self%nzeta(3) > 0) then
                        call rot_yz(5, 5, self%nzeta(3), rmat_dd, &
                                    self%xsmmf_sdd, &
                                    ptr2(:, kcount:kcount+5*self%nzeta(3)-1))
                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    M(icount, jcount:jcount+4, :) = ptr2

                    jcount = jcount + 5
                end if

                icount = icount + 1
                jcount = jcount - jnumlm
            end if

            if (iinc(2)) then
                if (jinc(1)) then
                    kcount = 1
                    call self%spl(2, 1, iset, jset)%evaluate_batch_nomask(r, &
                                                                        reftmp)
                    ptr2(1:3, 1:naux12) => reftmp(1:3*naux12)

                    ! pss
                    if (self%nzeta(1) > 0) then
                        call rot_y(3, self%nzeta(1), rmat_p, &
                                   ptr2(:, kcount:kcount+self%nzeta(1)-1))
                        kcount = kcount + self%nzeta(1)
                    end if

                    ! psp
                    if (self%nzeta(2) > 0) then
                        call rot_yz(3, 3, self%nzeta(2), rmat_pp, &
                                    self%xsmmf_spp, &
                                    ptr2(:, kcount:kcount+3*self%nzeta(2)-1))
                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! psd
                    if (self%nzeta(3) > 0) then
                        call rot_yz(3, 5, self%nzeta(3), rmat_pd, &
                                    self%xsmmf_spd, &
                                    ptr2(:, kcount:kcount+5*self%nzeta(3)-1))
                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    M(icount:icount+2, jcount, :) = ptr2

                    jcount = jcount + 1
                end if

                if (jinc(2)) then
                    kcount = 1
                    call self%spl(2, 2, iset, jset)%evaluate_batch_nomask(r, &
                                                                        reftmp)
                    ptr2(1:9, 1:naux12) => reftmp(1:9*naux12)

                    ! pps
                    if (self%nzeta(1) > 0) then
                        call rot_xy(3, 3, self%nzeta(1), rmat_pp, &
                                    self%xsmmf_pps, &
                                    ptr2(:, kcount:kcount+self%nzeta(1)-1))
                        kcount = kcount + self%nzeta(1)
                    end if

                    ! ppp
                    if (self%nzeta(2) > 0) then
                        call rot_xyz(3, 3, 3, self%nzeta(2), rmat_pp, pmat, &
                                     self%xsmmf_ppp1, self%xsmmf_ppp2, &
                                     ptr2(:, kcount:kcount+3*self%nzeta(2)-1))
                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! ppd
                    if (self%nzeta(3) > 0) then
                        call rot_xyz(3, 3, 5, self%nzeta(3), rmat_pp, dmat, &
                                     self%xsmmf_ppd1, self%xsmmf_ppd2, &
                                     ptr2(:, kcount:kcount+5*self%nzeta(3)-1))
                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    M(icount:icount+2, jcount:jcount+2, :) = &
                        reshape(ptr2, [3, 3, naux12])

                    jcount = jcount + 3
                end if

                if (jinc(3)) then
                    kcount = 1
                    call self%spl(2, 3, iset, jset)%evaluate_batch_nomask(r, &
                                                                        reftmp)
                    ptr2(1:15, 1:naux12) => reftmp(1:15*naux12)

                    ! pds
                    if (self%nzeta(1) > 0) then
                        call rot_xy(3, 5, self%nzeta(1), rmat_pd, &
                                    self%xsmmf_pds, &
                                    ptr2(:, kcount:kcount+self%nzeta(1)-1))
                        kcount = kcount + self%nzeta(1)
                    end if

                    ! pdp
                    if (self%nzeta(2) > 0) then
                        call rot_xyz(3, 5, 3, self%nzeta(2), rmat_pd, pmat, &
                                     self%xsmmf_pdp1, self%xsmmf_pdp2, &
                                     ptr2(:, kcount:kcount+3*self%nzeta(2)-1))
                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! pdd
                    if (self%nzeta(3) > 0) then
                        call rot_xyz(3, 5, 5, self%nzeta(3), rmat_pd, dmat, &
                                     self%xsmmf_pdd1, self%xsmmf_pdd2, &
                                     ptr2(:, kcount:kcount+5*self%nzeta(3)-1))
                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    M(icount:icount+2, jcount:jcount+4, :) = &
                        reshape(ptr2, [3, 5, naux12])

                    jcount = jcount + 5
                end if

                icount = icount + 3
                jcount = jcount - jnumlm
            end if

            if (iinc(3)) then
                if (jinc(1)) then
                    kcount = 1
                    call self%spl(3, 1, iset, jset)%evaluate_batch_nomask(r, &
                                                                        reftmp)
                    ptr2(1:5, 1:naux12) => reftmp(1:5*naux12)

                    ! dss
                    if (self%nzeta(1) > 0) then
                        call rot_y(5, self%nzeta(1), rmat_d, &
                                   ptr2(:, kcount:kcount+self%nzeta(1)-1))
                        kcount = kcount + self%nzeta(1)
                    end if

                    ! dsp
                    if (self%nzeta(2) > 0) then
                        call rot_yz(5, 3, self%nzeta(2), rmat_dp, &
                                    self%xsmmf_sdp, &
                                    ptr2(:, kcount:kcount+3*self%nzeta(2)-1))
                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! dsd
                    if (self%nzeta(3) > 0) then
                        call rot_yz(5, 5, self%nzeta(3), rmat_dd, &
                                    self%xsmmf_sdd, &
                                    ptr2(:, kcount:kcount+5*self%nzeta(3)-1))
                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    M(icount:icount+4, jcount, :) = ptr2

                    jcount = jcount + 1
                end if

                if (jinc(2)) then
                    kcount = 1
                    call self%spl(3, 2, iset, jset)%evaluate_batch_nomask(r, &
                                                                        reftmp)
                    ptr2(1:15, 1:naux12) => reftmp(1:15*naux12)

                    ! dps
                    if (self%nzeta(1) > 0) then
                        call rot_xy(5, 3, self%nzeta(1), rmat_dp, &
                                    self%xsmmf_pds, &
                                    ptr2(:, kcount:kcount+self%nzeta(1)-1))
                        kcount = kcount + self%nzeta(1)
                    end if

                    ! dpp
                    if (self%nzeta(2) > 0) then
                        call rot_xyz(5, 3, 3, self%nzeta(2), rmat_dp, pmat, &
                                     self%xsmmf_pdp1, self%xsmmf_pdp2, &
                                     ptr2(:, kcount:kcount+3*self%nzeta(2)-1))
                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! dpd
                    if (self%nzeta(3) > 0) then
                        call rot_xyz(5, 3, 5, self%nzeta(3), rmat_dp, dmat, &
                                     self%xsmmf_pdd1, self%xsmmf_pdd2, &
                                     ptr2(:, kcount:kcount+5*self%nzeta(3)-1))
                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    M(icount:icount+4, jcount:jcount+2, :) = &
                        reshape(ptr2, [5, 3, naux12])

                    jcount = jcount + 3
                end if

                if (jinc(3)) then
                    kcount = 1
                    call self%spl(3, 3, iset, jset)%evaluate_batch_nomask(r, &
                                                                        reftmp)
                    ptr2(1:25, 1:naux12) => reftmp(1:25*naux12)

                    ! dds
                    if (self%nzeta(1) > 0) then
                        call rot_xy(5, 5, self%nzeta(1), rmat_dd, &
                                    self%xsmmf_dds, &
                                    ptr2(:, kcount:kcount+self%nzeta(1)-1))
                        kcount = kcount + self%nzeta(1)
                    end if

                    ! ddp
                    if (self%nzeta(2) > 0) then
                        call rot_xyz(5, 5, 3, self%nzeta(2), rmat_dd, pmat, &
                                     self%xsmmf_ddp1, self%xsmmf_ddp2, &
                                     ptr2(:, kcount:kcount+3*self%nzeta(2)-1))
                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! ddd
                    if (self%nzeta(3) > 0) then
                        call rot_xyz(5, 5, 5, self%nzeta(3), rmat_dd, dmat, &
                                     self%xsmmf_ddd1, self%xsmmf_ddd2, &
                                     ptr2(:, kcount:kcount+5*self%nzeta(3)-1))
                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    M(icount:icount+4, jcount:jcount+4, :) = &
                        reshape(ptr2, [5, 5, naux12])

                    jcount = jcount + 5
                end if

                icount = icount + 5
                jcount = jcount - jnumlm
            end if

            jcount = jcount + jnumlm
            if (jset < self%numset2) icount = icount - inumlm

            end associate
            end associate
        end do

        end associate
        end associate
    end do
end subroutine evaluate_offsite


pure subroutine evaluate_onsite(self, M)
    !! Returns the onsite mapping coefficients.
    class(GieseYorkKernel_type), intent(in) :: self
    real(dp), dimension(:, :, :), intent(out) :: M
        !! output mapping coefficient matrix

    M = self%onsiteM
end subroutine evaluate_onsite


subroutine evaluate_offsite_deriv(self, x, y, z, r, dM)
    !! Returns the offsite mapping coefficient derivatives for the chosen
    !! orbitals and orientation.
    class(GieseYorkKernel_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :, :, :), intent(out) :: dM
        !! output mapping coefficient matrix derivatives

    integer :: ider, icount, jcount, kcount, naux12, iset, jset
    real(dp), dimension(3) :: drdx
    real(dp), dimension(3, 3) :: dxyzdx, dvABdA, dvCMdA, pmat, rmat_p
    real(dp), dimension(3, 3, 3) :: dpmat, drmat_p
    real(dp), dimension(5, 5) :: dmat, rmat_d
    real(dp), dimension(5, 5, 3) :: ddmat, drmat_d
    real(dp), dimension(9, 9) :: rmat_pp
    real(dp), dimension(9, 9, 3) :: drmat_pp
    real(dp), dimension(15, 15) :: rmat_pd, rmat_dp
    real(dp), dimension(15, 15, 3) :: drmat_pd, drmat_dp
    real(dp), dimension(25, 25) :: rmat_dd
    real(dp), dimension(25, 25, 3) :: drmat_dd
    real(dp), dimension(:), pointer :: ptr1
    real(dp), dimension(:, :), pointer :: ptr2, dptr1
    real(dp), dimension(:, :, :), pointer :: dptr2
    real(dp), dimension(5*5*(self%naux1+self%naux2)), target :: reftmp, reftmp2
    real(dp), dimension(5*5*(self%naux1+self%naux2)*3), target :: dreftmp

    pmat = get_rotation_matrix([x, y, z], [0._dp, 0._dp, 1._dp])
    rmat_p = transpose(pmat)
    rmat_pp = get_rmat_xx(3, rmat_p)

    dvABdA = get_vAB_derivA([x, y, z], r)
    dvCMdA = 0._dp
    dpmat = get_rotation_matrix_deriv([x, y, z], dvABdA, &
                                      [0._dp, 0._dp, 1._dp], dvCMdA, 1)

    do ider = 1, 3
        drmat_p(:, :, ider) = transpose(dpmat(:, :, ider))
        drmat_pp(:, :, ider) = &
            get_rmat_xy(3, 3, rmat_p, drmat_p(:, :, ider)) &
            + get_rmat_xy(3, 3, drmat_p(:, :, ider), rmat_p)
    end do

    if (self%maxdorb >= 1) then
        dmat = get_d_orbital_rotmat(pmat)
        rmat_d = transpose(dmat)
        rmat_pd = get_rmat_xy(3, 5, rmat_p, rmat_d)
        rmat_dp = get_rmat_xy(5, 3, rmat_d, rmat_p)

        ddmat = get_d_orbital_rotmat_deriv(pmat, dpmat)

        do ider = 1, 3
            drmat_d(:, :, ider) = transpose(ddmat(:, :, ider))
            drmat_pd(:, :, ider) = &
                     get_rmat_xy(3, 5, rmat_p, drmat_d(:, :, ider)) &
                     + get_rmat_xy(3, 5, drmat_p(:, :, ider), rmat_d)
            drmat_dp(:, :, ider) = &
                     get_rmat_xy(5, 3, rmat_d, drmat_p(:, :, ider)) &
                     + get_rmat_xy(5, 3, drmat_d(:, :, ider), rmat_p)
        end do

        if (self%maxdorb >= 2) then
            rmat_dd = get_rmat_xx(5, rmat_d)
            do ider = 1, 3
                drmat_dd(:, :, ider) = &
                    get_rmat_xy(5, 5, rmat_d, drmat_d(:, :, ider)) &
                    + get_rmat_xy(5, 5, drmat_d(:, :, ider), rmat_d)
            end do
        end if
    end if

    drdx(:) = [-x, -y, -z]
    dxyzdx(:, 1) = [(x**2 - 1._dp) / r, x * y / r, x * z / r]
    dxyzdx(:, 2) = [x * y / r, (y**2 - 1._dp) / r, y * z / r]
    dxyzdx(:, 3) = [x * z / r, y * z / r, (z**2 - 1._dp) / r]

    naux12 = self%naux1 + self%naux2

    icount = 1
    do iset = 1, self%numset1
        associate(iinc => self%inc1(:, iset))
        associate(inumlm => self%numlm1(iset))

        jcount = 1
        do jset = 1, self%numset2
            associate(jinc => self%inc2(:, jset))
            associate(jnumlm => self%numlm2(jset))

            if (iinc(1)) then
                if (jinc(1)) then
                    kcount = 1

                    call self%spl(1, 1, iset, jset)%evaluate_batch_nomask(r, &
                                                            reftmp, reftmp2)
                    ptr1(1:naux12) => reftmp(1:naux12)
                    dptr1(1:naux12, 1:3) => dreftmp(1:3*naux12)
                    dptr1(:, 3) = reftmp2(1:naux12)

                    ! sss
                    if (self%nzeta(1) > 0) then
                        do ider = 1, 3
                            dptr1(kcount:kcount+self%nzeta(1)-1, ider) = &
                                dptr1(kcount:kcount+self%nzeta(1)-1, 3) &
                                * drdx(ider)
                        end do

                        kcount = kcount + self%nzeta(1)
                    end if

                    ! ssp
                    if (self%nzeta(2) > 0) then
                        call rot_z(3, self%nzeta(2), rmat_p, self%xsmmf_p, &
                                   dptr1(kcount:kcount+3*self%nzeta(2)-1, 3))

                        do ider = 1, 3
                            dptr1(kcount:kcount+3*self%nzeta(2)-1, ider) = &
                                dptr1(kcount:kcount+3*self%nzeta(2)-1, 3) &
                                * drdx(ider)

                            call drot_z(3, self%nzeta(2), drmat_p(:, :, ider), &
                                        self%xsmmf_p, &
                                        ptr1(kcount:kcount+3*self%nzeta(2)-1), &
                                        dptr1(kcount:kcount+3*self%nzeta(2)-1, ider))
                        end do

                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! ssd
                    if (self%nzeta(3) > 0) then
                        call rot_z(5, self%nzeta(3), rmat_d, self%xsmmf_d, &
                                   dptr1(kcount:kcount+5*self%nzeta(3)-1, 3))

                        do ider = 1, 3
                            dptr1(kcount:kcount+5*self%nzeta(3)-1, ider) = &
                                dptr1(kcount:kcount+5*self%nzeta(3)-1, 3) &
                                * drdx(ider)

                            call drot_z(5, self%nzeta(3), drmat_d(:, :, ider), &
                                        self%xsmmf_d, &
                                        ptr1(kcount:kcount+5*self%nzeta(3)-1), &
                                        dptr1(kcount:kcount+5*self%nzeta(3)-1, ider))
                        end do

                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    dM(icount, jcount, :, :) = dptr1

                    jcount = jcount + 1
                end if

                if (jinc(2)) then
                    kcount = 1

                    call self%spl(1, 2, iset, jset)%evaluate_batch_nomask(r, &
                                                            reftmp, reftmp2)
                    ptr2(1:3, 1:naux12) => reftmp(1:3*naux12)
                    dptr2(1:3, 1:naux12, 1:3) => dreftmp(1:9*naux12)
                    dptr2(:, :, 3) = reshape(reftmp2(1:3*naux12), [3, naux12])

                    ! sps
                    if (self%nzeta(1) > 0) then
                        call rot_y(3, self%nzeta(1), rmat_p, &
                                   dptr2(:, kcount:kcount+self%nzeta(1)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+self%nzeta(1)-1, ider) = &
                                dptr2(:, kcount:kcount+self%nzeta(1)-1, 3) &
                                * drdx(ider)

                            call drot_y(3, self%nzeta(1), drmat_p(:, :, ider), &
                                        ptr2(:, kcount:kcount+self%nzeta(1)-1), &
                                        dptr2(:, kcount:kcount+self%nzeta(1)-1, ider))
                        end do

                        kcount = kcount + self%nzeta(1)
                    end if

                    ! spp
                    if (self%nzeta(2) > 0) then
                        call rot_yz(3, 3, self%nzeta(2), rmat_pp, &
                                    self%xsmmf_spp, &
                                    dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider) = &
                                dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3) &
                                * drdx(ider)

                            call drot_yz(3, 3, self%nzeta(2), drmat_pp(:, :, ider), &
                                         self%xsmmf_spp, &
                                         ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                         dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))
                        end do

                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! spd
                    if (self%nzeta(3) > 0) then
                        call rot_yz(3, 5, self%nzeta(3), rmat_pd, &
                                    self%xsmmf_spd, &
                                    dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider) = &
                                dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3) &
                                * drdx(ider)

                            call drot_yz(3, 5, self%nzeta(3), drmat_pd(:, :, ider), &
                                         self%xsmmf_spd, &
                                         ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                         dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))
                        end do

                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    dM(icount, jcount:jcount+2, :, :) = dptr2

                    jcount = jcount + 3
                end if

                if (jinc(3)) then
                    kcount = 1

                    call self%spl(1, 3, iset, jset)%evaluate_batch_nomask(r, &
                                                            reftmp, reftmp2)
                    ptr2(1:5, 1:naux12) => reftmp(1:5*naux12)
                    dptr2(1:5, 1:naux12, 1:3) => dreftmp(1:15*naux12)
                    dptr2(:, :, 3) = reshape(reftmp2(1:5*naux12), [5, naux12])

                    ! sds
                    if (self%nzeta(1) > 0) then
                        call rot_y(5, self%nzeta(1), rmat_d, &
                                   dptr2(:, kcount:kcount+self%nzeta(1)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+self%nzeta(1)-1, ider) = &
                                dptr2(:, kcount:kcount+self%nzeta(1)-1, 3) &
                                * drdx(ider)

                            call drot_y(5, self%nzeta(1), drmat_d(:, :, ider), &
                                        ptr2(:, kcount:kcount+self%nzeta(1)-1), &
                                        dptr2(:, kcount:kcount+self%nzeta(1)-1, ider))
                        end do

                        kcount = kcount + self%nzeta(1)
                    end if

                    ! sdp
                    if (self%nzeta(2) > 0) then
                        call rot_yz(5, 3, self%nzeta(2), rmat_dp, &
                                    self%xsmmf_sdp, &
                                    dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider) = &
                                dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3) &
                                * drdx(ider)

                            call drot_yz(5, 3, self%nzeta(2), drmat_dp(:, :, ider), &
                                         self%xsmmf_sdp, &
                                         ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                         dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))
                        end do

                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! sdd
                    if (self%nzeta(3) > 0) then
                        call rot_yz(5, 5, self%nzeta(3), rmat_dd, &
                                    self%xsmmf_sdd, &
                                    dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider) = &
                                dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3) &
                                * drdx(ider)

                            call drot_yz(5, 5, self%nzeta(3), drmat_dd(:, :, ider), &
                                         self%xsmmf_sdd, &
                                         ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                         dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))
                        end do

                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    dM(icount, jcount:jcount+4, :, :) = dptr2

                    jcount = jcount + 5
                end if

                icount = icount + 1
                jcount = jcount - jnumlm
            end if

            if (iinc(2)) then
                if (jinc(1)) then
                    kcount = 1

                    call self%spl(2, 1, iset, jset)%evaluate_batch_nomask(r, &
                                                            reftmp, reftmp2)
                    ptr2(1:3, 1:naux12) => reftmp(1:3*naux12)
                    dptr2(1:3, 1:naux12, 1:3) => dreftmp(1:9*naux12)
                    dptr2(:, :, 3) = reshape(reftmp2(1:3*naux12), [3, naux12])

                    ! pss
                    if (self%nzeta(1) > 0) then
                        call rot_y(3, self%nzeta(1), rmat_p, &
                                   dptr2(:, kcount:kcount+self%nzeta(1)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+self%nzeta(1)-1, ider) = &
                                dptr2(:, kcount:kcount+self%nzeta(1)-1, 3) &
                                * drdx(ider)

                            call drot_y(3, self%nzeta(1), drmat_p(:, :, ider), &
                                        ptr2(:, kcount:kcount+self%nzeta(1)-1), &
                                        dptr2(:, kcount:kcount+self%nzeta(1)-1, ider))
                        end do

                        kcount = kcount + self%nzeta(1)
                    end if

                    ! psp
                    if (self%nzeta(2) > 0) then
                        call rot_yz(3, 3, self%nzeta(2), rmat_pp, &
                                    self%xsmmf_spp, &
                                    dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider) = &
                                dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3) &
                                * drdx(ider)

                            call drot_yz(3, 3, self%nzeta(2), drmat_pp(:, :, ider), &
                                         self%xsmmf_spp, &
                                         ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                         dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))
                        end do

                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! psd
                    if (self%nzeta(3) > 0) then
                        call rot_yz(3, 5, self%nzeta(3), rmat_pd, &
                                    self%xsmmf_spd, &
                                    dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider) = &
                                dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3) &
                                * drdx(ider)

                            call drot_yz(3, 5, self%nzeta(3), drmat_pd(:, :, ider), &
                                         self%xsmmf_spd, &
                                         ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                         dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))
                        end do

                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    dM(icount:icount+2, jcount, :, :) = dptr2

                    jcount = jcount + 1
                end if

                if (jinc(2)) then
                    kcount = 1

                    call self%spl(2, 2, iset, jset)%evaluate_batch_nomask(r, &
                                                            reftmp, reftmp2)
                    ptr2(1:9, 1:naux12) => reftmp(1:9*naux12)
                    dptr2(1:9, 1:naux12, 1:3) => dreftmp(1:27*naux12)
                    dptr2(:, :, 3) = reshape(reftmp2(1:9*naux12), [9, naux12])

                    ! pps
                    if (self%nzeta(1) > 0) then
                        call rot_xy(3, 3, self%nzeta(1), rmat_pp, &
                                    self%xsmmf_pps, &
                                    dptr2(:, kcount:kcount+self%nzeta(1)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+self%nzeta(1)-1, ider) = &
                                dptr2(:, kcount:kcount+self%nzeta(1)-1, 3) &
                                * drdx(ider)

                            call drot_xy(3, 3, self%nzeta(1), drmat_pp(:, :, ider), &
                                         self%xsmmf_pps, &
                                         ptr2(:, kcount:kcount+self%nzeta(1)-1), &
                                         dptr2(:, kcount:kcount+self%nzeta(1)-1, ider))
                        end do

                        kcount = kcount + self%nzeta(1)
                    end if

                    ! ppp
                    if (self%nzeta(2) > 0) then
                        call rot_xyz(3, 3, 3, self%nzeta(2), rmat_pp, pmat, &
                                     self%xsmmf_ppp1, self%xsmmf_ppp2, &
                                     dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider) = &
                                dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3) &
                                * drdx(ider)

                            call drot_xyz(3, 3, 3, self%nzeta(2), rmat_pp, dpmat(:, :, ider), &
                                          self%xsmmf_ppp1, self%xsmmf_ppp2, &
                                          ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                          dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))

                            call drot_xyz(3, 3, 3, self%nzeta(2), drmat_pp(:, :, ider), pmat, &
                                          self%xsmmf_ppp1, self%xsmmf_ppp2, &
                                          ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                          dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))
                        end do

                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! ppd
                    if (self%nzeta(3) > 0) then
                        call rot_xyz(3, 3, 5, self%nzeta(3), rmat_pp, dmat, &
                                     self%xsmmf_ppd1, self%xsmmf_ppd2, &
                                     dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider) = &
                                dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3) &
                                * drdx(ider)

                            call drot_xyz(3, 3, 5, self%nzeta(3), rmat_pp, &
                                          ddmat(:, :, ider), &
                                          self%xsmmf_ppd1, self%xsmmf_ppd2, &
                                          ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                          dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))

                            call drot_xyz(3, 3, 5, self%nzeta(3), drmat_pp(:, :, ider), &
                                          dmat, self%xsmmf_ppd1, self%xsmmf_ppd2, &
                                          ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                          dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))
                        end do

                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    dM(icount:icount+2, jcount:jcount+2, :, :) = &
                        reshape(dptr2, [3, 3, naux12, 3])

                    jcount = jcount + 3
                end if

                if (jinc(3)) then
                    kcount = 1

                    call self%spl(2, 3, iset, jset)%evaluate_batch_nomask(r, &
                                                            reftmp, reftmp2)
                    ptr2(1:15, 1:naux12) => reftmp(1:15*naux12)
                    dptr2(1:15, 1:naux12, 1:3) => dreftmp(1:45*naux12)
                    dptr2(:, :, 3) = reshape(reftmp2(1:15*naux12), [15, naux12])

                    ! pds
                    if (self%nzeta(1) > 0) then
                        call rot_xy(3, 5, self%nzeta(1), rmat_pd, &
                                    self%xsmmf_pds, &
                                    dptr2(:, kcount:kcount+self%nzeta(1)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+self%nzeta(1)-1, ider) = &
                                dptr2(:, kcount:kcount+self%nzeta(1)-1, 3) &
                                * drdx(ider)

                            call drot_xy(3, 5, self%nzeta(1), drmat_pd(:, :, ider), &
                                         self%xsmmf_pds, &
                                         ptr2(:, kcount:kcount+self%nzeta(1)-1), &
                                         dptr2(:, kcount:kcount+self%nzeta(1)-1, ider))
                        end do

                        kcount = kcount + self%nzeta(1)
                    end if

                    ! pdp
                    if (self%nzeta(2) > 0) then
                        call rot_xyz(3, 5, 3, self%nzeta(2), rmat_pd, pmat, &
                                     self%xsmmf_pdp1, self%xsmmf_pdp2, &
                                     dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider) = &
                                dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3) &
                                * drdx(ider)

                            call drot_xyz(3, 5, 3, self%nzeta(2), rmat_pd, &
                                          dpmat(:, :, ider), self%xsmmf_pdp1, self%xsmmf_pdp2, &
                                          ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                          dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))

                            call drot_xyz(3, 5, 3, self%nzeta(2), drmat_pd(:, :, ider), &
                                          pmat, self%xsmmf_pdp1, self%xsmmf_pdp2, &
                                          ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                          dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))
                        end do

                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! pdd
                    if (self%nzeta(3) > 0) then
                        call rot_xyz(3, 5, 5, self%nzeta(3), rmat_pd, dmat, &
                                     self%xsmmf_pdd1, self%xsmmf_pdd2, &
                                     dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider) = &
                                dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3) &
                                * drdx(ider)

                            call drot_xyz(3, 5, 5, self%nzeta(3), rmat_pd, ddmat(:, :, ider), &
                                          self%xsmmf_pdd1, self%xsmmf_pdd2, &
                                          ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                          dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))

                            call drot_xyz(3, 5, 5, self%nzeta(3), drmat_pd(:, :, ider), &
                                          dmat, self%xsmmf_pdd1, self%xsmmf_pdd2, &
                                          ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                          dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))
                        end do

                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    dM(icount:icount+2, jcount:jcount+4, :, :) = &
                        reshape(dptr2, [3, 5, naux12, 3])

                    jcount = jcount + 5
                end if

                icount = icount + 3
                jcount = jcount - jnumlm
            end if

            if (iinc(3)) then
                if (jinc(1)) then
                    kcount = 1

                    call self%spl(3, 1, iset, jset)%evaluate_batch_nomask(r, &
                                                            reftmp, reftmp2)
                    ptr2(1:5, 1:naux12) => reftmp(1:5*naux12)
                    dptr2(1:5, 1:naux12, 1:3) => dreftmp(1:15*naux12)
                    dptr2(:, :, 3) = reshape(reftmp2(1:5*naux12), [5, naux12])

                    ! dss
                    if (self%nzeta(1) > 0) then
                        call rot_y(5, self%nzeta(1), rmat_d, &
                                   dptr2(:, kcount:kcount+self%nzeta(1)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+self%nzeta(1)-1, ider) = &
                                dptr2(:, kcount:kcount+self%nzeta(1)-1, 3) &
                                * drdx(ider)

                            call drot_y(5, self%nzeta(1), drmat_d(:, :, ider), &
                                        ptr2(:, kcount:kcount+self%nzeta(1)-1), &
                                        dptr2(:, kcount:kcount+self%nzeta(1)-1, ider))
                        end do

                        kcount = kcount + self%nzeta(1)
                    end if

                    ! dsp
                    if (self%nzeta(2) > 0) then
                        call rot_yz(5, 3, self%nzeta(2), rmat_dp, &
                                    self%xsmmf_sdp, &
                                    dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider) = &
                                dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3) &
                                * drdx(ider)

                            call drot_yz(5, 3, self%nzeta(2), &
                                         drmat_dp(:, :, ider), self%xsmmf_sdp, &
                                         ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                         dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))
                        end do

                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! dsd
                    if (self%nzeta(3) > 0) then
                        call rot_yz(5, 5, self%nzeta(3), rmat_dd, self%xsmmf_sdd, &
                                    dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider) = &
                                dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3) &
                                * drdx(ider)

                            call drot_yz(5, 5, self%nzeta(3), drmat_dd(:, :, ider), &
                                         self%xsmmf_sdd, &
                                         ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                         dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))
                        end do

                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    dM(icount:icount+4, jcount, :, :) = dptr2

                    jcount = jcount + 1
                end if

                if (jinc(2)) then
                    kcount = 1

                    call self%spl(3, 2, iset, jset)%evaluate_batch_nomask(r, &
                                                            reftmp, reftmp2)
                    ptr2(1:15, 1:naux12) => reftmp(1:15*naux12)
                    dptr2(1:15, 1:naux12, 1:3) => dreftmp(1:45*naux12)
                    dptr2(:, :, 3) = reshape(reftmp2(1:15*naux12), [15, naux12])

                    ! dps
                    if (self%nzeta(1) > 0) then
                        call rot_xy(5, 3, self%nzeta(1), rmat_dp, &
                                    self%xsmmf_pds, &
                                    dptr2(:, kcount:kcount+self%nzeta(1)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+self%nzeta(1)-1, ider) = &
                                dptr2(:, kcount:kcount+self%nzeta(1)-1, 3) &
                                * drdx(ider)

                            call drot_xy(5, 3, self%nzeta(1), &
                                         drmat_dp(:, :, ider), self%xsmmf_pds, &
                                         ptr2(:, kcount:kcount+self%nzeta(1)-1), &
                                         dptr2(:, kcount:kcount+self%nzeta(1)-1, ider))
                        end do

                        kcount = kcount + self%nzeta(1)
                    end if

                    ! dpp
                    if (self%nzeta(2) > 0) then
                        call rot_xyz(5, 3, 3, self%nzeta(2), rmat_dp, pmat, &
                                     self%xsmmf_pdp1, self%xsmmf_pdp2, &
                                     dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider) = &
                                dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3) &
                                * drdx(ider)

                            call drot_xyz(5, 3, 3, self%nzeta(2), rmat_dp, dpmat(:, :, ider), &
                                          self%xsmmf_pdp1, self%xsmmf_pdp2, &
                                          ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                          dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))

                            call drot_xyz(5, 3, 3, self%nzeta(2), drmat_dp(:, :, ider), &
                                          pmat, self%xsmmf_pdp1, self%xsmmf_pdp2, &
                                          ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                          dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))
                        end do

                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! dpd
                    if (self%nzeta(3) > 0) then
                        call rot_xyz(5, 3, 5, self%nzeta(3), rmat_dp, dmat, &
                                     self%xsmmf_pdd1, self%xsmmf_pdd2, &
                                     dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider) = &
                                dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3) &
                                * drdx(ider)

                            call drot_xyz(5, 3, 5, self%nzeta(3), rmat_dp, ddmat(:, :, ider), &
                                          self%xsmmf_pdd1, self%xsmmf_pdd2, &
                                          ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                          dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))

                            call drot_xyz(5, 3, 5, self%nzeta(3), drmat_dp(:, :, ider), &
                                          dmat, self%xsmmf_pdd1, self%xsmmf_pdd2, &
                                          ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                          dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))
                        end do

                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    dM(icount:icount+4, jcount:jcount+2, :, :) = &
                        reshape(dptr2, [5, 3, naux12, 3])

                    jcount = jcount + 3
                end if

                if (jinc(3)) then
                    kcount = 1

                    call self%spl(3, 3, iset, jset)%evaluate_batch_nomask(r, &
                                                            reftmp, reftmp2)
                    ptr2(1:25, 1:naux12) => reftmp(1:25*naux12)
                    dptr2(1:25, 1:naux12, 1:3) => dreftmp(1:75*naux12)
                    dptr2(:, :, 3) = reshape(reftmp2(1:25*naux12), [25, naux12])

                    ! dds
                    if (self%nzeta(1) > 0) then
                        call rot_xy(5, 5, self%nzeta(1), rmat_dd, &
                                    self%xsmmf_dds, &
                                    dptr2(:, kcount:kcount+self%nzeta(1)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+self%nzeta(1)-1, ider) = &
                                dptr2(:, kcount:kcount+self%nzeta(1)-1, 3) &
                                * drdx(ider)

                            call drot_xy(5, 5, self%nzeta(1), drmat_dd(:, :, ider), &
                                         self%xsmmf_dds, &
                                         ptr2(:, kcount:kcount+self%nzeta(1)-1), &
                                         dptr2(:, kcount:kcount+self%nzeta(1)-1, ider))
                        end do

                        kcount = kcount + self%nzeta(1)
                    end if

                    ! ddp
                    if (self%nzeta(2) > 0) then
                        call rot_xyz(5, 5, 3, self%nzeta(2), rmat_dd, pmat, &
                                     self%xsmmf_ddp1, self%xsmmf_ddp2, &
                                     dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider) = &
                                dptr2(:, kcount:kcount+3*self%nzeta(2)-1, 3) &
                                * drdx(ider)

                            call drot_xyz(5, 5, 3, self%nzeta(2), rmat_dd, dpmat(:, :, ider), &
                                          self%xsmmf_ddp1, self%xsmmf_ddp2, &
                                          ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                          dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))

                            call drot_xyz(5, 5, 3, self%nzeta(2), drmat_dd(:, :, ider), pmat, &
                                          self%xsmmf_ddp1, self%xsmmf_ddp2, &
                                          ptr2(:, kcount:kcount+3*self%nzeta(2)-1), &
                                          dptr2(:, kcount:kcount+3*self%nzeta(2)-1, ider))
                        end do

                        kcount = kcount + 3*self%nzeta(2)
                    end if

                    ! ddd
                    if (self%nzeta(3) > 0) then
                        call rot_xyz(5, 5, 5, self%nzeta(3), rmat_dd, dmat, &
                                     self%xsmmf_ddd1, self%xsmmf_ddd2, &
                                     dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3))

                        do ider = 1, 3
                            dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider) = &
                                dptr2(:, kcount:kcount+5*self%nzeta(3)-1, 3) &
                                * drdx(ider)

                            call drot_xyz(5, 5, 5, self%nzeta(3), rmat_dd, ddmat(:, :, ider), &
                                          self%xsmmf_ddd1, self%xsmmf_ddd2, &
                                          ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                          dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))

                            call drot_xyz(5, 5, 5, self%nzeta(3), drmat_dd(:, :, ider), dmat, &
                                          self%xsmmf_ddd1, self%xsmmf_ddd2, &
                                          ptr2(:, kcount:kcount+5*self%nzeta(3)-1), &
                                          dptr2(:, kcount:kcount+5*self%nzeta(3)-1, ider))
                        end do

                        kcount = kcount + 5*self%nzeta(3)
                    end if

                    dM(icount:icount+4, jcount:jcount+4, :, :) = &
                        reshape(dptr2, [5, 5, naux12, 3])

                    jcount = jcount + 5
                end if

                icount = icount + 5
                jcount = jcount - jnumlm
            end if

            jcount = jcount + jnumlm
            if (jset < self%numset2) icount = icount - inumlm

            end associate
            end associate
        end do

        end associate
        end associate
    end do
end subroutine evaluate_offsite_deriv


subroutine initialize(self, tbpar_dir, isym, jsym, ibasis, jbasis, ilmax, &
                      jlmax, inzeta, jnzeta, eps)
    !! Initialization by reading from *onsiteM.1cm and *offsiteM.2cm files.
    class(GieseYorkKernel_type), intent(out) :: self
    character(len=*), intent(in) :: tbpar_dir
        !! path to the directory with the parameter files
    character(len=2), intent(in) :: isym
        !! element symbols for the first kind
    character(len=2), intent(in) :: jsym
        !! element symbols for the second kind
    type(BasisAO_type), intent(in) :: ibasis
        !! basis set description for the first kind
    type(BasisAO_type), intent(in) :: jbasis
        !! basis set description for the second kind
    integer, intent(in) :: ilmax
        !! index of the maximum angular momentum to be considered
        !! in multipole expansions for the first kind
    integer, intent(in) :: jlmax
        !! index of the maximum angular momentum to be considered
        !! in multipole expansions for the second kind
    integer, intent(in) :: inzeta
        !! number of auxiliary radial functions for the first kind
    integer, intent(in) :: jnzeta
        !! number of auxiliary radial functions for the second kind
    real(dp), intent(in) :: eps
        !! accuracy threshold for building the cutoff radius

    character(len=1), dimension(3), parameter :: angmom = ['S', 'P', 'D']
    character(len=2) :: ilabel, jlabel
    character(len=:), allocatable :: filename, isym_set, jsym_set
    integer :: i, il, ilm, jlm, iset, jset, izeta
    logical :: has_on1c

    self%numset1 = ibasis%get_nr_of_zetas()
    self%numset2 = jbasis%get_nr_of_zetas()
    allocate(self%inc1(MAXL, self%numset1))
    allocate(self%inc2(MAXL, self%numset2))
    allocate(self%numlm1(self%numset1))
    allocate(self%numlm2(self%numset2))
    allocate(self%spl(MAXL_2CM, MAXL_2CM, self%numset1, self%numset2))

    self%ilmax1 = ilmax
    self%ilmax2 = jlmax
    self%nzeta1 = inzeta
    self%nzeta2 = jnzeta

    do il = 1, MAXL_2CM
        self%nzeta(il) = 0
        if (il <= self%ilmax1) self%nzeta(il) = self%nzeta(il) + self%nzeta1
        if (il <= self%ilmax2) self%nzeta(il) = self%nzeta(il) + self%nzeta2
    end do

    write(ilabel, '((i0), (a1))') inzeta, angmom(self%ilmax1)
    write(jlabel, '((i0), (a1))') jnzeta, angmom(self%ilmax2)
    self%naux1 = self%nzeta1 * self%ilmax1**2
    self%naux2 = self%nzeta2 * self%ilmax2**2

    has_on1c = isym == jsym
    if (has_on1c) then
        i = ibasis%get_nr_of_orbitals()
        allocate(self%onsiteM(i, i, self%naux1))
    end if

    allocate(self%order(self%naux1+self%naux2))
    i = 1
    do il = 1, max(self%ilmax1, self%ilmax2)
        if (il <= self%ilmax1) then
            do izeta = 1, self%nzeta1
                do ilm = orbital_index(il), orbital_index(il+1)-1
                    self%order(i) = ilm + (izeta - 1) * self%ilmax1**2
                    i = i + 1
                end do
            end do
        end if

        if (il <= self%ilmax2) then
            do izeta = 1, self%nzeta2
                do ilm = orbital_index(il), orbital_index(il+1)-1
                    self%order(i) = ilm + self%nzeta1 * self%ilmax1**2 &
                                    + (izeta - 1) * self%ilmax2**2
                    i = i + 1
                end do
            end do
        end if
    end do

    self%rcut = 0._dp

    ilm = 1
    do iset = 1, self%numset1
        isym_set = trim(isym) // repeat('+', iset-1)
        self%inc1(:, iset) = ibasis%get_subset_included_subshells(iset)
        self%numlm1(iset) = ibasis%get_subset_nr_of_orbitals(iset)
        associate(iinc => self%inc1(:, iset))
        associate(inumlm => self%numlm1(iset))

        jlm = 1
        do jset = 1, self%numset2
            jsym_set = trim(jsym) // repeat('+', jset-1)
            self%inc2(:, jset) = jbasis%get_subset_included_subshells(jset)
            self%numlm2(jset) = jbasis%get_subset_nr_of_orbitals(jset)
            associate(jinc => self%inc2(:, jset))
            associate(jnumlm => self%numlm2(jset))

            if (has_on1c) then
                ! Onsite, one-center
                filename = trim(tbpar_dir) // '/'  // isym_set // '-' // &
                           jsym_set // '_onsiteM_' // ilabel // '.1cm'
                if (is_master()) then
                    print *, 'Reading onsiteM table from ', filename
                end if

                self%onsiteM(ilm:ilm+inumlm-1, jlm:jlm+jnumlm-1, :) = &
                                    read_1cm_file(filename, iinc, jinc, &
                                                  inumlm, jnumlm, self%naux1)
                jlm = jlm + jnumlm
            end if

            end associate
            end associate

            ! Offsite, two-center
            call initialize_off2c_spl()
        end do

        ilm = ilm + inumlm
        end associate
        end associate
    end do

    if (is_master()) print *

    self%maxdorb = count([(self%ilmax1 >= 3) .or. (self%ilmax2 >= 3), &
                          any(self%inc1(3, :)), any(self%inc2(3, :))])

    call self%initialize_xsmmf()


    contains


    subroutine initialize_off2c_spl()

        integer :: iaux, il_, jl_, ilm_, jlm_, ir, izeta, kl, klm, numr
        integer, dimension(:), allocatable :: signs
        real(dp) :: dr, rcut
        real(dp), dimension(:, :, :, :), allocatable :: table1, table2, table12

        allocate(signs(self%naux2))

        filename = trim(tbpar_dir) // '/'  // isym_set // '-' // jsym_set // &
                   '_offsiteM_' // ilabel // '.2cm'
        if (is_master()) print *, 'Reading offsiteM table from ', filename
        call read_2cm_file(filename, self%naux1, numr, dr, rcut, table1)

        filename = trim(tbpar_dir) // '/'  // jsym_set // '-' // isym_set // &
                   '_offsiteM_' // jlabel  // '.2cm'
        if (is_master()) print *, 'Reading offsiteM table from ', filename
        call read_2cm_file(filename, self%naux2, numr, dr, rcut, table2)

        call assert(size(table1, dim=2) == size(table2, dim=2), 'The table' // &
                    ' pair should have the same number of radial grid points')

        allocate(table12(self%naux1+self%naux2, numr, MAXLM_2CM, MAXLM_2CM))

        do il_ = 1, MAXL_2CM
            do ilm_ = orbital_index(il_), orbital_index(il_+1)-1
                do jl_ = 1, MAXL_2CM
                    ! Set up the sign flips
                    iaux = 1
                    do izeta = 1, self%nzeta2
                        do kl = 1, self%ilmax2
                            do klm = orbital_index(kl), orbital_index(kl+1)-1
                                signs(iaux) = (-1)**(il_ + jl_ + kl - 3)
                                iaux = iaux + 1
                            end do
                        end do
                    end do

                    ! Join the two tables
                    do jlm_ = orbital_index(jl_), orbital_index(jl_+1)-1
                        do ir = 1, numr
                            table12(1:self%naux1, ir, jlm_, ilm_) = &
                                    table1(:, ir, jlm_, ilm_)

                            table12(self%naux1+1:self%naux1+self%naux2, &
                                    ir, jlm_, ilm_) = &
                                    signs * table2(:, ir, ilm_, jlm_)
                        end do
                    end do
                end do
            end do
        end do

        do il_ = 1, MAXL_2CM
            do jl_ = 1, MAXL_2CM
                if (self%inc1(il_, iset) .and. self%inc2(jl_, jset)) then
                    call self%spl(il_, jl_, iset, jset)%initialize_batch(dr, &
                                                        dr, rcut, il_, jl_,&
                                                        table12, self%order)

                    call self%spl(il_, jl_, iset, jset)%adjust_rcut_batch(eps)
                    self%rcut = max(self%rcut, &
                                    self%spl(il_, jl_, iset, jset)%rcut)
                end if
            end do
        end do

    end subroutine initialize_off2c_spl
end subroutine initialize


subroutine initialize_xsmmf(self)
    !! Initialization the self%xsmmf_* LibXSMM functions.
    class(GieseYorkKernel_type), intent(inout) :: self

    real(dp), parameter :: alpha = 1._dp
    real(dp), parameter :: beta = 0._dp

    ! Each x function (x={p,d}) is for replacing
    ! the following calls in rot_z:
    ! call libxsmm_dgemm('N', 'N', lmz, nzz, lmz, 1._dp, &
    !                    rmat, lmz, tmp1, lmz, 0._dp, tmp2, lmz)
    call libxsmm_dmmdispatch(self%xsmmf_p, 3, self%nzeta(2), 3, &
                             alpha=alpha, beta=beta, lda=3, ldb=3, ldc=3)
    call libxsmm_dmmdispatch(self%xsmmf_d, 5, self%nzeta(3), 5, &
                             alpha=alpha, beta=beta, lda=5, ldb=5, ldc=5)

    ! Each xys function (x/y={p,d}) is for replacing
    ! the following calls in rot_xy:
    ! call libxsmm_dgemm('N', 'N', lmxy, nzz, lmxy, 1._dp, &
    !                   rmat, lmxy, a, lmxy, 0._dp, tmp, lmxy)
    call libxsmm_dmmdispatch(self%xsmmf_pps, 9, self%nzeta(1), 9, &
                             alpha=alpha, beta=beta, lda=9, ldb=9, ldc=9)
    call libxsmm_dmmdispatch(self%xsmmf_pds, 15, self%nzeta(1), 15, &
                             alpha=alpha, beta=beta, lda=15, ldb=15, ldc=15)
    call libxsmm_dmmdispatch(self%xsmmf_dds, 25, self%nzeta(1), 25, &
                             alpha=alpha, beta=beta, lda=25, ldb=25, ldc=25)

    ! Each syz function (y/z={p,d}) is for replacing
    ! the following calls in rot_yz:
    ! call libxsmm_dgemm('N', 'N', lmyz, nzz, lmyz, 1._dp, &
    !                    rmat, lmyz, tmp1, lmyz, 0._dp, tmp2, lmyz)
    call libxsmm_dmmdispatch(self%xsmmf_spp, 9, self%nzeta(2), 9, &
                             alpha=alpha, beta=beta, lda=9, ldb=9, ldc=9)
    call libxsmm_dmmdispatch(self%xsmmf_sdp, 15, self%nzeta(2), 15, &
                             alpha=alpha, beta=beta, lda=15, ldb=15, ldc=15)
    call libxsmm_dmmdispatch(self%xsmmf_spd, 15, self%nzeta(3), 15, &
                             alpha=alpha, beta=beta, lda=15, ldb=15, ldc=15)
    call libxsmm_dmmdispatch(self%xsmmf_sdd, 25, self%nzeta(3), 25, &
                             alpha=alpha, beta=beta, lda=25, ldb=25, ldc=25)

    ! Each xyz{1,2} function pair (x/y/z={p,d}) is for replacing
    ! the following calls in rot_xyz:
    ! call libxsmm_dgemm('N', 'N', lmxy, lmz*nzz, lmxy, 1._dp, rmat_xy, lmxy, &
    !                    a, lmxy, 0._dp, tmp, lmxy)
    ! call libxsmm_dgemm('N', 'N', lmxy, lmz, lmz, 1._dp, tmp(:, j:j+lmz-1), &
    !                    lmxy, rmat_z, lmz, 0._dp, a(:, j:j+lmz-1), lmxy)
    call libxsmm_dmmdispatch(self%xsmmf_ppp1, 9, self%nzeta(2)*3, 9, &
                             alpha=alpha, beta=beta, lda=9, ldb=9, ldc=9)
    call libxsmm_dmmdispatch(self%xsmmf_ppp2, 9, 3, 3, &
                             alpha=alpha, beta=beta, lda=9, ldb=3, ldc=9)

    call libxsmm_dmmdispatch(self%xsmmf_ppd1, 9, self%nzeta(3)*5, 9, &
                             alpha=alpha, beta=beta, lda=9, ldb=9, ldc=9)
    call libxsmm_dmmdispatch(self%xsmmf_ppd2, 9, 5, 5, &
                             alpha=alpha, beta=beta, lda=9, ldb=5, ldc=9)

    call libxsmm_dmmdispatch(self%xsmmf_pdp1, 15, self%nzeta(2)*3, 15, &
                             alpha=alpha, beta=beta, lda=15, ldb=15, ldc=15)
    call libxsmm_dmmdispatch(self%xsmmf_pdp2, 15, 3, 3, &
                             alpha=alpha, beta=beta, lda=15, ldb=3, ldc=15)

    call libxsmm_dmmdispatch(self%xsmmf_pdd1, 15, self%nzeta(3)*5, 15, &
                             alpha=alpha, beta=beta, lda=15, ldb=15, ldc=15)
    call libxsmm_dmmdispatch(self%xsmmf_pdd2, 15, 5, 5, &
                             alpha=alpha, beta=beta, lda=15, ldb=5, ldc=15)

    call libxsmm_dmmdispatch(self%xsmmf_ddp1, 25, self%nzeta(2)*3, 25, &
                             alpha=alpha, beta=beta, lda=25, ldb=25, ldc=25)
    call libxsmm_dmmdispatch(self%xsmmf_ddp2, 25, 3, 3, &
                             alpha=alpha, beta=beta, lda=25, ldb=3, ldc=25)

    call libxsmm_dmmdispatch(self%xsmmf_ddd1, 25, self%nzeta(3)*5, 25, &
                             alpha=alpha, beta=beta, lda=25, ldb=25, ldc=25)
    call libxsmm_dmmdispatch(self%xsmmf_ddd2, 25, 5, 5, &
                             alpha=alpha, beta=beta, lda=25, ldb=5, ldc=25)
end subroutine initialize_xsmmf


function read_1cm_file(filename, iinc, jinc, inumlm, jnumlm, naux) result(m)
    !! Reads the content of the given 1cm file and returns it
    !! in "packed" form in the output matrix.
    character(len=*), intent(in) :: filename
        !! name of the parameter file
    logical, dimension(:), intent(in) :: iinc
        !! whether an angular momentum is present in the first basis subset
    logical, dimension(:), intent(in) :: jinc
        !! whether an angular momentum is present in the second basis subset
    integer, intent(in) :: inumlm
        !! number of orbitals in the first basis subset
    integer, intent(in) :: jnumlm
        !! number of orbitals in the second basis subset
    integer, intent(in) :: naux
        !! number of auxiliary basis functions
    real(dp), dimension(inumlm, jnumlm, naux) :: m

    integer :: fhandle, icount, jcount, il, jl, ilm, jlm
    real(dp), dimension(naux, MAXLM_1CM) :: table

    open(newunit=fhandle, file=filename, status='old', action='read')

    icount = 1
    do il = 1, MAXL_1CM
        do ilm = orbital_index(il), orbital_index(il+1)-1
            do jl = 1, MAXL_1CM
                do jlm = orbital_index(jl), orbital_index(jl+1)-1
                    read(fhandle, *) table(:, jlm)
                end do
            end do

            if (.not. iinc(il)) cycle

            jcount = 1
            do jl = 1, MAXL_1CM
                if (.not. jinc(jl)) cycle

                do jlm = orbital_index(jl), orbital_index(jl+1)-1
                    m(icount, jcount, :) = table(:, jlm)
                    jcount = jcount + 1
                end do
            end do

            icount = icount + 1
        end do
    end do

    close(fhandle)
end function read_1cm_file


subroutine read_2cm_file(filename, inaux, numr, dr, rcut, table)
    !! Returns the contents of the given '2cm' mapping coefficient file.
    character(len=*), intent(in) :: filename
        !! name of the parameter file
    integer, intent(in) :: inaux
        !! number of auxiliary basis functions for the first element
    integer, intent(out) :: numr
        !! number of radial grid points
    real(dp), intent(out) :: dr
        !! grid spacing
    real(dp), intent(out) :: rcut
        !! grid cutoff
    real(dp), dimension(:, :, :, :), allocatable, intent(out) :: table
        !! mapping coefficient table

    integer :: fhandle, i, il, jl, ilm, jlm
    character(len=24) :: separator
    character(len=1024) :: buffer

    open(newunit=fhandle, file=filename, status='old', action='read')

    read(fhandle, *) dr, numr
    rcut = numr * dr
    allocate(table(inaux, numr, MAXLM_2CM, MAXLM_2CM))

    do il = 1, MAXL_2CM
        do ilm = orbital_index(il), orbital_index(il+1)-1
            do jl = 1, MAXL_2CM
                do jlm = orbital_index(jl), orbital_index(jl+1)-1
                    read(fhandle, '(a)') buffer
                    separator = '# ' // trim(lm_labels(ilm)) // '_' // &
                                trim(lm_labels(jlm))
                    call assert(trim(buffer) == trim(separator), &
                            'Unexpected separator ' // '"' // trim(buffer) // &
                            '", expected "' // trim(separator))

                    do i = 1, numr
                        read(fhandle, *) table(:, i, jlm, ilm)
                    end do
                end do
            end do
        end do
    end do

    close(fhandle)
end subroutine read_2cm_file


subroutine initialize_batch(self, r0, dr, rcut, il, jl, table, order)
    !! Initializes a batch spline interpolator.
    class(MultipoleBatchSpline_type), intent(out) :: self
    real(dp), intent(in) :: r0
        !! first grid point
    real(dp), intent(in) :: dr
        !! grid spacing
    real(dp), intent(in) :: rcut
        !! grid cutoff
    integer, intent(in) :: il
        !! subshell index for the first element
    integer, intent(in) :: jl
        !! subshell index for the second element
    real(dp), dimension(:, :, :, :), intent(in) :: table
        !! (naux, numr, MAXLM_2CM, MAXLM_2CM) integral table
    integer, dimension(:), intent(in) :: order
        !! indices used for sorting the auxiliary basis functions

    integer :: i, ilm, jlm, iaux, naux, inumlm, jnumlm

    naux = size(table, dim=1)

    self%dr = dr
    self%r0 = r0
    self%rcut = rcut

    inumlm = il**2 - (il - 1)**2
    jnumlm = jl**2 - (jl - 1)**2

    self%numint = inumlm * jnumlm * naux
    self%numr = size(table, dim=2)
    allocate(self%table(self%numint, self%numr))

    i = 1
    do iaux = 1, naux
        do jlm = orbital_index(jl), orbital_index(jl+1)-1
            do ilm = orbital_index(il), orbital_index(il+1)-1
                self%table(i, :) = table(order(iaux), :, jlm, ilm)
                i = i + 1
            end do
        end do
    end do
end subroutine initialize_batch


pure function get_rmat_xx(lmx, bx) result(rmat)
    !! Get the 2D rotation matrix for two main basis function blocks
    !! of the same angular momentum.
    integer, intent(in) :: lmx
    real(dp), dimension(lmx, lmx), intent(in) :: bx
    real(dp), dimension(lmx**2, lmx**2) :: rmat

    integer :: i, j

    do i = 1, lmx
        do j = 1, lmx
            rmat(lmx*(j-1)+1:lmx*j, lmx*(i-1)+1:lmx*i) = bx(j, i) * bx
        end do
    end do
end function get_rmat_xx


pure function get_rmat_xy(lmx, lmy, bx, by) result(rmat)
    !! Get the 2D rotation matrix for two main basis function blocks
    !! of different angular momentum.
    integer, intent(in) :: lmx
    integer, intent(in) :: lmy
    real(dp), dimension(lmx, lmx), intent(in) :: bx
    real(dp), dimension(lmy, lmy), intent(in) :: by
    real(dp), dimension(lmx*lmy, lmx*lmy) :: rmat

    integer :: i, j

    do i = 1, lmy
        do j = 1, lmy
            rmat(lmx*(j-1)+1:lmx*j, lmx*(i-1)+1:lmx*i) = by(j, i) * bx
        end do
    end do
end function get_rmat_xy


subroutine rot_z(lmz, nzz, rmat, xsmmf, a)
    !! Apply rotation for one auxiliary basis function block.
    integer, intent(in) :: lmz
    integer, intent(in) :: nzz
    real(dp), dimension(lmz, lmz), intent(in) :: rmat
    type(libxsmm_dmmfunction), intent(in) :: xsmmf
    real(dp), dimension(lmz*nzz), intent(inout) :: a

    real(dp), dimension(lmz, nzz) :: tmp1, tmp2

    tmp1 = reshape(a, [lmz, nzz])
    call libxsmm_dmmcall(xsmmf, rmat, tmp1, tmp2)
    a = reshape(tmp2, [lmz*nzz])
end subroutine rot_z


pure subroutine rot_y(lmy, nzz, rmat, a)
    !! Apply rotation for one main basis function block.
    integer, intent(in) :: lmy
    integer, intent(in) :: nzz
    real(dp), dimension(lmy, lmy), intent(in) :: rmat
    real(dp), dimension(lmy, nzz), intent(inout) :: a

    a = matmul(rmat, a)
end subroutine rot_y


subroutine rot_yz(lmy, lmz, nzz, rmat, xsmmf, a)
    !! Apply rotation for one main basis function block and one auxiliary
    !! function block.
    integer, intent(in) :: lmy
    integer, intent(in) :: lmz
    integer, intent(in) :: nzz
    real(dp), dimension(lmy*lmz, lmy*lmz), intent(in) :: rmat
    type(libxsmm_dmmfunction), intent(in) :: xsmmf
    real(dp), dimension(lmy, lmz*nzz), intent(inout) :: a

    integer :: lmyz
    real(dp), dimension(lmy*lmz, nzz) :: tmp1, tmp2

    lmyz = lmy * lmz
    tmp1 = reshape(a, [lmyz, nzz])
    call libxsmm_dmmcall(xsmmf, rmat, tmp1, tmp2)
    a = reshape(tmp2, [lmy, lmz*nzz])
end subroutine rot_yz


subroutine rot_xy(lmx, lmy, nzz, rmat, xsmmf, a)
    !! Apply rotation for two main basis function blocks.
    integer, intent(in) :: lmx
    integer, intent(in) :: lmy
    integer, intent(in) :: nzz
    real(dp), dimension(lmx*lmy, lmx*lmy), intent(in) :: rmat
    type(libxsmm_dmmfunction), intent(in) :: xsmmf
    real(dp), dimension(lmx*lmy, nzz), intent(inout) :: a

    integer :: lmxy
    real(dp), dimension(lmx*lmy, nzz) :: tmp

    lmxy = lmx * lmy
    call libxsmm_dmmcall(xsmmf, rmat, a, tmp)
    a = reshape(tmp, [lmxy, nzz])
end subroutine rot_xy


subroutine rot_xyz(lmx, lmy, lmz, nzz, rmat_xy, rmat_z, xsmmf1, xsmmf2, a)
    !! Apply rotation for two main basis function blocks and one auxiliary
    !! function block.
    integer, intent(in) :: lmx
    integer, intent(in) :: lmy
    integer, intent(in) :: lmz
    integer, intent(in) :: nzz
    real(dp), dimension(lmx*lmy, lmx*lmy), intent(in) :: rmat_xy
    real(dp), dimension(lmz, lmz), intent(in) :: rmat_z
    type(libxsmm_dmmfunction), intent(in) :: xsmmf1
    type(libxsmm_dmmfunction), intent(in) :: xsmmf2
    real(dp), dimension(lmx*lmy, lmz*nzz), intent(inout) :: a

    integer :: i, j, lmxy
    real(dp), dimension(lmx*lmy, lmz*nzz) :: tmp

    lmxy = lmx * lmy

    call libxsmm_dmmcall(xsmmf1, rmat_xy, a, tmp)

    j = 1
    do i = 1, nzz
        call libxsmm_dmmcall(xsmmf2, tmp(:, j:j+lmz-1), rmat_z, a(:, j:j+lmz-1))
        j = j + lmz
    end do
end subroutine rot_xyz


subroutine drot_z(lmz, nzz, rmat, xsmmf, a_in, a_out)
    !! Adds the derivatives for rotating auxiliary basis function block.
    integer, intent(in) :: lmz
    integer, intent(in) :: nzz
    real(dp), dimension(lmz, lmz), intent(in) :: rmat
    type(libxsmm_dmmfunction), intent(in) :: xsmmf
    real(dp), dimension(lmz*nzz), intent(in) :: a_in
    real(dp), dimension(lmz*nzz), intent(inout) :: a_out

    real(dp), dimension(lmz, nzz) :: tmp1, tmp2

    tmp1 = reshape(a_in, [lmz, nzz])
    call libxsmm_dmmcall(xsmmf, rmat, tmp1, tmp2)
    a_out = a_out + reshape(tmp2, [lmz*nzz])
end subroutine drot_z


pure subroutine drot_y(lmy, nzz, rmat, a_in, a_out)
    !! Adds the derivatives for rotating one main basis function block.
    integer, intent(in) :: lmy
    integer, intent(in) :: nzz
    real(dp), dimension(lmy, lmy), intent(in) :: rmat
    real(dp), dimension(lmy, nzz), intent(in) :: a_in
    real(dp), dimension(lmy, nzz), intent(inout) :: a_out

    a_out = a_out + matmul(rmat, a_in)
end subroutine drot_y


subroutine drot_yz(lmy, lmz, nzz, rmat, xsmmf, a_in, a_out)
    !! Adds the derivatives for rotating one main basis function block
    !! and one auxiliary function block.
    integer, intent(in) :: lmy
    integer, intent(in) :: lmz
    integer, intent(in) :: nzz
    real(dp), dimension(lmy*lmz, lmy*lmz), intent(in) :: rmat
    type(libxsmm_dmmfunction), intent(in) :: xsmmf
    real(dp), dimension(lmy, lmz*nzz), intent(inout) :: a_in
    real(dp), dimension(lmy, lmz*nzz), intent(inout) :: a_out

    integer :: lmyz
    real(dp), dimension(lmy*lmz, nzz) :: tmp1, tmp2

    lmyz = lmy * lmz
    tmp1 = reshape(a_in, [lmyz, nzz])
    call libxsmm_dmmcall(xsmmf, rmat, tmp1, tmp2)
    a_out = a_out + reshape(tmp2, [lmy, lmz*nzz])
end subroutine drot_yz


subroutine drot_xy(lmx, lmy, nzz, rmat, xsmmf, a_in, a_out)
    !! Adds the derivatives for rotating two main basis function blocks.
    integer, intent(in) :: lmx
    integer, intent(in) :: lmy
    integer, intent(in) :: nzz
    real(dp), dimension(lmx*lmy, lmx*lmy), intent(in) :: rmat
    type(libxsmm_dmmfunction), intent(in) :: xsmmf
    real(dp), dimension(lmx*lmy, nzz), intent(in) :: a_in
    real(dp), dimension(lmx*lmy, nzz), intent(inout) :: a_out

    integer :: lmxy
    real(dp), dimension(lmx*lmy, nzz) :: tmp

    lmxy = lmx * lmy
    call libxsmm_dmmcall(xsmmf, rmat, a_in, tmp)
    a_out = a_out + reshape(tmp, [lmxy, nzz])
end subroutine drot_xy


subroutine drot_xyz(lmx, lmy, lmz, nzz, rmat_xy, rmat_z, xsmmf1, xsmmf2, &
                    a_in, a_out)
    !! Adds the derivatives for rotating two main basis function blocks
    !! and one auxiliary function block.
    integer, intent(in) :: lmx
    integer, intent(in) :: lmy
    integer, intent(in) :: lmz
    integer, intent(in) :: nzz
    real(dp), dimension(lmx*lmy, lmx*lmy), intent(in) :: rmat_xy
    real(dp), dimension(lmz, lmz), intent(in) :: rmat_z
    type(libxsmm_dmmfunction), intent(in) :: xsmmf1
    type(libxsmm_dmmfunction), intent(in) :: xsmmf2
    real(dp), dimension(lmx*lmy, lmz*nzz), intent(in) :: a_in
    real(dp), dimension(lmx*lmy, lmz*nzz), intent(inout) :: a_out

    integer :: i, j, lmxy
    real(dp), dimension(lmx*lmy, lmz*nzz) :: tmp1
    real(dp), dimension(lmx*lmy, lmz) :: tmp2

    lmxy = lmx * lmy

    call libxsmm_dmmcall(xsmmf1, rmat_xy, a_in, tmp1)

    j = 1
    do i = 1, nzz
        call libxsmm_dmmcall(xsmmf2, tmp1(:, j:j+lmz-1), rmat_z, tmp2)
        a_out(:, j:j+lmz-1) = a_out(:, j:j+lmz-1) + tmp2
        j = j + lmz
    end do
end subroutine drot_xyz

end module tibi_giese_york_kernel
