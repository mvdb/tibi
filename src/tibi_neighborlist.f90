!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_neighborlist
!! Module for abstract neighbor list type

use tibi_atoms, only: Atoms_type
use tibi_constants, only: dp
implicit none


type, abstract :: NeighborList_type
    !! An abstract neighbor list class
    logical :: both_ways = .false.
        !! whether two neighbor pairs are associated with every
        !! pair of atoms, one for each 'direction'
    integer, dimension(:), allocatable :: neighbors
        !! the actual number of neighbors for each atom
    integer, dimension(:), allocatable :: neighbor_indices
        !! neighbor atom indices
    real(dp), dimension(3, 3) :: original_cell
        !! cell vectors from the last neighborlist build
    integer, dimension(:), allocatable :: original_kinds
        !! array of atomic kinds from the last neighborlist build
    real(dp), dimension(:, :), allocatable :: original_positions
        !! array of atomic positions from the last neighborlist build
    real(dp), dimension(:), allocatable :: r
        !! distances to neighbors
    real(dp), dimension(:, :), allocatable :: rcuts
        !! cutoff radii for all kind pairs
    real(dp) :: rsame = 1e-8_dp
        !! threshold below which 2 atoms are considered to be the same
    logical :: self_interaction = .false.
        !! whether atoms are neighbors of themselves
    integer, dimension(:), allocatable :: start_indices
        !! where the slice of each atom begins in the
        !! indices, vec, r, and translations arrays
    real(dp), dimension(:, :), allocatable :: translations
        !! the associated translations on the Bravais lattice
    real(dp), dimension(:, :), allocatable :: vec
        !! normal vectors pointing to the neighbors
    contains
    procedure(update), deferred :: update
end type NeighborList_type


interface
    subroutine update(self, atoms)
        !! Updates the neighborlist based on the new atoms object.
        import Atoms_type, NeighborList_type
        implicit none
        class(NeighborList_type), intent(inout) :: self
        type(Atoms_type), intent(in) :: atoms
    end subroutine update
end interface


contains


subroutine get_translations(atoms, rcut, translations)
    !! Calculates all the lattice translations that need to be considered
    !! on the basis of an interatomic distance cutoff 'rcut'.
    type(Atoms_type), intent(in) :: atoms
    real(dp) :: rcut
        !! cutoff distance
    real(dp), dimension(:, :), allocatable, intent(out) :: translations
        !! Bravais lattice vectors (in a.u.)

    integer :: i, j, k, counter, num
    integer, dimension(3) :: Nabc

    if (atoms%is_periodic) then
        Nabc = [(int(rcut * norm2(atoms%invcell(i, :))) + 1, i = 1, 3)]
        num = product([(2 * Nabc(i) + 1, i = 1, 3)])
    else
        Nabc = [0, 0, 0]
        num = 1
    end if

    allocate(translations(3, num))

    counter = 1
    do i = -Nabc(1), Nabc(1)
        do j = -Nabc(2), Nabc(2)
            do k = -Nabc(3), Nabc(3)
                translations(:, counter) = matmul(atoms%cell, [i, j, k])
                counter = counter + 1
            end do
        end do
    end do
end subroutine get_translations

end module tibi_neighborlist
