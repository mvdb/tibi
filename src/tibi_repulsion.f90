!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_repulsion
!! Module for describing interatomic repulsion

use tibi_atoms, only: Atoms_type
use tibi_calculator, only: Calculator_type
use tibi_constants, only: dp
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_repulsion_2c, only: Repulsion2cCalculator_type
use tibi_repulsion_3c, only: Repulsion3cCalculator_type
use tibi_timer, only: task_timer
use tibi_utils_prog, only: assert
implicit none


type, extends(Calculator_type) :: RepulsionCalculator_type
    !! A calculator for the interatomic repulsion contributions
    !!
    !! Basically a wrapper around the two- and three-center calculators.
    type(Repulsion2cCalculator_type) :: rep2c
    type(Repulsion3cCalculator_type), allocatable :: rep3c
    integer :: repulsion_expansion = 2
        !! term at which to truncate the multicenter expansion of the
        !! repulsive energy
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: calculate_energy
    procedure :: calculate_forces
end type RepulsionCalculator_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(RepulsionCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('repulsion_expansion')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%repulsion_expansion
        call assert(self%repulsion_expansion == 2 .or. &
                    self%repulsion_expansion == 3, &
                    'repulsion_expansion must be either 2 or 3')
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input, atoms)
    !! Initializes the RepulsionCalculator from the given instances of
    !! [[tibi_input:Input_type]] and [[tibi_atoms:Atoms_type]].
    class(RepulsionCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms

    call self%fetch_input_keywords(input)
    call self%rep2c%initialize(input, atoms)

    if (self%repulsion_expansion > 2) then
        allocate(self%rep3c)
        call self%rep3c%initialize(input, atoms)
    end if

    call task_timer%add_timer('repulsion_3c_energy')
    call task_timer%add_timer('repulsion_3c_forces')
end subroutine initialize


subroutine calculate_energy(self, atoms)
    !! Calculates the (total) energy for the given [[tibi_atoms:Atoms_type]]
    !! object, which is stored in atoms%energy.
    class(RepulsionCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms

    real(dp) :: energy

    call self%rep2c%calculate_energy(atoms)
    energy = atoms%energy

    if (self%repulsion_expansion > 2) then
        call task_timer%start_timer('repulsion_3c_energy')
        call self%rep3c%calculate_energy_given_nl(atoms, self%rep2c%nl)
        energy = energy + atoms%energy
        call task_timer%stop_timer('repulsion_3c_energy')
    end if

    atoms%energy = energy
end subroutine calculate_energy


subroutine calculate_forces(self, atoms, do_stress)
    !! Calculates the atomic forces for the given [[tibi_atoms:Atoms_type]]
    !! object, which are stored in atoms%forces.
    class(RepulsionCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    logical, intent(in) :: do_stress
        !! whether to also compute the stress tensor for periodic structures

    real(dp), dimension(:, :), allocatable :: forces, stress

    call self%rep2c%calculate_forces(atoms, do_stress=do_stress)

    if (self%repulsion_expansion > 2) then
        call task_timer%start_timer('repulsion_3c_forces')
        allocate(forces(3, atoms%natom))
        forces = atoms%forces

        if (do_stress) then
            allocate(stress(3, 3))
            stress = atoms%stress
        end if

        call self%rep3c%calculate_forces_given_nl(atoms, self%rep2c%nl, &
                                                  do_stress=do_stress)
        atoms%forces = atoms%forces + forces
        if (do_stress) atoms%stress = atoms%stress + stress
        call task_timer%stop_timer('repulsion_3c_forces')
    end if
end subroutine calculate_forces

end module tibi_repulsion
