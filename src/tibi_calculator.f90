!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_calculator
!! Module for general (abstract) calculator attributes

use tibi_atoms, only: Atoms_type
use tibi_utils_prog, only: assert
implicit none


type, abstract :: Calculator_type
    !! An abstract Calculator class
    contains
    procedure :: calculate_energy
    procedure :: calculate_forces
end type Calculator_type


contains


subroutine calculate_energy(self, atoms)
    !! Calculates the (total) energy for the given [[tibi_atoms:Atoms_type]]
    !! object, which is stored in atoms%energy.
    class(Calculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms

    call assert(.false., 'Energy not yet implemented!')
end subroutine calculate_energy


subroutine calculate_forces(self, atoms, do_stress)
    !! Calculates the atomic forces for the given [[tibi_atoms:Atoms_type]]
    !! object, which are stored in atoms%forces.
    class(Calculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    logical, intent(in) :: do_stress
        !! whether to also compute the stress tensor for periodic structures

    call assert(.false., 'Forces not yet implemented!')
end subroutine calculate_forces

end module tibi_calculator
