!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

program tibi
!! The main Tibi program

use tibi_atoms, only: Atoms_type
use tibi_calculator_set, only: CalculatorSet_type
use tibi_driver_base, only: BaseDriver_type
use tibi_driver_init, only: initialize_driver
use tibi_input, only: Input_type
use tibi_timer, only: task_timer, &
                      Timer_type
use tibi_timer_set, only: global_timer
use tibi_utils_prog, only: is_master, &
                           print_date_and_time, &
                           print_program_header, &
                           print_version_string
implicit none


integer :: narg
character(len=4), parameter :: program_name = 'Tibi'
character(len=128) :: input_file, geometry_file, option
type(Atoms_type) :: atoms
class(BaseDriver_type), allocatable :: driver
type(CalculatorSet_type) :: calculatorset
type(Input_type) :: input
type(Timer_type), target :: main_timer

narg = command_argument_count()

! First the cases with an early exit
if (narg == 1) then
    call get_command_argument(1, option)

    if (trim(option) == '--version') then
        if (is_master()) call print_version_string(program_name)
        stop 0
    elseif (trim(option) == '--help') then
        if (is_master()) call print_help_message()
        stop 0
    else
        if (is_master()) call print_help_message()
        stop 1
    end if
elseif (narg /= 2) then
    if (is_master()) call print_help_message()
    stop 1
end if

! Now we assume the two arguments refer to an input file
! and a geometry file and can start the real work
call get_command_argument(1, input_file)
call get_command_argument(2, geometry_file)

if (is_master()) then
    call print_date_and_time(program_name // ' started')
    call print_program_header(program_name)
    call print_program_banner()
end if

call global_timer%add_timer(main_timer, 'main')
call global_timer%add_timer(task_timer, 'task', parent='main')
call main_timer%add_timer('init')
call main_timer%add_timer('task')

call main_timer%start_timer('init')
call input%read_input_file(input_file)
if (is_master()) then
    call input%print_input_description()
end if

call atoms%read_geometry_file(geometry_file)
if (is_master()) then
    call atoms%print_geometry_description()
end if

call calculatorset%fetch_input_keywords(input)
call calculatorset%initialize_calculators(input, atoms)

call initialize_driver(driver, input, atoms)
call main_timer%stop_timer('init')

call main_timer%start_timer('task')
call driver%run_task(atoms, calculatorset)
call main_timer%stop_timer('task')

if (is_master()) then
    call global_timer%print_timing_tables()
    call print_date_and_time(program_name // ' stopped')
end if


contains


subroutine print_help_message()
    !! Prints out the help message.

    print *, 'Usage: tibi input_file geometry_file'
    print *, 'Version: tibi --version'
    print *, 'Help: tibi --help'
end subroutine print_help_message


subroutine print_program_banner()
    !! Prints the Tibi program banner.
    !!
    !! ASCII art adapted from
    !! <https://user.xmission.com/~emailbox/ascii_cats.htm>.
    print *, '                                               .--.'
    print *, '                                               `.  \'
    print *, '                                                 \  \'
    print *, '                                                  .  \'
    print *, '                                                  :   .'
    print *, '                                                  |    .'
    print *, '                                                  |    :'
    print *, '                                                  |    |'
    print *, '  ..._  ___                                       |    |'
    print *, ' `."".`""""""--..___                              |    |'
    print *, ' ,-\  \             ""-...__         _____________/    |'
    print *, ' / ` " "                    `""""""""                  .'
    print *, ' \                                                      L'
    print *, ' (>                         _____   _   ____    _        \'
    print *, '/                          |_   _| (_) | __ )  (_)        \'
    print *, '\_    ___..---.              | |   | | |  _ \  | |         L'
    print *, '  `--"         ".            | |   | | | |_) | | |          \'
    print *, '                 .           |_|   |_| |____/  |_|           \_'
    print *, '                _/`.                                           `.._'
    print *, '             ."     -.                                             `.'
    print *, '            /     __.-Y     /""""""-...___,...--------.._            |'
    print *, '           /   _."    |    /                " .      \   "---..._    |'
    print *, '          /   /      /    /                _,. "    ,/           |   |'
    print *, '          \_,"     _."   /              /""     _,-"            _|   |'
    print *, '                  "     /               `-----""               /     |'
    print *, '                  `...-"                                       !_____)'
    print *
end subroutine print_program_banner

end program tibi
