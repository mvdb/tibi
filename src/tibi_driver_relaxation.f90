!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_driver_relaxation
!! Module for the atomic structure relaxation driver

use tibi_atoms, only: Atoms_type
use tibi_calculator, only: Calculator_type
use tibi_constants, only: angstrom, &
                          dp, &
                          electronvolt
use tibi_driver_base, only: BaseDriver_type
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_optimizer_base, only: BaseOptimizer_type
use tibi_optimizer_lbfgs, only: LBFGSOptimizer_type
use tibi_utils_prog, only: assert, &
                           is_master
implicit none


type, extends(BaseDriver_type) :: RelaxationDriver_type
    !! Class for performing structural relaxation tasks, in which
    !! a (local) energy minimum is searched with respect to the
    !! atomic positions
    class(BaseOptimizer_type), allocatable :: optimizer
        !! object which determines the next ionic displacement step to take
        !! in the relaxation cycle
    character(len=128) :: relax_algo = 'lbfgs'
        !! name of the relaxation algorithm to apply
    real(dp) :: relax_fmax = 0.05_dp * electronvolt / angstrom
        !! convergence threshold for the force magnitudes
    integer :: relax_history = 50
        !! number of most recent iteration results to be stored in the
        !! (LBFGS) relaxation algorithm
    real(dp) :: relax_initial_curvature = 100._dp * electronvolt / angstrom**2
        !! initial curvature in all directions, used in representing
        !! the initial inverse Hessian in the LBFGS method
    integer :: relax_maxiter = 250
        !! maximum number of relaxation iterations
    real(dp) :: relax_maxstep = 0.5_dp * angstrom
        !! largest allowed atom displacement in a relaxation step
        !! (if necessary, steps are rescaled to satisfy this requirement)
    logical :: relax_must_converge = .true.
        !! whether to abort in case force convergence is not reached within
        !! the maximum number of relaxation iterations
    logical :: relax_trajectory_append = .true.
        !! whether to append or overwrite self%relax_trajectory_file
    character(len=256) :: relax_trajectory_file = 'relax.xyz'
        !! file to which the geometry will be written
    integer :: relax_trajectory_frequency = 1
        !! frequency at which self%relax_trajectory_file will be updated
    logical :: relax_vc = .false.
        !! whether to perform a variable cell ('vc') relaxation in which
        !! the cell vectors are allowed to vary
    real(dp) :: relax_vc_maxstep = 0.5_dp * angstrom
        !! largest allowed change in the cell vector components in a vc-relax
        !! step (if necessary, steps are rescaled to satisfy this requirement)
    real(dp) :: relax_vc_scale = 0._dp
        !! preconditioning for the scaled forces (0 -> cube root of cell volume)
    real(dp) :: relax_vc_smax = 1e-3_dp * electronvolt / angstrom**3
        !! convergence threshold for the stress magnitudes in a vc-relaxation
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: run_task => run_task_relaxation
end type RelaxationDriver_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(RelaxationDriver_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('relax_algo')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_algo
        call assert(self%relax_algo == 'lbfgs', 'Only the following ' // &
                    'relaxation algorithm is available: "lbfgs"')
    end if

    buffer = input%fetch_keyword('relax_fmax')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_fmax
        call assert(self%relax_fmax > 0, 'relax_fmax must be strictly positive')
        self%relax_fmax = self%relax_fmax * electronvolt / angstrom
    end if

    buffer = input%fetch_keyword('relax_history')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_history
        call assert(self%relax_history > 0, &
                    'relax_history must be strictly positive')
    end if

    buffer = input%fetch_keyword('relax_initial_curvature')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_initial_curvature
        call assert(self%relax_initial_curvature > 0._dp, &
                    'relax_initial_curvature must be strictly positive')
        self%relax_initial_curvature = self%relax_initial_curvature &
                                       * electronvolt / angstrom**2
    end if

    buffer = input%fetch_keyword('relax_maxiter')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_maxiter
        call assert(self%relax_maxiter > 0, &
                    'relax_maxiter must be strictly positive')
    end if

    buffer = input%fetch_keyword('relax_maxstep')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_maxstep
        call assert(self%relax_maxstep > 0._dp, &
                    'relax_maxstep must be strictly positive')
        self%relax_maxstep = self%relax_maxstep * angstrom
    end if

    buffer = input%fetch_keyword('relax_must_converge')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_must_converge
    end if

    buffer = input%fetch_keyword('relax_trajectory_append')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_trajectory_append
    end if

    buffer = input%fetch_keyword('relax_trajectory_file')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_trajectory_file
        call assert(len_trim(self%relax_trajectory_file) > 0, &
                    'relax_trajectory_file should not be an empty string')
    end if

    buffer = input%fetch_keyword('relax_trajectory_frequency')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_trajectory_frequency
        call assert(self%relax_trajectory_frequency >= -1, &
                    'relax_trajectory_frequency must be >= -1')
    end if

    buffer = input%fetch_keyword('relax_vc')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_vc
    end if

    buffer = input%fetch_keyword('relax_vc_maxstep')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_vc_maxstep
        call assert(self%relax_vc_maxstep > 0._dp, &
                    'relax_vc_maxstep must be strictly positive')
        self%relax_vc_maxstep = self%relax_vc_maxstep * angstrom
    end if

    buffer = input%fetch_keyword('relax_vc_scale')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_vc_scale
        call assert(self%relax_vc_scale >= 0._dp, 'relax_vc_scale must be >= 0')
        self%relax_vc_scale = self%relax_vc_scale * angstrom
    end if

    buffer = input%fetch_keyword('relax_vc_smax')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%relax_vc_smax
        call assert(self%relax_vc_smax > 0._dp, &
                    'relax_vc_smax must be strictly positive')
        self%relax_vc_smax = self%relax_vc_smax * electronvolt / angstrom**3
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input, atoms)
    !! Initialization of the relaxation driver, which includes reading
    !! input keywords and setting up the optimizer.
    class(RelaxationDriver_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms

    integer :: vector_length

    call self%fetch_input_keywords(input)

    if (self%relax_vc) then
        call assert(atoms%is_periodic, 'Cannot perform variable-cell ' // &
                    'relaxation on a non-periodic structure')
    end if

    if (self%relax_vc_scale < epsilon(1._dp)) then
        self%relax_vc_scale = atoms%get_volume()**(1._dp / 3._dp)
    end if

    vector_length = size(atoms%positions)
    if (self%relax_vc) vector_length = vector_length + size(atoms%cell)

    if (self%relax_algo == 'lbfgs') then
        allocate(LBFGSOptimizer_type :: self%optimizer)
    end if

    select type(opt => self%optimizer)
    type is (LBFGSOptimizer_type)
        call opt%initialize(vector_length=vector_length, &
                            max_vectors=self%relax_history, &
                            initial_curvature=self%relax_initial_curvature)
    end select
end subroutine initialize


subroutine run_task_relaxation(self, atoms, calculator)
    !! Performs a relaxation of the atomic structure.
    class(RelaxationDriver_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    class(Calculator_type), intent(inout) :: calculator

    character(len=160) :: fmt, header
    logical :: is_converged
    integer :: iat, iter, natom_x_3
    real(dp) :: ediff, energy, fmax, r, rmax, smax
    real(dp), dimension(self%optimizer%vector_length) :: fder, step, xval

    header = 'Relaxation_iter                 E_total      delta_E   max(||F||)'
    if (self%relax_vc) then
        header = trim(header) // '     max(|S|)' // new_line('a') // ' ' // &
                 repeat('-', ncopies=78)
        fmt = '(1x, (a), " iter: ", (i0.4), 4x, (f18.10), 3(es13.3))'
    else
        header = trim(header) // new_line('a') // ' ' // repeat('-', ncopies=65)
        fmt = '(1x, (a), " iter: ", (i0.4), 4x, (f18.10), 2(es13.3))'
    end if

    natom_x_3 = atoms%natom * 3
    is_converged = .false.
    energy = 0._dp

    relax_loop: do iter = 1, self%relax_maxiter
        call calculator%calculate_energy(atoms)
        call calculator%calculate_forces(atoms, do_stress=self%relax_vc)

        where (.not. atoms%move_mask)
            atoms%forces = 0._dp
        end where

        ediff = atoms%energy - energy
        energy = atoms%energy
        fmax = maxval(norm2(atoms%forces(:, :), dim=1))
        if (self%relax_vc) smax = maxval(abs(atoms%stress))

        if (is_master()) then
            print *, trim(header)
            if (self%relax_vc) then
                print fmt, self%optimizer%name, iter, energy, ediff, fmax, smax
            else
                print fmt, self%optimizer%name, iter, energy, ediff, fmax
            end if

            if (self%relax_trajectory_frequency > 0) then
                if (modulo(iter, self%relax_trajectory_frequency) == 0) then
                    call write_to_trajectory_file()
                end if
            end if

            print *
        end if

        is_converged = fmax < self%relax_fmax
        if (self%relax_vc) then
            is_converged = is_converged .and. (smax < self%relax_vc_smax)
        end if

        if (is_converged) then
            exit relax_loop
        else
            if (self%relax_vc) then
                xval(1:natom_x_3) = &
                        pack(atoms%get_scaled_positions(), mask=.true.) &
                        * self%relax_vc_scale
                xval(natom_x_3+1:natom_x_3+9) = &
                        pack(atoms%cell, mask=.true.)
                fder(1:natom_x_3) = &
                        pack(-atoms%get_scaled_forces(), mask=.true.) &
                        / self%relax_vc_scale
                fder(natom_x_3+1:natom_x_3+9) = &
                        pack(atoms%get_cell_derivs(), mask=.true.)
            else
                xval(1:natom_x_3) = pack(atoms%positions, mask=.true.)
                fder(1:natom_x_3) = pack(-atoms%forces, mask=.true.)
            end if

            step = self%optimizer%get_next_step(xval=xval, fval=atoms%energy, &
                                                fder=fder)

            if (self%relax_vc) then
                ! Convert positions-step back to Cartesian coordinates
                step(1:natom_x_3) = step(1:natom_x_3) / self%relax_vc_scale
                do iat = 1, atoms%natom
                    step(3*(iat-1)+1:3*iat) = matmul(atoms%cell, &
                                                     step(3*(iat-1)+1:3*iat))
                end do
            end if

            ! Rescale the positions-step, if necessary
            rmax = -1._dp
            do iat = 1, atoms%natom
                r = norm2(step(3*(iat-1)+1:3*iat))
                if (r > rmax) rmax = r
            end do

            associate(p => step(1:natom_x_3))
            if (rmax > self%relax_maxstep) then
                p = p * self%relax_maxstep / rmax
            end if

            atoms%positions = atoms%positions + reshape(p, [3, atoms%natom])
            end associate

            if (self%relax_vc) then
                associate(c => step(natom_x_3+1:natom_x_3+9))
                ! Rescale the cell-step, if necessary
                rmax = maxval(abs(c))
                if (rmax > self%relax_vc_maxstep) then
                    c = c * self%relax_vc_maxstep / rmax
                end if

                c = c + pack(atoms%cell, mask=.true.)
                call atoms%set_cell(scale_atoms=.true., cell=reshape(c, [3, 3]))
                end associate
            end if
        end if
    end do relax_loop

    if (is_master()) then
        if (is_converged) then
            print '(" Relaxation converged in ", (i0), " iterations")', iter
        elseif (self%relax_must_converge) then
            call assert(.false., 'Relaxation did not converge!')
        else
            print *, 'WARNING: Relaxation did not converge!'
        end if

        if (self%relax_trajectory_frequency == -1) then
            call write_to_trajectory_file()
        elseif (self%relax_trajectory_frequency > 1) then
            if (modulo(iter, self%relax_trajectory_frequency) /= 0) then
                call write_to_trajectory_file()
            end if
        end if

        print *
    end if


    contains


    subroutine write_to_trajectory_file()
        associate(str => self%relax_trajectory_file)
        if (self%relax_trajectory_append) then
            print *, 'Appending geometry to ', trim(str)
        else
            print *, 'Writing geometry to ', trim(str)
        end if
        call atoms%write_extxyz_file(filename=trim(str), &
                                     append=self%relax_trajectory_append)
        end associate
    end subroutine write_to_trajectory_file
end subroutine run_task_relaxation

end module tibi_driver_relaxation
