!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_lcao
!! Main module for LCAO calculators

use tibi_calculator, only: Calculator_type
use tibi_constants, only: dp
use tibi_eigensolver, only: EigenSolverComplex_type, &
                            EigenSolverReal_type
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_occupations, only: Occupations_type
use tibi_timer, only: task_timer
use tibi_utils_string, only: lower_case
implicit none


type, abstract, extends(Calculator_type) :: LCAOCalculator_type
    !! Abstract class for linear-combination-of-atomic-orbital calculations.
    logical :: complex_wfn = .true.
        !! whether complex-valued wave functions are to be used
    character(len=128) :: diag_algo = 'divide&conquer'
        !! name of the diagonalization algorithm
    logical :: diag_parallel_ks = .false.
        !! whether to consider distributing the kpoint- and spin-dependent
        !! diagonalization tasks over the available OpenMP threads
    integer :: nband
        !! the total number of electronic bands
        !! (i.e. the order of the H, S, ... matrices)
    logical :: need_densmat_update = .true.
        !! for deciding on densmat updates
    logical :: need_ew_densmat_update = .true.
        !! for deciding on ew_densmat updates
    type(Occupations_type) :: occupations
        !! type for dealing with occupations
    class(EigenSolverReal_type), dimension(:), allocatable :: solver_real
        !! eigensolver(s) for real-valued wave functions
    class(EigenSolverComplex_type), dimension(:), allocatable :: solver_complex
        !! eigensolver(s) for complex-valued wave functions
    real(dp), dimension(:, :, :), allocatable :: eigenvalues
        !! (Nband, Nkpt, Nspin) eigenvalues
    real(dp), dimension(:, :, :, :), allocatable :: cholesky_real
        !! (Nband, Nband, Nkpt, Nspin) overlap matrix decompositions (real)
        !! (either their Cholesky factorizations or related decompositions)
    real(dp), dimension(:, :, :, :), allocatable :: densmat_real
        !! (Nband, Nband, Nkpt, Nspin) density matrices (real)
    real(dp), dimension(:, :, :, :), allocatable :: ew_densmat_real
        !! (Nband, Nband, Nkpt, Nspin) energy-weighted density matrices (real)
    real(dp), dimension(:, :, :, :), allocatable :: hamiltonian_real
        !! (Nband, Nband, Nkpt, Nspin) Hamiltonian matrices (real)
    real(dp), dimension(:, :, :, :), allocatable :: overlap_real
        !! (Nband, Nband, Nkpt, Nspin) overlap matrices (real)
    real(dp), dimension(:, :, :, :), allocatable :: wfncoeff_real
        !! (Nband, Nband, Nkpt, Nspin) wave function coefficients (real)
    complex(dp), dimension(:, :, :, :), allocatable :: cholesky_complex
        !! (Nband, Nband, Nkpt, Nspin) overlap matrix decompositions (complex)
        !! (either their Cholesky factorizations or related decompositions)
    complex(dp), dimension(:, :, :, :), allocatable :: densmat_complex
        !! (Nband, Nband, Nkpt, Nspin) density matrices (complex)
    complex(dp), dimension(:, :, :, :), allocatable :: ew_densmat_complex
        !! (Nband, Nband, Nkpt, Nspin) energy-weighted density matrices
        !! (complex)
    complex(dp), dimension(:, :, :, :), allocatable :: hamiltonian_complex
        !! (Nband, Nband, Nkpt, Nspin) Hamiltonian matrices (complex)
    complex(dp), dimension(:, :, :, :), allocatable :: overlap_complex
        !! (Nband, Nband, Nkpt, Nspin) overlap matrices (complex)
    complex(dp), dimension(:, :, :, :), allocatable :: wfncoeff_complex
        !! (Nband, Nband, Nkpt, Nspin) wave function coefficients (complex)
    contains
    procedure :: fetch_lcao_input_keywords
    procedure :: initialize_lcao
    procedure :: initialize_lcao_solver
    procedure :: allocate_matrices_lcao
    procedure :: calculate_band_energy
    procedure :: calculate_band_entropic_energy
    procedure :: calculate_densmat
    procedure :: get_nr_of_kpoints
    procedure :: get_nr_of_solved_bands
    procedure :: get_nr_of_spins
    procedure :: solve_band_structure
    procedure :: write_array
end type LCAOCalculator_type


interface
    ! Procedure(s) implemented in submodule tibi_lcao_output

    module subroutine write_array(self, array_name)
        !! Writes the chosen array (e.g. 'hamiltonian') to disk.
        implicit none
        class(LCAOCalculator_type), intent(in) :: self
        character(len=*), intent(in) :: array_name
    end subroutine write_array

    ! Procedure(s)s implemented in submodule tibi_lcao_densmat

    module subroutine calculate_densmat(self, energy_weighted)
        !! Calculates the lower triangular density matrix (possibly
        !! energy-weighted) which is stored in self%(ew_)densmat_real/complex.
        !!
        !! Assumes that the band structure has already been obtained through
        !! a preceding call to [[tibi_lcao:solve_band_structure]].
        !!
        !! $$ \mathrm{densmat}(\mu, \nu, k, s) =
        !!    \sum_{b \in bands} f_{bks} c_{bks, \mu}^* c_{bks, \nu}^{\,} $$
        implicit none
        class(LCAOCalculator_type), intent(inout) :: self
        logical, intent(in) :: energy_weighted
            !! weigh by eigenvalues or not?
    end subroutine calculate_densmat

    ! Procedure(s) implemented in submodule tibi_lcao_solver

    module subroutine initialize_lcao_solver(self, input, nelect)
        !! Initializes the eigensolver(s).
        !!
        !! In the kpt/spin-parallelized case, each thread needs its own solver.
        implicit none
        class(LCAOCalculator_type), intent(inout) :: self
        type(Input_type), intent(in) :: input
        real(dp), intent(in), optional :: nelect
            !! the number of electrons (only used, optionally, to help determine
            !! and check the number of eigenvectors that need to be calculated)
    end subroutine initialize_lcao_solver

    module function get_nr_of_solved_bands(self) result(nsolved)
        !! Returns the number of bands (eigenvectors) that the eigensolver
        !! calculates.
        implicit none
        class(LCAOCalculator_type), intent(in) :: self
        integer :: nsolved
    end function get_nr_of_solved_bands

    module subroutine solve_band_structure(self, is_first)
        !! Solves the electronic structure, assuming all the main matrices
        !! have been allocated and the H and S matrices are build.
        implicit none
        class(LCAOCalculator_type), intent(inout) :: self
        logical, intent(in) :: is_first
            !! whether this is the first of a series of calls
            !! with the same S matrix (and different H matrices)
    end subroutine solve_band_structure
end interface


contains


subroutine fetch_lcao_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(LCAOCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('complex_wfn')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%complex_wfn
    end if

    buffer = input%fetch_keyword('diag_algo')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%diag_algo
        call lower_case(self%diag_algo)
    end if

    buffer = input%fetch_keyword('diag_parallel_ks')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%diag_parallel_ks
    end if
end subroutine fetch_lcao_input_keywords


subroutine initialize_lcao(self, input, nband, nelect)
    !! Initializes the LCAO calculator (including allocatable attributes).
    class(LCAOCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    integer, intent(in) :: nband
        !! the number of bands (total nr of basis functions)
    real(dp), intent(in), optional :: nelect
        !! the number of electrons (only used, optionally, to help determine
        !! and check the number of eigenvectors that need to be calculated)

    call self%fetch_lcao_input_keywords(input)
    call self%occupations%initialize(input)

    self%nband = nband
    call self%allocate_matrices_lcao()

    if (present(nelect)) then
        call self%initialize_lcao_solver(input, nelect)
    else
        call self%initialize_lcao_solver(input)
    end if

    call initialize_lcao_timers()
end subroutine initialize_lcao


subroutine allocate_matrices_lcao(self)
    !! Allocates the matrix attributes, initializing to zero where needed.
    class(LCAOCalculator_type), intent(inout) :: self

    integer :: nband, nkpt, nspin

    nband = self%nband
    nkpt = self%get_nr_of_kpoints()
    nspin = self%get_nr_of_spins()

    allocate(self%eigenvalues(nband, nkpt, nspin))

    if (self%complex_wfn) then
        allocate(self%densmat_complex(nband, nband, nkpt, nspin))
        allocate(self%ew_densmat_complex(nband, nband, nkpt, nspin))
        allocate(self%hamiltonian_complex(nband, nband, nkpt, nspin))
        allocate(self%overlap_complex(nband, nband, nkpt, nspin))
        allocate(self%wfncoeff_complex(nband, nband, nkpt, nspin))
        allocate(self%cholesky_complex(nband, nband, nkpt, nspin))

        self%densmat_complex = (0._dp, 0._dp)
        self%ew_densmat_complex = (0._dp, 0._dp)
    else
        allocate(self%densmat_real(nband, nband, nkpt, nspin))
        allocate(self%ew_densmat_real(nband, nband, nkpt, nspin))
        allocate(self%hamiltonian_real(nband, nband, nkpt, nspin))
        allocate(self%overlap_real(nband, nband, nkpt, nspin))
        allocate(self%wfncoeff_real(nband, nband, nkpt, nspin))
        allocate(self%cholesky_real(nband, nband, nkpt, nspin))

        self%densmat_real = 0._dp
        self%ew_densmat_real = 0._dp
    end if
end subroutine allocate_matrices_lcao


function calculate_band_energy(self, nelect) result(eband)
    !! Calculates the band energy after from the eigenvalues and the number
    !! of electrons, updating the occupations and Fermi level in the process.
    !!
    !! @NOTE This value does not consider the electronic entropy
    !! i.e. it does not correspond to the band free energy.
    !!
    !! @NOTE This is the finite-temperature band energy,
    !! i.e. without extrapolation to zero Kelvin.
    class(LCAOCalculator_type), intent(inout) :: self
    real(dp), intent(in) :: nelect
        !! the total number of electrons
    real(dp) :: eband

    call self%occupations%update_occupations_and_fermi_levels( &
                                                        self%eigenvalues, &
                                                        nelect)
    eband = self%occupations%integrate_over_brillouin_zone(self%eigenvalues)
end function calculate_band_energy


function calculate_band_entropic_energy(self) result(eentro)
    !! Returns the entropic contribution to the electronic free energy.
    class(LCAOCalculator_type), intent(in) :: self
    real(dp) :: eentro

    eentro = self%occupations%calculate_entropic_energy()
end function calculate_band_entropic_energy


pure function get_nr_of_kpoints(self) result(nkpt)
    !! Returns the number of k-points.
    class(LCAOCalculator_type), intent(in) :: self
    integer :: nkpt

    nkpt = self%occupations%nkpt
end function get_nr_of_kpoints


pure function get_nr_of_spins(self) result(nspin)
    !! Returns the number of spin channels.
    class(LCAOCalculator_type), intent(in) :: self
    integer :: nspin

    nspin = self%occupations%nspin
end function get_nr_of_spins


subroutine initialize_lcao_timers()
    !! Adds the timers from this module's routines to the global timer.
    call task_timer%add_timer('lcao_densmat')
    call task_timer%add_timer('lcao_ew_densmat')
    call task_timer%add_timer('lcao_solve')
end subroutine initialize_lcao_timers

end module tibi_lcao
