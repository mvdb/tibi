!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_lcao) tibi_lcao_output
!! Implementation of writing out matrices to disk for [[tibi_lcao]]

use tibi_utils_prog, only: assert
use tibi_utils_string, only: int2str
implicit none


contains


module subroutine write_array(self, array_name)
    !! Writes the chosen array (e.g. 'hamiltonian') to disk.
    class(LCAOCalculator_type), intent(in) :: self
    character(len=*), intent(in) :: array_name

    select case(array_name)
    case('bands')
        call write_bands(self)
    case('densmat')
        call write_densmat(self)
    case('ew_densmat')
        call write_ew_densmat(self)
    case('hamiltonian')
        call write_hamiltonian(self)
    case('overlap')
        call write_overlap(self)
    case('wfncoeff')
        call write_wfncoeff(self)
    case default
        call assert(.false., 'Unknown array name: ' // array_name)
    end select
end subroutine write_array


subroutine write_bands(self)
    !! Writes the eigenvalues and corresponding occupations
    !! to a `bands_kpt<ikpt>_spin<ispin>.dat` file
    !! for each k-point and each spin spannel.
    class(LCAOCalculator_type), intent(in) :: self

    integer :: fhandle, iband, ikpt, ispin
    character(len=48) :: filename

    do ispin = 1, self%get_nr_of_spins()
        do ikpt = 1, self%get_nr_of_kpoints()
            write(filename, '("bands_kpt", (i0), "_spin", (i0), ".dat")') &
                  ikpt, ispin
            open(newunit=fhandle, file=filename, status='replace', &
                 action='write')
            write(fhandle, '(a)') '# Band  Eigenvalue  Occupation'

            do iband = 1, self%nband
                write(fhandle, '((i0), (f11.6), (f9.6))') iband, &
                      self%eigenvalues(iband, ikpt, ispin), &
                      self%occupations%occupations(iband, ikpt, ispin) &
                      * self%occupations%spin_weights(ispin)
            end do

            close(fhandle)
        end do
    end do
end subroutine write_bands


subroutine write_densmat(self)
    !! Writes the density matrix for each k-point and spin channel to a
    !! `densmat_kpt<ikpt>_spin<ispin>.dat` file (in the case of complex
    !! wave functions the files are `densmat_<real/imag>_...`).
    class(LCAOCalculator_type), intent(in) :: self

    if (self%complex_wfn) then
        call write_matrix(self, real(self%densmat_complex, kind=dp), &
                          'densmat_real')
        call write_matrix(self, aimag(self%densmat_complex), 'densmat_imag')
    else
        call write_matrix(self, self%densmat_real, 'densmat')
    end if
end subroutine write_densmat


subroutine write_ew_densmat(self)
    !! Writes the energy-weighted density matrix for each k-point and spin
    !! channel to a `ew_densmat_kpt<ikpt>_spin<ispin>.dat` file (in the case
    !! of complex wave functions the files are `ew_densmat_<real/imag>_...`).
    class(LCAOCalculator_type), intent(in) :: self

    if (self%complex_wfn) then
        call write_matrix(self, real(self%ew_densmat_complex, kind=dp), &
                          'ew_densmat_real')
        call write_matrix(self, aimag(self%ew_densmat_complex), &
                          'ew_densmat_imag')
    else
        call write_matrix(self, self%ew_densmat_real, 'ew_densmat')
    end if
end subroutine write_ew_densmat


subroutine write_hamiltonian(self)
    !! Writes the Hamiltonian matrix for each k-point and spin channel to a
    !! `hamiltonian_kpt<ikpt>_spin<ispin>.dat` file (in the case of complex
    !! wave functions the files are `hamiltonian_<real/imag>_...`).
    class(LCAOCalculator_type), intent(in) :: self

    if (self%complex_wfn) then
        call write_matrix(self, real(self%hamiltonian_complex, kind=dp), &
                          'hamiltonian_real')
        call write_matrix(self, aimag(self%hamiltonian_complex), &
                          'hamiltonian_imag')
    else
        call write_matrix(self, self%hamiltonian_real, 'hamiltonian')
    end if
end subroutine write_hamiltonian


subroutine write_matrix(self, matrix, prefix, trans)
    !! Writes the given bands-, k-point- and spin-dependent matrix to disk.
    !!
    !! @NOTE The input matrix should not contain values with magnitudes
    !! between 0 and 1e-99, as these will not be properly formatted.
    class(LCAOCalculator_type), intent(in) :: self
    real(dp), dimension(:, :, :, :), intent(in) :: matrix
        !! (Nband, Nband, Nkpts, Nspin) matrix
    character(len=*), intent(in) :: prefix
        !! prefix for the output file name
    logical, intent(in), optional :: trans
        !! whether to write the matrix itself (the default) or its transpose

    integer :: fhandle, iband, ikpt, ispin
    character(len=:), allocatable :: filename, fmt, suffix
    logical :: do_trans

    fmt = '((i0), *(es14.6))'

    do_trans = .false.
    if (present(trans)) do_trans = trans

    do ispin = 1, self%get_nr_of_spins()
        do ikpt = 1, self%get_nr_of_kpoints()
            suffix = '_kpt' // int2str(ikpt) // '_spin' // int2str(ispin)
            filename = trim(prefix) // suffix // '.dat'

            open(newunit=fhandle, file=filename, status='replace', &
                 action='write')
            write(fhandle, '(a)') '# Band  Matrix elements'

            if (do_trans) then
                do iband = 1, self%nband
                    write(fhandle, fmt) iband, matrix(:, iband, ikpt ,ispin)
                end do
            else
                do iband = 1, self%nband
                    write(fhandle, fmt) iband, matrix(iband, :, ikpt ,ispin)
                end do
            end if

            close(fhandle)
        end do
    end do
end subroutine write_matrix


subroutine write_overlap(self)
    !! Writes the overlap matrix for each k-point and spin channel to a
    !! `overlap_kpt<ikpt>_spin<ispin>.dat` file (in the case of complex
    !! wave functions the files are `overlap_<real/imag>_...`).
    class(LCAOCalculator_type), intent(in) :: self

    if (self%complex_wfn) then
        call write_matrix(self, real(self%overlap_complex, kind=dp), &
                          'overlap_real')
        call write_matrix(self, aimag(self%overlap_complex), 'overlap_imag')
    else
        call write_matrix(self, self%overlap_real, 'overlap')
    end if
end subroutine write_overlap


subroutine write_wfncoeff(self)
    !! Writes the wave function coefficients for each k-point and spin
    !! channel to a `wfncoeff_kpt<ikpt>_spin<ispin>.dat` file (in the case
    !! of complex wave functions the files are `wfncoeff_<real/imag>_...`).
    class(LCAOCalculator_type), intent(in) :: self

    if (self%complex_wfn) then
        call write_matrix(self, real(self%wfncoeff_complex, kind=dp), &
                          'wfncoeff_real', trans=.true.)
        call write_matrix(self, aimag(self%wfncoeff_complex), &
                          'wfncoeff_imag', trans=.true.)
    else
        call write_matrix(self, self%wfncoeff_real, 'wfncoeff', trans=.true.)
    end if
end subroutine write_wfncoeff

end submodule tibi_lcao_output
