!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_fixedpoint_base
!! Module for a base class for fixed-point iteration methods

use tibi_constants, only: dp
implicit none


type, abstract :: FixedPointBase_type
    !! Base class for fixed-point iteration of multivariable functions
    integer :: vector_length
        !! length of the individual vectors (= number of function arguments)
    contains
    procedure(add_input_vector), deferred :: add_input_vector
    procedure(add_output_vector), deferred :: add_output_vector
    procedure(get_max_abs_diff), deferred :: get_max_abs_diff
    procedure(get_next_vector), deferred :: get_next_vector
    procedure(reset), deferred :: reset
end type FixedPointBase_type


interface
    pure subroutine add_input_vector(self, vector)
        !! Processes a function argument input vector.
        import dp, FixedPointBase_type
        implicit none
        class(FixedPointBase_type), intent(inout) :: self
        real(dp), dimension(self%vector_length), intent(in) :: vector
            !! new input vector to be process
    end subroutine add_input_vector

    pure subroutine add_output_vector(self, vector)
        !! Processes a function argument output vector.
        import dp, FixedPointBase_type
        implicit none
        class(FixedPointBase_type), intent(inout) :: self
        real(dp), dimension(self%vector_length), intent(in) :: vector
            !! new output vector to be processed
    end subroutine add_output_vector

    pure function get_max_abs_diff(self) result(mad)
        !! Returns the maximum absolute difference between the last pair
        !! of input and output vectors.
        import dp, FixedPointBase_type
        implicit none
        class(FixedPointBase_type), intent(in) :: self
        real(dp) :: mad
    end function get_max_abs_diff

    function get_next_vector(self) result(vector)
        !! Returns an input vector for the next fixed-point iteration
        !! and increments the iteration counter.
        import dp, FixedPointBase_type
        class(FixedPointBase_type), intent(inout) :: self
        real(dp), dimension(self%vector_length) :: vector
    end function get_next_vector

    pure subroutine reset(self)
        !! Resets any history-dependent arrays and counters.
        import FixedPointBase_type
        implicit none
        class(FixedPointBase_type), intent(inout) :: self
    end subroutine reset
end interface


contains


end module tibi_fixedpoint_base
