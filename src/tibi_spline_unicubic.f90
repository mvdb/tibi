!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_spline) tibi_spline_unicubic
!! Submodule for unicubic spline interpolation for [[tibi_spline]]

implicit none


integer, dimension(4, 4), parameter :: AinvB = reshape( &
    [0, -1, 2, -1, 2, 0, -5, 3, 0, 1, 4, -3, 0, 0, -1, 1], [4, 4])
    !! product of the inverse of the interpolation coefficient matrix
    !! and the finite difference stencil, times 2


integer, dimension(4, 4), parameter :: AinvB_T = transpose(AinvB)
    !! the transpose of the AinvB matrix


contains


pure module function interpolate_unicubic(x, flocal) result(fval)
    !! Returns the function value at the given (scaled) coordinates based
    !! on unicubic interpolation of the surrounding function values.
    real(dp), intent(in) :: x
        !! scaled x coordinate within the central voxel
    real(dp), dimension(4), intent(in) :: flocal
        !! set of surrounding function values
    real(dp) :: fval

    fval = 0.5_dp * dot_product([1._dp, x, x**2, x**3], matmul(AinvB, flocal))
end function interpolate_unicubic


pure module function interpolate_unicubic_batch(x, n, flocal) result(fval)
    !! Variant of interpolate_unicubic for a multi-valued function.
    real(dp), intent(in) :: x
        !! scaled x coordinate within the central voxel
    integer, intent(in) :: n
        !! number of function values to evaluate
    real(dp), dimension(n, 4), intent(in) :: flocal
        !! set of surrounding function values
    real(dp), dimension(n) :: fval

    fval(:) = 0.5_dp * matmul(matmul(flocal, AinvB_T), [1._dp, x, x**2, x**3])
end function interpolate_unicubic_batch


pure module function interpolate_unicubic_deriv(x, flocal) result(fder)
    !! Returns the function value derivative at the given (scaled)
    !! coordinates based on unicubic interpolation of the surrounding
    !! function values.
    real(dp), intent(in) :: x
        !! scaled x coordinate within the central voxel
    real(dp), dimension(4), intent(in) :: flocal
        !! set of surrounding function values
    real(dp) :: fder

    fder = 0.5_dp * dot_product([0._dp, 1._dp, 2._dp*x, 3._dp*x**2], &
                                matmul(AinvB, flocal))
end function interpolate_unicubic_deriv


pure module function interpolate_unicubic_deriv_batch(x, n, flocal) result(fder)
    !! Variant of interpolate_unicubic_deriv for a multi-valued function.
    real(dp), intent(in) :: x
        !! scaled x coordinate within the central voxel
    integer, intent(in) :: n
        !! number of function values to evaluate
    real(dp), dimension(n, 4), intent(in) :: flocal
        !! set of surrounding function values
    real(dp), dimension(n) :: fder

    fder(:) = 0.5_dp * matmul(matmul(flocal, AinvB_T), &
                              [0._dp, 1._dp, 2._dp*x, 3._dp*x**2])
end function interpolate_unicubic_deriv_batch

end submodule tibi_spline_unicubic
