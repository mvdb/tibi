!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_input
!! Module for basic input file operations

use iso_fortran_env, only: iostat_end
use tibi_utils_prog, only: assert, &
                           is_master
use tibi_utils_string, only: find_string_index, &
                             lower_case
implicit none


integer, parameter :: INPUT_KEY_LENGTH = 128
integer, parameter :: INPUT_VALUE_LENGTH = 1024


type Input_type
    !! Class for reading, storing, and returning input parameters
    character(len=INPUT_KEY_LENGTH), dimension(:), allocatable :: keys
        !! the keywords in the input file
    character(len=INPUT_VALUE_LENGTH), dimension(:), allocatable :: values
        !! the corresponding values in the input file
    contains
    procedure :: fetch_keyword
    procedure :: print_input_description
    procedure :: read_input_file
end type Input_type


contains


pure function fetch_keyword(self, key) result(str)
    !! Given a keyword, returns the matching input parameter value.
    class(Input_type), intent(in) :: self
    character(len=*), intent(in) :: key
        !! keyword (should be lower case)
    character(len=INPUT_VALUE_LENGTH) :: str

    integer :: i

    i = find_string_index(self%keys, key)

    if (i > 0) then
        str = trim(self%values(i))
    else
        str = ''
    end if
end function fetch_keyword


subroutine print_input_description(self)
    !! Prints the stored (key, value) pairs.
    class(Input_type), intent(in) :: self

    integer :: i, n

    n = size(self%keys)

    print '(" Input description (number of key-value pairs = ", (i0), ")")', n
    do i = 1, n
        print '("   ", (a), " = ", (a))', trim(self%keys(i)), &
              trim(self%values(i))
    end do
    print *
end subroutine print_input_description


subroutine read_input_file(self, filename)
    !! Reads 'key = value' pairs from an ASCII input file
    !! and stores them in self%keys and self%values.
    !!
    !! @NOTE The 'out' intent of 'self' means that all
    !! previous content of the object will be 'wiped clean'.
    class(Input_type), intent(out) :: self
    character(len=*), intent(in) :: filename

    integer :: fhandle, nlines, pos, status
    character(len=INPUT_KEY_LENGTH) :: label
    character(len=INPUT_VALUE_LENGTH) :: buffer
    character(len=INPUT_KEY_LENGTH), dimension(:), allocatable :: tmpkeys
    character(len=INPUT_VALUE_LENGTH), dimension(:), allocatable :: tmpvals

    if (is_master()) then
        print *, 'Reading input parameters from ', trim(filename)
        print *
    end if

    ! First get the total number of lines for allocation
    nlines = 0
    open(newunit=fhandle, file=filename, status='old', action='read')
    count_loop: do
        read(fhandle, '(a)', iostat=status) buffer
        if (status == iostat_end) then
            exit count_loop
        else
            nlines = nlines + 1
        end if
    end do count_loop

    allocate(tmpkeys(nlines))
    allocate(tmpvals(nlines))

    rewind(fhandle)

    ! Now the actual parsing
    nlines = 0
    read_loop: do
        read(fhandle, '(a)', iostat=status) buffer

        if (status == iostat_end) then
            exit read_loop
        elseif (status == 0) then
            ! Skip comment lines that start with #
            pos = index(buffer, '#')
            if (pos == 1) then
                cycle
            end if

            nlines = nlines + 1

            pos = index(buffer, '=')
            call assert(pos > 0, 'Problem parsing the following line' // &
                        new_line('a') // buffer)

            label = trim(buffer(1:pos-1))
            buffer = trim(adjustl(buffer(pos+1:)))

            ! Remove in-line comment, if present
            pos = index(buffer, '#')
            if (pos > 0) then
                buffer = trim(buffer(1:pos-1))
            end if

            call lower_case(label)
            tmpkeys(nlines) = label
            tmpvals(nlines) = buffer
        end if
    end do read_loop

    close(fhandle)

    if (allocated(self%keys)) then
        deallocate(self%keys)
    end if
    allocate(self%keys(nlines))
    self%keys = tmpkeys(1:nlines)

    if (allocated(self%values)) then
        deallocate(self%values)
    end if
    allocate(self%values(nlines))
    self%values = tmpvals(1:nlines)
end subroutine read_input_file

end module tibi_input
