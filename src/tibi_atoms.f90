!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_atoms
!! Module for dealing with structures made of atoms

use tibi_constants, only: angstrom, &
                          dp
use tibi_utils_math, only: calculate_3x3_matrix_determinant, &
                           invert_3x3_matrix
use tibi_utils_prog, only: assert, &
                           is_master
use tibi_utils_string, only: get_file_extension, &
                             lower_case
implicit none


type Atoms_type
    !! A class for storing information on the atomic coordinates,
    !! chemical elements, cell vectors, et cetera, and with
    !! type-bound procedures for e.g. reading coordinate files
    !! and printing properties.
    real(dp), dimension(3, 3) :: cell = 0._dp
        !! unit cell vectors
    real(dp) :: energy = 0._dp
        !! energy, to be obtained by one or more calculators
    real(dp) :: entropic_energy = 0._dp
        !! entropic energy contribution
        !! (yielding the free energy when added to self%energy)
    real(dp), dimension(:, :), allocatable :: forces
        !! forces, to be obtained by one or more calculators
    real(dp), dimension(3, 3) :: invcell = 0._dp
        !! inverse cell vectors
    logical :: is_periodic = .false.
        !! whether the structure is periodic in all 3 directions
    integer, dimension(:), allocatable :: kinds
        !! symbol indices of size natom
    logical, dimension(:, :), allocatable :: move_mask
        !! (3, Natom) array showing the Cartesian directions
        !! along which the atoms may be moved
    integer :: natom
        !! number of atoms
    integer :: nkind
        !! number of unique chemical symbols (elements)
    real(dp), dimension(:, :), allocatable :: positions
        !! atomic positions
    real(dp), dimension(3, 3) :: stress = 0._dp
        !! stress tensor, to be obtained by one or more calculators
    character(len=2), dimension(:), allocatable :: symbols
        !! unique element symbols of size nkind
    contains
    procedure :: get_cell_derivs
    procedure :: get_free_energy
    procedure :: get_scaled_forces
    procedure :: get_scaled_positions
    procedure :: get_volume
    procedure :: print_energy
    procedure :: print_forces
    procedure :: print_geometry_description
    procedure :: print_stress
    procedure :: read_geometry_file
    procedure :: read_extxyz_file
    procedure :: set_cell
    procedure :: write_extxyz_file
end type Atoms_type


interface
    ! Procedure(s) implemented in submodule tibi_atoms_xyz

    module subroutine read_extxyz_file(self, filename)
        !! Reads an Atoms object from a file in extended XYZ format
        !! (with units of eV and Angstrom).
        !!
        !! @NOTE The 'out' intent of 'self' means that all
        !! previous content of the object will be wiped clean.
        !!
        !! @NOTE Only the first configuration is read, all
        !! subsequent ones are ignored.
        class(Atoms_type), intent(out) :: self
        character(len=*), intent(in) :: filename
    end subroutine read_extxyz_file

    module subroutine write_extxyz_file(self, filename, append)
        !! Writes the atomic positions and cell vectors to a file
        !! in extended XYZ format (with units of eV and Angstrom).
        class(Atoms_type), intent(in) :: self
        character(len=*), intent(in) :: filename
        logical, intent(in) :: append
            !! whether to append to the file if it already exists
    end subroutine write_extxyz_file
end interface


contains


pure function get_cell_derivs(self) result(cellder)
    !! Returns the derivatives of the energy with respect to the cell
    !! vector components (computed from self%stress). If the structure
    !! is non-periodic, a zero matrix is returned instead.
    class(Atoms_type), intent(in) :: self
    real(dp), dimension(3, 3) :: cellder

    if (self%is_periodic) then
        cellder = transpose(matmul(self%invcell, self%stress)) &
                  * self%get_volume()
    else
        cellder = 0._dp
    end if
end function get_cell_derivs


pure function get_free_energy(self) result(free_energy)
    !! Returns the total free energy as the sum of self%energy
    !! and self%entropic_energy.
    class(Atoms_type), intent(in) :: self
    real(dp) :: free_energy

    free_energy = self%energy + self%entropic_energy
end function get_free_energy


pure function get_scaled_forces(self) result(forces)
    !! Returns the scaled forces (i.e. with respect to changes in the
    !! fractional coordinates). If the structure is non-periodic, the
    !! regular (Cartesian) forces are returned instead.
    class(Atoms_type), intent(in) :: self
    real(dp), dimension(3, self%natom) :: forces

    if (self%is_periodic) then
        forces = matmul(self%cell, self%forces)
    else
        forces = self%forces
    end if
end function get_scaled_forces


pure function get_scaled_positions(self) result(spos)
    !! Returns the scaled positions (i.e. in fractional coordinates).
    !! If the structure is non-periodic, the regular (Cartesian) coordinates
    !! are returned instead.
    class(Atoms_type), intent(in) :: self
    real(dp), dimension(3, self%natom) :: spos

    if (self%is_periodic) then
        spos = matmul(self%invcell, self%positions)
    else
        spos = self%positions
    end if
end function get_scaled_positions


pure function get_volume(self) result(volume)
    !! Returns the cell volume.
    class(Atoms_type), intent(in) :: self
    real(dp) :: volume

    volume = abs(calculate_3x3_matrix_determinant(self%cell))
end function get_volume


subroutine print_energy(self)
    !! Prints out the total energies.
    class(Atoms_type), intent(in) :: self

    if (abs(self%energy) < 1e40_dp) then
        print '(" Total energy [Ha] = ", f0.8)', self%energy
        print '(" Total free energy [Ha] = ", f0.8)', self%get_free_energy()
    else
        print '(" Total energy [Ha] = ", g0.8)', self%energy
        print '(" Total free energy [Ha] = ", g0.8)', self%get_free_energy()
    end if
    print *
end subroutine print_energy


subroutine print_forces(self)
    !! Prints out atoms%forces.
    class(Atoms_type), intent(in) :: self

    integer :: iat

    print '(" Forces [Ha / a0] on the ", i0, " atoms:")', self%natom
    do iat = 1, self%natom
        if (maxval(abs(self%forces(:, iat))) < 1e7_dp) then
            print '("   Atom ", (i0.4), " forces = ", 3(f18.8))', &
                  iat, self%forces(:, iat)
        else
            print '("   Atom ", (i0.4), " forces = ", 3(es18.8))', &
                  iat, self%forces(:, iat)
        end if
    end do
    print *
end subroutine print_forces


subroutine print_geometry_description(self)
    !! Prints a description of the atomic geometry.
    class(Atoms_type), intent(in) :: self

    integer :: i

    print '(a)', ' Geometry description:'

    write(*, '(a)', advance='no') '   Periodic = '
    if (self%is_periodic) then
        write(*, '(a)') 'True'
        print '("   Unit cell volume [a0^3] = ", (f16.4))', &
              self%get_volume()
        print '(a)', '   Unit cell vectors [a0]:'
        do i = 1, 3
            print '("     Cell vector ", (i0), " = ", 3(f16.8))', i, &
                  self%cell(:, i)
        end do
    else
        write(*, '(a)') 'False'
    end if
    print *

    print '("   Number of atomic kinds = ", (i0))', self%nkind
    do i = 1, self%nkind
        print '("     Symbol of kind ", (i0), " = ", (a))', i, self%symbols(i)
    end do
    print *

    print '("   Number of atoms = ", (i0))', self%natom
    print '(a)', '   Cartesian coordinates [a0]:'
    do i = 1, self%natom
        print '("     Atom ", (i0.4), " ", (a2), " = ", 3(f19.8))', &
              i, self%symbols(self%kinds(i)), self%positions(:, i)
    end do
    print *
end subroutine print_geometry_description


subroutine print_stress(self)
    !! Prints out atoms%stress.
    class(Atoms_type), intent(in) :: self

    integer :: i
    character(len=11), dimension(3) :: labels

    labels = ['sxx sxy sxz', 'syx syy syz', 'szx szy szz']

    print *, 'Stress tensor [Ha / a0^3]:'
    do i = 1, 3
        if (maxval(abs(self%stress(:, i))) < 1e7_dp) then
            print '("   ", a, " = ", 3(f18.8))', labels(i), self%stress(:, i)
        else
            print '("   ", a, " = ", 3(es18.8))', labels(i), self%stress(:, i)
        end if
    end do
    print *
end subroutine print_stress


subroutine read_geometry_file(self, filename)
    !! Reads atomic geometry information from a text file.
    !!
    !! Chooses the appropriate read subroutine from the file extension.
    class(Atoms_type), intent(inout) :: self
    character(len=*), intent(in) :: filename

    character(len=12) :: ext

    if (is_master()) then
        print *, 'Reading geometry from ', trim(filename)
        print *
    end if

    ext = get_file_extension(filename)
    call lower_case(ext)

    select case(trim(ext))
    case('.extxyz', '.xyz')
        call self%read_extxyz_file(filename)
    case default
        call assert(.false., 'Unknown geometry file format: ' // ext)
    end select
end subroutine read_geometry_file


pure subroutine set_cell(self, cell, scale_atoms)
    !! Changes the cell vectors to the given set and updates the
    !! inverse cell. Also scales the atomic positions, if requested.
    !! Nothing is done in case the structure is non-periodic.
    class(Atoms_type), intent(inout) :: self
    real(dp), dimension(3, 3), intent(in) :: cell
        !! the new set of cell vectors
    logical, intent(in) :: scale_atoms
        !! whether or not to scale the atomic positions

    if (self%is_periodic) then
        if (scale_atoms) then
            self%positions = self%get_scaled_positions()
            self%positions = matmul(cell, self%positions)
        end if

        self%cell = cell
        self%invcell = invert_3x3_matrix(self%cell)
    end if
end subroutine set_cell

end module tibi_atoms
