!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_scf_tightbinding) tibi_scf_tightbinding_shift
!! Submodule for shift-related procedures for [[tibi_scf_tightbinding]]
!! ('shift' referring to charge- and/or magnetization-induced corrections,
!! as mentioned in e.g. [Aradi 2007](https://doi.org/10.1021/jp070186p))

use tibi_utils_string, only: count_items
implicit none


contains


module pure function copy_subshell_to_orbital_array(self, atoms, vector_l) &
                                                    result(vector_lm)
    !! Duplicates the entries of an l-dependent array to
    !! an lm-dependent array.
    class(ScfTightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(self%nsubshell), intent(in) :: vector_l
        !! output array with l-dependent values
    real(dp), dimension(self%nband) :: vector_lm
        !! output array with lm-dependent values

    integer :: iat, ik, istart_l, iend_l, istart_lm, iend_lm

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        istart_l = self%indices_l(1, iat)
        iend_l = self%indices_l(2, iat)
        istart_lm = self%indices_lm(1, iat)
        iend_lm = self%indices_lm(2, iat)

        call self%basis_sets%get_orbital_copies(ik, vector_l(istart_l:iend_l), &
                                                vector_lm(istart_lm:iend_lm))
    end do
end function copy_subshell_to_orbital_array


module function get_hubbard_parameters(self, atoms) result(params)
    !! Returns the subshell-dependent (diagonal)
    !! on-site one-center U values for each atomic kind.
    class(ScfTightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(self%basis_sets%maxnuml, atoms%nkind) :: params

    integer :: ik, inuml
    real(dp), dimension(self%basis_sets%maxnuml) :: U

    do ik = 1, atoms%nkind
        inuml = self%basis_sets%get_nr_of_subshells(ik)
        call self%basis_sets%get_subshell_properties(ik, U, 'hub')

        if (.not. self%aux_basis_mulliken_subshell_dependent) then
            U = U(1)
        end if

        params(:, ik) = 0._dp
        params(1:inuml, ik) = U(1:inuml)
    end do
end function get_hubbard_parameters


module function get_spin_parameters(self, atoms) result(params)
    !! Returns the subshell-dependent (diagonal and off-diagonal)
    !! on-site one-center W values for each atomic kind.
    class(ScfTightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(self%basis_sets%maxnuml**2, atoms%nkind) :: params

    character(len=2) :: symbol
    integer :: ik, inuml
    real(dp), dimension(self%basis_sets%maxnuml**2) :: W

    do ik = 1, atoms%nkind
        inuml = self%basis_sets%get_nr_of_subshells(ik)
        symbol = atoms%symbols(ik)

        call get_wkernel_from_input()

        if (.not. self%aux_basis_mulliken_subshell_dependent) then
            W = W(1)
        end if

        params(:, ik) = 0._dp
        params(1:inuml**2, ik) = W(1:inuml**2)
    end do


    contains


    subroutine get_wkernel_from_input()

        integer:: i, isym, nitems
        logical :: found

        found = .false.

        sym_loop: do isym = 1, size(self%scf_wkernel_on1c)
            associate(s => self%scf_wkernel_on1c(isym))

            i = scan(s, '_')
            call assert(i > 0, 'No "_" character found in: ' // trim(s))

            if (symbol == s(:i-1)) then
                found = .true.

                if (self%aux_basis_mulliken_subshell_dependent) then
                    nitems = inuml**2
                else
                    nitems = 1
                end if

                call assert(count_items(s(i+1:)) == nitems, &
                            'Unexpected number of spin constants for "' // &
                            trim(symbol) // '"')

                W = 0._dp
                read(s(i+1:), *) W(1:nitems)
                W = W * electronvolt
                exit sym_loop
            end if

            end associate
        end do sym_loop

        call assert(found, 'Could not find spin constant(s) for ' // &
                    '"' // trim(symbol) // '"')
    end subroutine get_wkernel_from_input
end function get_spin_parameters

end submodule tibi_scf_tightbinding_shift
