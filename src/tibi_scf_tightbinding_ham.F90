!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_scf_tightbinding) tibi_scf_tightbinding_ham
!! Submodule for Hamiltonian-related procedures for [[tibi_scf_tightbinding]]

use tibi_blas, only: ddot, &
                     zdotu
implicit none


contains


module subroutine add_mulliken_shifts_to_hamiltonian(self)
    !! Adds the self-consistent corrections to the Hamiltonian
    !! for Mulliken mapping.
    class(ScfTightBindingCalculator_type), intent(inout) :: self

    integer :: i, iband, ikpt, ispin, sign

    call task_timer%start_timer('scf_hamiltonian')

    ! NOTE: minus sign for locpot_lm because it's the
    ! electrostatic potential and electrons have charge -1

    do ispin = 1, self%get_nr_of_spins()
        if (ispin == 1) then
            sign = 1
        else
            sign = -1
        end if

        do ikpt = 1, self%get_nr_of_kpoints()
            if (self%occupations%spin_polarized) then
                if (self%complex_wfn) then
                    !$omp parallel do default(private), schedule(runtime), &
                    !$omp&            shared(self, ikpt, ispin, sign)
                    do iband = 1, self%nband
                        i = iband - mod(iband-1, 4)

                        self%hamiltonian_complex(i:, iband, ikpt, ispin) = &
                            self%hamiltonian_complex(i:, iband, ikpt, ispin) &
                            + self%overlap_complex(i:, iband, ikpt, ispin) &
                              * 0.5_dp * (-self%locpot_lm(i:) &
                                          - self%locpot_lm(iband)&
                                          + sign * self%magpot_lm(i:) &
                                          + sign * self%magpot_lm(iband))
                    end do
                    !$omp end parallel do
                else
                    !$omp parallel do default(private), schedule(runtime), &
                    !$omp&            shared(self, ikpt, ispin, sign)
                    do iband = 1, self%nband
                        i = iband - mod(iband-1, 4)

                        self%hamiltonian_real(i:, iband, ikpt, ispin) = &
                            self%hamiltonian_real(i:, iband, ikpt, ispin) &
                            + self%overlap_real(i:, iband, ikpt, ispin) &
                              * 0.5_dp * (-self%locpot_lm(i:) &
                                          - self%locpot_lm(iband) &
                                          + sign * self%magpot_lm(i:) &
                                          + sign * self%magpot_lm(iband))
                    end do
                    !$omp end parallel do
                end if
            else
                if (self%complex_wfn) then
                    !$omp parallel do default(private), schedule(runtime), &
                    !$omp&            shared(self, ikpt, ispin)
                    do iband = 1, self%nband
                        i = iband - mod(iband-1, 4)

                        self%hamiltonian_complex(i:, iband, ikpt, ispin) = &
                            self%hamiltonian_complex(i:, iband, ikpt, ispin) &
                            + self%overlap_complex(i:, iband, ikpt, ispin) &
                              * 0.5_dp * (-self%locpot_lm(i:) &
                                          - self%locpot_lm(iband))
                    end do
                    !$omp end parallel do
                else
                    !$omp parallel do default(private), schedule(runtime), &
                    !$omp&            shared(self, ikpt, ispin)
                    do iband = 1, self%nband
                        i = iband - mod(iband-1, 4)

                        self%hamiltonian_real(i:, iband, ikpt, ispin) = &
                            self%hamiltonian_real(i:, iband, ikpt, ispin) &
                            + self%overlap_real(i:, iband, ikpt, ispin) &
                              * 0.5_dp * (-self%locpot_lm(i:) &
                                          - self%locpot_lm(iband))
                    end do
                    !$omp end parallel do
                end if
            end if
        end do
    end do

    call task_timer%stop_timer('scf_hamiltonian')
end subroutine add_mulliken_shifts_to_hamiltonian


module subroutine add_giese_york_shifts_to_hamiltonian(self)
    !! Adds the self-consistent corrections to the Hamiltonian
    !! for Giese-York mapping.
    class(ScfTightBindingCalculator_type), intent(inout) :: self

    integer :: i, iat, ikpt, ispin, natom, sign
    integer :: iband, istart_lm, iend_lm, iaux, inaux, istart_mp
    real(dp), dimension(self%nband) :: pot

    call task_timer%start_timer('scf_hamiltonian')

    natom = size(self%indices_lm, dim=2)

    do ispin = 1, self%get_nr_of_spins()
        if (ispin == 1) then
            sign = 1
        else
            sign = -1
        end if

        do ikpt = 1, self%get_nr_of_kpoints()
            do iaux = 1, self%multipoles_maxnmp
                pot = 0._dp

                if (self%occupations%spin_polarized) then
                    do iat = 1, natom
                        istart_lm = self%indices_lm(1, iat)
                        iend_lm = self%indices_lm(2, iat)
                        istart_mp = self%indices_mp(1, iat)
                        inaux = self%indices_mp(2, iat) - istart_mp + 1

                        if (iaux <= inaux) then
                            pot(istart_lm:iend_lm) = &
                                self%locpot_mp(istart_mp+iaux-1) &
                                + sign * self%magpot_mp(istart_mp+iaux-1)
                        end if
                    end do
                else
                    do iat = 1, natom
                        istart_lm = self%indices_lm(1, iat)
                        iend_lm = self%indices_lm(2, iat)
                        istart_mp = self%indices_mp(1, iat)
                        inaux = self%indices_mp(2, iat) - istart_mp + 1

                        if (iaux <= inaux) then
                            pot(istart_lm:iend_lm) = &
                                self%locpot_mp(istart_mp+iaux-1)
                        end if
                    end do
                end if

                if (self%complex_wfn) then
                    !$omp parallel do default(private), schedule(runtime), &
                    !$omp&            shared(self, pot, iaux, ikpt, ispin)
                    do iband = 1, self%nband
                        i = iband - mod(iband-1, 4)

                        self%hamiltonian_complex(i:, iband, ikpt, ispin) = &
                            self%hamiltonian_complex(i:, iband, ikpt, ispin) &
                            + pot(i:) &
                            * self%mapping_complex(i:, iband, 1, iaux, ikpt) &
                            + pot(iband) &
                            * self%mapping_complex(i:, iband, 2, iaux, ikpt)
                    end do
                    !$omp end parallel do
                else
                    !$omp parallel do default(private), schedule(runtime), &
                    !$omp&            shared(self, pot, iaux, ikpt, ispin)
                    do iband = 1, self%nband
                        i = iband - mod(iband-1, 4)

                        self%hamiltonian_real(i:, iband, ikpt, ispin) = &
                            self%hamiltonian_real(i:, iband, ikpt, ispin) &
                            + pot(i:) &
                            * self%mapping_real(i:, iband, 1, iaux, ikpt) &
                            + pot(iband) &
                            * self%mapping_real(i:, iband, 2, iaux, ikpt)
                    end do
                    !$omp end parallel do
                end if
            end do
        end do
    end do

    call task_timer%stop_timer('scf_hamiltonian')
end subroutine add_giese_york_shifts_to_hamiltonian


module function calculate_h0_energy(self) result(eh0)
    !! Returns the expectation value sum associated with H0
    !! (i.e. \( \sum_i w_i \langle \phi_i | H^0 | \phi_i \rangle \).
    class(ScfTightBindingCalculator_type), intent(in) :: self
    real(dp) :: eh0

    integer :: iband, ikpt, ispin

    eh0 = 0._dp

    do ispin = 1, self%get_nr_of_spins()
        do ikpt = 1, self%get_nr_of_kpoints()
            ! Making use of the density and Hamiltonian matrices being Hermitian
            if (self%complex_wfn) then
                associate(ham => self%hamiltonian_complex)
                do iband = 1, self%nband
                    eh0 = eh0 + real( &
                        self%densmat_complex(iband, iband, ikpt, ispin) &
                        * conjg(ham(iband, iband, ikpt, ispin)), kind=dp)

                    eh0 = eh0 + 2._dp * real(zdotu(self%nband-iband,&
                        self%densmat_complex(iband+1:, iband, ikpt, ispin), 1, &
                        ham(iband+1:, iband, ikpt, ispin), 1), kind=dp)
                end do
                end associate
            else
                associate(ham => self%hamiltonian_real)
                do iband = 1, self%nband
                    eh0 = eh0 &
                        + self%densmat_real(iband, iband, ikpt, ispin) &
                        * ham(iband, iband, ikpt, ispin)

                    eh0 = eh0 + 2._dp * ddot(self%nband-iband, &
                        self%densmat_real(iband+1:, iband, ikpt, ispin), 1, &
                        ham(iband+1:, iband, ikpt, ispin), 1)
                end do
                end associate
            end if
        end do
    end do
end function calculate_h0_energy

end submodule tibi_scf_tightbinding_ham
