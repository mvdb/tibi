!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_tightbinding) tibi_tightbinding_forces
!! Submodule for band structure forces for [[tibi_tightbinding]]

use tibi_threecenter_geometry, only: get_rCM_and_vec_CM
implicit none


contains


module subroutine calculate_forces(self, atoms, do_stress)
    !! Calculates the atomic forces for the given [[tibi_atoms:Atoms_type]]
    !! object, which is stored in atoms%forces.
    !!
    !! Assumes that the band structure has already been obtained through
    !! a preceding call to [[tibi_lcao:solve_band_structure]].
    class(TightBindingCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    logical, intent(in) :: do_stress
        !! whether to also compute the stress tensor for periodic structures

    integer :: i, iat, jat, kat, ik, jk, kk, inumlm, jnumlm
    integer :: ineigh, ineighlist, jneigh, jneighlist
    real(dp) :: rAB, rAC, rBC2, rCM
    real(dp), dimension(3) :: dedx, vec_AB, vec_AC, vec_CM
    real(dp), dimension(3, 2) :: dedx3c
    real(dp), dimension(3, 3) :: stress
    real(dp), dimension(3, atoms%natom) :: forces
    real(dp), dimension(self%basis_sets%maxnumlm, &
                        self%basis_sets%maxnumlm) :: txdm, txewdm

    call self%calculate_densmat(energy_weighted=.false.)
    call self%calculate_densmat(energy_weighted=.true.)

    call task_timer%start_timer('tightbinding_forces')

    forces = 0._dp
    if (do_stress) stress = 0._dp

    !$omp parallel do default(private), schedule(runtime), &
    !$omp&            shared(self, atoms, do_stress), &
    !$omp&            reduction(+:forces, stress)
    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        call calculate_txdm_onsite(iat, ik, inumlm, txdm)

        if (self%h0s%veff_expansion_onsite >= 2) then
            ineigh_loop1: do ineigh = 1, self%nl%neighbors(iat)
                ineighlist = self%nl%start_indices(iat) + ineigh - 1
                jat = self%nl%neighbor_indices(ineighlist)
                jk = atoms%kinds(jat)

                vec_AB = self%nl%vec(:, ineighlist)
                rAB = self%nl%r(ineighlist)
                if (rAB > self%nl%rcuts(jk, ik) .or. &
                    rAB < self%nl%rsame) cycle ineigh_loop1

                dedx = calculate_onsite2c_deriv(self, ik, jk, vec_AB, rAB, &
                                                inumlm, txdm)

                forces(:, iat) = forces(:, iat) - dedx
                forces(:, jat) = forces(:, jat) + dedx

                if (do_stress) then
                    do i = 1, 3
                        stress(:, i) = stress(:, i) - rAB * dedx(:) * vec_AB(i)
                    end do
                end if

                if (self%h0s%veff_expansion_onsite < 3) cycle ineigh_loop1

                jneigh_loop1: do jneigh = 1, self%nl%neighbors(iat)
                    if (jneigh == ineigh) cycle jneigh_loop1
                    jneighlist = self%nl%start_indices(iat) + jneigh - 1
                    kat = self%nl%neighbor_indices(jneighlist)
                    kk = atoms%kinds(kat)

                    vec_AC = self%nl%vec(:, jneighlist)
                    rAC = self%nl%r(jneighlist)
                    if (rAC > self%nl%rcuts(kk, ik) .or. &
                        rAC < self%nl%rsame) cycle jneigh_loop1

                    call get_rCM_and_vec_CM(rAB, vec_AB, rAC, vec_AC, rCM, &
                                            vec_CM)

                    dedx3c = calculate_onsite3c_deriv(self, ik, jk, kk, &
                                         vec_AB, rAB, vec_CM, rCM, inumlm, txdm)
                    dedx3c = dedx3c * 0.5_dp

                    forces(:, iat) = forces(:, iat) - dedx3c(:, 1)
                    forces(:, jat) = forces(:, jat) - dedx3c(:, 2)
                    forces(:, kat) = forces(:, kat) + sum(dedx3c, dim=2)

                    if (do_stress) then
                        do i = 1, 3
                            stress(:, i) = stress(:, i) &
                                + dedx3c(:, 1) * atoms%positions(i, iat) &
                                + dedx3c(:, 2) * (atoms%positions(i, iat) &
                                                  + rAB * vec_AB(i)) &
                                - sum(dedx3c, dim=2) * (atoms%positions(i, iat)&
                                                        + rAC * vec_AC(i))
                        end do
                    end if
                end do jneigh_loop1
            end do ineigh_loop1
        end if

        ineigh_loop2: do ineigh = 1, self%nl%neighbors(iat)
            ineighlist = self%nl%start_indices(iat) + ineigh - 1
            jat = self%nl%neighbor_indices(ineighlist)
            if (jat > iat) exit ineigh_loop2
            jk = atoms%kinds(jat)

            vec_AB = self%nl%vec(:, ineighlist)
            rAB = self%nl%r(ineighlist)
            if (rAB > self%nl%rcuts(jk, ik) .or. &
                rAB < self%nl%rsame) cycle ineigh_loop2

            call calculate_txdm_offsite(iat, jat, ik, jk, ineighlist, inumlm, &
                                        jnumlm, txdm, txewdm)
            dedx = calculate_offsite2c_deriv(self, ik, jk, vec_AB, rAB, &
                                             inumlm, jnumlm, txdm, txewdm)

            forces(:, iat) = forces(:, iat) - dedx
            forces(:, jat) = forces(:, jat) + dedx

            if (do_stress) then
                do i = 1, 3
                    stress(:, i) = stress(:, i) - rAB * dedx(:) * vec_AB(i)
                end do
            end if

            if (self%h0s%veff_expansion_offsite >= 3) then
                jneigh_loop2: do jneigh = 1, self%nl%neighbors(iat)
                    if (jneigh == ineigh) cycle jneigh_loop2
                    jneighlist = self%nl%start_indices(iat) + jneigh - 1
                    kat = self%nl%neighbor_indices(jneighlist)
                    kk = atoms%kinds(kat)

                    vec_AC = self%nl%vec(:, jneighlist)
                    rAC = self%nl%r(jneighlist)
                    if (rAC > self%nl%rcuts(kk, ik) .or. &
                        rAC < self%nl%rsame) cycle jneigh_loop2

                    rBC2 = sum((rAC * vec_AC(:) - rAB * vec_AB(:))**2)
                    if (rBC2 > self%nl%rcuts(kk, jk)**2) cycle jneigh_loop2

                    call get_rCM_and_vec_CM(rAB, vec_AB, rAC, vec_AC, rCM, &
                                            vec_CM)

                    dedx3c = calculate_offsite3c_deriv(self, ik, jk, kk, &
                                                       vec_AB, rAB, vec_CM, &
                                                       rCM, inumlm, jnumlm, &
                                                       txdm)

                    forces(:, iat) = forces(:, iat) - dedx3c(:, 1)
                    forces(:, jat) = forces(:, jat) - dedx3c(:, 2)
                    forces(:, kat) = forces(:, kat) + sum(dedx3c, dim=2)

                    if (do_stress) then
                        do i = 1, 3
                            stress(:, i) = stress(:, i) &
                                + dedx3c(:, 1) * atoms%positions(i, iat) &
                                + dedx3c(:, 2) * (atoms%positions(i, iat) &
                                                  + rAB * vec_AB(i)) &
                                - sum(dedx3c, dim=2) * (atoms%positions(i, iat)&
                                                        + rAC * vec_AC(i))
                        end do
                    end if
                end do jneigh_loop2
            end if

        end do ineigh_loop2
    end do
    !$omp end parallel do

    atoms%forces = forces

    if (do_stress) then
        atoms%stress = stress / atoms%get_volume()
    end if

    call task_timer%stop_timer('tightbinding_forces')


    contains


    pure subroutine calculate_txdm_offsite(iat, jat, ik, jk, ineighlist, &
                                           inumlm, jnumlm, txdm, txewdm)
        ! Calculates the (k-point, spin) sum of the phase shift times
        ! the ordinary and energy-weighted density matrix slices
        ! associated with atoms iat and jat.
        !
        ! Includes a factor of 2 for the (non-diagonal) upper triangular
        ! contributions.
        integer, intent(in) :: iat, jat, ik, jk, ineighlist
        integer, intent(out) :: inumlm, jnumlm
        real(dp), dimension(:, :), intent(out) :: txdm
        real(dp), dimension(:, :), intent(out) :: txewdm

        integer :: ikpt, ispin, istart, iend, jstart, jend, ilm
        complex(dp) :: phase_shift

        istart = self%indices_lm(1, iat)
        iend = self%indices_lm(2, iat)
        inumlm = self%basis_sets%get_nr_of_orbitals(ik)

        jstart = self%indices_lm(1, jat)
        jend = self%indices_lm(2, jat)
        jnumlm = self%basis_sets%get_nr_of_orbitals(jk)

        txdm = 0._dp
        txewdm = 0._dp
        do ispin = 1, self%get_nr_of_spins()
            do ikpt = 1, self%get_nr_of_kpoints()
                if (self%complex_wfn) then
                    phase_shift = self%occupations%calculate_phase_shift(ikpt, &
                                            atoms%invcell, &
                                            self%nl%translations(:, ineighlist))
                    associate(dm => self%densmat_complex)
                    txdm(1:inumlm, 1:jnumlm) = txdm(1:inumlm, 1:jnumlm) &
                        + 2 * real(dm(istart:iend, jstart:jend, ikpt, ispin) &
                                * phase_shift, kind=dp)
                    end associate

                    associate(ewdm => self%ew_densmat_complex)
                    txewdm(1:inumlm, 1:jnumlm) = txewdm(1:inumlm, 1:jnumlm) &
                        + 2 * real(ewdm(istart:iend, jstart:jend, ikpt, ispin) &
                                * phase_shift, kind=dp)
                    end associate
                else
                    associate(dm => self%densmat_real)
                    txdm(1:inumlm, 1:jnumlm) = txdm(1:inumlm, 1:jnumlm) &
                        + 2 * dm(istart:iend, jstart:jend, ikpt, ispin)
                    end associate

                    associate(ewdm => self%ew_densmat_real)
                    txewdm(1:inumlm, 1:jnumlm) = txewdm(1:inumlm, 1:jnumlm) &
                        + 2 * ewdm(istart:iend, jstart:jend, ikpt, ispin)
                    end associate
                end if
            end do
        end do

        if (iat == jat) then
            ! Avoid double counting of the diagonal elements
            do ilm = 1, inumlm
                txdm(ilm, ilm) = txdm(ilm, ilm) * 0.5_dp
                txewdm(ilm, ilm) = txewdm(ilm, ilm) * 0.5_dp
            end do
        end if
    end subroutine calculate_txdm_offsite


    pure subroutine calculate_txdm_onsite(iat, ik, inumlm, txdm)
        ! Calculates the (k-point, spin) sum of the phase shift (= 0)
        ! times the density matrix slice associated with atom iat.
        !
        ! Includes a factor of 2 for the (non-diagonal) upper triangular
        ! contributions.
        integer, intent(in) :: iat, ik
        integer, intent(out) :: inumlm
        real(dp), dimension(:, :), intent(out) :: txdm

        integer :: ikpt, ispin, ilm, istart, iend

        istart = self%indices_lm(1, iat)
        iend = self%indices_lm(2, iat)
        inumlm = self%basis_sets%get_nr_of_orbitals(ik)

        txdm = 0._dp
        do ispin = 1, self%get_nr_of_spins()
            do ikpt = 1, self%get_nr_of_kpoints()
                if (self%complex_wfn) then
                    associate(dm => self%densmat_complex)
                    do ilm = 1, inumlm
                        txdm(ilm:inumlm, ilm) = txdm(ilm:inumlm, ilm) &
                            + 2 * real(dm(istart+ilm-1:iend, istart+ilm-1, &
                                          ikpt, ispin), kind=dp)
                    end do
                    end associate
                else
                    associate(dm => self%densmat_real)
                    do ilm = 1, inumlm
                        txdm(ilm:inumlm, ilm) = txdm(ilm:inumlm, ilm) &
                            + 2 * dm(istart+ilm-1:iend, istart+ilm-1, &
                                     ikpt, ispin)
                    end do
                    end associate
                end if
            end do
        end do

        ! Avoid double counting of the diagonal elements
        do ilm = 1, inumlm
            txdm(ilm, ilm) = txdm(ilm, ilm) * 0.5_dp
        end do
    end subroutine calculate_txdm_onsite
end subroutine calculate_forces


function calculate_offsite2c_deriv(self, ik, jk, vec_AB, rAB, inumlm, jnumlm, &
                                   txdm, txewdm) result(dedx)
    !! Returns the x/y/z-derivatives of the band energy associated with the
    !! off-site two-center Hamiltonian elements for the given atom pair.
    type(TightBindingCalculator_type), intent(in) :: self
    integer, intent(in) :: ik, jk
        !! kind indices of the two atoms
    real(dp), dimension(3), intent(in) :: vec_AB
        !! Normalized AB vector
    real(dp), intent(in) :: rAB
        !! AB distance
    integer, intent(in) :: inumlm
        !! total number of orbitals (basis functions) on the first atom
    integer, intent(in) :: jnumlm
        !! total number of orbitals (basis functions) on the second atom
    real(dp), dimension(:, :), intent(in) :: txdm
        !! sum of phase shifts times the atoms' density matrix entries
    real(dp), dimension(:, :), intent(in) :: txewdm
        !! sum of phase shifts times the atoms' ew-density matrix entries
    real(dp), dimension(3) :: dedx

    integer :: ilm, jlm
    real(dp), dimension(3, inumlm, jnumlm) :: dhdx, dsdx

    call self%h0s%on1coff2c(ik, jk)%evaluate_offsite_derivs(vec_AB(1), &
                                         vec_AB(2), vec_AB(3), rAB, dhdx, dsdx)

    dedx = 0._dp
    do jlm = 1, jnumlm
        do ilm = 1, inumlm
            dedx = dedx + txdm(ilm, jlm) * dhdx(:, ilm, jlm) &
                   -  txewdm(ilm, jlm) * dsdx(:, ilm, jlm)
        end do
    end do
end function calculate_offsite2c_deriv


pure function calculate_offsite3c_deriv(self, ik, jk, kk, vec_AB, rAB, vec_CM, &
                                        rCM, inumlm, jnumlm, txdm) &
                                        result(dedx3c)
    !! Returns the x/y/z-derivatives of the band energy associated with the
    !! off-site three-center Hamiltonian elements for the given atom triplet.
    type(TightBindingCalculator_type), intent(in) :: self
    integer, intent(in) :: ik, jk, kk
        !! kind indices of the 3 atoms
    real(dp), dimension(3), intent(in) :: vec_AB
        !! Normalized AB vector
    real(dp), intent(in) :: rAB
        !! AB distance
    real(dp), dimension(3), intent(in) :: vec_CM
        !! Normalized CM vector
    real(dp), intent(in) :: rCM
        !! CM distance
    integer, intent(in) :: inumlm
        !! total number of orbitals (basis functions) on the first atom
    integer, intent(in) :: jnumlm
        !! total number of orbitals (basis functions) on the second atom
    real(dp), dimension(:, :), intent(in) :: txdm
        !! sum of phase shifts times the atoms' density matrix entries
    real(dp), dimension(3, 2) :: dedx3c

    integer :: i, ider
    logical :: is_negligible
    real(dp), dimension(inumlm, jnumlm, 3, 2) :: dhdx

    call self%h0s%offsite3c(ik, jk, kk)%add_integral_derivs(vec_AB, rAB, &
                                                            vec_CM, rCM, dhdx, &
                                                            is_negligible)
    if (is_negligible) then
        dedx3c = 0._dp
    else
        do i = 1, 2
            do ider = 1, 3
                dedx3c(ider, i) = sum(txdm(:inumlm, :jnumlm) &
                                      * dhdx(:, :, ider, i))
            end do
        end do
    end if
end function calculate_offsite3c_deriv


function calculate_onsite2c_deriv(self, ik, jk, vec_AB, rAB, inumlm, txdm) &
                                  result(dedx)
    !! Returns the x/y/z-derivatives of the band energy associated with the
    !! on-site two-center Hamiltonian elements for the given atom pair.
    type(TightBindingCalculator_type), intent(in) :: self
    integer, intent(in) :: ik, jk
        !! kind indices of the two atoms
    real(dp), dimension(3), intent(in) :: vec_AB
        !! Normalized AB vector
    real(dp), intent(in) :: rAB
        !! AB distance
    integer, intent(in) :: inumlm
        !! total number of orbitals (basis functions) on the first atom
    real(dp), dimension(:, :), intent(in) :: txdm
        !! sum of phase shifts times the atoms' density matrix entries
    real(dp), dimension(3) :: dedx

    integer :: ilm, jlm
    real(dp), dimension(3, inumlm, inumlm) :: dhdx

    dhdx = 0._dp
    call self%h0s%onsite2c(ik, jk)%add_integral_derivs(vec_AB(1), vec_AB(2), &
                                                       vec_AB(3), rAB, dhdx)

    dedx = 0._dp
    do jlm = 1, inumlm
        do ilm = jlm, inumlm
            dedx = dedx + txdm(ilm, jlm) * dhdx(:, ilm, jlm)
        end do
    end do
end function calculate_onsite2c_deriv


pure function calculate_onsite3c_deriv(self, ik, jk, kk, vec_AB, rAB, vec_CM, &
                                       rCM, inumlm, txdm) result(dedx3c)
    !! Returns the x/y/z-derivatives of the band energy associated with the
    !! on-site three-center Hamiltonian elements for the given atom triplet.
    type(TightBindingCalculator_type), intent(in) :: self
    integer, intent(in) :: ik, jk, kk
        !! kind indices of the 3 atoms
    real(dp), dimension(3), intent(in) :: vec_AB
        !! Normalized AB vector
    real(dp), intent(in) :: rAB
        !! AB distance
    real(dp), dimension(3), intent(in) :: vec_CM
        !! Normalized CM vector
    real(dp), intent(in) :: rCM
        !! CM distance
    integer, intent(in) :: inumlm
        !! total number of orbitals (basis functions) on the first atom
    real(dp), dimension(:, :), intent(in) :: txdm
        !! sum of phase shifts times the atoms' density matrix entries
    real(dp), dimension(3, 2) :: dedx3c

    integer :: i, ider, ilm
    logical :: is_negligible
    real(dp), dimension(inumlm, inumlm, 3, 2) :: dhdx

    call self%h0s%onsite3c(ik, jk, kk)%add_integral_derivs(vec_AB, rAB, &
                                                           vec_CM, rCM, dhdx, &
                                                           is_negligible)
    if (is_negligible) then
        dedx3c = 0._dp
    else
        do i = 1, 2
            do ider = 1, 3
                dedx3c(ider, i) = 0._dp
                do ilm = 1, inumlm
                    dedx3c(ider, i) = dedx3c(ider, i) &
                                + dot_product(txdm(ilm:inumlm, ilm), &
                                              dhdx(ilm:inumlm, ilm, ider, i))
                end do
            end do
        end do
    end if
end function calculate_onsite3c_deriv

end submodule tibi_tightbinding_forces
