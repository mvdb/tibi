!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_basis_ao) tibi_basis_ao_subset
!! Submodule for basis subset procedures for [[tibi_basis_ao]]

use tibi_orbitals, only: l_labels, &
                         orbital_index
use tibi_utils_prog, only: assert
use tibi_utils_string, only: find_string_index
implicit none


contains


module subroutine subset_initialize_from_inc(self, inc)
    !! (Partially) initializes a BasisSubset_type object from a list
    !! of included angular momenta.
    class(BasisAOSubset_type), intent(out) :: self
    logical, dimension(MAXL), intent(in) :: inc
        !! whether an angular momentum is present in the basis subset

    integer :: il, istart, iend

    self%inc = inc
    self%inc_lm = .false.

    do il = 1, size(self%inc)
        if (self%inc(il)) then
            istart = orbital_index(il)
            iend = orbital_index(il+1) - 1
            self%inc_lm(istart:iend) = .true.
        end if
    end do
end subroutine subset_initialize_from_inc


module subroutine subset_initialize_from_str(self, basis_str)
    !! (Partially) initializes a BasisSubset_type object from a string.
    class(BasisAOSubset_type), intent(out) :: self
    character(len=*), intent(in) :: basis_str
        !! description of the AO basis subset (e.g. 'spd')

    integer :: i, il, istart, iend

    self%inc = .false.
    self%inc_lm = .false.

    associate(s => basis_str)
    do i = 1, len(s)
        il = find_string_index(l_labels, s(i:i))
        call assert(il > 0, 'Unknown subshell label in "' // s // '"')

        self%inc(il) = .true.

        istart = orbital_index(il)
        iend = orbital_index(il+1) - 1
        self%inc_lm(istart:iend) = .true.
    end do
    end associate
end subroutine subset_initialize_from_str


pure module function subset_get_nr_of_electrons(self) result(nelectrons)
    !! Returns the number of electrons of the basis subset.
    class(BasisAOSubset_type), intent(in) :: self
    real(dp) :: nelectrons

    nelectrons = sum(self%occ, mask=self%inc)
end function subset_get_nr_of_electrons


pure module function subset_get_nr_of_orbitals(self) result(norbitals)
    !! Returns the number of orbitals included in the basis subset.
    class(BasisAOSubset_type), intent(in) :: self
    integer :: norbitals

    norbitals = count(self%inc_lm)
end function subset_get_nr_of_orbitals


pure module function subset_get_nr_of_subshells(self) result(nsubshells)
    !! Returns the number of included subshells (angular momenta)
    !! in the basis subset.
    class(BasisAOSubset_type), intent(in) :: self
    integer :: nsubshells

    nsubshells = count(self%inc)
end function subset_get_nr_of_subshells


pure module function subset_get_repr(self) result(repr)
    !! Returns a printable representation of the basis subset.
    class(BasisAOSubset_type), intent(in) :: self
    character(len=:), allocatable :: repr

    integer :: il

    repr = ''
    do il = 1, size(self%inc)
        if (self%inc(il)) repr = repr // l_labels(il)
    end do
end function subset_get_repr

end submodule tibi_basis_ao_subset
