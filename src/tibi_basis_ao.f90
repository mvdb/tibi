!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_basis_ao
!! Module for atomic orbital basis bookkeeping

use tibi_constants, only: dp
use tibi_orbitals, only: MAXL, &
                         MAXLM
implicit none


type BasisAOSubset_type
    !! Class for basic properties related to an atomic orbital basis subset
    !! for a given element.
    !!
    !! A basis subset is the set of basis functions with the same 'zeta' index.
    real(dp), dimension(MAXL) :: eig = 0._dp
        !! eigenvalues for each implemented angular momentum
    real(dp), dimension(MAXL) :: hub = 0._dp
        !! Hubbard values for each implemented angular momentum
    logical, dimension(MAXL) :: inc = .false.
        !! for each angular momentum, specifies whether it is included
    logical, dimension(MAXLM) :: inc_lm = .false.
        !! for each orbital, specifies whether it is included
    real(dp), dimension(MAXL) :: occ = 0._dp
        !! occupation numbers for each implemented angular momentum
    contains
    procedure :: initialize_from_inc => subset_initialize_from_inc
    procedure :: initialize_from_str => subset_initialize_from_str
    procedure :: get_nr_of_electrons => subset_get_nr_of_electrons
    procedure :: get_nr_of_orbitals => subset_get_nr_of_orbitals
    procedure :: get_nr_of_subshells => subset_get_nr_of_subshells
    procedure :: get_repr => subset_get_repr
end type BasisAOSubset_type


type BasisAO_type
    !! Class for basic properties related to an atomic orbital basis
    !! for a given element.
    integer :: norbitals
        !! the number of orbitals included in the basis set
    integer :: nsubshells
        !! the number of subshells included in the basis set
    integer :: nzetas
        !! the number of 'zetas' (aka subsets) included in the basis set
    character(len=:), allocatable :: repr
        !! string-based representation (e.g. 'spd_sp')
    type(BasisAOSubset_type), dimension(:), allocatable :: subsets
    contains
    procedure :: initialize
    procedure :: get_nr_of_electrons
    procedure :: get_nr_of_orbitals
    procedure :: get_nr_of_subshells
    procedure :: get_nr_of_zetas
    procedure :: get_repr
    procedure :: get_subset_included_orbitals
    procedure :: get_subset_included_subshells
    procedure :: get_subset_nr_of_orbitals
    procedure :: get_subset_nr_of_subshells
    procedure :: get_subset_eigenvalues
    procedure :: get_subset_hubbardvalues
    procedure :: get_subset_occupations
end type BasisAO_type


interface
    ! Procedure(s) implemented in submodule tibi_basis_ao_subset

    module subroutine subset_initialize_from_inc(self, inc)
        !! (Partially) initializes a BasisSubset_type object from a list
        !! of included angular momenta.
        implicit none
        class(BasisAOSubset_type), intent(out) :: self
        logical, dimension(MAXL), intent(in) :: inc
            !! whether an angular momentum is present in the basis subset
    end subroutine subset_initialize_from_inc

    module subroutine subset_initialize_from_str(self, basis_str)
        !! (Partially) initializes a BasisSubset_type object from a string.
        implicit none
        class(BasisAOSubset_type), intent(out) :: self
        character(len=*), intent(in) :: basis_str
            !! description of the AO basis subset (e.g. 'spd')
    end subroutine subset_initialize_from_str

    pure module function subset_get_nr_of_electrons(self) result(nelectrons)
        !! Returns the number of electrons of the basis subset.
        implicit none
        class(BasisAOSubset_type), intent(in) :: self
        real(dp) :: nelectrons
    end function subset_get_nr_of_electrons

    pure module function subset_get_nr_of_orbitals(self) result(norbitals)
        !! Returns the number of orbitals included in the basis subset.
        implicit none
        class(BasisAOSubset_type), intent(in) :: self
        integer :: norbitals
    end function subset_get_nr_of_orbitals

    pure module function subset_get_nr_of_subshells(self) result(nsubshells)
        !! Returns the number of included subshells (angular momenta)
        !! in the basis subset.
        implicit none
        class(BasisAOSubset_type), intent(in) :: self
        integer :: nsubshells
    end function subset_get_nr_of_subshells

    pure module function subset_get_repr(self) result(repr)
        !! Returns a printable representation of the basis subset.
        implicit none
        class(BasisAOSubset_type), intent(in) :: self
        character(len=:), allocatable :: repr
    end function subset_get_repr
end interface


contains


subroutine initialize(self, basis_str)
    !! Initializes a BasisAO_type object from a string.
    class(BasisAO_type), intent(out) :: self
    character(len=*), intent(in) :: basis_str
        !! description of the AO basis (e.g. 'spd_sp')

    integer :: i, istart, iend, izeta
    integer, dimension(len_trim(basis_str)+1) :: indices

    self%nzetas = 1
    indices(1) = 0

    associate(s => basis_str)
    do i = 1, len_trim(s)
        if (s(i:i) == '_') then
            self%nzetas = self%nzetas + 1
            indices(self%nzetas) = i
        end if
    end do
    indices(self%nzetas+1) = len_trim(s) + 1
    end associate

    allocate(self%subsets(self%nzetas))
    self%norbitals = 0
    self%nsubshells = 0
    self%repr = ''

    do izeta = 1, self%nzetas
        istart = indices(izeta) + 1
        iend = indices(izeta+1) - 1

        associate(s => basis_str(istart:iend))
        call self%subsets(izeta)%initialize_from_str(s)
        end associate

        self%norbitals = self%norbitals &
                          + self%subsets(izeta)%get_nr_of_orbitals()
        self%nsubshells = self%nsubshells &
                          + self%subsets(izeta)%get_nr_of_subshells()

        if (len_trim(self%repr) > 0) self%repr = self%repr // '_'
        self%repr = self%repr // trim(self%subsets(izeta)%get_repr())
    end do
end subroutine initialize


pure function get_nr_of_electrons(self) result(nelectrons)
    !! Returns the number of electrons of the basis set.
    class(BasisAO_type), intent(in) :: self
    real(dp) :: nelectrons

    integer :: izeta

    nelectrons = 0._dp

    do izeta = 1, self%nzetas
        nelectrons = nelectrons + self%subsets(izeta)%get_nr_of_electrons()
    end do
end function get_nr_of_electrons


pure function get_nr_of_orbitals(self) result(norbitals)
    !! Returns the number of orbitals included in the basis set.
    class(BasisAO_type), intent(in) :: self
    integer :: norbitals

    norbitals = self%norbitals
end function get_nr_of_orbitals


pure function get_nr_of_subshells(self) result(nsubshells)
    !! Returns the number of subshells (angular momenta)
    !! included in the basis set.
    class(BasisAO_type), intent(in) :: self
    integer :: nsubshells

    nsubshells = self%nsubshells
end function get_nr_of_subshells


pure function get_nr_of_zetas(self) result(nzetas)
    !! Returns the number of 'zetas' included in the basis set.
    class(BasisAO_type), intent(in) :: self
    integer :: nzetas

    nzetas = self%nzetas
end function get_nr_of_zetas


pure function get_repr(self) result(repr)
    !! Returns a printable representation of the basis set.
    class(BasisAO_type), intent(in) :: self
    character(len=:), allocatable :: repr

    repr = self%repr
end function get_repr


pure function get_subset_included_orbitals(self, izeta) result(inc)
    !! Returns an array specifying, for every orbital (s/px/py/.../fz3),
    !! whether it is included in the given basis subset.
    class(BasisAO_type), intent(in) :: self
    integer, intent(in) :: izeta
        !! basis subset index
    logical, dimension(MAXLM) :: inc

    inc = self%subsets(izeta)%inc_lm
end function get_subset_included_orbitals


pure function get_subset_included_subshells(self, izeta) result(inc)
    !! Returns an array specifying, for every subshell (s/p/d/f),
    !! whether it is included in the given basis subset.
    class(BasisAO_type), intent(in) :: self
    integer, intent(in) :: izeta
        !! basis subset index
    logical, dimension(MAXL) :: inc

    inc = self%subsets(izeta)%inc
end function get_subset_included_subshells


pure function get_subset_nr_of_orbitals(self, izeta) result(norbitals)
    !! Returns the number of subshells in the given basis subset.
    class(BasisAO_type), intent(in) :: self
    integer, intent(in) :: izeta
        !! basis subset index
    integer :: norbitals

    norbitals = count(self%subsets(izeta)%inc_lm)
end function


pure function get_subset_nr_of_subshells(self, izeta) result(nsubshells)
    !! Returns the number of subshells in the given basis subset.
    class(BasisAO_type), intent(in) :: self
    integer, intent(in) :: izeta
        !! basis subset index
    integer :: nsubshells

    nsubshells = count(self%subsets(izeta)%inc)
end function


pure function get_subset_eigenvalues(self, izeta) result(eig)
    !! Returns an array with how the electronic eigenvalues
    !! for every subshell (s/p/d/f) in the given basis subset.
    class(BasisAO_type), intent(in) :: self
    integer, intent(in) :: izeta
        !! basis subset index
    real(dp), dimension(MAXL) :: eig

    eig = self%subsets(izeta)%eig
end function get_subset_eigenvalues


pure function get_subset_hubbardvalues(self, izeta) result(hub)
    !! Returns an array with how the Hubbard values
    !! for every subshell (s/p/d/f) in the given basis subset.
    class(BasisAO_type), intent(in) :: self
    integer, intent(in) :: izeta
        !! basis subset index
    real(dp), dimension(MAXL) :: hub

    hub = self%subsets(izeta)%hub
end function get_subset_hubbardvalues


pure function get_subset_occupations(self, izeta) result(occ)
    !! Returns an array with how the electronic occupations
    !! for every subshell (s/p/d/f) in the given basis subset.
    class(BasisAO_type), intent(in) :: self
    integer, intent(in) :: izeta
        !! basis subset index
    real(dp), dimension(MAXL) :: occ

    occ = self%subsets(izeta)%occ
end function get_subset_occupations

end module tibi_basis_ao
