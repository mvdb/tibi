!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_blas
!! Explicit interfaces to selected BLAS routines
!!
!! <http://www.netlib.org/lapack/explore-html/d1/df9/group__blas.html>

use tibi_constants, only: dp
implicit none


interface
    pure function ddot(n, x, incx, y, incy) result(dotprod)
        !! \(dotprod = x \cdot y \) (double precision real).
        import dp
        implicit none
        integer, intent(in) :: n
        real(dp), dimension(*), intent(in) :: x
        integer, intent(in) :: incx
        real(dp), dimension(*), intent(in) :: y
        integer, intent(in) :: incy
        real(dp) :: dotprod
    end function ddot

    subroutine dgemv(trans, m, n, alpha, a, lda, x, incx, beta, y, incy)
        !! \(y = \alpha A x + \beta y \)
        !! (general matrices, double precision real).
        import dp
        implicit none
        character, intent(in) :: trans
        integer, intent(in) :: m
        integer, intent(in) :: n
        real(dp), intent(in) :: alpha
        integer, intent(in) :: lda
        real(dp), dimension(lda, *), intent(in) :: a
        real(dp), dimension(*), intent(in) :: x
        integer, intent(in) :: incx
        real(dp), intent(in) :: beta
        real(dp), dimension(*), intent(inout) :: y
        integer, intent(in) :: incy
    end subroutine dgemv

    subroutine dsymv(uplo, n, alpha, a, lda, x, incx, beta, y, incy)
        !! \(y = \alpha A x + \beta y \)
        !! (symmetric matrices, double precision real).
        import dp
        implicit none
        character, intent(in) :: uplo
        integer, intent(in) :: n
        real(dp), intent(in) :: alpha
        integer, intent(in) :: lda
        real(dp), dimension(lda, *), intent(in) :: a
        real(dp), dimension(*), intent(in) :: x
        integer, intent(in) :: incx
        real(dp), intent(in) :: beta
        real(dp), dimension(*), intent(inout) :: y
        integer, intent(in) :: incy
    end subroutine dsymv

    subroutine dsyrk(uplo, trans, n, k, alpha, a, lda, beta, c, ldc)
        !! \(C = \alpha A A^T + \beta C \) or \(C = \alpha A^TA + \beta C \)
        !! (symmetric matrices, double precision real).
        import dp
        implicit none
        character, intent(in) :: uplo
        character, intent(in) :: trans
        integer, intent(in) :: n
        integer, intent(in) :: k
        real(dp), intent(in) :: alpha
        integer, intent(in) :: lda
        real(dp), dimension(lda, *), intent(in) :: a
        real(dp), intent(in) :: beta
        integer, intent(in) :: ldc
        real(dp), dimension(ldc, *), intent(inout) :: c
    end subroutine dsyrk

    pure function zdotu(n, zx, incx, zy, incy) result(dotprod)
        !! \(dotprod = x^T \cdot y \) (double precision complex).
        import dp
        implicit none
        integer, intent(in) :: n
        complex(dp), dimension(*), intent(in) :: zx
        integer, intent(in) :: incx
        complex(dp), dimension(*), intent(in) :: zy
        integer, intent(in) :: incy
        complex(dp) :: dotprod
    end function zdotu

    subroutine zherk(uplo, trans, n, k, alpha, a, lda, beta, c, ldc)
        !! \(C = \alpha A A^H + \beta C \) or \(C = \alpha A^HA + \beta C \)
        !! (hermitian matrices, double precision complex).
        import dp
        implicit none
        character, intent(in) :: uplo
        character, intent(in) :: trans
        integer, intent(in) :: n
        integer, intent(in) :: k
        real(dp), intent(in) :: alpha
        integer, intent(in) :: lda
        complex(dp), dimension(lda, *), intent(in) :: a
        real(dp), intent(in) :: beta
        integer, intent(in) :: ldc
        complex(dp), dimension(ldc, *), intent(inout) :: c
    end subroutine zherk
end interface

end module tibi_blas
