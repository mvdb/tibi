!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_h0s
!! Module for evaluating H0 and S integrals

use tibi_atoms, only: Atoms_type
use tibi_basis_sets_ao, only: BasisAO_type
use tibi_constants, only: angstrom, &
                          dp, &
                          electronvolt
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_offsite3c, only: Offsite3c_type
use tibi_onsite1c_offsite2c, only: Onsite1cOffsite2c_type
use tibi_onsite2c, only: Onsite2c_type
use tibi_onsite3c, only: Onsite3c_type
use tibi_utils_prog, only: assert, &
                           is_master
implicit none


type H0S_type
    !! A class for handling the different contributions to the
    !! zeroth-order Hamiltonian in a cluster expansion (and also the overlaps)
    type(Offsite3c_type),  dimension(:, :, :), allocatable :: offsite3c
        !! Off-site three-center Hamiltonian contribution evaluators
    type(Onsite1cOffsite2c_type), dimension(:, :), allocatable :: on1coff2c
        !! On-site one-center and off-site two-center Hamiltonian and
        !! overlap matrix element evaluators
    type(Onsite2c_type),  dimension(:, :), allocatable :: onsite2c
        !! Two-center, on-site Hamiltonian contribution evaluators
    type(Onsite3c_type),  dimension(:, :, :), allocatable :: onsite3c
        !! On-site three-center Hamiltonian contribution evaluators
    integer :: overlap_2c_switching_degree = 3
        !! degree of the switching function polynomial (should be 3 or 5)
        !! to let the tail of the 2c S integrals smoothly approach zero
    real(dp) :: overlap_2c_switching_length = 0.5_dp * angstrom
        !! length of the interval beyond the cutoff radius
        !! in which the 2c S integral switching function is applied
    character(len=128) :: tbpar_dir = './'
        !! path to the directory with the tight-binding parameter files
    integer :: veff_2c_switching_degree = 3
        !! degree of the switching function polynomial (should be 3 or 5)
        !! to let the tail of the 2c H integrals smoothly approach zero
    real(dp) :: veff_2c_switching_length = 0.5_dp * angstrom
        !! length of the interval beyond the cutoff radius
        !! in which the 2c H integral switching function is applied
    logical :: veff_3c_binary_io = .false.
        !! whether to try to read from binary .3cb files (unformatted)
        !! files instead of ASCII .3cf files
    real(dp) :: veff_3c_eps_inner = 1e-12_dp * electronvolt
        !! threshold for deciding which interpolation order to select
        !! for three-center Hamiltonian contributions
    real(dp) :: veff_3c_eps_outer = 1e-12_dp * electronvolt
        !! threshold for deciding which three-center Hamiltonian
        !! contributions to neglect
    integer :: veff_3c_interpolation = 3
        !! interpolation order for the three-center integral evaluation
    integer, dimension(3) :: veff_3c_upsampling = [1, 1, 1]
        !! upsampling factors for refining the three-center integral tables
    integer :: veff_expansion_offsite = 2
        !! term at which to truncate the multicenter expansion of the
        !! effective potential in off-site Hamiltonian integrals
    integer :: veff_expansion_onsite = 1
        !! term at which to truncate the multicenter expansion of the
        !! effective potential in on-site Hamiltonian integrals
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: get_largest_cutoff_radius
end type H0S_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(H0S_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('overlap_2c_switching_degree')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%overlap_2c_switching_degree
        call assert(self%overlap_2c_switching_degree == 3 .or. &
                    self%overlap_2c_switching_degree == 5, &
                    'overlap_2c_switching_degree should be either 3 or 5')
    end if

    buffer = input%fetch_keyword('overlap_2c_switching_length')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%overlap_2c_switching_length
         self%overlap_2c_switching_length = self%overlap_2c_switching_length &
                                            * angstrom
        call assert(self%overlap_2c_switching_length >= 0._dp, &
                    'overlap_2c_switching_length should be >= 0')
    end if

    buffer = input%fetch_keyword('tbpar_dir')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%tbpar_dir
    end if

    buffer = input%fetch_keyword('veff_2c_switching_degree')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%veff_2c_switching_degree
        call assert(self%veff_2c_switching_degree == 3 .or. &
                    self%veff_2c_switching_degree == 5, &
                    'veff_2c_switching_degree should be either 3 or 5')
    end if

    buffer = input%fetch_keyword('veff_2c_switching_length')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%veff_2c_switching_length
         self%veff_2c_switching_length = self%veff_2c_switching_length &
                                         * angstrom
        call assert(self%veff_2c_switching_length >= 0._dp, &
                    'veff_2c_switching_length should be >= 0')
    end if

    buffer = input%fetch_keyword('veff_3c_binary_io')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%veff_3c_binary_io
    end if

    buffer = input%fetch_keyword('veff_3c_eps_inner')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%veff_3c_eps_inner
        self%veff_3c_eps_inner = self%veff_3c_eps_inner * electronvolt
        call assert(self%veff_3c_eps_inner >= 0._dp, &
                    'veff_3c_eps_inner must be >= 0')
    end if

    buffer = input%fetch_keyword('veff_3c_eps_outer')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%veff_3c_eps_outer
        self%veff_3c_eps_outer = self%veff_3c_eps_outer * electronvolt
        call assert(self%veff_3c_eps_outer <= self%veff_3c_eps_inner, &
                    'veff_3c_eps_outer must be <= veff_3c_eps_inner')
    end if

    buffer = input%fetch_keyword('veff_3c_interpolation')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%veff_3c_interpolation
        call assert(self%veff_3c_interpolation == 1 .or. &
                    self%veff_3c_interpolation == 3, &
                    'veff_3c_interpolation must be either 1 or 3')
    end if

    buffer = input%fetch_keyword('veff_3c_upsampling')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%veff_3c_upsampling
        call assert(all(self%veff_3c_upsampling >= 1), &
                    'all elements in veff_3c_upsampling must be >= 1')
    end if

    buffer = input%fetch_keyword('veff_expansion_offsite')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%veff_expansion_offsite
        call assert(self%veff_expansion_offsite == 2 .or. &
                    self%veff_expansion_offsite == 3, &
                    'veff_expansion_offsite must be 2 or 3')
    end if

    buffer = input%fetch_keyword('veff_expansion_onsite')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%veff_expansion_onsite
        call assert(self%veff_expansion_onsite == 1 .or. &
                    self%veff_expansion_onsite == 2 .or. &
                    self%veff_expansion_onsite == 3, &
                    'veff_expansion_onsite must be 1, 2 or 3')
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input, atoms, basis_set)
    !! Initialization based on input keywords and H0- and S-related
    !! parameter files.
    class(H0S_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms
    type(BasisAO_type), dimension(:), intent(in) :: basis_set
        !! basis set descriptors for every atomic kind

    call self%fetch_input_keywords(input)

    call initialize_on1coff2c(self, atoms, basis_set)

    if (self%veff_expansion_offsite >= 3) then
        call initialize_offsite3c(self, atoms, basis_set)
    end if

    if (self%veff_expansion_onsite >= 2) then
        call initialize_onsite2c(self, atoms, basis_set)

        if (self%veff_expansion_onsite >= 3) then
            call initialize_onsite3c(self, atoms, basis_set)
        end if
    end if
end subroutine initialize


subroutine initialize_offsite3c(self, atoms, basis_set)
    !! Initializes self%offsite3c from the corresponding parameter files.
    type(H0S_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    type(BasisAO_type), dimension(:), intent(in) :: basis_set
        !! basis set descriptors for every atomic kind

    integer :: ik, jk, kk

    allocate(self%offsite3c(atoms%nkind, atoms%nkind, atoms%nkind))

    do ik = 1, atoms%nkind
        do jk = 1, atoms%nkind
            do kk = 1, atoms%nkind
                call self%offsite3c(ik, jk, kk)%initialize( &
                                                self%tbpar_dir, &
                                                atoms%symbols(ik), &
                                                atoms%symbols(jk), &
                                                atoms%symbols(kk), &
                                                basis_set(ik), &
                                                basis_set(jk), &
                                                self%veff_3c_binary_io, &
                                                self%veff_3c_eps_inner,&
                                                self%veff_3c_eps_outer, &
                                                self%veff_3c_interpolation, &
                                                self%veff_3c_upsampling)
            end do
        end do
    end do

    if (is_master()) print *
end subroutine initialize_offsite3c


subroutine initialize_on1coff2c(self, atoms, basis_set)
    !! Initializes self%on1coff2c using the on1c and off2c Slater-Koster
    !! parameters (integral tables) for every kind pair in the
    !! [[tibi_atoms:Atoms_type]] object.
    type(H0S_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    type(BasisAO_type), dimension(:), intent(in) :: basis_set
        !! basis set descriptors for every atomic kind

    integer :: ik, jk

    allocate(self%on1coff2c(atoms%nkind, atoms%nkind))

    do ik = 1, atoms%nkind
        do jk = 1, atoms%nkind
            call self%on1coff2c(ik, jk)%initialize_from_skf(self%tbpar_dir, &
                                        atoms%symbols(ik), atoms%symbols(jk), &
                                        basis_set(ik), basis_set(jk), &
                                        self%overlap_2c_switching_degree, &
                                        self%overlap_2c_switching_length, &
                                        self%veff_2c_switching_degree, &
                                        self%veff_2c_switching_length)
        end do
    end do

    if (is_master()) print *
end subroutine initialize_on1coff2c


subroutine initialize_onsite2c(self, atoms, basis_set)
    !! Initializes self%onsite2c from the corresponding parameter files.
    type(H0S_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    type(BasisAO_type), dimension(:), intent(in) :: basis_set
        !! basis set descriptors for every atomic kind

    integer :: ik, jk

    allocate(self%onsite2c(atoms%nkind, atoms%nkind))

    do ik = 1, atoms%nkind
        do jk = 1, atoms%nkind
            call self%onsite2c(ik, jk)%initialize_from_skf( &
                                            self%tbpar_dir, &
                                            atoms%symbols(ik), &
                                            atoms%symbols(jk), &
                                            basis_set(ik), &
                                            self%veff_2c_switching_degree, &
                                            self%veff_2c_switching_length)

            if (self%onsite2c(ik, jk)%rcut &
                > self%on1coff2c(ik, jk)%rcut) then
                print *, 'Warning: Hon2c integral cuttofs should be ' // &
                         'smaller than or equal to the Hoff2c cutoffs'
            end if
        end do
    end do

    if (is_master()) print *
end subroutine initialize_onsite2c


subroutine initialize_onsite3c(self, atoms, basis_set)
    !! Initializes self%onsite3c from the corresponding parameter files.
    type(H0S_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    type(BasisAO_type), dimension(:), intent(in) :: basis_set
        !! basis set descriptors for every atomic kind

    integer :: ik, jk, kk

    allocate(self%onsite3c(atoms%nkind, atoms%nkind, atoms%nkind))

    do ik = 1, atoms%nkind
        do jk = 1, atoms%nkind
            do kk = 1, atoms%nkind
                call self%onsite3c(ik, jk, kk)%initialize( &
                                                self%tbpar_dir, &
                                                atoms%symbols(ik), &
                                                atoms%symbols(jk), &
                                                atoms%symbols(kk), &
                                                basis_set(ik), &
                                                self%veff_3c_binary_io, &
                                                self%veff_3c_eps_inner, &
                                                self%veff_3c_eps_outer, &
                                                self%veff_3c_interpolation, &
                                                self%veff_3c_upsampling)
            end do
        end do
    end do

    if (is_master()) print *
end subroutine initialize_onsite3c


pure function get_largest_cutoff_radius(self, ik, jk) result(rcut)
    !! Returns the largest cutoff radius (across the different Slater-Koster
    !! tables) for the given pair of kind indices.
    class(H0S_type), intent(in) :: self
    integer, intent(in) :: ik, jk
        !! atomic kind indices
    real(dp) :: rcut

    rcut = max(self%on1coff2c(ik, jk)%rcut, self%on1coff2c(jk, ik)%rcut)
end function get_largest_cutoff_radius

end module tibi_h0s
