!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_twobody
!! Module for simple pairwise (two-body) functions

use tibi_constants, only: dp
use tibi_utils_math, only: polyval
implicit none


type, abstract :: PairFunction_type
    !! Abstract pairwise function
    integer, dimension(2) :: kinds
        !! the atomic kinds
    real(dp) :: rcut
        !! cutoff in Bohr radii
    contains
    procedure(evaluate), deferred :: evaluate
end type PairFunction_type


interface
    elemental function evaluate(self, r, der) result(fval)
        !! Abstract interface for evaluating the pair function,
        !! which the derived PairFunction types must implement.
        import dp, PairFunction_type
        implicit none
        class(PairFunction_type), intent(in) :: self
        real(dp), intent(in) :: r
            !! distance
        integer, intent(in) :: der
            !! derivative wrt r
        real(dp) :: fval
    end function evaluate
end interface


type, extends(PairFunction_type) :: PolynomialFunction_type
    !! For a polynomial function.
    integer, dimension(:), allocatable :: powers
        !! array of polynomial powers
    real(dp), dimension(:), allocatable :: coeff
        !! array of polynomial coeff
    contains
    procedure :: evaluate => evaluate_poly
end type PolynomialFunction_type


type, extends(PairFunction_type) :: ExponentialFunction_type
    !! For an exponential function of the form \( \exp(-a_1 r + a_2) + a_3 \).
    real(dp) :: a1, a2, a3
        !! parameters
    contains
    procedure :: evaluate => evaluate_exponential
end type ExponentialFunction_type


contains


elemental function evaluate_poly(self, r, der) result(fval)
    !! Evaluates a polynomial function in (rcut - r)
    class(PolynomialFunction_type), intent(in) :: self
    real(dp), intent(in) :: r
        !! distance
    integer, intent(in) :: der
        !! derivative wrt r
    real(dp) :: fval

    integer :: i, pow

    fval = 0._dp
    if (r < self%rcut) then
        do i = 1, size(self%powers)
            pow = self%powers(i)
            if (pow < 0 .or. pow >= der) then
                fval = fval + self%coeff(i) * (-1._dp)**der &
                        * polyval(self%rcut-r, pow, der)
            end if
        end do
    end if
end function evaluate_poly


elemental function evaluate_exponential(self, r, der) result(fval)
    !! Evaluates a exponential function \( \exp(-a_1 r + a_2) + a_3 \).
    !!
    !! @NOTE Assumes the r values are within the pp cutoff.
    class(ExponentialFunction_type), intent(in) :: self
    real(dp), intent(in) :: r
        !! distance
    integer, intent(in) :: der
        !! derivative wrt r
    real(dp) :: fval

    fval = 0._dp
    if (r < self%rcut) then
        if (der == 0) then
            fval = exp(-self%a1*r + self%a2) + self%a3
        elseif (der == 1) then
            fval = -self%a1 * exp(-self%a1*r + self%a2)
        end if
    end if
end function evaluate_exponential

end module tibi_twobody
