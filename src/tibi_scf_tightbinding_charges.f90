!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_scf_tightbinding) tibi_scf_tightbinding_charges
!! Submodule for charge- and magnetization-related procedures
!! for [[tibi_scf_tightbinding]]

use tibi_constants, only: pi
use tibi_fluctuation1c_kernel, only: read_1ck_files
use tibi_orbitals, only: get_nr_of_orbitals
implicit none


contains


pure module subroutine apply_multipoles2pointcartesian(self, atoms, vec_mp)
    !! Transforms the given multipole-dependent array from a spherical
    !! to a cartesian-point-multipole representation.
    class(ScfTightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(self%nmultipoles), intent(inout) :: vec_mp

    integer :: iat, ik, istart, iend, inmulti, izeta, nzeta

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        istart = self%indices_mp(1, iat)
        iend = self%indices_mp(2, iat)
        inmulti = self%multipoles_nmulti(ik)

        nzeta = (iend - istart + 1) / inmulti
        do izeta = 1, nzeta
            vec_mp(istart:istart+inmulti-1) = matmul( &
                self%multipoles2pointcartesian(:inmulti, :inmulti, izeta, ik), &
                vec_mp(istart:istart+inmulti-1))

            istart = istart + inmulti
        end do
    end do
end subroutine apply_multipoles2pointcartesian


pure module subroutine apply_pointcartesian2multipoles(self, atoms, vec_mp)
    !! Transforms the given cartesian-point-multipole-dependent array
    !! from a cartesian to a spherical representation.
    class(ScfTightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(self%nmultipoles), intent(inout) :: vec_mp

    integer :: iat, ik, istart, iend, inmulti, izeta, nzeta

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        istart = self%indices_mp(1, iat)
        iend = self%indices_mp(2, iat)
        inmulti = self%multipoles_nmulti(ik)

        nzeta = (iend - istart + 1) / inmulti
        do izeta = 1, nzeta
            vec_mp(istart:istart+inmulti-1) = matmul( &
                self%pointcartesian2multipoles(:inmulti, :inmulti, izeta, ik), &
                vec_mp(istart:istart+inmulti-1))

            istart = istart + inmulti
        end do
    end do
end subroutine apply_pointcartesian2multipoles


module subroutine get_initial_fluctuation_arrays(self, atoms, charges, &
                                                 magnetizations)
    !! Gets the initial set of subshell- or multipole-resolved charges
    !! (and, if present, magnetizations) to be used in a self-consistency
    !! cycle.
    class(ScfTightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(:), intent(inout) :: charges
    real(dp), dimension(:), intent(inout), optional :: magnetizations

    logical :: file_exists, need_initial_guess, need_mag, need_mag_normalization

    need_mag = present(magnetizations)

    if (self%scf_aux_mapping == 'giese_york') then
        need_initial_guess = self%need_giese_york_chg_update .or. &
                             (need_mag .and. self%need_giese_york_mag_update)
    elseif (self%scf_aux_mapping == 'mulliken') then
        need_initial_guess = self%need_mulliken_pop_update .or. &
                             (need_mag .and. self%need_mulliken_mag_update)
    end if

    if (trim(self%scf_extrapolation) == 'use_guess' .or. &
        need_initial_guess) then
        ! Either the initial guess scheme is requested or it is
        ! simply required because there is no previous solution

        if (trim(self%scf_initial_guess) == 'zero') then
            charges(:) = 0._dp
            if (need_mag) magnetizations(:) = 0._dp
        elseif (trim(self%scf_initial_guess) == 'restart') then
            inquire(file=self%scf_restart_input_file, exist=file_exists)

            if (is_master()) then
                if (file_exists) then
                    write(*, '(a)', advance='no') &
                          ' Reading initial auxiliary charges'
                    if (need_mag) then
                        write(*, '(a)', advance='no') ' and magnetizations'
                    end if
                    write(*, *) 'from ', trim(self%scf_restart_input_file)
                else
                    write(*, '(a)', advance='no') &
                          ' Setting initial auxiliary charges'
                    if (need_mag) then
                        write(*, '(a)', advance='no') ' and magnetizations'
                    end if
                    write(*, *) 'to zero because the restart file ', &
                          trim(self%scf_restart_input_file), ' does not exist'
                end if
                print *
            end if

            if (file_exists) then
                if (need_mag) then
                    call self%read_scf_restart_file(charges, magnetizations)
                else
                    call self%read_scf_restart_file(charges)
                end if
            else
                charges(:) = 0._dp
                if (need_mag) magnetizations(:) = 0._dp
            end if
        else
            call assert(.false., 'Unknown method for SCF initial guess: ' // &
                        trim(self%scf_initial_guess))
        end if
    elseif (trim(self%scf_extrapolation) == 'use_previous') then
        if (self%scf_aux_mapping == 'giese_york') then
            ! Nothing to do (charges/magnetizations = self%giese_york_chg/mag).
        elseif (self%scf_aux_mapping == 'mulliken') then
            ! Populations/magnetizations are available from a previous
            ! calculation, and the subshell charges/magnetizations will be
            ! calculated from them
            charges(:) = self%calculate_mulliken_subshell_chg(atoms)
            if (need_mag) then
                magnetizations(:) = &
                        self%calculate_mulliken_subshell_mag(atoms)
            end if
        end if
    else
        call assert(.false., 'Unknown method for SCF extrapolation: ' // &
                    trim(self%scf_extrapolation))
    end if

    need_mag_normalization = need_mag .and. &
            (self%occupations%spin_fix_nupdown .or. &
             (trim(self%scf_initial_guess) == 'zero' .and. need_initial_guess))

    if (self%scf_aux_mapping == 'giese_york') then
        call self%normalize_multipole_fluctuation_array(atoms, charges, &
                                                        'charges')

        if (need_mag_normalization) then
            call self%normalize_multipole_fluctuation_array(atoms, &
                                                            magnetizations, &
                                                            'magnetizations')
        end if
    elseif (self%scf_aux_mapping == 'mulliken') then
        call self%normalize_monopole_fluctuation_array(charges, 'charges')

        if (need_mag_normalization) then
            call self%normalize_monopole_fluctuation_array(magnetizations, &
                                                           'magnetizations')
        end if
    end if
end subroutine get_initial_fluctuation_arrays


module function get_total_magnetization(self, atoms) result(mag_tot)
    !! Returns the total magnetization, as calculated from the
    !! mono- or multipole-expanded magnetizations.
    class(ScfTightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp) :: mag_tot

    integer :: iat, ik, istart, iend, inmulti, izeta, nzeta

    if (self%scf_aux_mapping == 'giese_york') then
        mag_tot = 0._dp

        do iat = 1, atoms%natom
            ik = atoms%kinds(iat)
            istart = self%indices_mp(1, iat)
            iend = self%indices_mp(2, iat)
            inmulti = self%multipoles_nmulti(ik)

            nzeta = (iend - istart + 1) / inmulti
            do izeta = 1, nzeta
                mag_tot = mag_tot + self%giese_york_mag(istart)
                istart = istart + inmulti
            end do
        end do

        mag_tot = mag_tot * sqrt(4 * pi)
    elseif (self%scf_aux_mapping == 'mulliken') then
        mag_tot = sum(self%mulliken_mag)
    end if
end function get_total_magnetization


module subroutine initialize_pointcartesian_transforms(self, atoms)
    !! Initializes the self%multipoles2pointcartesian and
    !! self%pointcartesian2multipoles transformation matrices.
    class(ScfTightBindingCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: ik, il, ilmax, imp, izeta
    real(dp), dimension(maxval(self%multipoles_ilmax)) :: radmom
    real(dp), dimension(self%multipoles_maxnmp, &
                        self%multipoles_maxnmp) :: kernels, radmoms

    allocate(self%multipoles2pointcartesian(self%multipoles_maxnmulti, &
                                            self%multipoles_maxnmulti, &
                                            maxval(self%multipoles_nzeta), &
                                            atoms%nkind))
    self%multipoles2pointcartesian = 0._dp

    allocate(self%pointcartesian2multipoles, &
             mold=self%multipoles2pointcartesian)
    self%pointcartesian2multipoles = 0._dp

    ! NOTE: the transformations include a sign change,
    ! as self%multipoles represents the electron density difference

    do ik = 1, atoms%nkind
        ilmax = self%multipoles_ilmax(ik)
        call read_1ck_files(self%h0s%tbpar_dir, atoms%symbols(ik), 'U', &
                            ilmax, self%multipoles_nzeta(ik), radmoms, kernels)

        imp = 1
        do izeta = 1, self%multipoles_nzeta(ik)
            associate(m2pc => self%multipoles2pointcartesian(:, :, izeta, ik))
            associate(pc2m => self%pointcartesian2multipoles(:, :, izeta, ik))

            do il = 1, ilmax
                radmom(il) = radmoms(imp, imp)
                imp = imp + get_nr_of_orbitals(il)
            end do

            m2pc(1, 1) = -sqrt(4 * pi) * radmom(1)
            pc2m(1, 1) = -sqrt(4 * pi) * radmom(1)

            if (ilmax >= 2) then
                m2pc(2, 2) = -2 * sqrt(3 * pi) / 3 * radmom(2)
                m2pc(3, 3) = -2 * sqrt(3 * pi) / 3 * radmom(2)
                m2pc(4, 4) = -2 * sqrt(3 * pi) / 3 * radmom(2)
                pc2m(2, 2) = -2 * sqrt(3 * pi) / 3 * radmom(2)
                pc2m(3, 3) = -2 * sqrt(3 * pi) / 3 * radmom(2)
                pc2m(4, 4) = -2 * sqrt(3 * pi) / 3 * radmom(2)

                if (ilmax >= 3) then
                    ! cartesian order: xx yy xy xz yz

                    m2pc(5, 8) = -sqrt(15 * pi) / 5 * radmom(3)  ! xx - dx2-y2
                    m2pc(5, 9) = sqrt(5 * pi) / 5 * radmom(3)  ! xx - dz2
                    m2pc(6, 8) = sqrt(15 * pi) / 5 * radmom(3)  ! yy - dx2-y2
                    m2pc(6, 9) = sqrt(5 * pi) / 5 * radmom(3)  ! yy - dz2
                    m2pc(7, 5) = -sqrt(15 * pi) / 5 * radmom(3)  ! xy - dxy
                    m2pc(8, 7) = -sqrt(15 * pi) / 5 * radmom(3)  ! xz - dxz
                    m2pc(9, 6) = -sqrt(15 * pi) / 5 * radmom(3)  ! yz - dyz

                    pc2m(8, 5) = -sqrt(15 * pi) / 5 * radmom(3)  ! xx - dx2-y2
                    pc2m(9, 5) = 3 * sqrt(5 * pi) / 5 * radmom(3)  ! xx - dz2
                    pc2m(8, 6) = sqrt(15 * pi) / 5 * radmom(3)  ! yy - dx2-y2
                    pc2m(9, 6) = 3 * sqrt(5 * pi) / 5 * radmom(3)  ! yy - dz2
                    pc2m(5, 7) = -2 * sqrt(15 * pi) / 5 * radmom(3)  ! xy - dxy
                    pc2m(7, 8) = -2 * sqrt(15 * pi) / 5 * radmom(3)  ! xz - dxz
                    pc2m(6, 9) = -2 * sqrt(15 * pi) / 5 * radmom(3)  ! yz - dyz
                end if
            end if

            end associate
            end associate
        end do
    end do
end subroutine initialize_pointcartesian_transforms


module subroutine normalize_monopole_fluctuation_array(self, array, which)
    !! Ensures that the sum of the given subshell-dependent array
    !! equals the imposed total charge (for which='charges') or
    !! total magnetization (for which='magnetizations'). If the deviation
    !! is considered significant, a correction is applied, spread out over
    !! the included subshells.
    class(ScfTightBindingCalculator_type), intent(in) :: self
    real(dp), dimension(:), intent(inout) :: array
    character(len=*), intent(in) :: which
        !! which type of array ('charges' or 'magnetizations')

    real(dp) :: diff, ref, total
    character(len=6) :: unit

    select case(which)
    case('charges')
        ref = self%net_charge
        unit = '[|e|] '
    case('magnetizations')
        ref = self%occupations%spin_nupdown
        unit = '[-|e|]'
    case default
        call assert(.false., 'Cannot normalize this array type: ' // which)
    end select

    total = sum(array)
    diff = ref - total

    if (is_master()) then
        write(*, '((a), (f0.6))') ' Sum of initial auxiliary monopole ' // &
                                  which // ' ' // unit // ': ', total
    end if

    if (abs(diff) < self%fluctuation_sum_tolerance) then
        print *
        return
    else
        if (is_master()) then
            write(*, '((a), (f0.6))') ' Shifting initial auxiliary ' // &
                  'monopole ' // which // ' to get a total of ' // &
                  trim(unit) // ': ', ref
            print *
        end if
        array = array + diff / size(array)
    end if
end subroutine normalize_monopole_fluctuation_array


module subroutine normalize_multipole_fluctuation_array(self, atoms, array, &
                                                        which)
    !! Ensures that the sum of the monopoles in the given multipole-
    !! dependent array equals the imposed total charge (for which='charges')
    !! or total magnetization (for which='magnetizations'). If the deviation
    !! is considered significant, a correction is applied, spread out over
    !! the included monopoles.
    class(ScfTightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(:), intent(inout) :: array
    character(len=*), intent(in) :: which
        !! which type of array ('charges' or 'magnetizations')

    integer :: iat, ik, istart_mp, iend_mp, inmulti, izeta, nzeta
    real(dp) :: diff, ref, total
    real(dp), parameter :: scaling = sqrt(4 * pi)
    character(len=6) :: unit

    select case(which)
    case('charges')
        ! Minus sign because positive net charge means electron deficiency
        ref = -self%net_charge
        unit = '[-|e|]'
    case('magnetizations')
        ref = self%occupations%spin_nupdown
        unit = '[-|e|]'
    case default
        call assert(.false., 'Cannot normalize this array type: ' // which)
    end select

    total = 0._dp
    do iat = 1, atoms%natom
        istart_mp = self%indices_mp(1, iat)
        iend_mp = self%indices_mp(2, iat)
        ik = atoms%kinds(iat)
        inmulti = self%multipoles_nmulti(ik)

        total = total + sum(array(istart_mp:iend_mp:inmulti))
    end do

    total = total * scaling
    diff = ref - total

    if (is_master()) then
        write(*, '((a), (f0.6))') ' Sum of initial auxiliary monopole ' // &
                                  which // ' ' // unit // ': ', total
    end if

    if (abs(diff) < self%fluctuation_sum_tolerance) then
        print *
        return
    else
        if (is_master()) then
            write(*, '((a), (f0.6))') ' Shifting initial auxiliary ' // &
                  'monopole ' // which // ' to get a total of ' // &
                  trim(unit) // ': ', ref
            print *
        end if

        do iat = 1, atoms%natom
            istart_mp = self%indices_mp(1, iat)
            iend_mp = self%indices_mp(2, iat)
            ik = atoms%kinds(iat)
            inmulti = self%multipoles_nmulti(ik)

            nzeta = (iend_mp - istart_mp + 1) / inmulti
            do izeta = 1, nzeta
                array(istart_mp) = array(istart_mp) &
                                   + diff / (atoms%natom * nzeta * scaling)
                istart_mp = istart_mp + inmulti
            end do
        end do
    end if
end subroutine normalize_multipole_fluctuation_array

end submodule tibi_scf_tightbinding_charges
