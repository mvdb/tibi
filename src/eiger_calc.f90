!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module eiger_calc
!! Module for the Eiger LCAO mini-calculator

use tibi_constants, only: dp
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_lapack, only: dpotrf, &
                       dtrtri, &
                       zpotrf, &
                       ztrtri
use tibi_lcao, only: LCAOCalculator_type
use tibi_timer, only: task_timer, &
                      Timer_type
use tibi_utils_math, only: copym_lower2upper
use tibi_utils_prog, only: assert, &
                           is_master
use tibi_utils_string, only: int2str
implicit none


type, extends(LCAOCalculator_type) :: EigerCalculator_type
    !! Class for minimalistic LCAO calculations
    !! for eigensolver performance testing.
    logical :: decompose_overlap = .false.
        !! whether to decompose the overlap matrices beforehand
        !! so that the eigensolver can skip this decomposition
        !! already at the first call
    integer :: nbasis = 0
        !! total number of basis functions
    integer :: niter = 1
        !! number of iterations (each iteration involves
        !! one eigensolver call and one band energy calculation)
    integer :: random_seed = 0
        !! seed for the random generator used for populating
        !! the H and S matrices; all elements of the generator's
        !! seed array will be set to this value (unless it is 0,
        !! in which case no seeding will be done)
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: get_nr_of_electrons
    procedure :: build_H_and_S
    procedure :: decompose_S
    procedure :: run
end type EigerCalculator_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(EigerCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('decompose_overlap')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%decompose_overlap
    end if

    buffer = input%fetch_keyword('nbasis')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%nbasis
        call assert(self%nbasis > 0, 'nbasis must be > 0')
    else
        call assert(.false., 'the required "nbasis" input keyword is missing')
    end if

    buffer = input%fetch_keyword('niter')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%niter
        call assert(self%niter > 0, 'niter must be > 0')
    end if

    buffer = input%fetch_keyword('random_seed')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%random_seed
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input)
    !! Initializes the whole calculator from the given input.
    !!
    !! @NOTE This possibly includes seeding the random generator.
    class(EigerCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    integer :: nseed
    integer, dimension(:), allocatable :: seeds

    call self%fetch_input_keywords(input)

    call self%initialize_lcao(input, self%nbasis)

    ! Initialize the random generator
    if (self%random_seed /= 0) then
        call random_seed(size=nseed)
        allocate(seeds(nseed), source=self%random_seed)
        call random_seed(put=seeds)
    end if

    call task_timer%add_timer('eiger_build_H_and_S')
    call task_timer%add_timer('eiger_energy')
    if (self%decompose_overlap) then
        call task_timer%add_timer('eiger_decompose_S')
    end if
end subroutine initialize


function get_nr_of_electrons(self) result(nelect)
    !! Returns the number of (valence) electrons.
    class(EigerCalculator_type), intent(in) :: self
    real(dp) :: nelect

    nelect = 2._dp * self%nbasis
end function get_nr_of_electrons


subroutine build_H_and_S(self)
    !! Fills the Hamiltonian (H) and overlap (S) matrices
    !! with random numbers (lower triangles only).
    !!
    !! @NOTE Different k-points and spin channels get
    !! the same H and S matrices.
    class(EigerCalculator_type), intent(inout) :: self

    call task_timer%start_timer('eiger_build_H_and_S')

    if (self%complex_wfn) then
        call build_one_complex(self%hamiltonian_complex)
        call build_one_complex(self%overlap_complex)
    else
        call build_one_real(self%hamiltonian_real)
        call build_one_real(self%overlap_real)
    end if

    call task_timer%stop_timer('eiger_build_H_and_S')


    contains


    subroutine build_one_complex(matrix)
        complex(dp), dimension(:, :, :, :), intent(out) :: matrix

        integer :: ispin, ikpt

        call random_zhepd(matrix(:, :, 1, 1))

        do ispin = 1, self%get_nr_of_spins()
            do ikpt = 1, self%get_nr_of_kpoints()
                if (ispin > 1 .or. ikpt > 1) then
                    matrix(:, :, ikpt, ispin) = matrix(:, :, 1, 1)
                end if
            end do
        end do
    end subroutine build_one_complex

    subroutine build_one_real(matrix)
        real(dp), dimension(:, :, :, :), intent(out) :: matrix

        integer :: ispin, ikpt

        call random_dsypd(matrix(:, :, 1, 1))

        do ispin = 1, self%get_nr_of_spins()
            do ikpt = 1, self%get_nr_of_kpoints()
                if (ispin > 1 .or. ikpt > 1) then
                    matrix(:, :, ikpt, ispin) = matrix(:, :, 1, 1)
                end if
            end do
        end do
    end subroutine build_one_real
end subroutine build_H_and_S


subroutine decompose_S(self)
    !! Decomposes the overlap matrices S to a form that the eigensolver
    !! can (re)use to convert the generalized eigenvalue problem to
    !! a standard one.
    class(EigerCalculator_type), intent(inout) :: self

    integer :: ispin, ikpt
    character(len=:), allocatable :: gen2std

    if (self%complex_wfn) then
        gen2std = self%solver_complex(1)%gen2std
    else
        gen2std = self%solver_real(1)%gen2std
    end if

    call task_timer%start_timer('eiger_decompose_S')

    do ispin = 1, self%get_nr_of_spins()
        do ikpt = 1, self%get_nr_of_kpoints()
            select case (trim(self%diag_algo))
            case ('divide&conquer')
                call convert_to_cholesky_L()
            case ('elpa')
                if (gen2std == 'eigensolver') then
                   call convert_to_trans_inv_cholesky_L()
                elseif (gen2std == 'LAPACK') then
                   call convert_to_cholesky_L()
                else
                    call assert(.false., 'Not implemented: ' // gen2std)
                end if
            case default
                call assert(.false., &
                            'Not implemented: ' // trim(self%diag_algo))
            end select
        end do
    end do

    call task_timer%stop_timer('eiger_decompose_S')


    contains


    subroutine convert_to_cholesky_L()
        !! For the lower Cholesky factor of S.

        character, parameter :: uplo = 'L'
        character(len=128) :: msg
        integer :: info

        if (self%complex_wfn) then
            self%cholesky_complex(:, :, ikpt, ispin) = &
                        self%overlap_complex(:, :, ikpt, ispin)

            call zpotrf(uplo, self%nbasis, &
                        self%cholesky_complex(:, :, ikpt, ispin), &
                        self%nbasis, info)
        else
            self%cholesky_real(:, :, ikpt, ispin) = &
                        self%overlap_real(:, :, ikpt, ispin)

            call dpotrf(uplo, self%nbasis, &
                        self%cholesky_real(:, :, ikpt, ispin), &
                        self%nbasis, info)
        end if

        write(msg, '("potrf returned nonzero exit code: ", (i0))') info
        call assert(info == 0, trim(msg))
    end subroutine convert_to_cholesky_L

    subroutine convert_to_trans_inv_cholesky_L()
        !! For the transposed inverse of the lower Cholesky factor of S.

        character, parameter :: diag = 'N'
        character, parameter :: uplo = 'U'
        character(len=128) :: msg
        integer :: info

        if (self%complex_wfn) then
            call copym_lower2upper(self%nbasis, &
                                   self%overlap_complex(:, :, ikpt, ispin), &
                                   self%cholesky_complex(:, :, ikpt, ispin), &
                                   include_diagonal=.true.)

            call zpotrf(uplo, self%nbasis, &
                        self%cholesky_complex(:, :, ikpt, ispin), &
                        self%nbasis, info)
        else
            call copym_lower2upper(self%nbasis, &
                                   self%overlap_real(:, :, ikpt, ispin), &
                                   self%cholesky_real(:, :, ikpt, ispin), &
                                   include_diagonal=.true.)

            call dpotrf(uplo, self%nbasis, &
                        self%cholesky_real(:, :, ikpt, ispin), &
                        self%nbasis, info)
        end if

        write(msg, '("potrf returned nonzero exit code: ", (i0))') info
        call assert(info == 0, trim(msg))

        if (self%complex_wfn) then
            call ztrtri(uplo, diag, self%nbasis, &
                        self%cholesky_complex(:, :, ikpt, ispin), &
                        self%nbasis, info)
        else
            call dtrtri(uplo, diag, self%nbasis, &
                        self%cholesky_real(:, :, ikpt, ispin), &
                        self%nbasis, info)
        end if

        write(msg, '("trtri returned nonzero exit code: ", (i0))') info
        call assert(info == 0, trim(msg))
    end subroutine convert_to_trans_inv_cholesky_L
end subroutine decompose_S


subroutine run(self)
    !! Runs a 'singlepoint' calculation where the same eigenvalue
    !! problems are solved one or several times in succession.
    !!
    !! First of course (random) H and S matrices need to be build.
    !! This is optionally followed by overlap matrix decompositions.
    class(EigerCalculator_type), intent(inout) :: self

    character(len=:), allocatable :: iter_name
    integer :: iter
    logical :: is_first
    real(dp) :: elapsed, energy
    type(Timer_type) :: iter_timer

    if (is_master()) then
        print '(" Eiger run (matrix order: ", (i0), ")")', self%nbasis
        print *
        print *, 'Iteration             Energy [-]        Time [s]'
        print *, repeat('-', ncopies=48)
    end if

    call self%build_H_and_S()

    if (self%decompose_overlap) then
        call self%decompose_S()
    end if

    do iter = 1, self%niter
        is_first = .not. (self%decompose_overlap .or. (iter > 1))

        iter_name = 'iter' // int2str(iter)
        call iter_timer%add_timer(iter_name)

        call iter_timer%start_timer(iter_name)
        call self%solve_band_structure(is_first=is_first)
        call iter_timer%stop_timer(iter_name)

        call task_timer%start_timer('eiger_energy')
        energy = self%calculate_band_energy(self%get_nr_of_electrons())
        call task_timer%stop_timer('eiger_energy')

        if (is_master()) then
            elapsed = iter_timer%get_timing(iter_name)
            write(*, '(" iter: ", (i0.4), "  ", (f20.10), "  ", (es14.4))') &
                  iter, energy, elapsed
        end if
    end do

    if (is_master()) then
        print *
        print '(" Band energy [-] = ", f0.10)', energy
        print *
    end if
end subroutine run


subroutine random_dsypd(matrix)
    !! Fills the lower triangle with random values (between 0 and 1)
    !! and then raises the diagonal elements such that the corresponding
    !! symmetric matrix is positive definite. The strict upper triangle
    !! is not referenced.
    real(dp), dimension(:, :), intent(inout) :: matrix
        !! (square) matrix to be randomized (double precision real)

    integer :: i

    do i = 1, size(matrix, dim=2)
        call random_number(matrix(i:, i))
        matrix(i, i) = matrix(i, i) + size(matrix, dim=1)
    end do
end subroutine random_dsypd


subroutine random_zhepd(matrix)
    !! Fills the lower triangle with random values (with real and imaginary
    !! parts between 0 and 1; real numbers on the diagonal) and then raises
    !! the diagonal elements such that the corresponding Hermitian matrix
    !! is positive definite. The strict upper triangle is not referenced.
    complex(dp), dimension(:, :), intent(inout) :: matrix
        !! (square) matrix to be randomized (double precision complex)

    integer :: i
    real(dp), dimension(:), allocatable :: tmp

    allocate(tmp(size(matrix, dim=1)))

    do i = 1, size(matrix, dim=2)
        call random_number(tmp(i:))
        matrix(i:, i) = tmp(i:)

        call random_number(tmp(i:))
        matrix(i:, i) = matrix(i:, i) + (0._dp, 1._dp) * tmp(i:)

        matrix(i, i) = real(matrix(i, i), kind=dp) + size(matrix, dim=1)
    end do
end subroutine random_zhepd

end module eiger_calc
