!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_offsite3c
!! Module for evaluating off-site three-center H terms

use tibi_basis_ao, only: BasisAO_type
use tibi_constants, only: dp
use tibi_orbitals, only: MAXL
use tibi_threecenter, only: MAXLM_3C, &
                            ThreeCenter_type
use tibi_threecenter_geometry, only: get_d_orbital_rotmat, &
                                     get_d_orbital_rotmat_deriv, &
                                     get_rAB_derivs, &
                                     get_rCM_derivs, &
                                     get_rotation_matrix, &
                                     get_rotation_matrix_deriv, &
                                     get_theta_angle, &
                                     get_vAB_derivA, &
                                     get_vCM_derivA
use tibi_utils_prog, only: is_master
implicit none


type Offsite3c_type
    !! Class for evaluating the off-site three-center H integrals
    !! for one kind triplet.
    logical :: has_dmat
        !! whether d-orbital rotations are needed
    logical, dimension(:, :), allocatable :: inc1
        !! (MAXL, numset1) array with included subshells for the first kind
    logical, dimension(:, :), allocatable :: inc2
        !! (MAXL, numset2) array with included subshells for the second kind
    integer, dimension(:), allocatable :: numlm1
        !! number of orbitals for the first kind
    integer, dimension(:), allocatable :: numlm2
        !! number of orbitals for the second kind
    integer :: numset1
        !! number of basis subsets for the first kind
    integer :: numset2
        !! number of basis subsets for the second kind
    integer :: order
        !! interpolation order for integrals within the inner cutoff radius
        !! (the ones between the inner and outer cutoff radii are interpolated
        !! linearly)
    type(ThreeCenter_type), dimension(:, :), allocatable :: spl
        !! three-center Hamiltonian integral interpolators
        !! for every pair of main basis subsets
    contains
    procedure :: add_integrals
    procedure :: add_integral_derivs
    procedure :: initialize
end type Offsite3c_type


contains


pure subroutine add_integrals(self, vec_AB, rAB, vec_CM, rCM, h)
    !! Adds the three-center contribution to the Hamiltonian for all
    !! included orbitals of the atom pairs, due to the presence of
    !! a third atom.
    class(Offsite3c_type), intent(in) :: self
    real(dp), dimension(:), intent(in) :: vec_AB
        !! normalized AB vector components
    real(dp), intent(in) :: rAB
        !! distance between atoms A and B
    real(dp), dimension(:), intent(in) :: vec_CM
        !! normalized CM vector components
    real(dp), intent(in) :: rCM
        !! distance between atoms C and the middle of atoms A and B
    real(dp), dimension(:, :), intent(inout) :: h
        !! (inumlm, jnumlm) hamiltonian matrix

    integer :: ilmcount, jlmcount, iset, jset, order
    logical :: is_negligible
    real(dp) :: r_inner, r_outer, theta
    real(dp), dimension(3, 3) :: pmat, rmat_p, tmp_pp
    real(dp), dimension(5, 5) :: dmat, rmat_d, tmp_dd
    real(dp), dimension(3, 5) :: tmp_pd
    real(dp), dimension(5, 3) :: tmp_dp
    real(dp), dimension(MAXLM_3C**2), target :: int3c
    real(dp), dimension(:, :), pointer :: ptr

    ! Check for quick return
    is_negligible = .true.

    do iset = 1, self%numset1
        do jset = 1, self%numset2
            call self%spl(iset, jset)%get_rcut_rCM(rAB, r_inner, r_outer)
            if (rCM < r_outer) is_negligible = .false.
        end do
    end do

    if (is_negligible) return

    pmat = get_rotation_matrix(vec_AB, vec_CM)
    rmat_p = transpose(pmat)
    theta = get_theta_angle(vec_CM, pmat)

    ! Obtain the d-orbital coefficient matrix if needed
    if (self%has_dmat) then
        dmat = get_d_orbital_rotmat(pmat)
        rmat_d = transpose(dmat)
    end if

    ilmcount = 1
    do iset = 1, self%numset1
        associate(iinc => self%inc1(:, iset))
        associate(inumlm => self%numlm1(iset))

        jlmcount = 1
        do jset = 1, self%numset2
            associate(jinc => self%inc2(:, jset))
            associate(jnumlm => self%numlm2(jset))

            call self%spl(iset, jset)%get_rcut_rCM(rAB, r_inner, r_outer)

            if (rCM > r_outer) then
                jlmcount = jlmcount + jnumlm
                if (jset == self%numset2) ilmcount = ilmcount + inumlm
                cycle
            end if

            if (rCM < r_inner) then
                order = self%order
            else
                order = 1
            end if

            call self%spl(iset, jset)%evaluate(rAB, rCM, theta, order, int3c)

            if (iinc(1)) then
                if (jinc(1)) then
                    h(ilmcount, jlmcount) = h(ilmcount, jlmcount) + int3c(1)
                    jlmcount = jlmcount + 1
                end if

                if (jinc(2)) then
                    h(ilmcount, jlmcount:jlmcount+2) = &
                        h(ilmcount, jlmcount:jlmcount+2) &
                        + matmul(int3c(2:4), pmat)
                    jlmcount = jlmcount + 3
                end if

                if (jinc(3)) then
                    h(ilmcount, jlmcount:jlmcount+4) = &
                        h(ilmcount, jlmcount:jlmcount+4) &
                        + matmul(int3c(5:9), dmat)
                    jlmcount = jlmcount + 5
                end if

                ilmcount = ilmcount + 1
                jlmcount = jlmcount - jnumlm
            end if

            if (iinc(2)) then
                if (jinc(1)) then
                    h(ilmcount:ilmcount+2, jlmcount) = &
                        h(ilmcount:ilmcount+2, jlmcount) &
                        + matmul(rmat_p, int3c(10:12))
                    jlmcount = jlmcount + 1
                end if

                if (jinc(2)) then
                    ptr(1:3, 1:3) => int3c(13:21)
                    tmp_pp = matmul(ptr, pmat)
                    tmp_pp = matmul(rmat_p, tmp_pp)

                    h(ilmcount:ilmcount+2, jlmcount:jlmcount+2) = &
                        h(ilmcount:ilmcount+2, jlmcount:jlmcount+2) + tmp_pp
                    jlmcount = jlmcount + 3
                end if

                if (jinc(3)) then
                    ptr(1:3, 1:5) => int3c(22:36)
                    tmp_pd = matmul(ptr, dmat)
                    tmp_pd = matmul(rmat_p, tmp_pd)

                    h(ilmcount:ilmcount+2, jlmcount:jlmcount+4) = &
                        h(ilmcount:ilmcount+2, jlmcount:jlmcount+4) + tmp_pd
                    jlmcount = jlmcount + 5
                end if

                ilmcount = ilmcount + 3
                jlmcount = jlmcount - jnumlm
            end if

            if (iinc(3)) then
                if (jinc(1)) then
                    h(ilmcount:ilmcount+4, jlmcount) = &
                        h(ilmcount:ilmcount+4, jlmcount) &
                        + matmul(rmat_d, int3c(37:41))
                    jlmcount = jlmcount + 1
                end if

                if (jinc(2)) then
                    ptr(1:5, 1:3) => int3c(42:56)
                    tmp_dp = matmul(ptr, pmat)
                    tmp_dp = matmul(rmat_d, tmp_dp)

                    h(ilmcount:ilmcount+4, jlmcount:jlmcount+2) = &
                        h(ilmcount:ilmcount+4, jlmcount:jlmcount+2) + tmp_dp
                    jlmcount = jlmcount + 3
                end if

                if (jinc(3)) then
                    ptr(1:5, 1:5) => int3c(57:81)
                    tmp_dd = matmul(ptr, dmat)
                    tmp_dd = matmul(rmat_d, tmp_dd)

                    h(ilmcount:ilmcount+4, jlmcount:jlmcount+4) = &
                        h(ilmcount:ilmcount+4, jlmcount:jlmcount+4) + tmp_dd
                    jlmcount = jlmcount + 5
                end if

                ilmcount = ilmcount + 5
                jlmcount = jlmcount - jnumlm
            end if

            jlmcount = jlmcount + jnumlm
            if (jset < self%numset2) ilmcount = ilmcount - inumlm

            end associate
            end associate
        end do

        end associate
        end associate
    end do
end subroutine add_integrals


pure subroutine add_integral_derivs(self, vec_AB, rAB, vec_CM, rCM, dhdx, &
                                    is_negligible)
    !! Fills in the derivatives of the three-center contribution to
    !! the Hamiltonian for all included orbitals of the given atom pair,
    !! due to the presence of a third atom.
    class(Offsite3c_type), intent(in) :: self
    real(dp), dimension(:), intent(in) :: vec_AB
        !! normalized AB vector components
    real(dp), intent(in) :: rAB
        !! distance between atoms A and B
    real(dp), dimension(:), intent(in) :: vec_CM
        !! normalized CM vector components
    real(dp), intent(in) :: rCM
        !! distance between atoms C and the middle of atoms A and B
    real(dp), dimension(:, :, :, :), intent(out) :: dhdx
        !! (inumlm, jnumlm, dx/dy/dz, A/B) hamiltonian matrix derivatives
    logical, intent(out) :: is_negligible
        !! whether the contributions for this geometry are considered
        !! negligible (if so, dhdx does not get calculated)

    integer :: i, ider, ilmcount, jlmcount, istart, jstart, iset, jset, order
    real(dp) :: rCM_clip, r_inner, r_outer, theta
    real(dp), parameter :: eps_CM = 1e-12_dp
    real(dp), dimension(3, 2) :: drAB, drCM, dvABvCM
    real(dp), dimension(3, 3) :: pmat, rmat_p, dvABdA, dvCMdA, tmp_pp1, tmp_pp2
    real(dp), dimension(5, 5) :: dmat, rmat_d, tmp_dd1, tmp_dd2
    real(dp), dimension(3, 5) :: tmp_pd1, tmp_pd2
    real(dp), dimension(5, 3) :: tmp_dp1, tmp_dp2
    real(dp), dimension(3, 3, 3, 2) :: dpmat, drmat_p
    real(dp), dimension(5, 5, 3, 2) :: ddmat, drmat_d
    real(dp), dimension(MAXLM_3C**2), target :: int3c
    real(dp), dimension(MAXLM_3C**2, 3, 2), target :: dint3c
    real(dp), dimension(:, :), pointer :: dptr, ptr

    ! Check for quick return
    is_negligible = .true.

    do iset = 1, self%numset1
        do jset = 1, self%numset2
            call self%spl(iset, jset)%get_rcut_rCM(rAB, r_inner, r_outer)
            if (rCM < r_outer) is_negligible = .false.
        end do
    end do

    if (is_negligible) return

    pmat = get_rotation_matrix(vec_AB, vec_CM)
    rmat_p = transpose(pmat)
    theta = get_theta_angle(vec_CM, pmat)

    drAB = get_rAB_derivs(vec_AB)
    drCM = get_rCM_derivs(vec_CM)
    dvABdA = get_vAB_derivA(vec_AB, rAB)
    dvCMdA = get_vCM_derivA(vec_CM, rCM)
    rCM_clip = max(rCM, eps_CM)

    do ider = 1, 3
        dvABvCM(ider, 1) = dot_product(dvABdA(:, ider), vec_CM) &
                           + dot_product(vec_AB, dvCMdA(:, ider))
        dvABvCM(ider, 2) = dot_product(-dvABdA(:, ider), vec_CM) &
                           + dot_product(vec_AB, dvCMdA(:, ider))
    end do

    dpmat(:, :, :, 1) = get_rotation_matrix_deriv(vec_AB, dvABdA, vec_CM, &
                                                  dvCMdA, 1)
    dpmat(:, :, :, 2) = get_rotation_matrix_deriv(vec_AB, -dvABdA, vec_CM, &
                                                  dvCMdA, 2)
    do i = 1, 2
        do ider = 1, 3
            drmat_p(:, :, ider, i) = transpose(dpmat(:, :, ider, i))
        end do
    end do

    ! Obtain the d-orbital coefficient matrices if needed
    if (self%has_dmat) then
        dmat = get_d_orbital_rotmat(pmat)
        rmat_d = transpose(dmat)
        do i = 1, 2
            ddmat(:, :, :, i) = get_d_orbital_rotmat_deriv(pmat, &
                                                           dpmat(:, :, :, i))
            do ider = 1, 3
                drmat_d(:, :, ider, i) = transpose(ddmat(:, :, ider, i))
            end do
        end do
    end if

    ilmcount = 1
    do iset = 1, self%numset1
        associate(iinc => self%inc1(:, iset))
        associate(inumlm => self%numlm1(iset))

        jlmcount = 1
        do jset = 1, self%numset2
            associate(jinc => self%inc2(:, jset))
            associate(jnumlm => self%numlm2(jset))

            call self%spl(iset, jset)%get_rcut_rCM(rAB, r_inner, r_outer)

            if (rCM > r_outer) then
                dhdx(ilmcount:ilmcount+inumlm-1, &
                     jlmcount:jlmcount+jnumlm-1, :, :) = 0._dp

                jlmcount = jlmcount + jnumlm
                if (jset == self%numset2) ilmcount = ilmcount + inumlm
                cycle
            end if

            if (rCM < r_inner) then
                order = self%order
            else
                order = 1
            end if

            call self%spl(iset, jset)%evaluate(rAB, rCM, theta, order, int3c)
            call self%spl(iset, jset)%evaluate_deriv(rAB, rCM_clip, theta, &
                                                     drAB, drCM, dvABvCM, &
                                                     order, dint3c)

            istart = ilmcount
            jstart = jlmcount

            do i = 1, 2
                do ider = 1, 3
                    ilmcount = istart
                    jlmcount = jstart

                    if (iinc(1)) then
                        if (jinc(1)) then
                            dhdx(ilmcount, jlmcount, ider, i) = &
                                dint3c(1, ider, i)
                            jlmcount = jlmcount + 1
                        end if

                        if (jinc(2)) then
                            dhdx(ilmcount, jlmcount:jlmcount+2, ider, i) = &
                                matmul(dint3c(2:4, ider, i), pmat) &
                                + matmul(int3c(2:4), dpmat(:, :, ider, i))
                            jlmcount = jlmcount + 3
                        end if

                        if (jinc(3)) then
                            dhdx(ilmcount, jlmcount:jlmcount+4, ider, i) = &
                                matmul(dint3c(5:9, ider, i), dmat) &
                                + matmul(int3c(5:9), ddmat(:, :, ider, i))
                            jlmcount = jlmcount + 5
                        end if

                        ilmcount = ilmcount + 1
                        jlmcount = jlmcount - jnumlm
                    end if

                    if (iinc(2)) then
                        if (jinc(1)) then
                            dhdx(ilmcount:ilmcount+2, jlmcount, ider, i) = &
                                matmul(rmat_p, dint3c(10:12, ider, i)) &
                                + matmul(drmat_p(:, :, ider, i), int3c(10:12))
                            jlmcount = jlmcount + 1
                        end if

                        if (jinc(2)) then
                            ptr(1:3, 1:3) => int3c(13:21)
                            dptr(1:3, 1:3) => dint3c(13:21, ider, i)
                            tmp_pp1 = matmul(dptr, pmat)
                            tmp_pp2 = matmul(rmat_p, tmp_pp1)
                            tmp_pp1 = matmul(ptr, pmat)
                            tmp_pp2 = tmp_pp2 + matmul( &
                                        drmat_p(:, :, ider, i), tmp_pp1)
                            tmp_pp1 = matmul(ptr, dpmat(:, :, ider, i))
                            tmp_pp2 = tmp_pp2 + matmul(rmat_p, tmp_pp1)

                            dhdx(ilmcount:ilmcount+2, &
                                 jlmcount:jlmcount+2, ider, i) = tmp_pp2
                            jlmcount = jlmcount + 3
                        end if

                        if (jinc(3)) then
                            ptr(1:3, 1:5) => int3c(22:36)
                            dptr(1:3, 1:5) => dint3c(22:36, ider, i)
                            tmp_pd1 = matmul(dptr, dmat)
                            tmp_pd2 = matmul(rmat_p, tmp_pd1)
                            tmp_pd1 = matmul(ptr, dmat)
                            tmp_pd2 = tmp_pd2 + matmul( &
                                        drmat_p(:, :, ider, i), tmp_pd1)
                            tmp_pd1 = matmul(ptr, ddmat(:, :, ider, i))
                            tmp_pd2 = tmp_pd2 + matmul(rmat_p, tmp_pd1)

                            dhdx(ilmcount:ilmcount+2, &
                                 jlmcount:jlmcount+4, ider, i) = tmp_pd2
                            jlmcount = jlmcount + 5
                        end if

                        ilmcount = ilmcount + 3
                        jlmcount = jlmcount - jnumlm
                    end if

                    if (iinc(3)) then
                        if (jinc(1)) then
                            dhdx(ilmcount:ilmcount+4, jlmcount, ider, i) = &
                                matmul(rmat_d, dint3c(37:41, ider, i)) &
                                + matmul(drmat_d(:, :, ider, i), int3c(37:41))
                            jlmcount = jlmcount + 1
                        end if

                        if (jinc(2)) then
                            ptr(1:5, 1:3) => int3c(42:56)
                            dptr(1:5, 1:3) => dint3c(42:56, ider, i)
                            tmp_dp1 = matmul(dptr, pmat)
                            tmp_dp2 = matmul(rmat_d, tmp_dp1)
                            tmp_dp1 = matmul(ptr, pmat)
                            tmp_dp2 = tmp_dp2 + matmul( &
                                        drmat_d(:, :, ider, i), tmp_dp1)
                            tmp_dp1 = matmul(ptr, dpmat(:, :, ider, i))
                            tmp_dp2 = tmp_dp2 + matmul(rmat_d, tmp_dp1)

                            dhdx(ilmcount:ilmcount+4, &
                                 jlmcount:jlmcount+2, ider, i) = tmp_dp2
                            jlmcount = jlmcount + 3
                        end if

                        if (jinc(3)) then
                            ptr(1:5, 1:5) => int3c(57:81)
                            dptr(1:5, 1:5) => dint3c(57:81, ider, i)
                            tmp_dd1 = matmul(dptr, dmat)
                            tmp_dd2 = matmul(rmat_d, tmp_dd1)
                            tmp_dd1 = matmul(ptr, dmat)
                            tmp_dd2 = tmp_dd2 + matmul( &
                                        drmat_d(:, :, ider, i), tmp_dd1)
                            tmp_dd1 = matmul(ptr, ddmat(:, :, ider, i))
                            tmp_dd2 = tmp_dd2 + matmul(rmat_d, tmp_dd1)

                            dhdx(ilmcount:ilmcount+4, &
                                 jlmcount:jlmcount+4, ider, i) = tmp_dd2
                            jlmcount = jlmcount + 5
                        end if

                        ilmcount = ilmcount + 5
                        jlmcount = jlmcount - jnumlm
                    end if
                end do
            end do

            jlmcount = jlmcount + jnumlm
            if (jset < self%numset2) ilmcount = ilmcount - inumlm

            end associate
            end associate
        end do

        end associate
        end associate
    end do
end subroutine add_integral_derivs


subroutine initialize(self, tbpar_dir, isym, jsym, ksym, ibasis, jbasis, &
                      use_binary_io, eps_inner, eps_outer, order, upsampling)
    !! Ínitialize from the given basis sets and the corresponding
    !! "{element1}-{element2}\_offsite3c\_{element3}.{3cf,3cb}"
    !! files (with the usual extensions for non-minimal basis sets).
    class(Offsite3c_type), intent(out) :: self
    character(len=*), intent(in) :: tbpar_dir
        !! path to the directory with the parameter files
    character(len=2), intent(in) :: isym
        !! element symbols for the first atomic kind
    character(len=2), intent(in) :: jsym
        !! element symbols for the second atomic kind
    character(len=2), intent(in) :: ksym
        !! element symbols for the third atomic kind
    type(BasisAO_type), intent(in) :: ibasis
        !! basis set description for the first atomic kind
    type(BasisAO_type), intent(in) :: jbasis
        !! basis set description for the second atomic kind
    logical, intent(in) :: use_binary_io
        !! whether to try to read from binary .3cb files (unformatted)
    real(dp), intent(in) :: eps_inner
        !! threshold for deciding which interpolation order to select
        !! for three-center Hamiltonian contributions
    real(dp), intent(in) :: eps_outer
        !! threshold for deciding which three-center Hamiltonian
        !! contributions to neglect
    integer, intent(in) :: order
        !! interpolation order for the three-center integral evaluation
    integer, dimension(3), intent(in) :: upsampling
        !! upsampling factors for refining the three-center integral tables

    character(len=4) :: ext
    character(len=:), allocatable :: filename, isym_set, jsym_set
    integer :: iset, jset
    logical :: exists_3cb, write_3cb

    self%has_dmat = .false.
    self%order = order

    self%numset1 = ibasis%get_nr_of_zetas()
    allocate(self%inc1(MAXL, self%numset1))
    allocate(self%numlm1(self%numset1))

    do iset = 1, self%numset1
        self%inc1(:, iset) = ibasis%get_subset_included_subshells(iset)
        self%numlm1(iset) = ibasis%get_subset_nr_of_orbitals(iset)
        if (self%inc1(3, iset)) self%has_dmat = .true.
    end do

    self%numset2 = jbasis%get_nr_of_zetas()
    allocate(self%inc2(MAXL, self%numset2))
    allocate(self%numlm2(self%numset2))

    do jset = 1, self%numset2
        self%inc2(:, jset) = jbasis%get_subset_included_subshells(jset)
        self%numlm2(jset) = jbasis%get_subset_nr_of_orbitals(jset)
        if (self%inc2(3, jset)) self%has_dmat = .true.
    end do

    allocate(self%spl(self%numset1, self%numset2))

    do iset = 1, self%numset1
        isym_set = trim(isym) // repeat('+', iset-1)
        associate(iinc => self%inc1(:, iset))
        associate(inumlm => self%numlm1(iset))

        do jset = 1, self%numset2
            jsym_set = trim(jsym) // repeat('+', jset-1)
            associate(jinc => self%inc2(:, jset))
            associate(jnumlm => self%numlm2(jset))

            filename = trim(tbpar_dir) // '/'  // isym_set // '-' // &
                       jsym_set // '_offsite3c_' // trim(ksym)

            ext = '.3cf'
            write_3cb = .false.

            if (use_binary_io) then
                ext = '.3cb'
                inquire(file=filename // ext, exist=exists_3cb)

                if (.not. exists_3cb) then
                    ext = '.3cf'
                    write_3cb = .true.
                end if
            end if

            filename = filename // ext

            if (is_master()) then
                print *, 'Reading offsite3c parameters from ', filename
            end if

            call self%spl(iset, jset)%initialize(filename, &
                                                 eps_inner=eps_inner, &
                                                 eps_outer=eps_outer, &
                                                 upuvw=upsampling, &
                                                 write_3cb=write_3cb)
            end associate
            end associate
        end do

        end associate
        end associate
    end do
end subroutine initialize

end module tibi_offsite3c