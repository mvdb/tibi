!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_driver_init
!! Module for allocating and initializing a [[tibi_driver_base:BaseDriver_type]]

use tibi_atoms, only: Atoms_type
use tibi_driver_base, only: BaseDriver_type, &
                            SinglePointDriver_type
use tibi_driver_relaxation, only: RelaxationDriver_type
use tibi_driver_socket, only: SocketDriver_type
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_utils_prog, only: assert
implicit none


contains


subroutine initialize_driver(driver, input, atoms)
    !! Allocates the given driver to one of the non-abstract drivers based
    !! on the 'task' input keyword, and then further initializes that driver.
    class(BaseDriver_type), allocatable, intent(inout) :: driver
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('task')
    if (len_trim(buffer) == 0) then
        buffer = 'energy'
    end if

    if (trim(buffer) == 'energy' .or. trim(buffer) == 'energy_forces') then
        allocate(SinglePointDriver_type :: driver)
    elseif (trim(buffer) == 'relaxation') then
        allocate(RelaxationDriver_type :: driver)
    elseif (trim(buffer) == 'socket') then
        allocate(SocketDriver_type :: driver)
    else
        call assert(.false., 'Task "' // trim(buffer) // '" is not one of ' // &
                    'the known tasks ("energy", "energy_forces", ' // &
                    '"relaxation", "socket")')
    end if

    select type(driver)
    type is (SinglePointDriver_type)
        if (trim(buffer) == 'energy_forces') driver%need_forces = .true.
    type is (RelaxationDriver_type)
        call driver%initialize(input, atoms)
    type is (SocketDriver_type)
        call driver%initialize(input)
    end select
end subroutine initialize_driver

end module tibi_driver_init
