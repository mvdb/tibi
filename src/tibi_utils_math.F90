!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_utils_math
!! Module for small math-related utilities

use tibi_constants, only: dp
implicit none


interface copym
    procedure dcopym
    procedure zcopym
end interface copym


interface copym_lower2full
    procedure dcopym_lower2full_exsitu
    procedure dcopym_lower2full_insitu
    procedure zcopym_lower2full_exsitu
    procedure zcopym_lower2full_insitu
end interface copym_lower2full


interface copym_lower2upper
    procedure dcopym_lower2upper
    procedure zcopym_lower2upper
end interface copym_lower2upper


contains


pure function argsort(array, ascend) result(ordering)
    !! Returns the list of indices that would sort the given
    !! (double precision real) array.
    !!
    !! @NOTE The simple algorithm used here is not suitable
    !! for efficiently sorting large arrays.
    real(dp), dimension(:), intent(in) :: array
        !! the array for which to get the ordering
    logical, intent(in) :: ascend
        !! whether to ascend (low to high) or not (high to low)
    integer, dimension(size(array)) :: ordering

    integer :: i, index
    logical, dimension(size(array)) :: mask

    mask(:) = .true.

    do i = 1, size(array)
        if (ascend) then
            index = minloc(array, mask=mask, dim=1)
        else
            index = maxloc(array, mask=mask, dim=1)
        end if

        ordering(i) = index
        mask(index) = .false.
    end do
end function argsort


pure function calculate_3x3_matrix_determinant(A) result(det)
    !! Returns the determinant of a 3x3 matrix.
    real(dp), dimension(3, 3), intent(in) :: A
        !! matrix for which to calculate the determinant
    real(dp) :: det

    det = A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2) &
          - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1) &
          + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1)
end function calculate_3x3_matrix_determinant


subroutine dcopym(a, b)
    !! (Parallel) copy of one matrix to another.
    real(dp), dimension(:, :), intent(in) :: a
        !! input matrix
    real(dp), dimension(:, :), intent(out) :: b
        !! output matrix

    integer :: i, j

    ! In theory a workshare construct should achieve the same thing
    ! but in practice this is not always the case (e.g. when one of
    ! the matrices is a member of a derived type)

    !$omp parallel do default(private), schedule(static), shared(a, b), &
    !$omp&         collapse(2)
    do i = 1, size(a, dim=2)
        do j = 1, size(a, dim=1)
            b(j, i) = a(j, i)
        end do
    end do
    !$omp end parallel do
end subroutine dcopym


subroutine dcopym_lower2full_exsitu(n, a, b)
    !! Constructs the full (symmetric) output matrix
    !! from the lower triangular part of the input matrix.
    !!
    !! @NOTE Applies basic loop blocking to reduce cache misses
    !! if the matrices don't fit in cache.
    integer, intent(in) :: n
        !! order of the in- and output matrices
    real(dp), dimension(n, n), intent(in) :: a
        !! input matrix
    real(dp), dimension(n, n), intent(out) :: b
        !! output matrix

    integer :: i, j, k, l, m, ii, jj, kk
    integer, parameter :: stride = 128

    kk = ceiling(n * 1._dp / stride)
    kk = (kk * (kk + 1)) / 2

    ! Manually collapsing the original outer loops as compiler support for
    ! non-rectangular loop nest collapse is limited

    !$omp parallel do default(private), schedule(static), shared(a, b, kk, n)
    do k = 1, kk
        l = ceiling(0.5_dp * (sqrt(8*k + 1._dp) - 1))
        m = l - (l * (l + 1)) / 2 + k
        ii = 1 + stride * (l - 1)
        jj = 1 + stride * (m - 1)

        do i = ii, min(ii+stride-1, n)
            do j = jj, min(jj+stride-1, i)
                b(i, j) = a(i, j)
                b(j, i) = a(i, j)
            end do
        end do
    end do
    !$omp end parallel do
end subroutine dcopym_lower2full_exsitu


subroutine dcopym_lower2full_insitu(n, a)
    !! Fills the upper triangle of the given matrix
    !! with the transpose of its lower triangular part.
    !!
    !! @NOTE Applies basic loop blocking to reduce cache misses
    !! if the matrix doesn't fit in cache.
    integer, intent(in) :: n
        !! matrix order
    real(dp), dimension(n, n), intent(inout) :: a
        !! matrix to process

    integer :: i, j, k, l, m, ii, jj, kk
    integer, parameter :: stride = 128

    kk = ceiling(n * 1._dp / stride)
    kk = (kk * (kk + 1)) / 2

    ! Manually collapsing the original outer loops as compiler support for
    ! non-rectangular loop nest collapse is limited

    !$omp parallel do default(private), schedule(static), shared(a, kk, n)
    do k = 1, kk
        l = ceiling(0.5_dp * (sqrt(8*k + 1._dp) - 1))
        m = l - (l * (l + 1)) / 2 + k
        ii = 1 + stride * (l - 1)
        jj = 1 + stride * (m - 1)

        do i = ii, min(ii+stride-1, n)
            do j = jj, min(jj+stride-1, i-1)
                a(j, i) = a(i, j)
            end do
        end do
    end do
    !$omp end parallel do
end subroutine dcopym_lower2full_insitu


subroutine dcopym_lower2upper(n, a, b, include_diagonal)
    !! Copies the (transposed) lower triangle of the
    !! input matrix to the upper triangle of the output matrix.
    !!
    !! @NOTE Applies basic loop blocking to reduce cache misses
    !! if the matrices don't fit in cache.
    integer, intent(in) :: n
        !! order of the in- and output matrices
    real(dp), dimension(n, n), intent(in) :: a
        !! input matrix
    real(dp), dimension(n, n), intent(inout) :: b
        !! output matrix
    logical, intent(in), optional :: include_diagonal
        !! whether to also copy the diagonal elements (default: no)

    integer :: i, j, ii, jj
    integer, parameter :: stride = 128
    logical :: copy_diag

    copy_diag = .false.
    if (present(include_diagonal)) copy_diag = include_diagonal

    if (copy_diag) then
        do ii = 1, n, stride
            do jj = 1, ii, stride
                do i = ii, min(ii+stride-1, n)
                    do j = jj, min(jj+stride-1, i)
                        b(j, i) = a(i, j)
                    end do
                end do
            end do
        end do
    else
        do ii = 2, n, stride
            do jj = 1, ii-1, stride
                do i = ii, min(ii+stride-1, n)
                    do j = jj, min(jj+stride-1, i-1)
                        b(j, i) = a(i, j)
                    end do
                end do
            end do
        end do
    end if
end subroutine dcopym_lower2upper


subroutine zcopym(a, b)
    !! (Parallel) copy of one matrix to another.
    complex(dp), dimension(:, :), intent(in) :: a
        !! input matrix
    complex(dp), dimension(:, :), intent(out) :: b
        !! output matrix

    integer :: i, j

    ! In theory a workshare construct should achieve the same thing
    ! but in practice this is not always the case (e.g. when one of
    ! the matrices is a member of a derived type)

    !$omp parallel do default(private), shared(a, b), collapse(2)
    do i = 1, size(a, dim=2)
        do j = 1, size(a, dim=1)
            b(j, i) = a(j, i)
        end do
    end do
    !$omp end parallel do
end subroutine zcopym


subroutine zcopym_lower2full_exsitu(n, a, b)
    !! Constructs the full (Hermitian) output matrix
    !! from the lower triangular part of the input matrix.
    !!
    !! @NOTE Applies basic loop blocking to reduce cache misses
    !! if the matrices don't fit in cache.
    integer, intent(in) :: n
        !! order of the in- and output matrices
    complex(dp), dimension(n, n), intent(in) :: a
        !! input matrix
    complex(dp), dimension(n, n), intent(out) :: b
        !! output matrix

    integer :: i, j, k, l, m, ii, jj, kk
    integer, parameter :: stride = 128

    kk = ceiling(n * 1._dp / stride)
    kk = (kk * (kk + 1)) / 2

    ! Manually collapsing the original outer loops as compiler support for
    ! non-rectangular loop nest collapse is limited

    !$omp parallel do default(private), schedule(static), shared(a, b, kk, n)
    do k = 1, kk
        l = ceiling(0.5_dp * (sqrt(8*k + 1._dp) - 1))
        m = l - (l * (l + 1)) / 2 + k
        ii = 1 + stride * (l - 1)
        jj = 1 + stride * (m - 1)

        do i = ii, min(ii+stride-1, n)
            do j = jj, min(jj+stride-1, i)
                b(i, j) = a(i, j)
                b(j, i) = conjg(a(i, j))
            end do
        end do
    end do
    !$omp end parallel do
end subroutine zcopym_lower2full_exsitu


subroutine zcopym_lower2full_insitu(n, a)
    !! Fills the upper triangle of the given matrix
    !! with the conjugate transpose of its lower triangular part.
    !!
    !! @NOTE Applies basic loop blocking to reduce cache misses
    !! if the matrix doesn't fit in cache.
    integer, intent(in) :: n
        !! matrix order
    complex(dp), dimension(n, n), intent(inout) :: a
        !! matrix to process

    integer :: i, j, k, l, m, ii, jj, kk
    integer, parameter :: stride = 128

    kk = ceiling(n * 1._dp / stride)
    kk = (kk * (kk + 1)) / 2

    ! Manually collapsing the original outer loops as compiler support for
    ! non-rectangular loop nest collapse is limited

    !$omp parallel do default(private), schedule(static), shared(a, kk, n)
    do k = 1, kk
        l = ceiling(0.5_dp * (sqrt(8*k + 1._dp) - 1))
        m = l - (l * (l + 1)) / 2 + k
        ii = 1 + stride * (l - 1)
        jj = 1 + stride * (m - 1)

        do i = ii, min(ii+stride-1, n)
            do j = jj, min(jj+stride-1, i)
                a(j, i) = conjg(a(i, j))
            end do
        end do
    end do
    !$omp end parallel do
end subroutine zcopym_lower2full_insitu


subroutine zcopym_lower2upper(n, a, b, include_diagonal)
    !! Copies the (Hermitian transposed) lower triangle of the
    !! input matrix to the upper triangle of the output matrix.
    !!
    !! @NOTE Applies basic loop blocking to reduce cache misses
    !! if the matrices don't fit in cache.
    integer, intent(in) :: n
        !! order of the in- and output matrices
    complex(dp), dimension(n, n), intent(in) :: a
        !! input matrix
    complex(dp), dimension(n, n), intent(inout) :: b
        !! output matrix
    logical, intent(in), optional :: include_diagonal
        !! whether to also copy the diagonal elements (default: no)

    integer :: i, j, ii, jj
    integer, parameter :: stride = 64
    logical :: copy_diag

    copy_diag = .false.
    if (present(include_diagonal)) copy_diag = include_diagonal

    if (copy_diag) then
        do ii = 1, n, stride
            do jj = 1, ii, stride
                do i = ii, min(ii+stride-1, n)
                    do j = jj, min(jj+stride-1, i)
                        b(j, i) = conjg(a(i, j))
                    end do
                end do
            end do
        end do
    else
        do ii = 2, n, stride
            do jj = 1, ii-1, stride
                do i = ii, min(ii+stride-1, n)
                    do j = jj, min(jj+stride-1, i-1)
                        b(j, i) = conjg(a(i, j))
                    end do
                end do
            end do
        end do
    end if
end subroutine zcopym_lower2upper


pure function invert_3x3_matrix(A) result(B)
    !! Performs a direct calculation of the inverse of a 3×3 matrix
    !! (see <http://fortranwiki.org/fortran/show/Matrix+inversion>).
    real(dp), dimension(3, 3), intent(in) :: A
        !! matrix to be inverted
    real(dp), dimension(3, 3) :: B

    real(dp) :: detinv

    ! Calculate the inverse determinant of the matrix
    detinv = 1._dp / calculate_3x3_matrix_determinant(A)

    ! Calculate the inverse of the matrix
    B(1, 1) = detinv * (A(2, 2)*A(3, 3) - A(2, 3)*A(3, 2))
    B(2, 1) = -detinv * (A(2, 1)*A(3, 3) - A(2, 3)*A(3, 1))
    B(3, 1) = detinv * (A(2, 1)*A(3, 2) - A(2, 2)*A(3, 1))
    B(1, 2) = -detinv * (A(1, 2)*A(3, 3) - A(1, 3)*A(3, 2))
    B(2, 2) = detinv * (A(1, 1)*A(3, 3) - A(1, 3)*A(3, 1))
    B(3, 2) = -detinv * (A(1, 1)*A(3, 2) - A(1, 2)*A(3, 1))
    B(1, 3) = detinv * (A(1, 2)*A(2, 3) - A(1, 3)*A(2, 2))
    B(2, 3) = -detinv * (A(1, 1)*A(2, 3) - A(1, 3)*A(2, 1))
    B(3, 3) = detinv * (A(1, 1)*A(2, 2) - A(1, 2)*A(2, 1))
end function invert_3x3_matrix


pure function arange(first, last, stride) result(arr)
    !! Generates an integer sequence.
    integer, intent(in) :: first
        !! the starting value
    integer, intent(in) :: last
        !! the final value (inclusive!)
    integer, intent(in) :: stride
        !! the stride (nonzero)
    integer, dimension(:), allocatable :: arr

    integer :: i, N

    N  = int(ceiling((last - first) * 1._dp / stride))
    allocate(arr(N))
    arr = [(i, i = first, last, stride)]
end function arange


elemental function polyval(x, pow, der) result(fval)
    !! Function for computing \( x^{pow} \) and its derivatives.
    real(dp), intent(in) :: x
    integer, intent(in) :: pow
        !! must be either < 0 or >= der
    integer, intent(in) :: der
        !! the derivative wrt x
    real(dp) :: fval

    integer :: i

    fval = 1._dp
    do i = 1, der
        fval = fval * (pow - i + 1)
    end do
    fval = fval * x**(pow - der)
end function polyval


pure function cross(a, b) result(c)
    !! Returns the cross product of the given vectors.
    real(dp), dimension(3), intent(in) :: a, b
        !! input vectors
    real(dp), dimension(3) :: c

    c(1) = a(2) * b(3) - a(3) * b(2)
    c(2) = a(3) * b(1) - a(1) * b(3)
    c(3) = a(1) * b(2) - a(2) * b(1)
end function cross


pure subroutine symmetrize(matrix)
    !! Makes the given (real, double precision) matrix a symmetric one.
    real(dp), dimension(:, :), intent(inout) :: matrix
      !! matrix to symmetrize

    matrix = 0.5_dp * (matrix + transpose(matrix))
end subroutine symmetrize

end module tibi_utils_math
