!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_basis_sets_ao
!! Module for dealing with collections of AO basis sets

use tibi_atoms, only: Atoms_type
use tibi_basis_ao, only: BasisAO_type
use tibi_constants, only: dp
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_onsite1c_offsite2c, only: Onsite1cOffsite2c_type
use tibi_orbitals, only: get_nr_of_lm => get_nr_of_orbitals, &
                         l_labels, &
                         lm_labels, &
                         MAXL
use tibi_utils_prog, only: assert
use tibi_utils_string, only: count_items
implicit none


type BasisSetsAO_type
    !! A class for basis set information spanning different atomic kinds.
    type(BasisAO_type), dimension(:), allocatable :: basis_set
        !! basis set descriptors for every atomic kind
    character(len=16), dimension(:), allocatable :: basis_set_input
        !! Strings indicating which subshells to include
        !! for each atomic kind (e.g. ["sp_s", "s"]).
    integer :: maxnuml
        !! the largest number of included subshells among the atomic kinds
    integer :: maxnumlm
        !! the largest number of included orbitals among the atomic kinds
    integer :: maxnumzeta
        !! the largest zeta count among the atomic kinds
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: initialize_on1c_properties
    procedure :: get_nr_of_orbitals_atoms
    procedure :: get_nr_of_orbitals_kind
    procedure :: get_nr_of_subshells_atoms
    procedure :: get_nr_of_subshells_kind
    procedure :: get_nr_of_electrons
    procedure :: get_orbital_copies
    procedure :: get_orbital_occupations
    procedure :: get_subshell_properties
    procedure :: get_subshell_sums
    procedure :: get_zeta_count
    procedure :: print_description
    procedure :: process_basis_set_input
    generic :: get_nr_of_orbitals => get_nr_of_orbitals_atoms, &
                                     get_nr_of_orbitals_kind
    generic :: get_nr_of_subshells => get_nr_of_subshells_atoms, &
                                      get_nr_of_subshells_kind
end type BasisSetsAO_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(BasisSetsAO_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer
    integer :: nitems

    buffer = input%fetch_keyword('basis_set')
    call assert(len_trim(buffer) > 0, &
                'The mandatory "basis_set" keyword is either absent or empty')
    nitems = count_items(buffer)
    allocate(self%basis_set_input(nitems))
    read(buffer, *) self%basis_set_input
end subroutine fetch_input_keywords


subroutine initialize(self, input, atoms)
    !! Initialization from basis set info in the input and the atomic kinds.
    class(BasisSetsAO_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms

    integer :: ik

    call self%fetch_input_keywords(input)
    call self%process_basis_set_input(atoms)

    self%maxnuml = maxval([(self%get_nr_of_subshells(ik), ik = 1, atoms%nkind)])
    self%maxnumlm = maxval([(self%get_nr_of_orbitals(ik), ik = 1, atoms%nkind)])
    self%maxnumzeta = maxval([(self%get_zeta_count(ik), ik = 1, atoms%nkind)])
end subroutine initialize


subroutine initialize_on1c_properties(self, atoms, on1coff2c)
    !! Populates the atomic eigenvalues, Hubbard values and occupation numbers.
    class(BasisSetsAO_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    type(Onsite1cOffsite2c_type), dimension(:), intent(in) :: on1coff2c
        !! objects providing eigenvalues, Hubbard values and occupation numbers
        !! for every atomic kind

    integer :: ik, izeta

    do ik = 1, atoms%nkind
        do izeta = 1, self%basis_set(ik)%get_nr_of_zetas()
            self%basis_set(ik)%subsets(izeta)%eig = on1coff2c(ik)%eig(:, izeta)
            self%basis_set(ik)%subsets(izeta)%hub = on1coff2c(ik)%hub(:, izeta)
            self%basis_set(ik)%subsets(izeta)%occ = on1coff2c(ik)%occ(:, izeta)
        end do
    end do
end subroutine initialize_on1c_properties


pure function get_nr_of_orbitals_atoms(self, atoms) result(norbitals)
    !! Returns the number of orbitals for the given
    !! [[tibi_atoms:Atoms_type]] object.
    class(BasisSetsAO_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    integer :: norbitals

    integer :: iat, ik

    norbitals = 0
    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        norbitals = norbitals + self%get_nr_of_orbitals(ik)
    end do
end function get_nr_of_orbitals_atoms


pure function get_nr_of_orbitals_kind(self, ik) result(norbitals)
    !! Returns the number of orbitals for the given atomic kind.
    class(BasisSetsAO_type), intent(in) :: self
    integer, intent(in) :: ik
    integer :: norbitals

    norbitals = self%basis_set(ik)%get_nr_of_orbitals()
end function get_nr_of_orbitals_kind


pure function get_nr_of_subshells_atoms(self, atoms) result(nsubshells)
    !! Returns the number of subshells (angular momenta l) for the given
    !! [[tibi_atoms:Atoms_type]] object.
    class(BasisSetsAO_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    integer :: nsubshells

    integer :: iat, ik

    nsubshells = 0

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        nsubshells = nsubshells + self%get_nr_of_subshells(ik)
    end do
end function get_nr_of_subshells_atoms


pure function get_nr_of_subshells_kind(self, ik) result(nsubshells)
    !! Returns the number of subshells (angular momenta l) for the given
    !! atomic kind.
    class(BasisSetsAO_type), intent(in) :: self
    integer, intent(in) :: ik
    integer :: nsubshells

    nsubshells = self%basis_set(ik)%get_nr_of_subshells()
end function get_nr_of_subshells_kind


pure function get_nr_of_electrons(self, atoms) result(nelectrons)
    !! Returns the number of (valence) electrons for a given
    !! [[tibi_atoms:Atoms_type]] object.
    class(BasisSetsAO_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp) :: nelectrons

    integer :: iat, ik

    nelectrons = 0._dp

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        nelectrons = nelectrons + self%basis_set(ik)%get_nr_of_electrons()
    end do
end function get_nr_of_electrons


pure subroutine get_orbital_copies(self, ik, array_l, array_lm)
    !! Fills the given orbital-dependent array with copies of the corresponding
    !! entry in the subshell-dependent array, for the given atomic kind.
    class(BasisSetsAO_type), intent(in) :: self
    integer, intent(in) :: ik
    real(dp), dimension(:), intent(in) :: array_l
    real(dp), dimension(:), intent(out) :: array_lm

    integer :: counter_l, counter_lm, il, izeta, numlm
    logical, dimension(MAXL) :: inc

    counter_l = 1
    counter_lm = 1

    do izeta = 1, self%basis_set(ik)%get_nr_of_zetas()
        inc = self%basis_set(ik)%get_subset_included_subshells(izeta)

        do il = 1, size(inc)
            if (inc(il)) then
                numlm = get_nr_of_lm(il)
                array_lm(counter_lm:counter_lm+numlm-1) = array_l(counter_l)
                counter_l = counter_l + 1
                counter_lm = counter_lm + numlm
            end if
        end do
    end do
end subroutine get_orbital_copies


pure subroutine get_orbital_occupations(self, ik, array_lm)
    !! Fills the given array with the orbital occupations associated
    !! with the initial density (i.e. as in the free, spherically
    !! symmetric atom).
    class(BasisSetsAO_type), intent(in) :: self
    integer, intent(in) :: ik
        !! atomic kind index
    real(dp), dimension(:), intent(out) :: array_lm

    integer :: counter_lm, il, izeta, numlm
    logical, dimension(MAXL) :: inc

    do izeta = 1, self%basis_set(ik)%get_nr_of_zetas()
        inc = self%basis_set(ik)%get_subset_included_subshells(izeta)

        do il = 1, size(inc)
            if (inc(il)) then
                numlm = get_nr_of_lm(il)
                array_lm(counter_lm:counter_lm+numlm-1) = &
                    self%basis_set(ik)%get_subset_occupations(izeta) / numlm
                counter_lm = counter_lm + numlm
            end if
        end do
    end do
end subroutine get_orbital_occupations


pure subroutine get_subshell_properties(self, ik, array_l, property)
    !! Fills the given output array with the requested subshell-dependent
    !! properties (i.e. atomic eigenvalues, Hubbard values and occupations),
    !! for the given atomic kind.
    class(BasisSetsAO_type), intent(in) :: self
    integer, intent(in) :: ik
    real(dp), dimension(:), intent(out) :: array_l
    character(len=*), intent(in) :: property
        !! the property of interest ('eig', 'hub' or 'occ')

    integer :: counter_l, il, izeta
    logical, dimension(MAXL) :: inc
    real(dp), dimension(MAXL) :: tmp

    counter_l = 1

    do izeta = 1, self%basis_set(ik)%get_nr_of_zetas()
        inc = self%basis_set(ik)%get_subset_included_subshells(izeta)

        select case(property)
        case('eig')
            tmp = self%basis_set(ik)%get_subset_eigenvalues(izeta)
        case('hub')
            tmp = self%basis_set(ik)%get_subset_hubbardvalues(izeta)
        case('occ')
            tmp = self%basis_set(ik)%get_subset_occupations(izeta)
        end select

        do il = 1, size(inc)
            if (inc(il)) then
                array_l(counter_l) = tmp(il)
                counter_l = counter_l + 1
            end if
        end do
    end do
end subroutine get_subshell_properties


pure subroutine get_subshell_sums(self, ik, array_lm, array_l)
    !! Fills the given subshell-dependent array with the sums of the
    !! corresponding entries in the orbital-dependent array, for the
    !! given atomic kind.
    class(BasisSetsAO_type), intent(in) :: self
    integer, intent(in) :: ik
    real(dp), dimension(:), intent(in) :: array_lm
    real(dp), dimension(:), intent(out) :: array_l

    integer :: counter_l, counter_lm, il, izeta, numlm
    logical, dimension(MAXL) :: inc

    counter_l = 1
    counter_lm = 1

    do izeta = 1, self%basis_set(ik)%get_nr_of_zetas()
        inc = self%basis_set(ik)%get_subset_included_subshells(izeta)

        do il = 1, size(inc)
            if (inc(il)) then
                numlm = get_nr_of_lm(il)
                array_l(counter_l) = &
                    sum(array_lm(counter_lm:counter_lm+numlm-1))
                counter_l = counter_l + 1
                counter_lm = counter_lm + numlm
            end if
        end do
    end do
end subroutine get_subshell_sums


pure function get_zeta_count(self, ik) result(nzeta)
    !! Returns the zeta count for the given atomic kind
    !! (number of basis 'subsets').
    class(BasisSetsAO_type), intent(in) :: self
    integer, intent(in) :: ik
    integer :: nzeta

    nzeta = self%basis_set(ik)%get_nr_of_zetas()
end function get_zeta_count


subroutine print_description(self, atoms)
    !! Prints a description of the basis set
    class(BasisSetsAO_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: counter, ik, il, ilm, numl, numlm

    numl = self%get_nr_of_subshells(atoms)
    numlm =  self%get_nr_of_orbitals(atoms)

    print '(a)', ' Basis set description:'
    print '(a, i0)', '   Total number of subshells: ', numl
    print '(a, i0)', '   Total number of orbitals: ', numlm

    print '(a)', '   Subshells for each atomic kind:'
    do ik = 1, atoms%nkind
        print '(a, i0, a)', '     Kind ', ik, ': ' // &
              self%basis_set(ik)%get_repr()
    end do

    print '(a)', '   Orbitals in each subshell:'
    counter = 0
    do il = 1, MAXL
        numlm = get_nr_of_lm(il)
        write(*, '(a)', advance='no') '     Subshell ' // l_labels(il) // ':'
        do ilm = 1, numlm
            write(*, '(1x, a)', advance='no') trim(lm_labels(counter+ilm))
        end do
        print *
        counter = counter + numlm
    end do

    print *
end subroutine print_description


subroutine process_basis_set_input(self, atoms)
    !! Initializes self%basis_set based on self%basis_set_input.
    !!
    !! @NOTE self%basis_set is set up to follow the atoms' kind order,
    !! while self%basis_set_input will typically not be in that order.
    class(BasisSetsAO_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    character(len=:), allocatable :: symbol, symbol_basis
    integer :: i, ik, isym
    logical :: found

    allocate(self%basis_set(atoms%nkind))

    do ik = 1, atoms%nkind
        found = .false.
        symbol = trim(atoms%symbols(ik))

        sym_loop: do isym = 1, size(self%basis_set_input)
            symbol_basis = trim(self%basis_set_input(isym))

            i = scan(symbol_basis, '_')
            call assert(i > 0, 'No "_" delimiter found in "' // &
                        symbol_basis // '"')

            found = symbol == symbol_basis(:i-1)

            if (found) then
                call self%basis_set(ik)%initialize(symbol_basis(i+1:))
                exit sym_loop
            end if
        end do sym_loop

        call assert(found, 'Missing basis set info for "' // symbol // '"')
    end do
end subroutine process_basis_set_input

end module tibi_basis_sets_ao
