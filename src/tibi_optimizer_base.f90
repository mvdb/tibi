!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_optimizer_base
!! Module for a base class for optimization methods

use tibi_constants, only: dp
implicit none


type, abstract :: BaseOptimizer_type
    !! Base class for local optimization of (multi-variable,
    !! single-objective) functions
    integer :: iter
        !! iteration counter
    character(len=6) :: name = 'NoName'
        !! (short) name of the optimization algorithm
    integer :: vector_length
        !! length of the individual vectors (= number of function arguments)
    contains
    procedure(get_next_step), deferred :: get_next_step
    procedure(reset), deferred :: reset
end type BaseOptimizer_type


interface
    function get_next_step(self, xval, fval, fder) result(step)
        !! Returns the next step to take in the optimization cycle
        !! (normally to be added to xval).
        import BaseOptimizer_type, dp
        implicit none
        class(BaseOptimizer_type), intent(inout) :: self
        real(dp), dimension(self%vector_length), intent(in) :: xval
            !! array with the function arguments
        real(dp), intent(in) :: fval
            !! the function value
        real(dp), dimension(self%vector_length), intent(in) :: fder
            !! array with the function derivatives
        real(dp), dimension(self%vector_length) :: step
    end function get_next_step

    pure subroutine reset(self)
        !! Resets any history-dependent arrays and counters.
        import BaseOptimizer_type
        implicit none
        class(BaseOptimizer_type), intent(inout) :: self
    end subroutine reset
end interface


contains


end module tibi_optimizer_base
