!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_occupations) tibi_occupations_kpoints
!! Submodule for k-point related aspects of [[tibi_occupations]]

use iso_fortran_env, only: iostat_end
use tibi_constants, only: pi
implicit none


contains


pure module function calculate_phase_shift(self, ikpt, inverse_cell, &
                                           translation) result(phase_shift)
    !! Computes the phase shift for a k-point index, reciprocal cell,
    !! and Bravais lattice translation.
    !!
    !! \( \exp(2 \pi i \mathbf{k} \mathbf{C}^{-1} \cdot \mathbf{T}) \)
    class(Occupations_type), intent(in) :: self
    integer, intent(in) :: ikpt
        !! k-point index
    real(dp), dimension(3, 3), intent(in) :: inverse_cell
        !! matrix inverse of the real-space cell
    real(dp), dimension(:), intent(in) :: translation
        !! Bravais lattice translation vector
    complex(dp) :: phase_shift

    phase_shift = dot_product(matmul(self%kpoint_coords(:, ikpt), &
                                     inverse_cell), translation)
    phase_shift = exp(2 * pi * (0._dp, 1._dp) * phase_shift)
end function calculate_phase_shift


module subroutine print_kpoints_description(self)
    !! Prints a description of the k-point grid.
    class(Occupations_type), intent(in) :: self

    integer :: i

    print '(a)', ' K-points description:'
    print '("   Number of spins = ", (i0))', self%nspin
    print '("   Number of k-points = ", (i0))', self%nkpt
    print '(a)', '   Direct coordinates and weights of k-points:'

    do i = 1, self%nkpt
        print '("     K-point ", (i0.4), " coord =", 3(f11.6), "  weight = ", &
              & (f8.6))', i, self%kpoint_coords(:, i), self%kpoint_weights(i)
    end do
    print *
end subroutine print_kpoints_description


module subroutine set_kpoint_coordinates_and_weights(self)
    !! Sets the reduced k-point coordinates (self%kpoint_coords)
    !! and weights (self%kpoint_weights) based on self%kpts.
    class(Occupations_type), intent(inout) :: self

    integer, dimension(3) :: mp_grid

    if (count_items(self%kpts) == 3) then
        read(self%kpts, *) mp_grid
        call generate_mp_kpoint_coordinates_and_weights(self, mp_grid)
    else
        call read_kpoint_coordinates_and_weights(self, trim(self%kpts))
    end if

    call assert(abs(sum(self%kpoint_weights) - 1._dp) < 1e-8_dp, &
                'Bug: sum of k-point weights does not equal unity!')
end subroutine set_kpoint_coordinates_and_weights


subroutine generate_mp_kpoint_coordinates_and_weights(self, mp_grid)
    !! Calculates the coordinates and weights of the k-points in the
    !! irreducible Brillouin zone for the given Monkhorst-Pack grid.
    type(Occupations_type), intent(inout) :: self
    integer, dimension(3), intent(in) :: mp_grid
        !! tuple defining a Monkhorst-Pack uniform k-point grid

    integer :: i, ia, ib, ic, ikpt, jkpt, nfull
    logical :: is_included, is_gamma
    real(dp) :: spacing
    real(dp), parameter :: tol = 1e-8_dp
    real(dp), dimension(3) :: coord
    real(dp), dimension(:), allocatable :: tmp_weights
    real(dp), dimension(:, :), allocatable :: tmp_coords

    nfull = product(mp_grid)
    allocate(tmp_coords(3, nfull))
    allocate(tmp_weights(nfull))
    tmp_weights(:) = 1._dp / nfull

    ! (Na, Nb, Nc)-Monkhorst-Pack grid is a regular 3D grid
    ! in the Brillouin zone. This is how it looks in one
    ! dimension for N=3 (grid points marked with x):
    !
    !      -1/3       0       1/3
    !   |----x--------x--------x----|
    ! -1/2                         1/2
    ! BZ edge                    BZ edge

    ikpt = 0
    do ia = mp_grid(1), 1, -1
        do ib = mp_grid(2), 1, -1
            do ic = mp_grid(3), 1, -1
                ! Generate the coordinates of a new point on the full grid
                do i = 1, 3
                    spacing = 1._dp / mp_grid(i)

                    coord(i) = -0.5_dp + 0.5_dp * spacing
                    if (self%kpts_gamma_shift .and. &
                        modulo(mp_grid(i), 2) == 0) then
                        coord(i) = coord(i) + 0.5_dp * spacing
                    end if

                    associate(x => [ia, ib, ic])
                        coord(i) = coord(i) + (x(i) - 1) * spacing
                    end associate
                end do

                ! Add the k-point if any symmetry-equivalent point hasn't
                ! been added already (and increment the weights otherwise)
                ! Only time-inversion symmetry is considered here
                is_included = .false.
                do jkpt = 1, ikpt
                    is_included = all(abs(tmp_coords(:, jkpt) + coord(:)) < tol)
                    if (is_included) then
                        is_gamma = all(abs(tmp_coords(:, jkpt)) < tol)
                        if (.not. is_gamma) then
                            tmp_weights(jkpt) = tmp_weights(jkpt) &
                                                + 1._dp / nfull
                        end if
                        exit
                    end if
                end do

                if (.not. is_included) then
                    ikpt = ikpt + 1
                    tmp_coords(:, ikpt) = coord(:)
                end if
            end do
        end do
    end do

    if (allocated(self%kpoint_coords)) then
        deallocate(self%kpoint_coords)
    end if
    allocate(self%kpoint_coords(3, ikpt))
    self%kpoint_coords(:, :) = tmp_coords(:, 1:ikpt)

    if (allocated(self%kpoint_weights)) then
        deallocate(self%kpoint_weights)
    end if
    allocate(self%kpoint_weights(ikpt))
    self%kpoint_weights(:) = tmp_weights(1:ikpt)
end subroutine generate_mp_kpoint_coordinates_and_weights


subroutine read_kpoint_coordinates_and_weights(self, filename)
    !! Reads in the reduced k-point coordinates and weights from the
    !! given file.
    type(Occupations_type), intent(inout) :: self
    character(len=*), intent(in) :: filename

    integer :: fhandle, iostat, ikpt, nkpt

    open(newunit=fhandle, file=filename, status='old', action='read')

    ! First count the number of lines
    nkpt = 0
    do
        read(fhandle, iostat=iostat, fmt=*)
        if (iostat == iostat_end) exit
        nkpt = nkpt + 1
    end do
    call assert(nkpt > 0, 'K-point file ' // filename // ' is empty')

    ! Now process the actual content
    allocate(self%kpoint_weights(nkpt))
    allocate(self%kpoint_coords(3, nkpt))
    rewind(fhandle)
    do ikpt = 1, nkpt
        read(fhandle, *) self%kpoint_coords(:, ikpt), self%kpoint_weights(ikpt)
    end do

    close(fhandle)
end subroutine read_kpoint_coordinates_and_weights

end submodule tibi_occupations_kpoints
