!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_scf_tightbinding) tibi_scf_tightbinding_restart
!! Submodule for restart I/O for [[tibi_scf_tightbinding]]

use iso_fortran_env, only: iostat_end
implicit none


contains


module subroutine read_scf_restart_file(self, charges, magnetizations)
    !! Reads the self%scf_restart_input_file and fills in the given
    !! auxiliary charge array (and if, present, also the magnetizations).
    class(ScfTightBindingCalculator_type), intent(in) :: self
    real(dp), dimension(:), intent(out) :: charges
    real(dp), dimension(:), intent(out), optional :: magnetizations

    integer :: fhandle, iat, istart, iend, natom, status
    character(len=2048) :: buffer

    open(newunit=fhandle, file=trim(self%scf_restart_input_file), &
         status='old', action='read')

    natom = size(self%indices_l, dim=2)

    do iat = 1, natom
        if (self%scf_aux_mapping == 'giese_york') then
            istart = self%indices_mp(1, iat)
            iend = self%indices_mp(2, iat)
        elseif (self%scf_aux_mapping == 'mulliken') then
            istart = self%indices_l(1, iat)
            iend = self%indices_l(2, iat)
        end if

        read(fhandle, '(a)', iostat=status) buffer

        if (status == iostat_end) then
            call assert(.false., 'Premature ending when reading the ' // &
                        trim(self%scf_restart_input_file) // ' file')
        else
            if (present(magnetizations)) then
                read(buffer, *) charges(istart:iend), &
                                magnetizations(istart:iend)
            else
                read(buffer, *) charges(istart:iend)
            end if
        end if
    end do

    close(fhandle)
end subroutine read_scf_restart_file


module subroutine write_scf_restart_file(self, charges, magnetizations)
    !! Writes the self%scf_restart_output_file, with one line per atom
    !! with the auxiliary charges (and, if present, magnetizations).
    class(ScfTightBindingCalculator_type), intent(in) :: self
    real(dp), dimension(:), intent(in) :: charges
    real(dp), dimension(:), intent(in), optional :: magnetizations

    integer :: fhandle, iat, istart, iend, natom
    character(len=:), allocatable :: advance_str

    write(*, '(a)', advance='no') ' Writing auxiliary charges'
    if (present(magnetizations)) then
        write(*, '(a)', advance='no') ' and magnetizations'
    end if
    write(*, *) 'to ', trim(self%scf_restart_output_file)
    print *

    open(newunit=fhandle, file=trim(self%scf_restart_output_file), &
         status='replace', action='write')

    natom = size(self%indices_l, dim=2)

    if (present(magnetizations)) then
        advance_str = 'no'
    else
        advance_str = 'yes'
    end if

    do iat = 1, natom
        if (self%scf_aux_mapping == 'giese_york') then
            istart = self%indices_mp(1, iat)
            iend = self%indices_mp(2, iat)
        elseif (self%scf_aux_mapping == 'mulliken') then
            istart = self%indices_l(1, iat)
            iend = self%indices_l(2, iat)
        end if

        write(fhandle, '(*(es16.8))', advance=advance_str) charges(istart:iend)

        if (present(magnetizations)) then
            if (self%scf_aux_mapping == 'giese_york') then
                istart = self%indices_mp(1, iat)
                iend = self%indices_mp(2, iat)
            elseif (self%scf_aux_mapping == 'mulliken') then
                istart = self%indices_l(1, iat)
                iend = self%indices_l(2, iat)
            end if

            write(fhandle, '(*(es16.8))') magnetizations(istart:iend)
        end if
    end do

    close(fhandle)
end subroutine write_scf_restart_file

end submodule tibi_scf_tightbinding_restart
