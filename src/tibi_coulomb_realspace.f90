!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_coulomb_realspace
!! Base module for Coulomb interactions in real space

use tibi_atoms, only: Atoms_type
use tibi_constants, only: dp
use tibi_neighborlist_verlet, only: NeighborListVerlet_type
use tibi_realspace_pair, only: RealSpacePairCalculator_type
implicit none


type, abstract, extends(RealSpacePairCalculator_type) :: &
                        RealSpaceCoulombCalculator_type
    !! Abstract calculator for real-space Coulomb interactions
    character(len=:), allocatable :: distribution
        !! functional form of the charge distributions. Example values:
        !! **'delta'**: Dirac-\( \delta(r) \) functions, i.e. point charges,
        !! **'exponential'**: exponentially decaying functions
        !!            \( \rho(r) = q \frac{\tau^3}{8 \pi} \exp (-\tau r) \).
    real(dp) :: ewald_alpha = 0._dp
        !! Ewald screening parameter (zero means no screening)
        !! (see [Ewald (1921)](https://doi.org/10.1002/andp.19213690304))
    contains
    procedure :: determine_cutoff_radius_delta
    procedure(determine_cutoff_radii), deferred :: determine_cutoff_radii
    procedure :: determine_ewald_alpha
    procedure :: initialize_neighborlist
    procedure(add_local_potentials), deferred :: add_local_potentials
    procedure(calculate_local_potentials), deferred :: &
                                           calculate_local_potentials
    procedure(update_geometry), deferred :: update_geometry
    procedure(update_moments), deferred :: update_moments
end type RealSpaceCoulombCalculator_type


interface
    function determine_cutoff_radii(self, atoms, tolerance) result(rcuts)
        !! Returns the cutoff radii to be used in the neighbor list builds.
        import Atoms_type, dp, RealSpaceCoulombCalculator_type
        implicit none
        class(RealSpaceCoulombCalculator_type), intent(in) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp), intent(in) :: tolerance
            !! accuracy threshold
        real(dp), dimension(atoms%nkind, atoms%nkind) :: rcuts
    end function determine_cutoff_radii

    pure subroutine add_local_potentials(self, locpot)
        !! Adds, for each charge moment, the energy derivative w.r.t.
        !! that charge moment (e.g. the electrostatic potential in case
        !! of a charge monopole).
        import dp, RealSpaceCoulombCalculator_type
        implicit none
        class(RealSpaceCoulombCalculator_type), intent(in) :: self
        real(dp), dimension(:), intent(inout) :: locpot
    end subroutine add_local_potentials

    subroutine calculate_local_potentials(self)
        !! Calculates the energy derivatives w.r.t. the charge moments.
        import RealSpaceCoulombCalculator_type
        implicit none
        class(RealSpaceCoulombCalculator_type), intent(inout) :: self
    end subroutine calculate_local_potentials

    subroutine update_geometry(self, atoms)
        !! Updates the calculator parts which depend on the atomic positions
        !! and cell vectors.
        import Atoms_type, RealSpaceCoulombCalculator_type
        implicit none
        class(RealSpaceCoulombCalculator_type), intent(inout) :: self
        type(Atoms_type), intent(in) :: atoms
    end subroutine update_geometry

    pure subroutine update_moments(self, moments)
        !! Updates the calculator with respect to a new set of charge moments.
        import dp, RealSpaceCoulombCalculator_type
        implicit none
        class(RealSpaceCoulombCalculator_type), intent(inout) :: self
        real(dp), dimension(:), intent(in) :: moments
    end subroutine update_moments
end interface


contains


function determine_cutoff_radius_delta(self, tolerance) result(rcut)
    !! Returns the cutoff radius that satisfies the given tolerance
    !! in the case of point monopoles.
    class(RealSpaceCoulombCalculator_type), intent(in) :: self
    real(dp), intent(in) :: tolerance
        !! accuracy threshold
    real(dp) :: rcut

    real(dp), parameter :: dr = 0.1_dp
    real(dp) :: gam, s

    rcut = 0._dp
    gam = 2._dp * tolerance

    do while (abs(gam) > tolerance)
        rcut = rcut + dr
        s = eval_switchfunction_delta_gaussian(rcut, self%ewald_alpha)
        gam = (1._dp - s) / rcut
    end do
end function determine_cutoff_radius_delta


subroutine determine_ewald_alpha(self, tolerance)
    !! Sets the Ewald parameter (self%ewald_alpha) based on the smallest
    !! cutoff employed in the neighborlist and the given tolerance.
    class(RealSpaceCoulombCalculator_type), intent(inout) :: self
    real(dp), intent(in) :: tolerance
        !! accuracy threshold

    real(dp) :: gam, rcut, s
    real(dp), parameter :: step = 0.01_dp

    rcut = minval(self%nl%rcuts(:, :))
    self%ewald_alpha = 0._dp
    gam = 2._dp * tolerance

    do while (abs(gam) > tolerance)
        self%ewald_alpha = self%ewald_alpha + step
        s = eval_switchfunction_delta_gaussian(rcut, self%ewald_alpha)
        gam = (1._dp - s) / rcut
    end do
end subroutine determine_ewald_alpha


subroutine initialize_neighborlist(self, atoms, tolerance)
    !! Sets up the neighborlist based on the given tolerance.
    !!
    !! The cutoff radii are chosen such that a pair of unit charges beyond
    !! the cutoff would give an energy contribution that is less than the
    !! specified tolerance.
    class(RealSpaceCoulombCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), intent(in) :: tolerance
        !! accuracy threshold

    real(dp), dimension(atoms%nkind, atoms%nkind) :: rcuts

    ! First determine the cutoff radius for every kind pair
    if (atoms%is_periodic) then
        rcuts(:, :) = self%determine_cutoff_radii(atoms, tolerance)
    else
        rcuts(:, :) = 1._dp / tolerance
    end if

    ! Set up the neighbor list
    self%nl = NeighborListVerlet_type(atoms, rcuts, self_interaction=.false., &
                                      both_ways=.false.)
end subroutine initialize_neighborlist


elemental function eval_switchfunction_delta_gaussian(r, alpha) result(s)
    !! Returns the function value of the switching function S for a
    !! Dirac delta and a Gaussian charge density, with S defined by
    !! \( \gamma(r; param) = S(r; param) / r \), with \( \gamma \)
    !! the Coulomb interaction kernel.
    real(dp), intent(in) :: r
        !! the interatomic distance
    real(dp), intent(in) :: alpha
        !! Ewald parameter (related to the standard deviation sigma
        !! of a Gaussian distribution as \( \alpha = 1 / (\sqrt{2} \sigma)  \)
    real(dp) :: s

    s = erf(alpha * r)
end function eval_switchfunction_delta_gaussian

end module tibi_coulomb_realspace
