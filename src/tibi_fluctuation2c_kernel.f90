!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_fluctuation2c_kernel
!! Module for evaluating two-center spin/charge fluctuation kernels

use tibi_basis_ao, only: BasisAO_type
use tibi_constants, only: dp
use tibi_orbitals, only: calculate_slako_coeff_batch, &
                         calculate_slako_coeff_deriv_batch, &
                         orbital_index
use tibi_spline, only: BatchSpline_type, &
                       CatmullRomSpline_type
use tibi_utils_prog, only: assert, &
                           is_master
use tibi_utils_string, only: find_string_index
implicit none


integer, parameter :: MAXSK = 14
    !! number of Slater-Koster integrals for the multipole-dependent case
    !! (see sk_labels below)

integer, parameter :: MAXTAU = 5
    !! maximum number of of Slater-Koster integral types (5 = up to gamma)

character(len=3), dimension(MAXSK), parameter :: sk_labels = &
    ['sss', 'sps', 'sds', 'pss', 'pps', 'ppp', 'pds', 'pdp', &
     'dss', 'dps', 'dpp', 'dds', 'ddp', 'ddd']
    !! Slater-Koster integral labels


type, abstract :: Fluctuation2cKernelBase_type
    !! Abstract type for evaluates the two-center fluctuation kernels
    !! for one kind pair by interpolation.
    integer :: numl1
        !! number of subshells for the first kind
    integer :: numl2
        !! number of subshells for the second kind
    real(dp) :: rcut
        !! cutoff radius
    contains
    procedure(evaluate), deferred :: evaluate
    procedure(evaluate_derivative), deferred :: evaluate_derivative
end type Fluctuation2cKernelBase_type


interface
    pure subroutine evaluate(self, x, y, z, r, K)
        !! Abstract interface for evaluating the (rotated) fluctuation
        !! kernels, which the derived types must implement.
        import dp, Fluctuation2cKernelBase_type
        implicit none
        class(Fluctuation2cKernelBase_type), intent(in) :: self
        real(dp), intent(in) :: x, y, z
            !! normalized vector components
        real(dp), intent(in) :: r
            !! interatomic distance
        real(dp), dimension(:, :), intent(out) :: K
            !! output kernel matrix
    end subroutine evaluate


    pure subroutine evaluate_derivative(self, x, y, z, r, dK)
        !! Abstract interface for evaluating the (rotated) fluctuation
        !! kernel derivatives, which the derived types must implement.
        import dp, Fluctuation2cKernelBase_type
        implicit none
        class(Fluctuation2cKernelBase_type), intent(in) :: self
        real(dp), intent(in) :: x, y, z
            !! normalized vector components
        real(dp), intent(in) :: r
            !! interatomic distance
        real(dp), dimension(:, :, :), intent(out) :: dK
            !! output kernel matrix derivatives
    end subroutine evaluate_derivative
end interface


type, extends(Fluctuation2cKernelBase_type) :: Fluctuation2cKernelSubshell_type
    !! Evaluates the subshell-dependent two-center fluctuation kernel
    !! for one atomic kind pair by interpolation.
    type(CatmullRomSpline_type), dimension(:, :), allocatable :: spl
        !! spline interpolators for every subshell pair
    contains
    procedure :: evaluate => evaluate_subshell
    procedure :: evaluate_derivative => evaluate_subshell_derivative
    procedure :: initialize => initialize_from_2cl
end type Fluctuation2cKernelSubshell_type


type, extends(BatchSpline_type) :: FluctuationBatchSpline_type
    contains
    procedure :: initialize_batch
end type FluctuationBatchSpline_type


type, extends(Fluctuation2cKernelBase_type) :: Fluctuation2cKernelMultipole_type
    !! Evaluates the multipole-dependent two-center fluctuation kernel
    !! for one atomic kind pair by interpolation.
    integer, dimension(2) :: ilmax
        !! maximum angular momentum indices in the multipole expansion
    type(FluctuationBatchSpline_type), dimension(:, :), allocatable :: spl
        !! batch spline interpolator for all needed integrals
    contains
    procedure :: evaluate => evaluate_multipole
    procedure :: evaluate_derivative => evaluate_multipole_derivative
    procedure :: initialize => initialize_from_2ck
end type Fluctuation2cKernelMultipole_type


contains


pure subroutine evaluate_subshell(self, x, y, z, r, K)
    !! Implementation of Fluctuation2cKernelBase_type%evaluate
    !! for Fluctuation2cKernelSubshell_type.
    class(Fluctuation2cKernelSubshell_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :), intent(out) :: K
        !! output kernel matrix

    integer :: il, jl

    do il = 1, self%numl1
        do jl = 1, self%numl2
            K(il, jl) = self%spl(il, jl)%evaluate(r, der=0)
        end do
    end do
end subroutine evaluate_subshell


pure subroutine evaluate_subshell_derivative(self, x, y, z, r, dK)
    !! Implementation of Fluctuation2cKernelBase_type%evaluate_derivative
    !! for Fluctuation2cKernelSubshell_type.
    class(Fluctuation2cKernelSubshell_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :, :), intent(out) :: dK
        !! output kernel matrix derivatives

    integer :: il, jl
    real(dp) :: dKdr

    do il = 1, self%numl1
        do jl = 1, self%numl2
            dKdr = self%spl(il, jl)%evaluate(r, der=1)
            dK(:, il, jl) = [-x*dKdr, -y*dKdr, -z*dKdr]
        end do
    end do
end subroutine evaluate_subshell_derivative


pure subroutine evaluate_multipole(self, x, y, z, r, K)
    !! Implementation of Fluctuation2cKernelBase_type%evaluate
    !! for Fluctuation2cKernelMultipole_type.
    class(Fluctuation2cKernelMultipole_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :), intent(out) :: K
        !! output kernel matrix

    integer :: il, jl, ilm, jlm, ilmcount, jlmcount, iint
    integer :: imulti, jmulti, izeta, jzeta
    real(dp), dimension(3) :: tmp
    real(dp), dimension(MAXSK) :: refint
    real(dp), dimension(MAXTAU) :: c1
    real(dp), dimension(3, self%ilmax(1)**2, self%ilmax(2)**2) :: coeff

    imulti = self%ilmax(1)**2
    jmulti = self%ilmax(2)**2

    do jlm = orbital_index(1), orbital_index(self%ilmax(2)+1)-1
        do ilm = orbital_index(1), orbital_index(self%ilmax(1)+1)-1
            if (jlm >= ilm) then
                c1 = calculate_slako_coeff_batch(x, y, z, ilm, jlm)
            else
                c1 = calculate_slako_coeff_batch(x, y, z, jlm, ilm)
            end if

            coeff(:, ilm, jlm) = c1(1:3)
        end do
    end do

    jlmcount = 1
    do jzeta = 1, self%numl2

        ilmcount = 1
        do izeta = 1, self%numl1
            ! Calculate integrals in the reference orientation
            call self%spl(izeta, jzeta)%evaluate_batch(r, refint)

            ! Rotate to the actual geometry
            do jl = 1, self%ilmax(2)
                do jlm = orbital_index(jl), orbital_index(jl+1)-1
                    do il = 1, self%ilmax(1)
                        iint = get_integral_start_index(il, jl)
                        tmp(1:min(il, jl)) = refint(iint:iint+min(il, jl)-1)
                        tmp(min(il, jl)+1:3) = 0._dp

                        do ilm = orbital_index(il), orbital_index(il+1)-1
                            K(ilmcount+ilm-1, jlmcount+jlm-1) = &
                                sum(coeff(:, ilm, jlm) * tmp)
                        end do
                    end do
                end do
            end do

            ilmcount = ilmcount + imulti
        end do

        jlmcount = jlmcount + jmulti
    end do
end subroutine evaluate_multipole


pure subroutine evaluate_multipole_derivative(self, x, y, z, r, dK)
    !! Implementation of Fluctuation2cKernelBase_type%evaluate_derivative
    !! for Fluctuation2cKernelMultipole_type.
    class(Fluctuation2cKernelMultipole_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :, :), intent(out) :: dK
        !! output kernel matrix derivatives

    integer :: ider, il, jl, ilm, jlm, ilmcount, jlmcount, iint
    integer :: imulti, jmulti, izeta, jzeta
    real(dp), dimension(3) :: dcoeffdx, dr, tmp, dtmp
    real(dp), dimension(3, 3) :: dxyz
    real(dp), dimension(MAXSK) :: refint, drefint
    real(dp), dimension(MAXTAU) :: c1
    real(dp), dimension(MAXTAU, 3) :: dc1
    real(dp), dimension(3, self%ilmax(1)**2, self%ilmax(2)**2) :: coeff
    real(dp), dimension(3, 3, self%ilmax(1)**2, self%ilmax(2)**2) :: dcoeff

    imulti = self%ilmax(1)**2
    jmulti = self%ilmax(2)**2

    do jlm = orbital_index(1), orbital_index(self%ilmax(2)+1)-1
        do ilm = orbital_index(1), orbital_index(self%ilmax(1)+1)-1
            if (jlm >= ilm) then
                call calculate_slako_coeff_deriv_batch(x, y, z, ilm, &
                                                       jlm, c1, dc1)
            else
                call calculate_slako_coeff_deriv_batch(x, y, z, jlm, &
                                                       ilm, c1, dc1)
            end if
            coeff(:, ilm, jlm) = c1(1:3)
            dcoeff(:, :, ilm, jlm) = dc1(1:3, :)
        end do
    end do

    dr = [-x, -y, -z]
    dxyz(:, 1) = [(x**2 - 1._dp) / r, x * y / r, x * z / r]
    dxyz(:, 2) = [x * y / r, (y**2 - 1._dp) / r, y * z / r]
    dxyz(:, 3) = [x * z / r, y * z / r, (z**2 - 1._dp) / r]

    jlmcount = 1
    do jzeta = 1, self%numl2

        ilmcount = 1
        do izeta = 1, self%numl1
            ! Calculate integrals in the reference orientation
            call self%spl(izeta, jzeta)%evaluate_batch(r, refint, drefint)

            ! Rotate to the actual geometry
            do jl = 1, self%ilmax(2)
                do jlm = orbital_index(jl), orbital_index(jl+1)-1
                    do il = 1, self%ilmax(1)
                        iint = get_integral_start_index(il, jl)
                        tmp(1:min(il, jl)) = refint(iint:iint+min(il, jl)-1)
                        tmp(min(il, jl)+1:3) = 0._dp
                        dtmp(1:min(il, jl)) = drefint(iint:iint+min(il, jl)-1)
                        dtmp(min(il, jl)+1:3) = 0._dp

                        do ilm = orbital_index(il), orbital_index(il+1)-1
                            do ider = 1, 3
                                dcoeffdx = matmul(dcoeff(:, :, ilm, jlm), &
                                                  dxyz(:, ider))
                                dK(ider, ilmcount+ilm-1, jlmcount+jlm-1) = &
                                    sum(dcoeffdx * tmp &
                                        + coeff(:, ilm, jlm) * dtmp * dr(ider))
                            end do
                        end do
                    end do
                end do
            end do

            ilmcount = ilmcount + imulti
        end do

        jlmcount = jlmcount + jmulti
    end do
end subroutine evaluate_multipole_derivative


elemental function get_integral_start_index(il, jl) result(iint)
    !! Given two subshell indices returns the start index of the
    !! corresponding Slater-Koster integrals (i.e. for tau=1).
    integer, intent(in) :: il
        !! the first subshell index
    integer, intent(in) :: jl
        !! the second subshell index
    integer :: iint

    iint = 0

    if (il == 1) then
        iint = jl  ! s**

    elseif (il == 2) then
        if (jl == 1) then
            iint = 4  ! ps*
        elseif (jl == 2) then
            iint = 5  ! pp*
        elseif (jl == 3) then
            iint = 7  ! pd*
        end if

    elseif (il == 3) then
        if (jl == 1) then
            iint = 9  ! ds*
        elseif (jl == 2) then
            iint = 10  ! dp*
        elseif (jl == 3) then
            iint = 12  ! dd*
        end if
    end if
end function get_integral_start_index


subroutine initialize_from_2cl(self, tbpar_dir, isym, jsym, ibasis, jbasis, &
                               is_onsite, variant)
    !! Initializes a subshell-dependent two-center fluctuation kernel type
    !! with parameters from '2cl' files.
    class(Fluctuation2cKernelSubshell_type), intent(out) :: self
    character(len=*), intent(in) :: tbpar_dir
        !! path to the directory with the parameter files
    character(len=2), intent(in) :: isym
        !! element symbol for the first atomic kind
    character(len=2), intent(in) :: jsym
        !! element symbol for the second atomic kind
    type(BasisAO_type), intent(in) :: ibasis
        !! basis set description for the first atomic kind
    type(BasisAO_type), intent(in) :: jbasis
        !! basis set description for the second atomic kind
    logical, intent(in) :: is_onsite
        !! whether onsite or offsite two-center data is needed
    character, intent(in) :: variant
        !! fluctuation type (e.g. 'U' or 'W')

    character(len=:), allocatable :: filename, isym_zeta, jsym_zeta
    logical, dimension(:), allocatable :: iinc, jinc
    integer :: il, jl, izeta, jzeta

    self%numl1 = ibasis%get_nr_of_subshells()
    self%numl2 = jbasis%get_nr_of_subshells()
    allocate(self%spl(self%numl1, self%numl2))

    il = 1
    do izeta = 1, ibasis%get_nr_of_zetas()
        isym_zeta = trim(isym) // repeat('+', izeta-1)
        iinc = ibasis%get_subset_included_subshells(izeta)

        jl = 1
        do jzeta = 1, jbasis%get_nr_of_zetas()
            if (is_onsite) then
                jsym_zeta = trim(isym) // repeat('+', jzeta-1)
            else
                jsym_zeta = trim(jsym) // repeat('+', jzeta-1)
            end if

            jinc = jbasis%get_subset_included_subshells(jzeta)

            filename = trim(tbpar_dir) // '/'  // isym_zeta // '-' // jsym_zeta
            if (is_onsite) then
                filename = filename // '_onsite' // variant // '_' // trim(jsym)
            else
                filename = filename // '_offsite' // variant
            end if
            filename = filename // '.2cl'

            if (is_master()) then
                if (is_onsite) then
                    print *, 'Reading onsite' // variant // ' table from ', &
                          filename
                else
                    print *, 'Reading offsite' // variant // ' table from ', &
                          filename
                end if
            end if

            self%spl(il:il+count(iinc)-1, jl:jl+count(jinc)-1) = &
                                        read_2cl_file(filename, iinc, jinc)
            jl = jl + count(jinc)
        end do

        il = il + count(iinc)
    end do

    self%rcut = 0._dp
    do il = 1, self%numl1
        do jl = 1, self%numl2
            self%rcut = max(self%rcut, self%spl(il, jl)%rcut)
        end do
    end do
end subroutine initialize_from_2cl


subroutine initialize_from_2ck(self, tbpar_dir, isym, jsym, is_onsite, &
                               variant, ilmax, use_delta, nzeta)
    !! Initializes a multipole-dependent two-center fluctuation kernel type
    !! with parameters from '2ck' files.
    class(Fluctuation2cKernelMultipole_type), intent(out) :: self
    character(len=*), intent(in) :: tbpar_dir
        !! path to the directory with the parameter files
    character(len=2), intent(in) :: isym
        !! element symbols for the first atomic kind
    character(len=2), intent(in) :: jsym
        !! element symbols for the second atomic kind
    logical, intent(in) :: is_onsite
        !! whether onsite or offsite two-center data is needed
    character, intent(in) :: variant
        !! fluctuation type (e.g. 'U' or 'W')
    integer, dimension(2), intent(in) :: ilmax
        !! maximum angular momentum indices in multipole expansions
    logical, intent(in) :: use_delta
        !! if false, add the point multipole contributions to the
        !! tabulated integrals (by default those are not included)
    integer, dimension(2), intent(in) :: nzeta
        !! zeta counts in multipole expansions

    character(len=:), allocatable :: filename, isym_zeta, jsym_zeta
    integer :: izeta, jzeta

    if (is_onsite) then
        call assert(.not. use_delta, 'Subtracting point multipole ' // &
                    'contributions does not make sense in the onsite case.')
    end if

    self%ilmax = ilmax
    self%rcut = 0._dp
    self%numl1 = nzeta(1)
    self%numl2 = nzeta(2)

    allocate(self%spl(self%numl1, self%numl2))

    do izeta = 1, self%numl1
        isym_zeta = trim(isym) // repeat('+', izeta-1)

        do jzeta = 1, self%numl2
            if (is_onsite) then
                jsym_zeta = trim(isym) // repeat('+', jzeta-1)
            else
                jsym_zeta = trim(jsym) // repeat('+', jzeta-1)
            end if

            filename = trim(tbpar_dir) // '/'  // isym_zeta // '-' // jsym_zeta
            if (is_onsite) then
                filename = filename // '_onsite' // variant // '_' // trim(jsym)
            else
                filename = filename // '_offsite' // variant
            end if
            filename = filename // '.2ck'

            call read_2ck_file(filename, self%ilmax, use_delta, &
                               self%spl(izeta, jzeta))

            self%rcut = max(self%rcut, self%spl(izeta, jzeta)%rcut)
        end do
    end do
end subroutine initialize_from_2ck


function read_2cl_file(filename, iinc, jinc) result(func)
    !! Returns a two-dimensional array of spline evaluators
    !! to interpolate the data in the given '2cl' file.
    character(len=*), intent(in) :: filename
        !! name of the parameter file
    logical, dimension(:), intent(in) :: iinc
        !! whether an angular momentum is present in the first basis subset
    logical, dimension(:), intent(in) :: jinc
        !! whether an angular momentum is present in the second basis subset
    type(CatmullRomSpline_type), &
        dimension(count(iinc), count(jinc)) :: func

    integer, parameter :: MAXL_2CL = 4
    integer :: fhandle, i, icol, il, jl, ilcount, jlcount, numr
    real(dp) :: dr, rcut
    real(dp), dimension(:, :), allocatable :: table

    open(newunit=fhandle, file=filename, status='old', action='read')

    read(fhandle, *) dr, numr
    rcut = numr * dr
    allocate(table(numr, MAXL_2CL**2))

    do i = 1, numr
        read(fhandle, *) table(i, :)
    end do

    close(fhandle)

    icol = 1
    ilcount = 1
    do il = 1, MAXL_2CL
        if (.not. iinc(il)) then
            icol = icol + MAXL_2CL
            cycle
        end if

        jlcount = 1
        do jl = 1, MAXL_2CL
            if (.not. jinc(jl)) then
                icol = icol + 1
                cycle
            end if

            associate(f => func(ilcount, jlcount))
            f%r0 = dr
            f%dr = dr
            f%rcut = rcut
            call f%build_catmull_rom_coefficients(table(:, icol))
            end associate

            icol = icol + 1
            jlcount = jlcount + 1
        end do

        ilcount = ilcount + 1
    end do
end function read_2cl_file


subroutine read_2ck_file(filename, ilmax, use_delta, spl)
    !! Builds the array of spline evaluators to interpolate the integrals
    !! in the given '2ck' file.
    character(len=*), intent(in) :: filename
        !! name of the parameter file
    integer, dimension(2), intent(in) :: ilmax
        !! maximum angular momentum indices in multipole expansions
    logical, intent(in) :: use_delta
        !! if false, add the point multipole contributions to the
        !! tabulated integrals (by default those are not included)
    type(FluctuationBatchSpline_type), intent(out) :: spl

    integer :: fhandle, i, il, jl, iint, numr
    integer, dimension(MAXSK) :: powers
    real(dp) :: dr, r, rcut
    real(dp), dimension(MAXSK) :: point_kernels
    real(dp), dimension(:, :), allocatable :: table

    if (.not. use_delta) then
        powers = 0
        do il = 1, ilmax(1)
            do jl = 1, ilmax(2)
                iint = get_integral_start_index(il, jl)
                powers(iint:iint+min(il, jl)-1) = il + jl - 1
            end do
        end do
    end if

    open(newunit=fhandle, file=filename, status='old', action='read')

    read(fhandle, *) dr, numr
    rcut = numr * dr

    allocate(table(MAXSK, numr))

    read(fhandle, *) point_kernels

    do i = 1, numr
        read(fhandle, *) table(:, i)

        if (.not. use_delta) then
            r = i * dr
            table(:, i) = table(:, i) + point_kernels / r**powers
        end if
    end do

    call spl%initialize_batch(dr, dr, rcut, table, ilmax)

    close(fhandle)
end subroutine read_2ck_file


subroutine initialize_batch(self, r0, dr, rcut, table, ilmax)
    !! Initializes the batch spline interpolator.
    class(FluctuationBatchSpline_type), intent(out) :: self
    real(dp), intent(in) :: r0
        !! first grid point
    real(dp), intent(in) :: dr
        !! grid spacing
    real(dp), intent(in) :: rcut
        !! grid cutoff
    real(dp), dimension(:, :), intent(in) :: table
        !! (MAXSK, numr) integral table
    integer, dimension(2), intent(in) :: ilmax
        !! maximum angular momentum indices in multipole expansions

    integer :: i, iint

    self%dr = dr
    self%r0 = r0
    self%rcut = rcut
    self%maxsk = MAXSK

    ! Find non-zero contributions
    allocate(self%mask(self%maxsk))
    do iint = 1, MAXSK
        call is_needed(self%mask(iint))
    end do

    self%numint = count(self%mask)
    self%numr = size(table, dim=2)
    allocate(self%table(self%numint, self%numr))

    i = 1
    do iint = 1, MAXSK
        if (self%mask(iint)) then
            self%table(i, :) = table(iint, :)
            i = i + 1
        end if
    end do


    contains


    subroutine is_needed(bool)
        logical, intent(out) :: bool

        integer :: il, jl
        character(len=1), dimension(3), parameter :: l_labels = ['s', 'p', 'd']

        associate(s => sk_labels(iint))
        il = find_string_index(l_labels, s(1:1))
        jl = find_string_index(l_labels, s(2:2))
        end associate

        if (il > ilmax(1) .or. jl > ilmax(2)) then
            bool = .false.
            return
        end if

        bool = .true.
    end subroutine is_needed
end subroutine initialize_batch

end module tibi_fluctuation2c_kernel
