!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_onsite1c_offsite2c
!! Module for evaluating on-site one-center and off-site two-center
!! Hamiltonian and overlap integrals

use tibi_basis_ao, only: BasisAO_type
use tibi_constants, only: dp
use tibi_orbitals, only: calculate_slako_coeff_batch, &
                         calculate_slako_coeff_deriv_batch, &
                         l_labels, &
                         MAXL, &
                         MAXLM, &
                         MAXTAU, &
                         orbital_index
use tibi_spline, only: BatchSpline_type
use tibi_twobody, only: PolynomialFunction_type
use tibi_utils_prog, only: assert, &
                           is_master
use tibi_utils_string, only: count_items, &
                             find_string_index, &
                             replace_tabs_by_whitespace
implicit none


integer, parameter :: MAXSK = 30
    !! number of (redundant) Slater-Koster integrals (see sk_labels below)

character(len=3), dimension(MAXSK), parameter :: sk_labels &
    = ['sss', 'sps', 'sds', 'sfs', &
       'pss', 'pps', 'ppp', 'pds', 'pdp', 'pfs', 'pfp', &
       'dss', 'dps', 'dpp', 'dds', 'ddp', 'ddd', 'dfs', 'dfp', 'dfd', &
       'fss', 'fps', 'fpp', 'fds', 'fdp', 'fdd', 'ffs', 'ffp', 'ffd', 'fff']
    !! (redundant) Slater-Koster integral labels


type, extends(BatchSpline_type) :: TwocenterBatchSpline_type
    contains
    procedure :: initialize_batch
end type TwocenterBatchSpline_type


type Onsite1cOffsite2c_type
    !! Class for evaluating the on-site one-center and off-site two-center
    !! H and S contributions for one kind pair.
    real(dp), dimension(:, :), allocatable :: eig
        !! atomic eigenvalues for every angular momentum
        !! and basis subset (if homonuclear)
    real(dp), dimension(:, :), allocatable :: hub
        !! Hubbard values for every angular momentum
        !! and basis subset (if homonuclear)
    logical, dimension(:, :), allocatable :: inc1
        !! (MAXL, numset1) array with included subshells for the first kind
    logical, dimension(:, :), allocatable :: inc2
        !! (MAXL, numset2) array with included subshells for the second kind
    integer, dimension(:), allocatable :: numlm1
        !! number of orbitals for the first kind
    integer, dimension(:), allocatable :: numlm2
        !! number of orbitals for the second kind
    integer :: numset1
        !! number of basis subsets for the first kind
    integer :: numset2
        !! number of basis subsets for the second kind
    real(dp), dimension(:, :), allocatable :: occ
        !! occupation numbers for every angular momentum
        !! and basis subset (if homonuclear)
    real(dp), dimension(:, :, :), allocatable :: offdiagH
        !! off-diagonal, on-site (and one-center) Hamiltonian integrals
        !! for every angular momentum and basis subset pair (if homonuclear)
    real(dp), dimension(:, :, :), allocatable :: offdiagS
        !! off-diagonal, on-site (and one-center) overlap integrals
        !! for every angular momentum and basis subset pair (if homonuclear)
    real(dp), dimension(:, :), allocatable :: onsiteH
        !! onsite Hamiltonian matrix (if homonuclear)
    real(dp), dimension(:, :), allocatable :: onsiteS
        !! onsite overlap matrix (if homonuclear)
    real(dp) :: rcut
        !! cutoff radius
    type(TwocenterBatchSpline_type), dimension(:, :), allocatable :: spl_h
        !! batch spline interpolator for the Hamiltonian integrals
        !! for every pair of main basis subsets
    type(TwocenterBatchSpline_type), dimension(:, :), allocatable :: spl_s
        !! batch spline interpolator for the overlap integrals
        !! for every pair of main basis subsets
    contains
    procedure :: evaluate_onsite
    procedure :: evaluate_offsite
    procedure :: evaluate_offsite_derivs
    procedure :: initialize_from_skf
end type Onsite1cOffsite2c_type


contains


subroutine evaluate_onsite(self, h, s)
    !! Returns the on-site, one-center Hamiltonian and overlap matrix elements
    !! for all included orbitals.
    class(Onsite1cOffsite2c_type), intent(in) :: self
    real(dp), dimension(:, :), intent(out) :: h
        !! Hamiltonian integral matrix
    real(dp), dimension(:, :), intent(out) :: s
        !! overlap integral matrix

    h = self%onsiteH
    s = self%onsiteS
end subroutine evaluate_onsite


subroutine evaluate_offsite(self, x, y, z, r, h, s)
    !! Returns the off-site, two-center Hamiltonian and overlap matrix elements
    !! for all included orbitals.
    class(Onsite1cOffsite2c_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :), intent(out) :: h
        !! Hamiltonian integral matrix
    real(dp), dimension(:, :), intent(out) :: s
        !! overlap integral matrix

    integer :: il, jl, ilm, jlm, ilmcount, jlmcount
    integer :: iint, ntau, iset, jset
    real(dp), dimension(MAXSK) :: refint_h, refint_s
    real(dp), dimension(MAXTAU+1) :: c1
    real(dp), dimension(MAXTAU) :: tmp_h, tmp_s
    real(dp), dimension(MAXTAU, MAXLM, MAXLM) :: coeff

    do jl = 1, MAXL
        if (.not. any(self%inc2(jl, :))) cycle
        do il = 1, MAXL
            if (.not. any(self%inc1(il, :))) cycle

            do jlm = orbital_index(jl), orbital_index(jl+1)-1
                do ilm = orbital_index(il), orbital_index(il+1)-1
                    if (jlm >= ilm) then
                        c1 = calculate_slako_coeff_batch(x, y, z, ilm, jlm)
                    else
                        c1 = calculate_slako_coeff_batch(x, y, z, jlm, ilm)
                    end if

                    coeff(1:MAXTAU, ilm, jlm) = c1(1:MAXTAU)
                end do
            end do
        end do
    end do

    jlmcount = 1
    do jset = 1, self%numset2
        associate(jinc => self%inc2(:, jset))
        associate(jnumlm => self%numlm2(jset))

        ilmcount = 1
        do iset = 1, self%numset1
            associate(iinc => self%inc1(:, iset))
            associate(inumlm => self%numlm1(iset))

            call self%spl_h(iset, jset)%evaluate_batch(r, refint_h)
            call self%spl_s(iset, jset)%evaluate_batch(r, refint_s)

            do jl = 1, MAXL
                if (.not. jinc(jl)) cycle

                do jlm = orbital_index(jl), orbital_index(jl+1)-1
                    do il = 1, MAXL
                        if (.not. iinc(il)) cycle

                        iint = get_integral_start_index(il, jl)
                        ntau = min(il, jl)

                        tmp_h(1:ntau) = refint_h(iint:iint+ntau-1)
                        tmp_h(ntau+1:MAXTAU) = 0._dp

                        tmp_s(1:ntau) = refint_s(iint:iint+ntau-1)
                        tmp_s(ntau+1:MAXTAU) = 0._dp

                        do ilm = orbital_index(il), orbital_index(il+1)-1
                            h(ilmcount, jlmcount) = &
                                sum(coeff(:, ilm, jlm) * tmp_h)

                            s(ilmcount, jlmcount) = &
                                sum(coeff(:, ilm, jlm) * tmp_s)

                            ilmcount = ilmcount + 1
                        end do
                    end do

                    ilmcount = ilmcount - inumlm
                    jlmcount = jlmcount + 1
                end do
            end do

            ilmcount = ilmcount + inumlm
            jlmcount = jlmcount - jnumlm
            end associate
            end associate
        end do

        jlmcount = jlmcount + jnumlm
        end associate
        end associate
    end do
end subroutine evaluate_offsite


subroutine evaluate_offsite_derivs(self, x, y, z, r, dh, ds)
    !! Returns the off-site, two-center Hamiltonian and overlap matrix element
    !! derivatives for all included orbitals.
    class(Onsite1cOffsite2c_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :, :), intent(out) :: dh
        !! Hamiltonian integral derivatives matrix
    real(dp), dimension(:, :, :), intent(out) :: ds
        !! overlap integral derivatives matrix

    integer :: il, jl, ilm, jlm, ilmcount, jlmcount
    integer :: ider, iint, ntau, iset, jset
    real(dp), dimension(3) :: dr
    real(dp), dimension(3, 3) :: dxyz
    real(dp), dimension(MAXSK) :: refint_h, refint_s, drefint_h, drefint_s
    real(dp), dimension(MAXTAU+1) :: c1
    real(dp), dimension(MAXTAU+1, 3) :: dc1
    real(dp), dimension(MAXTAU) :: dcoeffdx, tmp_h, tmp_s, dtmp_h, dtmp_s
    real(dp), dimension(MAXTAU, MAXLM, MAXLM) :: coeff
    real(dp), dimension(MAXTAU, 3, MAXLM, MAXLM) :: dcoeff

    do jl = 1, MAXL
        if (.not. any(self%inc2(jl, :))) cycle
        do il = 1, MAXL
            if (.not. any(self%inc1(il, :))) cycle

            do jlm = orbital_index(jl), orbital_index(jl+1)-1
                do ilm = orbital_index(il), orbital_index(il+1)-1
                    if (jlm >= ilm) then
                        call calculate_slako_coeff_deriv_batch(x, y, z, ilm, &
                                                               jlm, c1, dc1)
                    else
                        call calculate_slako_coeff_deriv_batch(x, y, z, jlm, &
                                                               ilm, c1, dc1)
                    end if

                    coeff(1:MAXTAU, ilm, jlm) = c1(1:MAXTAU)
                    dcoeff(1:MAXTAU, :, ilm, jlm) = dc1(1:MAXTAU, :)
                end do
            end do
        end do
    end do

    dr = [-x, -y, -z]
    dxyz(:, 1) = [(x**2 - 1._dp) / r, x * y / r, x * z / r]
    dxyz(:, 2) = [x * y / r, (y**2 - 1._dp) / r, y * z / r]
    dxyz(:, 3) = [x * z / r, y * z / r, (z**2 - 1._dp) / r]

    jlmcount = 1
    do jset = 1, self%numset2
        associate(jinc => self%inc2(:, jset))
        associate(jnumlm => self%numlm2(jset))

        ilmcount = 1
        do iset = 1, self%numset1
            associate(iinc => self%inc1(:, iset))
            associate(inumlm => self%numlm1(iset))

            call self%spl_h(iset, jset)%evaluate_batch(r, refint_h, drefint_h)
            call self%spl_s(iset, jset)%evaluate_batch(r, refint_s, drefint_s)

            do jl = 1, MAXL
                if (.not. jinc(jl)) cycle

                do jlm = orbital_index(jl), orbital_index(jl+1)-1
                    do il = 1, MAXL
                        if (.not. iinc(il)) cycle

                        iint = get_integral_start_index(il, jl)
                        ntau = min(il, jl)

                        tmp_h(1:ntau) = refint_h(iint:iint+ntau-1)
                        tmp_h(ntau+1:MAXTAU) = 0._dp
                        dtmp_h(1:ntau) = drefint_h(iint:iint+ntau-1)
                        dtmp_h(ntau+1:MAXTAU) = 0._dp

                        tmp_s(1:ntau) = refint_s(iint:iint+ntau-1)
                        tmp_s(ntau+1:MAXTAU) = 0._dp
                        dtmp_s(1:ntau) = drefint_s(iint:iint+ntau-1)
                        dtmp_s(ntau+1:MAXTAU) = 0._dp

                        do ilm = orbital_index(il), orbital_index(il+1)-1
                            do ider = 1, 3
                                dcoeffdx = matmul(dcoeff(:, :, ilm, jlm), &
                                                  dxyz(:, ider))

                                dh(ider, ilmcount, jlmcount) = &
                                    sum(dcoeffdx * tmp_h &
                                      + coeff(:, ilm, jlm) * dtmp_h * dr(ider))

                                ds(ider, ilmcount, jlmcount) = &
                                    sum(dcoeffdx * tmp_s &
                                      + coeff(:, ilm, jlm) * dtmp_s * dr(ider))
                            end do

                            ilmcount = ilmcount + 1
                        end do
                    end do

                    ilmcount = ilmcount - inumlm
                    jlmcount = jlmcount + 1
                end do
            end do

            ilmcount = ilmcount + inumlm
            jlmcount = jlmcount - jnumlm
            end associate
            end associate
        end do

        jlmcount = jlmcount + jnumlm
        end associate
        end associate
    end do
end subroutine evaluate_offsite_derivs


subroutine initialize_from_skf(self, tbpar_dir, isym, jsym, ibasis, jbasis, &
                               overlap_switching_degree, &
                               overlap_switching_length, &
                               veff_switching_degree, veff_switching_length)
    !! Initialize from `.skf` files.
    class(Onsite1cOffsite2c_type), intent(out) :: self
    character(len=*), intent(in) :: tbpar_dir
        !! path to the directory with the parameter files
    character(len=2), intent(in) :: isym
        !! element symbol for the first atomic kind
    character(len=2), intent(in) :: jsym
        !! element symbol for the second atomic kind
    type(BasisAO_type), intent(in) :: ibasis
        !! basis set description for the first atomic kind
    type(BasisAO_type), intent(in) :: jbasis
        !! basis set description for the second atomic kind
    integer, intent(in) :: overlap_switching_degree
        !! degree of the switching function polynomial for the off2c S tails
    real(dp), intent(in) :: overlap_switching_length
        !! length of the interval beyond the cutoff radius
        !! in which the off2c S integral switching function is applied
    integer, intent(in) :: veff_switching_degree
        !! degree of the switching function polynomial for the off2c H tails
    real(dp), intent(in) :: veff_switching_length
        !! length of the interval beyond the cutoff radius
        !! in which the off2c H integral switching function is applied

    character(len=:), allocatable :: filename, isym_set, jsym_set
    integer :: i, numr, numr2, numsk, numsk2, ilm, jlm, iset, jset
    integer, parameter :: ntables = 2
    logical, parameter :: is_onsite = .false.
    logical :: extended_format, extended_format2, has_on1c
    logical :: has_diagonal_properties, has_offdiagonal_properties
    real(dp) :: dr, dr2, rcut
    real(dp), dimension(MAXL) :: occ, eig, hub, offdiagH, offdiagS
    real(dp), dimension(:, :), allocatable :: tables1, tables2

    self%numset1 = ibasis%get_nr_of_zetas()
    self%numset2 = jbasis%get_nr_of_zetas()
    allocate(self%inc1(MAXL, self%numset1))
    allocate(self%inc2(MAXL, self%numset2))
    allocate(self%numlm1(self%numset1))
    allocate(self%numlm2(self%numset2))
    allocate(self%spl_h(self%numset1, self%numset2))
    allocate(self%spl_s(self%numset1, self%numset2))
    self%rcut = 0._dp

    has_on1c = isym == jsym
    if (has_on1c) then
        allocate(self%occ(MAXL, self%numset1))
        allocate(self%eig(MAXL, self%numset1))
        allocate(self%hub(MAXL, self%numset1))
        allocate(self%offdiagH(MAXL, self%numset1, self%numset2))
        allocate(self%offdiagS(MAXL, self%numset1, self%numset2))

        i = ibasis%get_nr_of_orbitals()
        allocate(self%onsiteH(i, i))
        allocate(self%onsiteS(i, i))
    end if

    ilm = 1
    do iset = 1, self%numset1
        isym_set = trim(isym) // repeat('+', iset-1)
        self%inc1(:, iset) = ibasis%get_subset_included_subshells(iset)
        self%numlm1(iset) = ibasis%get_subset_nr_of_orbitals(iset)
        associate(iinc => self%inc1(:, iset))
        associate(inumlm => self%numlm1(iset))

        jlm = 1
        do jset = 1, self%numset2
            jsym_set = trim(jsym) // repeat('+', jset-1)
            self%inc2(:, jset) = jbasis%get_subset_included_subshells(jset)
            self%numlm2(jset) = jbasis%get_subset_nr_of_orbitals(jset)
            associate(jinc => self%inc2(:, jset))
            associate(jnumlm => self%numlm2(jset))

            filename = trim(tbpar_dir) // '/'  // isym_set // '-' // &
                       jsym_set // '.skf'

            if (is_master()) print *, 'Reading SK table from ', trim(filename)

            has_diagonal_properties = has_on1c .and. (iset == jset)
            has_offdiagonal_properties = has_on1c .and. (iset /= jset)

            call read_skf_file(filename, ntables, has_diagonal_properties, &
                               has_offdiagonal_properties, extended_format, &
                               occ, eig, hub, offdiagH, offdiagS, tables1, dr, &
                               numr, numsk)

            if (has_on1c) then
                ! On-site, one-center
                if (has_diagonal_properties) then
                    self%occ(:, iset) = occ
                    self%hub(:, iset) = hub
                    self%eig(:, iset) = eig
                    call set_diagonals(ilm, inumlm, iset, iinc)
                elseif (has_offdiagonal_properties) then
                    self%offdiagH(:, iset, jset) = offdiagH
                    self%offdiagS(:, iset, jset) = offdiagS
                    call set_offdiagonals(ilm, jlm, inumlm, jnumlm, iset, jset,&
                                          iinc, jinc)
                end if

                jlm = jlm + jnumlm
            end if

            ! Off-site, two-center
            rcut = numr * dr

            if (.not. ((isym == jsym) .and. (iset == jset))) then
                ! Additional integrals needed from the "other" file
                filename = trim(tbpar_dir) // '/'  // jsym_set // '-' // &
                            isym_set // '.skf'

                if (is_master()) then
                    print *, 'Reading SK table from ', trim(filename)
                end if

                call read_skf_file(filename, ntables, has_diagonal_properties, &
                                has_offdiagonal_properties, extended_format2, &
                                occ, eig, hub, offdiagH, offdiagS, tables2, &
                                dr2, numr2, numsk2)

                call assert(extended_format .eqv. extended_format2, 'Format '//&
                            'must be the same for Slater-Koster file pairs')
                call assert(abs(dr - dr2) < epsilon(1._dp), 'Grid spacing ' // &
                            'must be the same for Slater-Koster file pairs')
                call assert(numr == numr2, 'Number of grid points ' // &
                            'must be the same for Slater-Koster file pairs')
                call assert(numsk == numsk2, 'Number of integrals ' // &
                            'must be the same for Slater-Koster file pairs')
            else
                allocate(tables2, mold=tables1)
                tables2 = tables1
            end if

            call self%spl_h(iset, jset)%initialize_batch(dr, dr, rcut, &
                                        tables1(1:numsk, :), &
                                        tables2(1:numsk, :), &
                                        iinc, jinc, extended_format, &
                                        is_onsite, veff_switching_degree, &
                                        veff_switching_length)

            self%rcut = max(self%rcut, self%spl_h(iset, jset)%rcut)

            call self%spl_s(iset, jset)%initialize_batch(dr, dr, rcut, &
                                        tables1(numsk+1:, :), &
                                        tables2(numsk+1:, :), &
                                        iinc, jinc, extended_format, &
                                        is_onsite, overlap_switching_degree, &
                                        overlap_switching_length)
            deallocate(tables1)
            deallocate(tables2)

            end associate
            end associate
        end do

        ilm = ilm + inumlm
        end associate
        end associate
    end do


    contains


    subroutine set_diagonals(ilm, inumlm, iset, iinc)
        integer, intent(in) :: ilm
        integer, intent(in) :: inumlm
        integer, intent(in) :: iset
        logical, dimension(MAXL), intent(in) :: iinc

        integer :: i, il, ilm_
        real(dp), dimension(inumlm, inumlm) :: h, s

        h = 0._dp
        s = 0._dp

        i = 1
        do il = 1, MAXL
            if (.not. iinc(il)) cycle

            do ilm_ = orbital_index(il), orbital_index(il+1)-1
                h(i, i) = self%eig(il, iset)
                s(i, i) = 1._dp
                i = i + 1
            end do
        end do

        self%onsiteH(ilm:ilm+inumlm-1, ilm:ilm+inumlm-1) = h
        self%onsiteS(ilm:ilm+inumlm-1, ilm:ilm+inumlm-1) = s
    end subroutine set_diagonals

    subroutine set_offdiagonals(ilm, jlm, inumlm, jnumlm, iset, jset, iinc, &
                                jinc)
        integer, intent(in) :: ilm, jlm
        integer, intent(in) :: inumlm, jnumlm
        integer, intent(in) :: iset, jset
        logical, dimension(MAXL), intent(in) :: iinc, jinc

        integer :: i, j, il, jl, ilm_, jlm_
        real(dp), dimension(inumlm, jnumlm) :: h, s

        h = 0._dp
        s = 0._dp

        i = 1
        do il = 1, MAXL
            if (.not. iinc(il)) cycle

            do ilm_ = orbital_index(il), orbital_index(il+1)-1

                j = 1
                do jl = 1, MAXL
                    if (.not. jinc(jl)) cycle

                    do jlm_ = orbital_index(jl), orbital_index(jl+1)-1
                        if (ilm_ == jlm_) then
                            h(i, j) = self%offdiagH(il, iset, jset)
                            s(i, j) = self%offdiagS(il, iset, jset)
                        end if

                        j = j + 1
                    end do
                end do

                i = i + 1
            end do
        end do

        self%onsiteH(ilm:ilm+inumlm-1, jlm:jlm+jnumlm-1) = h
        self%onsiteS(ilm:ilm+inumlm-1, jlm:jlm+jnumlm-1) = s
    end subroutine set_offdiagonals

end subroutine initialize_from_skf


subroutine initialize_batch(self, r0, dr, rcut, table1, table2, iinc, jinc, &
                            extended_format, is_onsite, switching_degree, &
                            switching_length)
    !! Initializes a batch spline interpolator for a basis subset pair.
    class(TwocenterBatchSpline_type), intent(out) :: self
    real(dp), intent(in) :: r0
        !! first grid point
    real(dp), intent(in) :: dr
        !! grid spacing
    real(dp), intent(in) :: rcut
        !! grid cutoff
    real(dp), dimension(:, :), intent(in) :: table1
        !! (numsk, numr) integral table for the main pair
    real(dp), dimension(:, :), intent(in) :: table2
        !! (numsk, numr) integral table for the other pair
    logical, dimension(MAXL), intent(in) :: iinc
        !! whether an angular momentum is present in the first basis subset
    logical, dimension(MAXL), intent(in) :: jinc
        !! whether an angular momentum is present in the second basis subset
    logical, intent(in) :: extended_format
        !! whether the table is in 'extended' format (up to f) or not (up to d)
    logical, intent(in) :: is_onsite
        !! whether the table contains on-site or off-site two-center integrals
    integer, intent(in) :: switching_degree
        !! degree of the switching function polynomial for the tails
    real(dp), intent(in) :: switching_length
        !! length of the interval beyond the cutoff radius
        !! in which the switching function is applied

    integer :: i, iint, il, jl, nswitch, numr, numsk, tau
    character(len=3) :: sk_label
    character(len=3), dimension(:), allocatable :: sk_labels_skf
    real(dp), dimension(:), allocatable :: tmp_in, tmp_out

    numsk = size(table1, dim=1)
    numr = size(table1, dim=2)

    nswitch = get_nr_of_switching_points(dr, switching_length)
    allocate(tmp_in(numr))
    allocate(tmp_out(numr+nswitch))

    self%dr = dr
    self%r0 = r0
    self%maxsk = MAXSK
    self%numr = numr + nswitch
    self%rcut = rcut + nswitch * dr

    allocate(self%mask(self%maxsk))
    iint = 1
    do il = 1, MAXL
        do jl = 1, MAXL
            do tau = 1, min(il, jl)
                self%mask(iint) = iinc(il) .and. jinc(jl)
                iint = iint + 1
            end do
        end do
    end do
    self%numint = count(self%mask)

    ! Build the integral tables
    allocate(self%table(self%numint, self%numr))

    allocate(sk_labels_skf(numsk))
    if (extended_format) then
        sk_labels_skf = ['ffs', 'ffp', 'ffd', 'fff', 'dfs', 'dfp', 'dfd', &
                         'dds', 'ddp', 'ddd', 'pfs', 'pfp', 'pds', 'pdp', &
                         'pps', 'ppp', 'sfs', 'sds', 'sps', 'sss']
    else
        sk_labels_skf = ['dds', 'ddp', 'ddd', 'pds', 'pdp', 'pps', 'ppp', &
                         'sds', 'sps', 'sss']
    end if

    iint = 1
    do il = 1, MAXL
        do jl = 1, MAXL
            do tau = 1, min(il, jl)
                if (iinc(il) .and. jinc(jl)) then
                    if (il <= jl) then
                        sk_label = l_labels(il) // l_labels(jl) // l_labels(tau)
                    else
                        sk_label = l_labels(jl) // l_labels(il) // l_labels(tau)
                    end if

                    i = find_string_index(sk_labels_skf, sk_label)
                    call assert(i > 0, 'Internal error: ' // sk_label)

                    if (il < jl) then
                        tmp_in = table1(i, :)
                    elseif (il == jl) then
                        ! Symmetrize
                        tmp_in = 0.5_dp * (table1(i, :) + table2(i, :))
                    else
                        if (is_onsite) then
                            tmp_in = table2(i, :)
                        else
                            tmp_in = table2(i, :) * (-1)**(il + jl - 2)
                        end if
                    end if

                    call add_switching_points(dr, nswitch, switching_degree, &
                                              tmp_in, tmp_out)
                    self%table(iint, :) = tmp_out

                    iint = iint + 1
                end if
            end do
        end do
    end do
end subroutine initialize_batch


pure function get_nr_of_switching_points(dr, switching_length) result(nswitch)
    !! Returns the number of switching points for the giving length and spacing.
    real(dp), intent(in) :: dr
        !! grid spacing
    real(dp), intent(in) :: switching_length
        !! length of switching interval
    integer :: nswitch

    nswitch = int(ceiling(switching_length / dr))
end function get_nr_of_switching_points


subroutine add_switching_points(dr, nswitch, switching_degree, array_in, &
                                array_out)
    !! Returns the given array with extra points beyond the cutoff radius
    !! to ensure the tail smoothly approaches zero.
    real(dp), intent(in) :: dr
        !! grid spacing
    integer, intent(in) :: nswitch
        !! number of extra grid points
    integer, intent(in) :: switching_degree
        !! degree of the switching function polynomial for tail smoothening
    real(dp), dimension(:), intent(in) :: array_in
        !! input on the grid
    real(dp), dimension(:), intent(out) :: array_out
        !! integrals on the extended grid

    integer :: numr
    type(PolynomialFunction_type) :: switching_function

    call assert(switching_degree == 3 .or. switching_degree == 5, &
                'Degree of the switching function polynomial should be 3 or 5')

    numr = size(array_in)
    call assert(size(array_out) == (numr + nswitch), &
                'Wrong size for output array')

    array_out(:numr) = array_in
    array_out(numr+1:) = fit_tail(array_in(numr-2:numr), switching_degree)


    contains


    function fit_tail(arr, degree) result(tail)
        ! Generates a smooth tail using a polynomial switching function
        ! which matches derivatives 0, 1 qnd 2 at the last grid point and
        ! where both derivatives are 0 at the end of the switching interval.
        real(dp), dimension(3), intent(in) :: arr
        integer, intent(in) :: degree
        real(dp), dimension(nswitch) :: tail

        integer :: i
        real(dp) :: fder1, fder2, x

        ! Initialize the switching function
        switching_function%rcut = 1._dp
        switching_function%powers = [(i, i = 0, degree)]
        switching_function%coeff = [(0._dp, i = 0, degree)]

        ! Estimate first and second derivatives at the last grid point
        ! using the last 3 function values
        ! NOTE: a 5-point stencil seems to yield similar estimates
        fder2 = (arr(1) - 2*arr(2) + arr(3)) / dr**2
        fder1 = (arr(3) - arr(1)) / (2*dr) + fder2 * dr

        ! Set the switching function coefficients
        switching_function%coeff(1) = arr(3)
        switching_function%coeff(2) = fder1 * nswitch * dr
        if (degree == 3) then
            switching_function%coeff(3) = -3*switching_function%coeff(1) &
                                          - 2*switching_function%coeff(2)
            switching_function%coeff(4) = 2*switching_function%coeff(1) &
                                          + switching_function%coeff(2)
        elseif (degree == 5) then
            switching_function%coeff(3) = 0.5_dp * fder2 * (nswitch * dr)**2
            switching_function%coeff(4) = -10*switching_function%coeff(1) &
                                          - 6*switching_function%coeff(2) &
                                          - 3*switching_function%coeff(3)
            switching_function%coeff(5) = 15*switching_function%coeff(1) &
                                          + 8*switching_function%coeff(2) &
                                          + 3*switching_function%coeff(3)
            switching_function%coeff(6) = -6*switching_function%coeff(1) &
                                          - 3*switching_function%coeff(2) &
                                          - 1*switching_function%coeff(3)
        end if

        ! Apply the switching function
        do i = 1, nswitch - 1
            x = i * 1._dp / nswitch
            tail(i) = switching_function%evaluate(1._dp - x, 0)
        end do

        tail(nswitch) = 0._dp
    end function fit_tail
end subroutine add_switching_points


elemental function get_integral_start_index(il, jl) result(iint)
    !! Given two subshell indices returns the start index of the
    !! corresponding Slater-Koster integrals (i.e. for tau=1).
    integer, intent(in) :: il
        !! the first subshell index
    integer, intent(in) :: jl
        !! the second subshell index
    integer :: iint

    iint = 0

    if (il == 1) then
        iint = jl  ! s**

    elseif (il == 2) then
        if (jl == 1) then
            iint = 5  ! ps*
        elseif (jl == 2) then
            iint = 6  ! pp*
        elseif (jl == 3) then
            iint = 8  ! pd*
        elseif (jl == 4) then
            iint = 10 ! pf*
        end if

    elseif (il == 3) then
        if (jl == 1) then
            iint = 12  ! ds*
        elseif (jl == 2) then
            iint = 13  ! dp*
        elseif (jl == 3) then
            iint = 15  ! dd*
        elseif (jl == 4) then
            iint = 18  ! df*
        end if

    elseif (il == 4) then
        if (jl == 1) then
            iint = 21  ! fs*
        elseif (jl == 2) then
            iint = 22  ! fp*
        elseif (jl == 3) then
            iint = 24  ! fd*
        elseif (jl == 4) then
            iint = 27  ! ff*
        end if
    end if
end function get_integral_start_index


subroutine read_skf_file(filename, ntables, has_diagonal_properties, &
                         has_offdiagonal_properties, extended_format, &
                         occ, eig, hub, offdiagH, offdiagS, tables, dr, numr, &
                         numsk)
    !! Returns the contents of the given `.skf` file.
    character(len=*), intent(in) :: filename
        !! path to the `.skf` file
    integer, intent(in) :: ntables
        !! number of different integral tables in the file
        !! (e.g. 2 for both H and S)
    logical, intent(in) :: has_diagonal_properties
        !! whether the file contains "diagonal" one-center properties
    logical, intent(in) :: has_offdiagonal_properties
        !! whether the file contains "off-diagonal" one-center properties
    logical, intent(out) :: extended_format
        !! whether the file is in 'extended' format (up to f) or not (up to d)
    real(dp), dimension(MAXL), intent(out) :: occ
        !! atomic occupation numbers
    real(dp), dimension(MAXL), intent(out) :: eig
        !! on-site eigenvalues
    real(dp), dimension(MAXL), intent(out) :: hub
        !! on-site Hubbard values
    real(dp), dimension(MAXL), intent(out) :: offdiagH
        !! on-site off-diagonal Hamiltonian matrix elements
    real(dp), dimension(MAXL), intent(out) :: offdiagS
        !! on-site off-diagonal overlap matrix elements
    real(dp), dimension(:, :), allocatable, intent(out) :: tables
        !! Slater-Koster integral tables
    real(dp), intent(out) :: dr
        !! grid spacing (and first grid point)
    integer, intent(out) :: numr
        !! number of grid points for every table
    integer, intent(out) :: numsk
        !! number of Slater-Koster integrals for every table

    integer :: fhandle

    open(newunit=fhandle, file=filename, status='old', action='read')

    call read_skf_header(fhandle, has_diagonal_properties, &
                         has_offdiagonal_properties, extended_format, occ, &
                         eig, hub, offdiagH, offdiagS, dr, numr, numsk)

    allocate(tables(ntables*numsk, numr))
    call read_skf_twocenter_tables(fhandle, numr, tables)

    close(fhandle)
end subroutine read_skf_file


subroutine read_skf_header(fhandle, has_diagonal_properties, &
                           has_offdiagonal_properties, extended_format, &
                           occ, eig, hub, offdiagH, offdiagS, dr, numr, numsk)
    !! Returns the contents of the header of the given `.skf` file
    !! (the first few lines before the two-center tables).
    integer, intent(in) :: fhandle
        !! open file handle pointing to the beginning of the `.skf` file
    logical, intent(in) :: has_diagonal_properties
        !! whether the file contains "diagonal" one-center properties
    logical, intent(in) :: has_offdiagonal_properties
        !! whether the file contains "off-diagonal" one-center properties
    logical, intent(out) :: extended_format
        !! whether the file is in 'extended' format (up to f) or not (up to d)
    real(dp), dimension(MAXL), intent(out) :: occ
        !! atomic occupation numbers
    real(dp), dimension(MAXL), intent(out) :: eig
        !! on-site eigenvalues
    real(dp), dimension(MAXL), intent(out) :: hub
        !! on-site Hubbard values
    real(dp), dimension(MAXL), intent(out) :: offdiagH
        !! on-site off-diagonal Hamiltonian matrix elements
    real(dp), dimension(MAXL), intent(out) :: offdiagS
        !! on-site off-diagonal overlap matrix elements
    real(dp), intent(out) :: dr
        !! grid spacing (and first grid point)
    integer, intent(out) :: numr
        !! number of grid points for every table
    integer, intent(out) :: numsk
        !! number of Slater-Koster integrals for every table

    integer :: i, line_number, n, n_expected, nlines, numl
    integer, parameter :: MAXSK_skf = 20
    character(len=1024) :: buffer, msg
    real(dp) :: tmp

    extended_format = .false.
    numl = MAXL - 1
    numsk = MAXSK_skf - 10

    if (has_diagonal_properties .or. has_offdiagonal_properties) then
        nlines = 3
    else
        nlines = 2
    end if

    line_number = 1
    do while (line_number <= nlines)
        read(fhandle, '(a)') buffer

        ! Skip comment lines that start with #
        i = index(buffer, '#')
        if (i == 1) then
            cycle
        end if

        if (line_number == 1 .and. buffer(1:1) == '@') then
            extended_format = .true.
            numl = MAXL
            numsk = MAXSK_skf
            cycle
        end if

        call replace_tabs_by_whitespace(buffer)

        if (line_number == 1) then
            read(buffer, *) dr, numr

        elseif (line_number == 2) then
            if (has_diagonal_properties) then
                n = count_items(buffer)
                n_expected = 3*numl + 1

                if (n /= n_expected) then
                    write(msg, '("Number of items in second line is ", &
                          &(i0), ", expected ", (i0), ".")') n, n_expected
                    call assert(.false., trim(msg))
                end if

                read(buffer, *) eig(numl:1:-1), tmp, hub(numl:1:-1), &
                                occ(numl:1:-1)

                if (.not. extended_format) then
                    eig(MAXL) = 0._dp
                    hub(MAXL) = 0._dp
                    occ(MAXL) = 0._dp
                end if
            elseif (has_offdiagonal_properties) then
                n = count_items(buffer)
                n_expected = 2*numl

                if (n /= n_expected) then
                    write(msg, '("Number of items in second line is ", &
                          &(i0), ", expected ", (i0), ".")') n, n_expected
                    call assert(.false., trim(msg))
                end if

                read(buffer, *) offdiagH(numl:1:-1), offdiagS(numl:1:-1)

                if (.not. extended_format) then
                    offdiagH(MAXL) = 0._dp
                    offdiagS(MAXL) = 0._dp
                end if
            else
                ! Unused line containing e.g. the atomic mass
                continue
            end if
        elseif (line_number == 3) then
            ! Unused line containing e.g. the atomic mass
            continue
        end if

        line_number = line_number + 1
    end do
end subroutine read_skf_header


subroutine read_skf_twocenter_tables(fhandle, numr, tables)
    !! Returns the contents of the given `.skf` file.
    integer, intent(in) :: fhandle
        !! open file handle pointing to the start of the tables in the file
    integer, intent(in) :: numr
        !! number of grid points for every table
    real(dp), dimension(:, :), intent(out) :: tables
        !! Slater-Koster integral tables

    integer :: i

    do i = 1, numr
        read(fhandle, *) tables(:, i)
    end do
end subroutine read_skf_twocenter_tables

end module tibi_onsite1c_offsite2c
