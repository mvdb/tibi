!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_realspace_pair
!! Module for pairwise interactions summed in real space

use tibi_atoms, only: Atoms_type
use tibi_calculator, only: Calculator_type
use tibi_constants, only: dp
use tibi_neighborlist, only: NeighborList_type
use tibi_utils_prog, only: is_master
implicit none


type, abstract, extends(Calculator_type) :: RealSpacePairCalculator_type
    !! Abstract calculator for pairwise interactions summed in real space
    character(len=128) :: description
        !! description for printing
    class(NeighborList_type), allocatable :: nl
        !! neighbor list
    contains
    procedure :: calculate_energy
    procedure :: calculate_forces
    procedure(evaluate_pair), deferred :: evaluate_pair
end type RealSpacePairCalculator_type


interface
    elemental function evaluate_pair(self, iat, jat, r, der) result(fval)
        !! Evaluates the pairwise interaction.
        import dp, RealSpacePairCalculator_type
        implicit none
        class(RealSpacePairCalculator_type), intent(in) :: self
        integer, intent(in) :: iat, jat
            !! the atomic indices
        real(dp), intent(in) :: r
            !! the interatomic distance
        integer, intent(in) :: der
            !! the derivative index (0 or 1)
        real(dp) :: fval
    end function evaluate_pair
end interface


contains


subroutine calculate_energy(self, atoms)
    !! Calculates the (total) energy for the given [[tibi_atoms:Atoms_type]]
    !! object, which is stored in atoms%energy.
    class(RealSpacePairCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms

    integer :: iat, jat, ik, jk, ineigh, ineighlist
    real(dp) :: r, e

    call self%nl%update(atoms)
    atoms%energy = 0._dp

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)

        do ineigh = 1, self%nl%neighbors(iat)
            ineighlist = self%nl%start_indices(iat) + ineigh - 1

            jat = self%nl%neighbor_indices(ineighlist)
            jk = atoms%kinds(jat)

            r = self%nl%r(ineighlist)
            if (r > self%nl%rcuts(ik, jk)) cycle

            e = self%evaluate_pair(iat, jat, r, 0)

            if (self%nl%both_ways) then
                e = e * 0.5_dp
            end if

            if (iat == jat .and. r > self%nl%rsame) then
                atoms%energy = atoms%energy + 0.5_dp * e
            else
                atoms%energy = atoms%energy + e
            end if
        end do
    end do

    if (is_master() .and. len_trim(self%description) > 0) then
        if (abs(atoms%energy) < 1e40_dp) then
            print '(" ", (a), " energy [Ha] = ", f0.8)', &
                  trim(self%description), atoms%energy
        else
            print '(" ", (a), " energy [Ha] = ", g0.8)', &
                  trim(self%description), atoms%energy
        end if
        print *
    end if
end subroutine calculate_energy


subroutine calculate_forces(self, atoms, do_stress)
    !! Calculates the atomic forces for the given [[tibi_atoms:Atoms_type]]
    !! object, which are stored in atoms%forces.
    class(RealSpacePairCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    logical, intent(in) :: do_stress
        !! whether to also compute the stress tensor for periodic structures

    integer :: i, iat, jat, ik, jk, ineigh, ineighlist
    real(dp) :: r, dedr
    real(dp), dimension(3) :: drdx, dedx

    atoms%forces(:, :) = 0._dp
    if (do_stress) atoms%stress(:, :) = 0._dp

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)

        do ineigh = 1, self%nl%neighbors(iat)
            ineighlist = self%nl%start_indices(iat) + ineigh - 1

            jat = self%nl%neighbor_indices(ineighlist)
            jk = atoms%kinds(jat)

            r = self%nl%r(ineighlist)
            if (r > self%nl%rcuts(ik, jk)) cycle

            dedr = self%evaluate_pair(iat, jat, r, 1)

            if (self%nl%both_ways) then
                dedr = dedr * 0.5_dp
            end if

            ! drdx is just the negative of the normalized i->j vector component
            drdx = -self%nl%vec(:, ineighlist)
            dedx = dedr * drdx

            atoms%forces(:, iat) = atoms%forces(:, iat) - dedx
            atoms%forces(:, jat) = atoms%forces(:, jat) + dedx

            if (do_stress) then
                do i = 1, 3
                    if (iat == jat) then
                        atoms%stress(:, i) = atoms%stress(:, i) &
                            - 0.5_dp * dedx(:) * r * self%nl%vec(i, ineighlist)
                    else
                        atoms%stress(:, i) = atoms%stress(:, i) &
                            - dedx(:) * r * self%nl%vec(i, ineighlist)
                    end if
                end do
            end if
        end do
    end do

    if (do_stress) atoms%stress = atoms%stress / atoms%get_volume()
end subroutine calculate_forces

end module tibi_realspace_pair
