!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_tightbinding) tibi_tightbinding_mulliken
!! Submodule for Mulliken population analysis tools for [[tibi_tightbinding]]
!! (see [Mulliken (1955)](https://doi.org/10.1063/1.1740588))

use tibi_blas, only: ddot, &
                     zdotu
implicit none


contains


module subroutine calculate_mulliken_orbital_pop(self)
    !! Calculates the Mulliken orbital populations and stores them in
    !! self%mulliken_pop.
    !!
    !! Assumes that the band structure has already been obtained through
    !! a preceding call to [[tibi_lcao:solve_band_structure]].
    class(TightBindingCalculator_type), intent(inout) :: self

    integer :: iband, ikpt, ispin

    ! Check if we still need to do this calculation
    if (.not. self%need_mulliken_pop_update) return

    call self%calculate_densmat(energy_weighted=.false.)

    call task_timer%start_timer('tightbinding_mulliken_pop')

    self%mulliken_pop = 0._dp

    do ispin = 1, self%get_nr_of_spins()
        do ikpt = 1, self%get_nr_of_kpoints()
            if (self%complex_wfn) then
                associate(dm => self%densmat_complex)
                associate(overlap => self%overlap_complex)
                do iband = 1, self%nband
                    self%mulliken_pop(iband) = self%mulliken_pop(iband) &
                        + real(zdotu(self%nband-iband, &
                                     dm(iband+1:, iband, ikpt, ispin), 1, &
                                     overlap(iband+1:, iband, ikpt, ispin), 1),&
                               kind=dp)

                    self%mulliken_pop(iband:) = self%mulliken_pop(iband:) &
                        + real(dm(iband:, iband, ikpt, ispin) &
                               * overlap(iband:, iband, ikpt, ispin), &
                               kind=dp)
                end do
                end associate
                end associate
            else
                associate(dm => self%densmat_real)
                associate(overlap => self%overlap_real)
                do iband = 1, self%nband
                    self%mulliken_pop(iband) = self%mulliken_pop(iband) &
                        + ddot(self%nband-iband, &
                               dm(iband+1:, iband, ikpt, ispin), 1, &
                               overlap(iband+1:, iband, ikpt, ispin), 1)

                    self%mulliken_pop(iband:) = self%mulliken_pop(iband:) &
                                        + dm(iband:, iband, ikpt, ispin) &
                                        * overlap(iband:, iband, ikpt, ispin)
                end do
                end associate
                end associate
            end if
        end do
    end do

    self%need_mulliken_pop_update = .false.
    call task_timer%stop_timer('tightbinding_mulliken_pop')
end subroutine calculate_mulliken_orbital_pop


module subroutine calculate_mulliken_orbital_mag(self)
    !! Calculates the Mulliken orbital magnetizations and stores them
    !! in self%mulliken_mag.
    !!
    !! Assumes that the band structure has already been obtained through
    !! a preceding call to [[tibi_lcao:solve_band_structure]].
    class(TightBindingCalculator_type), intent(inout) :: self

    integer :: iband, ikpt, ispin, sign

    ! Check if we still need to do this calculation
    if (.not. self%need_mulliken_mag_update) return

    call self%calculate_densmat(energy_weighted=.false.)

    call task_timer%start_timer('tightbinding_mulliken_mag')

    self%mulliken_mag = 0._dp

    ! Check for quick return if not spin-polarized
    if (.not. self%occupations%spin_polarized) then
        return
    end if

    do ispin = 1, self%get_nr_of_spins()
        do ikpt = 1, self%get_nr_of_kpoints()
            if (ispin == 1) then
                sign = 1
            else
                sign = -1
            end if

            if (self%complex_wfn) then
                associate(dm => self%densmat_complex)
                associate(overlap => self%overlap_complex)
                do iband = 1, self%nband
                    self%mulliken_mag(iband) = self%mulliken_mag(iband) &
                        + sign * real( &
                            zdotu(self%nband-iband, &
                                  dm(iband+1:, iband, ikpt, ispin), 1, &
                                  overlap(iband+1:, iband, ikpt, ispin), 1), &
                            kind=dp)

                    self%mulliken_mag(iband:) = self%mulliken_mag(iband:) &
                        + sign * real( &
                            dm(iband:, iband, ikpt, ispin) &
                            * overlap(iband:, iband, ikpt, ispin), kind=dp)
                end do
                end associate
                end associate
            else
                associate(dm => self%densmat_real)
                associate(overlap => self%overlap_real)
                do iband = 1, self%nband
                    self%mulliken_mag(iband) = self%mulliken_mag(iband) &
                        + sign * ddot(self%nband-iband, &
                                      dm(iband+1:, iband, ikpt, ispin), 1, &
                                      overlap(iband+1:, iband, ikpt, ispin), 1)

                    self%mulliken_mag(iband:) = self%mulliken_mag(iband:) &
                        + sign * dm(iband:, iband, ikpt, ispin) &
                          * overlap(iband:, iband, ikpt, ispin)
                end do
                end associate
                end associate
            end if
        end do
    end do

    self%need_mulliken_mag_update = .false.
    call task_timer%stop_timer('tightbinding_mulliken_mag')
end subroutine calculate_mulliken_orbital_mag


module function calculate_mulliken_orbital_chg(self, atoms) result(charges_lm)
    !! Returns the Mulliken orbital charges.
    !!
    !! Assumes the (Mulliken) populations have been calculated by
    !! calling [[tibi_tightbinding:calculate_mulliken_orbital_pop]].
    class(TightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(self%nband) :: charges_lm

    integer :: iat, ik, istart, iend, numlm
    real(dp), dimension(:), allocatable :: occup

    call task_timer%start_timer('tightbinding_mulliken_chg')

    allocate(occup(self%basis_sets%maxnumlm))

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        istart = self%indices_lm(1, iat)
        iend = self%indices_lm(2, iat)
        numlm = iend - istart + 1

        call self%basis_sets%get_orbital_occupations(ik, occup(:numlm))
        charges_lm(istart:iend) = occup(:numlm) - self%mulliken_pop(istart:iend)
    end do

    call task_timer%stop_timer('tightbinding_mulliken_chg')
end function calculate_mulliken_orbital_chg


module function calculate_mulliken_subshell_chg(self, atoms) result(charges_l)
    !! Returns the Mulliken subshell charges.
    !!
    !! Assumes the (Mulliken) populations have been calculated by
    !! calling [[tibi_tightbinding:calculate_mulliken_orbital_pop]].
    class(TightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(self%nsubshell) :: charges_l

    integer :: iat, ik, istart_l, iend_l, istart_lm, iend_lm, numl
    real(dp), dimension(:), allocatable :: occup

    call task_timer%start_timer('tightbinding_mulliken_chg')

    allocate(occup(self%basis_sets%maxnuml))

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        istart_l = self%indices_l(1, iat)
        iend_l = self%indices_l(2, iat)
        numl = iend_l - istart_l + 1
        istart_lm = self%indices_lm(1, iat)
        iend_lm = self%indices_lm(2, iat)

        call self%basis_sets%get_subshell_properties(ik, occup(:numl), 'occ')
        call self%basis_sets%get_subshell_sums(ik, &
                                 self%mulliken_pop(istart_lm:iend_lm), &
                                 charges_l(istart_l:iend_l))
        charges_l(istart_l:iend_l) = occup(:numl) - charges_l(istart_l:iend_l)
    end do

    call task_timer%stop_timer('tightbinding_mulliken_chg')
end function calculate_mulliken_subshell_chg


module function calculate_mulliken_subshell_mag(self, atoms) result(mag_l)
    !! Returns the Mulliken subshell magnetizations.
    !!
    !! Assumes the (Mulliken) magnetizations have been calculated by
    !! calling [[tibi_tightbinding:calculate_mulliken_orbital_mag]].
    class(TightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(self%nsubshell) :: mag_l

    integer :: iat, ik, istart_l, iend_l, istart_lm, iend_lm, numl
    real(dp), dimension(:), allocatable :: occup

    call task_timer%start_timer('tightbinding_mulliken_chg')

    allocate(occup(self%basis_sets%maxnuml))

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        istart_l = self%indices_l(1, iat)
        iend_l = self%indices_l(2, iat)
        numl = iend_l - istart_l + 1
        istart_lm = self%indices_lm(1, iat)
        iend_lm = self%indices_lm(2, iat)

        call self%basis_sets%get_subshell_sums(ik, &
                                 self%mulliken_mag(istart_lm:iend_lm), &
                                 mag_l(istart_l:iend_l))
    end do

    call task_timer%stop_timer('tightbinding_mulliken_chg')
end function calculate_mulliken_subshell_mag


module subroutine print_mulliken_subshell_chg(self, atoms)
    !! Calculates and prints out the Mulliken charges per atom and subshell.
    class(TightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms

    call print_mulliken_subshell_array(self, atoms, which='charges')
end subroutine print_mulliken_subshell_chg


module subroutine print_mulliken_subshell_mag(self, atoms)
    !! Calculates and prints out the Mulliken magnetizations
    !! per atom and subshell.
    class(TightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms

    call print_mulliken_subshell_array(self, atoms, which='magnetizations')
end subroutine print_mulliken_subshell_mag


subroutine print_mulliken_subshell_array(self, atoms, which)
    !! Calculates and prints out the Mulliken charges or magnetizations
    !! per atom and subshell.
    type(TightBindingCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    character(len=*), intent(in) :: which
        !! 'charges' or 'magnetizations'

    integer :: iat, istart, iend
    real(dp), dimension(self%nsubshell) :: array_l
    character(len=:), allocatable :: unit

    select case(which)
    case('charges')
        array_l = self%calculate_mulliken_subshell_chg(atoms)
        unit = '[|e|]'
    case('magnetizations')
        array_l = self%calculate_mulliken_subshell_mag(atoms)
        unit = '[-|e|]'
    end select

    print '(" ", a, 1x, a, " on the ", i0, " atoms:")', &
          "Mulliken subshell and atom " // which, unit, atoms%natom

    do iat = 1, atoms%natom
        istart = self%indices_l(1, iat)
        iend = self%indices_l(2, iat)
        write(*, '("   Atom ", (i0.4), 1x, (a), " =")', advance='no') iat, which
        write(*, '(*(f8.4))', advance='no') array_l(istart:iend)
        write(*, '("  total: ", (f8.4))') sum(array_l(istart:iend))
    end do
    print *
end subroutine print_mulliken_subshell_array


module subroutine write_mulliken_orbital_mag(self)
    !! Writes the Mulliken orbital magnetizations to a
    !! `mulliken_orbital_mag.dat` file.
    class(TightBindingCalculator_type), intent(in) :: self

    call write_matrix(self, which='magnetizations')
end subroutine write_mulliken_orbital_mag


module subroutine write_mulliken_orbital_pop(self)
    !! Writes the Mulliken orbital populations to a
    !! `mulliken_orbital_pop.dat` file.
    class(TightBindingCalculator_type), intent(in) :: self

    call write_matrix(self, which='populations')
end subroutine write_mulliken_orbital_pop


subroutine write_matrix(self, which)
    !! Writes the Mulliken orbital populations or magnetizations to file.
    type(TightBindingCalculator_type), intent(in) :: self
    character(len=*), intent(in) :: which
        !! 'populations' or 'magnetizations'

    integer :: fhandle, iat, iend, istart
    character(len=24) :: filename

    select case(which)
    case('magnetizations')
        filename = 'mulliken_orbital_mag.dat'
    case('populations')
        filename = 'mulliken_orbital_pop.dat'
    end select

    open(newunit=fhandle, file=filename, status='replace', action='write')
    write(fhandle, '(a)') '# Mulliken orbital ' // which

    do iat = 1, size(self%indices_lm, dim=2)
        istart = self%indices_lm(1, iat)
        iend = self%indices_lm(2, iat)

        select case(which)
        case('populations')
            write(fhandle, '((i0), *(es14.6))') iat, &
                                                self%mulliken_pop(istart:iend)
        case('magnetizations')
            write(fhandle, '((i0), *(es14.6))') iat, &
                                                self%mulliken_mag(istart:iend)
        end select
    end do

    close(fhandle)
end subroutine write_matrix

end submodule tibi_tightbinding_mulliken
