!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_coulomb
!! Module for Coulomb interactions

use tibi_atoms, only: Atoms_type
use tibi_calculator, only: Calculator_type
use tibi_constants, only: angstrom, &
                          dp, &
                          electronvolt
use tibi_coulomb_realspace, only: RealSpaceCoulombCalculator_type
use tibi_coulomb_realspace_monopole, only: RealSpaceMonopoleCalculator_type
use tibi_coulomb_realspace_multipole, only: RealSpaceMultipoleCalculator_type
use tibi_coulomb_reciprocal, only: ReciprocalCoulombCalculator_type
use tibi_coulomb_reciprocal_monopole, only: ReciprocalMonopoleCalculator_type
use tibi_coulomb_reciprocal_multipole, only: ReciprocalMultipoleCalculator_type
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_timer, only: task_timer
use tibi_utils_prog, only: assert
implicit none


type, extends(Calculator_type) :: CoulombCalculator_type
    !! Calculator for evaluating Coulomb interactions, in real space
    !! as well as reciprocal space (Ewald summation for periodic structures).
    real(dp) :: ewald_alpha = 0._dp
        !! Ewald screening parameter (zero means no screening)
        !! (see [Ewald (1921)](https://doi.org/10.1002/andp.19213690304))
    real(dp) :: ewald_tolerance = 0._dp
        !! Parameter controlling the accuracy of the electrostatics
        !! calculations. For the real space contributions, it defines a cutoff
        !! distance, beyond which a pair of unit charges will contribute less
        !! than the specified tolerance to the electrostatic energy. For the
        !! reciprocal space contributions, it defines an analogous cutoff
        !! distance for the set of reciprocal lattice vectors.
    logical :: include_reciprocal = .false.
        !! whether to include reciprocal space contributions
    class(RealSpaceCoulombCalculator_type), allocatable :: &
                                            realspacecoulomb_calc
        !! calculator for Coulomb interactions in real-space
    class(ReciprocalCoulombCalculator_type), allocatable :: &
                                             recipcoulomb_calc
        !! calculator for Coulomb interactions in reciprocal space
        !! (only used if include_reciprocal=.true.)
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: print_description
    procedure :: calculate_energy
    procedure :: calculate_forces
    procedure :: get_local_potentials
    procedure :: update_moments
    procedure :: update_geometry
end type CoulombCalculator_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(CoulombCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('ewald_alpha')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%ewald_alpha
        self%ewald_alpha = self%ewald_alpha / angstrom
        call assert(self%ewald_alpha >= 0._dp, 'ewald_alpha must be >= 0')
    end if

    buffer = input%fetch_keyword('ewald_tolerance')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%ewald_tolerance
        self%ewald_tolerance = self%ewald_tolerance * electronvolt
        call assert(self%ewald_tolerance >= 0._dp, &
                    'ewald_tolerance must be >= 0')
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input, atoms, nchg, indices, distribution, &
                      use_multipoles, ilmax, parameters)
    !! Initializes all allocatable attributes; this should normally
    !! only be done once, at the very beginning of a (series of)
    !! calculations on a given atomic structure.
    class(CoulombCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms
    integer, intent(in) :: nchg
        !! total number of charges
        !! (only relevant in the case of monopoles-only case)
    integer, dimension(2, atoms%natom), intent(in) :: indices
        !! start and end indices of the charges on each atom
    character(len=*), intent(in) :: distribution
        !! functional form of the charge distributions
    logical, intent(in) :: use_multipoles
        !! whether fluctuations include multipoles or only monopoles
    integer, dimension(:), intent(in), optional :: ilmax
        !! maximum angular momentum indices in multipole expansions
        !! (one for each atomic kind)
    real(dp), dimension(:, :), intent(in), optional :: parameters
        !! parameters related to the charge distributions for each atomic kind
        !! (required for charge distributions other than 'delta')

    self%include_reciprocal = atoms%is_periodic
    call self%fetch_input_keywords(input)

    ! Set default tolerance (if not defined via input)
    if (self%ewald_tolerance < epsilon(1._dp)) then
        if (self%include_reciprocal) then
            self%ewald_tolerance = 1e-6_dp * electronvolt
        else
            self%ewald_tolerance = 1e-9_dp * electronvolt
        end if
    end if

    call task_timer%add_timer('coulomb_energy_realspace')
    call task_timer%add_timer('coulomb_forces_realspace')
    call task_timer%add_timer('coulomb_geometry_realspace')
    call task_timer%add_timer('coulomb_potential_realspace')

    if (use_multipoles) then
        call assert(present(ilmax), 'Need ilmax argument for multipoles')
        allocate(RealSpaceMultipoleCalculator_type :: &
                 self%realspacecoulomb_calc)
    else
        allocate(RealSpaceMonopoleCalculator_type :: &
                 self%realspacecoulomb_calc)
    end if

    select type(realcalc => self%realspacecoulomb_calc)
    type is (RealSpaceMonopoleCalculator_type)
        call realcalc%initialize(atoms, nchg, indices, distribution, &
                                 parameters, self%ewald_alpha, &
                                 self%ewald_tolerance)
    type is (RealSpaceMultipoleCalculator_type)
        call realcalc%initialize(atoms, indices, distribution, &
                                 self%ewald_alpha, self%ewald_tolerance, ilmax)
    end select

    self%ewald_alpha = self%realspacecoulomb_calc%ewald_alpha

    if (self%include_reciprocal) then
        call task_timer%add_timer('coulomb_energy_reciprocal')
        call task_timer%add_timer('coulomb_forces_reciprocal')
        call task_timer%add_timer('coulomb_geometry_reciprocal')
        call task_timer%add_timer('coulomb_potential_reciprocal')

        if (use_multipoles) then
            call assert(present(ilmax), 'Need ilmax argument for multipoles')
            allocate(ReciprocalMultipoleCalculator_type :: &
                     self%recipcoulomb_calc)
        else
            allocate(ReciprocalMonopoleCalculator_type :: &
                     self%recipcoulomb_calc)
        end if

        select type(recipcalc => self%recipcoulomb_calc)
        type is (ReciprocalMonopoleCalculator_type)
            call recipcalc%initialize(atoms, indices, self%ewald_alpha, &
                                      self%ewald_tolerance)
        type is (ReciprocalMultipoleCalculator_type)
            call recipcalc%initialize(atoms, indices, self%ewald_alpha, &
                                      self%ewald_tolerance, ilmax)
        end select
    end if
end subroutine initialize


subroutine print_description(self)
    !! Prints a description with the more important characteristics.
    class(CoulombCalculator_type), intent(in) :: self

    integer :: ik, jk

    print '(a)', ' Coulomb calculator description:'

    print '("   Charge distribution function:            ", (a))', &
          trim(self%realspacecoulomb_calc%distribution)

    print '("   Ewald alpha parameter [a0^-1]: ", (f8.4))', &
          self%realspacecoulomb_calc%ewald_alpha

    if (self%include_reciprocal) then
        print '("   Number of reciprocal lattice vectors:    ", (i0))', &
              size(self%recipcoulomb_calc%gvectors(:, :), dim=2)
    end if

    print '(a)', '   Real space neighborlist cutoffs [a0]:'
    do ik = 1, size(self%realspacecoulomb_calc%nl%rcuts, dim=1)
        do jk = 1, ik
            if (self%realspacecoulomb_calc%nl%rcuts(ik, jk) < 1e5_dp) then
                print '("     Kind pair (", (i0), ", ", (i0), "): ", (f9.3))', &
                      ik, jk, self%realspacecoulomb_calc%nl%rcuts(ik, jk)
            else
                print '("     Kind pair (", (i0), ", ", (i0), "): ", (g0.3))', &
                      ik, jk, self%realspacecoulomb_calc%nl%rcuts(ik, jk)
            end if
        end do
    end do
    print *
end subroutine print_description


subroutine calculate_energy(self, atoms)
    !! Calculates the electrostatic energy for atom-centered charge
    !! moments associated with the given [[tibi_atoms:Atoms_type]]
    !! object, which is stored in atoms%energy.
    class(CoulombCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms

    real(dp) :: energy

    call task_timer%start_timer('coulomb_potential_realspace')
    call self%realspacecoulomb_calc%calculate_local_potentials()
    call task_timer%stop_timer('coulomb_potential_realspace')

    call task_timer%start_timer('coulomb_energy_realspace')
    call self%realspacecoulomb_calc%calculate_energy(atoms)
    call task_timer%stop_timer('coulomb_energy_realspace')

    energy = atoms%energy

    if (self%include_reciprocal) then
        call task_timer%start_timer('coulomb_potential_reciprocal')
        call self%recipcoulomb_calc%calculate_local_potentials()
        call task_timer%stop_timer('coulomb_potential_reciprocal')

        call task_timer%start_timer('coulomb_energy_reciprocal')
        call self%recipcoulomb_calc%calculate_energy(atoms)
        call task_timer%stop_timer('coulomb_energy_reciprocal')

        energy = energy + atoms%energy
    end if

    atoms%energy = energy
end subroutine calculate_energy


subroutine calculate_forces(self, atoms, do_stress)
    !! Calculates the atomic forces for the given [[tibi_atoms:Atoms_type]]
    !! object, which is stored in atoms%forces.
    class(CoulombCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    logical, intent(in) :: do_stress
        !! whether to also compute the stress tensor for periodic structures

    real(dp), dimension(:, :), allocatable :: forces, stress

    call task_timer%start_timer('coulomb_forces_realspace')
    call self%realspacecoulomb_calc%calculate_forces(atoms, do_stress=do_stress)
    call task_timer%stop_timer('coulomb_forces_realspace')

    if (self%include_reciprocal) then
        call task_timer%start_timer('coulomb_forces_reciprocal')
        allocate(forces(3, atoms%natom))
        forces = atoms%forces

        if (do_stress) then
            allocate(stress(3, 3))
            stress = atoms%stress
        end if

        call self%recipcoulomb_calc%calculate_forces(atoms, do_stress=do_stress)
        atoms%forces = atoms%forces + forces
        if (do_stress) atoms%stress = atoms%stress + stress
        call task_timer%stop_timer('coulomb_forces_reciprocal')
    end if
end subroutine calculate_forces


pure subroutine get_local_potentials(self, locpot)
    !! Returns an array with (for each charge moment) the energy derivative
    !! w.r.t. that charge moment (e.g. the electrostatic potential in case
    !! of a charge monopole).
    class(CoulombCalculator_type), intent(in) :: self
    real(dp), dimension(:), intent(out) :: locpot

    locpot = 0._dp
    call self%realspacecoulomb_calc%add_local_potentials(locpot)

    if (self%include_reciprocal) then
        call self%recipcoulomb_calc%add_local_potentials(locpot)
    end if
end subroutine get_local_potentials


subroutine update_moments(self, moments)
    !! Sets the charge moments of the associated real and reciprocal space
    !! Coulomb interaction calculators.
    class(CoulombCalculator_type), intent(inout) :: self
    real(dp), dimension(:), intent(in) :: moments

    call self%realspacecoulomb_calc%update_moments(moments)

    if (self%include_reciprocal) then
        call self%recipcoulomb_calc%update_moments(moments)
    end if
end subroutine update_moments


subroutine update_geometry(self, atoms)
    !! Updates the calculator parts which depend on the atomic positions
    !! and cell vectors.
    class(CoulombCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    call task_timer%start_timer('coulomb_geometry_realspace')
    call self%realspacecoulomb_calc%update_geometry(atoms)
    call task_timer%stop_timer('coulomb_geometry_realspace')

    if (self%include_reciprocal) then
        call task_timer%start_timer('coulomb_geometry_reciprocal')
        call self%recipcoulomb_calc%update_geometry(atoms, self%ewald_tolerance)
        call task_timer%stop_timer('coulomb_geometry_reciprocal')
    end if
end subroutine update_geometry

end module tibi_coulomb
