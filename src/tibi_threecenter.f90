 !-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_threecenter
!! Module for three-center terms and rotations

use tibi_constants, only: dp, &
                          pi
use tibi_orbitals, only: lm_labels, &
                         orbital_index
use tibi_spline, only: interpolate_tricubic_1d_batch, &
                       interpolate_tricubic_batch, &
                       interpolate_tricubic_deriv_batch, &
                       interpolate_trilinear_batch, &
                       interpolate_trilinear_deriv_batch, &
                       interpolate_unicubic_batch, &
                       interpolate_unicubic_deriv_batch
use tibi_utils_prog, only: assert, &
                           is_master
use tibi_utils_string, only: count_items, &
                             find_string_index, &
                             get_file_extension, &
                             int2str
implicit none


integer, parameter :: MAXL_3C = 3
    !! highest implemented subshell index for three-center integrals
integer, parameter :: MAXLM_3C = 9
    !! highest implemented orbital index for three-center integrals


type ThreeCenter_type
    !! Class for evaluating the (tau-dependent) three-center contributions
    !! for a set of basis functions on a pair of atoms, due to the presence
    !! of a third atom.
    integer :: numint
        !! the number of stored three-center integrals
    integer :: numu, numv, numw
        !! sizes of the grid
    real(dp) :: minu, minv, minw
        !! origin of the (padded) grid
    real(dp) :: du, dv, dw
        !! grid spacings
    integer, dimension(MAXLM_3C, MAXLM_3C) :: integral_indices
        !! index of every (ilm, jlm)-integral in the table (0 if not present)
    logical, dimension(MAXLM_3C**2) :: mask_int
        !! array for unpacking a (numint,) array
    logical, dimension(:), allocatable :: mask_sign
        !! (numint) array indicating which integrals change sign when x < 0
    real(dp) :: rCM_switch
        !! inner CM radius (equal to exp(self%minv+self%dv)) below which the
        !! rCM0 tables are used together with a (linear) switching function
    real(dp), dimension(:, :), allocatable :: rcuts_rCM
        !! (2, numu) array of inner and outer cutoffs for the rCM distances
    real(dp), dimension(:, :, :, :), allocatable :: tables
        !! (numint, numw, numv, numu) precalculated three-center contributions
        !! for every included integral
    real(dp), dimension(:, :), allocatable :: tables_rCM0
        !! (numint, numu) precalculated three-center contributions for every
        !! included integral in the special case of rCM = 0
    real(dp), dimension(:, :, :, :), allocatable :: tables_upsampled
        !! refinement of self%tables (see upu/upv/upw factors), to be used
        !! with e.g. linear instead of cubic interpolation
    integer :: upu = 1
        !! Upsampling factor for the denser grid along the 'u' direction
    integer :: upv = 1
        !! Upsampling factor for the denser grid along the 'v' direction
    integer :: upw = 1
        !! Upsampling factor for the denser grid along the 'w' direction
    contains
    procedure :: evaluate
    procedure :: evaluate_deriv
    procedure :: get_rcut_rCM
    procedure :: initialize
    procedure :: initialize_masks
    procedure :: initialize_rcuts_rCM
    procedure :: initialize_upsampling
    procedure :: read_ascii_file
    procedure :: read_binary_file
    procedure :: write_binary_file
end type ThreeCenter_type


contains


pure subroutine evaluate(self, rAB, rCM, theta, order, fval)
    !! Returns all three-center contributions for the given geometry.
    class(ThreeCenter_type), intent(in) :: self
    real(dp), intent(in) :: rAB
        !! distance between atoms A and B
    real(dp), intent(in) :: rCM
        !! distance between atom C and the middle of atoms A and B
    real(dp), intent(in) :: theta
        !! 'polar' angle of the C-M axis w.r.t. the A-B axis (between 0 and pi)
    integer, intent(in) :: order
        !! interpolation order (1 or 3)
    real(dp), dimension(MAXLM_3C**2), intent(out) :: fval
        !! three-center contributions for all implemented orbital pairs
        !! (packed format)

    integer :: i, iint, iu, iv, iw
    real(dp) :: theta_clip, u, v, w
    real(dp), parameter :: eps_pi = 1e-6_dp
    real(dp), dimension(self%numint) :: tmp
    real(dp), dimension(self%numint, order+1, order+1, order+1) :: flocal
    logical :: opposite

    opposite = theta < 0._dp

    u = log(rAB) - self%minu

    if (rCM < self%rCM_switch) then
        v = self%dv
    else
        v = log(rCM) - self%minv
    end if

    theta_clip = max(eps_pi, min(pi - eps_pi, abs(theta)))
    w = theta_clip - self%minw

    iu = floor(u / self%du)
    iv = floor(v / self%dv)
    iw = floor(w / self%dw)

    u = u / self%du - iu
    v = v / self%dv - iv
    w = w / self%dw - iw

    if (order == 1) then
        if (iu < 0 .or. iv < 0 .or. iw < 0) then
            fval = 0._dp
            return
        end if
        if (iu > self%numu-2 .or. iv > self%numv-2 .or. iw > self%numw-2) then
            fval = 0._dp
            return
        end if
    elseif (order == 3) then
        if (iu < 1 .or. iv < 1 .or. iw < 1) then
            fval = 0._dp
            return
        end if
        if (iu > self%numu-3 .or. iv > self%numv-3 .or. iw > self%numw-3) then
            fval = 0._dp
            return
        end if
    end if

    if (order == 1) then
        flocal = self%tables_upsampled(:, iw+1:iw+2, iv+1:iv+2, iu+1:iu+2)
        tmp = interpolate_trilinear_batch(w, v, u, self%numint, flocal)
    elseif (order == 3) then
        flocal = self%tables_upsampled(:, iw:iw+3, iv:iv+3, iu:iu+3)
        tmp = interpolate_tricubic_batch(w, v, u, self%numint, flocal)
    end if

    if (rCM < self%rCM_switch) then
        u = log(rAB) - (self%minu - (self%upu - 1) * self%du)
        iu = floor(u / (self%du * self%upu))
        u = u / (self%du * self%upu) - iu
        v = rCM / self%rCM_switch

        tmp = v * tmp &
              + (1._dp - v) &
                * interpolate_unicubic_batch(u, self%numint, &
                                             self%tables_rCM0(:, iu:iu+3))
    end if

    if (opposite) then
        i = 1
        do iint = 1, MAXLM_3C**2
            if (self%mask_int(iint)) then
                if (self%mask_sign(i)) then
                    fval(iint) = -tmp(i)
                else
                    fval(iint) = tmp(i)
                end if
                i = i + 1
            else
                fval(iint) = 0._dp
            end if
        end do
    else
        i = 1
        do iint = 1, MAXLM_3C**2
            if (self%mask_int(iint)) then
                fval(iint) = tmp(i)
                i = i + 1
            else
                fval(iint) = 0._dp
            end if
        end do
    end if
end subroutine evaluate


pure subroutine evaluate_deriv(self, rAB, rCM, theta, drAB, drCM, dvABvCM, &
                               order, fder)
    !! Returns all three-center contribution derivatives for the given geometry.
    class(ThreeCenter_type), intent(in) :: self
    real(dp), intent(in) :: rAB
        !! Distance between A and B
    real(dp), intent(in) :: rCM
        !! Distance between C and the middle of A and B
    real(dp), intent(in) :: theta
        !! 'polar' angle of the C-M axis w.r.t. the A-B axis (between 0 and pi)
    real(dp), dimension(3, 2), intent(in) :: drAB
        !! derivatives of rAB
    real(dp), dimension(3, 2), intent(in) :: drCM
        !! derivatives of rCM
    real(dp), dimension(3, 2), intent(in) :: dvABvCM
        !! derivatives of the dot product of the (normalized) AB and CM vectors
    integer, intent(in) :: order
        !! interpolation order (1 or 3)
    real(dp), dimension(MAXLM_3C**2, 3, 2), intent(out) :: fder
        !! three-center contribution derivatives for all implemented orbital
        !! pairs (packed format) and xyz directions for A and B

    integer :: i, j, ider, iint, iu, iv, iw
    logical :: opposite
    real(dp) :: dtheta, theta_clip, u, v, w
    real(dp), parameter :: eps_pi = 1e-6_dp
    real(dp), dimension(3) :: duvwdx
    real(dp), dimension(self%numint) :: fval, tmp
    real(dp), dimension(self%numint, 3) :: dfduvw
    real(dp), dimension(self%numint, order+1, order+1, order+1) :: flocal

    opposite = theta < 0._dp

    u = log(rAB) - self%minu

    if (rCM < self%rCM_switch) then
        v = self%dv
    else
        v = log(rCM) - self%minv
    end if

    theta_clip = max(eps_pi, min(pi - eps_pi, abs(theta)))
    w = theta_clip - self%minw

    iu = floor(u / self%du)
    iv = floor(v / self%dv)
    iw = floor(w / self%dw)

    u = u / self%du - iu
    v = v / self%dv - iv
    w = w / self%dw - iw

    if (order == 1) then
        if (iu < 0 .or. iv < 0 .or. iw < 0) then
            fder = 0._dp
            return
        end if
        if (iu > self%numu-2 .or. iv > self%numv-2 .or. iw > self%numw-2) then
            fder = 0._dp
            return
        end if
        flocal = self%tables_upsampled(:, iw+1:iw+2, iv+1:iv+2, iu+1:iu+2)
        dfduvw = interpolate_trilinear_deriv_batch(w, v, u, self%numint, flocal)
    elseif (order == 3) then
        if (iu < 1 .or. iv < 1 .or. iw < 1) then
            fder = 0._dp
            return
        end if
        if (iu > self%numu-3 .or. iv > self%numv-3 .or. iw > self%numw-3) then
            fder = 0._dp
            return
        end if
        flocal = self%tables_upsampled(:, iw:iw+3, iv:iv+3, iu:iu+3)
        dfduvw = interpolate_tricubic_deriv_batch(w, v, u, self%numint, flocal)
    end if

    if (opposite) then
        where (self%mask_sign)
            dfduvw(:, 1) = -dfduvw(:, 1)
            dfduvw(:, 2) = -dfduvw(:, 2)
            dfduvw(:, 3) = -dfduvw(:, 3)
        end where
    end if

    if (rCM < self%rCM_switch) then
        if (order == 1) then
            fval = interpolate_trilinear_batch(w, v, u, self%numint, flocal)
        elseif (order == 3) then
            fval = interpolate_tricubic_batch(w, v, u, self%numint, flocal)
        end if

        if (opposite) then
            where (self%mask_sign)
                fval = -fval
            end where
        end if

        u = log(rAB) - (self%minu - (self%upu - 1) * self%du)
        iu = floor(u / (self%du * self%upu))
        u = u / (self%du * self%upu) - iu
        v = rCM / self%rCM_switch
    end if

    dtheta = -1._dp / sqrt(1._dp - cos(theta_clip)**2)

    do i = 1, 2
        do ider = 1, 3
            if (rCM < self%rCM_switch) then
                duvwdx = [rCM * 1._dp/rAB * drAB(ider, i) / self%du, 0._dp, &
                          rCM * dtheta * dvABvCM(ider, i) / self%dw]

                tmp = matmul(dfduvw, duvwdx) / self%rCM_switch &
                      + drCM(ider, i) / self%rCM_switch * fval(:) &
                      + (1._dp - v) * 1._dp/rAB * drAB(ider, i) &
                        / (self%upu * self%du) &
                        * interpolate_unicubic_deriv_batch(u, self%numint, &
                                                self%tables_rCM0(:, iu:iu+3)) &
                      - drCM(ider, i) / self%rCM_switch &
                        * interpolate_unicubic_batch(u, self%numint, &
                                                self%tables_rCM0(:, iu:iu+3))
            else
                duvwdx = [1._dp/rAB * drAB(ider, i) / self%du, &
                          1._dp/rCM * drCM(ider, i) / self%dv, &
                          dtheta * dvABvCM(ider, i) / self%dw]
                tmp = matmul(dfduvw, duvwdx)
            end if

            j = 1
            do iint = 1, MAXLM_3C**2
                if (self%mask_int(iint)) then
                    fder(iint, ider, i) = tmp(j)
                    j = j + 1
                else
                    fder(iint, ider, i) = 0._dp
                end if
            end do
        end do
    end do
end subroutine evaluate_deriv


pure subroutine get_rcut_rCM(self, rAB, r_inner, r_outer)
    !! Returns the C-M distance cutoffs, for the given A-B distance,
    !! within which a (potentially) higher interpolation should be
    !! applied (r_inner) and beyond which the contributions can be
    !! neglected alltogether (r_outer).
    class(ThreeCenter_type), intent(in) :: self
    real(dp), intent(in) :: rAB
        !! distance between A and B
    real(dp), intent(out) :: r_inner
        !! inner cutoff radius
    real(dp), intent(out) :: r_outer
        !! outer cutoff radius

    integer :: iu, nu
    real(dp) :: u

    u = log(rAB) - (self%minu - (self%upu - 1) * self%du)
    iu = floor(u / (self%du * self%upu))

    nu = size(self%rcuts_rCM, dim=2)
    iu = max(1, min(nu, iu+1))

    r_outer = self%rcuts_rCM(1, iu)
    r_inner = self%rcuts_rCM(2, iu)
end subroutine get_rcut_rCM


subroutine initialize(self, filename, eps_inner, eps_outer, upuvw, write_3cb)
    !! Full initialization from the given parameter file.
    !!
    !! If in ASCII format (.3cf extension), the grid settings and integral
    !! values are read in, followed by calculating the rCM cutoffs
    !! corresponding to the given threshold, followed by grid refinement.
    !!
    !! If in binary format (.3cb extension) then all attributes are read
    !! in from that file.
    class(ThreeCenter_type), intent(out) :: self
    character(len=*), intent(in) :: filename
        !! name of the three-center parameter file in 3cf or 3cb format
    real(dp), intent(in) :: eps_inner
        !! threshold for deciding the interpolation order (if in 3cf format)
    real(dp), intent(in) :: eps_outer
        !! threshold for truncating the integrals (if in 3cf format)
    integer, dimension(3), intent(in) :: upuvw
        !! upsampling factors (if in 3cf format)
    logical, intent(in) :: write_3cb
        !! whether to write a 3cb file after reading (if in 3cf format)

    character(len=:), allocatable :: ext
    integer :: length

    ext = trim(get_file_extension(filename))

    select case(ext)
    case('.3cf')
        call self%read_ascii_file(filename)
        call self%initialize_rcuts_rCM(eps_inner, eps_outer)
        call self%initialize_upsampling(upuvw)

        if (write_3cb .and. is_master()) then
            length = len(filename) - len(ext)
            call self%write_binary_file(filename(1:length) // '.3cb')
        end if
    case('.3cb')
        call self%read_binary_file(filename)
        call self%initialize_rcuts_rCM(eps_inner, eps_outer)

        call assert(all([self%upu, self%upv, self%upw] == upuvw), &
                    'Upsampling in ' // filename // ' (' // int2str(self%upu) &
                    // ', ' // int2str(self%upv) // ', ' // int2str(self%upw) &
                    // ') does not match the given upsampling (' &
                    // int2str(upuvw(1)) // ', ' // int2str(upuvw(2)) // ', ' &
                    // int2str(upuvw(3)) // ')')
    case default
        call assert(.false., 'Unknown three-center file format: ' // ext)
    end select
end subroutine initialize


subroutine initialize_masks(self)
    !! Initializes the self%mask_int and self%mask_sign arrays.
    class(ThreeCenter_type), intent(inout) :: self

    integer :: i, iint, il, jl, ilm, jlm
    logical :: ix, jx
    integer, dimension(3), parameter :: lm_asym = [2, 5, 7]  ! px, dxy, dxz

    allocate(self%mask_sign(self%numint))

    i = 1
    iint = 1

    do il = 1, MAXL_3C
        do jl = 1, MAXL_3C
            do jlm = orbital_index(jl), orbital_index(jl+1)-1
                jx = any(lm_asym == jlm)
                do ilm = orbital_index(il), orbital_index(il+1)-1
                    ix = any(lm_asym == ilm)
                    self%mask_int(iint) = self%integral_indices(ilm, jlm) > 0

                    if (self%mask_int(iint)) then
                        self%mask_sign(i) = (ix .and. .not. jx) .or. &
                                            (.not. ix .and. jx)
                        i = i + 1
                    end if

                    iint = iint + 1
                end do
            end do
        end do
    end do

    call assert(count(self%mask_int) == self%numint, 'Internal error: ' // &
                'numint does not equal .true. values in mask_int')
end subroutine initialize_masks


subroutine initialize_rcuts_rCM(self, eps_inner, eps_outer)
    !! Sets up the self%rcuts_rCM array with the rCM cutoffs.
    class(ThreeCenter_type), intent(inout) :: self
    real(dp), intent(in) :: eps_inner
        !! threshold for deciding the interpolation order
    real(dp), intent(in) :: eps_outer
        !! threshold for truncating the integrals

    integer :: iu, iv, iw
    real(dp) :: fval, rCM

    call assert(eps_inner >= eps_outer, &
                'threshold eps_inner must be >= eps_outer')

    allocate(self%rcuts_rCM(2, self%numu))
    self%rcuts_rCM(1, :) = -1._dp

    iu_loop: do iu = 1, self%numu
        do iv = self%numv, 2, -1
            rCM = exp(self%minv + self%dv*iv)

            do iw = 1, self%numw
                fval = maxval(abs(self%tables(:, iw, iv, iu)))

                if (fval > eps_outer) then
                    if (self%rcuts_rCM(1, iu) < 0._dp) then
                        self%rcuts_rCM(1, iu) = rCM
                    end if

                    if (fval > eps_inner) then
                        self%rcuts_rCM(2, iu) = rCM
                        cycle iu_loop
                    end if
                end if
            end do
        end do

        ! This section is only reached if the iu-loop does not get cycled

        rCM = exp(self%minv + self%dv)
        fval = maxval(abs(self%tables_rCM0(:, iu)))

        if (fval > eps_outer) then
            if (self%rcuts_rCM(1, iu) < 0._dp) then
                self%rcuts_rCM(1, iu) = rCM
            end if

            self%rcuts_rCM(2, iu) = rCM
        else
            if (self%rcuts_rCM(1, iu) < 0._dp) then
                self%rcuts_rCM(1, iu) = -epsilon(1._dp)
            end if

            if (fval > eps_inner) then
                self%rcuts_rCM(2, iu) = rCM
            else
                self%rcuts_rCM(2, iu) = -epsilon(1._dp)
            end if
        end if
    end do iu_loop
end subroutine initialize_rcuts_rCM


pure subroutine initialize_upsampling(self, upuvw)
    !! Constructs the denser integral tables (self%tables_upsampled) from
    !! self%tables and the given upsampling factors. Updates self%du/dv/dw,
    !! self%numu/numv/numw and self%minu/minv/minw to those of the denser grid.
    class(ThreeCenter_type), intent(inout) :: self
    integer, dimension(3), intent(in) :: upuvw
        !! upsampling factors

    integer :: i, i2, iint, numw, numv, numu, nup, nup2
    integer :: iu, iu1, iv, iv1, iw, iw1
    real(dp) :: du, dv, dw, x
    real(dp), dimension(self%numint) :: fval0
    real(dp), dimension(:), allocatable :: u, u2, v, v2, w, w2
    real(dp), dimension(self%numint, 64) :: flocal
    real(dp), dimension(:, :), allocatable :: fval, fval2

    self%upu = upuvw(1)
    self%upv = upuvw(2)
    self%upw = upuvw(3)

    numu = 3 + (self%numu - 3) * self%upu
    numv = 3 + (self%numv - 3) * self%upv
    numw = 3 + (self%numw - 3) * self%upw
    allocate(self%tables_upsampled(self%numint, numw, numv, numu))

    du = self%du / self%upu
    dv = self%dv / self%upv
    dw = self%dw / self%upw

    if (self%upu == 1 .and. self%upv == 1 .and. self%upw == 1) then
        self%tables_upsampled(:, :, :, :) = self%tables(:, :, :, :)
    else
        nup = (self%upu+1) * (self%upv+1) * (self%upw+1)
        allocate(u(nup))
        allocate(v(nup))
        allocate(w(nup))
        allocate(fval(self%numint, nup))

        nup2 = self%upu * self%upv * self%upw
        allocate(u2(nup2))
        allocate(v2(nup2))
        allocate(w2(nup2))
        allocate(fval2(self%numint, nup2))

        i = 1
        i2 = 1
        do iu = 0, self%upu
            do iv = 0, self%upv
                do iw = 0, self%upw
                    u(i) = iu * 1._dp / self%upu
                    v(i) = iv * 1._dp / self%upv
                    w(i) = iw * 1._dp / self%upw

                    if (iu /= self%upu .and. iv /= self%upv .and. &
                        iw /= self%upw) then
                        u2(i2) = u(i)
                        v2(i2) = v(i)
                        w2(i2) = w(i)
                        i2 = i2 + 1
                    end if

                    i = i + 1
                end do
            end do
        end do

        do iu = 2, self%numu-2
            iu1 = 2 + self%upu * (iu - 2)

            do iv = 2, self%numv-2
                iv1 = 2 + self%upv * (iv - 2)

                do iw = 2, self%numw-2
                    iw1 = 2 + self%upw * (iw - 2)

                    flocal = reshape(self%tables(:, iw-1:iw+2, iv-1:iv+2, &
                                                 iu-1:iu+2), [self%numint, 64])

                    if (iu == self%numu-2 .or. iv == self%numv-2 .or. &
                        iw == self%numw-2) then
                        fval = interpolate_tricubic_1d_batch(nup, self%numint, &
                                                             w, v, u, flocal)
                        self%tables_upsampled(:, iw1:iw1+self%upw, &
                                              iv1:iv1+self%upv, &
                                              iu1:iu1+self%upu) = &
                            reshape(fval, [self%numint, &
                                           self%upw+1, self%upv+1, self%upu+1])
                    else
                        fval2 = interpolate_tricubic_1d_batch(nup2, &
                                            self%numint, w2, v2, u2, flocal)
                        self%tables_upsampled(:, iw1:iw1+self%upw-1, &
                                              iv1:iv1+self%upv-1, &
                                              iu1:iu1+self%upu-1) = &
                            reshape(fval2, [self%numint, &
                                            self%upw, self%upv, self%upu])
                    end if
                end do
            end do
        end do

        ! Boundary conditions for theta (both Neumann)
        self%tables_upsampled(:, 1, 2:numv-1, 2:numu-1) = &
                            self%tables_upsampled(:, 3, 2:numv-1, 2:numu-1)
        self%tables_upsampled(:, numw, 2:numv-1, 2:numu-1) = &
                            self%tables_upsampled(:, numw-2, 2:numv-1, 2:numu-1)
        do iint = 1, self%numint
            if (self%mask_sign(iint)) then
                self%tables_upsampled(iint, 1, 2:numv-1, 2:numu-1) = &
                        -self%tables_upsampled(iint, 1, 2:numv-1, 2:numu-1)
                self%tables_upsampled(iint, numw, 2:numv-1, 2:numu-1) = &
                        -self%tables_upsampled(iint, numw, 2:numv-1, 2:numu-1)
            end if
        end do

        ! Boundary conditions for rCM (left Neumann, right Dirichlet)
        do iu = 2, self%numu-2
            i = 2 + self%upu * (iu - 2)
            do iu1 = i, i + self%upu
                x = (iu1 - i) * 1._dp / self%upu
                fval0(:) = interpolate_unicubic_batch(x, self%numint, &
                                                self%tables_rCM0(:, iu-1:iu+2))

                self%tables_upsampled(:, :, 1, iu1) = &
                    self%tables_upsampled(:, :, 3, iu1) &
                    - 2._dp * dv * (self%tables_upsampled(:, :, 2, iu1) &
                                    - spread(fval0, dim=2, ncopies=numw))
            end do
        end do
        self%tables_upsampled(:, :, numv, 2:numu-1) = 0._dp

        ! Boundary conditions for rAB (left Neumann, right Dirichlet)
        self%tables_upsampled(:, :, :, 1) = &
                            self%tables_upsampled(:, :, :, 3)
        self%tables_upsampled(:, :, :, numu) = 0._dp
    end if

    self%numu = numu
    self%numv = numv
    self%numw = numw

    self%minu = self%minu + self%du - du
    self%minv = self%minv + self%dv - dv
    self%minw = self%minw + self%dw - dw

    self%du = du
    self%dv = dv
    self%dw = dw

    self%rCM_switch = exp(self%minv + self%dv)
end subroutine initialize_upsampling


subroutine read_ascii_file(self, filename)
    !! Reads the three-center parameters from the given file.
    !!
    !! File structure:
    !!
    !! rAB_min  rAB_max  num_rAB
    !! rCM_min  rCM_max  num_rCM
    !! num_theta
    !! s_s s_px ...
    !! <s_s, s_px, ... tables, varying first per theta,
    !!  then per rCM, then per rAB>
    class(ThreeCenter_type), intent(out) :: self
    character(len=*), intent(in) :: filename
        !! Name of the three-center parameter file in 3cf format

    character(len=1024) :: buffer
    character(2*len(lm_labels(1))+1), dimension(:), allocatable :: labels
    logical :: file_exists
    integer :: fhandle, il, jl, ilm, jlm, iint, isep, iu, iv, iw, numx
    integer, dimension(:), allocatable :: ordering
    real(dp) :: xhi, xlo
    real(dp), dimension(:), allocatable :: tmp

    inquire(file=filename, exist=file_exists)
    call assert(file_exists, 'Parameter file does not exist: ' // filename)

    open(newunit=fhandle, file=filename, status='old', action='read')

    ! line 1: rAB grid definition (logarithmic)
    read(fhandle, *) xlo, xhi, numx
    self%minu = log(xlo)
    self%numu = numx
    if (numx > 1) then
        self%du = (log(xhi) - log(xlo)) / (numx - 1)
    else
        self%du = 0._dp
    end if

    ! line 2: rCM grid definition (logarithmic))
    read(fhandle, *) xlo, xhi, numx
    self%minv = log(xlo)
    self%numv = numx
    if (numx > 1) then
        self%dv = (log(xhi) - log(xlo)) / (numx - 1)
    else
        self%dv = 0._dp
    end if

    ! Line 3: theta grid definition (linear, from 0 to pi)
    read(fhandle, *) numx
    self%minw = 0.
    self%numw = numx
    if (numx > 1) then
        self%dw = pi / (numx - 1)
    else
        self%dw = 0._dp
    end if

    ! Line 4: integral labels (s_s, s_px, ...)
    read(fhandle, '(a)') buffer
    self%numint = count_items(buffer)
    allocate(labels(self%numint))
    read(buffer, *) labels(:)

    self%integral_indices = 0
    do iint = 1, self%numint
        associate(s => labels(iint))
        isep = index(s, '_')
        ilm = find_string_index(lm_labels, s(1:isep-1))
        jlm = find_string_index(lm_labels, trim(s(isep+1:)))
        self%integral_indices(ilm, jlm) = iint
        end associate
    end do

    ! Determine how to reorder the tabulated integrals
    allocate(ordering(self%numint))
    iint = 1
    do il = 1, MAXL_3C
        do jl = 1, MAXL_3C
            do jlm = orbital_index(jl), orbital_index(jl+1)-1
                do ilm = orbital_index(il), orbital_index(il+1)-1
                   if (self%integral_indices(ilm, jlm) > 0) then
                        ordering(iint) = self%integral_indices(ilm, jlm)
                        iint = iint + 1
                    end if
                end do
            end do
        end do
    end do

    call self%initialize_masks()

    ! Add padding
    self%minu = self%minu - self%du
    self%minv = self%minv - self%dv
    self%minw = self%minw - self%dw
    self%numu = self%numu + 2
    self%numv = self%numv + 2
    self%numw = self%numw + 2

    self%rCM_switch = exp(self%minv + self%dv)

    ! Read in the function values and fill in padding slices
    allocate(self%tables(self%numint, self%numw, self%numv, self%numu))
    allocate(self%tables_rCM0(self%numint, self%numu+3))
    allocate(tmp(self%numint))

    do iu = 2, self%numu-1
        ! Starts with single value for rCM = 0
        read(fhandle, *) tmp
        do iint = 1, self%numint
            self%tables_rCM0(iint, iu) = tmp(ordering(iint))
        end do

        do iv = 2, self%numv-1
            do iw = 2, self%numw-1
                read(fhandle, *) tmp
                do iint = 1, self%numint
                    self%tables(iint, iw, iv, iu) = tmp(ordering(iint))
                end do
            end do

            ! Boundary conditions for theta (both Neumann)
            self%tables(:, 1, iv, iu) = self%tables(:, 3, iv, iu)
            self%tables(:, self%numw, iv, iu) = &
                                        self%tables(:, self%numw-2, iv, iu)
            where (self%mask_sign)
                self%tables(:, 1, iv, iu) = -self%tables(:, 1, iv, iu)
                self%tables(:, self%numw, iv, iu) = &
                                            -self%tables(:, self%numw, iv, iu)
            end where
        end do

        ! Boundary conditions for rCM (left Neumann, right Dirichlet)
        self%tables(:, :, 1, iu) = self%tables(:, :, 3, iu) &
            - 2._dp * self%dv * (self%tables(:, :, 2, iu) &
                                - spread(self%tables_rCM0(:, iu), 2, self%numw))
        self%tables(:, :, self%numv, iu) = 0._dp
    end do

    ! Boundary conditions for rAB (left Neumann, right Dirichlet)
    self%tables(:, :, :, 1) = self%tables(:, :, :, 3)
    self%tables(:, :, :, self%numu) = 0._dp
    self%tables_rCM0(:, 1) = self%tables_rCM0(:, 3)
    self%tables_rCM0(:, self%numu:) = 0._dp

    close(fhandle)
end subroutine read_ascii_file


subroutine read_binary_file(self, filename)
    !! Reads in all attributes from the given file in binary format
    !! (see write_binary_file).
    class(ThreeCenter_type), intent(out) :: self
    character(len=*), intent(in) :: filename
        !! Name of the unformatted parameter file

    integer :: fhandle
    logical :: file_exists
    integer :: n1, n2, n3, n4

    inquire(file=filename, exist=file_exists)
    call assert(file_exists, 'Parameter file does not exist: ' // filename)

    open(newunit=fhandle, file=filename, status='old', action='read', &
         form='unformatted')

    read(fhandle) self%numint, &
                  self%numu, self%numv, self%numw, &
                  self%minu, self%minv, self%minw, &
                  self%du, self%dv, self%dw, &
                  self%rCM_switch, &
                  self%upu, self%upv, self%upw

    allocate(self%mask_sign(self%numint))
    read(fhandle) self%integral_indices, self%mask_int, self%mask_sign

    read(fhandle) n1, n2, n3, n4
    allocate(self%tables(n1, n2, n3, n4))
    read(fhandle) self%tables

    read(fhandle) n1, n2
    allocate(self%tables_rCM0(n1, n2))
    read(fhandle) self%tables_rCM0

    read(fhandle) n1, n2, n3, n4
    allocate(self%tables_upsampled(n1, n2, n3, n4))
    read(fhandle) self%tables_upsampled

    close(fhandle)
end subroutine read_binary_file


subroutine write_binary_file(self, filename)
    !! Writes all attributes to the given file in binary format.
    class(ThreeCenter_type), intent(in) :: self
    character(len=*), intent(in) :: filename
        !! Name of the unformatted (binary) output file

    integer :: fhandle

    open(newunit=fhandle, file=filename, status='replace', action='write', &
         form='unformatted')

    write(fhandle) self%numint, &
                   self%numu, self%numv, self%numw, &
                   self%minu, self%minv, self%minw, &
                   self%du, self%dv, self%dw, &
                   self%rCM_switch, &
                   self%upu, self%upv, self%upw

    write(fhandle) self%integral_indices, self%mask_int, self%mask_sign

    write(fhandle) shape(self%tables)
    write(fhandle) self%tables

    write(fhandle) shape(self%tables_rCM0)
    write(fhandle) self%tables_rCM0

    write(fhandle) shape(self%tables_upsampled)
    write(fhandle) self%tables_upsampled

    close(fhandle)
end subroutine write_binary_file

end module tibi_threecenter
