!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_neighborlist_verlet
!! Module for Verlet-type neighbor lists

use tibi_atoms, only: Atoms_type
use tibi_constants, only: dp
use tibi_neighborlist, only: get_translations, &
                             NeighborList_type
use tibi_utils_prog, only: assert
implicit none


type, extends(NeighborList_type) :: NeighborListVerlet_type
    !! A basic Verlet-type neighbor list
    real(dp) :: max_cell_change = 1e-9_dp
        !! maximal change in cell vector components before rebuilding
    real(dp) :: rskin = 2._dp
        !! skin radius
    contains
    procedure :: need_rebuild
    procedure :: rebuild
    procedure :: recalculate_vectors_and_distances
    procedure :: update => update_verlet
end type NeighborListVerlet_type


interface NeighborListVerlet_type
    module procedure construct_nl_verlet
end interface NeighborListVerlet_type


contains


function construct_nl_verlet(atoms, rcuts, rskin, self_interaction, both_ways) &
                             result(nl_verlet)
    !! Returns an initialized NeighborListVerlet_type neighborlist.
    type(Atoms_type), intent(in) :: atoms
    real(dp), dimension(:, :), intent(in) :: rcuts
        !! cutoff radii for all kind pairs
    real(dp), intent(in), optional :: rskin
        !! skin radius (default: 2 a0)
    logical, intent(in), optional :: self_interaction
        !! whether atoms are neighbors of themselves (default: .false.)
    logical, intent(in), optional :: both_ways
        !! whether two neighbor pairs are associated with every
        !! pair of atoms, one for each 'direction' (default: .false.)
    type(NeighborListVerlet_type) :: nl_verlet

    integer :: ik, jk

    call assert(size(rcuts, dim=1) == atoms%nkind .and. &
                size(rcuts, dim=2) == atoms%nkind, &
                'Bad rcuts dimensions for Verlet list')

    do ik = 1, atoms%nkind
        do jk = 1, ik-1
            call assert(abs(rcuts(jk, ik) - rcuts(ik, jk)) < 1e-6_dp, &
                        'Neighborlist rcuts matrix is expected to be symmetric')
        end do
    end do
    nl_verlet%rcuts = rcuts

    if (present(rskin)) then
        nl_verlet%rskin = rskin
    end if

    if (present(self_interaction)) then
        nl_verlet%self_interaction = self_interaction
    end if

    if (present(both_ways)) then
        nl_verlet%both_ways = both_ways
    end if

    allocate(nl_verlet%neighbors(atoms%natom))
    allocate(nl_verlet%start_indices(atoms%natom))

    call nl_verlet%rebuild(atoms)
end function construct_nl_verlet


pure function need_rebuild(self, atoms) result(is_needed)
    !! Returns whether a neighborlist rebuild is needed.
    class(NeighborListVerlet_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    logical :: is_needed

    real(dp) :: max_change

    max_change = maxval(abs(self%original_cell - atoms%cell))
    is_needed = max_change > self%max_cell_change

    if (.not. is_needed) then
        max_change =  maxval(norm2(self%original_positions - atoms%positions, &
                                   dim=1))
        is_needed = max_change > self%rskin
    end if
end function need_rebuild


subroutine rebuild(self, atoms)
    !! Rebuilds the Verlet neighborlist from scratch.
    !!
    !! @NOTE Only the 'list' itself is rebuild here (i.e. the self%neighbors,
    !! self%start_indices, self%neighbor_indices and self%translations arrays).
    !! Call the [[tibi_neighborlist:self%recalculate_vectors_and_distances]]
    !! routine afterwards to also update the self%vec and self%r arrays.
    class(NeighborListVerlet_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: iat, jat, ik, jk, itrans, counter, start_index, tot_neigh
    real(dp) :: r, rcutmax
    integer, dimension(:), allocatable :: tmp_neighbor_indices
    real(dp), dimension(:, :), allocatable :: tmp_translations, translations

    if (allocated(self%neighbor_indices)) deallocate(self%neighbor_indices)
    if (allocated(self%translations)) deallocate(self%translations)
    if (allocated(self%vec)) deallocate(self%vec)
    if (allocated(self%r)) deallocate(self%r)

    ! Find the Bravais lattice translations that need to be
    ! considered based on rcutmax
    rcutmax = maxval(self%rcuts) + self%rskin
    call get_translations(atoms, rcutmax, translations)

    ! Get upper bound to the total number of neighbors (inc. buffers)
    ! based on the scenario where all atoms in each image cell are
    ! a neighbor of each atom in the central unit cell
    tot_neigh = (atoms%natom + 1) * size(translations, dim=2)
    if (self%both_ways) then
        tot_neigh = tot_neigh * atoms%natom
    else
        tot_neigh = (tot_neigh * (atoms%natom + 1)) / 2
    end if

    allocate(tmp_neighbor_indices(tot_neigh))
    allocate(tmp_translations(3, tot_neigh))

    start_index = 1

    iat_loop: do iat = 1, atoms%natom
        self%start_indices(iat) = start_index
        ik = atoms%kinds(iat)

        counter = 0
        jat_loop: do jat = 1, atoms%natom
            if (.not. self%both_ways .and. jat > iat) then
                exit jat_loop
            end if

            jk = atoms%kinds(jat)

            itrans_loop: do itrans = 1, size(translations, dim=2)
                r = norm2(atoms%positions(:, jat) &
                          - atoms%positions(:, iat) &
                          + translations(:, itrans))

                if (iat == jat .and. r < self%rsame .and. &
                    .not. self%self_interaction) cycle

                if (r < self%rcuts(ik, jk) + self%rskin) then
                    tmp_neighbor_indices(start_index + counter) = jat
                    tmp_translations(:, start_index + counter) = &
                                                        translations(:, itrans)
                    counter = counter + 1
                end if
            end do itrans_loop
        end do jat_loop

        self%neighbors(iat) = counter
        start_index = start_index + counter
    end do iat_loop

    tot_neigh = start_index

    allocate(self%neighbor_indices(tot_neigh))
    allocate(self%translations(3, tot_neigh))
    self%neighbor_indices(:) = tmp_neighbor_indices(:tot_neigh)
    self%translations(:, :) = tmp_translations(:, :tot_neigh)

    allocate(self%vec(3, tot_neigh))
    allocate(self%r(tot_neigh))

    self%original_cell = atoms%cell
    self%original_positions = atoms%positions
    self%original_kinds = atoms%kinds
end subroutine rebuild


pure subroutine recalculate_vectors_and_distances(self, atoms)
    !! Recalculates self%vec and self%r from the updated atoms object,
    !! i.e. without building the neighborlist itself.
    class(NeighborListVerlet_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: iat, ineigh, ineighlist, jat
    real(dp) :: r
    real(dp), dimension(3) :: vec

    do iat = 1, atoms%natom
        do ineigh = 1, self%neighbors(iat)
            ineighlist = self%start_indices(iat) + ineigh - 1
            jat = self%neighbor_indices(ineighlist)

            vec(:) = atoms%positions(:, jat) - atoms%positions(:, iat) &
                     + self%translations(:, ineighlist)

            r = norm2(vec(:))
            self%r(ineighlist) = r

            if (r > self%rsame) then
                vec(:) = vec(:) / r
            end if

            self%vec(:, ineighlist) = vec(:)
        end do
    end do
end subroutine recalculate_vectors_and_distances


subroutine update_verlet(self, atoms)
    !! Updates the Verlet neighborlist based on the new atoms object.
    !!
    !! The update consists of rebuilding the neighborlist if the largest
    !! atomic displacement exceeds the skin radius.
    class(NeighborListVerlet_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    if (self%need_rebuild(atoms)) then
        call self%rebuild(atoms)
    end if

    call self%recalculate_vectors_and_distances(atoms)
end subroutine update_verlet

end module tibi_neighborlist_verlet
