!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_scf_tightbinding
!! Module for self-consistent tight-binding calculations

use tibi_atoms, only: Atoms_type
use tibi_constants, only: angstrom, &
                          dp, &
                          electronvolt
use tibi_coulomb, only: CoulombCalculator_type
use tibi_fluctuation, only: FluctuationCalculator_type
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_mixer, only: Mixer_type
use tibi_tightbinding, only: TightBindingCalculator_type
use tibi_timer, only: task_timer
use tibi_utils_prog, only: assert, &
                           is_master
implicit none


type, extends(TightBindingCalculator_type) :: ScfTightBindingCalculator_type
    !! Class for handling self-consistent tight-binding calculations.
    !!
    !! The 'mulliken' mapping is equivalent to the so-called SCC-TB method,
    !! see e.g. [Elstner (1998)](https://doi.org/10.1103/PhysRevB.58.7260)
    !! and [Koehler (2005)](https://doi.org/10.1016/j.chemphys.2004.03.034).
    !! The 'giese_york' mapping implements the scheme from
    !! [Giese and York (2011)](https://doi.org/10.1063/1.3587052).
    logical :: aux_basis_mulliken_subshell_dependent = .false.
        !! whether the Mulliken-type auxiliary basis is subshell-dependent
        !! instead of just atom-dependent. In the latter case, the atomic
        !! Hubbard values and spin constants are taken equal to those of
        !! the atom's lowest included angular momentum.
    type(FluctuationCalculator_type) :: chg_calc
        !! calculator for (non-point-Coulomb) interactions involving charges
    type(CoulombCalculator_type), allocatable :: coulomb_calc
        !! (point-)Coulomb interactions calculator
    real(dp) :: fluctuation_sum_tolerance = 1e-8_dp
        !! threshold for the difference between the charge/magnetization sum
        !! and the imposed total charge/magnetization (above this threshold,
        !! the auxiliary charges/magnetizations get shifted to satisfy the
        !! required sum)
    real(dp), dimension(:, :, :, :), allocatable :: hamiltonian0_real
        !! real-valued zeroth-order Hamiltonian
    complex(dp), dimension(:, :, :, :), allocatable :: hamiltonian0_complex
        !! complex-valued zeroth-order Hamiltonian
    real(dp), dimension(:), allocatable :: locpot_l
        !! array with the (l-dependent) contribution to the local potential
        !! arising from electrostatic interactions and (nonmagnetic)
        !! exchange-correlation
    real(dp), dimension(:), allocatable :: locpot_lm
        !! array with the (lm-dependent) contribution to the local potential
        !! arising from electrostatic interactions and (nonmagnetic)
        !! exchange-correlation
    real(dp), dimension(:), allocatable :: locpot_mp
        !! array with the (multipole-dependent) contribution to the local
        !! potential arising from electrostatic interactions and (nonmagnetic)
        !! exchange-correlation
    type(FluctuationCalculator_type), allocatable :: mag_calc
        !! calculator interactions involving magnetizations
    real(dp), dimension(:), allocatable :: magpot_l
        !! array with the (l-dependent) contribution to the local potential
        !! arising from magnetization
    real(dp), dimension(:), allocatable :: magpot_lm
        !! array with the (lm-dependent) contribution to the local potential
        !! arising from magnetization
    real(dp), dimension(:), allocatable :: magpot_mp
        !! array with the (multipole-dependent) contribution to the local
        !! potential arising from magnetization
    type(Mixer_type) :: mixer
        !! mixer used for the non-spin-dependent part of the potential
    type(Mixer_type), allocatable :: mixer_mag
        !! mixer used for mixing the magnetization potential contributions
    real(dp), dimension(:, :, :, :), allocatable :: multipoles2pointcartesian
        !! transformation matrix for every atomic kind to obtain Cartesian
        !! point multipoles from a spherical harmonics based representation
    real(dp), dimension(:, :, :, :), allocatable :: pointcartesian2multipoles
        !! transformation matrix for every atomic kind to obtain spherical
        !! harmonics based multipoles from a Cartesian (point) multipole one
    character(len=:), allocatable :: scf_aux_mapping
        !! type of mapping to apply ('giese_york' or 'mulliken')
    logical :: scf_aux_use_delta = .true.
        !! whether to handle the multipole electrostatics by subtracting
        !! the corresponding point multipole contributions and handle
        !! those separately (only for 'giese_york' scf_aux_mapping)
    character(len=128) :: scf_extrapolation = 'use_previous'
        !! method for 'extrapolating' the charges (and, if spin-polarized,
        !! also subshell magnetizations) between successive
        !! SCF cycles (e.g. between ionic steps)
    character(len=128) :: scf_initial_guess = 'zero'
        !! method for guessing the initial subshell charges (and, if spin-
        !! polarized, also subshell magnetizations)
    integer :: scf_maxiter = 250
        !! maximum number of SCF iterations
    logical :: scf_must_converge = .true.
        !! whether to abort in case SCF convergence is not reached
        !! within the maximum number of iterations
    character(len=256) :: scf_restart_input_file = 'scf_restart.in'
        !! file with subshell charges (and possibly magnetizations)
        !! to use as initial guesses
    character(len=256) :: scf_restart_output_file = 'scf_restart.out'
        !! file to which subshell charges (and possibly magnetizations)
        !! will be written for restarts
    real(dp) :: scf_tolerance = 1e-3_dp * electronvolt
        !! convergence criterion for the SCF cycle (more precisely the maximum
        !! absolute difference between successive potential vectors)
    integer :: scf_ukernel_expansion_offsite = 2
        !! term at which to truncate the multicenter expansion of the
        !! Hartree-XC kernel in off-site 'U' integrals
    integer :: scf_ukernel_expansion_onsite = 1
        !! term at which to truncate the multicenter expansion of the
        !! Hartree-XC kernel in on-site 'U' integrals
    character(len=128) :: scf_ukernel_off2c_model = 'elstner'
        !! model for the off-site two-center gamma function for charge
        !! fluctuations
    character(len=128) :: scf_ukernel_on1c_model = 'elstner'
        !! model for the (off-diagonal) on-site one-center U values
        !! for charge fluctuations
    integer :: scf_wkernel_expansion_offsite = 0
        !! term at which to truncate the multicenter expansion of the
        !! spin-polarized XC kernel in off-site 'W' integrals
    integer :: scf_wkernel_expansion_onsite = 1
        !! term at which to truncate the multicenter expansion of the
        !! spin-polarized XC kernel in on-site 'W' integrals
    character(len=1024), dimension(:), allocatable :: scf_wkernel_on1c
        !! array of on-site, one-center spin constant strings 'element_W1,...'
        !! (in electronvolt) for each element (and subshell pair, if
        !! subshell-dependence of the spin polarization is enabled);
        !! when equal to 'tabulated' this indicates that these values
        !! should be extracted from '1cl' files instead
    logical :: scf_write_restart_file = .true.
        !! whether to write a restart file at the end of each SCF cycle
    logical :: scf_write_verbose = .false.
        !! whether to write additional quantities to file (typically
        !! for debugging or testing purposes)
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: add_mulliken_shifts_to_hamiltonian
    procedure :: add_giese_york_shifts_to_hamiltonian
    procedure :: copy_subshell_to_orbital_array
    procedure :: get_hubbard_parameters
    procedure :: get_spin_parameters
    procedure :: calculate_energy
    procedure :: calculate_forces
    procedure :: calculate_h0_energy
    procedure :: apply_multipoles2pointcartesian
    procedure :: apply_pointcartesian2multipoles
    procedure :: get_initial_fluctuation_arrays
    procedure :: get_total_magnetization
    procedure :: initialize_pointcartesian_transforms
    procedure :: normalize_monopole_fluctuation_array
    procedure :: normalize_multipole_fluctuation_array
    procedure :: read_scf_restart_file
    procedure :: write_scf_restart_file
end type ScfTightBindingCalculator_type


interface
    ! Procedure(s) implemented in tibi_scf_tightbinding_charges

    pure module subroutine apply_multipoles2pointcartesian(self, atoms, vec_mp)
        !! Transforms the given multipole-dependent array from a spherical
        !! to a cartesian-point-multipole representation.
        class(ScfTightBindingCalculator_type), intent(in) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp), dimension(self%nmultipoles), intent(inout) :: vec_mp
    end subroutine apply_multipoles2pointcartesian

    pure module subroutine apply_pointcartesian2multipoles(self, atoms, vec_mp)
        !! Transforms the given cartesian-point-multipole-dependent array
        !! from a cartesian to a spherical representation.
        class(ScfTightBindingCalculator_type), intent(in) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp), dimension(self%nmultipoles), intent(inout) :: vec_mp
    end subroutine apply_pointcartesian2multipoles

    module subroutine get_initial_fluctuation_arrays(self, atoms, charges, &
                                                     magnetizations)
        !! Gets the initial set of subshell- or multipole-resolved charges
        !! (and, if present, magnetizations) to be used in a self-consistency
        !! cycle.
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp), dimension(:), intent(inout) :: charges
        real(dp), dimension(:), intent(inout), optional :: magnetizations
    end subroutine get_initial_fluctuation_arrays

    module function get_total_magnetization(self, atoms) result(mag_tot)
        !! Returns the total magnetization, as calculated from the
        !! mono- or multipole-expanded magnetizations.
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp) :: mag_tot
    end function get_total_magnetization

    module subroutine initialize_pointcartesian_transforms(self, atoms)
        !! Initializes the self%multipoles2pointcartesian and
        !! self%pointcartesian2multipoles transformation matrices.
        class(ScfTightBindingCalculator_type), intent(inout) :: self
        type(Atoms_type), intent(in) :: atoms
    end subroutine initialize_pointcartesian_transforms

    module subroutine normalize_monopole_fluctuation_array(self, array, which)
        !! Ensures that the sum of the given subshell-dependent array
        !! equals the imposed total charge (for which='charges') or
        !! total magnetization (for which='magnetizations'). If the deviation
        !! is considered significant, a correction is applied, spread out over
        !! the included subshells.
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        real(dp), dimension(:), intent(inout) :: array
        character(len=*), intent(in) :: which
            !! which type of array ('charges' or 'magnetizations')
    end subroutine normalize_monopole_fluctuation_array

    module subroutine normalize_multipole_fluctuation_array(self, atoms, &
                                                            array, which)
        !! Ensures that the sum of the monopoles in the given multipole-
        !! dependent array equals the imposed total charge (for which='charges')
        !! or total magnetization (for which='magnetizations'). If the deviation
        !! is considered significant, a correction is applied, spread out over
        !! the included monopoles.
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp), dimension(:), intent(inout) :: array
        character(len=*), intent(in) :: which
            !! which type of array ('charges' or 'magnetizations')
    end subroutine normalize_multipole_fluctuation_array

    ! Procedure(s) implemented in submodule tibi_scf_tightbinding_forces

    module subroutine calculate_forces(self, atoms, do_stress)
        !! Calculates the atomic forces for the given [[tibi_atoms:Atoms_type]]
        !! object, which is stored in atoms%forces.
        !!
        !! Assumes that the band structure has already been obtained through
        !! a preceding call to [[tibi_scf_tightbinding:calculate_energy]].
        implicit none
        class(ScfTightBindingCalculator_type), intent(inout) :: self
        type(Atoms_type), intent(inout) :: atoms
        logical, intent(in) :: do_stress
            !! whether to also compute the stress tensor for periodic structures
    end subroutine calculate_forces

    ! Procedure(s) implemented in submodule tibi_scf_tightbinding_ham

    module subroutine add_mulliken_shifts_to_hamiltonian(self)
        !! Adds the self-consistent corrections to the Hamiltonian
        !! for Mulliken mapping.
        implicit none
        class(ScfTightBindingCalculator_type), intent(inout) :: self
    end subroutine add_mulliken_shifts_to_hamiltonian

    module subroutine add_giese_york_shifts_to_hamiltonian(self)
        !! Adds the self-consistent corrections to the Hamiltonian
        !! for Giese-York mapping.
        implicit none
        class(ScfTightBindingCalculator_type), intent(inout) :: self
    end subroutine add_giese_york_shifts_to_hamiltonian

    module function calculate_h0_energy(self) result(eh0)
        !! Returns the expectation value sum associated with H0
        !! (i.e. \( \sum_i w_i \langle \phi_i | H^0 | \phi_i \rangle \).
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        real(dp) :: eh0
    end function calculate_h0_energy

    ! Procedure(s) implemented in submodule tibi_scf_tightbinding_restart

    module subroutine read_scf_restart_file(self, charges, magnetizations)
        !! Reads the self%scf_restart_input_file and fills in the given
        !! auxiliary charge array (and if, present, also the magnetizations).
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        real(dp), dimension(:), intent(out) :: charges
        real(dp), dimension(:), intent(out), optional :: magnetizations
    end subroutine read_scf_restart_file

    module subroutine write_scf_restart_file(self, charges, magnetizations)
        !! Writes the self%scf_restart_output_file, with one line per atom
        !! with the auxiliary charges (and, if present, magnetizations).
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        real(dp), dimension(:), intent(in) :: charges
        real(dp), dimension(:), intent(in), optional :: magnetizations
    end subroutine write_scf_restart_file

    ! Procedure(s) implemented in submodule tibi_scf_tightbinding_shift

    module pure function copy_subshell_to_orbital_array(self, atoms, vector_l) &
                                                        result(vector_lm)
        !! Duplicates the entries of an l-dependent array to
        !! an lm-dependent array.
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp), dimension(self%nsubshell), intent(in) :: vector_l
            !! output array with l-dependent values
        real(dp), dimension(self%nband) :: vector_lm
            !! output array with lm-dependent values
    end function copy_subshell_to_orbital_array

    module function get_hubbard_parameters(self, atoms) result(params)
        !! Returns the subshell-dependent (diagonal)
        !! on-site one-center U values for each atomic kind.
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp), dimension(self%basis_sets%maxnuml, atoms%nkind) :: params
    end function get_hubbard_parameters

    module function get_spin_parameters(self, atoms) result(params)
        !! Returns the subshell-dependent (diagonal and off-diagonal)
        !! on-site one-center W values for each atomic kind.
        implicit none
        class(ScfTightBindingCalculator_type), intent(in) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp), dimension(self%basis_sets%maxnuml**2, atoms%nkind) :: params
    end function get_spin_parameters
end interface


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(ScfTightBindingCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer
    integer :: i, isep, nitems

    buffer = input%fetch_keyword('aux_basis_mulliken')
    if (len_trim(buffer) > 0) then
        if (trim(buffer) == 'atom_dependent') then
            self%aux_basis_mulliken_subshell_dependent = .false.
        elseif (trim(buffer) == 'subshell_dependent') then
            self%aux_basis_mulliken_subshell_dependent = .true.
        else
            call assert(.false., &
                        'Unknown aux_basis_mulliken value:' // trim(buffer))
        end if
    end if

    buffer = input%fetch_keyword('scf_aux_mapping')
    if (len_trim(buffer) > 0) then
        self%scf_aux_mapping = trim(buffer)
        call assert(self%scf_aux_mapping == 'giese_york' .or. &
                    self%scf_aux_mapping == 'mulliken', &
                    'Unknown scf_aux_mapping value:' // self%scf_aux_mapping)
    end if

    buffer = input%fetch_keyword('scf_aux_use_delta')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_aux_use_delta
    end if

    buffer = input%fetch_keyword('scf_extrapolation')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_extrapolation
        associate (str => self%scf_extrapolation)
        call assert(trim(str) == 'use_guess' .or. trim(str) == 'use_previous', &
                    'Unknown method for SCF extrapolation: ' // trim(str))
        end associate
    end if

    buffer = input%fetch_keyword('scf_initial_guess')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_initial_guess
        associate (str => self%scf_initial_guess)
        call assert(trim(str) == 'zero' .or. trim(str) == 'restart', &
                    'Unknown method for SCF initial guess: ' // trim(str))
        end associate
    end if

    buffer = input%fetch_keyword('scf_maxiter')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_maxiter
        call assert(self%scf_maxiter > 0, 'scf_maxiter must be > 0')
    end if

    buffer = input%fetch_keyword('scf_must_converge')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_must_converge
    end if

    buffer = input%fetch_keyword('scf_restart_input_file')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_restart_input_file
        call assert(len_trim(self%scf_restart_input_file) > 0, &
                    'scf_restart_input_file is an empty string')
    end if

    buffer = input%fetch_keyword('scf_restart_output_file')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_restart_output_file
        call assert(len_trim(self%scf_restart_output_file) > 0, &
                    'scf_restart_output_file is an empty string')
    end if

    buffer = input%fetch_keyword('scf_tolerance')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_tolerance
        call assert(self%scf_tolerance > 0._dp, &
                    'scf_tolerance must be (strictly) positive')
        self%scf_tolerance = self%scf_tolerance * electronvolt
    end if

    buffer = input%fetch_keyword('scf_ukernel_expansion_offsite')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_ukernel_expansion_offsite
        call assert(self%scf_ukernel_expansion_offsite == 2, &
                    'scf_ukernel_expansion_offsite must be 2')
    end if

    buffer = input%fetch_keyword('scf_ukernel_expansion_onsite')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_ukernel_expansion_onsite
        call assert(self%scf_ukernel_expansion_onsite == 1 .or. &
                    self%scf_ukernel_expansion_onsite == 2, &
                    'scf_ukernel_expansion_onsite must be 1 or 2')
    end if

    buffer = input%fetch_keyword('scf_ukernel_off2c_model')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_ukernel_off2c_model
        associate (str => self%scf_ukernel_off2c_model)
        call assert(trim(str) == 'elstner' .or. trim(str) == 'tabulated', &
                    'Unknown scf_ukernel_off2c_model value: ' // trim(str))
        end associate
    end if

    buffer = input%fetch_keyword('scf_ukernel_on1c_model')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_ukernel_on1c_model
        associate (str => self%scf_ukernel_on1c_model)
        call assert(trim(str) == 'elstner' .or. trim(str) == 'tabulated', &
                    'Unknown scf_ukernel_on1c_model value: ' // trim(str))
        end associate
    end if

    buffer = input%fetch_keyword('scf_wkernel_expansion_offsite')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_wkernel_expansion_offsite
        call assert(self%scf_wkernel_expansion_offsite == 0 .or. &
                    self%scf_wkernel_expansion_offsite == 2, &
                    'scf_wkernel_expansion_offsite must be 0 or 2')
    end if

    buffer = input%fetch_keyword('scf_wkernel_expansion_onsite')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_wkernel_expansion_onsite
        call assert(self%scf_wkernel_expansion_onsite == 1 .or. &
                    self%scf_wkernel_expansion_onsite == 2, &
                    'scf_wkernel_expansion_onsite must be 1 or 2')
    end if

    buffer = input%fetch_keyword('scf_wkernel_on1c')
    if (len_trim(buffer) > 0) then
        if (is_master()) then
            print *, 'Warning: the "scf_wkernel_on1c" option is deprecated'
            print *, '         and will be removed in the next major version.'
        end if

        if (trim(buffer) == 'tabulated') then
            allocate(self%scf_wkernel_on1c(1))
            self%scf_wkernel_on1c(1) = trim(buffer)
        else
            nitems = count([(buffer(i:i) == '_', i = 1, len_trim(buffer))])

            call assert(nitems > 0, 'scf_wkernel_on1c must be in ' // &
                        'element1_Wss,Wsp,... element2_Wss,Wsp,... format')
            allocate(self%scf_wkernel_on1c(nitems))

            do i = 1, nitems
                buffer = adjustl(buffer)
                isep = index(buffer, ' ')
                if (isep == 0) isep = len_trim(buffer)
                self%scf_wkernel_on1c(i) = buffer(1:isep)
                buffer = buffer(isep:)
            end do
        end if
    end if

    buffer = input%fetch_keyword('scf_write_restart_file')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_write_restart_file
    end if

    buffer = input%fetch_keyword('scf_write_verbose')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%scf_write_verbose
    end if

    ! Additional checks

    if (self%scf_ukernel_expansion_offsite >= 2) then
        if (self%scf_ukernel_off2c_model == 'tabulated') then
            if (self%scf_aux_mapping == 'mulliken') then
                call assert(self%aux_basis_mulliken_subshell_dependent, &
                            'setting scf_ukernel_off2c_model = "tabulated" ' // &
                            'with scf_aux_basis = "mulliken" requires ' // &
                            'aux_basis_mulliken = "subshell_dependent"')
            end if
        end if
    end if

    if (self%scf_ukernel_expansion_onsite >= 2) then
        if (self%scf_aux_mapping == 'mulliken') then
            call assert(self%aux_basis_mulliken_subshell_dependent, &
                        'setting scf_ukernel_expansion_onsite >= 2 ' // &
                        'with scf_aux_basis = "mulliken" requires ' // &
                        'aux_basis_mulliken = "subshell_dependent"')
        end if
    end if

    if (self%occupations%spin_polarized) then
        if (self%scf_wkernel_expansion_offsite >= 2 .or. &
            self%scf_wkernel_expansion_onsite >= 2) then
            if (self%scf_aux_mapping == 'mulliken') then
                call assert(self%aux_basis_mulliken_subshell_dependent, &
                            'setting scf_wkernel_expansion_off/onsite >= 2 ' //&
                            'with scf_aux_basis = "mulliken" requires ' // &
                            'aux_basis_mulliken = "subshell_dependent"')
            end if
        end if
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input, atoms)
    !! First initializes the parent class and then sets up the SCF specific
    !! attributes such as the Coulomb interaction calculator and the mixer.
    class(ScfTightBindingCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms

    character(len=:), allocatable :: distribution
    logical :: include_off2c, include_on1c, include_on2c

    ! Initialize the parent calculator and H0
    call self%TightBindingCalculator_type%initialize(input, atoms)

    if (self%complex_wfn) then
        allocate(self%hamiltonian0_complex, mold=self%hamiltonian_complex)
    else
        allocate(self%hamiltonian0_real, mold=self%hamiltonian_real)
    end if

    ! Register SCF-specific timers
    call initialize_scf_timers()

    ! Set own simple attributes, perform charge-fluctuation-specific
    ! initializations, and set up the potential mixer
    call self%fetch_input_keywords(input)

    if (self%scf_aux_mapping == 'giese_york') then
        if (.not. allocated(self%giese_york_kernels)) then
            call self%initialize_multipoles(atoms)
        end if

        call self%initialize_pointcartesian_transforms(atoms)
        allocate(self%locpot_mp(self%nmultipoles))

        if (self%scf_aux_use_delta) then
            allocate(self%coulomb_calc)
            call self%coulomb_calc%initialize(input, atoms, self%nmultipoles, &
                                              self%indices_mp, 'delta', &
                                              use_multipoles=.true., &
                                              ilmax=self%multipoles_ilmax)
        end if
        call self%mixer%initialize(input, vector_length=self%nmultipoles)
    elseif (self%scf_aux_mapping == 'mulliken') then
        allocate(self%coulomb_calc)
        allocate(self%locpot_l(self%nsubshell))
        allocate(self%locpot_lm(self%nband))
        distribution = 'exponential'

        if (self%scf_ukernel_off2c_model == 'elstner') then
            call self%coulomb_calc%initialize( &
                                input, atoms, self%nsubshell, self%indices_l, &
                                distribution, use_multipoles=.false., &
                                parameters=self%get_hubbard_parameters(atoms))
        elseif (self%scf_ukernel_off2c_model == 'tabulated') then
            call self%coulomb_calc%initialize(input, atoms, self%nsubshell, &
                                              self%indices_l, 'delta', &
                                              use_multipoles=.false.)
        end if
        call self%mixer%initialize(input, vector_length=self%nsubshell)
    end if

    include_off2c = (self%scf_ukernel_expansion_offsite >= 2) .and. &
                    (self%scf_ukernel_off2c_model /= 'elstner')
    include_on1c = self%scf_ukernel_expansion_onsite >= 1
    include_on2c = self%scf_ukernel_expansion_onsite >= 2

    if (self%scf_aux_mapping == 'giese_york') then
        call self%chg_calc%initialize(atoms, self%multipoles_maxnmp, &
                            self%nmultipoles, self%indices_mp, &
                            self%h0s%tbpar_dir, self%basis_sets%basis_set, &
                            include_off2c, include_on1c, include_on2c, &
                            use_multipoles=.true., variant='U', &
                            ilmax=self%multipoles_ilmax, &
                            use_delta=self%scf_aux_use_delta, &
                            nzeta=self%multipoles_nzeta)
    elseif (self%scf_aux_mapping == 'mulliken') then
        associate(subshelldep => self%aux_basis_mulliken_subshell_dependent)

        select case(self%scf_ukernel_on1c_model)
        case('tabulated')
            call self%chg_calc%initialize(atoms, self%basis_sets%maxnuml, &
                                self%nsubshell, self%indices_l, &
                                self%h0s%tbpar_dir, &
                                self%basis_sets%basis_set, &
                                include_off2c, include_on1c, include_on2c, &
                                use_multipoles=.false., variant='U', &
                                subshell_dependent=subshelldep)
        case('elstner')
            call self%chg_calc%initialize(atoms, self%basis_sets%maxnuml, &
                                self%nsubshell, self%indices_l, &
                                self%h0s%tbpar_dir, &
                                self%basis_sets%basis_set, &
                                include_off2c, include_on1c, include_on2c, &
                                use_multipoles=.false., variant='U', &
                                subshell_dependent=subshelldep, &
                                distribution=distribution, &
                                parameters=self%get_hubbard_parameters(atoms), &
                                has_offdiagonal=.false.)
        end select

        end associate
    end if

    if (is_master() .and. allocated(self%coulomb_calc)) then
        call self%coulomb_calc%print_description()
    end if

    ! Magnetization-fluctuation-specific initializations
    if (self%occupations%spin_polarized) then
        allocate(self%mixer_mag)

        if (self%scf_aux_mapping == 'giese_york') then
            allocate(self%magpot_mp(self%nmultipoles))
            call self%mixer_mag%initialize(input, &
                                           vector_length=self%nmultipoles)
        elseif (self%scf_aux_mapping == 'mulliken') then
            allocate(self%magpot_l(self%nsubshell))
            allocate(self%magpot_lm(self%nband))
            call self%mixer_mag%initialize(input, vector_length=self%nsubshell)
        end if

        include_off2c = self%scf_wkernel_expansion_offsite >= 2
        include_on1c = self%scf_wkernel_expansion_onsite >= 1
        include_on2c = self%scf_wkernel_expansion_onsite >= 2

        allocate(self%mag_calc)

        if (self%scf_aux_mapping == 'giese_york') then
            call self%mag_calc%initialize(atoms, self%multipoles_maxnmp, &
                                self%nmultipoles, self%indices_mp, &
                                self%h0s%tbpar_dir, &
                                self%basis_sets%basis_set, &
                                include_off2c, include_on1c, include_on2c, &
                                use_multipoles=.true., variant='W', &
                                ilmax=self%multipoles_ilmax, &
                                use_delta=.false., nzeta=self%multipoles_nzeta)
        elseif (self%scf_aux_mapping == 'mulliken') then
            associate(subshelldep => self%aux_basis_mulliken_subshell_dependent)
            call assert(allocated(self%scf_wkernel_on1c), 'atomic spin ' // &
                        'constants are missing in spin-polarized calculation')

            select case(self%scf_wkernel_on1c(1))
            case('tabulated')
                call self%mag_calc%initialize(atoms, self%basis_sets%maxnuml, &
                                self%nsubshell, self%indices_l, &
                                self%h0s%tbpar_dir, &
                                self%basis_sets%basis_set, &
                                include_off2c, include_on1c, include_on2c, &
                                use_multipoles=.false., variant='W', &
                                subshell_dependent=subshelldep)
            case default
                call self%mag_calc%initialize(atoms, self%basis_sets%maxnuml, &
                                self%nsubshell, self%indices_l, &
                                self%h0s%tbpar_dir, &
                                self%basis_sets%basis_set, &
                                include_off2c, include_on1c, include_on2c, &
                                use_multipoles=.false., variant='W', &
                                subshell_dependent=subshelldep, &
                                distribution='n/a', &
                                parameters=self%get_spin_parameters(atoms), &
                                has_offdiagonal=.true.)
            end select

            end associate
        end if
    end if
end subroutine initialize


subroutine calculate_energy(self, atoms)
    !! Calculates the (total) band energy for the given
    !! [[tibi_atoms:Atoms_type]] object, which is stored in atoms%energy.
    class(ScfTightBindingCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms

    character(len=160) :: header
    logical :: is_converged
    integer :: iter
    real(dp) :: ediff, mad, nelect
    real(dp) :: band_energy, coulomb_energy, elec_energy, h0_energy, scf_energy
    real(dp) :: chg_energy, mag_energy
    real(dp), dimension(:), allocatable :: chg, mag

    call self%update_neighborlists(atoms)

    self%need_mapping_update = .true.

    if (self%scf_aux_mapping == 'giese_york') then
        call self%build_mapping(atoms)
    end if

    call task_timer%start_timer('scf_misc')

    if (self%scf_aux_mapping == 'giese_york') then
        allocate(chg(self%nmultipoles))
    elseif (self%scf_aux_mapping == 'mulliken') then
        allocate(chg(self%nsubshell))
    end if

    if (self%occupations%spin_polarized) then
        if (self%scf_aux_mapping == 'giese_york') then
            allocate(mag(self%nmultipoles))
            call self%get_initial_fluctuation_arrays(atoms, &
                                                     self%giese_york_chg, &
                                                     self%giese_york_mag)
            chg = self%giese_york_chg
            call self%apply_multipoles2pointcartesian(atoms, chg)
            mag = self%giese_york_mag
        elseif (self%scf_aux_mapping == 'mulliken') then
            allocate(mag(self%nsubshell))
            call self%get_initial_fluctuation_arrays(atoms, chg, mag)
        end if
    else
        if (self%scf_aux_mapping == 'giese_york') then
            call self%get_initial_fluctuation_arrays(atoms, self%giese_york_chg)
            chg = self%giese_york_chg
            call self%apply_multipoles2pointcartesian(atoms, chg)
        elseif (self%scf_aux_mapping == 'mulliken') then
            call self%get_initial_fluctuation_arrays(atoms, chg)
        end if
    end if

    nelect = self%get_nr_of_electrons(atoms)

    call self%mixer%reset()
    if (self%occupations%spin_polarized) then
        call self%mixer_mag%reset()
    end if
    call task_timer%stop_timer('scf_misc')

    ! Update any geometry-dependent parts of the Coulomb calculator,
    ! then calculate the electrostatic (Coulomb) contribution,
    ! which will also initialize the local potentials
    if (allocated(self%coulomb_calc)) then
        call self%coulomb_calc%update_geometry(atoms)
        call self%coulomb_calc%update_moments(chg)
        call self%coulomb_calc%calculate_energy(atoms)

        if (self%scf_aux_mapping == 'giese_york') then
            call self%coulomb_calc%get_local_potentials(self%locpot_mp)
            call self%apply_pointcartesian2multipoles(atoms, self%locpot_mp)
        elseif (self%scf_aux_mapping == 'mulliken') then
            call self%coulomb_calc%get_local_potentials(self%locpot_l)
        end if
    else
        if (self%scf_aux_mapping == 'giese_york') then
            self%locpot_mp = 0._dp
        elseif (self%scf_aux_mapping == 'mulliken') then
            self%locpot_l = 0._dp
        end if
    end if

    ! Same for other charge fluctuation (two-center) interactions
    call self%chg_calc%update_kernel(atoms)

    if (self%scf_aux_mapping == 'giese_york') then
        call self%chg_calc%update_fluctuations(self%giese_york_chg)
        call self%chg_calc%calculate_energy(atoms)
        call self%chg_calc%add_local_potentials(self%locpot_mp)
    elseif (self%scf_aux_mapping == 'mulliken') then
        call self%chg_calc%update_fluctuations(chg)
        call self%chg_calc%calculate_energy(atoms)
        call self%chg_calc%add_local_potentials(self%locpot_l)
    end if

    if (self%scf_write_verbose) then
        call self%chg_calc%write_kernel()
    end if

    ! Same for other magnetization fluctuation (two-center) interactions
    if (self%occupations%spin_polarized) then
        if (self%scf_aux_mapping == 'giese_york') then
            self%magpot_mp = 0._dp
        elseif (self%scf_aux_mapping == 'mulliken') then
            self%magpot_l = 0._dp
        end if

        call self%mag_calc%update_kernel(atoms)
        call self%mag_calc%update_fluctuations(mag)
        call self%mag_calc%calculate_energy(atoms)

        if (self%scf_aux_mapping == 'giese_york') then
            call self%mag_calc%add_local_potentials(self%magpot_mp)
        elseif (self%scf_aux_mapping == 'mulliken') then
            call self%mag_calc%add_local_potentials(self%magpot_l)
        end if

        if (self%scf_write_verbose) then
            call self%mag_calc%write_kernel()
        end if
    end if

    if (self%scf_aux_mapping == 'mulliken') then
        self%locpot_lm = self%copy_subshell_to_orbital_array(atoms, &
                                                             self%locpot_l)
        if (self%occupations%spin_polarized) then
            self%magpot_lm = self%copy_subshell_to_orbital_array(atoms, &
                                                                 self%magpot_l)
        end if
    end if

    ! Calculate the Hamiltonian and the overlap matrix
    call self%build_H0_and_S(atoms)

    if (self%complex_wfn) then
        self%hamiltonian0_complex = self%hamiltonian_complex
    else
        self%hamiltonian0_real = self%hamiltonian_real
     end if

    if (self%scf_aux_mapping == 'giese_york') then
        call self%add_giese_york_shifts_to_hamiltonian()
    elseif (self%scf_aux_mapping == 'mulliken') then
        call self%add_mulliken_shifts_to_hamiltonian()
    end if

    call task_timer%start_timer('scf_misc')
    if (self%scf_aux_mapping == 'giese_york') then
        call self%mixer%add_input_vector(self%locpot_mp)
    elseif (self%scf_aux_mapping == 'mulliken') then
        call self%mixer%add_input_vector(self%locpot_l)
    end if

    if (self%occupations%spin_polarized) then
        if (self%scf_aux_mapping == 'giese_york') then
            call self%mixer_mag%add_input_vector(self%magpot_mp)
        elseif (self%scf_aux_mapping == 'mulliken') then
            call self%mixer_mag%add_input_vector(self%magpot_l)
        end if
    end if

    call self%process_anterior_output_requests()

    header = 'SCF_iter            E_electronic      delta_E    max(|dV|)'
    header = trim(header) // new_line('a') // ' ' // repeat('-', ncopies=58)
    if (is_master()) then
        print '(a)', ' SCF tight-binding electronic structure calculation'
        print *, trim(header)
    end if
    call task_timer%stop_timer('scf_misc')

    elec_energy = 0._dp
    ! Iterate to charge self-consistency
    scf_loop: do iter = 1, self%scf_maxiter
        call self%solve_band_structure(is_first=(iter==1))
        self%need_densmat_update = .true.
        self%need_ew_densmat_update = .true.
        self%need_densmat_diff_update = .true.
        self%need_giese_york_chg_update = .true.
        self%need_giese_york_mag_update = .true.
        self%need_mulliken_mag_update = .true.
        self%need_mulliken_pop_update = .true.

        ! Restore H0
        if (self%complex_wfn) then
            self%hamiltonian_complex = self%hamiltonian0_complex
        else
            self%hamiltonian_real = self%hamiltonian0_real
        end if

        call task_timer%start_timer('scf_misc')
        call self%occupations%update_occupations_and_fermi_levels( &
                                                     self%eigenvalues, nelect)
        call task_timer%stop_timer('scf_misc')

        ! Get the current charge & spin distributions
        if (self%scf_aux_mapping == 'giese_york') then
            call self%calculate_giese_york_chg(atoms)
            chg = self%giese_york_chg
            call self%apply_multipoles2pointcartesian(atoms, chg)
        elseif (self%scf_aux_mapping == 'mulliken') then
            call self%calculate_mulliken_orbital_pop()
            chg = self%calculate_mulliken_subshell_chg(atoms)
        end if

        if (allocated(self%coulomb_calc)) then
            call self%coulomb_calc%update_moments(chg)
        end if

        if (self%scf_aux_mapping == 'giese_york') then
            call self%chg_calc%update_fluctuations(self%giese_york_chg)
        elseif (self%scf_aux_mapping == 'mulliken') then
            call self%chg_calc%update_fluctuations(chg)
        end if

        if (self%occupations%spin_polarized) then
            if (self%scf_aux_mapping == 'giese_york') then
                call self%calculate_giese_york_mag(atoms)
                mag = self%giese_york_mag
            elseif (self%scf_aux_mapping == 'mulliken') then
                call self%calculate_mulliken_orbital_mag()
                mag = self%calculate_mulliken_subshell_mag(atoms)
            end if

            call self%mag_calc%update_fluctuations(mag)
        end if

        ! Calculate the electrostatic (Coulomb) contribution,
        ! which will also provide the local electrostatic potentials
        if (allocated(self%coulomb_calc)) then
            call self%coulomb_calc%calculate_energy(atoms)
            coulomb_energy = atoms%energy
            if (self%scf_aux_mapping == 'giese_york') then
                call self%coulomb_calc%get_local_potentials(self%locpot_mp)
                call self%apply_pointcartesian2multipoles(atoms, self%locpot_mp)
            elseif (self%scf_aux_mapping == 'mulliken') then
                call self%coulomb_calc%get_local_potentials(self%locpot_l)
            end if
        else
            if (self%scf_aux_mapping == 'giese_york') then
                self%locpot_mp = 0._dp
            elseif (self%scf_aux_mapping == 'mulliken') then
                self%locpot_l = 0._dp
            end if
            coulomb_energy = 0._dp
        end if

        ! Add the other (non-Coulomb) contributions
        call self%chg_calc%calculate_energy(atoms)
        chg_energy = atoms%energy
        if (self%scf_aux_mapping == 'giese_york') then
            call self%chg_calc%add_local_potentials(self%locpot_mp)
        elseif (self%scf_aux_mapping == 'mulliken') then
            call self%chg_calc%add_local_potentials(self%locpot_l)
        end if

        if (self%occupations%spin_polarized) then
            if (self%scf_aux_mapping == 'giese_york') then
                self%magpot_mp = 0._dp
            elseif (self%scf_aux_mapping == 'mulliken') then
                self%magpot_l = 0._dp
            end if

            call self%mag_calc%calculate_energy(atoms)
            mag_energy = atoms%energy
            if (self%scf_aux_mapping == 'giese_york') then
                call self%mag_calc%add_local_potentials(self%magpot_mp)
            elseif (self%scf_aux_mapping == 'mulliken') then
                call self%mag_calc%add_local_potentials(self%magpot_l)
            end if
        end if

        call task_timer%start_timer('scf_misc')

        ! Mixing
        if (self%scf_aux_mapping == 'giese_york') then
            call self%mixer%add_output_vector(self%locpot_mp)
        elseif (self%scf_aux_mapping == 'mulliken') then
            call self%mixer%add_output_vector(self%locpot_l)
            self%locpot_lm = self%copy_subshell_to_orbital_array(atoms, &
                                                                 self%locpot_l)
        end if

        if (self%occupations%spin_polarized) then
            if (self%scf_aux_mapping == 'giese_york') then
                call self%mixer_mag%add_output_vector(self%magpot_mp)
            elseif (self%scf_aux_mapping == 'mulliken') then
                call self%mixer_mag%add_output_vector(self%magpot_l)
                self%magpot_lm = self%copy_subshell_to_orbital_array(atoms, &
                                                                  self%magpot_l)
            end if
        end if

        ! Calculate remaining energy terms
        scf_energy = coulomb_energy + chg_energy
        if (self%occupations%spin_polarized) then
            scf_energy = scf_energy + mag_energy
        end if

        h0_energy = self%calculate_h0_energy()

        ediff = elec_energy - (h0_energy + scf_energy)
        elec_energy = h0_energy + scf_energy

        ! Calculate deviation wrt mixed quantity of the previous iteration
        mad = self%mixer%get_max_abs_diff()
        if (self%occupations%spin_polarized) then
            mad = max(mad, self%mixer_mag%get_max_abs_diff())
        end if

        ! Print out convergence information
        if (is_master()) then
            print '(" iter: ", (i0.4), "    ", (f18.10), 2(es13.3))', &
                  iter, elec_energy, ediff, mad
        end if

        ! If there will be a next iteration, mix in the previous potential(s)
        is_converged = mad < self%scf_tolerance
        if (is_converged .or. iter == self%scf_maxiter) then
            exit scf_loop
        else
            if (self%scf_aux_mapping == 'giese_york') then
                call self%mixer%get_next_vector(self%locpot_mp)
                call self%mixer%add_input_vector(self%locpot_mp)
            elseif (self%scf_aux_mapping == 'mulliken') then
                call self%mixer%get_next_vector(self%locpot_l)
                call self%mixer%add_input_vector(self%locpot_l)
                self%locpot_lm = self%copy_subshell_to_orbital_array(atoms, &
                                                                  self%locpot_l)
            end if
            if (self%occupations%spin_polarized) then
                if (self%scf_aux_mapping == 'giese_york') then
                    call self%mixer_mag%get_next_vector(self%magpot_mp)
                    call self%mixer_mag%add_input_vector(self%magpot_mp)
                elseif (self%scf_aux_mapping == 'mulliken') then
                    call self%mixer_mag%get_next_vector(self%magpot_l)
                    call self%mixer_mag%add_input_vector(self%magpot_l)
                    self%magpot_lm = self%copy_subshell_to_orbital_array( &
                                                        atoms, self%magpot_l)
                end if
            end if

            call task_timer%stop_timer('scf_misc')

            if (self%scf_aux_mapping == 'giese_york') then
                call self%add_giese_york_shifts_to_hamiltonian()
            elseif (self%scf_aux_mapping == 'mulliken') then
                call self%add_mulliken_shifts_to_hamiltonian()
            end if
        end if
    end do scf_loop

    if (is_master()) then
        if (is_converged) then
            print '(" SCF cycle converged in ", (i0), " iterations")', iter
        elseif (self%scf_must_converge) then
            call assert(.false., 'SCF cycle did not converge!')
        else
            print *, 'WARNING: SCF cycle did not converge!'
        end if
        print *
    end if

    atoms%energy = elec_energy
    band_energy = self%occupations%integrate_over_brillouin_zone( &
                                                  self%eigenvalues)
    atoms%entropic_energy = self%calculate_band_entropic_energy()

    if (is_master()) then
        call self%print_bandstructure_info(nelect)

        print '(" Band energy [Ha] = ", f0.8)', band_energy
        print '(" Band free energy [Ha] = ", f0.7)', &
              band_energy + atoms%entropic_energy
        print '(" H0 energy [Ha] = ", f0.8)', h0_energy
        print '(" Total charge-fluctuation energy [Ha] = ", f0.8)', &
              coulomb_energy + chg_energy

        if (self%occupations%spin_polarized) then
            print '(" Total spin-fluctuation energy [Ha] = ", f0.8)', &
                  mag_energy
            print '(" Total magnetization [-|e|] = ", f0.4)', &
                  self%get_total_magnetization(atoms)
        end if

        print '(" Electronic energy [Ha] = ", f0.8)', elec_energy
        print '(" Electronic free energy [Ha] = ", f0.8)', &
              elec_energy + atoms%entropic_energy
        print *

        if (self%scf_write_restart_file) then
            if (self%occupations%spin_polarized) then
                if (self%scf_aux_mapping == 'giese_york') then
                    call self%write_scf_restart_file(self%giese_york_chg, &
                                                     self%giese_york_mag)
                elseif (self%scf_aux_mapping == 'mulliken') then
                    call self%write_scf_restart_file(chg, mag)
                end if
            else
                if (self%scf_aux_mapping == 'giese_york') then
                    call self%write_scf_restart_file(self%giese_york_chg)
                elseif (self%scf_aux_mapping == 'mulliken') then
                    call self%write_scf_restart_file(chg)
                end if
            end if
        end if
    end if

    call task_timer%stop_timer('scf_misc')
    call self%process_posterior_output_requests(atoms)
end subroutine calculate_energy


subroutine initialize_scf_timers
    !! Adds the timers from this module's routines to the global timer.
    call task_timer%add_timer('scf_forces')
    call task_timer%add_timer('scf_hamiltonian')
    call task_timer%add_timer('scf_misc')
    call task_timer%add_timer('scf_on1c')
end subroutine initialize_scf_timers

end module tibi_scf_tightbinding
