!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_constants
!! Module for precision kinds and unit conversion parameters

implicit none

integer, parameter :: dp = kind(0.d0)
    !! double precision kind parameter
real(dp), parameter :: angstrom = 1.8897261258369282_dp
    !! Angstrom to a0 (Bohr radius) conversion parameter
real(dp), parameter :: electronvolt = 0.03674932247495664_dp
    !! eV to Hartree conversion parameter
real(dp), parameter :: pi = 3.1415926535897932384626433832795_dp
    !! \( \pi \)

end module tibi_constants
