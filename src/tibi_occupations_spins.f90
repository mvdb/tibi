!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_occupations) tibi_occupations_spins
!! Submodule for spin channel related aspects of [[tibi_occupations]]

implicit none


contains


module subroutine set_spin_weights(self)
    !! Calculates the weights of the spin channels for the
    !! Brillouin zone integrations.
    class(Occupations_type), intent(inout) :: self

    if (self%spin_polarized) then
        self%spin_weights = [1, 1]
    else
        self%spin_weights = [2, 0]
    end if
end subroutine set_spin_weights


module subroutine print_spin_description(self)
    !! Prints a description of the spin channels.
    class(Occupations_type), intent(in) :: self

    print '(a)', ' Spin description:'
    print '("   Number of spins = ", (i0))', self%nspin
    print *
end subroutine print_spin_description

end submodule tibi_occupations_spins
