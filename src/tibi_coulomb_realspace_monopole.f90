!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_coulomb_realspace_monopole
!! Module for Coulomb interactions in real space for monopoles

use tibi_atoms, only: Atoms_type
use tibi_blas, only: ddot, &
                     dsymv
use tibi_constants, only: dp, &
                          pi
use tibi_coulomb_realspace, only: eval_switchfunction_delta_gaussian, &
                                  RealSpaceCoulombCalculator_type
use tibi_utils_prog, only: assert
implicit none


type, extends(RealSpaceCoulombCalculator_type) :: &
              RealSpaceMonopoleCalculator_type
    !! Calculator for real-space Coulomb interactions between monopoles
    real(dp), dimension(:), allocatable :: charges
        !! array of the charges on each atom (possibly more than one)
    integer, dimension(:, :), allocatable :: indices
        !! start and end indices of the charges on each atom
    real(dp), dimension(:, :), allocatable :: kernel
        !! the Coulomb 'kernel'
    real(dp), dimension(:), allocatable :: locpot
        !! array with the real space contributions to the
        !! electrostatic potential for every charge
    real(dp), dimension(:), allocatable :: parameters
        !! array with a charge distribution parameter for every charge
        !! (e.g. decay constants for exponential distributions)
    contains
    procedure :: determine_cutoff_radii
    procedure :: initialize
    procedure :: initialize_charge_parameters
    procedure :: add_local_potentials
    procedure :: build_kernel
    procedure :: calculate_energy
    procedure :: calculate_local_potentials
    procedure :: evaluate_pair
    procedure :: update_geometry
    procedure :: update_moments
end type RealSpaceMonopoleCalculator_type


contains


function determine_cutoff_radii(self, atoms, tolerance) result(rcuts)
    !! Returns the cutoff radii to be used in the neighbor list builds
    !! for periodic structures.
    !!
    !! @NOTE In periodic structures, the cutoff radius in the case of point
    !! charges will depend on the Ewald screening parameter.
    !! For other charge distributions, also the 'second' switching function
    !! needs to be taken into account, meaning that the interactions at the
    !! cutoff radii must be sufficiently point-like to get converged results.
    !! If an Ewald screening parameter is explicitly provided in such cases,
    !! the cutoff radii may still be determined by this 'second' switching
    !! function, and not by the Ewald parameter.
    class(RealSpaceMonopoleCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), intent(in) :: tolerance
        !! accuracy threshold
    real(dp), dimension(atoms%nkind, atoms%nkind) :: rcuts

    integer :: iat, ik, jk, istart, iend
    real(dp), parameter :: dr = 0.1_dp
    real(dp) :: gam, rcut, rcut_delta, s
    real(dp), dimension(atoms%nkind) :: minpar

    call assert(atoms%is_periodic, 'determine_cutoff_radii should only ' // &
                'be called for periodic structures')

    if (self%ewald_alpha > epsilon(1._dp)) then
        rcut_delta = self%determine_cutoff_radius_delta(tolerance)
    else
        rcut_delta = 0._dp
    end if

    if (rcut_delta < epsilon(1._dp)) then
        call assert(self%distribution /= 'delta', &
                    'Positive Ewald parameter needed for point charges')
    end if

    select case(self%distribution)
    case('delta')
        rcuts(:, :) = rcut_delta
    case('exponential')
        ! Determine cutoff radii based on the most diffuse charge
        ! distributions (i.e. with the smallest decay constants)
        minpar(:) = huge(1._dp)
        do iat = 1, atoms%natom
            ik = atoms%kinds(iat)
            istart = self%indices(1, iat)
            iend = self%indices(2, iat)
            minpar(ik) = min(minpar(ik), &
                             minval(self%parameters(istart:iend)))
        end do

        do ik = 1, atoms%nkind
             do jk = 1, ik
                rcut = 0._dp
                gam = 2._dp * tolerance

                do while (abs(gam) > tolerance)
                    rcut = rcut + dr
                    s = eval_switchfunction_exponentials(rcut, minpar(ik), &
                                                         minpar(jk))
                    gam = (1._dp - s) / rcut
                end do

                rcut = maxval([rcut, rcut_delta])
                rcuts(ik, jk) = rcut
                rcuts(jk, ik) = rcut
            end do
        end do
    end select
end function determine_cutoff_radii


subroutine initialize(self, atoms, nchg, indices, distribution, parameters, &
                      alpha, tolerance)
    !! Initialization of the real-space Ewald sum calculator.
    class(RealSpaceMonopoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    integer, intent(in) :: nchg
        !! total number of charges
    integer, dimension(2, atoms%natom), intent(in) :: indices
        !! start and end indices of the charges on each atom
    character(len=*), intent(in) :: distribution
        !! functional form of the charge distributions
    real(dp), dimension(:, :), intent(in), optional :: parameters
        !! parameters related to the charge distributions for each atomic kind
        !! (required for charge distributions other than 'delta')
    real(dp), intent(in) :: alpha
        !! Ewald screening parameter
    real(dp), intent(in) :: tolerance
        !! accuracy threshold

    self%indices = indices

    ! Set empty description, to exclude results from output
    self%description = ''

    ! Set charge distribution type and parameters
    self%distribution = trim(distribution)

    select case(self%distribution)
    case('delta')
        call assert(.not. present(parameters), 'Point charges are not ' // &
                    'suppposed to need charge distribution parameters')
    case default
        call assert(present(parameters), 'Charge distribution parameters' // &
                    'needed for non-delta distributions')
        call self%initialize_charge_parameters(atoms, nchg, parameters)
    end select

    if (atoms%is_periodic .and. self%distribution == 'delta' .and. &
        alpha < epsilon(1._dp)) then
        self%ewald_alpha = 0.5_dp
    else
        self%ewald_alpha = alpha
    end if

    call self%initialize_neighborlist(atoms, tolerance)

    ! Determine an appropriate value for the Ewald parameter (if undefined)
    if (atoms%is_periodic .and. self%ewald_alpha < epsilon(1._dp)) then
        call self%determine_ewald_alpha(tolerance)
    end if

    allocate(self%charges(nchg))
    allocate(self%locpot(nchg))
    allocate(self%kernel(nchg, nchg))
end subroutine initialize


subroutine initialize_charge_parameters(self, atoms, nchg, parameters)
    !! Initializes self%parameters with one parameter per charge
    !! characterizing the charge distribution (e.g. the decay constant
    !! for exponential distributions).
    class(RealSpaceMonopoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    integer, intent(in) :: nchg
        !! total number of charges
    real(dp), dimension(:, :), intent(in) :: parameters
        !! parameters related to the charge distributions for each atomic kind

    integer :: iat, ik, istart, iend

    allocate(self%parameters(nchg))

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)
        istart = self%indices(1, iat)
        iend = self%indices(2, iat)

        select case(self%distribution)
        case('exponential')
            call assert(all(parameters(1:iend-istart+1, ik) > epsilon(1._dp)), &
                    'Parameters for "exponential" distribution must be > 0')

            self%parameters(istart:iend) = &
                get_exponential_decay_constant(parameters(1:iend-istart+1, ik))
        case default
            call assert(.false., 'Unknown charge distribution: ' // &
                        self%distribution)
        end select
    end do
end subroutine initialize_charge_parameters


pure subroutine add_local_potentials(self, locpot)
    !! Adds, for each charge moment, the energy derivative w.r.t.
    !! that charge moment (e.g. the electrostatic potential in case
    !! of a charge monopole).
    class(RealSpaceMonopoleCalculator_type), intent(in) :: self
    real(dp), dimension(:), intent(inout) :: locpot

    locpot = locpot + self%locpot
end subroutine add_local_potentials


subroutine build_kernel(self, atoms)
    !! Build the Coulomb 'kernel', i.e. the part that does not depend
    !! on the atomic charges and can hence be reused throughout a self-
    !! consistency cycle.
    !!
    !! @NOTE The kernel does depend on the atomic positions, and hence
    !! needs to be rebuilt when the atoms have moved.
    !!
    !! @NOTE Only the lower triangle of the kernel is built.
    class(RealSpaceMonopoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: iat, jat, ichg, jchg, ik, jk, ineigh, ineighlist
    real(dp) :: gam, s1, s2, r

    self%kernel(:, :) = 0._dp

    do iat = 1, atoms%natom
        ik = atoms%kinds(iat)

        do ineigh = 1, self%nl%neighbors(iat)
            ineighlist = self%nl%start_indices(iat) + ineigh - 1

            jat = self%nl%neighbor_indices(ineighlist)
            jk = atoms%kinds(jat)

            r = self%nl%r(ineighlist)
            if (r > self%nl%rcuts(ik, jk) .or. r < self%nl%rsame) cycle

            ! Get the real-space part of the coulomb interaction kernel
            ! gamma(r; param) = S(r; param) / r
            !                 = 1/r - (1-S(r; param)) / r
            !
            ! Without Ewald summation (i.e. a direct sum), that's it.
            !
            ! With Ewald summation, the real-space part amounts to
            !   (1-erf(ewald_alpha*r))/r - (1-S(r; param)) / r
            if (self%ewald_alpha > epsilon(1._dp)) then
                s1 = eval_switchfunction_delta_gaussian(r, self%ewald_alpha)
            else
                s1 = 0._dp
            end if

            select case(self%distribution)
            case('delta')
                gam = (1._dp - s1) / r
                do ichg = self%indices(1, iat), self%indices(2, iat)
                    do jchg = self%indices(1, jat), self%indices(2, jat)
                        self%kernel(ichg, jchg) = self%kernel(ichg, jchg) + gam
                    end do
                end do
            case('exponential')
                do ichg = self%indices(1, iat), self%indices(2, iat)
                    do jchg = self%indices(1, jat), self%indices(2, jat)
                        s2 = eval_switchfunction_exponentials(r, &
                                                self%parameters(ichg), &
                                                self%parameters(jchg))
                        gam = ((1._dp - s1) - (1._dp - s2)) / r
                        self%kernel(ichg, jchg) = self%kernel(ichg, jchg) + gam
                    end do
                end do
            end select
        end do
    end do
end subroutine build_kernel


subroutine calculate_energy(self, atoms)
    !! Calculates the electrostatic energy for atom-centered charge
    !! moments associated with the given [[tibi_atoms:Atoms_type]]
    !! object, which is stored in atoms%energy.
    class(RealSpaceMonopoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms

    atoms%energy = 0.5_dp * ddot(size(self%locpot), self%locpot(:), 1, &
                                 self%charges(:), 1)
end subroutine calculate_energy


subroutine calculate_local_potentials(self)
    !! Calculates the energy derivatives w.r.t. the charge moments.
    !!
    !! Assumes that self%kernel and self%charges are up-to-date.
    class(RealSpaceMonopoleCalculator_type), intent(inout) :: self

    integer :: nchg

    nchg = size(self%charges)

    call dsymv('L', nchg, 1._dp, self%kernel(:, :), nchg, &
               self%charges(:), 1, 0._dp, self%locpot(:), 1)
end subroutine calculate_local_potentials


elemental function evaluate_pair(self, iat, jat, r, der) result(fval)
    !! Evaluates the pairwise interaction between two charge distributions.
    !!
    !! @NOTE The self%nl neighborlist must not include self-interaction.
    !!
    !! @NOTE This function is supposed to be used only in force calculations.
    class(RealSpaceMonopoleCalculator_type), intent(in) :: self
    integer, intent(in) :: iat, jat
        !! the atomic indices
    real(dp), intent(in) :: r
        !! the interatomic distance
    integer, intent(in) :: der
        !! the derivative index (only der=1 is allowed here)
    real(dp) :: fval

    integer :: ichg, jchg
    real(dp) :: dgam_dr, ds1_dr, ds2_dr, s1, s2

    fval = 0._dp

    if (self%ewald_alpha > epsilon(1._dp)) then
        s1 = eval_switchfunction_delta_gaussian(r, self%ewald_alpha)
        ds1_dr = eval_switchfunction_delta_gaussian_derivative(r, &
                                                               self%ewald_alpha)
    else
        s1 = 0._dp
        ds1_dr = 0._dp
    end if

    do ichg = self%indices(1, iat), self%indices(2, iat)
        do jchg = self%indices(1, jat), self%indices(2, jat)
            select case(self%distribution)
            case('delta')
                s2 = 1._dp
                ds2_dr = 0._dp
            case('exponential')
                s2 = eval_switchfunction_exponentials(r, &
                                                self%parameters(ichg), &
                                                self%parameters(jchg))
                ds2_dr = eval_switchfunction_exponentials_derivative(r, &
                                                self%parameters(ichg), &
                                                self%parameters(jchg))
            end select
            dgam_dr = -(1._dp - s1) - r*ds1_dr + (1._dp - s2) + r*ds2_dr
            dgam_dr = dgam_dr / r**2
            fval = fval + dgam_dr * self%charges(ichg) * self%charges(jchg)
        end do
    end do
end function evaluate_pair


subroutine update_geometry(self, atoms)
    !! Updates the calculator parts which depend on the atomic positions
    !! and cell vectors.
    class(RealSpaceMonopoleCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    call self%nl%update(atoms)
    call self%build_kernel(atoms)
end subroutine update_geometry


pure subroutine update_moments(self, moments)
    !! Updates the calculator with respect to a new set of charge moments.
    class(RealSpaceMonopoleCalculator_type), intent(inout) :: self
    real(dp), dimension(:), intent(in) :: moments

    self%charges = moments
end subroutine update_moments


elemental function get_exponential_decay_constant(U) result(tau)
    !! Returns the decay constant for an exponential distribution
    !! yielding the same Hubbard value (in a Hartree approximation).
    !!
    !! See e.g. [Elstner (1998)](https://doi.org/10.1103/PhysRevB.58.7260)
    real(dp), intent(in) :: U
        !! Hubbard value
    real(dp) :: tau

    tau = 16._dp / 5._dp * U
end function get_exponential_decay_constant


elemental function eval_switchfunction_delta_gaussian_derivative(r, alpha) &
                                                                 result(ds_dr)
    !! Returns the first derivative with respect to r of
    !! [[tibi_coulomb_realspace:eval_switchfunction_delta_gaussian]].
    real(dp), intent(in) :: r
        !! the interatomic distance
    real(dp), intent(in) :: alpha
        !! Ewald parameter (related to the standard deviation sigma
        !! of a Gaussian distribution as \( \alpha = 1 / (\sqrt{2} \sigma)  \)
    real(dp) :: ds_dr

    ds_dr = 2._dp * alpha * exp(-alpha**2 * r**2) / sqrt(pi)
end function eval_switchfunction_delta_gaussian_derivative


elemental function eval_switchfunction_exponentials(r, tau_a, tau_b) result(s)
    !! Returns the function value of the switching function S for two
    !! exponentially decaying charge densities, with S defined by
    !! \( \gamma(r; param) = S(r; param) / r \), with \( \gamma \)
    !! the Coulomb interaction kernel.
    !!
    !! See the appendix of [Elstner (1998)](
    !! https://doi.org/10.1103/PhysRevB.58.7260).
    real(dp), intent(in) :: r
        !! the interatomic distance
    real(dp), intent(in) :: tau_a, tau_b
        !! decay constants \( \tau \) of the charge densities
    real(dp) :: s

    real(dp), parameter :: eps = epsilon(1._dp)

    if (tau_a < eps .or. tau_b < eps) then
        s = 1._dp
    elseif (abs(tau_a - tau_b) < eps) then
        s = 1._dp - r * exp(-tau_a*r) * (1._dp / r + 11._dp / 16._dp * tau_a &
                                         + 3._dp / 16._dp * r * tau_a**2 &
                                         + 1._dp / 48._dp * r**2 * tau_a**3)
    else
        s = 1._dp - r * (exp(-tau_a*r) * fpoly(r, tau_a, tau_b) &
                         + exp(-tau_b*r) * fpoly(r, tau_b, tau_a))
    end if


    contains


    pure function fpoly(r, tau_a, tau_b) result(fval)
        real(dp), intent(in) :: r, tau_a, tau_b
        real(dp) :: fval

        fval = (tau_b**4 * tau_a) / (2._dp * (tau_a**2 - tau_b**2)**2) &
               - (tau_b**6 - 3._dp * tau_b**4 * tau_a**2) &
                 / (r * (tau_a**2 - tau_b**2)**3)
    end function fpoly
end function eval_switchfunction_exponentials


elemental function eval_switchfunction_exponentials_derivative( &
                                                r, tau_a, tau_b) result(ds_dr)
    !! Returns the first derivative with respect to r of
    !! [[tibi_coulomb_realspace_monopole:eval_switchfunction_exponentials]].
    real(dp), intent(in) :: r
        !! the interatomic distance
    real(dp), intent(in) :: tau_a, tau_b
        !! decay constants \( \tau \) of the charge densities
    real(dp) :: ds_dr

    real(dp), parameter :: eps = epsilon(1._dp)

    if (tau_a < eps .or. tau_b < eps) then
        ds_dr = 0._dp
    elseif (abs(tau_a - tau_b) < eps) then
        ds_dr = exp(-tau_a*r) * (tau_a*r - 1._dp) * fpoly_aa(r, tau_a) &
                - r * exp(-tau_a*r) * fpoly_aa_dr(r, tau_a)
    else
        ds_dr = exp(-tau_a*r) * (tau_a*r - 1._dp) * fpoly_ab(r, tau_a, tau_b) &
                + exp(-tau_b*r) * (tau_b*r - 1._dp) * fpoly_ab(r, tau_b, tau_a) &
                - r * (exp(-tau_a*r) * fpoly_ab_dr(r, tau_a, tau_b) &
                       + exp(-tau_b*r) * fpoly_ab_dr(r, tau_b, tau_a))
    end if


    contains


    pure function fpoly_aa(r, tau) result(fval)
        real(dp), intent(in) :: r, tau
        real(dp) :: fval

        fval = 1._dp / r + 11._dp / 16._dp * tau &
               + 3._dp / 16._dp * r * tau**2 &
               + 1._dp / 48._dp * r**2 * tau**3
    end function fpoly_aa

    pure function fpoly_aa_dr(r, tau) result(fval)
        real(dp), intent(in) :: r, tau
        real(dp) :: fval

        fval = -1._dp / r**2 &
               + 3._dp / 16._dp * tau**2 &
               + 1._dp / 24._dp * r * tau**3
    end function fpoly_aa_dr

    pure function fpoly_ab(r, tau_a, tau_b) result(fval)
        real(dp), intent(in) :: r, tau_a, tau_b
        real(dp) :: fval

        fval = (tau_b**4 * tau_a) / (2._dp * (tau_a**2 - tau_b**2)**2) &
               - (tau_b**6 - 3._dp * tau_b**4 * tau_a**2) &
                 / (r * (tau_a**2 - tau_b**2)**3)
    end function fpoly_ab

    pure function fpoly_ab_dr(r, tau_a, tau_b) result(fval)
        real(dp), intent(in) :: r, tau_a, tau_b
        real(dp) :: fval

        fval = (tau_b**6 - 3._dp * tau_b**4 * tau_a**2) &
               / (r**2 * (tau_a**2 - tau_b**2)**3)
    end function fpoly_ab_dr
end function eval_switchfunction_exponentials_derivative

end module tibi_coulomb_realspace_monopole
