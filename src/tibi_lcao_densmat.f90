!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_lcao) tibi_lcao_densmat
!! Submodule for density matrix builds for [[tibi_lcao]]

use tibi_blas, only: dsyrk, &
                     zherk
implicit none


contains


module subroutine calculate_densmat(self, energy_weighted)
    !! Calculates the lower triangular density matrix (possibly
    !! energy-weighted) which is stored in self%(ew_)densmat_real/complex.
    !!
    !! Assumes that the band structure has already been obtained through
    !! a preceding call to [[tibi_lcao:solve_band_structure]].
    !!
    !! $$ \mathrm{densmat}(\mu, \nu, k, s) =
    !!    \sum_{b \in bands} f_{bks} c_{bks, \mu}^* c_{bks, \nu}^{\,} $$
    !!
    !! @NOTE The eigenvalues are assumed to be in ascending order
    !! and the weighted occupations are assumed to be non-negative.
    class(LCAOCalculator_type), intent(inout) :: self
    logical, intent(in) :: energy_weighted
        !! weigh by eigenvalues or not?

    integer :: ikpt, ispin, iband, nband, noccupied
    logical :: is_shifted
    real(dp) :: alpha, eps, shift
    real(dp), dimension(:), allocatable :: weights

    ! Check if we still need to do this calculation
    if (.not. energy_weighted .and. .not. self%need_densmat_update) return
    if (energy_weighted .and. .not. self%need_ew_densmat_update) return

    ! Set up the timer
    if (energy_weighted) then
        call task_timer%start_timer('lcao_ew_densmat')
    else
        call task_timer%start_timer('lcao_densmat')
    end if

    ! Start the real work
    eps = sqrt(epsilon(1._dp))
    nband = self%nband
    allocate(weights(nband))

    do ispin = 1, self%get_nr_of_spins()
        do ikpt = 1, self%get_nr_of_kpoints()
            ! Determine the number of bands with nonzero occupations
            do iband = 1, nband
                if (self%occupations%occupations(iband, ikpt, ispin) < eps) exit
            end do
            noccupied = iband - 1

            weights(:noccupied) = self%occupations%weighted_occupations( &
                                                        :noccupied, ikpt, ispin)

            if (energy_weighted) then
                weights(:noccupied) = weights(:noccupied) &
                                    * self%eigenvalues(:noccupied, ikpt, ispin)
            end if

            ! For good performance, we will want to compute the whole
            ! matrix in a single BLAS dsyrk / zherk call:
            !
            !     (ew_)densmat = (w^(1/2) * C) (w^(1/2) * C)^T
            !
            ! If there are both negative and positive weights, we need to
            ! apply a rigid shift to avoid introducing complex weights,
            ! with two BLAS calls in total:
            !
            !     (ew_)densmat = ((w-shift)^(1/2) * C) ((w-shift)^(1/2) * C)^T
            !                    + shift * C * C^T

            alpha = 1._dp
            shift = 0._dp
            is_shifted = .false.

            if (energy_weighted) then
                if (self%eigenvalues(1, ikpt, ispin) < 0._dp) then
                    if (self%eigenvalues(noccupied, ikpt, ispin) > 0._dp) then
                        shift = minval(weights(:noccupied)) - 1._dp
                        weights(:noccupied) = weights(:noccupied) - shift
                        is_shifted = .true.
                    else
                        alpha = -1._dp
                        weights(:noccupied) = weights(:noccupied) * alpha
                    end if
                end if
            end if

            weights(:noccupied) = sqrt(weights(:noccupied))

            if (self%complex_wfn) then
                if (is_shifted) then
                    if (energy_weighted) then
                        call zherk('L', 'N', nband, noccupied, 1._dp, &
                            self%wfncoeff_complex(:, :noccupied, ikpt, ispin), &
                            nband, 0._dp, &
                            self%ew_densmat_complex(:, :, ikpt, ispin), nband)
                    else
                        call zherk('L', 'N', nband, noccupied, 1._dp, &
                            self%wfncoeff_complex(:, :noccupied, ikpt, ispin), &
                            nband, 0._dp, &
                            self%densmat_complex(:, :, ikpt, ispin), nband)
                    end if
                end if

                do iband = 1, noccupied
                    self%wfncoeff_complex(:, iband, ikpt, ispin) = &
                        self%wfncoeff_complex(:, iband, ikpt, ispin) &
                        * weights(iband)
                end do

                if (energy_weighted) then
                    call zherk('L', 'N', nband, noccupied, alpha, &
                        self%wfncoeff_complex(:, :noccupied, ikpt, ispin), &
                        nband, shift, &
                        self%ew_densmat_complex(:, :, ikpt, ispin), nband)

                    self%ew_densmat_complex(:, :, ikpt, ispin) = &
                        conjg(self%ew_densmat_complex(:, :, ikpt, ispin))
                else
                    call zherk('L', 'N', nband, noccupied, alpha, &
                        self%wfncoeff_complex(:, :noccupied, ikpt, ispin), &
                        nband, shift, &
                        self%densmat_complex(:, :, ikpt, ispin), nband)

                    self%densmat_complex(:, :, ikpt, ispin) = &
                        conjg(self%densmat_complex(:, :, ikpt, ispin))
                end if

                do iband = 1, noccupied
                    self%wfncoeff_complex(:, iband, ikpt, ispin) = &
                        self%wfncoeff_complex(:, iband, ikpt, ispin) &
                        / weights(iband)
                end do
            else
                if (is_shifted) then
                    if (energy_weighted) then
                        call dsyrk('L', 'N', nband, noccupied, 1._dp, &
                            self%wfncoeff_real(:, :noccupied, ikpt, ispin), &
                            nband, 0._dp, &
                            self%ew_densmat_real(:, :, ikpt, ispin), nband)
                    else
                        call dsyrk('L', 'N', nband, noccupied, 1._dp, &
                            self%wfncoeff_real(:, :noccupied, ikpt, ispin), &
                            nband, 0._dp, &
                            self%densmat_real(:, :, ikpt, ispin), nband)
                    end if
                end if

                do iband = 1, noccupied
                    self%wfncoeff_real(:, iband, ikpt, ispin) = &
                        self%wfncoeff_real(:, iband, ikpt, ispin) &
                        * weights(iband)
                end do

                if (energy_weighted) then
                    call dsyrk('L', 'N', nband, noccupied, alpha, &
                        self%wfncoeff_real(:, :noccupied, ikpt, ispin), &
                        nband, shift, &
                        self%ew_densmat_real(:, :, ikpt, ispin), nband)
                else
                    call dsyrk('L', 'N', nband, noccupied, alpha, &
                        self%wfncoeff_real(:, :noccupied, ikpt, ispin), &
                        nband, shift, &
                        self%densmat_real(:, :, ikpt, ispin), nband)
                end if

                do iband = 1, noccupied
                    self%wfncoeff_real(:, iband, ikpt, ispin) = &
                        self%wfncoeff_real(:, iband, ikpt, ispin) &
                        / weights(iband)
                end do
            end if
        end do
    end do

    if (energy_weighted) then
        self%need_ew_densmat_update = .false.
    else
        self%need_densmat_update = .false.
    end if

    if (energy_weighted) then
        call task_timer%stop_timer('lcao_ew_densmat')
    else
        call task_timer%stop_timer('lcao_densmat')
    end if
end subroutine calculate_densmat

end submodule tibi_lcao_densmat
