!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_driver_socket
!! Module for performing externally driven tasks through sockets

use f90sockets, only: close_socket, &
                      open_socket, &
                      readbuffer, &
                      writebuffer
use tibi_atoms, only: Atoms_type
use tibi_calculator, only: Calculator_type
use tibi_constants, only: dp
use tibi_driver_base, only: BaseDriver_type
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_timer, only: task_timer
use tibi_utils_prog, only: assert, &
                           is_master
implicit none


type, extends(BaseDriver_type) :: SocketDriver_type
    !! Class for interacting with an external driver through INET or UNIX
    !! sockets and the i-PI protocol (see [Ceriotti (2014)](
    !! https://doi.org/10.1016/j.cpc.2013.10.027) and [Kapil (2018)](
    !! https://doi.org/10.1016/j.cpc.2018.09.020)).
    integer :: handle
        !! Unique ID for a socket connection
    character(len=1024) :: socket_inet_host = 'localhost' // achar(0)
        !! Server hostname, specific to INET sockets (null terminated)
    integer :: socket_inet_port = 31415
        !! Port number, specific to INET sockets
    character(len=16) :: socket_type = 'unix'
        !! Which type of socket connection (either 'inet' or 'unix')
    character(len=1024) :: socket_unix_suffix = 'tibi' // achar(0)
        !! Filename suffix to use for UNIX sockets (null terminated)
        !! (the full filename will be "/tmp/ipi_<suffix>")
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: run_task => run_task_socket
end type SocketDriver_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(SocketDriver_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('socket_inet_host')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%socket_inet_host
        call assert(len_trim(self%socket_inet_host) > 0, &
                    'socket_inet_host should not be empty string')
        self%socket_inet_host = trim(self%socket_inet_host) // achar(0)
    end if

    buffer = input%fetch_keyword('socket_inet_port')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%socket_inet_port
    end if

    buffer = input%fetch_keyword('socket_type')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%socket_type
        call assert(trim(self%socket_type) == 'inet' .or. &
                    trim(self%socket_type) == 'unix', &
                    'socket_type should either be "inet" or "unix"')
    end if

    buffer = input%fetch_keyword('socket_unix_suffix')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%socket_unix_suffix
        self%socket_unix_suffix = trim(self%socket_unix_suffix) // achar(0)
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input)
    !! Initialization of the socket driver, which includes reading
    !! input keywords and opening the socket connection.
    class(SocketDriver_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    call self%fetch_input_keywords(input)
    call task_timer%add_timer('socket_communication')

    if (is_master()) then
        select case(trim(self%socket_type))
        case('inet')
            call open_socket(self%handle, 1, self%socket_inet_port, &
                             self%socket_inet_host)
        case('unix')
            call open_socket(self%handle, 0, 0, self%socket_unix_suffix)
        end select
    end if
end subroutine initialize


subroutine run_task_socket(self, atoms, calculator)
    !! Listens to the socket and executes the received instructions.
    class(SocketDriver_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    class(Calculator_type), intent(inout) :: calculator

    logical :: results_available
    integer :: i, iat, natom_x_3
    integer, parameter :: msg_len = 12
    character(len=msg_len) :: header
    real(dp), dimension(:), allocatable :: buffer

    call task_timer%start_timer('socket_communication')

    natom_x_3 = atoms%natom * 3
    if (is_master()) then
        allocate(buffer(max(9, natom_x_3)))
    end if

    results_available = .false.

    socket_loop: do
        if (is_master()) then
            call readbuffer(self%handle, header, msg_len)
        end if

        select case(trim(header))
        case('EXIT')
            if (is_master()) then
                call close_socket(self%handle)
            end if
            call task_timer%stop_timer('socket_communication')
            exit socket_loop

        case('GETFORCE')
            ! Request to get the forces (and the total free energy and stress)
            if (is_master()) then
                call writebuffer(self%handle, 'FORCEREADY  ', msg_len)
                call writebuffer(self%handle, atoms%get_free_energy())
                call writebuffer(self%handle, atoms%natom)

                do iat = 1, atoms%natom
                   buffer(1+3*(iat-1):3*iat) = atoms%forces(:, iat)
                end do
                call writebuffer(self%handle, buffer(1:natom_x_3), natom_x_3)

                buffer(1:9) = pack(-atoms%stress * atoms%get_volume(), &
                                   mask=.true.)
                call writebuffer(self%handle, buffer(1:9), 9)

                call writebuffer(self%handle, 0)
            end if

            results_available = .false.

        case('POSDATA')
            ! Incoming set of atomic positions and other data
            ! This is followed up with an energy & force calculation
            if (is_master()) then
                call readbuffer(self%handle, buffer(1:9), 9)
                atoms%cell = transpose(reshape(buffer(1:9), [3, 3]))

                call readbuffer(self%handle, buffer(1:9), 9)
                atoms%invcell = transpose(reshape(buffer(1:9), [3, 3]))

                call readbuffer(self%handle, i)
                call assert(i == atoms%natom, &
                            'Changing the number of atoms is not allowed')

                call readbuffer(self%handle, buffer(1:natom_x_3), natom_x_3)

                do iat = 1, atoms%natom
                    atoms%positions(:, iat) = buffer(1+3*(iat-1):3*iat)
                end do
            end if

            ! Re-set the cell to recalculate the inverse cell
            call atoms%set_cell(atoms%cell, scale_atoms=.false.)

            call task_timer%stop_timer('socket_communication')
            call calculator%calculate_energy(atoms)
            call calculator%calculate_forces(atoms, do_stress=atoms%is_periodic)
            results_available = .true.
            call task_timer%start_timer('socket_communication')

        case('STATUS')
            ! Inquiry about the present status
            if (is_master()) then
                if (results_available) then
                    call writebuffer(self%handle, 'HAVEDATA    ', msg_len)
                else
                    call writebuffer(self%handle, 'READY       ', msg_len)
                end if
            end if

        case default
            call assert(.false., 'Received header ' // trim(header) // ' ' // &
                        'but expected one of: EXIT, GETFORCE, POSDATA, STATUS')
        end select
    end do socket_loop
end subroutine run_task_socket

end module tibi_driver_socket
