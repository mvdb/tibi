!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_smearing
!! Module for smearing occupation numbers

use tibi_constants, only: dp, &
                          electronvolt
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_utils_prog, only: assert
implicit none


type, abstract :: SmearingBase_type
    !! Base class for smearing methods
    real(dp) :: smearing_width = 0.01_dp * electronvolt
        !! the smearing width
    contains
    procedure :: fetch_input_keywords
    procedure(calculate_entropy), deferred :: calculate_entropy
    procedure(calculate_occupation), deferred :: calculate_occupation
end type SmearingBase_type


interface
    elemental function calculate_entropy(self, occupation) result(entropy)
        !! Returns the entropy term in the evaluation of the electronic free
        !! energy (to be multiplied by the negative of the smearing width).
        import dp, SmearingBase_type
        implicit none
        class(SmearingBase_type), intent(in) :: self
        real(dp), intent(in) :: occupation
        real(dp) :: entropy
    end function calculate_entropy

    elemental function calculate_occupation(self, eigenvalue, fermi_level) &
                                            result(occupation)
        !! Returns the occupation number as a function of the eigenvalue
        !! and Fermi level (to be implemented by child classes).
        import dp, SmearingBase_type
        implicit none
        class(SmearingBase_type), intent(in) :: self
        real(dp), intent(in) :: eigenvalue
        real(dp), intent(in) :: fermi_level
        real(dp) :: occupation
    end function calculate_occupation
end interface


type, extends(SmearingBase_type) :: SmearingFermiDirac_type
    !! Fermi-Dirac smearing function.
    contains
    procedure :: calculate_entropy => calculate_entropy_fermidirac
    procedure :: calculate_occupation => calculate_occupation_fermidirac
end type SmearingFermiDirac_type


type, extends(SmearingBase_type) :: SmearingNone_type
    !! No smearing (step function).
    contains
    procedure :: calculate_entropy => calculate_entropy_none
    procedure :: calculate_occupation => calculate_occupation_none
end type SmearingNone_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(SmearingBase_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('smearing_width')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%smearing_width
        self%smearing_width = self%smearing_width * electronvolt
        call assert(self%smearing_width >= 0, 'Smearing width must be >= 0!')
    end if
end subroutine fetch_input_keywords


elemental function calculate_entropy_fermidirac(self, occupation) &
                                                result(entropy)
    !! Returns the entropy term for a Fermi-Dirac distribution.
    !!
    !! See e.g. [Kresse and Furthmueller (1996)](10.1016/0927-0256(96)00008-0).
    class(SmearingFermiDirac_type), intent(in) :: self
    real(dp), intent(in) :: occupation
    real(dp) :: entropy

    real(dp), parameter :: flimit = 1e-12_dp

    if ((occupation < flimit) .or. (occupation > (1 - flimit))) then
        entropy = 0._dp
    else
        entropy = -(occupation * log(occupation) &
                    + (1._dp - occupation) * log(1._dp - occupation))
    end if
end function calculate_entropy_fermidirac


elemental function calculate_entropy_none(self, occupation) result(entropy)
    !! Returns the entropy term for a step function (no smearing).
    class(SmearingNone_type), intent(in) :: self
    real(dp), intent(in) :: occupation
    real(dp) :: entropy

    entropy = 0._dp
end function calculate_entropy_none


elemental function calculate_occupation_fermidirac(self, eigenvalue, &
                                        fermi_level) result(occupation)
    !! Calculates the occupation using a Fermi-Dirac distribution.
    class(SmearingFermiDirac_type), intent(in) :: self
    real(dp), intent(in) :: eigenvalue
    real(dp), intent(in) :: fermi_level
    real(dp) :: occupation

    real(dp) :: x
    real(dp), parameter :: xlimit = 40_dp

    x = (eigenvalue - fermi_level) / self%smearing_width

    if (x > xlimit) then
        occupation = 0._dp
    elseif (x < -xlimit) then
        occupation = 1._dp
    else
        occupation = 1._dp / (1._dp + exp(x))
    end if
end function calculate_occupation_fermidirac


elemental function calculate_occupation_none(self, eigenvalue, fermi_level) &
                                             result(occupation)
    !! Calculates the occupation using a step function.
    class(SmearingNone_type), intent(in) :: self
    real(dp), intent(in) :: eigenvalue
    real(dp), intent(in) :: fermi_level
    real(dp) :: occupation

    real(dp), parameter :: eps = 1e-12_dp

    if (eigenvalue < (fermi_level - eps)) then
        occupation = 1._dp
    elseif (eigenvalue < (fermi_level + eps)) then
        occupation = 0.5_dp
    else
        occupation = 0._dp
    end if
end function calculate_occupation_none

end module tibi_smearing
