!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_lapack
!! Explicit interfaces to selected LAPACK routines
!!
!! <http://www.netlib.org/lapack/explore-html/d8/d70/group__lapack.html>

use tibi_constants, only: dp
implicit none


interface
    subroutine dpotrf(uplo, n, a, lda, info)
        !! Cholesky-decomposes a symmetric (and positive definite) matrix
        !! (double precision real).
        import dp
        implicit none
        character, intent(in) :: uplo
            !! upper or lower triangles of A
        integer, intent(in) :: n
            !! order of matrices A
        integer, intent(in) :: lda
            !! leading dimension of A
        real(dp), dimension(lda, *), intent(inout) :: a
            !! matrix to be Cholesky decomposed
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine dpotrf

    subroutine dsyevd(jobz, uplo, n, a, lda, w, work, lwork, iwork, liwork, &
                      info)
        !! Divide-and-conquer standard eigenvalue solver
        !! (symmetric matrices, double precision real).
        import dp
        implicit none
        character, intent(in) :: jobz
            !! only eigenvals only or also eigenvecs
        character, intent(in) :: uplo
            !! upper or lower triangles of A
        integer, intent(in) :: n
            !! order of matrix A
        integer, intent(in) :: lda
            !! leading dimension of A
        real(dp), dimension(lda, *), intent(inout) :: a
            !! matrix A
        real(dp), dimension(*), intent(out) :: w
            !! will contain the (real) eigvalues
        real(dp), dimension(*), intent(out) :: work
            !! real work array
        integer, intent(in) :: lwork
            !! length of work array
        integer, dimension(*), intent(out) :: iwork
            !! integer work array
        integer, intent(in) :: liwork
            !! length of iwork array
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine dsyevd

    subroutine dsygst(itype, uplo, n, a, lda, b, ldb, info)
        !! Transforms a generalized eigenvalue problem to its standard form
        !! (symmetric matrices, double precision real).
        import dp
        implicit none
        integer, intent(in) :: itype
            !! problem type to be solved
        character, intent(in) :: uplo
            !! upper or lower triangles of A and B
        integer, intent(in) :: n
            !! order of matrices A and B
        integer, intent(in) :: lda
            !! leading dimension of A
        real(dp), dimension(lda, *), intent(inout) :: a
            !! matrix A
        integer, intent(in) :: ldb
            !! leading dimension of B
        real(dp), dimension(ldb, *), intent(in) :: b
            !! Cholesky factorization of right-hand side matrix B
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine dsygst

    subroutine dsygvd(itype, jobz, uplo, n, a, lda, b, ldb, w, work, lwork, &
                      iwork, liwork, info)
        !! Divide-and-conquer general eigenvalue solver
        !! (symmetric matrices, double precision real).
        import dp
        implicit none
        integer, intent(in) :: itype
            !! problem type to be solved
        character, intent(in) :: jobz
            !! only eigenvals only or also eigenvecs
        character, intent(in) :: uplo
            !! upper or lower triangles of A and B
        integer, intent(in) :: n
            !! order of matrices A and B
        integer, intent(in) :: lda
            !! leading dimension of A
        real(dp), dimension(lda, *), intent(inout) :: a
            !! matrix A
        integer, intent(in) :: ldb
            !! leading dimension of B
        real(dp), dimension(ldb, *), intent(inout) :: b
            !! matrix B
        real(dp), dimension(*), intent(out) :: w
            !! will contain the (real) eigvalues
        real(dp), dimension(*), intent(out) :: work
            !! real work array
        integer, intent(in) :: lwork
            !! length of work array
        integer, dimension(*), intent(out) :: iwork
            !! integer work array
        integer, intent(in) :: liwork
            !! length of iwork array
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine dsygvd

    subroutine dsysv(uplo, n, nrhs, a, lda, ipiv, b, ldb, work, lwork, info)
        !! Solves \(\alpha A x = b \)
        !! (symmetric matrices, double precision real).
        import dp
        implicit none
        character, intent(in) :: uplo
        integer, intent(in) :: n
        integer, intent(in) :: nrhs
        integer, intent(in) :: lda
        real(dp), dimension(lda, *), intent(inout) :: a
        integer, dimension(*) :: ipiv
        integer, intent(in) :: ldb
        real(dp), dimension(ldb, *), intent(inout) :: b
        integer, intent(in) :: lwork
        real(dp), dimension(*), intent(out) :: work
        integer, intent(out) :: info
    end subroutine dsysv

    subroutine dtrsm(side, uplo, transa, diag, m, n, alpha, a, lda, b, ldb)
        !! Triangular matrix equation solver (double precision real).
        import dp
        implicit none
        character, intent(in) :: side
            !! problem type to be solved
        character, intent(in) :: uplo
            !! upper or lower triangles of A and B
        character, intent(in) :: transa
            !! transposing A or not
        character, intent(in) :: diag
            !! unit triangular A or not
        integer, intent(in) :: m
            !! number of rows of B
        integer, intent(in) :: n
            !! number of columns of B
        real(dp), intent(in) :: alpha
            !! scalar multiplier
        integer, intent(in) :: lda
            !! leading dimension of A
        real(dp), dimension(lda, *), intent(in) :: a
            !! matrix A
        integer, intent(in) :: ldb
            !! leading dimension of B
        real(dp), dimension(ldb, *), intent(inout) :: b
            !! matrix B
    end subroutine dtrsm

    subroutine dtrtri(uplo, diag, n, a, lda, info)
        !! Inverts a triangular matrix (double precision real).
        import dp
        implicit none
        character, intent(in) :: uplo
            !! upper or lower triangles of A
        character, intent(in) :: diag
            !! unit diagonal elements or not
        integer, intent(in) :: n
            !! order of matrices A
        integer, intent(in) :: lda
            !! leading dimension of A
        real(dp), dimension(lda, *), intent(inout) :: a
            !! matrix to be inverted
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine dtrtri

    subroutine zheevd(jobz, uplo, n, a, lda, w, work, lwork, rwork, lrwork, &
                      iwork, liwork, info)
        !! Divide-and-conquer standard eigenvalue solver
        !! (Hermitian matrices, double precision complex).
        import dp
        implicit none
        character, intent(in) :: jobz
            !! only eigenvals only or also eigenvecs
        character, intent(in) :: uplo
            !! upper or lower triangles of A
        integer, intent(in) :: n
            !! order of matrix A
        integer, intent(in) :: lda
            !! leading dimension of A
        complex(dp), dimension(lda, *), intent(inout) :: a
            !! matrix A
        real(dp), dimension(*), intent(out) :: w
            !! will contain the (real) eigvalues
        complex(dp), dimension(*), intent(out) :: work
            !! complex work array
        integer, intent(in) :: lwork
            !! length of work array
        real(dp), dimension(*), intent(out) :: rwork
            !! real work array
        integer, intent(in) :: lrwork
            !! length of rwork array
        integer, dimension(*), intent(out) :: iwork
            !! integer work array
        integer, intent(in) :: liwork
            !! length of iwork array
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine zheevd

    subroutine zhegst(itype, uplo, n, a, lda, b, ldb, info)
        !! Transforms a generalized eigenvalue problem to its standard form
        !! (Hermitian matrices, double precision complex).
        import dp
        implicit none
        integer, intent(in) :: itype
            !! problem type to be solved
        character, intent(in) :: uplo
            !! upper or lower triangles of A and B
        integer, intent(in) :: n
            !! order of matrices A and B
        integer, intent(in) :: lda
            !! leading dimension of A
        complex(dp), dimension(lda, *), intent(inout) :: a
            !! matrix A
        integer, intent(in) :: ldb
            !! leading dimension of B
        complex(dp), dimension(ldb, *), intent(in) :: b
            !! Cholesky factorization of right-hand side matrix B
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine zhegst

    subroutine zhegvd(itype, jobz, uplo, n, a, lda, b, ldb, w, work, lwork, &
                      rwork, lrwork, iwork, liwork, info)
        !! Divide-and-conquer general eigenvalue solver
        !! (Hermitian matrices, double precision complex).
        import dp
        implicit none
        integer, intent(in) :: itype
            !! problem type to be solved
        character, intent(in) :: jobz
            !! only eigenvals only or also eigenvecs
        character, intent(in) :: uplo
            !! upper or lower triangles of A and B
        integer, intent(in) :: n
            !! order of matrices A and B
        integer, intent(in) :: lda
            !! leading dimension of A
        complex(dp), dimension(lda, *), intent(inout) :: a
            !! matrix A
        integer, intent(in) :: ldb
            !! leading dimension of B
        complex(dp), dimension(ldb, *), intent(inout) :: b
            !! matrix B
        real(dp), dimension(*), intent(out) :: w
            !! will contain the (real) eigvalues
        complex(dp), dimension(*), intent(out) :: work
            !! complex work array
        integer, intent(in) :: lwork
            !! length of work array
        real(dp), dimension(*), intent(out) :: rwork
            !! real work array
        integer, intent(in) :: lrwork
            !! length of rwork array
        integer, dimension(*), intent(out) :: iwork
            !! integer work array
        integer, intent(in) :: liwork
            !! length of iwork array
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine zhegvd

    subroutine zpotrf(uplo, n, a, lda, info)
        !! Cholesky-decomposes a Hermitian (and positive definite) matrix
        !! (double precision complex).
        import dp
        implicit none
        character, intent(in) :: uplo
            !! upper or lower triangles of A
        integer, intent(in) :: n
            !! order of matrices A
        integer, intent(in) :: lda
            !! leading dimension of A
        complex(dp), dimension(lda, *), intent(inout) :: a
            !! matrix to be Cholesky decomposed
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine zpotrf

    subroutine ztrsm(side, uplo, transa, diag, m, n, alpha, a, lda, b, ldb)
        !! Triangular matrix equation solver (double precision complex).
        import dp
        implicit none
        character, intent(in) :: side
            !! problem type to be solved
        character, intent(in) :: uplo
            !! upper or lower triangles of A and B
        character, intent(in) :: transa
            !! conjugate transposing A or not
        character, intent(in) :: diag
            !! unit triangular A or not
        integer, intent(in) :: m
            !! number of rows of B
        integer, intent(in) :: n
            !! number of columns of B
        complex(dp), intent(in) :: alpha
            !! scalar multiplier
        integer, intent(in) :: lda
            !! leading dimension of A
        complex(dp), dimension(lda, *), intent(in) :: a
            !! matrix A
        integer, intent(in) :: ldb
            !! leading dimension of B
        complex(dp), dimension(ldb, *), intent(inout) :: b
            !! matrix B
    end subroutine ztrsm

    subroutine ztrtri(uplo, diag, n, a, lda, info)
        !! Inverts a triangular matrix (double precision complex).
        import dp
        implicit none
        character, intent(in) :: uplo
            !! upper or lower triangles of A
        character, intent(in) :: diag
            !! unit diagonal elements or not
        integer, intent(in) :: n
            !! order of matrices A
        integer, intent(in) :: lda
            !! leading dimension of A
        complex(dp), dimension(lda, *), intent(inout) :: a
            !! matrix to be inverted
        integer, intent(out) :: info
            !! exit code (0 on successful termination)
    end subroutine ztrtri
end interface

end module tibi_lapack
