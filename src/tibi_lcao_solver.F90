!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_lcao) tibi_lcao_solver
!! Submodule for solving LCAO band structures for [[tibi_lcao]]

#if defined(_OMP)
use omp_lib, only: omp_get_max_threads, &
                   omp_get_thread_num
#endif
use tibi_eigensolver, only: EigenSolverComplexDC_type, &
                            EigenSolverRealDC_type
#if defined(_ELPA)
use tibi_eigensolver, only: EigenSolverComplexELPA_type, &
                            EigenSolverRealELPA_type
#endif
use tibi_timer_set, only: global_timer
use tibi_utils_prog, only: assert, &
                           is_master
implicit none


contains


module subroutine initialize_lcao_solver(self, input, nelect)
    !! Sets up the eigensolver(s).
    !!
    !! In the kpt/spin-parallelized case, each thread needs its own solver.
    class(LCAOCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    real(dp), intent(in), optional :: nelect
        !! the number of electrons (only used, optionally, to help determine
        !! and check the number of eigenvectors that need to be calculated)

    character(len=INPUT_VALUE_LENGTH) :: buffer
    integer :: isolver, nkpt, nspin, nsolved, nsolver
    real(dp) :: ratio

    nkpt = self%get_nr_of_kpoints()
    nspin = self%get_nr_of_spins()

    if (self%diag_parallel_ks .and. (nkpt > 1 .or. nspin > 1)) then
#if defined(_OMP)
        nsolver = min(nkpt * nspin, omp_get_max_threads())
#else
        nsolver = 1
#endif
    else
        nsolver = 1
    end if

    ! Allocate the solvers
    if (self%diag_algo ==  'divide&conquer') then
        if (self%complex_wfn) then
            allocate(EigenSolverComplexDC_type :: self%solver_complex(nsolver))
        else
            allocate(EigenSolverRealDC_type :: self%solver_real(nsolver))
        end if
    elseif (self%diag_algo == 'elpa') then
#if defined(_ELPA)
        if (self%complex_wfn) then
            allocate(EigenSolverComplexELPA_type :: &
                     self%solver_complex(nsolver))
        else
            allocate(EigenSolverRealELPA_type :: self%solver_real(nsolver))
        end if
#else
        call assert(.false., 'Tibi has not been compiled with ELPA support')
#endif
    else
        call assert(.false., 'Unknown diagonalization algorithm: ' // &
                    self%diag_algo)
    end if

    ! Determine the number of eigenvectors that need to be calculated
    buffer = input%fetch_keyword('diag_nband')
    if (len_trim(buffer) > 0) then
        if (verify(trim(buffer), '+-0123456789') == 0) then
            read(buffer, *) nsolved
        else
            read(buffer, *) ratio
            call assert(ratio > 1 - epsilon(1._dp), 'When given as a' // &
                        'real number, diag_nband needs to be >= 1.0')

            if (present(nelect)) then
                nsolved = ceiling(nelect * 0.5_dp * ratio)
            else
                nsolved = self%nband
            end if
        end if
    else
        nsolved = self%nband
    end if

    if (nsolved == 0 .or. nsolved > self%nband) then
        nsolved = self%nband
    end if

    if (present(nelect)) then
        call assert(nsolved >= ceiling(nelect * 0.5_dp), 'diag_nband must ' // &
                    'amount to at least half the number of electrons')
    end if

    ! Initialize the solvers
    if (self%complex_wfn) then
        do isolver = 1, size(self%solver_complex)
            call self%solver_complex(isolver)%initialize(input, self%nband, &
                                                         nsolved)
        end do

        if (is_master()) then
            call self%solver_complex(1)%print_description()
            call global_timer%add_timer(self%solver_complex(1)%timer, &
                                        'lcao_solve', parent='task')
        end if
    else
        do isolver = 1, size(self%solver_real)
            call self%solver_real(isolver)%initialize(input, self%nband, &
                                                      nsolved)
        end do

        if (is_master()) then
            call self%solver_real(1)%print_description()
            call global_timer%add_timer(self%solver_real(1)%timer, &
                                        'lcao_solve', parent='task')
        end if
    end if
end subroutine initialize_lcao_solver


module function get_nr_of_solved_bands(self) result(nsolved)
    !! Returns the number of bands (eigenvectors) that the eigensolver
    !! calculates.
    class(LCAOCalculator_type), intent(in) :: self
    integer :: nsolved

    if (self%complex_wfn) then
        nsolved = self%solver_complex(1)%get_nr_of_eigenvectors()
    else
        nsolved = self%solver_real(1)%get_nr_of_eigenvectors()
    end if
end function get_nr_of_solved_bands


module subroutine solve_band_structure(self, is_first)
    !! Solves the electronic structure, assuming all the main matrices
    !! have been allocated and the H and S matrices are build.
    class(LCAOCalculator_type), intent(inout) :: self
    logical, intent(in) :: is_first
        !! whether this is the first of a series of calls
        !! with the same S matrix (and different H matrices)

    integer :: ikpt, ispin, isolver, nkpt, nspin

    call task_timer%start_timer('lcao_solve')

    nkpt = self%get_nr_of_kpoints()
    nspin = self%get_nr_of_spins()

    ! NOTE: It seems tempting to merge both cases with an OpenMP if-clause,
    ! but it's safest to not let any OpenMP directives be involved in the
    ! non-kpt/spin-parallelized case.

    if (self%diag_parallel_ks .and. (nkpt > 1 .or. nspin > 1)) then
        !$omp parallel do default(private), schedule(static), collapse(2) &
        !$omp&            shared(self, is_first, nkpt, nspin)
        do ispin = 1, nspin
            do ikpt = 1, nkpt
#if defined(_OMP)
                isolver = 1 + omp_get_thread_num()
#else
                isolver = 1
#endif
                call solve_one(isolver, ikpt, ispin, &
                               use_cholesky=(.not. is_first))
            end do
        end do
        !$omp end parallel do
    else
        do ispin = 1, nspin
            do ikpt = 1, nkpt
                call solve_one(1, ikpt, ispin, &
                               use_cholesky=(.not. is_first))
            end do
        end do
    end if

    call task_timer%stop_timer('lcao_solve')


    contains


    subroutine solve_one(isolver, ikpt, ispin, use_cholesky)
        integer, intent(in) :: isolver
        integer, intent(in) :: ikpt
        integer, intent(in) :: ispin
        logical, intent(in) :: use_cholesky

        integer :: info
        character(len=128) :: msg

        if (self%complex_wfn) then
            call self%solver_complex(isolver)%solve( &
                        self%hamiltonian_complex(:, :, ikpt, ispin), &
                        self%overlap_complex(:, :, ikpt, ispin), &
                        self%cholesky_complex(:, :, ikpt, ispin), &
                        use_cholesky, self%eigenvalues(:, ikpt, ispin), &
                        self%wfncoeff_complex(:, :, ikpt, ispin), info)
        else
            call self%solver_real(isolver)%solve( &
                        self%hamiltonian_real(:, :, ikpt, ispin), &
                        self%overlap_real(:, :, ikpt, ispin), &
                        self%cholesky_real(:, :, ikpt, ispin), &
                        use_cholesky, self%eigenvalues(:, ikpt, ispin), &
                        self%wfncoeff_real(:, :, ikpt, ispin), info)
        end if

        write(msg, '("Eigensolver returned nonzero exit code: ", (i0))') info
        call assert(info == 0, trim(msg))
    end subroutine solve_one
end subroutine solve_band_structure

end submodule tibi_lcao_solver
