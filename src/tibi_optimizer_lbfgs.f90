!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_optimizer_lbfgs
!! Module for optimization using the limited-memory BFGS method

use tibi_blas, only: ddot
use tibi_constants, only: dp
use tibi_optimizer_base, only: BaseOptimizer_type
implicit none


type, extends(BaseOptimizer_type) :: LBFGSOptimizer_type
    !! Class for the limited-memory Broyden-Fletcher-Goldfarb-Shanno method
    !! ([Nocedal (1980)](https://doi.org/10.1090/S0025-5718-1980-0572855-7))
    real(dp), dimension(:), allocatable :: dot_prod
        !! (max_vectors) array with the dot products of xval_diff and fder_diff
    real(dp), dimension(:, :), allocatable :: fder_diff
        !! (vector_length, max_vectors) array with the most recent
        !! function value derivative steps
    real(dp) :: initial_curvature = 1._dp
        !! initial curvature in all directions, used in representing
        !! the initial inverse Hessian
    integer :: max_vectors = 50
        !! maximal number of vectors to be stored
    integer :: oldest_vector_index
        !! index of the oldest xval_diff and fder_diff vectors
    real(dp), dimension(:, :), allocatable :: xval_diff
        !! (vector_length, max_vectors) array with the most recent
        !! function argument steps
    contains
    procedure :: initialize
    procedure :: get_next_step => get_next_step_lbfgs
    procedure :: reset
    procedure :: wrap_vector_index
end type LBFGSOptimizer_type


contains


subroutine initialize(self, vector_length, max_vectors, initial_curvature)
    !! Initializes the LBFGS optimizer, which includes allocating
    !! and resetting the arrays holding the recent iteration history.
    class(LBFGSOptimizer_type), intent(inout) :: self
    integer, intent(in) :: vector_length
        !! length of the individual vectors (= number of function arguments)
    integer, intent(in), optional :: max_vectors
        !! maximal number of vectors to be stored
    real(dp), intent(in), optional :: initial_curvature
        !! initial curvature in all directions

    self%name = 'LBFGS'
    self%vector_length = vector_length

    if (present(initial_curvature)) then
        self%initial_curvature = initial_curvature
    end if

    if (present(max_vectors)) then
        self%max_vectors = max_vectors
    end if

    allocate(self%dot_prod(self%max_vectors))
    allocate(self%xval_diff(self%vector_length, self%max_vectors))
    allocate(self%fder_diff(self%vector_length, self%max_vectors))

    call self%reset()
end subroutine initialize


function get_next_step_lbfgs(self, xval, fval, fder) result(step)
    !! Returns the next step to take in the optimization cycle
    !! (normally to be added to xval).
    class(LBFGSOptimizer_type), intent(inout) :: self
    real(dp), dimension(self%vector_length), intent(in) :: xval
        !! array with the function arguments
    real(dp), intent(in) :: fval
        !! the function value
    real(dp), dimension(self%vector_length), intent(in) :: fder
        !! array with the function derivatives
    real(dp), dimension(self%vector_length) :: step

    integer :: i, iold, ivec, m
    real(dp) :: b
    real(dp), dimension(self%max_vectors) :: a
    real(dp), dimension(self%vector_length) :: q

    iold = self%oldest_vector_index

    ! History updates (part 1)
    if (self%iter > 1) then
        self%xval_diff(:, iold) = xval(:) - self%xval_diff(:, iold)
        self%fder_diff(:, iold) = fder(:) - self%fder_diff(:, iold)
        self%dot_prod(iold) = ddot(self%vector_length, &
                                   self%xval_diff(:, iold), 1, &
                                   self%fder_diff(:, iold), 1)
    end if

    ! Quasi-Newton step with implicit update of the inverse Hessian
    m = min(self%iter-1, self%max_vectors)
    q(:) = fder(:)

    do i = 1, m
        ! Looping from most to least recent entry
        ivec = self%wrap_vector_index(iold - i + 1)
        a(ivec) = ddot(self%vector_length, self%xval_diff(:, ivec), 1, q(:), 1)
        a(ivec) = a(ivec) / self%dot_prod(ivec)
        q(:) = q(:) - a(ivec) * self%fder_diff(:, ivec)
    end do

    step(:) = q(:) / self%initial_curvature

    do i = m, 1, -1
        ! Reverse loop order here
        ivec = self%wrap_vector_index(iold - i + 1)
        b = ddot(self%vector_length, self%fder_diff(:, ivec), 1, step(:), 1)
        b = b / self%dot_prod(ivec)
        step(:) = step(:) + self%xval_diff(:, ivec) * (a(ivec) - b)
    end do

    step(:) = -step(:)

    ! History updates (part 2)
    iold = self%wrap_vector_index(iold+1)
    self%oldest_vector_index = iold
    self%xval_diff(:, iold) = xval(:)
    self%fder_diff(:, iold) = fder(:)
    self%iter = self%iter + 1
end function get_next_step_lbfgs


pure subroutine reset(self)
    !! Resets any history-dependent arrays and counters.
    class(LBFGSOptimizer_type), intent(inout) :: self

    self%dot_prod(:) = 0._dp
    self%xval_diff(:, :) = 0._dp
    self%fder_diff(:, :) = 0._dp

    self%oldest_vector_index = 1
    self%iter = 1
end subroutine reset


pure function wrap_vector_index(self, index) result(wrapped_index)
    !! Wraps the index back into the [1, self%max_vectors] interval.
    class(LBFGSOptimizer_type), intent(in) :: self
    integer, intent(in) :: index
    integer :: wrapped_index

    wrapped_index = modulo(index, self%max_vectors)

    if (wrapped_index == 0) then
        wrapped_index = self%max_vectors
    end if
end function wrap_vector_index

end module tibi_optimizer_lbfgs
