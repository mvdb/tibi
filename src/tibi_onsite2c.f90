!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_onsite2c
!! Module for evaluating on-site two-center Hamiltonian integrals

use tibi_basis_ao, only: BasisAO_type
use tibi_constants, only: dp
use tibi_onsite1c_offsite2c, only: get_integral_start_index, &
                                   MAXSK, &
                                   read_skf_file, &
                                   TwocenterBatchSpline_type
use tibi_orbitals, only: calculate_slako_coeff_batch, &
                         calculate_slako_coeff_deriv_batch, &
                         MAXL, &
                         MAXLM, &
                         MAXTAU, &
                         orbital_index
use tibi_utils_prog, only: assert, &
                           is_master
implicit none


type Onsite2c_type
    !! Class for evaluating the on-site two-center H contributions
    !! for one kind pair.
    logical, dimension(:, :), allocatable :: inc
        !! (MAXL, numset) array with included subshells for every basis subset
    integer, dimension(:), allocatable :: numlm
        !! number of orbitals for every basis subset
    integer :: numset
        !! number of basis subsets
    real(dp) :: rcut
        !! cutoff radius
    type(TwocenterBatchSpline_type), dimension(:, :), allocatable :: spl
        !! batch spline interpolator for the Hamiltonian integrals
        !! for every pair of main basis subsets
    contains
    procedure :: add_integrals
    procedure :: add_integral_derivs
    procedure :: initialize_from_skf
end type Onsite2c_type


contains


subroutine add_integrals(self, x, y, z, r, h)
    !! Adds the on-site, two-center Hamiltonian matrix elements
    !! for all included orbitals.
    class(Onsite2c_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :), intent(inout) :: h
        !! Hamiltonian integral matrix

    integer :: il, jl, ilm, jlm, ilmcount, jlmcount
    integer :: iint, ntau, iset, jset
    real(dp), dimension(MAXSK) :: refint
    real(dp), dimension(MAXTAU+1) :: c1
    real(dp), dimension(MAXTAU) :: tmp
    real(dp), dimension(MAXTAU, MAXLM, MAXLM) :: coeff

    do jl = 1, MAXL
        if (.not. any(self%inc(jl, :))) cycle
        do il = 1, MAXL
            if (.not. any(self%inc(il, :))) cycle

            do jlm = orbital_index(jl), orbital_index(jl+1)-1
                do ilm = orbital_index(il), orbital_index(il+1)-1
                    if (jlm >= ilm) then
                        c1 = calculate_slako_coeff_batch(x, y, z, ilm, jlm)
                    else
                        c1 = calculate_slako_coeff_batch(x, y, z, jlm, ilm)
                    end if

                    coeff(1:MAXTAU, ilm, jlm) = c1(1:MAXTAU)
                end do
            end do
        end do
    end do

    jlmcount = 1
    do jset = 1, self%numset
        associate(jinc => self%inc(:, jset))
        associate(jnumlm => self%numlm(jset))

        ilmcount = 1
        do iset = 1, self%numset
            associate(iinc => self%inc(:, iset))
            associate(inumlm => self%numlm(iset))

            call self%spl(iset, jset)%evaluate_batch(r, refint)

            do jl = 1, MAXL
                if (.not. jinc(jl)) cycle

                do jlm = orbital_index(jl), orbital_index(jl+1)-1
                    do il = 1, MAXL
                        if (.not. iinc(il)) cycle

                        iint = get_integral_start_index(il, jl)
                        ntau = min(il, jl)

                        tmp(1:ntau) = refint(iint:iint+ntau-1)
                        tmp(ntau+1:MAXTAU) = 0._dp

                        do ilm = orbital_index(il), orbital_index(il+1)-1
                            h(ilmcount, jlmcount) = &
                                h(ilmcount, jlmcount) &
                                + sum(coeff(:, ilm, jlm) * tmp)

                            ilmcount = ilmcount + 1
                        end do
                    end do

                    ilmcount = ilmcount - inumlm
                    jlmcount = jlmcount + 1
                end do
            end do

            ilmcount = ilmcount + inumlm
            jlmcount = jlmcount - jnumlm
            end associate
            end associate
        end do

        jlmcount = jlmcount + jnumlm
        end associate
        end associate
    end do
end subroutine add_integrals


subroutine add_integral_derivs(self, x, y, z, r, dh)
    !! Adds the on-site, two-center Hamiltonian matrix element
    !! derivatives for all included orbitals.
    class(Onsite2c_type), intent(in) :: self
    real(dp), intent(in) :: x, y, z
        !! normalized vector components
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:, :, :), intent(inout) :: dh
        !! Hamiltonian integral derivatives matrix

    integer :: il, jl, ilm, jlm, ilmcount, jlmcount
    integer :: ider, iint, ntau, iset, jset
    real(dp), dimension(3) :: dr
    real(dp), dimension(3, 3) :: dxyz
    real(dp), dimension(MAXSK) :: refint, drefint
    real(dp), dimension(MAXTAU+1) :: c1
    real(dp), dimension(MAXTAU+1, 3) :: dc1
    real(dp), dimension(MAXTAU) :: dcoeffdx, tmp, dtmp
    real(dp), dimension(MAXTAU, MAXLM, MAXLM) :: coeff
    real(dp), dimension(MAXTAU, 3, MAXLM, MAXLM) :: dcoeff

    do jl = 1, MAXL
        if (.not. any(self%inc(jl, :))) cycle
        do il = 1, MAXL
            if (.not. any(self%inc(il, :))) cycle

            do jlm = orbital_index(jl), orbital_index(jl+1)-1
                do ilm = orbital_index(il), orbital_index(il+1)-1
                    if (jlm >= ilm) then
                        call calculate_slako_coeff_deriv_batch(x, y, z, ilm, &
                                                               jlm, c1, dc1)
                    else
                        call calculate_slako_coeff_deriv_batch(x, y, z, jlm, &
                                                               ilm, c1, dc1)
                    end if

                    coeff(1:MAXTAU, ilm, jlm) = c1(1:MAXTAU)
                    dcoeff(1:MAXTAU, :, ilm, jlm) = dc1(1:MAXTAU, :)
                end do
            end do
        end do
    end do

    dr = [-x, -y, -z]
    dxyz(:, 1) = [(x**2 - 1._dp) / r, x * y / r, x * z / r]
    dxyz(:, 2) = [x * y / r, (y**2 - 1._dp) / r, y * z / r]
    dxyz(:, 3) = [x * z / r, y * z / r, (z**2 - 1._dp) / r]

    jlmcount = 1
    do jset = 1, self%numset
        associate(jinc => self%inc(:, jset))
        associate(jnumlm => self%numlm(jset))

        ilmcount = 1
        do iset = 1, self%numset
            associate(iinc => self%inc(:, iset))
            associate(inumlm => self%numlm(iset))

            call self%spl(iset, jset)%evaluate_batch(r, refint, drefint)

            do jl = 1, MAXL
                if (.not. jinc(jl)) cycle

                do jlm = orbital_index(jl), orbital_index(jl+1)-1
                    do il = 1, MAXL
                        if (.not. iinc(il)) cycle

                        iint = get_integral_start_index(il, jl)
                        ntau = min(il, jl)

                        tmp(1:ntau) = refint(iint:iint+ntau-1)
                        tmp(ntau+1:MAXTAU) = 0._dp
                        dtmp(1:ntau) = drefint(iint:iint+ntau-1)
                        dtmp(ntau+1:MAXTAU) = 0._dp

                        do ilm = orbital_index(il), orbital_index(il+1)-1
                            do ider = 1, 3
                                dcoeffdx = matmul(dcoeff(:, :, ilm, jlm), &
                                                  dxyz(:, ider))

                                dh(ider, ilmcount, jlmcount) = &
                                    dh(ider, ilmcount, jlmcount) + &
                                    sum(dcoeffdx * tmp &
                                        + coeff(:, ilm, jlm) * dtmp * dr(ider))
                            end do

                            ilmcount = ilmcount + 1
                        end do
                    end do

                    ilmcount = ilmcount - inumlm
                    jlmcount = jlmcount + 1
                end do
            end do

            ilmcount = ilmcount + inumlm
            jlmcount = jlmcount - jnumlm
            end associate
            end associate
        end do

        jlmcount = jlmcount + jnumlm
        end associate
        end associate
    end do
end subroutine add_integral_derivs


subroutine initialize_from_skf(self, tbpar_dir, isym, jsym, ibasis, &
                               switching_degree, switching_length)
    !! Initialize from `.skf` files.
    class(Onsite2c_type), intent(out) :: self
    character(len=*), intent(in) :: tbpar_dir
        !! path to the directory with the parameter files
    character(len=2), intent(in) :: isym
        !! element symbols for the first atomic kind
    character(len=2), intent(in) :: jsym
        !! element symbols for the second atomic kind
    type(BasisAO_type), intent(in) :: ibasis
        !! basis set description for the first atomic kind
    integer, intent(in) :: switching_degree
        !! degree of the switching function polynomial for the tails
    real(dp), intent(in) :: switching_length
        !! length of the interval beyond the cutoff radius
        !! in which the switching function is applied

    character(len=:), allocatable :: filename, isym_set, jsym_set
    integer :: numr, numr2, numsk, numsk2, iset, jset
    integer, parameter :: ntables = 1
    logical :: extended_format, extended_format2
    logical :: has_diagonal_properties, has_offdiagonal_properties
    logical, parameter :: is_onsite = .true.
    real(dp) :: dr, dr2, rcut
    real(dp), dimension(MAXL) :: occ, eig, hub, offdiagH, offdiagS
    real(dp), dimension(:, :), allocatable :: tables1, tables2

    self%numset = ibasis%get_nr_of_zetas()
    allocate(self%inc(MAXL, self%numset))
    allocate(self%numlm(self%numset))
    allocate(self%spl(self%numset, self%numset))
    self%rcut = 0._dp

    do iset = 1, self%numset
        self%inc(:, iset) = ibasis%get_subset_included_subshells(iset)
        self%numlm(iset) = ibasis%get_subset_nr_of_orbitals(iset)
    end do

    do iset = 1, self%numset
        isym_set = trim(isym) // repeat('+', iset-1)
        associate(iinc => self%inc(:, iset))
        associate(inumlm => self%numlm(iset))

        do jset = 1, self%numset
            jsym_set = trim(isym) // repeat('+', jset-1)
            associate(jinc => self%inc(:, jset))
            associate(jnumlm => self%numlm(jset))

            filename = trim(tbpar_dir) // '/'  // isym_set // '-' // &
                       jsym_set // '_onsite2c_' // trim(jsym) // '.skf'

            if (is_master()) then
                print *, 'Reading onsite2c SK table from ' // filename
            end if

            has_diagonal_properties = .false.
            has_offdiagonal_properties = .false.

            call read_skf_file(filename, ntables, has_diagonal_properties, &
                               has_offdiagonal_properties, extended_format, &
                               occ, eig, hub, offdiagH, offdiagS, tables1, dr, &
                               numr, numsk)

            rcut = numr * dr

            if (iset /= jset) then
                ! Additional integrals needed from the "other" file
                filename = trim(tbpar_dir) // '/'  // jsym_set // '-' // &
                           isym_set // '_onsite2c_' // trim(jsym) // '.skf'

                if (is_master()) then
                    print *, 'Reading onsite2c SK table from ', trim(filename)
                end if

                call read_skf_file(filename, ntables, has_diagonal_properties, &
                                has_offdiagonal_properties, extended_format2, &
                                occ, eig, hub, offdiagH, offdiagS, tables2, &
                                dr2, numr2, numsk2)

                call assert(extended_format .eqv. extended_format2, 'Format '//&
                            'must be the same for Slater-Koster file pairs')
                call assert(abs(dr - dr2) < epsilon(1._dp), 'Grid spacing ' // &
                            'must be the same for Slater-Koster file pairs')
                call assert(numr == numr2, 'Number of grid points ' // &
                            'must be the same for Slater-Koster file pairs')
                call assert(numsk == numsk2, 'Number of integrals ' // &
                            'must be the same for Slater-Koster file pairs')
            else
                allocate(tables2, mold=tables1)
                tables2 = tables1
            end if

            call self%spl(iset, jset)%initialize_batch(dr, dr, rcut, &
                                                       tables1, tables2, &
                                                       iinc, jinc, &
                                                       extended_format, &
                                                       is_onsite, &
                                                       switching_degree, &
                                                       switching_length)

            self%rcut = max(self%rcut, self%spl(iset, jset)%rcut)
            deallocate(tables1)
            deallocate(tables2)

            end associate
            end associate
        end do

        end associate
        end associate
    end do
end subroutine initialize_from_skf

end module tibi_onsite2c
