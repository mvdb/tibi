!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_driver_base
!! Module for performing higher-level tasks, which involve
!! one or more energy/force calls to a calculator set

use tibi_atoms, only: Atoms_type
use tibi_calculator, only: Calculator_type
implicit none


type, abstract :: BaseDriver_type
    !! A base class for defining tasks
    contains
    procedure(run_task), deferred :: run_task
end type BaseDriver_type


interface
    subroutine run_task(self, atoms, calculator)
        !! Calls the necessary subroutines for performing the task at hand
        import Atoms_type, BaseDriver_type, Calculator_type
        implicit none
        class(BaseDriver_type), intent(inout) :: self
        type(Atoms_type), intent(inout) :: atoms
        class(Calculator_type), intent(inout) :: calculator
    end subroutine run_task
end interface


type, extends(BaseDriver_type) :: SinglePointDriver_type
    !! Class for simpy performing a singlepoint calculation
    logical :: need_forces = .false.
        !! whether forces need to be calculated, or only the energy
    contains
    procedure :: run_task => run_task_singlepoint
end type SinglePointDriver_type


contains


subroutine run_task_singlepoint(self, atoms, calculator)
    !! Calls the necessary subroutines for performing a singlepoint
    !! calculation of the total energy (and possibly also forces).
    class(SinglePointDriver_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    class(Calculator_type), intent(inout) :: calculator

    call calculator%calculate_energy(atoms)

    if (self%need_forces) then
        call calculator%calculate_forces(atoms, do_stress=atoms%is_periodic)
    end if
end subroutine run_task_singlepoint

end module tibi_driver_base
