!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_fluctuation1c_kernel
!! Module for evaluating one-center spin/charge fluctuation kernels

use tibi_basis_ao, only: BasisAO_type
use tibi_constants, only: dp
use tibi_orbitals, only: get_nr_of_orbitals, &
                         MAXL
use tibi_utils_math, only: symmetrize
use tibi_utils_prog, only: assert, &
                           is_master
implicit none


type, abstract :: Fluctuation1cKernelBase_type
    !! Abstract type for the (on-site) one-center fluctuation kernels
    !! of one atomic kind.
    real(dp), dimension(:, :), allocatable :: kernel
        !! kernel for every pair of included subshells
    contains
    procedure :: check_kernel_sign
end type Fluctuation1cKernelBase_type


type, extends(Fluctuation1cKernelBase_type) :: Fluctuation1cKernelSubshell_type
    !! Provides the subshell-dependent on-site one-center fluctuation kernel
    !! for one atomic kind.
    contains
    procedure :: initialize => initialize_subshell
    procedure :: initialize_from_arg
    procedure :: initialize_from_1cl
end type Fluctuation1cKernelSubshell_type


type, extends(Fluctuation1cKernelBase_type) :: Fluctuation1cKernelMultipole_type
    !! Provides the multipole-dependent on-site one-center fluctuation kernel
    !! for one atomic kind.
    contains
    procedure :: initialize => initialize_multipole
end type Fluctuation1cKernelMultipole_type


contains


subroutine check_kernel_sign(self, symbol, variant, strict)
    !! Checks whether the kernel elements have the expected sign.
    class(Fluctuation1cKernelBase_type), intent(in) :: self
    character(len=2), intent(in) :: symbol
        !! element symbol for the atomic kind
    character, intent(in) :: variant
        !! fluctuation type (e.g. 'U' or 'W')
    logical, intent(in) :: strict
        !! whether strict negativity/positivity should be checked

    character(len=:), allocatable :: expected_sign
    logical :: is_ok

    if (strict) then
        expected_sign = 'strictly '
    else
        expected_sign = ''
    end if

    select case(variant)
    case('U')
        expected_sign = expected_sign // 'positive'
    case('W')
        expected_sign = expected_sign // 'negative'
    case default
        call assert(.false., 'Unknown fluctuation type: ' // variant)
    end select

    select case(expected_sign)
    case('positive')
        is_ok = all(self%kernel > -epsilon(1._dp))
    case('strictly positive')
        is_ok = all(self%kernel > 0._dp)
    case('negative')
        is_ok = all(self%kernel < epsilon(1._dp))
    case('strictly negative')
        is_ok = all(self%kernel < 0._dp)
    case default
        call assert(.false., 'Unknown expected sign: ' // expected_sign)
    end select

    call assert(is_ok, 'Not all on1c' // variant // ' kernel elements ' // &
                'for "' // trim(symbol) // '" are ' // expected_sign)
end subroutine check_kernel_sign


subroutine initialize_subshell(self, symbol, basis, source, variant, &
                               subshell_dependent, tbpar_dir, distribution, &
                               parameters, has_offdiagonal)
    !! Initializes a subshell-dependent one-center fluctuation kernel type.
    class(Fluctuation1cKernelSubshell_type), intent(out) :: self
    character(len=2), intent(in) :: symbol
        !! element symbol for the atomic kind
    type(BasisAO_type), intent(in) :: basis
        !! basis set description for the atomic kind
    character(len=*), intent(in) :: source
        !! where to get the kernel from ('from_arg' or 'from_1cl')
    character, intent(in) :: variant
        !! fluctuation type (e.g. 'U' or 'W')
    logical, intent(in) :: subshell_dependent
        !! whether the kernel sources should be treated as
        !! subshell-dependent or only as atom-dependent
    character(len=*), intent(in), optional :: tbpar_dir
        !! path to the parameter file directory for the 'from_1cl' source
    character(len=*), intent(in), optional :: distribution
        !! charge distribution (e.g. 'exponential') for the 'from_arg' source
    real(dp), dimension(:), intent(in), optional :: parameters
        !! parameters for every subshell pair for the 'from_arg' source
        !! (or atom pair, depending on subshell_dependent)
    logical, intent(in), optional :: has_offdiagonal
        !! whether the parameters argument only contains the on-diagonal
        !! elements or also the off-diagonal ones

    integer :: inuml

    inuml = basis%get_nr_of_subshells()
    allocate(self%kernel(inuml, inuml), source=0._dp)

    if (source == 'from_1cl') then
        call assert(present(tbpar_dir), &
                    'No parameter file directory has been provided')
        call self%initialize_from_1cl(tbpar_dir, symbol, basis, variant, &
                                      subshell_dependent)
    elseif (source == 'from_arg') then
        call assert(present(distribution), 'No distribution has been provided')
        call assert(present(parameters), 'No parameters have been provided')
        call assert(present(has_offdiagonal), &
                    'has_offdiagonal has not been provided')
        call self%initialize_from_arg(variant, subshell_dependent, &
                                      distribution, parameters, has_offdiagonal)
    else
        call assert(.false., 'Unknown kernel source: ' // source)
    end if

    call symmetrize(self%kernel)
    call self%check_kernel_sign(symbol, variant, strict=.true.)
end subroutine initialize_subshell


subroutine initialize_from_1cl(self, tbpar_dir, symbol, basis, variant, &
                               subshell_dependent)
    !! Initializes a subshell-dependent one-center fluctuation kernel
    !! from '1cl' files.
    class(Fluctuation1cKernelSubshell_type), intent(inout) :: self
    character(len=*), intent(in) :: tbpar_dir
        !! path to the parameter file directory
    character(len=2), intent(in) :: symbol
        !! element symbol for the atomic kind
    type(BasisAO_type), intent(in) :: basis
        !! basis set description for the atomic kind
    character, intent(in) :: variant
        !! fluctuation type (e.g. 'U' or 'W')
    logical, intent(in) :: subshell_dependent
        !! whether the parameter files should be treated as
        !! subshell-dependent or only as atom-dependent

    integer :: icount, jcount, inuml, jnuml, izeta, jzeta, nzeta
    character(len=:), allocatable :: filename
    logical, dimension(:), allocatable :: iinc, jinc

    if (subshell_dependent) then
        nzeta = basis%get_nr_of_zetas()
    else
        nzeta = 1
    end if

    icount = 1
    do izeta = 1, nzeta
        iinc = basis%get_subset_included_subshells(izeta)
        inuml = count(iinc)

        jcount = 1
        do jzeta = 1, nzeta
            jinc = basis%get_subset_included_subshells(jzeta)
            jnuml = count(jinc)

            filename = trim(tbpar_dir) // '/' // trim(symbol) // &
                       repeat('+', izeta-1) // '-' // trim(symbol) // &
                       repeat('+', jzeta-1) // '_onsite' // variant // '.1cl'

            if (is_master()) then
                print *, 'Reading on1c' // variant // ' table from ' // filename
            end if

            call read_1cl_file(filename, iinc, jinc, &
                               self%kernel(icount:icount+inuml-1, &
                                           jcount:jcount+jnuml-1))
            jcount = jcount + jnuml
        end do

        icount = icount + inuml
    end do

    if (.not. subshell_dependent) then
        self%kernel = self%kernel(1, 1)
    end if
end subroutine initialize_from_1cl


subroutine initialize_from_arg(self, variant, subshell_dependent, &
                               distribution, parameters, has_offdiagonal)
    !! Initializes a subshell-dependent one-center fluctuation kernel
    !! from given parameters.
    class(Fluctuation1cKernelSubshell_type), intent(inout) :: self
    character, intent(in) :: variant
        !! fluctuation type (e.g. 'U' or 'W')
    logical, intent(in) :: subshell_dependent
        !! whether the given parameters should be treated as
        !! subshell-dependent or only as atom-dependent
    character(len=*), intent(in) :: distribution
        !! charge distribution (e.g. 'exponential')
    real(dp), dimension(:), intent(in) :: parameters
        !! parameters for every subshell, subshell pair or one subshell pair
        !! (depending on subshell_dependent and has_offdiagonal)
    logical, intent(in) :: has_offdiagonal
        !! whether the parameters argument only contains the on-diagonal
        !! elements or also the off-diagonal ones

    integer :: il, jl, inuml, needed

    inuml = size(self%kernel, dim=1)

    if (subshell_dependent) then
        if (has_offdiagonal) then
            needed = inuml**2
        else
            needed = inuml
        end if
    else
        needed = 1
    end if

    call assert(size(parameters) >= needed, 'The "parameters" array ' // &
                'does not contain enough elements')

    if (subshell_dependent) then
        if (has_offdiagonal) then
            self%kernel = reshape(parameters(1:inuml**2), [inuml, inuml])
        else
            call assert(variant == 'U', 'Approximations for off-diagonal ' // &
                        'on1c kernel matrix elements are only available ' // &
                        'for U kernels, not ' // variant)

            do il = 1, inuml
                do jl = 1, inuml
                    if (il == jl) then
                        self%kernel(jl, il) = parameters(il)
                    else
                        self%kernel(jl, il) = get_offdiag_on1cU_kernel( &
                                                            parameters(il), &
                                                            parameters(jl), &
                                                            distribution)
                    end if
                end do
            end do
        end if
    else
        self%kernel = parameters(1)
    end if
end subroutine initialize_from_arg


subroutine initialize_multipole(self, tbpar_dir, symbol, ilmax, nzeta, variant)
    !! Initializes a multipole-dependent one-center fluctuation kernel type.
    class(Fluctuation1cKernelMultipole_type), intent(out) :: self
    character(len=*), intent(in) :: tbpar_dir
        !! path to the parameter file directory
    character(len=2), intent(in) :: symbol
        !! element symbol for the atomic kind
    integer, intent(in) :: ilmax
        !! maximum angular momentum index
    integer, intent(in) :: nzeta
        !! zeta count
    character, intent(in) :: variant
        !! fluctuation type (e.g. 'U' or 'W')

    integer :: nmp
    real(dp), dimension(:, :), allocatable :: radmoms

    nmp = nzeta * ilmax**2
    allocate(self%kernel(nmp, nmp), source=0._dp)
    allocate(radmoms, mold=self%kernel)

    call read_1ck_files(tbpar_dir, symbol, variant, ilmax, nzeta, radmoms, &
                        self%kernel)

    call symmetrize(self%kernel)

    if (variant == 'W') then
        ! U kernel values are usually positive but in some cases not
        ! (the negative XC contribution may offset the positive Hartree part)
        ! W kernel values should always be negative
        call self%check_kernel_sign(symbol, variant, strict=.false.)
    end if
end subroutine initialize_multipole


subroutine read_1cl_file(filename, iinc, jinc, kernel)
    !! Reads the needed kernel matrix elements from the given '1cl' file.
    character(len=*), intent(in) :: filename
        !! name of the parameter file
    logical, dimension(MAXL), intent(in) :: iinc
        !! whether an angular momentum is present in the first basis subset
    logical, dimension(MAXL), intent(in) :: jinc
        !! whether an angular momentum is present in the second basis subset
    real(dp), dimension(:, :), intent(out) :: kernel
        !! kernel matrix

    integer :: fhandle, i, j, il, jl
    real(dp), dimension(MAXL) :: array

    open(newunit=fhandle, file=filename, status='old', action='read')

    i = 1
    do il = 1, MAXL
        read(fhandle, *) array

        j = 1
        do jl = 1, MAXL
            if (iinc(il) .and. jinc(jl)) then
                kernel(i, j) = array(jl)
                j = j + 1
            end if
        end do

        if (iinc(il)) i = i + 1
    end do

    close(fhandle)
end subroutine read_1cl_file


subroutine read_1ck_files(tbpar_dir, symbol, variant, ilmax, nzeta, radmoms, &
                          kernels)
    !! Reads the content of the 1ck file(s) for the given element
    !! (needed radial moment integrals and kernel matrix elements).
    character(len=*), intent(in) :: tbpar_dir
        !! path to the parameter file directory
    character(len=2), intent(in) :: symbol
        !! chemical symbol
    character, intent(in) :: variant
        !! fluctuation type (e.g. 'U' or 'W')
    integer, intent(in) :: ilmax
        !! maximum angular momentum index in the multipole expansion
        !! for the given element
    integer, intent(in) :: nzeta
        !! zeta count in the multipole expansion for the given element
    real(dp), dimension(:, :), intent(out) :: radmoms
        !! radial moment integrals (square form)
    real(dp), dimension(:, :), intent(out) :: kernels
        !! kernel matrix elements (square form)

    integer :: icount, jcount, izeta, jzeta, nmp
    character(len=:), allocatable :: filename

    radmoms = 0._dp
    kernels = 0._dp
    nmp = ilmax**2

    icount = 1
    do izeta = 1, nzeta

        jcount = 1
        do jzeta = 1, nzeta
            filename = trim(tbpar_dir) // '/' // trim(symbol) // &
                       repeat('+', izeta-1) // '-' // trim(symbol) // &
                       repeat('+', jzeta-1) // '_onsite' // variant // '.1ck'

            if (is_master()) then
                print *, 'Reading on1c' // variant // ' table from ' // filename
            end if

            call read_1ck_file(filename, ilmax, &
                            radmoms(icount:icount+nmp-1, jcount:jcount+nmp-1), &
                            kernels(icount:icount+nmp-1, jcount:jcount+nmp-1))

            jcount = jcount + nmp
        end do

        icount = icount + nmp
    end do
end subroutine read_1ck_files


subroutine read_1ck_file(filename, ilmax, radmom, kernel)
    !! Reads needed the radial moment integrals and kernel matrix elements
    !! from the given '1ck' file.
    character(len=*), intent(in) :: filename
        !! name of the parameter file
    integer, intent(in) :: ilmax
        !! maximum angular momentum index in the multipole expansion
    real(dp), dimension(:, :), intent(out) :: radmom
        !! radial moment integrals (square form)
    real(dp), dimension(:, :), intent(out) :: kernel
        !! kernel matrix elements (square form)

    integer :: fhandle, i, il, imp
    integer, parameter :: MAXL_MP = 3
    real(dp), dimension(MAXL_MP) :: array1, array2

    open(newunit=fhandle, file=filename, status='old', action='read')
    read(fhandle, *) array1
    read(fhandle, *) array2
    close(fhandle)

    radmom = 0._dp
    kernel = 0._dp

    imp = 1
    do il = 1, ilmax
        do i = 1, get_nr_of_orbitals(il)
            radmom(imp, imp) = array1(il)
            kernel(imp, imp) = array2(il)
            imp = imp + 1
        end do
    end do
end subroutine read_1ck_file


elemental function get_offdiag_on1cU_kernel(U_a, U_b, distribution) result(k)
    !! Returns an anlytical approximation to the off-diagonal onsite one-center
    !! U value, calculated from the corresponding diagonal values.
    !!
    !! See Appendix A.1 of the PhD dissertation by J. Elsner (1998).
    real(dp), intent(in) :: U_a
        !! first Hubbard parameter
    real(dp), intent(in) :: U_b
        !! second Hubbard parameter
    character(len=*), intent(in) :: distribution
        !! charge distribution (e.g. 'exponential')
    real(dp) :: k

    if (trim(distribution) == 'exponential') then
        if (U_a < epsilon(1._dp) .or. U_b < epsilon(1._dp)) then
            k = 0._dp
        else
            ! NOTE: indeed reduces to U for U_a = U_b = U
            k = 8._dp / 5._dp * ((U_a * U_b) / (U_a + U_b) &
                                 + (U_a * U_b)**2 / (U_a + U_b)**3)
        end if
    end if
end function get_offdiag_on1cU_kernel

end module tibi_fluctuation1c_kernel
