!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_repulsion_3c
!! Module for three-body repulsive interactions

use tibi_atoms, only: Atoms_type
use tibi_calculator, only: Calculator_type
use tibi_constants, only: dp, &
                          electronvolt
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_neighborlist, only: NeighborList_type
use tibi_threecenter, only: MAXLM_3C, &
                            ThreeCenter_type
use tibi_threecenter_geometry, only: get_rAB_derivs, &
                                     get_rCM_and_vec_CM, &
                                     get_rCM_derivs, &
                                     get_rotation_matrix, &
                                     get_theta_angle, &
                                     get_vAB_derivA, &
                                     get_vCM_derivA
use tibi_utils_prog, only: assert, &
                           is_master
implicit none


type, extends(Calculator_type) :: Repulsion3cCalculator_type
    !! A calculator for the three-center repulsion contributions
    character(len=128) :: description = 'Three-center repulsive'
        !! description for printing
    character(len=128) :: tbpar_dir = './'
        !! path to the directory with the tight-binding parameter files
    type(ThreeCenter_type),  dimension(:, :, :), allocatable :: vrep
        !! three-center repulsion evaluators for every atomic kind triplet
    logical :: repulsion_3c_binary_io = .false.
        !! whether to try to read from binary .3cb files (unformatted)
        !! files instead of ASCII .3cf files
    real(dp) :: repulsion_3c_eps_inner = 1e-12_dp * electronvolt
        !! threshold for deciding which interpolation order to select
        !! for three-center repulsion contributions
    real(dp) :: repulsion_3c_eps_outer = 1e-12_dp * electronvolt
        !! threshold for deciding which three-center repulsion
        !! contributions to neglect
    integer :: repulsion_3c_interpolation = 3
        !! interpolation order for the three-center evaluations
    real(dp) :: repulsion_3c_scaling = 1._dp
        !! empirical scaling factor to reduce the 3-body repulsion contribution
    integer, dimension(3) :: repulsion_3c_upsampling = [1, 1, 1]
        !! upsampling factors for refining the three-center repulsion tables
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: calculate_energy_given_nl
    procedure :: calculate_forces_given_nl
    procedure :: read_all_parameters
end type Repulsion3cCalculator_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(Repulsion3cCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('tbpar_dir')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%tbpar_dir
    end if

    buffer = input%fetch_keyword('repulsion_3c_binary_io')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%repulsion_3c_binary_io
    end if

    buffer = input%fetch_keyword('repulsion_3c_eps_inner')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%repulsion_3c_eps_inner
        self%repulsion_3c_eps_inner = self%repulsion_3c_eps_inner * electronvolt
        call assert(self%repulsion_3c_eps_inner >= 0._dp, &
                    'repulsion_3c_eps_inner must be >= 0')
    end if

    buffer = input%fetch_keyword('repulsion_3c_eps_outer')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%repulsion_3c_eps_outer
        self%repulsion_3c_eps_outer = self%repulsion_3c_eps_outer * electronvolt
        call assert(self%repulsion_3c_eps_outer <= self%repulsion_3c_eps_inner, &
                    'repulsion_3c_eps_outer must be <= repulsion_3c_eps_inner')
    end if

    buffer = input%fetch_keyword('repulsion_3c_interpolation')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%repulsion_3c_interpolation
        call assert(self%repulsion_3c_interpolation == 1 .or. &
                    self%repulsion_3c_interpolation == 3, &
                    'repulsion_3c_interpolation must be either 1 or 3')
    end if

    buffer = input%fetch_keyword('repulsion_3c_scaling')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%repulsion_3c_scaling
        call assert(self%repulsion_3c_scaling >= 0._dp .and. &
                    self%repulsion_3c_scaling <= 1._dp, &
                    'repulsion_3c_scaling must lie between 0 and 1')
    end if

    buffer = input%fetch_keyword('repulsion_3c_upsampling')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%repulsion_3c_upsampling
        call assert(all(self%repulsion_3c_upsampling >= 1), &
                    'all elements in repulsion_3c_upsampling must be >= 1')
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input, atoms)
    !! Initializes the RepulsionCalculator from the given instances of
    !! [[tibi_input:Input_type]] and [[tibi_atoms:Atoms_type]].
    class(Repulsion3cCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms

    call self%fetch_input_keywords(input)
    call self%read_all_parameters(atoms)
end subroutine initialize


subroutine calculate_energy_given_nl(self, atoms, nl)
    !! Calculates the (total) energy for the given [[tibi_atoms:Atoms_type]]
    !! object, which is stored in atoms%energy.
    class(Repulsion3cCalculator_type), intent(in) :: self
    type(Atoms_type), intent(inout) :: atoms
    class(NeighborList_type), intent(in) :: nl
        !! two-body neighborlist (assumed to be up-to-date,
        !! and not both-ways and without self-interaction)

    integer :: iat, jat, kat, ik, jk, kk, order
    integer :: ineigh, jneigh, ineighlist, jneighlist
    real(dp) :: rBC2, rCM, r_inner, r_outer, theta
    real(dp), dimension(3) :: vec_CM
    real(dp), dimension(3, 3) :: rotmat
    real(dp), dimension(MAXLM_3C**2) :: e

    call assert(.not. nl%both_ways, 'Cannot deal with both-way neighborlists')
    atoms%energy = 0._dp

    iat_loop: do iat = 1, atoms%natom
        ik = atoms%kinds(iat)

        ineigh_loop: do ineigh = 1, nl%neighbors(iat)
            ineighlist = nl%start_indices(iat) + ineigh - 1

            jat = nl%neighbor_indices(ineighlist)
            jk = atoms%kinds(jat)

            associate(vec_AB => nl%vec(:, ineighlist))
            associate(rAB => nl%r(ineighlist))
            if (rAB > nl%rcuts(jk, ik)) cycle ineigh_loop

            jneigh_loop: do jneigh = 1, nl%neighbors(iat)
                if (jneigh == ineigh) cycle
                jneighlist = nl%start_indices(iat) + jneigh - 1

                kat = nl%neighbor_indices(jneighlist)
                kk = atoms%kinds(kat)

                associate(vec_AC => nl%vec(:, jneighlist))
                associate(rAC => nl%r(jneighlist))
                if (rAC > nl%rcuts(kk, ik) .or. rAC < nl%rsame) &
                    cycle jneigh_loop

                rBC2 = sum((rAC * vec_AC(:) - rAB * vec_AB(:))**2)
                if (rBC2 > nl%rcuts(kk, jk)**2) cycle jneigh_loop

                call get_rCM_and_vec_CM(rAB, vec_AB, rAC, vec_AC, rCM, vec_CM)

                call self%vrep(ik, jk, kk)%get_rcut_rCM(rAB, r_inner, r_outer)

                if (rCM > r_outer) cycle jneigh_loop

                rotmat = get_rotation_matrix(vec_AB(:), vec_CM(:))
                theta = get_theta_angle(vec_CM(:), rotmat)

                if (rCM < r_inner) then
                    order = self%repulsion_3c_interpolation
                else
                    order = 1
                end if

                call self%vrep(ik, jk, kk)%evaluate(rAB, rCM, theta, order, e)

                if (iat == jat .and. iat == kat) then
                    e(1) = e(1) / 3._dp
                elseif (iat == jat .or. iat == kat .or. jat == kat) then
                    e(1) = e(1) * 2._dp / 3._dp
                end if

                atoms%energy = atoms%energy + 0.5_dp * e(1)
                end associate
                end associate
            end do jneigh_loop

            end associate
            end associate
        end do ineigh_loop
    end do iat_loop

    atoms%energy = atoms%energy * self%repulsion_3c_scaling

    if (is_master() .and. len_trim(self%description) > 0) then
        if (abs(atoms%energy) < 1e40_dp) then
            print '(" ", (a), " energy [Ha] = ", f0.8)', &
                  trim(self%description), atoms%energy
        else
            print '(" ", (a), " energy [Ha] = ", g0.8)', &
                  trim(self%description), atoms%energy
        end if
        print *
    end if
end subroutine calculate_energy_given_nl


subroutine calculate_forces_given_nl(self, atoms, nl, do_stress)
    !! Calculates the atomic forces for the given [[tibi_atoms:Atoms_type]]
    !! object, which are stored in atoms%forces.
    class(Repulsion3cCalculator_type), intent(in) :: self
    type(Atoms_type), intent(inout) :: atoms
    class(NeighborList_type), intent(in) :: nl
        !! two-body neighborlist (assumed to be up-to-date,
        !! and not both-ways and without self-interaction)
    logical, intent(in) :: do_stress
        !! whether to also compute the stress tensor for periodic structures

    integer :: i, ider, iat, jat, kat, ik, jk, kk, order
    integer :: ineigh, jneigh, ineighlist, jneighlist
    real(dp) :: rBC2, rCM, rCM_clip, r_inner, r_outer, theta
    real(dp), parameter :: eps_CM = 1e-12_dp
    real(dp), dimension(3) :: vec_CM, f_i, f_j, f_k
    real(dp), dimension(3, 2) :: drAB, drCM, dvABvCM
    real(dp), dimension(3, 3) :: rotmat, dvABdA, dvCMdA
    real(dp), dimension(MAXLM_3C**2, 3, 2) :: dedx

    call assert(.not. nl%both_ways, 'Cannot deal with both-way neighborlists')
    atoms%forces(:, :) = 0._dp
    if (do_stress) atoms%stress(:, :) = 0._dp

    iat_loop: do iat = 1, atoms%natom
        ik = atoms%kinds(iat)

        ineigh_loop: do ineigh = 1, nl%neighbors(iat)
            ineighlist = nl%start_indices(iat) + ineigh - 1

            jat = nl%neighbor_indices(ineighlist)
            jk = atoms%kinds(jat)

            associate(vec_AB => nl%vec(:, ineighlist))
            associate(rAB => nl%r(ineighlist))
            if (rAB > nl%rcuts(jk, ik)) cycle ineigh_loop

            jneigh_loop: do jneigh = 1, nl%neighbors(iat)
                if (jneigh == ineigh) cycle
                jneighlist = nl%start_indices(iat) + jneigh - 1

                kat = nl%neighbor_indices(jneighlist)
                kk = atoms%kinds(kat)

                associate(vec_AC => nl%vec(:, jneighlist))
                associate(rAC => nl%r(jneighlist))
                if (rAC > nl%rcuts(kk, ik) .or. rAC < nl%rsame) &
                    cycle jneigh_loop

                rBC2 = sum((rAC * vec_AC(:) - rAB * vec_AB(:))**2)
                if (rBC2 > nl%rcuts(kk, jk)**2) cycle jneigh_loop

                call get_rCM_and_vec_CM(rAB, vec_AB, rAC, vec_AC, rCM, vec_CM)

                call self%vrep(ik, jk, kk)%get_rcut_rCM(rAB, r_inner, r_outer)

                if (rCM > r_outer) cycle jneigh_loop

                rotmat = get_rotation_matrix(vec_AB(:), vec_CM(:))
                theta = get_theta_angle(vec_CM(:), rotmat)

                drAB(:, :) = get_rAB_derivs(vec_AB)
                drCM(:, :) = get_rCM_derivs(vec_CM)
                dvABdA(:, :) = get_vAB_derivA(vec_AB, rAB)
                dvCMdA(:, :) = get_vCM_derivA(vec_CM, rCM)
                rCM_clip = max(rCM, eps_CM)

                do ider = 1, 3
                    dvABvCM(ider, 1) = dot_product(dvABdA(:, ider), vec_CM(:))&
                                       + dot_product(vec_AB(:), dvCMdA(:, ider))
                    dvABvCM(ider, 2) = dot_product(-dvABdA(:, ider), vec_CM(:))&
                                       + dot_product(vec_AB(:), dvCMdA(:, ider))
                end do

                if (rCM < r_inner) then
                    order = self%repulsion_3c_interpolation
                else
                    order = 1
                end if

                call self%vrep(ik, jk, kk)%evaluate_deriv(rAB, rCM_clip, &
                                                          theta, drAB, drCM, &
                                                          dvABvCM, order, dedx)

                if (iat == jat .and. iat == kat) then
                    dedx(1, :, :) = dedx(1, :, :) / 3._dp
                elseif (iat == jat .or. iat == kat .or. jat == kat) then
                    dedx(1, :, :) = dedx(1, :, :) * 2._dp / 3._dp
                end if

                f_i = -0.5_dp * dedx(1, :, 1)
                f_j = -0.5_dp * dedx(1, :, 2)
                f_k = -(f_i + f_j)

                atoms%forces(:, iat) = atoms%forces(:, iat) + f_i
                atoms%forces(:, jat) = atoms%forces(:, jat) + f_j
                atoms%forces(:, kat) = atoms%forces(:, kat) + f_k

                if (do_stress) then
                    do i = 1, 3
                        atoms%stress(:, i) = atoms%stress(:, i) &
                            + (rAB * vec_AB(i) + rAC * vec_AC(i)) * f_i &
                            - (2._dp * rAB * vec_AB(i) - rAC * vec_AC(i)) * f_j&
                            - (2._dp * rAC * vec_AC(i) - rAB * vec_AB(i)) * f_k
                    end do
                end if

                end associate
                end associate
            end do jneigh_loop

            end associate
            end associate
        end do ineigh_loop
    end do iat_loop

    atoms%forces = atoms%forces * self%repulsion_3c_scaling
    if (do_stress) then
        atoms%stress = atoms%stress * self%repulsion_3c_scaling &
                                    / (3._dp * atoms%get_volume())
    end if
end subroutine calculate_forces_given_nl


subroutine read_all_parameters(self, atoms)
    !! Reads the definitions of the repulsive interactions
    !! from `.3cf` files in self%tbpar_dir and initializes
    !! the self%vrep interpolators accordingly.
    class(Repulsion3cCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    character(len=4) :: ext
    character(len=:), allocatable :: filename, isym, jsym, ksym
    integer :: ik, jk, kk
    logical :: exists_3cb, write_3cb

    allocate(self%vrep(atoms%nkind, atoms%nkind, atoms%nkind))

    do ik = 1, atoms%nkind
        isym = trim(atoms%symbols(ik))

        do jk = 1, atoms%nkind
            jsym = trim(atoms%symbols(jk))

            do kk = 1, atoms%nkind
                ksym = trim(atoms%symbols(kk))

                filename = trim(self%tbpar_dir) // '/' // isym // '-' // &
                           jsym // '_repulsion3c_' // ksym

                ext = '.3cf'
                write_3cb = .false.
                if (self%repulsion_3c_binary_io) then
                    ext = '.3cb'
                    inquire(file=filename // ext, exist=exists_3cb)
                    if (.not. exists_3cb) then
                        ext = '.3cf'
                        write_3cb = .true.
                    end if
                end if
                filename = filename // ext

                if (is_master()) then
                    print *, 'Reading repulsion3c parameters from ', filename
                end if

                call self%vrep(ik, jk, kk)%initialize(filename, &
                                        eps_inner=self%repulsion_3c_eps_inner, &
                                        eps_outer=self%repulsion_3c_eps_outer, &
                                        upuvw=self%repulsion_3c_upsampling, &
                                        write_3cb=write_3cb)
            end do
        end do
    end do

    if (is_master()) print *
end subroutine read_all_parameters

end module tibi_repulsion_3c
