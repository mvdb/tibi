!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_mixer
!! Module for mixing of e.g. charge densities or potentials
!!
!! This module is mostly for convenience / keyword parsing / (un)packing arrays.
!! The real work gets done in [[tibi_fixedpoint_pulay]].

use tibi_constants, only: dp
use tibi_fixedpoint_base, only: FixedPointBase_type
use tibi_fixedpoint_pulay, only: FixedPointPulay_type
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_utils_prog, only: assert
implicit none


type :: Mixer_type
    !! Class for storing vectors of the quantities to be mixed and
    !! using them to propose new vectors
    class(FixedPointBase_type), allocatable :: iterator
        !! the actual object for mixing and updating the quantities
    real(dp) :: mixing_fraction = 0.25_dp
        !! the (linear) mixing fraction
    integer :: mixing_history = 6
        !! number of most recent iteration results to be stored by the iterator
    real(dp) :: mixing_pulay_precon = 1e-6_dp
        !! preconditioning factor to apply in Pulay mixing
    logical :: mixing_pulay_restart = .false.
        !! whether to periodically clean the Pulay mixing history
    integer :: mixing_pulay_restart_history = 1
        !! number of latest vectors (if any) to keep in Pulay restarts
    character(len=128) :: mixing_scheme = 'pulay'
        !! name of the mixing scheme
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: add_input_vector
    procedure :: add_output_vector
    procedure :: get_max_abs_diff
    procedure :: get_next_vector
    procedure :: reset
end type Mixer_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(Mixer_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('mixing_fraction')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%mixing_fraction
        call assert(self%mixing_fraction > 0._dp &
                    .and. self%mixing_fraction < 1._dp, &
                    'mixing_fraction must lie in the ]0, 1[ interval')
    end if

    buffer = input%fetch_keyword('mixing_history')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%mixing_history
        call assert(self%mixing_history > 0, &
                    'mixing_history must be strictly positive')
    end if

    buffer = input%fetch_keyword('mixing_pulay_precon')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%mixing_pulay_precon
        call assert(.not. self%mixing_pulay_precon < 0._dp, &
                    'mixing_pulay_precon must not be negative')
    end if

    buffer = input%fetch_keyword('mixing_pulay_restart')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%mixing_pulay_restart
    end if

    buffer = input%fetch_keyword('mixing_pulay_restart_history')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%mixing_pulay_restart_history
    end if

    buffer = input%fetch_keyword('mixing_scheme')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%mixing_scheme
        call assert(trim(self%mixing_scheme) == 'linear' &
                    .or. trim(self%mixing_scheme) == 'pulay', &
                    'Only "linear" and "pulay" mixing schemes have been ' // &
                    'implemented')
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input, vector_length)
    !! Initialization procedure, which includes reading input keywords,
    !! allocating arrays and setting counters.
    class(Mixer_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    integer, intent(in) :: vector_length
        !! size of the vectors to be mixed

    call self%fetch_input_keywords(input)

    if (trim(self%mixing_scheme) == 'linear') then
        self%mixing_history = 1
        self%mixing_pulay_restart = .false.
        self%mixing_pulay_restart_history = 0
    end if

    select case (trim(self%mixing_scheme))
    case ('linear', 'pulay')
        allocate(FixedPointPulay_type :: self%iterator)
    end select

    select type(iterator => self%iterator)
    type is (FixedPointPulay_type)
        call iterator%initialize( &
                        vector_length=vector_length, &
                        max_vectors=self%mixing_history, &
                        damping=self%mixing_fraction, &
                        precon=self%mixing_pulay_precon, &
                        apply_periodic_restart=self%mixing_pulay_restart, &
                        restart_history=self%mixing_pulay_restart_history)
    end select
end subroutine initialize


subroutine add_input_vector(self, vector)
    !! See [[tibi_fixedpoint_base:FixedPointBase_type]].
    class(Mixer_type), intent(inout) :: self
    real(dp), dimension(:), intent(in) :: vector

    call self%iterator%add_input_vector(vector)
end subroutine add_input_vector


subroutine add_output_vector(self, vector)
    !! See [[tibi_fixedpoint_base:FixedPointBase_type]].
    class(Mixer_type), intent(inout) :: self
    real(dp), dimension(:), intent(in) :: vector

    call self%iterator%add_output_vector(vector)
end subroutine add_output_vector


pure function get_max_abs_diff(self) result(mad)
    !! See [[tibi_fixedpoint_base:FixedPointBase_type]].
    class(Mixer_type), intent(in) :: self
    real(dp) :: mad

    mad = self%iterator%get_max_abs_diff()
end function get_max_abs_diff


subroutine get_next_vector(self, vector)
    !! See [[tibi_fixedpoint_base:FixedPointBase_type]].
    class(Mixer_type), intent(inout) :: self
    real(dp), dimension(:), intent(out) :: vector

    vector = self%iterator%get_next_vector()
end subroutine get_next_vector


subroutine reset(self)
    !! See [[tibi_fixedpoint_base:FixedPointBase_type]].
    class(Mixer_type), intent(inout) :: self

    call self%iterator%reset()
end subroutine reset

end module tibi_mixer
