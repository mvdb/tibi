!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_atoms) tibi_atoms_extxyz
!! Submodule for extended XYZ file I/O for [[tibi_atoms]]

use tibi_constants, only: electronvolt
implicit none


contains


module subroutine read_extxyz_file(self, filename)
    !! Reads an Atoms object from a file in extended XYZ format
    !! (with units of eV and Angstrom).
    !!
    !! @NOTE The 'out' intent of 'self' means that all
    !! previous content of the object will be wiped clean.
    !!
    !! @NOTE Only the first configuration is read, all
    !! subsequent ones are ignored.
    class(Atoms_type), intent(out) :: self
    character(len=*), intent(in) :: filename

    logical :: is_new
    integer :: fhandle, i, ik, npair
    character(len=2), dimension(:), allocatable :: all_symbols
    character(len=256) :: buffer, properties
    character(len=256), dimension(:), allocatable :: comment_keys_values
    real(dp), dimension(3, 3) :: cell

    open(newunit=fhandle, file=filename, status='old', action='read')

    ! First line: number of atoms
    read(fhandle, *) self%natom
    allocate(self%positions(3, self%natom))
    allocate(self%kinds(self%natom))
    allocate(all_symbols(self%natom))
    cell(:, :) = 0._dp

    ! Second line: per-configuration properties
    read(fhandle, '(a)') buffer
    npair = 0
    do i = 1, len_trim(buffer)
        if (buffer(i:i) == '=') then
            npair = npair + 1
            buffer(i:i) = ' '
        end if
    end do

    allocate(comment_keys_values(2*npair))
    read(buffer, *) comment_keys_values

    properties = 'species:S:1:pos:R:3'

    do i = 1, npair
        associate(key => comment_keys_values(2*i-1))
        associate(val => comment_keys_values(2*i))

        select case(trim(key))
        case('Lattice')
            read(val, *) cell(:, :)
        case('pbc')
            select case(trim(val))
            case('T T T')
                self%is_periodic = .true.
            case('F F F')
                self%is_periodic = .false.
            case default
                call assert(.false., 'Partial periodicity is not implemented')
            end select
        case('Properties')
            select case(trim(val))
            case('species:S:1:pos:R:3', &
                 'species:S:1:pos:R:3:move_mask:L:1', &
                 'species:S:1:pos:R:3:move_mask:L:3')
                properties = trim(val)
            case default
                call assert(.false., 'Unable to deal with Properties=' // &
                            trim(val))
            end select
        end select

        end associate
        end associate
    end do

    ! Next (Natom) lines: per-atom properties
    allocate(self%move_mask(3, self%natom))

    select case(trim(properties))
    case('species:S:1:pos:R:3')
        do i = 1, self%natom
            read(fhandle, *) all_symbols(i), self%positions(:, i)
        end do
        self%move_mask(:, :) = .true.
    case('species:S:1:pos:R:3:move_mask:L:1')
        do i = 1, self%natom
            read(fhandle, *) all_symbols(i), self%positions(:, i), &
                             self%move_mask(1, i)
            self%move_mask(2:3, i) = self%move_mask(1, i)
        end do
    case('species:S:1:pos:R:3:move_mask:L:3')
        do i = 1, self%natom
            read(fhandle, *) all_symbols(i), self%positions(:, i), &
                             self%move_mask(:, i)
        end do
    end select

    close(fhandle)

    ! Get the number of unique kinds, and their symbols
    self%nkind = 0
    do i = 1, self%natom
        is_new = .true.
        do ik = 1, self%nkind
            if (all_symbols(i) == all_symbols(ik)) then
                is_new = .false.
                exit
            end if
        end do

        if (is_new) then
            self%nkind = self%nkind + 1
            all_symbols(self%nkind) = all_symbols(i)
            self%kinds(i) = self%nkind
        else
            self%kinds(i) = ik
        end if
    end do

    allocate(self%symbols(self%nkind))
    self%symbols(:) = all_symbols(1:self%nkind)

    ! convert from Angstrom to a0
    self%positions = self%positions * angstrom
    cell = cell * angstrom

    call self%set_cell(cell, scale_atoms=.false.)

    ! initialize the forces
    allocate(self%forces(3, self%natom))
    self%forces = 0._dp
end subroutine read_extxyz_file


module subroutine write_extxyz_file(self, filename, append)
    !! Writes the atomic positions and cell vectors to a file
    !! in extended XYZ format (with units of eV and Angstrom).
    class(Atoms_type), intent(in) :: self
    character(len=*), intent(in) :: filename
    logical, intent(in) :: append
        !! whether to append to the file if it already exists

    character(len=256) :: str
    logical :: file_exists, include_move_mask
    integer :: fhandle, iat

    inquire(file=filename, exist=file_exists)

    if (file_exists .and. append) then
        open(newunit=fhandle, file=filename, status='old', position='append', &
             action='write')
    elseif (file_exists) then
        open(newunit=fhandle, file=filename, status='replace', action='write')
    else
        open(newunit=fhandle, file=filename, status='new', action='write')
    end if

    ! First line: total number of atoms
    write(fhandle, '(i0)') self%natom

    ! Second line: any per-configuration properties
    write(str, '(f0.8)') self%energy / electronvolt
    write(fhandle, '(a, 1x)', advance='no') 'energy=' // trim(adjustl(str))

    write(fhandle, '(a)', advance='no') 'Properties=species:S:1:pos:R:3'

    include_move_mask = .not. all(self%move_mask)
    if (include_move_mask) then
        write(fhandle, '(a)', advance='no') ':move_mask:L:3'
    end if

    write(fhandle, '(1x)', advance='no')

    if (self%is_periodic) then
        write(str, '(9(f0.8, 1x))') pack(self%cell, mask=.true.) / angstrom
        str = adjustl(str)
        write(fhandle, '(a, 1x)', advance='no') 'Lattice="' // trim(str) // '"'
        write(fhandle, '(a, 1x)', advance='no') 'pbc="T T T"'
    else
        write(fhandle, '(a, 1x)', advance='no') 'pbc="F F F"'
    end if
    write(fhandle, *)

    ! Subsequent lines: element symbol and coordinates
    ! (and possibly other atom-dependent properties)
    do iat = 1, self%natom
        write(fhandle, '(a2, 1x, *(f0.8, 1x))', advance='no') &
              self%symbols(self%kinds(iat)), self%positions(:, iat) / angstrom

        if (include_move_mask) then
            write(fhandle, '(3(l1, 1x))', advance='no') self%move_mask(:, iat)
        end if

        write(fhandle, *)
    end do

    close(fhandle)
end subroutine write_extxyz_file

end submodule tibi_atoms_extxyz
