!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_occupations
!! Module for describing band occupations, taking a set of
!! k-points and spin channels into account.

use tibi_blas, only: ddot
use tibi_constants, only: dp
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_smearing, only: SmearingBase_type, &
                         SmearingFermiDirac_type, &
                         SmearingNone_type
use tibi_utils_prog, only: assert, &
                           is_master
use tibi_utils_string, only: count_items, &
                             lower_case
implicit none


type Occupations_type
    !! Class for storing and calculating k-point
    !! and spin dependent occupation numbers
    real(dp) :: eps_fermi = 1e-8_dp
        !! threshold controlling the accuracy of the Fermi level search
    real(dp), dimension(2) :: fermi_levels = huge(1._dp)
        !! the Fermi energies
    real(dp), dimension(:, :), allocatable :: kpoint_coords
        !! (reduced) k-point coordinates, typically to be multiplied
        !! by the the reciprocal lattice vectors.
    real(dp), dimension(:), allocatable :: kpoint_weights
        !! k-point weights (should always sum to 1)
    character(len=256) :: kpts
        !! string defining the k-point grid (can be a 3-tuple or a filename)
    logical :: kpts_gamma_shift = .true.
        !! whether to shift the k-point grid so that the Gamma-point is included
    integer :: maxiter_fermi = 250
        !! maximum number of iterations in the Fermi level search
    integer :: nkpt
        !! number of k-points
    integer :: nspin
        !! number of spin channels
    real(dp), dimension(:, :, :), allocatable :: occupations
        !! occupation numbers for every k-point and spin channel
    class(SmearingBase_type), allocatable :: smearing
        !! describes how occupation numbers are to be smeared
    character(len=16) :: smearing_method = 'none'
        !! type of smearing
    logical :: spin_fix_nupdown = .true.
        !! whether to keep self%spin_nupdown fixed to its initial value,
        !! or allow it to vary so that the Fermi levels of both spin
        !! channels are the same
    real(dp) :: spin_nupdown = 0._dp
        !! difference between the number of electrons in the two spin
        !! channels in a spin-polarized calculation
    logical :: spin_polarized = .false.
        !! whether to include spin polarization
    integer, dimension(2) :: spin_weights
        !! spin channel weights (should always sum to 2)
    real(dp), dimension(:, :, :), allocatable :: weighted_occupations
        !! as self%occupations, but multiplied by the k-point and spin weights
    contains
    procedure :: calculate_phase_shift
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: set_kpoint_coordinates_and_weights
    procedure :: set_spin_weights
    procedure :: get_fermi_level
    procedure :: get_fermi_levels
    procedure :: print_kpoints_description
    procedure :: print_spin_description
    procedure :: update_occupations_and_fermi_levels
    procedure :: integrate_over_brillouin_zone
    procedure :: calculate_entropic_energy
    procedure :: calculate_vbm_cbm
    procedure :: calculate_nr_of_electrons
    procedure :: calculate_weighted_occupations
end type Occupations_type


interface
    ! Procedures implemented in submodule tibi_occupations_kpoints

    pure module function calculate_phase_shift(self, ikpt, inverse_cell, &
                                               translation) result(phase_shift)
        !! Computes the phase shift for a k-point index, reciprocal cell,
        !! and Bravais lattice translation.
        !!
        !! \( \exp(2 \pi i \mathbf{k} \mathbf{C}^{-1} \cdot \mathbf{T}) \)
        implicit none
        class(Occupations_type), intent(in) :: self
        integer, intent(in) :: ikpt
            !! k-point index
        real(dp), dimension(3, 3), intent(in) :: inverse_cell
            !! matrix inverse of the real-space cell
        real(dp), dimension(:), intent(in) :: translation
            !! Bravais lattice translation vector
        complex(dp) :: phase_shift
    end function calculate_phase_shift

    module subroutine print_kpoints_description(self)
        !! Prints a description of the k-point grid.
        class(Occupations_type), intent(in) :: self
    end subroutine print_kpoints_description

    module subroutine set_kpoint_coordinates_and_weights(self)
        !! Sets the reduced k-point coordinates (self%kpoint_coords)
        !! and weights (self%kpoint_weights) based on self%kpts.
        class(Occupations_type), intent(inout) :: self
    end subroutine set_kpoint_coordinates_and_weights

    ! Procedures implemented in submodule tibi_occupations_spins

    module subroutine set_spin_weights(self)
        !! Calculates the weights of the spin channels for the
        !! Brillouin zone integrations.
        class(Occupations_type), intent(inout) :: self
    end subroutine set_spin_weights

    module subroutine print_spin_description(self)
        !! Prints a description of the spin channels.
        class(Occupations_type), intent(in) :: self
    end subroutine print_spin_description
end interface


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(Occupations_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('kpts')
    if (len_trim(buffer) > 0) then
        ! Take into account that it might be enclosed in quotes
        if (count_items(buffer) == 3) then
            read(buffer, '(a)') self%kpts
        else
            read(buffer, *) self%kpts
        end if
    end if

    buffer = input%fetch_keyword('kpts_gamma_shift')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%kpts_gamma_shift
    end if

    buffer = input%fetch_keyword('smearing_method')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%smearing_method
        call lower_case(self%smearing_method)
        call assert(self%smearing_method == 'none' .or. &
                    self%smearing_method == 'fermi-dirac', &
                    'Unknown smearing method: ' // self%smearing_method)
    end if

    buffer = input%fetch_keyword('spin_fix_nupdown')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%spin_fix_nupdown
    end if

    buffer = input%fetch_keyword('spin_nupdown')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%spin_nupdown
    end if

    buffer = input%fetch_keyword('spin_polarized')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%spin_polarized
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input)
    !! Sets up the [[tibi_occupations:Occupations_type]] instance;
    !! this should normally only be done once, at the very beginning
    !! of an electronic structure calculation.
    class(Occupations_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    call self%fetch_input_keywords(input)
    call self%set_spin_weights()
    call self%set_kpoint_coordinates_and_weights()

    self%nkpt = size(self%kpoint_weights)
    if (self%spin_polarized) then
        self%nspin = 2
    else
        self%nspin = 1
    end if

    if (trim(self%smearing_method) == 'fermi-dirac') then
        allocate(SmearingFermiDirac_type :: self%smearing)
    elseif (trim(self%smearing_method) == 'none') then
        allocate(SmearingNone_type :: self%smearing)
    end if
    call self%smearing%fetch_input_keywords(input)

    if (is_master()) then
        call self%print_spin_description()
        call self%print_kpoints_description()
    end if
end subroutine initialize


pure function get_fermi_level(self) result(efermi)
    !! If there is no band gap, the Fermi level corresponds
    !! to the eigenenergy where bands are 50% filled.
    !! If there is a band gap, the Fermi level lies somewhere
    !! in the gap. This function ignores spin polarization.
    class(Occupations_type), intent(in) :: self
    real(dp) :: efermi

    efermi = maxval(self%fermi_levels(1:self%nspin))
end function get_fermi_level


pure function get_fermi_levels(self) result(efermi)
    !! If there is no band gap, the Fermi level corresponds
    !! to the eigenenergy where bands are 50% filled.
    !! If there is a band gap, the Fermi level lies somewhere
    !! in the gap. This function returns the Fermi levels for
    !! the two spin channels.
    class(Occupations_type), intent(in) :: self
    real(dp), dimension(2) :: efermi

    efermi = self%fermi_levels
end function get_fermi_levels


subroutine update_occupations_and_fermi_levels(self, eigenvalues, nelect)
    !! Updates self%occupations, self%weighted_occupations & self%fermi_levels,
    !! given the matrix of eivenvalues and the number of electrons
    !! and the smearing function in self%smearing_method.
    class(Occupations_type), intent(inout) :: self
    real(dp), dimension(:, :, :), intent(in) :: eigenvalues
        !! the eigenvalues for every k-point and spin channel
    real(dp), intent(in) :: nelect
        !! the target number of electrons

    logical :: found
    integer :: i, ikpt, ispin
    real(dp) :: tmp
    real(dp), dimension(2) :: targets, emin, emax

    if (.not. allocated(self%occupations)) then
        allocate(self%occupations, mold=eigenvalues)
        allocate(self%weighted_occupations, mold=eigenvalues)
    end if

    if (self%spin_polarized .and. self%spin_fix_nupdown) then
        targets = [nelect+self%spin_nupdown, nelect-self%spin_nupdown] / 2._dp
    else
        targets = [nelect, 0._dp]
    end if

    ! First, find the extremal eigenvalues
    emin = 0._dp
    emax = 0._dp

    do ispin = 1, self%nspin
        emin(ispin) = huge(0._dp)
        emax(ispin) = -huge(0._dp)

        do ikpt = 1, self%nkpt
            emin(ispin) = min(emin(ispin), minval(eigenvalues(:, ikpt, ispin)))
            emax(ispin) = max(emax(ispin), maxval(eigenvalues(:, ikpt, ispin)))
        end do
    end do

    ! Check if all bands are either empty or full (early return)
    if (abs(nelect - 0._dp) < self%eps_fermi) then
        self%fermi_levels(:) = emin(:)
        do ispin = 1, self%nspin
            do ikpt = 1, self%nkpt
                self%occupations(:, ikpt, ispin) = 0._dp
                call self%calculate_weighted_occupations(ikpt, ispin)
            end do
        end do
        return
    elseif (abs(nelect - 2*size(eigenvalues, dim=1)) < self%eps_fermi) then
        self%fermi_levels(:) = emax(:)
        do ispin = 1, self%nspin
            do ikpt = 1, self%nkpt
                self%occupations(:, ikpt, ispin) = 1._dp
                call self%calculate_weighted_occupations(ikpt, ispin)
            end do
        end do
        return
    end if

    ! Refine using bisection
    if (self%spin_fix_nupdown .or. .not. self%spin_polarized) then
        do ispin = 1, self%nspin
            found = .false.

            fermi_loop1: do i = 1, self%maxiter_fermi
                self%fermi_levels(ispin) = 0.5_dp * (emin(ispin) + emax(ispin))

                tmp = 0._dp
                do ikpt = 1, self%nkpt
                    tmp = tmp + self%calculate_nr_of_electrons( &
                                    eigenvalues(:, ikpt, ispin), ikpt, ispin)
                end do

                found = abs(tmp - targets(ispin)) < self%eps_fermi
                if (found) then
                    exit fermi_loop1
                elseif (tmp < targets(ispin)) then
                    emin(ispin) = self%fermi_levels(ispin)
                elseif (tmp > targets(ispin)) then
                    emax(ispin) = self%fermi_levels(ispin)
                end if
            end do fermi_loop1

            call check_found()
        end do
    else
        emin(:) = minval(emin(:))
        emax(:) = maxval(emax(:))

        fermi_loop2: do i = 1, self%maxiter_fermi
            self%fermi_levels(:) = 0.5_dp * (emin(:) + emax(:))

            tmp = 0._dp
            do ispin = 1, self%nspin
                do ikpt = 1, self%nkpt
                    tmp = tmp + self%calculate_nr_of_electrons( &
                                                eigenvalues(:, ikpt, ispin), &
                                                ikpt, ispin)
                end do
            end do

            found = abs(tmp - nelect) < self%eps_fermi
            if (found) then
                exit fermi_loop2
            elseif (tmp < nelect) then
                emin(:) = self%fermi_levels(:)
            elseif (tmp > nelect) then
                emax(:) = self%fermi_levels(:)
            end if
        end do fermi_loop2

        call check_found()
    end if


    contains


    subroutine check_found()
        call assert(found, 'Fermi level bisection was not successful. ' // &
                    'This might be solved by applying (more) smearing.')
    end subroutine check_found
end subroutine update_occupations_and_fermi_levels


function integrate_over_brillouin_zone(self, property) result(s)
    !! Calculates the following sum:
    !! $$\sum_{bands,spins,kpts} w_{sk} \, occup_{bsk} \, property_{bsk}$$
    !!
    !! The weights should sum up to 1 and the occupations for completely
    !! filled states should be 1 with and 2 without spin polarization.
    class(Occupations_type), intent(in) :: self
    real(dp), dimension(:, :, :), intent(in) :: property
        !! (nband, nkpt, nspin) array of the property to be integrated
    real(dp) :: s

    integer :: ikpt, ispin, nband

    nband = size(property, dim=1)

    s = 0._dp
    do ispin = 1, self%nspin
        do ikpt = 1, self%nkpt
            s = s + self%kpoint_weights(ikpt) * self%spin_weights(ispin) &
                    * ddot(nband, property(:, ikpt, ispin), 1, &
                           self%occupations(:, ikpt, ispin), 1)
        end do
    end do
end function integrate_over_brillouin_zone


function calculate_entropic_energy(self) result(eentro)
    !! Returns the entropic contribution to the electronic free energy.
    !!
    !! @NOTE Assumes the occupations are up-to-date.
    class(Occupations_type), intent(in) :: self
    real(dp) :: eentro

    integer :: iband, ikpt, ispin, nband
    real(dp) :: f, s

    nband = size(self%occupations, dim=1)

    s = 0._dp
    do ispin = 1, self%nspin
        do ikpt = 1, self%nkpt
            do iband = 1, nband
                f = self%occupations(iband, ikpt, ispin)
                s = s + self%kpoint_weights(ikpt) * self%spin_weights(ispin) &
                        * self%smearing%calculate_entropy(f)
            end do
        end do
    end do

    eentro = -s * self%smearing%smearing_width
end function calculate_entropic_energy


subroutine calculate_vbm_cbm(self, eigenvalues, nelect, noccupied, vbm, cbm)
    !! Calculates the number of occupied bands and (for each spin spannel)
    !! the valence band maximum and conduction band minimum.
    !!
    !! Assumes self%fermi_levels and self%occupations are up-to-date.
    class(Occupations_type), intent(in) :: self
    real(dp), dimension(:, :, :), intent(in) :: eigenvalues
        !! the eigenvalues for every k-point and spin channel
    real(dp), intent(in) :: nelect
        !! the number of electrons
    integer, intent(out) :: noccupied
        !! the number of occupied bands (across k-points and spin channels)
    real(dp), dimension(2), intent(out) :: vbm
        !! valence band maxima
    real(dp), dimension(2), intent(out) :: cbm
        !! conduction band minima

    integer :: iband, nband, ikpt, ispin, istart, icbm, ivbm
    logical :: found, is_metallic
    real(dp) :: efermi
    real(dp), parameter :: eps = 1e-2_dp

    noccupied = 0
    vbm(1:self%nspin) = -huge(1._dp)
    cbm(1:self%nspin) = huge(1._dp)

    nband = size(eigenvalues, dim=1)
    istart = nint(nelect / 2._dp)
    istart = max(1, min(nband, istart))
    is_metallic = .false.

    outer_loop: do ispin = 1, self%nspin
        do ikpt = 1, self%nkpt
            efermi = self%fermi_levels(ispin)

            ! Go up or down till we cross the Fermi level
            found = .false.

            if (eigenvalues(istart, ikpt, ispin) < efermi) then
                ascend_loop: do iband = istart+1, nband
                    found = eigenvalues(iband, ikpt, ispin) > efermi
                    if (found) exit ascend_loop
                end do ascend_loop

                if (found) then
                    ivbm = iband - 1
                    icbm = iband
                else
                    ivbm = nband
                    icbm = nband
                end if
            else
                descend_loop: do iband = istart-1, 1, -1
                    found = eigenvalues(iband, ikpt, ispin) < efermi
                    if (found) exit descend_loop
                end do descend_loop

                if (found) then
                    ivbm = iband
                    icbm = iband + 1
                else
                    ivbm = 1
                    icbm = 1
                end if
            end if

            if (.not. is_metallic) then
                is_metallic = (ivbm == icbm) .or. &
                (abs(self%occupations(ivbm, ikpt, ispin) - 1._dp) > eps) .or. &
                (abs(self%occupations(icbm, ikpt, ispin) - 0._dp) > eps)
            end if

            if (is_metallic) then
                vbm = self%fermi_levels
                cbm = self%fermi_levels
            else
                vbm(ispin) = max(vbm(ispin), eigenvalues(ivbm, ikpt, ispin))
                cbm(ispin) = min(cbm(ispin), eigenvalues(icbm, ikpt, ispin))
            end if

            noccupied = max(noccupied, ivbm)
        end do
    end do outer_loop
end subroutine calculate_vbm_cbm


function calculate_nr_of_electrons(self, eigenvalues, ikpt, ispin) result(n)
    !! Integrates the occupation numbers to get the number of electrons.
    !! Updates self%occupations and self%weighted_occupations in the process.
    class(Occupations_type), intent(inout) :: self
    real(dp), dimension(:), intent(in) :: eigenvalues
        !! eigenvalues associated with for a (k-point, spin channel) pair
    integer, intent(in) :: ikpt
        !! k-point index
    integer, intent(in) :: ispin
        !! spin channel index
    real(dp) :: n

    self%occupations(:, ikpt, ispin) = self%smearing%calculate_occupation( &
                                                eigenvalues(:), &
                                                self%fermi_levels(ispin))
    call self%calculate_weighted_occupations(ikpt, ispin)
    n = sum(self%weighted_occupations(:, ikpt, ispin))
end function calculate_nr_of_electrons


subroutine calculate_weighted_occupations(self, ikpt, ispin)
    !! Calculates self%weighted_occupations(:, ikpt, ispin)
    !! based on self%occupations(:, ikpt, ispin).
    class(Occupations_type), intent(inout) :: self
    integer, intent(in) :: ikpt
        !! k-point index
    integer, intent(in) :: ispin
        !! spin channel index

    self%weighted_occupations(:, ikpt, ispin) = self%kpoint_weights(ikpt) &
                                            * self%spin_weights(ispin) &
                                            * self%occupations(:, ikpt, ispin)
end subroutine calculate_weighted_occupations

end module tibi_occupations
