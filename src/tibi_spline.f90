!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_spline
!! Module for spline-based functions

use tibi_constants, only: dp
use tibi_twobody, only: ExponentialFunction_type, &
                        PairFunction_type
use tibi_utils_math, only: polyval
implicit none


type, abstract :: BatchSpline_type
    !! Abstract class for batch interpolating an integral table
    !! using unicubic splines.
    !!
    !! Child classes will mainly need to take care of initialization.
    real(dp) :: dr
        !! grid spacing
    logical, dimension(:), allocatable :: mask
        !! mask for unpacking to (self%maxsk,) integral array
    integer :: maxsk
        !! total number of (redundant) integral types
    integer :: numint
        !! number of (non-redundant) integral types
    integer :: numr
        !! number of radial grid points
    real(dp) :: r0
        !! first grid point
    real(dp) :: rcut
        !! grid cutoff
    real(dp), dimension(:, :), allocatable :: table
        !! (numint, numr) array of integrals
    contains
    procedure :: adjust_rcut_batch
    procedure :: evaluate_batch
    procedure :: evaluate_batch_nomask
end type BatchSpline_type


type, extends(PairFunction_type) :: CatmullRomSpline_type
    !! For a 'Catmull-Rom' type spline (a uniform cubic spline in Hermite form).
    real(dp), dimension(:, :), allocatable :: coeff
        !! spline coefficients for every grid point
    real(dp) :: dr
        !! grid spacing
    real(dp) :: r0
        !! starting point
    contains
    procedure :: evaluate => evaluate_catmull_rom_spline
    procedure :: get_uniform_spline_segment
    procedure :: build_catmull_rom_coefficients
end type CatmullRomSpline_type


type, extends(PairFunction_type) :: CanonicalSpline_type
    !! For a (possibly non-uniform) spline in canonical form.
    real(dp), dimension(:, :), allocatable :: coeff
        !! spline coefficients for every grid point
    real(dp), dimension(:), allocatable :: r
        !! grid points
    contains
    procedure :: evaluate => evaluate_canonical_spline
end type CanonicalSpline_type


type, extends(PairFunction_type) :: ExpCanonicalSpline_type
    !! For a piecewise function where the first part is an exponential function
    !! and the second part is a uniform spline in canonical form.
    type(ExponentialFunction_type) :: exponential_function
        !! function for the exponential part
    type(CanonicalSpline_type) :: spline_function
        !! function for the spline part
    contains
    procedure :: evaluate => evaluate_exp_canonical_spline
end type ExpCanonicalSpline_type


interface
    ! Procedure(s) implemented in tibi_spline_batch

    module subroutine adjust_rcut_batch(self, eps)
        !! Sets self%rcut to the distance beyond which all integrals
        !! have magnitudes smaller than the given threshold.
        implicit none
        class(BatchSpline_type), intent(inout) :: self
        real(dp), intent(in) :: eps
            !! accuracy threshold
    end subroutine adjust_rcut_batch

    pure module subroutine evaluate_batch(self, r, fval, fder)
        !! Returns all integral values (and optionally derivatives)
        !! at the given distance (one-dimensional output arrays).
        class(BatchSpline_type), intent(in) :: self
        real(dp), intent(in) :: r
            !! interatomic distance
        real(dp), dimension(:), contiguous, intent(out) :: fval
            !! function values
        real(dp), dimension(:), contiguous, intent(out), optional :: fder
            !! first derivative (optional)
    end subroutine evaluate_batch

    pure module subroutine evaluate_batch_nomask(self, r, fval, fder)
        !! Returns all integral values (and optionally derivatives)
        !! at the given distance (one-dimensional output arrays, no mask).
        class(BatchSpline_type), intent(in) :: self
        real(dp), intent(in) :: r
            !! interatomic distance
        real(dp), dimension(:), contiguous, intent(out) :: fval
            !! function values
        real(dp), dimension(:), contiguous, intent(out), optional :: fder
            !! first derivative (optional)
    end subroutine evaluate_batch_nomask

    !! Procedure(s) implemented in submodule tibi_spline_tricubic

    pure module function interpolate_tricubic(x, y, z, flocal) result(fval)
        !! Returns the function value at the given (scaled) coordinates based
        !! on tricubic interpolation of the surrounding function values.
        !!
        !! Based on [Lekien 2005](https://doi.org/10.1002/nme.1296).
        real(dp), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        real(dp), dimension(4, 4, 4), intent(in) :: flocal
            !! set of surrounding function values
        real(dp) :: fval
    end function interpolate_tricubic

    pure module function interpolate_tricubic_1d(n, x, y, z, flocal) &
                                                 result(fval)
        !! Variant of interpolate_tricubic for more efficient evaluation
        !! of a set of points within the same voxel.
        integer, intent(in) :: n
            !! Array length
        real(dp), dimension(n), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        real(dp), dimension(64), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(n) :: fval
    end function interpolate_tricubic_1d

    pure module function interpolate_tricubic_1d_batch(n, m, x, y, z, flocal) &
                                                       result(fval)
        !! Variant of interpolate_tricubic_1d for a multi-valued function.
        integer, intent(in) :: n
            !! length of the scaled coordinate arrays
        integer, intent(in) :: m
            !! number of function values to evaluate
        real(dp), dimension(n), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        real(dp), dimension(m, 64), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(m, n) :: fval
    end function interpolate_tricubic_1d_batch

    pure module function interpolate_tricubic_batch(x, y, z, n, flocal) result(fval)
        !! Variant of interpolate_tricubic for a multi-valued function.
        real(dp), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        integer, intent(in) :: n
            !! number of function values to evaluate
        real(dp), dimension(n, 4, 4, 4), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(n) :: fval
    end function interpolate_tricubic_batch

    pure module function interpolate_tricubic_deriv(x, y, z, flocal) &
                                                    result(fder)
        !! Returns the function value derivatives at the given (scaled)
        !! coordinates based on tricubic interpolation of the surrounding
        !! function values.
        real(dp), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        real(dp), dimension(4, 4, 4), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(3) :: fder
    end function interpolate_tricubic_deriv

    pure module function interpolate_tricubic_deriv_batch(x, y, z, n, flocal) &
                                                          result(fder)
        !! Variant of interpolate_trilinear_deriv for a multi-valued function.
        real(dp), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        integer, intent(in) :: n
            !! number of function values to evaluate
        real(dp), dimension(n, 4, 4, 4), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(n, 3) :: fder
    end function interpolate_tricubic_deriv_batch

    !! Procedure(s) implemented in submodule tibi_spline_trilinear

    pure module function interpolate_trilinear(x, y, z, flocal) result(fval)
        !! Returns the function value at the given (scaled) coordinates based
        !! on trilinear interpolation of the surrounding function values.
        real(dp), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        real(dp), dimension(2, 2, 2), intent(in) :: flocal
            !! set of surrounding function values
        real(dp) :: fval
    end function interpolate_trilinear

    pure module function interpolate_trilinear_batch(x, y, z, n, flocal) &
                                                     result(fval)
        !! Variant of interpolate_trilinear for a multi-valued function.
        real(dp), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        integer, intent(in) :: n
            !! number of function values to evaluate
        real(dp), dimension(n, 2, 2, 2), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(n) :: fval
    end function interpolate_trilinear_batch

    pure module function interpolate_trilinear_deriv(x, y, z, flocal) &
                                                     result(fder)
        !! Returns the function value derivatives at the given (scaled)
        !! coordinates based on trilinear interpolation of the surrounding
        !! function values.
        real(dp), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        real(dp), dimension(2, 2, 2), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(3) :: fder
    end function interpolate_trilinear_deriv

    pure module function interpolate_trilinear_deriv_batch(x, y, z, n, flocal) &
                                                           result(fder)
        !! Variant of interpolate_trilinear_deriv for a multi-valued function.
        real(dp), intent(in) :: x, y, z
            !! scaled x/y/z coordinates within the central voxel
        integer, intent(in) :: n
            !! number of function values
        real(dp), dimension(n, 2, 2, 2), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(n, 3) :: fder
    end function interpolate_trilinear_deriv_batch

    !! Procedure(s) implemented in submodule tibi_spline_unicubic

    pure module function interpolate_unicubic(x, flocal) result(fval)
        !! Returns the function value at the given (scaled) coordinates based
        !! on unicubic interpolation of the surrounding function values.
        real(dp), intent(in) :: x
            !! scaled x coordinate within the central voxel
        real(dp), dimension(4), intent(in) :: flocal
            !! set of surrounding function values
        real(dp) :: fval
    end function interpolate_unicubic

    pure module function interpolate_unicubic_batch(x, n, flocal) result(fval)
        !! Variant of interpolate_unicubic for a multi-valued function.
        real(dp), intent(in) :: x
            !! scaled x coordinate within the central voxel
        integer, intent(in) :: n
            !! number of function values to evaluate
        real(dp), dimension(n, 4), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(n) :: fval
    end function interpolate_unicubic_batch

    pure module function interpolate_unicubic_deriv(x, flocal) result(fder)
        !! Returns the function value derivative at the given (scaled)
        !! coordinates based on unicubic interpolation of the surrounding
        !! function values.
        real(dp), intent(in) :: x
            !! scaled x coordinate within the central voxel
        real(dp), dimension(4), intent(in) :: flocal
            !! set of surrounding function values
        real(dp) :: fder
    end function interpolate_unicubic_deriv

    pure module function interpolate_unicubic_deriv_batch(x, n, flocal) &
                                                          result(fder)
        !! Variant of interpolate_unicubic_deriv for a multi-valued function.
        real(dp), intent(in) :: x
            !! scaled x coordinate within the central voxel
        integer, intent(in) :: n
            !! number of function values to evaluate
        real(dp), dimension(n, 4), intent(in) :: flocal
            !! set of surrounding function values
        real(dp), dimension(n) :: fder
    end function interpolate_unicubic_deriv_batch
end interface


contains


elemental function get_uniform_spline_segment(self, r) result(index)
    !! Returns the index of the segment of the uniform spline
    !! which straddles the given distance.
    !!
    !! Does not check whether this coordinate is within the
    !! bounds for which the spline has been constructed.
    class(CatmullRomSpline_type), intent(in) :: self
    real(dp), intent(in) :: r
        !! distance
    integer :: index

    index = ceiling((r - self%r0) / self%dr)
end function get_uniform_spline_segment


subroutine build_catmull_rom_coefficients(self, y)
    !! Determines the coefficients of a Catmull-Rom uniform spline.
    !!
    !! self%coeff(:, 1) will be the function values y on the regular grid
    !!                  (including the values at the start and end points).
    !!
    !! self%coeff(:, 2) are the tangents; these will be calculated here
    !!                  using forward backward) difference at the start
    !!                  (end) points and finite difference in between.
    class(CatmullRomSpline_type), intent(inout) :: self
    real(dp), dimension(:), intent(in) :: y
        !! function values

    integer :: i, n

    n = size(y)

    if (allocated(self%coeff)) then
        deallocate(self%coeff)
    end if

    allocate(self%coeff(n, 2))

    self%coeff(1, 1) = y(1)
    self%coeff(1, 2) = (y(2) - y(1)) / self%dr

    self%coeff(n, 1) = y(n)
    self%coeff(n, 2) = (y(n) - y(n-1)) / self%dr

    do i = 2, n-1
        self%coeff(i, 1) = y(i)
        self%coeff(i, 2) = (y(i+1) - y(i-1)) / (2*self%dr)
    end do
end subroutine build_catmull_rom_coefficients


elemental function evaluate_catmull_rom_spline(self, r, der) result(fval)
    !! Evaluates a Catmull-Rom type spline (a cubic spline in Hermite form).
    class(CatmullRomSpline_type), intent(in) :: self
    real(dp), intent(in) :: r
        !! distance
    integer, intent(in) :: der
        !! derivative wrt r
    real(dp) :: fval

    integer :: i
    real(dp) :: h00, h10, h01, h11, t

    fval = 0._dp
    if (r >= self%r0 .and. r <= self%rcut) then
        i = self%get_uniform_spline_segment(r)
        t = (r - self%r0 - (i - 1) * self%dr) / self%dr

        if (der == 0) then
            h00 = 2 * t**3 - 3 * t**2 + 1
            h01 = 1 - h00
            h10 = t**3 - 2 * t**2 + t
            h11 = t**3 - t**2
        elseif (der == 1) then
            h00 = 6 * t**2 - 6 * t
            h01 = -h00
            h10 = 3 * t**2 - 4 * t + 1
            h11 = 3 * t**2 - 2 * t
        end if

        fval = h00 * self%coeff(i, 1) + h01 * self%coeff(i+1, 1) + &
               h10 * self%coeff(i, 2) * self%dr + &
               h11 * self%coeff(i+1, 2) * self%dr

        if (der == 1) then
            ! Chain rule; derivative of t with respect to r
            fval = fval / self%dr
        end if
    end if
end function evaluate_catmull_rom_spline


elemental function evaluate_canonical_spline(self, r, der) result(fval)
    !! Evaluates a spline in canonical form.
    class(CanonicalSpline_type), intent(in) :: self
    real(dp), intent(in) :: r
        !! distance
    integer, intent(in) :: der
        !! derivative wrt r
    real(dp) :: fval

    integer :: i, j, n

    fval = 0._dp
    n = size(self%r)

    if (r >= self%r(1) .and. r <= self%rcut) then
        ! Find the right segment
        do i = 1, n
            if (i == n) exit
            if (self%r(i+1) > r) exit
        end do

        do j = 1, size(self%coeff, 1)
           fval = fval + self%coeff(j, i) * polyval(r-self%r(i), j-1, der)
        end do
    end if
end function evaluate_canonical_spline


elemental function evaluate_exp_canonical_spline(self, r, der) result(fval)
    !! Evaluates a piecewise function where the first part is an exponential
    !! function and the second part is a uniform spline in canonical form.
    class(ExpCanonicalSpline_type), intent(in) :: self
    real(dp), intent(in) :: r
        !! distance
    integer, intent(in) :: der
        !! derivative wrt r
    real(dp) :: fval

    if (r < self%spline_function%r(1)) then
        fval = self%exponential_function%evaluate(r, der)
    else
        fval = self%spline_function%evaluate(r, der)
    end if
end function evaluate_exp_canonical_spline

end module tibi_spline
