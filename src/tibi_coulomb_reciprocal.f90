!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_coulomb_reciprocal
!! Base module for Coulomb interactions in reciprocal space

use tibi_atoms, only: Atoms_type
use tibi_calculator, only: Calculator_type
use tibi_constants, only: dp, &
                          pi
implicit none


type, abstract, extends(Calculator_type) :: ReciprocalCoulombCalculator_type
    !! Abstract calculator for evaluating the reciprocal space term
    !! for Coulomb (used as part of Ewald summation, see [Ewald (1921)](
    !! https://doi.org/10.1002/andp.19213690304))
    real(dp) :: ewald_alpha
        !! Ewald screening parameter (should be > 0)
    real(dp) :: gcut
        !! cutoff for the norms of the G vectors used in the reciprocal sums
    real(dp), dimension(:, :), allocatable :: gvectors
        !! (3, nG) array of reciprocal lattice vectors
    real(dp) :: max_cell_change = 1e-9_dp
        !! maximal change in cell vector components before rebuilding G vectors
    real(dp), dimension(3, 3) :: original_cell = 0._dp
        !! cell vectors from the last build of the G vectors
    contains
    procedure :: determine_gcut
    procedure :: initialize_gvectors
    procedure :: need_lattice_update
    procedure(add_local_potentials), deferred :: add_local_potentials
    procedure(calculate_local_potentials), deferred :: &
                                           calculate_local_potentials
    procedure :: update_gvectors
    procedure(update_moments), deferred :: update_moments
    procedure(update_geometry), deferred :: update_geometry
end type ReciprocalCoulombCalculator_type


interface
    pure subroutine add_local_potentials(self, locpot)
        !! Adds, for each charge moment, the energy derivative w.r.t.
        !! that charge moment (e.g. the electrostatic potential in case
        !! of a charge monopole).
        import dp, ReciprocalCoulombCalculator_type
        implicit none
        class(ReciprocalCoulombCalculator_type), intent(in) :: self
        real(dp), dimension(:), intent(inout) :: locpot
    end subroutine add_local_potentials

    subroutine calculate_local_potentials(self)
        !! Calculates the energy derivatives w.r.t. the charge moments.
        import ReciprocalCoulombCalculator_type
        implicit none
        class(ReciprocalCoulombCalculator_type), intent(inout) :: self
    end subroutine calculate_local_potentials

    subroutine update_geometry(self, atoms, tolerance)
        !! Updates the calculator parts which depend on the atomic positions
        !! and cell vectors.
        import Atoms_type, dp, ReciprocalCoulombCalculator_type
        implicit none
        class(ReciprocalCoulombCalculator_type), intent(inout) :: self
        type(Atoms_type), intent(in) :: atoms
        real(dp), intent(in) :: tolerance
            !! accuracy threshold
    end subroutine update_geometry

    pure subroutine update_moments(self, moments)
        !! Updates the calculator with respect to a new set of charge moments.
        import dp, ReciprocalCoulombCalculator_type
        implicit none
        class(ReciprocalCoulombCalculator_type), intent(inout) :: self
        real(dp), dimension(:), intent(in) :: moments
    end subroutine update_moments
end interface


contains


subroutine determine_gcut(self, atoms, tolerance)
    !! Sets self%gcut based on the given tolerance.
    !!
    !! Gcut is chosen such that G vectors beyond this (spherical) cutoff
    !! would give an energy contribution that is less than the specified
    !! tolerance, for a pair of unit charges.
    !!
    !! Gcut is hence determined by solving the following equation:
    !! \( \mathrm{tol} = \frac{4\pi}{V\,G_{cut}^2}
    !!                   \exp \left( -\frac{G_{cut}^2}{4\alpha^2} \right) \).
    class(ReciprocalCoulombCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), intent(in) :: tolerance

    real(dp) :: gam, gcut, volume

    volume = atoms%get_volume()
    gcut = 0._dp
    gam = 2._dp * tolerance

    do while (gam > tolerance)
        gcut = gcut + 2 * pi / volume**(1._dp / 3._dp)
        gam = 4._dp * pi / (volume * gcut**2) &
              * exp(-gcut**2 / (4._dp * self%ewald_alpha**2))
    end do

    self%gcut = gcut
end subroutine determine_gcut


subroutine initialize_gvectors(self, atoms)
    !! Generate the needed reciprocal lattice vectors G,
    !! taking inversion symmetry into account
    class(ReciprocalCoulombCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: g1, g2, g3, i, ig, ngmax
    integer, dimension(3) :: gmax
    real(dp), dimension(3) :: gvec
    real(dp), dimension(:, :), allocatable :: gvectors

    do i = 1, 3
        gmax(i) = ceiling(self%gcut / (2._dp * pi &
                          * norm2(atoms%invcell(i, :))))
    end do

    ngmax = (product(2 * gmax(:) + 1) - 1) / 2
    allocate(gvectors(3, ngmax))

    ig = 0
    do g1 = 0, gmax(1)
        do g2 = -gmax(2), gmax(2)
            do g3 = -gmax(3), gmax(3)
                if (g1 == 0) then
                    if (g2 < 0) cycle
                    if (g2 == 0 .and. g3 <= 0) cycle
                end if

                gvec(:) = 2._dp * pi * matmul([g1, g2, g3], atoms%invcell)
                if (norm2(gvec) > (self%gcut + epsilon(1._dp))) cycle

                ig = ig + 1
                gvectors(:, ig) = gvec
            end do
        end do
    end do

    if (allocated(self%gvectors)) deallocate(self%gvectors)
    self%gvectors = gvectors(:, 1:ig)
end subroutine initialize_gvectors


function need_lattice_update(self, atoms) result(is_needed)
    !! Decides whether an update of reciprocal lattice properties is needed,
    !! based on the largest allowed cell vector change.
    class(ReciprocalCoulombCalculator_type), intent(in) :: self
    type(Atoms_type), intent(in) :: atoms
    logical :: is_needed

    real(dp) :: max_change

    max_change = maxval(abs(self%original_cell - atoms%cell))
    is_needed = max_change > self%max_cell_change
end function need_lattice_update


subroutine update_gvectors(self, atoms, tolerance)
    !! Updates the set of reciprocal lattice vectors.
    class(ReciprocalCoulombCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms
    real(dp), intent(in) :: tolerance
        !! parameter controlling the accuracy of the Ewald sum

    call self%determine_gcut(atoms, tolerance)
    call self%initialize_gvectors(atoms)
    self%original_cell = atoms%cell
end subroutine update_gvectors

end module tibi_coulomb_reciprocal
