!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_fixedpoint_pulay
!! Module for fixed-point iteration using the Pulay method

use tibi_blas, only: ddot
use tibi_constants, only: dp
use tibi_fixedpoint_base, only: FixedPointBase_type
use tibi_lapack, only: dsysv
use tibi_utils_prog, only: assert
implicit none


type, extends(FixedPointBase_type) :: FixedPointPulay_type
    !! Class for the [Pulay method](https://doi.org/10.1002/jcc.540030413),
    !! also known as direct inversion in the iterative subspace (DIIS).
    !!
    !! The [r-Pulay](https://dx.doi.org/10.1016/j.cplett.2015.06.029) variant
    !! with periodic restarts is also available.
    logical :: apply_periodic_restart = .false.
        !! whether to apply the periodically restarted ("r-Pulay") variant
        !! instead of the original method
    real(dp) :: damping = 0.25_dp
        !! damping coefficient (corresponds to a linear mixing fraction
        !! in the first iteration)
    real(dp), dimension(:, :), allocatable :: error_products
        !! (max_vectors, max_vectors) array with the error vector dot products
    real(dp), dimension(:, :), allocatable :: error_vectors
        !! (vector_length, max_vectors) array with the most recent error
        !! vectors (difference between the output and input vectors)
    real(dp), dimension(:, :), allocatable :: input_vectors
        !! (vector_length, max_vectors) array with the input vectors
    integer :: iter
        !! iteration counter (reset to 1 at every reset or restart)
    integer :: max_vectors = 8
        !! maximal number of vectors to be stored
    integer :: num_vectors
        !! number of stored vectors
    integer :: oldest_vector_index
        !! index of the oldest error and input vectors
    real(dp) :: precon = 1e-6_dp
        !! value determining the amount of preconditioning applied to the
        !! Pulay coefficient matrix, for increased numerical stability
    integer :: restart_history = 1
        !! number of latest vectors (if any) to keep when applying a restart
    contains
    procedure :: initialize
    procedure :: add_input_vector => add_input_vector_pulay
    procedure :: add_output_vector => add_output_vector_pulay
    procedure :: get_max_abs_diff => get_max_abs_diff_pulay
    procedure :: get_newest_index
    procedure :: get_next_vector => get_next_vector_pulay
    procedure :: get_weights
    procedure :: reset => reset_pulay
    procedure :: restart
    procedure :: update_error_products
    procedure :: wrap_vector_index
end type FixedPointPulay_type


contains


subroutine initialize(self, vector_length, max_vectors, damping, precon, &
                      apply_periodic_restart, restart_history)
    !! Initializes the Pulay fixed-point iterator, which includes allocating
    !! and resetting the arrays holding the recent iteration history.
    class(FixedPointPulay_type), intent(inout) :: self
    integer, intent(in) :: vector_length
        !! length of the individual error and input vectors
    integer, intent(in), optional :: max_vectors
        !! maximal number of vectors to be stored
    real(dp), intent(in), optional :: damping
        !! damping coefficient (corresponds to a linear mixing fraction
        !! in the first iteration)
    real(dp), intent(in), optional :: precon
        !! preconditioning factor
    logical, intent(in), optional :: apply_periodic_restart
        !! whether to periodically clean the history
    integer, intent(in), optional :: restart_history
        !! number of latest vectors (if any) to keep when applying a restart

    if (present(max_vectors)) then
        self%max_vectors = max_vectors
    end if

    if (present(damping)) then
        self%damping = damping
    end if

    if (present(precon)) then
        self%precon = precon
    end if

    if (present(apply_periodic_restart)) then
        self%apply_periodic_restart = apply_periodic_restart
    end if

    if (present(restart_history)) then
        self%restart_history = restart_history
    end if

    if (self%apply_periodic_restart) then
        call assert(self%restart_history >= 0, &
                    'Pulay restart_history must larger or equal than 0')
        call assert(self%restart_history < self%max_vectors, &
                    'Number of latest Pulay vectors to keep at restarts ' // &
                    'be smaller than the maximum number of Pulay vectors')
    end if

    self%vector_length = vector_length

    allocate(self%error_products(self%max_vectors, self%max_vectors))
    allocate(self%error_vectors(self%vector_length, self%max_vectors))
    allocate(self%input_vectors(self%vector_length, self%max_vectors))

    call self%reset()
end subroutine initialize


pure subroutine add_input_vector_pulay(self, vector)
    !! Adds a new vector to the list of input vectors.
    !!
    !! If the maximum number of vectors has already been reached,
    !! the oldest vector is replaced by the new one.
    class(FixedPointPulay_type), intent(inout) :: self
    real(dp), dimension(self%vector_length), intent(in) :: vector
        !! new input vector to be added

    if (self%apply_periodic_restart) then
        if (self%iter == self%max_vectors + 1) then
            call self%restart()
        end if
    end if

    self%input_vectors(:, self%oldest_vector_index) = vector(:)
end subroutine add_input_vector_pulay


pure subroutine add_output_vector_pulay(self, vector)
    !! Accepts a new output vector and uses it to update the list
    !! of error vectors.
    !!
    !! If the maximum number of vectors has already been reached,
    !! the oldest vector is replaced by the new one.
    class(FixedPointPulay_type), intent(inout) :: self
    real(dp), dimension(self%vector_length), intent(in) :: vector
        !! new output vector to be processed

    self%error_vectors(:, self%oldest_vector_index) = vector(:) &
        - self%input_vectors(:, self%oldest_vector_index)

    self%num_vectors = self%num_vectors + 1
    call self%update_error_products()

    self%oldest_vector_index = self%oldest_vector_index + 1
    self%oldest_vector_index = self%wrap_vector_index(self%oldest_vector_index)
end subroutine add_output_vector_pulay


pure function get_max_abs_diff_pulay(self) result(mad)
    !! Returns the maximum absolute difference between the last pair
    !! of input and output vectors.
    class(FixedPointPulay_type), intent(in) :: self
    real(dp) :: mad

    integer :: newest_index

    newest_index = self%get_newest_index()
    mad = maxval(abs(self%error_vectors(:, newest_index)))
end function get_max_abs_diff_pulay


pure function get_newest_index(self) result(index)
    !! Returns the index at which to find the most recently added
    !! error and input vectors.
    class(FixedPointPulay_type), intent(in) :: self
    integer :: index

    index = self%wrap_vector_index(self%oldest_vector_index-1)
end function get_newest_index


function get_next_vector_pulay(self) result(vector)
    !! Returns an input vector for the next fixed-point iteration
    !! and increments the iteration counter.
    class(FixedPointPulay_type), intent(inout) :: self
    real(dp), dimension(self%vector_length) :: vector

    integer :: ivec
    real(dp), dimension(self%max_vectors) :: weights

    weights(:) = self%get_weights()
    vector(:) = 0._dp

    do ivec = 1, self%max_vectors
        if (abs(weights(ivec)) > epsilon(1._dp)) then
            vector(:) = vector(:) + weights(ivec) &
                        * (self%input_vectors(:, ivec) &
                           + self%damping * self%error_vectors(:, ivec))
        end if
    end do

    self%iter = self%iter + 1
end function get_next_vector_pulay


function get_weights(self) result(weights)
    !! Returns the weights of the self%max_vectors most recent iterations
    !! which minimize the corresponding weighted sum of the residual norms,
    !! under the constraint that the weights sum up to 1.
    class(FixedPointPulay_type), intent(in) :: self
    real(dp), dimension(self%max_vectors) :: weights

    integer :: info, ivec, lwork, m
    integer, dimension(:), allocatable :: pivots
    real(dp), dimension(:), allocatable :: work
    real(dp), dimension(:, :), allocatable :: A, b

    m = min(self%num_vectors, self%max_vectors)
    allocate(pivots(m+1))
    allocate(A(m+1, m+1))
    allocate(b(m+1, 1))

    b(1:m, 1) = 0._dp
    b(m+1, 1) = -1._dp

    A(1:m, 1:m) = self%error_products(1:m, 1:m)

    if (all(abs(A(1:m, 1:m)) < 1e-12_dp)) then
        ! pathological case, use identity matrix
        A(1:m, 1:m) = 0._dp
        do ivec = 1, m
            A(ivec, ivec) = 1._dp
        end do
    end if

    do ivec = 1, m
        A(ivec, ivec) = A(ivec, ivec) * (1._dp + self%precon)
    end do

    A(1:m, m+1) = -1._dp
    A(m+1, m+1) = 0._dp

    ! Workspace allocation
    lwork = -1
    allocate(work(1))
    call dsysv('U', m+1, 1, A(:, :), m+1, pivots(:), b(:, :), m+1, &
               work, lwork, info)
    lwork = int(work(1))
    deallocate(work)
    allocate(work(lwork))

    ! Actual solver call
    call dsysv('U', m+1, 1, A(:, :), m+1, pivots(:), b(:, :), m+1, &
               work, lwork, info)
    call assert(info == 0, 'Unable to solve Pulay equations')

    weights(:) = 0._dp
    weights(1:m) = b(1:m, 1)
end function get_weights


pure subroutine reset_pulay(self)
    !! Wipes the history of input and error vectors and resets the counters.
    class(FixedPointPulay_type), intent(inout) :: self

    self%error_products(:, :) = 0._dp
    self%error_vectors(:, :) = 0._dp
    self%input_vectors(:, :) = 0._dp
    self%oldest_vector_index = 1
    self%iter = 1
    self%num_vectors = 0
end subroutine reset_pulay


pure subroutine restart(self)
    !! Restarts the iterator, keeping only the last self%restart_history
    !! entries.
    class(FixedPointPulay_type), intent(inout) :: self

    integer :: m, n

    n = self%restart_history

    if (n == 0) then
        call self%reset()
    else
        m = self%max_vectors

        self%error_products(1:n, 1:n) = self%error_products(m-n+1:m, m-n+1:m)
        self%error_products(n+1:m, 1:m) = 0._dp
        self%error_products(1:m, n+1:m) = 0._dp

        self%error_vectors(:, 1:n) = self%error_vectors(:, m-n+1:m)
        self%error_vectors(:, n+1:m) = 0._dp

        self%input_vectors(:, 1:n) = self%input_vectors(:, m-n+1:m)
        self%input_vectors(:, n+1:m) = 0._dp

        self%oldest_vector_index = n + 1
        self%iter = 1
        self%num_vectors = n
    end if
end subroutine restart


pure subroutine update_error_products(self)
    !! Updates the upper triangular part of the error dot product matrix,
    !! to be called after a new error vector has been added at the index
    !! self%oldest_vector_index.
    class(FixedPointPulay_type), intent(inout) :: self

    integer :: ivec, m

    m = min(self%num_vectors, self%max_vectors)

    do ivec = 1, m
        self%error_products(ivec, self%oldest_vector_index) = &
                ddot(self%vector_length, &
                     self%error_vectors(:, self%oldest_vector_index), 1, &
                     self%error_vectors(:, ivec), 1)
    end do
end subroutine update_error_products


pure function wrap_vector_index(self, index) result(wrapped_index)
    !! Wraps the index back into the [1, self%max_vectors] interval.
    class(FixedPointPulay_type), intent(in) :: self
    integer, intent(in) :: index
    integer :: wrapped_index

    wrapped_index = modulo(index, self%max_vectors)

    if (wrapped_index == 0) then
        wrapped_index = self%max_vectors
    end if
end function wrap_vector_index

end module tibi_fixedpoint_pulay
