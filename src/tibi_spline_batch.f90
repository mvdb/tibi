!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

submodule(tibi_spline) tibi_spline_batch
!! Submodule for batch (cubic) spline interpolation for [[tibi_spline]]

implicit none


integer, dimension(4, 4), parameter :: AinvB_T = transpose(reshape( &
    [0, -1, 2, -1, 2, 0, -5, 3, 0, 1, 4, -3, 0, 0, -1, 1], [4, 4]))
    !! product of the inverse of the interpolation coefficient matrix
    !! and the finite difference stencil, times 2 (transposed)


contains


module subroutine adjust_rcut_batch(self, eps)
    !! Sets self%rcut to the distance beyond which all integrals
    !! have magnitudes smaller than the given threshold.
    class(BatchSpline_type), intent(inout) :: self
    real(dp), intent(in) :: eps

    integer :: ir
    real(dp) :: fval

    do ir = self%numr, 2, -1
        fval = maxval(abs(self%table(:, ir)))
        if (fval > eps) then
            self%rcut = self%r0 + (ir - 1) * self%dr
            exit
        end if
    end do
end subroutine adjust_rcut_batch


pure module subroutine evaluate_batch(self, r, fval, fder)
    !! Returns all integral values (and optionally derivatives)
    !! at the given distance (one-dimensional output arrays).
    class(BatchSpline_type), intent(in) :: self
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:), contiguous, intent(out) :: fval
        !! function values
    real(dp), dimension(:), contiguous, intent(out), optional :: fder
        !! first derivative (optional)

    integer :: i, j, ix
    real(dp) :: invdr, x
    real(dp), dimension(4) :: kder, kval
    real(dp), dimension(self%numint) :: tmpder, tmpval

    ix = ceiling((r - self%r0) / self%dr)

    if (ix < 2 .or. ix > self%numr-2) then
        fval = 0._dp
        if (present(fder)) fder = 0._dp
        return
    end if

    invdr = 1._dp / self%dr
    x = (r - self%r0 - (ix - 1) * self%dr) * invdr
    kval = 0.5_dp * [1._dp, x, x**2, x**3]
    kval = matmul(AinvB_T, kval)

    if (present(fder)) then
        kder = 0.5_dp *  [0._dp, 1._dp, 2._dp*x, 3._dp*x**2] * invdr
        kder = matmul(AinvB_T, kder)

        do i = 1, self%numint
            tmpval(i) = sum(self%table(i, ix-1:ix+2) * kval)
            tmpder(i) = sum(self%table(i, ix-1:ix+2) * kder)
        end do

        j = 1
        do i = 1, self%maxsk
            if (self%mask(i)) then
                fval(i) = tmpval(j)
                fder(i) = tmpder(j)
                j = j + 1
            else
                fval(i) = 0._dp
                fder(i) = 0._dp
            end if
        end do
    else
        do i = 1, self%numint
            tmpval(i) = sum(self%table(i, ix-1:ix+2) * kval)
        end do

        j = 1
        do i = 1, self%maxsk
            if (self%mask(i)) then
                fval(i) = tmpval(j)
                j = j + 1
            else
                fval(i) = 0._dp
            end if
        end do
    end if
end subroutine evaluate_batch


pure module subroutine evaluate_batch_nomask(self, r, fval, fder)
    !! Returns all integral values (and optionally derivatives)
    !! at the given distance (one-dimensional output arrays, no mask).
    class(BatchSpline_type), intent(in) :: self
    real(dp), intent(in) :: r
        !! interatomic distance
    real(dp), dimension(:), contiguous, intent(out) :: fval
        !! function values
    real(dp), dimension(:), contiguous, intent(out), optional :: fder
        !! first derivative (optional)

    integer :: i, ix
    real(dp) :: invdr, x
    real(dp), dimension(4) :: kder, kval

    ix = ceiling((r - self%r0) / self%dr)

    if (ix < 2 .or. ix > self%numr-2) then
        fval = 0._dp
        if (present(fder)) fder = 0._dp
        return
    end if

    invdr = 1._dp / self%dr
    x = (r - self%r0 - (ix - 1) * self%dr) * invdr
    kval = 0.5_dp * [1._dp, x, x**2, x**3]
    kval = matmul(AinvB_T, kval)

    if (present(fder)) then
        kder = 0.5_dp *  [0._dp, 1._dp, 2._dp*x, 3._dp*x**2] * invdr
        kder = matmul(AinvB_T, kder)

        do i = 1, self%numint
            fval(i) = sum(self%table(i, ix-1:ix+2) * kval)
            fder(i) = sum(self%table(i, ix-1:ix+2) * kder)
        end do
    else
        do i = 1, self%numint
            fval(i) = sum(self%table(i, ix-1:ix+2) * kval)
        end do
    end if
end subroutine evaluate_batch_nomask

end submodule tibi_spline_batch
