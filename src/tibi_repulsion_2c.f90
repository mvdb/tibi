!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_repulsion_2c
!! Module for two-body repulsive interactions

use iso_fortran_env, only: iostat_end
use tibi_atoms, only: Atoms_type
use tibi_constants, only: dp
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_neighborlist_verlet, only: NeighborListVerlet_type
use tibi_realspace_pair, only: RealSpacePairCalculator_type
use tibi_spline, only: ExpCanonicalSpline_type
use tibi_twobody, only: PairFunction_type, &
                        PolynomialFunction_type
use tibi_utils_math, only: arange
use tibi_utils_prog, only: assert, &
                           is_master
use tibi_utils_string, only: count_items, &
                             expand_multiplications, &
                             int2str, &
                             lower_case, &
                             replace_tabs_by_whitespace
implicit none


type, extends(RealSpacePairCalculator_type) :: Repulsion2cCalculator_type
    !! A calculator for two-body interatomic repulsion interactions
    character(len=16) :: repulsion_2c_form = 'polynomial'
        !! functional form of the repulsion
    character(len=128) :: tbpar_dir = './'
        !! path to the directory with the tight-binding parameter files
    class(PairFunction_type),  dimension(:, :), allocatable :: vrep
        !! functions for evaluating the interatomic repulsion
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize
    procedure :: initialize_neighborlist
    procedure :: evaluate_pair
    procedure :: read_all_repulsive_interactions
end type Repulsion2cCalculator_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(Repulsion2cCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('repulsion_2c_form')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%repulsion_2c_form
        call lower_case(self%repulsion_2c_form)
        call assert(trim(self%repulsion_2c_form) == 'polynomial' .or. &
                    trim(self%repulsion_2c_form) == 'spline', &
                    'Unknown repulsion form: ' // self%repulsion_2c_form)
    end if

    buffer = input%fetch_keyword('tbpar_dir')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%tbpar_dir
    end if
end subroutine fetch_input_keywords


subroutine initialize(self, input, atoms)
    !! Initializes the Repulsion2cCalculator from the given instances of
    !! [[tibi_input:Input_type]] and [[tibi_atoms:Atoms_type]].
    class(Repulsion2cCalculator_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms

    call self%fetch_input_keywords(input)
    call self%read_all_repulsive_interactions(atoms)
    call self%initialize_neighborlist(atoms)
    self%description = 'Two-center repulsive'
end subroutine initialize


subroutine initialize_neighborlist(self, atoms)
    !! Sets up the neighborlist.
    class(Repulsion2cCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: ik, jk
    real(dp), dimension(atoms%nkind, atoms%nkind) :: rcuts

    do ik = 1, atoms%nkind
        do jk = 1, atoms%nkind
            rcuts(ik, jk) = self%vrep(ik, jk)%rcut
        end do
    end do

    self%nl = NeighborListVerlet_type(atoms, rcuts, both_ways=.false., &
                                      self_interaction=.false.)
end subroutine initialize_neighborlist


elemental function evaluate_pair(self, iat, jat, r, der) result(fval)
    !! Evaluates the pairwise interaction.
    class(Repulsion2cCalculator_type), intent(in) :: self
    integer, intent(in) :: iat, jat
        !! the atomic indices
    real(dp), intent(in) :: r
        !! the interatomic distance
    integer, intent(in) :: der
        !! the derivative index (0 or 1)
    real(dp) :: fval

    integer :: ik, jk

    ik = self%nl%original_kinds(iat)
    jk = self%nl%original_kinds(jat)
    fval = self%vrep(ik, jk)%evaluate(r, der)
end function evaluate_pair


subroutine read_all_repulsive_interactions(self, atoms)
    !! Reads the definitions of the repulsive interactions
    !! from `.skf` files in self%tbpar_dir and
    !! initializes the potentials in self%vrep accordingly.
    class(Repulsion2cCalculator_type), intent(inout) :: self
    type(Atoms_type), intent(in) :: atoms

    integer :: ik, jk
    character(len=:), allocatable :: filename

    if (trim(self%repulsion_2c_form) == 'polynomial') then
        allocate(PolynomialFunction_type :: self%vrep(atoms%nkind, atoms%nkind))
    elseif (trim(self%repulsion_2c_form) == 'spline') then
        allocate(ExpCanonicalSpline_type :: self%vrep(atoms%nkind, atoms%nkind))
    end if

    do ik = 1, atoms%nkind
        do jk = 1, atoms%nkind
            self%vrep(ik, jk)%kinds = [ik, jk]

            filename = trim(self%tbpar_dir) // '/' // trim(atoms%symbols(ik)) &
                       // '-' // trim(atoms%symbols(jk)) // '.skf'

            select type(assoc => self%vrep(ik, jk))
            type is (ExpCanonicalSpline_type)
                if (is_master()) then
                    print *, 'Reading spline Vrep from ' // trim(filename)
                end if
                call read_skf_spline_block(filename, assoc)
            type is (PolynomialFunction_type)
                if (is_master()) then
                    print *, 'Reading polynomial Vrep from ' // trim(filename)
                end if
                call read_skf_poly_line(filename, assoc)
            end select
        end do
    end do

    if (is_master()) print *
end subroutine read_all_repulsive_interactions


subroutine read_skf_poly_line(filename, vrep)
    !! Reads a single line from the given SKF file where the polynomial form
    !! of the repulsive potential is given.
    character(len=*), intent(in) :: filename
    type(PolynomialFunction_type), intent(inout) :: vrep

    integer, parameter :: numpow = 8
    integer :: i, n, counter, fhandle, line_number, status
    character(len=1024) :: buffer
    real(dp) :: dummy
    real(dp), dimension(10) :: dummy_array

    if (vrep%kinds(1) == vrep%kinds(2)) then
        line_number = 3
    else
        line_number = 2
    end if

    allocate(vrep%coeff(numpow))
    vrep%coeff = 0._dp

    allocate(vrep%powers(numpow))
    vrep%powers = arange(2, 2 + numpow - 1, 1)

    open(newunit=fhandle, file=filename, status='old', action='read')

    counter = 1
    read_loop: do
        read(fhandle, '(a)', iostat=status) buffer

        if (status == iostat_end) then
            exit read_loop
        elseif (status == 0) then
            ! Skip comment lines that start with #
            i = index(buffer, '#')
            if (i == 1) then
                cycle
            end if

            call replace_tabs_by_whitespace(buffer)

            if (counter < line_number) then
                ! Do nothing
                continue
            elseif (counter == line_number) then
                call expand_multiplications(buffer)
                n = count_items(buffer)

                if (n == numpow + 2) then
                    read(buffer, *, iostat=status) dummy, vrep%coeff, vrep%rcut
                elseif (n == numpow + 12) then
                    read(buffer, *, iostat=status) dummy, vrep%coeff, &
                                                   vrep%rcut, dummy_array
                else
                    call assert(.false., 'Unexpected number of items (Vrep): ' &
                                // int2str(n))
                end if
            elseif (counter > line_number) then
                exit read_loop
            end if

            counter = counter + 1
            if (status /= 0) then
                print *, buffer
                call assert(.false., 'Problem when reading the above line')
            end if
        else
            exit read_loop
        end if
    end do read_loop

    close(fhandle)
end subroutine read_skf_poly_line


subroutine read_skf_spline_block(filename, vrep)
    !! Reads the block from the given SKF file where the (cubic) spline form
    !! of the repulsive potential is given.
    !!
    !! @NOTE The SKF format does not impose a uniform grid,
    !! and indeed not all distributed parameter files use one.
    character(len=*), intent(in) :: filename
    type(ExpCanonicalSpline_type), intent(inout) :: vrep

    integer :: i, fhandle, line_number, num_segments, status
    character(len=1024) :: buffer
    logical :: flag
    real(dp) :: dummy, rstart, rcut
    real(dp), dimension(6) :: coeff

    flag = .false.  ! whether we have entered in the Spline block

    open(newunit=fhandle, file=filename, status='old', action='read')

    line_number = 1
    read_loop: do
        read(fhandle, '(a)', iostat=status) buffer

        call assert(status /= iostat_end, 'Premature ending when reading ' // &
                    'spline repulsion data from ' // filename)
        call assert(status == 0, 'Problem while reading ' // filename)

        ! Skip comment lines that start with #
        i = index(buffer, '#')
        if (i == 1) then
            cycle
        end if

        call lower_case(buffer)
        i = index(buffer, 'spline')
        if (i > 0) then
            ! Now we enter the spline block
            flag = .true.
            cycle
        end if

        if (.not. flag) then
            cycle

        elseif (line_number == 1) then
            read(buffer, *, iostat=status) num_segments, rcut
            allocate(vrep%spline_function%r(num_segments))
            allocate(vrep%spline_function%coeff(6, num_segments))
            vrep%spline_function%coeff = 0._dp
            vrep%rcut = rcut
            vrep%spline_function%rcut = rcut
            vrep%exponential_function%rcut = rcut

        elseif (line_number == 2) then
            read(buffer, *, iostat=status) coeff(1:3)
            vrep%exponential_function%a1 = coeff(1)
            vrep%exponential_function%a2 = coeff(2)
            vrep%exponential_function%a3 = coeff(3)

        elseif (line_number >= 3 .and. line_number < 2 + num_segments) then
            read(buffer, *, iostat=status) rstart, dummy, coeff(1:4)
            vrep%spline_function%r(line_number - 2) = rstart
            vrep%spline_function%coeff(1:4, line_number - 2) = coeff(1:4)

        elseif (line_number == 2 + num_segments) then
            read(buffer, *, iostat=status) rstart, dummy, coeff(1:6)
            vrep%spline_function%r(line_number - 2) = rstart
            vrep%spline_function%coeff(1:6, line_number - 2) = coeff(1:6)
            exit read_loop
        end if

        call assert(status == 0, 'Problem when reading the following line:' // &
                    new_line('a') // trim(buffer))

        line_number = line_number + 1
    end do read_loop

    close(fhandle)
end subroutine read_skf_spline_block

end module tibi_repulsion_2c
