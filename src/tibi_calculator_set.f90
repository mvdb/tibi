!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

module tibi_calculator_set
!! Module for combining different calculators together

use tibi_atoms, only: Atoms_type
use tibi_calculator, only: Calculator_type
use tibi_constants, only: dp
use tibi_input, only: Input_type, &
                      INPUT_VALUE_LENGTH
use tibi_repulsion, only: RepulsionCalculator_type
use tibi_scf_tightbinding, only: ScfTightBindingCalculator_type
use tibi_tightbinding, only: TightBindingCalculator_type
use tibi_utils_prog, only: is_master
implicit none


type, extends(Calculator_type) :: CalculatorSet_type
    !! A calculator that is itself a combination of different
    !! calculators (for now: [[tibi_repulsion:RepulsionCalculator_type]]
    !! and [[tibi_tightbinding:TightBindingCalculator_type]]).
    !!
    !! @NOTE One can imagine a more general/abstract version
    !! (that only needs to know about a list of calculators,
    !! and about not the calculators themselves), but it seems
    !! difficult to do this in Fortran because array elements
    !! must always be of the same type.
    logical :: include_repulsion = .true.
        !! whether to include an interatomic repulsion calculator
    logical :: include_scf = .false.
        !! whether to include self-consistent corrections
        !! in the tight-binding calculations
    logical :: include_tightbinding = .true.
        !! whether to include a tight-binding electronic structure calculator
    type(RepulsionCalculator_type), allocatable :: repulsion
        !! interatomic repulsion calculator
    type(ScfTightBindingCalculator_type), allocatable :: scf_tightbinding
        !! self-consistent-charge tight-binding electronic structure calculator
    type(TightBindingCalculator_type), allocatable :: tightbinding
        !! tight-binding electronic structure calculator
    contains
    procedure :: fetch_input_keywords
    procedure :: initialize_calculators
    procedure :: calculate_energy
    procedure :: calculate_forces
end type CalculatorSet_type


contains


subroutine fetch_input_keywords(self, input)
    !! Fetches input keywords from an [[tibi_input:Input_type]] object.
    class(CalculatorSet_type), intent(inout) :: self
    type(Input_type), intent(in) :: input

    character(len=INPUT_VALUE_LENGTH) :: buffer

    buffer = input%fetch_keyword('include_repulsion')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%include_repulsion
    end if

    buffer = input%fetch_keyword('include_scf')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%include_scf
    end if

    buffer = input%fetch_keyword('include_tightbinding')
    if (len_trim(buffer) > 0) then
        read(buffer, *) self%include_tightbinding
    end if
end subroutine fetch_input_keywords


subroutine initialize_calculators(self, input, atoms)
    !! Initializes all included calculators.
    class(CalculatorSet_type), intent(inout) :: self
    type(Input_type), intent(in) :: input
    type(Atoms_type), intent(in) :: atoms

    if (self%include_repulsion) then
        allocate(self%repulsion)
        call self%repulsion%initialize(input, atoms)
    end if

    if (self%include_tightbinding) then
        if (self%include_scf) then
            allocate(self%scf_tightbinding)
            call self%scf_tightbinding%initialize(input, atoms)
        else
            allocate(self%tightbinding)
            call self%tightbinding%initialize(input, atoms)
        end if
    end if
end subroutine initialize_calculators


subroutine calculate_energy(self, atoms)
    !! Calculates the (total) energy for the given [[tibi_atoms:Atoms_type]]
    !! object, which is stored in atoms%energy.
    class(CalculatorSet_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms

    real(dp) :: energy

    energy = 0._dp

    if (self%include_repulsion) then
        call self%repulsion%calculate_energy(atoms)
        energy = energy + atoms%energy
    end if

    if (self%include_tightbinding) then
        if (self%include_scf) then
            call self%scf_tightbinding%calculate_energy(atoms)
            energy = energy + atoms%energy
        else
            call self%tightbinding%calculate_energy(atoms)
            energy = energy + atoms%energy
        end if
    end if

    atoms%energy = energy
    if (is_master()) then
        call atoms%print_energy()
    end if
end subroutine calculate_energy


subroutine calculate_forces(self, atoms, do_stress)
    !! Calculates the atomic forces for the given [[tibi_atoms:Atoms_type]]
    !! object, which are stored in atoms%forces.
    class(CalculatorSet_type), intent(inout) :: self
    type(Atoms_type), intent(inout) :: atoms
    logical, intent(in) :: do_stress
        !! whether to also compute the stress tensor for periodic structures

    real(dp), dimension(3, atoms%natom) :: forces
    real(dp), dimension(3, 3) :: stress

    forces(:, :) = 0._dp
    if (do_stress) stress(:, :) = 0._dp

    if (self%include_repulsion) then
        call self%repulsion%calculate_forces(atoms, do_stress=do_stress)
        forces = forces + atoms%forces
        if (do_stress) stress = stress + atoms%stress
    end if

    if (self%include_tightbinding) then
        if (self%include_scf) then
            call self%scf_tightbinding%calculate_forces(atoms, &
                                                        do_stress=do_stress)
            forces = forces + atoms%forces
            if (do_stress) stress = stress + atoms%stress
        else
            call self%tightbinding%calculate_forces(atoms, do_stress=do_stress)
            forces = forces + atoms%forces
            if (do_stress) stress = stress + atoms%stress
        end if
    end if

    atoms%forces = forces
    if (do_stress) atoms%stress = stress

    if (is_master()) then
        call atoms%print_forces()
        if (do_stress) call atoms%print_stress()
    end if
end subroutine calculate_forces

end module tibi_calculator_set
