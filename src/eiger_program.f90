!-----------------------------------------------------------------------------!
!   Tibi: an ab-initio tight-binding electronic structure code                !
!   Copyright 2020-2025 Maxime Van den Bossche                                !
!   SPDX-License-Identifier: GPL-3.0-or-later                                 !
!-----------------------------------------------------------------------------!

program eiger
!! The main Eiger program

use eiger_calc, only: EigerCalculator_type
use tibi_input, only: Input_type
use tibi_timer, only: task_timer, &
                      Timer_type
use tibi_timer_set, only: global_timer
use tibi_utils_prog, only: is_master, &
                           print_date_and_time, &
                           print_program_header, &
                           print_version_string
implicit none


integer :: narg
character(len=5), parameter :: program_name = 'Eiger'
character(len=128) :: input_file, option
type(EigerCalculator_type) :: calc
type(Input_type) :: input
type(Timer_type), target :: main_timer

narg = command_argument_count()

! First the cases with an early exit
if (narg == 1) then
    call get_command_argument(1, option)

    if (trim(option) == '--version') then
        if (is_master()) call print_version_string(program_name)
        stop 0
    elseif (trim(option) == '--help') then
        if (is_master()) call print_help_message()
        stop 0
    end if
else
    if (is_master()) call print_help_message()
    stop 1
end if

! At this point the one given argument refers to an input file
call get_command_argument(1, input_file)

if (is_master()) then
    call print_date_and_time(program_name // ' started')
    call print_program_header(program_name)
    call print_program_banner()
end if

call global_timer%add_timer(main_timer, 'main')
call global_timer%add_timer(task_timer, 'task', parent='main')
call main_timer%add_timer('init')
call main_timer%add_timer('task')

call main_timer%start_timer('init')
call input%read_input_file(input_file)
if (is_master()) then
    call input%print_input_description()
end if
call calc%initialize(input)
call main_timer%stop_timer('init')

call main_timer%start_timer('task')
call calc%run()
call main_timer%stop_timer('task')

if (is_master()) then
    call global_timer%print_timing_tables()
    call print_date_and_time(program_name // ' stopped')
end if


contains


subroutine print_help_message()
    !! Prints out the help message.

    print *, 'Usage: eiger input_file'
    print *, 'Version: eiger --version'
    print *, 'Help: eiger --help'
end subroutine print_help_message


subroutine print_program_banner()
    !! Prints the Eiger program banner.
    print *
    print *, '               _/"""""""""\'
    print *, "             _//           \"
    print *, "            / /             \____"
    print *, "          _/ /               \   \___"
    print *, "        _/  /                 \      \"
    print *, "       /   /   _____ _         \      \__"
    print *, "      /   |   | ____(_) __ _  ___ _ __   \"
    print *, "   __/    |   |  _| | |/ _` |/ _ \ '__|   \"
    print *, "__/      /    | |___| | (_| |  __/ |       \"
    print *, "        |     |_____|_|\__, |\___|_|        \"
    print *, "       /               |___/                 \"
    print *
end subroutine print_program_banner

end program eiger
