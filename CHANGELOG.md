## Changelog

### Version 1.1.0

07 July 2024

* Added support for reading on-site one-center U and W integrals
  from '1cl' files (Mulliken mapping). This can be enabled with
  `scf_ukernel_on1c_model='tabulated'` and `scf_wkernel_on1c='tabulated'`,
  respectively.

* Deprecated the `scf_wkernel_on1c` option (to be removed in the next
  major version). On-site one-center W integrals for use with Mulliken mapping
  can then only be provided via '1cl' files.

* Restructured the evaluation of the one-center U and W contributions.
  As a result, the Tibi output now only contains the total U and W energy
  contributions and the optional `scf_kernel_X.dat` files (X = U/W)
  now also contain the one-center contributions.

* Restructured the evaluation of the H and S contributions and the
  handling of basis set properties.

* The self-consistent corrections to the Hamiltonian and the electronic forces
  in case of Mulliken mapping are now performed in a more efficient way.

* Transposed the content of the `wfncoeff_*.dat` output files
  so that each line indeed contains the wave function coefficients
  for the given band, as suggested by the header.

* Let the divide-and-conquer eigensolver reuse the Cholesky-decomposed
  overlap matrix if possible.

* Added a `diag_gen2std` option to choose whether the eigensolver should
  handle the generalized-to-standard eigenvalue problem transformations
  if possible. By default these get handled by Tibi through LAPACK calls.
  Subtimings for `lcao_solve` are now shown to help assess this.

* Added support for [ELPA](https://elpa.mpcdf.mpg.de/) eigensolvers.

* Added a second executable program (`Eiger`) for easier eigensolver
  performance benchmarking.

* Added a `concur` tool which makes it easier to verify how Tibi
  (or Eiger) performance changes as a function of the number of
  concurrent processes and their CPU affinity (see `tools/concur`).

* Added a Performance manual page and a tutorial on (ELPA) eigensolver
  performance using `Eiger` and `concur`.


### Version 1.0.0

18 March 2024

* Added support for *f*-orbitals and the 'extended' SKF format.
* Added calculation of the stress tensor and variable-cell
  relaxation.
* Important bug fixes in the repulsion evaluation and the
  non-self-consistent forces.
* The `kpoint_mp_grid` and `kpoint_gamma_shift` keywords have been
  replaced by `kpts` and `kpts_gamma_shift`, respectively. The `kpts`
  keyword can now also refer to a file containing the k-point
  coordinates and weights.
* The output now also contains free energies (containing contributions
  from the electronic entropy). These can be retrieved in the ASE
  interface by calling `atoms.get_potential_energy(force_consistent=True)`.
  Note that the energy returned by the socket interface is now the
  free energy.
* Enabled three-center expansions for off-site Hamiltonian matrix
  elements and two- and three-center expansions for the on-site ones.
  The three-center terms are only implemented up to *d*-orbitals.
* Enabled two-center expansions for on- and off-site U and W matrix elements.
* Implemented self-consistency by 'Giese-York' mapping to auxiliary
  charges and magnetizations, with multipolar auxiliary functions
  (up to *d*). The already-existing SCC-DFTB functionality is now
  available as 'Mulliken'-type mapping. The previous `scc_...`
  input keywords have been renamed and restructured as a result.
* Removed the limited support for MPI parallelization, in favor of
  more comprehensive OpenMP parallelism. The related `VARIANT=...`
  Make option has been simplified to `OMP=0/1`.
* Input keyword changes (with corresponding changes in the
  `Tibi.get_...()` methods) including, but not limited to:
  `eps_overlap` to `overlap_2c_eps`,
  `kpoint_gamma_shift` to `kpts_gamma_shift`,
  `output_charges` to `output_subshell_magnetizations`,
  `output_magnetic_moments` to `output_subshell_magnetizations`,
  `output_magnetizations` to `output_orbital_magnetizations`,
  `output_populations` to `output_orbital_populations`,
  `repulsion_form` to `repulsion_2c_form`,
  `slako_switching_degree` to `{overlap,veff}_2c_switching_degree`,
  `slako_switching_length` to `{overlap,veff}_2c_switching_length`,
  `tbpar_ext` (removed).
* Added DFTB and 3cTB-GY/Mu tutorials.


### Version 0.7

30 December 2020

* Added socket communication through the i-PI protocol.
* Switched to a denser storage of orbital population and
  charge arrays using 1D instead of 2D arrays. As a result,
  the printed populations and charges, as well as the SCC
  restart files, no longer include zeroes for subshells
  that are not part of the basis set.
* Keyword changes:
  `orbital_angular_momenta` to `basis set`,
  `scc_angular_momentum_dependent` to `scc_subshell_dependent`,
  `nspin` to `spin_polarized` (now boolean-valued),
  `nupdown` to `spin_nupdown`.
* Added the possibility to include subshells with the same
  angular momentum, but originating from different shells
  (see the `basis_set` keyword).
* Implemented spin polarized SCC-tight-binding.


### Version 0.6

28 November 2020

* The `slako_orbitals_to_include` keyword was renamed
  to `orbital_angular_momenta`.
* Fixed bug in the Mulliken population analysis.
* Implemented self-consistent-charge (SCC) tight-binding,
  with and without angular momentum dependence of the
  SCC corrections.
* Improved timings.
* Fixed bug in which Tibi would crash for Gamma-point
  shifted Monkhorst-Pack grids.
* Implemented structural relaxation using the LBFGS
  algorithm.
* Dropped support for GEN-formatted input coordinate files,
  in favor of the more versatile extended XYZ format
  (allowing e.g. to fix atoms in one or more directions).


### Version 0.5

30 August 2020

* The BLAS library is now also required to build Tibi
* Faster force evaluation by calculating density matrices
  using BLAS routines
* Switched to the [FORD](
  https://github.com/Fortran-FOSS-Programmers/ford)
  documentation generator
* Changed the "VERSION" make option to "VARIANT"
* Dropped support for a separate '.tibi' Slater-Koster
  format with analytical expressions for H and S matrix
  elements
* Fixed several bugs in the H and S builds as well
  as the repulsive potentials
* The `param_dir` and `param_ext` input keywords have been
  renamed to `tbpar_dir` and `tbpar_ext`, respectively.
* The 3 additional booleans are no longer required in the
  first line of the homo-elemental SKF files. Now the
  `slako_orbitals_to_include` keyword can be used instead.


### Version 0.4

09 July 2020

* Fixed bugs in H and S matrix builds
* Added calculation of the (energy-weighted) density matrix
* Bands (eigenvalues) are no longer written by default;
  set the output_bands keyword to True to write them out
* Forces are now available for the tight-binding band structure
  and the repulsive interactions


### Version 0.3

11 June 2020

* When asking for output of the bands and H and S matrices,
  these are now written to disk instead of stdout
* Building and testing different VERSIONs of the tibi binary
  (now placed in ./build/bin/VERSION/) has become easier
* Hybrid memory parallelism (MPI/OpenMP)
* More efficient H and S matrix builds using neighbor lists
* Dropped FITPACK dependency, now using own spline routines


### Version 0.2

29 April 2020

* Added a timing module
* OpenMP and MPI parallelization for the spin channels
  and k-points


### Version 0.1

11 March 2020

* Start of versioning.
