{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4e52554e-e5da-4cf2-9bd9-12e1921d3d4a",
   "metadata": {},
   "source": [
    "# Tutorial 3\n",
    "\n",
    "This tutorial examines the performance of the available eigensolvers in Tibi (see also the [Performance](https://mvdb.gitlab.io/tibi/page/performance.html) section of the manual). The main goal is to illustrate how the two-stage [ELPA](https://elpa.mpcdf.mpg.de/index.html) eigensolver (ELPA2) compares to the Divide-and-Conquer (DC) method from LAPACK as a function of problem size, number of threads and the fraction of calculated eigenvectors. The one-stage ELPA eigensolver (ELPA1) is not considered because no situations occurred where it outperformed the ELPA2 and/or DC eigensolvers.\n",
    "\n",
    "\n",
    "## Input\n",
    "\n",
    "These tests are done using the simpler Eiger program which focuses on the generalized eigenvalue problem and e.g. does not require a geometry input file. The keyword input file template used for the Eiger runs looks as follows:\n",
    "\n",
    "```bash\n",
    "# Tibi options:\n",
    "complex_wfn = .false.\n",
    "diag_algo = ...\n",
    "diag_elpa_blocksize = 64\n",
    "diag_gen2std = LAPACK\n",
    "diag_nband = ...\n",
    "diag_parallel_ks = .false.\n",
    "kpts = 1 1 1\n",
    "spin_polarized = .false.\n",
    "# Eiger options:\n",
    "decompose_overlap = .true.\n",
    "nbasis = ...\n",
    "niter = 1\n",
    "random_seed = 42\n",
    "```\n",
    "\n",
    "By starting from an already decomposed overlap matrix (`decompose_overlap = .true.`), the tests are representative of Tibi calculations with multiple SCF iterations per ionic step (in which case the decomposition will only consume a very small part of the total runtime). It is then also sufficient to just carry out a single SCF iteration (`niter = 1`). The problem size (number of basis functions) is set via the `nbasis` keyword, while the `diag_nband` keyword controls the number of calculated eigenvectors. The `diag_algo` keyword and `ELPA_DEFAULT_SOLVER` environment variable are used to select the eigensolver. The `ELPA_DEFAULT_REAL_KERNEL` environment variable is here set to `ELPA_2STAGE_REAL_AVX512_BLOCK6`. The `diag_gen2std = LAPACK` setting normally provides best performance and also results in the different eigensolvers using the same LAPACK-based generalized-to-standard-eigenproblem transformations. Any difference in performance therefore stems from the eigensolvers' standard eigenvalue problem solving routines.\n",
    "\n",
    "\n",
    "## Hardware\n",
    "\n",
    "The Eiger runs have been carried out on a [wICE](https://docs.vscentrum.be/leuven/tier2_hardware.html) node equipped with two [Intel Xeon Platinum 8360Y](https://ark.intel.com/content/www/us/en/ark/products/212459/intel-xeon-platinum-8360y-processor-54m-cache-2-40-ghz.html) CPUs. Each CPU consists of 36 cores running at the base frequency, with a shared L3 cache and a single NUMA domain.\n",
    "\n",
    "\n",
    "## Software\n",
    "\n",
    "The results have been obtained with Eiger v1.1 (as distributed with Tibi v1.1) compiled with Intel compilers (ifx `v2023.2.0`) and linked to Intel oneMKL `v2023.2.0` and ELPA `v2023.11.001`. The ELPA library itself is also built with Intel compilers (icx `v2023.2.0`, ifort `v2021.10.0`) and linked to the same oneMKL libraries. The DC implementation is therefore provided by oneMKL."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb3f77bb-2990-4e95-bd7b-47aa3f249239",
   "metadata": {},
   "source": [
    "## Part 1: (strong) scaling tests\n",
    "\n",
    "The figure below shows eigensolver runtimes (`lcao_solve` timer), parallel efficiencies and speedups w.r.t. DC for three problem sizes (N = 5000, 20000, and 30000). To relate this to a number of atoms, divide N by e.g. 9 for `spd` basis sets or by 15 for `spd_sd` basis sets. The largest case (N = 30000) therefore represents calculations with several thousands of atoms while the smallest case (N = 5000) would correspond to (at least) several hundreds of atoms.\n",
    "\n",
    "For even smaller problem sizes (e.g. N = 1000, not included here), DC will normally be the most efficient. Keep in mind that Tibi calculations typically also involve other operations such as force calls, Hamiltonian updates, ... which can take up a significant fraction of the runtime for small problem sizes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4ed8364-d928-41b9-af0a-a2725964278a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "\n",
    "# Load the JSON files containing the performance data and plot settings\n",
    "datasets = {}\n",
    "for test in ['scaling', 'throughput']:\n",
    "    with open(f'tutorial_3_{test}.json', 'r') as f:\n",
    "        datasets[test] = json.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b1a18e6c-20e3-445c-9e87-9830fbaf502f",
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Markdown as md\n",
    "from tutorial_3_plotter import make_plot\n",
    "\n",
    "Ns = [5000, 20000, 30000]\n",
    "methods = ['DC', 'ELPA2 (100% EV)', 'ELPA2 (50% EV)', 'ELPA2 (25% EV)']\n",
    "properties = ['runtime', 'efficiency', 'speedup']\n",
    "test = 'scaling'\n",
    "filename = make_plot(test, datasets[test], methods, properties, Ns)\n",
    "# Show the figure in a way that also works in Markdown export\n",
    "md(f\"![{test}]({filename})\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2dc813a4-9445-42ce-a75c-9b389298f10b",
   "metadata": {},
   "source": [
    "We can see that\n",
    "\n",
    "- When seeking all eigenvectors, ELPA2 is indeed faster than DC, except for the case of N = 5000 with 18 threads.\n",
    "- The benefit of ELPA2 becomes much more pronounced when not all eigenvectors are needed (DC always produces all eigenvectors). Though some LAPACK eigensolvers such as MRRR also offer to do this, their performance normally remains inferior to DC (at least for the cases considered here).\n",
    "- When increasing the number of threads, DC's parallel efficiency remains higher than that of ELPA2. The largest difference in runtimes are hence found for the single-core calculations.\n",
    "- For a given eigensolver, the scaling improves when increasing the problem size, as is usually the case.\n",
    "\n",
    "Based on `lcao_solve` subtimings (not shown here), the generalized-to-standard-eigenproblem transformations are found to consistently take about 15% of the total runtime in the case of DC. Their scaling with the number of threads is therefore similar to that of DC. For the ELPA2 runs with 100% EV, this fraction varies between 17% and 20% depending on the core count. When not all eigenvectors are needed, this fraction understandably increases a bit further to circa 25% (at least for the lower core counts). The scaling test results therefore remain largely determined by the behavior of the eigensolvers' standard eigenvalue problem solving routines."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c03eb59-145e-4da9-b24e-2af36ac462f7",
   "metadata": {},
   "source": [
    "## Part 2: throughput tests\n",
    "\n",
    "Part 1 looked at the behavior of one Eiger process running on a node, potentially using multiple threads. Certain workloads, however, rather involve packing the node with several independent processes, each confined to a subset of the available cores. High-throughput screening and global structure optimization are examples of such workloads. The different processes may then compete for shared resources (such as memory bandwidth), which will lead to lower performance compared to the single-process case.\n",
    "\n",
    "This effect is investigated here for the smaller case (N = 5000) with just a single thread per process. The figure below shows what happens when more and more processes are run simultaneously on the node, which can be seen as a test for weak scaling. The different processes are closely packed (i.e. process `i` is pinned to core `i`) using the `concur` tool located in the `tools` directory:\n",
    "\n",
    "```bash\n",
    "# For more information, see the output of 'concur -h'\n",
    "concur -p <processes>:1 -t 1:1 eiger eiger.in\n",
    "```\n",
    "\n",
    "Note: in principle the scaling may also be affected by CPU frequency throttling at high load. On the hardware used for these tests, however, this effect does not seem to be significant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "860d9572-3809-4ed1-a2e3-4a2d923dff45",
   "metadata": {},
   "outputs": [],
   "source": [
    "# problem sizes\n",
    "Ns = [5000]\n",
    "test = 'throughput'\n",
    "filename = make_plot(test, datasets[test], methods, properties, Ns)\n",
    "# Show the figure in a way that also works in Markdown export\n",
    "md(f\"![{test}]({filename})\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4fb5d2e0-1535-40c6-8d0d-1d91f3d8485e",
   "metadata": {},
   "source": [
    "The single-core performance is already known from Part 1. At larger core counts, i.e. with more concurrent Eiger processes, all eigensolvers indeed require more time to solve the eigenproblem and so the associated parallel efficiencies are decreasing. The performance per process is identical for one full CPU (36 processes) and two full CPUs (72 processes), as expected from the close packing. This slowdown is however quite less pronounced for ELPA2 than for DC, which is presumably due to DC's higher sensitivity to available memory bandwidth. In other words, there are additional gains to be had with ELPA2 in case of resource contention."
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
