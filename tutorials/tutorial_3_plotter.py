from cycler import cycler
from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter, ScalarFormatter
import numpy as np


def make_plot(test, dataset, methods, properties, Ns):
    baseline = methods[0]

    styles = {
        'color': ['teal', 'olive', 'orange', 'crimson'],
        'linestyle': ['dashed'] * len(methods),
        'marker': ['s', '^', 'x', 'o'],
    }
    plt.rc('axes', prop_cycle=cycler(**styles))

    ncols = len(Ns)
    nrows = len(properties)
    figsize = (3*ncols, 3*nrows)
    fig, axs = plt.subplots(nrows=nrows, ncols=ncols, squeeze=False,
                            figsize=figsize, layout='constrained')

    for i, N in enumerate(Ns):
        size = f'N={N}'
        x = dataset[size]['cores']

        axs[0][i].set_title(size)
        for j in range(1, len(properties)):
            axs[j][i].sharex(axs[j-1][i])

        for method in methods:
            for j, prop in enumerate(properties):
                if prop == 'runtime':
                    f = 'loglog'
                    y = dataset[size][method]['runtimes']
                elif prop == 'efficiency':
                    f = 'semilogx'
                    y = 100 * np.array(dataset[size][method]['efficiencies'])
                elif prop == 'speedup':
                    f = 'semilogx'
                    y = np.array(dataset[size][baseline]['runtimes']) \
                        / np.array(dataset[size][method]['runtimes'])

                func = getattr(axs[j][i], f)
                func(x, y, label=method)

                if f == 'loglog':
                    axs[j][i].yaxis.set_minor_formatter(NullFormatter())
                    axs[j][i].yaxis.set_major_formatter(ScalarFormatter())

        axs[0][i].xaxis.set_ticks(x + [10], x + [''])
        axs[0][i].xaxis.axes.set_xlim(dataset[size]['xlim'])
        axs[-1][i].xaxis.axes.set_xlabel('Number of cores [-]')

        for j, prop in enumerate(properties):
            axs[j][i].yaxis.axes.set_ylim(dataset[size]['ylims'][j])
            axs[j][i].grid(which='both', ls='dotted')
            axs[j][i].tick_params(top=True, right=True, direction='in',
                                  which='both')
            if i == 0:
                if prop == 'runtime':
                    ylabel = 'Runtime [s]'
                elif prop == 'efficiency':
                    ylabel = 'Parallel efficiency [%]'
                elif prop == 'speedup':
                    ylabel = f'Speedup w.r.t. {baseline} [-]'
                axs[j][i].yaxis.axes.set_ylabel(ylabel)

    handles, labels = axs[0][0].get_legend_handles_labels()
    legend = fig.legend(handles, labels, loc='outside upper center',
                        title=f'Eigensolver {test} test', ncol=len(methods))

    if len(Ns) == 1 and len(properties) > 1:
        # Turn single-column figure into single row

        for i, N in enumerate(Ns):
            size = f'N={N}'
            for j in range(len(properties)):
                axs[j][i].set_title(size)
                axs[j][i].xaxis.axes.set_xlabel('Number of cores [-]')

        figsize = fig.get_size_inches()[::-1]
        fig.set_size_inches(figsize)

        # The layout seems to have lost the legend,
        # so some top margin needs to be reserved
        fig.canvas.draw()
        h = legend.get_window_extent().height / 100
        top = 1 - 1.75 * h / figsize[1]

        gs = GridSpec(ncols, nrows, figure=fig, top=top, wspace=0.4)

        k = 0
        for i in range(ncols):
            for j in range(nrows):
                axs[j][i].set_position(gs[k].get_position(fig))
                axs[j][i].set_subplotspec(gs[k])
                k += 1

    filename = f'tutorial_3_{test}.png'
    plt.savefig(filename, bbox_inches='tight')
    plt.close()
    return filename
