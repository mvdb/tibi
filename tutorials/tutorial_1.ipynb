{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7c40f33b-3ace-4590-893f-f63133d3ca63",
   "metadata": {},
   "source": [
    "# Tutorial 1\n",
    "\n",
    "This first tutorial will show how to run a basic DFTB calculation with Tibi.\n",
    "\n",
    "For this we will use a Slater-Koster file that is included in the test suite\n",
    "(`../tests/skf_files/Si-Si.skf`), which happened to be generated as part of\n",
    "the [Tango](https://gitlab.com/mvdb/tango) tutorial.\n",
    "\n",
    "Tibi can be used in two different ways:\n",
    "\n",
    "* by providing a keyword input file and geometry input file (in xyz format)\n",
    "  and then launching the `tibi` executable with e.g.\n",
    "  `tibi tibi.in tibi.xyz > tibi.out`.\n",
    "* via the Python interface, i.e. the `Tibi` calculator class in combination\n",
    "  with the [Atomic Simulation Engine (ASE)](https://wiki.fysik.dtu.dk/ase).\n",
    "\n",
    "This tutorial will focus on the second way as it is usually the more\n",
    "convenient and flexible one. If new to ASE, consider trying out some of the\n",
    "[ASE tutorials](https://wiki.fysik.dtu.dk/ase/tutorials/tutorials.html).\n",
    "\n",
    "At this point it is assumed that you have compiled the `tibi` executable,\n",
    "installed the corresponding Python package (see the\n",
    "[installation instructions](https://mvdb.gitlab.io/tibi/page/install.html)).\n",
    "\n",
    "\n",
    "## Part 1: FileIO mode with Tibi driver\n",
    "\n",
    "Now let's perform a variable-cell relaxation of this compound with Tibi\n",
    "using a DFTB2 model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "00a9cb66-2622-460d-8829-d549d5d0c85d",
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.build import bulk\n",
    "from tibi import Tibi\n",
    "\n",
    "calc = Tibi(aux_basis_mulliken='subshell_dependent', # subshell-resolved SCF\n",
    "            basis_set={'Si': 'sp'},  # minimal basis of s- and p-electrons\n",
    "            include_scf=True,  # include (2nd-order) self-consistency\n",
    "            kpts=[5, 5, 5],  # Monkhorst-Pack k-point grid dimensions\n",
    "            output_mulliken_subshell_chg=True,  # print the Mulliken charges\n",
    "            relax_vc=True,  # also allow the cell vectors to vary\n",
    "            repulsion_2c_form='spline',  # spline-based two-center repulsion\n",
    "            task='relaxation',  # perform a relaxation task\n",
    "            tbpar_dir='../tests/skf_files',  # directory with parameter files\n",
    "            )\n",
    "\n",
    "atoms = bulk('Si', crystalstructure='diamond')\n",
    "atoms.set_calculator(calc)\n",
    "e = atoms.get_potential_energy()\n",
    "chg = calc.get_mulliken_subshell_chg()\n",
    "print('Final energy:', e)\n",
    "print('Final Mulliken charges:', chg)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c397223-f015-4e10-bee0-851dd93ecd7c",
   "metadata": {},
   "source": [
    "In this mode, Tibi is simply used as a `FileIOCalculator`: the Python\n",
    "interface sets up the input files, runs `tibi` and extracts results from\n",
    "the output files. The generated keyword input file is here called `tibi.in`.\n",
    "Aside from the chosen settings it also contains the default choices for the\n",
    "other keywords:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "85c629e8-0dac-4ce3-b39b-6af4d1215147",
   "metadata": {},
   "outputs": [],
   "source": [
    "! cat tibi.in"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30dab56e-5d96-4573-9012-5302d45faa5c",
   "metadata": {},
   "source": [
    "The main output file, here called `tibi.out`, shows that the LBFGS\n",
    "optimizer needed about 10 ionic steps to reach the (default) force\n",
    "and stress convergence thresholds:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0f4ba8eb-3e87-4d69-9871-e093e28379da",
   "metadata": {},
   "outputs": [],
   "source": [
    "! cat tibi.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd4a0060-1ecc-4670-b985-c14debe707cf",
   "metadata": {},
   "source": [
    "You can also visualize the trajectory with e.g. `ase gui relax.xyz`.\n",
    "This will confirm that the geometry only changed a little during the\n",
    "relaxation, which is expected since the repulsive potential was fitted\n",
    "to reproduce the (LDA) energy-volume curve of diamond Si.\n",
    "\n",
    "Keep in mind that:\n",
    "\n",
    "* ASE uses eV as unit of energy and angstrom as unit of length,\n",
    "* the same units are assumed in the keyword input file\n",
    "  (and in xyz-format geometry input files for that matter),\n",
    "* Tibi uses atomic units internally,\n",
    "* Tibi output is also in atomic units (except for xyz output files,\n",
    "  where eV and angstrom are expected),\n",
    "* when reading the Tibi output, the Python interface converts\n",
    "  the results to the ASE units.\n",
    "\n",
    "\n",
    "## Part 2: FileIO mode with ASE driver\n",
    "\n",
    "So far we have used the LBFGS optimizer as implemented in Tibi,\n",
    "which is also the only available one for relaxation tasks.\n",
    "When changing the task to `energy_forces` however, we can let ASE handle\n",
    "the optimization, for example using the FIRE algorithm which has not\n",
    "been implemented in Tibi:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6ace15cf-46ce-46ed-ba66-ae7f84989b13",
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.calculators.socketio import SocketIOCalculator\n",
    "from ase.constraints import UnitCellFilter\n",
    "from ase.optimize import FIRE\n",
    "\n",
    "calc.set(task='energy_forces')\n",
    "\n",
    "atoms = bulk('Si', crystalstructure='diamond')\n",
    "atoms.set_calculator(calc)\n",
    "\n",
    "ecf = UnitCellFilter(atoms)\n",
    "opt = FIRE(ecf, logfile='-', trajectory='opt.traj')\n",
    "opt.run(fmax=5e-2, steps=50)\n",
    "\n",
    "e =  atoms.get_potential_energy()\n",
    "chg = calc.get_mulliken_subshell_chg()\n",
    "print('Final energy:', e)\n",
    "print('Final Mulliken charges:', chg)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef5b98d9-8a0f-4d53-83f7-afbf32a2ec7c",
   "metadata": {},
   "source": [
    "## Part 3: SocketIO mode with ASE driver\n",
    "\n",
    "In addition to the 'FileIO' mode it is also possible to let the\n",
    "Python calculator communicate with the `tibi` executable via [sockets](\n",
    "https://wiki.fysik.dtu.dk/ase/ase/calculators/socketio/socketio.html).\n",
    "\n",
    "The main advantage is that the `tibi` (Fortran) program only needs\n",
    "to initialize once, after which it starts listening to the socket\n",
    "for new instructions from the (ASE) driver via the [i-pi protocol](\n",
    "https://ipi-code.org/i-pi/introduction.html#communication-protocol).\n",
    "\n",
    "This reduces the initialization and file IO related overheads\n",
    "which can be significant for larger structures or more complex\n",
    "theoretical descriptions, especially when driving the task from ASE\n",
    "as done in Part 2. Here is the same example but this time with\n",
    "(Unix) socket communication:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a068d17c-9112-4928-9954-9539a3e82327",
   "metadata": {},
   "outputs": [],
   "source": [
    "atoms = bulk('Si', crystalstructure='diamond')\n",
    "\n",
    "unixsocket = 'ase_tibi'\n",
    "\n",
    "calc.set(socket_type='unix',\n",
    "         socket_unix_suffix=unixsocket,\n",
    "         task='socket')\n",
    "\n",
    "with SocketIOCalculator(calc, unixsocket=unixsocket) as socketcalc:\n",
    "    atoms.calc = socketcalc\n",
    "\n",
    "    ecf = UnitCellFilter(atoms)\n",
    "    opt = FIRE(ecf, logfile='-', trajectory='opt.traj')\n",
    "    opt.run(fmax=5e-2, steps=50)\n",
    "\n",
    "    e =  atoms.get_potential_energy()\n",
    "    chg = calc.get_mulliken_subshell_chg()\n",
    "    print('Final energy:', e)\n",
    "    print('Final Mulliken charges:', chg)\n",
    "\n",
    "    atoms.calc.server.protocol.end()"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
