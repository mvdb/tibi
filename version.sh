#!/usr/bin/env bash
# Prints a suitable version string

set -e
rootdir=$(dirname "$(readlink -f "$0")")
prefer=${1-git}

if [ "${prefer}" == "git" ] && [ -d "${rootdir}/.git" ]; then
  git describe --tags --match="v[0-9]*" --always --dirty
else
  pyinit="${rootdir}/python/tibi/__init__.py"
  pyver=$(grep -Po "__version__ = .\\K[^\\'\\\"]*" "${pyinit}")
  echo v"${pyver}"
fi
