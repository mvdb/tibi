project: Tibi
summary: **An ab-initio tight-binding electronic structure code**
author: Maxime Van den Bossche
project_website: https://gitlab.com/mvdb/tibi
src_dir: ../src
         ../python/tibi
extra_filetypes: py  #
output_dir: ./_build
page_dir: ./pages
predocmark: >
source: true
graph: false
search: true
print_creation_date: true
creation_date: %Y-%m-%d %H:%M %z
parallel: 0
version: VERSTR
macro: _NODEBUG
       _OMP
       _ELPA

{!../README.md!}
