#!/usr/bin/env bash
# Converts a tutorial notebook to markdown

cd pages/tutorials
mdfile="tutorial_$1.md"
nbdir="../../../tutorials"
nbfile="${nbdir}/tutorial_$1.ipynb"
nbopts="--to markdown --stdout"
if [[ "$1" == "3" ]]; then
  nbopts="${nbopts} --execute"
fi

echo "title: Tutorial $1" > ${mdfile}
jupyter nbconvert ${nbopts} ${nbfile} >> ${mdfile}

if [[ "$1" == "3" ]]; then
  cp ${nbdir}/tutorial_$1_*.png .
fi
