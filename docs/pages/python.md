title: Python interface

## Python interface

The `tibi` module provides a Python interface in the form of
an [ASE](https://wiki.fysik.dtu.dk/ase/)-style FileIOCalculator class
`Tibi`. See the [[tibi.py]] file.

The module also includes:

* a `MultipoleEwald` ASE calculator for evaluating electrostatic multipole
  interactions via Ewald summation ([[ewald.py]]), which proved useful to
  verify the Fortran implementation in `Tibi`.

* an `Eiger` ASE calculator which provides a Python interface for the
  Eiger program ([[eiger.py]]).
