title: Frequently asked questions

* How should I cite Tibi?

    If Tibi has been useful in your work, please cite the following publication:

    M. Van den Bossche, J. Chem. Theory Comput. **2024**
    [(doi)](https://doi.org/10.1021/acs.jctc.4c00018).
