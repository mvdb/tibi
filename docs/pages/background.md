title: Background

## Background

### Tight-binding

Tight-binding methods aim to describe electronic interactions in molecules
and solids in a conceptually simple and computationally efficient way.
To this end, the main tactic is to approximate density functional theory
(DFT) by:

* using small- to medium-sized sets of atom-centered basis functions,
* applying a low-order Taylor expansion of the Kohn-Sham energy
  functional around a suitable input density,
* simplifying the resulting contributions to the Hamiltonian
  and remaining total energy terms (i.e. other than the band energy).

Variants may classify as semi-empirical tight-binding (SETB) when
extensive approximations are made so that empirical corrections
are required. Less approximate (but necessarily also more expensive)
'ab initio' tight-binding (AITB) methods are supposed to perform well
with little or no empiricism.

### Tibi implementation

The most advanced AITB model in Tibi corresponds to three-center tight-binding
with 'Giese-York' mapping for the multipolar auxiliary basis (3cTB-GY).
Also a simpler 'Mulliken' mapping can be used, with a monopolar
auxiliary basis (3cTB-Mu). The [2024 Tibi paper](
https://doi.org/10.1021/acs.jctc.4c00018) contains a detailed
description of these approaches.

Tibi furthermore provides a basic implementation of the SETB method known as
[density functional tight-binding (DFTB)](https://dftb.org/about-dftb).

In both cases, Tibi needs various tables of precomputed integrals
as input. For DFTB runs these correspond to files in the [SKF](
https://www.dftb.org/parameters/introduction/) format. For AITB runs
additional files are needed, which can be generated with the [Hotcent
code](https://gitlab.com/mvdb/hotcent). Note that Tibi has no notion
of e.g. exchange-correlation functionals or even radial basis functions,
which only come into play when constructing the integral tables.
