title: Fortran code

## Fortran code

The Fortran code and its documentation can be browsed using the tabs
at the top.


### Tibi

Here follows an overview of the components in Tibi:

The main program:

- [[tibi]]

The different high-level modules:

- [[tibi_atoms]]
- [[tibi_calculator]]
- [[tibi_calculator_set]]
- [[tibi_driver_base]]
- [[tibi_driver_relaxation]]
- [[tibi_driver_socket]]
- [[tibi_input]]

Calculator modules:

- [[tibi_coulomb]]
- [[tibi_repulsion]]
- [[tibi_scf_tightbinding]]
- [[tibi_tightbinding]]

General electronic structure modules:

- [[tibi_basis_ao]]
- [[tibi_basis_sets_ao]]
- [[tibi_lcao]]
- [[tibi_mixer]]
- [[tibi_occupations]]
- [[tibi_smearing]]

Tight-binding related modules:

- [[tibi_fluctuation]]
- [[tibi_fluctuation1c_kernel]]
- [[tibi_fluctuation2c_kernel]]
- [[tibi_giese_york_kernel]]
- [[tibi_h0s]]
- [[tibi_offsite3c]]
- [[tibi_onsite1c_offsite2c]]
- [[tibi_onsite2c]]
- [[tibi_onsite3c]]
- [[tibi_orbitals]]
- [[tibi_threecenter]]

Helper modules:

- [[tibi_constants]]
- [[tibi_coulomb_realspace]]
- [[tibi_coulomb_realspace_monopole]]
- [[tibi_coulomb_realspace_multipole]]
- [[tibi_coulomb_reciprocal]]
- [[tibi_coulomb_reciprocal_monopole]]
- [[tibi_coulomb_reciprocal_multipole]]
- [[tibi_driver_init]]
- [[tibi_eigensolver]]
- [[tibi_fixedpoint_base]]
- [[tibi_fixedpoint_pulay]]
- [[tibi_neighborlist]]
- [[tibi_neighborlist_verlet]]
- [[tibi_optimizer_base]]
- [[tibi_optimizer_lbfgs]]
- [[tibi_realspace_pair]]
- [[tibi_spline]]
- [[tibi_timer]]
- [[tibi_timer_set]]
- [[tibi_twobody]]
- [[tibi_utils_math]]
- [[tibi_utils_prog]]
- [[tibi_utils_string]]

Interface modules:

- [[tibi_blas]]
- [[tibi_lapack]]


### Eiger

The corresponding overview for Eiger is as follows:

The main program:

- [[eiger]]

The different high-level modules:

- [[eiger_calc]]
