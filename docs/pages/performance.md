title: Performance

This page gives some hints for getting optimal performance with Tibi
(for a given model, basis set and input structure). Note that timing
information is included at the end of the main Tibi output file.


## Eigensolvers

Solving the generalized eigenvalue problem (once for every SCF iteration)
becomes the main bottleneck when the number of basis functions grows large.
It is then recommended to compare different eigensolvers (and their
implementations) to find the fastest approach.

### LAPACK

For smaller problem sizes (\( \leq 10^3 - 10^4 \) basis functions)
the default divide-and-conquer solver (`diag_algo='divide&conquer'`)
is typically the best choice, provided that Tibi is linked to an optimized
LAPACK library (MKL, OpenBLAS, BLIS, ...). Depending on the hardware, some
implementations may perform significantly better or worse than others.

### ELPA

[ELPA](https://elpa.mpcdf.mpg.de/) eigensolvers (`diag_algo='elpa'`)
are expected to provide better performance for larger problem sizes.
The chosen ELPA solver, block size, number of eigenvectors and
(if applicable) 2-stage kernel are reported in the `Eigensolver
description` section of the main Tibi output.

By default Tibi only uses ELPA for solving the standard eigenvalue problem
(after converting the generalized problem using LAPACK routines) as this
typically gives the best performance. This can be changed via the
`diag_gen2std` input keyword.

By default Tibi will select the 2-stage ELPA solver, which is usually
the more efficient one. The 1-stage algorithm can be chosen by setting
the `ELPA_DEFAULT_solver` environment variable to `ELPA_SOLVER_1STAGE`.

ELPA provides different kernels for the eigenvector backtransformation
step of the 2-stage solver and tends to make a reasonable default choice.
Different, potentially better-performing kernels can be selected via
the `ELPA_DEFAULT_real_kernel` environment variable
(`ELPA_DEFAULT_complex_kernel` for complex matrices and eigenvectors).
See the output of the `elpa2_print_kernels...` command for a list of
available kernels.

The block size can be set via the `diag_elpa_blocksize` input keyword
and can also be tuned for best performance. While the default value of 64
should be close to optimal, other values such as 16 or 32 may also be worth
considering.

By default all eigenvectors are calculated, but this can be reduced
in case not all unoccupied bands are needed (see the `diag_nband` input
keyword).


## Parallelism

### OpenMP

Tibi can be compiled with OpenMP support (see the
[installation instructions](install.html)) which allows for
shared-memory parallelism. This is mainly intended to accelerate
\( \Gamma \)-point-only calculations with a sufficiently large number of
basis functions. It is then also necessary to link to threaded BLAS, LAPACK
and ELPA libraries (as illustrated in the `arch/Makefile.include.*` examples)
since the eigensolver calls make up a large portion of the workload.
The number of threads can then be controlled with the `OMP_NUM_THREADS`
environment variable. When using a threaded ELPA library linked to Intel MKL,
the `MKL_NUM_THREADS` environment variable should also be set to this number
of threads.

### diag_parallel_ks keyword

If the calculation involves multiple k-points and spin channels, then it is
also possible to distribute their (independent) eigensolver calls over
different OpenMP threads, instead of parallelizing the individual calls.
This can be enabled by setting the `diag_parallel_ks` input keyword to
`True`/`.true.`. This can bring a performance benefit but normally requires
using serial or single-threaded eigensolver routines as good performance
seems difficult to achieve with nested parallelism. In case Tibi is linked
to a threaded ELPA library, then this can be achieved by setting the
`ELPA_DEFAULT_omp_threads` environment variable to `1`.


## Eiger

The Eiger program ([[eiger.py]]) makes it easier to check how different
parameters influence the speed at which the eigenproblems get solved
(e.g. the number of basis functions, the number of OpenMP threads and the
choice of eigensolver and eigensolver settings). This can help to find the
optimal parameters for subsequent Tibi calculations.


## Initialization overheads

### Socket communication

While Tibi can be used as a regular `FileIOCalculator` in ASE, this may not
be optimal for e.g. molecular dynamics or geometry optimization runs because
Tibi needs to reinitialize from scratch at every ionic step.
The [`SocketIOCalculator` mode](
https://wiki.fysik.dtu.dk/ase/ase/calculators/socketio/socketio.html)
allows to avoid these overheads.

### 3cb file format

When running 3cTB models in Tibi, the costlier initialization steps
correspond to reading and processing the three-center integral tables,
especially in their plain text version (`.3cf` extension). By setting the
`veff_3c_binary_io` and `repulsion_3c_binary_io` input keywords to
`True` / `.true.`, this is sped up by reading the corresponding binary files
instead (unformatted, `.3cb` extension). If a certain binary file is not found,
then the `.3cf` file will be read and the `.3cb` file will be generated
so that it can be used on the next occasion.
