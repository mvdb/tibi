#-----------------------------------------------------------------------------#
#   Tibi: an ab-initio tight-binding electronic structure code                #
#   Copyright 2020-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#

import numpy as np
from scipy.special import erfc, factorial2
from ase.calculators.calculator import all_changes, Calculator
from ase.neighborlist import NeighborList
from ase.units import Bohr, Ha


class MultipoleEwald(Calculator):
    """
    Calculator with a reference implementation of point multipole
    electrostatics (up to quadrupoles) using Ewald summation.

    Surface contributions are not included, meaning that "tinfoil"
    boundary conditions are assumed.

    References
    ----------
    * Ewald, P. P. (1921). Die Berechnung optischer und elektrostatischer
      Gitterpotentiale. Ann. Phys. 369(3), 253-287
      (https://doi.org/10.1002/andp.19213690304).
    * Smith, W. (1982). Point multipoles in the Ewald summation.
      CCP5 Inf. Q, 4, 13-25.
    * Nymand, T. M. and Linse, P. (2000). Ewald summation and reaction field
      methods for potentials with atomic charges, dipoles, and
      polarizabilities. J. Chem. Phys. 112(14), 6152-6160
      (https://doi.org/10.1063/1.481216).
    * Aguado, A. and Madden, P. A. (2003). Ewald summation of electrostatic
      multipole interactions up to the quadrupolar level. J. Chem. Phys.
      119(14), 7471-7483 (https://doi.org/10.1063/1.1605941).
    * Laino, T. and Hutter, J. (2008). Notes on “Ewald summation of
      electrostatic multipole interactions up to quadrupolar level”.
      [J. Chem. Phys. 119, 7471 (2003)]. J. Chem. Phys. 129(7), 074102
      (https://doi.org/10.1063/1.2970887).
    * Sala, J., Guardia, E. and Masia, M. (2010). The polarizable point
      dipoles method with electrostatic damping: Implementation on a model
      system. J. Chem. Phys. 133(23), 234101
      (https://doi.org/10.1063/1.3511713).
    * Stenhammar, J., Trulsson, M. and Linse, P. (2011). Some comments
      and corrections regarding the calculation of electrostatic potential
      derivatives using the Ewald summation technique. J. Chem. Phys.
      134(22), 224104 (https://doi.org/10.1063/1.3599045).
    * Lamichhane, M., Gezelter, J. D. and Newman, K. E. (2014). Real space
      electrostatics for multipoles. I. Development of methods. J. Chem.
      Phys. 141(13), 134109 (https://doi.org/10.1063/1.4896627).

    Parameters
    ----------
    alpha : float or None
        Ewald alpha parameter in Angstrom^-1. If None, it will be set
        to 0 if non-periodic and 0.5 a0^-1 if periodic.
    tolerance : float or None
        Threshold (in eV) governing the convergence of the real
        and reciprocal space sums.
    lmax : int
        Expansion order (0 = only monopoles, 1 = up to dipoles,
        2 = up to quadrupoles).
    verbose : bool
        Verbosity flag.
    """
    implemented_properties = ['energy', 'free_energy', 'forces', 'stress']

    def __init__(self, atoms=None, alpha=None, tolerance=None, lmax=2,
                 verbose=False, **kwargs):
        Calculator.__init__(self, **kwargs)

        self.alpha = alpha if alpha is None else alpha * Bohr
        self.tolerance = tolerance if tolerance is None else tolerance / Ha
        assert 0 <= lmax <= 2, lmax
        self.lmax = lmax
        self.verbose = verbose

        self.nl = None
        self.monopoles = None
        self.dipoles = None
        self.quadrupoles = None

    def log(self, *args, flush=True, **kwargs):
        if self.verbose:
            print('LOG|', *args, flush=flush, **kwargs)
        return

    def calculate(self, atoms=None, properties=None,
                  system_changes=all_changes):
        if properties is None:
            properties = self.implemented_properties

        Calculator.calculate(self, atoms, properties, system_changes)

        nat = len(self.atoms)
        pos = self.atoms.positions / Bohr
        cell = self.atoms.cell / Bohr
        is_periodic = np.all(atoms.pbc)

        if is_periodic:
            invcell = np.linalg.inv(cell)
            vol = atoms.get_volume() / Bohr**3
            if not np.isclose(np.sum(self.monopoles), 0):
                self.log('Warning: Monopole moments do not sum exactly to 0')

        if self.tolerance is None:
            self.tolerance = 1e-6 / Ha if is_periodic else 1e-9 / Ha
        self.log('Ewald tolerance [Ha]:', self.tolerance)

        if self.alpha is None:
            self.alpha = 0.5 if is_periodic else 0
        assert not (is_periodic and np.isclose(self.alpha, 0))
        self.log('Ewald alpha [1/Bohr]:', self.alpha)

        if self.nl is None or 'numbers' in system_changes:
            rc = self.determine_cutoff_radius()
            self.log('real-space cutoff radius [Bohr]:', rc)
            self.nl = NeighborList([rc * Bohr / 2] * nat,
                                   self_interaction=False,
                                   bothways=False)
        self.nl.update(self.atoms)

        # Real space
        energy_sr = 0.
        forces_sr = np.zeros((nat, 3))
        stress_sr = np.zeros((3, 3))
        efield0_sr = np.zeros(nat)
        efield1_sr = np.zeros((nat, 3))
        efield2_sr = np.zeros((nat, 3, 3))

        for iat in range(nat):
            neighbors, offsets = self.nl.get_neighbors(iat)
            trans = np.dot(offsets, cell)
            vecs = pos[neighbors] + trans - pos[iat]
            rs = np.sqrt((vecs**2).sum(axis=1))

            for jneigh, jat in enumerate(neighbors):
                vec = -vecs[jneigh]
                r = rs[jneigh]
                if is_periodic:
                    fij = np.zeros(3)

                T1 = self.get_tensor(r, vec, p=1)
                T3 = self.get_tensor(r, vec, p=3)
                T5 = self.get_tensor(r, vec, p=5)
                T7 = self.get_tensor(r, vec, p=7)
                T9 = self.get_tensor(r, vec, p=9)
                T11 = self.get_tensor(r, vec, p=11)

                mi = self.monopoles[iat]
                mj = self.monopoles[jat]

                # monopole-monopole
                energy_sr += mi * T1 * mj
                f = mi * T3 * mj
                forces_sr[iat, :] -= f
                forces_sr[jat, :] += f
                if is_periodic:
                    fij += f

                efield0_sr[iat] += T1 * mj
                efield0_sr[jat] += T1 * mi
                efield1_sr[iat, :] -= T3 * mj
                efield1_sr[jat, :] += T3 * mi
                efield2_sr[iat, :, :] -= T5 * mj
                efield2_sr[jat, :, :] -= T5 * mi

                if self.lmax >= 1:
                    di = self.dipoles[iat, :]
                    dj = self.dipoles[jat, :]

                    # monopole-dipole
                    energy_sr -= mi * np.dot(T3, dj)
                    f = mi * np.matmul(T5, dj)
                    forces_sr[iat, :] += f
                    forces_sr[jat, :] -= f
                    if is_periodic:
                        fij -= f

                    # dipole-monopole
                    energy_sr += mj * np.dot(T3, di)
                    f = mj * np.matmul(T5, di)
                    forces_sr[iat, :] -= f
                    forces_sr[jat, :] += f
                    if is_periodic:
                        fij += f

                    # dipole-dipole
                    energy_sr -= np.dot(di, np.matmul(T5, dj))
                    f = np.tensordot(di, np.tensordot(T7, dj, axes=1), axes=1)
                    forces_sr[iat, :] += f
                    forces_sr[jat, :] -= f
                    if is_periodic:
                        fij -= f

                    efield0_sr[iat] -= np.dot(T3, dj)
                    efield0_sr[jat] += np.dot(T3, di)
                    efield1_sr[iat, :] += np.tensordot(T5, dj, axes=1)
                    efield1_sr[jat, :] += np.tensordot(T5, di, axes=1)
                    efield2_sr[iat, :, :] += np.tensordot(T7, dj, axes=1)
                    efield2_sr[jat, :, :] -= np.tensordot(T7, di, axes=1)

                    if self.lmax >= 2:
                        qi = self.quadrupoles[iat, :, :]
                        qj = self.quadrupoles[jat, :, :]

                        # monopole-quadrupole
                        energy_sr += mi * np.tensordot(T5, qj, axes=2) / 3
                        f = mi * np.tensordot(T7, qj, axes=2) / 3
                        forces_sr[iat, :] -= f
                        forces_sr[jat, :] += f
                        if is_periodic:
                            fij += f

                        # quadrupole-monopole
                        energy_sr += mj * np.tensordot(T5, qi, axes=2) / 3
                        f = mj * np.tensordot(T7, qi, axes=2) / 3
                        forces_sr[iat, :] -= f
                        forces_sr[jat, :] += f
                        if is_periodic:
                            fij += f

                        # dipole-quadrupole
                        energy_sr += np.dot(di,
                                            np.tensordot(T7, qj, axes=2)) / 3
                        f = np.tensordot(di, np.tensordot(T9, qj, axes=2),
                                         axes=1) / 3
                        forces_sr[iat, :] -= f
                        forces_sr[jat, :] += f
                        if is_periodic:
                            fij += f

                        # quadrupole-dipole
                        energy_sr -= np.dot(dj,
                                            np.tensordot(T7, qi, axes=2)) / 3
                        f = np.tensordot(dj, np.tensordot(T9, qi, axes=2),
                                         axes=1) / 3
                        forces_sr[iat, :] += f
                        forces_sr[jat, :] -= f
                        if is_periodic:
                            fij -= f

                        # quadrupole-quadrupole
                        energy_sr += np.tensordot(qi,
                                                  np.tensordot(T9, qj, axes=2),
                                                  axes=2) / 9
                        f = np.tensordot(qi,
                                         np.tensordot(T11, qj, axes=2),
                                         axes=2) / 9
                        forces_sr[iat, :] -= f
                        forces_sr[jat, :] += f
                        if is_periodic:
                            fij += f

                        efield0_sr[iat] += np.tensordot(T5, qj, axes=2) / 3
                        efield0_sr[jat] += np.tensordot(T5, qi, axes=2) / 3
                        efield1_sr[iat, :] -= np.tensordot(T7, qj, axes=2) / 3
                        efield1_sr[jat, :] += np.tensordot(T7, qi, axes=2) / 3
                        efield2_sr[iat, :, :] -= np.tensordot(T9, qj,
                                                              axes=2) / 3
                        efield2_sr[jat, :, :] -= np.tensordot(T9, qi,
                                                              axes=2) / 3

                if is_periodic:
                    for i in range(3):
                        for j in range(3):
                            stress_sr[i, j] += \
                                (fij[i] * vec[j] + fij[j] * vec[i]) / (2 * vol)

        self.log('energy_sr [Ha]:', energy_sr)
        self.log('forces_sr [Ha/Bohr]:', forces_sr)
        if is_periodic:
            self.log('stress_sr [Ha/Bohr^3]:', stress_sr)

        # Reciprocal space
        energy_lr = 0.
        energy_si = 0.
        forces_lr = np.zeros((nat, 3))
        stress_lr = np.zeros((3, 3))
        efield0_lr = np.zeros(nat)
        efield1_lr = np.zeros((nat, 3))
        efield2_lr = np.zeros((nat, 3, 3))
        efield0_si = np.zeros(nat)
        efield1_si = np.zeros((nat, 3))
        efield2_si = np.zeros((nat, 3, 3))

        if is_periodic:
            gcut = self.determine_cutoff_gnorm(vol)

            ggrid = []
            for i in range(3):
                L = np.linalg.norm(invcell[i, :])
                gmax = int(np.ceil(gcut / (2. * np.pi * L)))
                ggrid.append(list(range(-gmax, gmax+1)))

            self.log('reciprocal grid shape:', [np.size(gs) for gs in ggrid])

            for g1 in ggrid[0]:
                for g2 in ggrid[1]:
                    for g3 in ggrid[2]:
                        if (g1 == 0 and g2 == 0 and g3 == 0):
                            continue

                        gvector = 2 * np.pi * np.matmul([g1, g2, g3],
                                                        invcell.T)
                        gnorm2 = sum(gvector**2)
                        Ag = np.exp(-gnorm2 / (4. * self.alpha**2)) / gnorm2

                        rhog = 0j
                        for iat in range(nat):
                            term = self.monopoles[iat]
                            if self.lmax >= 1:
                                term += 1j * np.dot(self.dipoles[iat], gvector)

                            if self.lmax >= 2:
                                term -= np.dot(gvector,
                                               np.matmul(self.quadrupoles[iat],
                                                         gvector)) / 3

                            phase = np.exp(1j*np.dot(gvector, pos[iat, :]))
                            rhog += term * phase

                        energy_lr += 2 * np.pi / vol * Ag * np.abs(rhog)**2

                        # efield*_lr
                        for iat in range(nat):
                            phase = np.exp(1j*np.dot(gvector, pos[iat, :]))

                            efield0_lr[iat] += 4 * np.pi / vol * Ag \
                                * np.real(phase * np.conj(rhog))

                            efield1_lr[iat, :] -= 4 * np.pi / vol * Ag \
                                * gvector * np.real(1j * phase * np.conj(rhog))

                            efield2_lr[iat, :] += 4 * np.pi / vol * Ag \
                                * gvector * np.array([gvector]).T \
                                * np.real(phase * np.conj(rhog))

                        # forces_lr
                        for iat in range(nat):
                            term = self.monopoles[iat]
                            if self.lmax >= 1:
                                term += 1j * np.dot(self.dipoles[iat], gvector)

                            if self.lmax >= 2:
                                term -= np.dot(gvector,
                                               np.matmul(self.quadrupoles[iat],
                                                         gvector)) / 3.

                            phase = np.exp(1j*np.dot(gvector, pos[iat, :]))
                            forces_lr[iat, :] -= 4 * np.pi / vol * Ag \
                                * np.real(rhog \
                                          * np.conj(1j*gvector*phase*term))

                        # stress_lr
                        drhogdg = np.zeros(3, dtype=complex)
                        for iat in range(nat):
                            phase = np.exp(1j*np.dot(gvector, pos[iat, :]))
                            term = np.zeros(3, dtype=complex)

                            if self.lmax >= 1:
                                term += 1j * self.dipoles[iat]

                            if self.lmax >= 2:
                                term -= np.matmul(self.quadrupoles[iat],
                                                  gvector) / 3.
                                term -= np.matmul(self.quadrupoles[iat].T,
                                                  gvector) / 3.
                            drhogdg += term * phase

                        for i in range(3):
                            for j in range(3):
                                c = 1. if i == j else 0.
                                term = c - 2 * (1. / gnorm2 \
                                                + 1. / (4 * self.alpha**2)) \
                                       * gvector[i] * gvector[j]

                                stress_lr[i, j] -= 2 * np.pi / vol**2 * Ag \
                                    * (term * np.abs(rhog)**2 + 2*gvector[i] \
                                       * np.real(rhog * np.conj(drhogdg[j])))

            stress_lr = 0.5 * (stress_lr + stress_lr.T)
            self.log('forces_lr [Ha/Bohr]:', forces_lr)
            self.log('stress_lr [Ha/Bohr^3]:', stress_lr)

            # Self-interaction
            for iat in range(nat):
                mi = self.monopoles[iat]

                energy_si += self.alpha / np.sqrt(np.pi) * mi**2

                efield0_si[iat] -= 2 * self.alpha / np.sqrt(np.pi) * mi
                efield2_si[iat, :, :] -= 8 * self.alpha**3 \
                                         / (3 * np.sqrt(np.pi)) \
                                         * mi * np.identity(3)

                if self.lmax >= 1:
                    di = self.dipoles[iat, :]

                    energy_si += 2 * self.alpha**3 / (3 * np.sqrt(np.pi)) \
                                 * np.linalg.norm(di)**2

                    efield1_si[iat, :] += 4 * self.alpha**3 \
                                          / (3 * np.sqrt(np.pi)) * di

                    if self.lmax >= 2:
                        qi = self.quadrupoles[iat, :, :]

                        energy_si -= 4 * self.alpha**3 / (9 * np.sqrt(np.pi)) \
                                     * mi * np.trace(qi)

                        energy_si += 8 * self.alpha**5 / (45 * np.sqrt(np.pi))\
                                     * (np.tensordot(qi, qi, axes=2) \
                                        + 0.5 * np.trace(qi)**2)

                        efield2_si[iat, :, :] += 16 * self.alpha**5 \
                                                 / (15 * np.sqrt(np.pi)) \
                                                 * (qi + 0.5 * np.identity(3) \
                                                    * np.trace(qi))

        self.log('energy_lr [Ha]:', energy_lr)
        self.log('energy_si [Ha]:', energy_si)

        energy = energy_sr + energy_lr - energy_si
        forces = forces_sr + forces_lr
        stress = stress_sr + stress_lr

        self.log('energy [Ha]:', energy)
        self.log('forces [Ha/Bohr]:', forces)
        if is_periodic:
            self.log('stress [Ha/Bohr^3]:', stress)

        efield0 = efield0_sr + efield0_lr + efield0_si
        efield1 = efield1_sr + efield1_lr + efield1_si
        efield2 = efield2_sr + efield2_lr + efield2_si

        e0, e1, e2 = 0., 0., 0.
        for iat in range(nat):
            e0 += efield0[iat] * self.monopoles[iat]

            if self.lmax >= 1:
                e1 -= np.dot(efield1[iat], self.dipoles[iat])

            if self.lmax >= 2:
                e2 -= np.tensordot(efield2[iat], self.quadrupoles[iat],
                                   axes=2) / 3.

        energy_check = (e0 + e1 + e2) / 2.
        energy_diff = energy - energy_check
        assert abs(energy_diff) < 1e-14, (energy, energy_check, energy_diff)
        self.log('energy_check [Ha]:', energy_check, 'diff:', energy_diff)

        assert np.allclose(np.sum(forces, axis=0), 0), forces
        assert np.allclose(stress, stress.T), stress

        self.results['energy'] = energy * Ha
        self.results['free_energy'] = energy * Ha
        self.results['forces'] = forces * Ha / Bohr
        self.results['stress'] = stress * Ha / Bohr**3
        self.results['efield0'] = efield0 * Ha  # electrostatic potential
        self.results['efield1'] = efield1 * Ha / Bohr  # E-field
        self.results['efield2'] = efield2 * Ha / Bohr**2  # E-field gradient

    def get_energy_moment_derivatives(self, index, l):
        if l == 0:
            return self.results['efield0'][index]
        elif l == 1:
            return -self.results['efield1'][index, :]
        elif l == 2:
            return -self.results['efield2'][index, :, :] / 3.
        else:
            raise NotImplementedError

    def set_monopoles(self, monopoles):
        self.monopoles = np.array(monopoles)

    def set_dipoles(self, dipoles):
        self.dipoles = np.array(dipoles)

    def set_quadrupoles(self, quadrupoles):
        for i in range(len(quadrupoles)):
            quad = quadrupoles[i, :, :]
            assert np.allclose(quad, quad.T), \
                   'Quadrupole moments must be symmetric'
        self.quadrupoles = np.array(quadrupoles)

    def determine_cutoff_radius(self):
        rc = 0.
        dr = 0.1
        if self.alpha == 0:
            rc = 1. / self.tolerance
        else:
            K = 2 * self.tolerance
            while K > self.tolerance:
                rc += dr
                K = erfc(self.alpha * rc) / rc
        return rc

    def determine_cutoff_gnorm(self, volume):
        gcut = 0.
        K = 2 * self.tolerance

        while K > self.tolerance:
            gcut += 2 * np.pi / volume**(1./3)
            K = 4 * np.pi / (volume * gcut**2) \
                * np.exp(-gcut**2 / (4 * self.alpha**2))
        return gcut

    def get_tensor(self, r, vec, p):
        f = self.get_screening_functions(r)

        if p == 1:
            T = f[0]
        elif p == 3:
            T = -f[1] * vec
        elif p == 5:
            T = np.zeros((3, 3))
            for i in range(3):
                for j in range(3):
                    T[i, j] = 3 * f[2] * vec[i] * vec[j]
                    if i == j:
                        T[i, j] -= f[1]
        elif p == 7:
            T = np.zeros((3, 3, 3))
            for i in range(3):
                for j in range(3):
                    for k in range(3):
                        T[i, j, k] = -15 * f[3] * vec[i] * vec[j] * vec[k]
                        if j == k:
                            T[i, j, k] += 3 * f[2] * vec[i]
                        if i == k:
                            T[i, j, k] += 3 * f[2] * vec[j]
                        if i == j:
                            T[i, j, k] += 3 * f[2] * vec[k]
        elif p == 9:
            T = np.zeros((3, 3, 3, 3))
            for i in range(3):
                for j in range(3):
                    for k in range(3):
                        for l in range(3):
                            T[i, j, k, l] = 105 * f[4] * vec[i] * vec[j] \
                                            * vec[k] * vec[l]
                            if k == l:
                                T[i, j, k, l] -= 15 * f[3] * vec[i] * vec[j]
                            if j == l:
                                T[i, j, k, l] -= 15 * f[3] * vec[i] * vec[k]
                            if j == k:
                                T[i, j, k, l] -= 15 * f[3] * vec[i] * vec[l]
                            if i == l:
                                T[i, j, k, l] -= 15 * f[3] * vec[j] * vec[k]
                            if i == k:
                                T[i, j, k, l] -= 15 * f[3] * vec[j] * vec[l]
                            if i == j:
                                T[i, j, k, l] -= 15 * f[3] * vec[k] * vec[l]
                            if i == j and k == l:
                                T[i, j, k, l] += 3 * f[2]
                            if i == k and j == l:
                                T[i, j, k, l] += 3 * f[2]
                            if i == l and j == k:
                                T[i, j, k, l] += 3 * f[2]
        elif p == 11:
            T = np.zeros((3, 3, 3, 3, 3))
            for i in range(3):
                for j in range(3):
                    for k in range(3):
                        for l in range(3):
                            for m in range(3):
                                T[i, j, k, l, m] = -945 * f[5] * vec[i] \
                                                   * vec[j] * vec[k] * vec[l] \
                                                   * vec[m]
                                if l == m:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[i] \
                                                        * vec[j] * vec[k]
                                if k == m:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[i] \
                                                        * vec[j] * vec[l]
                                if k == l:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[i] \
                                                        * vec[j] * vec[m]
                                if j == m:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[i] \
                                                        * vec[k] * vec[l]
                                if j == l:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[i] \
                                                        * vec[k] * vec[m]
                                if j == k:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[i] \
                                                        * vec[l] * vec[m]
                                if i == m:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[j] \
                                                        * vec[k] * vec[l]
                                if i == l:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[j] \
                                                        * vec[k] * vec[m]
                                if i == k:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[j] \
                                                        * vec[l] * vec[m]
                                if i == j:
                                    T[i, j, k, l, m] += 105 * f[4] * vec[k] \
                                                        * vec[l] * vec[m]

                                if i == j and l == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[k]
                                if i == j and k == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[l]
                                if i == k and l == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[j]
                                if i == k and j == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[l]

                                if i == l and k == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[j]
                                if i == l and j == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[k]
                                if j == k and l == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[i]
                                if j == k and i == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[l]

                                if j == l and k == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[i]
                                if j == l and i == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[k]
                                if k == l and j == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[i]
                                if k == l and i == m:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[j]

                                if i == j and k == l:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[m]
                                if i == k and j == l:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[m]
                                if j == k and i == l:
                                    T[i, j, k, l, m] -= 15 * f[3] * vec[m]
        else:
            raise NotImplementedError
        return T

    def get_screening_functions(self, r):
        N = 6
        f = np.zeros(N)
        a = self.alpha
        if a == 0:
            for n in range(N):
                f[n] = 1. / r**(2*n+1)
        else:
            f[0] = erfc(a * r) / r
            for n in range(1, N):
                f[n] = (f[n-1] + 2**n * a**(2*n-1) * np.exp(-a**2 * r**2) \
                        / (factorial2(2*n-1, exact=True) \
                        * np.sqrt(np.pi))) / r**2
        return f


if __name__ == '__main__':
    from ase import Atoms

    positions = np.array([
        [0.78611986,  3.35233271, -0.01173743],
        [0.93267004,  1.49854643,  1.0742059 ],
        [-0.26232522, 1.23185992, -0.16143373],
    ])

    cell = np.array([
        [2.90606443, -0.19014505, -0.12051073],
        [0.0928596,   4.79579319, -0.14915655],
        [0.24668985,  0.12062477,  3.96836933],
    ])

    atoms = Atoms('XXX', positions=positions, cell=cell, pbc=True)

    monopoles = np.array(
        [-1., 0.3, 0.7],
    ) * 0.22

    dipoles = np.array([
        [0.2, -0.3,  0.66],
        [-1., 0.25,  0.11],
        [0.3,  0.3, -0.4]],
    ) * 0.44

    quadrupoles = np.array([
        [[-0.17670977, -0.05498865, -0.38530125],
         [-0.05498865, -0.43017588,  0.21013141],
         [-0.38530125,  0.21013141,  0.42215393]],
        [[-0.14711642,  0.14756084, -0.06325197],
         [ 0.14756084,  0.46961809,  0.03047297],
         [-0.06325197,  0.03047297,  0.09067454]],
        [[ 0.10304001, -0.19671689, -0.0780669 ],
         [-0.19671689, -0.63204485,  0.0021007 ],
         [-0.0780669 ,  0.0021007 , -0.03550816]],
    ])

    energies = []
    etol, ftol, stol = 1e-7, 1e-8, 1e-9

    for alpha in [0.4, 2.0]:
        print('========== alpha {0} =========='.format(alpha))

        calc = MultipoleEwald(lmax=2, alpha=alpha, tolerance=1e-8,
                              verbose=True)
        calc.set_monopoles(monopoles)
        calc.set_dipoles(dipoles)
        calc.set_quadrupoles(quadrupoles)
        atoms.set_calculator(calc)

        e = atoms.get_potential_energy()
        print('Total energy [eV]:', e)
        energies.append(e)

        f = atoms.get_forces()
        print('Analytical forces [eV/Ang]:', f)

        s = atoms.get_stress()
        print('Analytical stress [eV/Ang^3]:', s)

        f_ref = calc.calculate_numerical_forces(atoms, d=1e-4)
        print('Numerical forces [eV/Ang]:', f_ref)
        assert np.max(np.abs(f - f_ref)) < ftol

        s_ref = calc.calculate_numerical_stress(atoms, d=1e-4)
        print('Numerical stress [eV/Ang^3]:', s_ref)
        assert np.max(np.abs(s - s_ref)) < stol

    assert np.max(np.abs(energies[0] - energies[1])) < etol
