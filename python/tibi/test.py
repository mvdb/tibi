#-----------------------------------------------------------------------------#
#   Tibi: an ab-initio tight-binding electronic structure code                #
#   Copyright 2020-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#

import numpy as np
import os


def colored_message(is_ok):
    """
    Returns a green colored "OK" or a red colored "FAIL"
    depending on the value of is_ok.
    """
    msg = '\033[92mOK\033[0m' if is_ok else '\033[91mFAIL\033[0m'
    return msg


class Tester:
    def __init__(self, diag_algo=None, verbose=True):
        self.verbose = verbose
        self.set_diag_algo(diag_algo)

        self.passed = 0
        self.failed = 0
        self.reason_for_skipping = None

    def _update_log(self, passed):
        if passed:
            self.passed += 1
        else:
            self.failed += 1

    def print_summary(self):
        msg = colored_message(self.failed == 0)
        if self.reason_for_skipping is None:
            print('summary: number of failures = %d    %s' % (self.failed, msg))
            assert self.failed == 0, 'One or more tests failed!'
        else:
            print('summary: skipped (%s)' % self.reason_for_skipping)

    def check_float(self, description, x, y, eps):
        passed = abs(x - y) < eps
        if self.verbose:
            line = '{: .8f} | reference: {: .8f} | '.format(x, y)
            line += colored_message(passed)
            print(description, '::', line)
        self._update_log(passed)

    def check_array(self, description, x, y, eps):
        diffs = np.abs(np.array(x) - np.array(y))
        passed = np.all(diffs < eps)
        if self.verbose:
            line = '%s | reference: %s | ' % (str(x), str(y))
            line += colored_message(passed)
            print(description, '::', line)
        self._update_log(passed)

    def check_int(self, description, x, y):
        passed = x == y
        if self.verbose:
            line = '%d | reference: %d | ' % (x, y)
            line += colored_message(passed)
            print(description, '::', line)
        self._update_log(passed)

    def check_bounds(self, description, x, bounds):
        passed = bounds[0] <= x <= bounds[1]
        if self.verbose:
            line = '{: .8f} | bounds: {: .8f} -> {: .8f} | '.format(x, *bounds)
            line += colored_message(passed)
            print(description, '::', line)
        self._update_log(passed)

    def get_diag_algo(self):
        return self.diag_algo

    def set_diag_algo(self, diag_algo):
        if diag_algo is None:
            test_diag = os.environ.get('TEST_DIAG', 'DC')
            diag2algo = dict(DC='divide&conquer', ELPA='elpa')
            try:
                self.diag_algo = diag2algo[test_diag.upper()]
            except KeyError:
                raise ValueError(f'Unexpected TEST_DIAG value "{test_diag}"')
        else:
            self.diag_algo = diag_algo

        if self.verbose:
            print(f'Info: using {self.diag_algo} eigensolvers')
        return
