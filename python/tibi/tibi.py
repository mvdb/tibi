#-----------------------------------------------------------------------------#
#   Tibi: an ab-initio tight-binding electronic structure code                #
#   Copyright 2020-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#

import warnings
import numpy as np
from ase.io import write
from ase.calculators.calculator import (CalculatorSetupError,
                                        FileIOCalculator,
                                        PropertyNotPresent)
from tibi.io import (add_kpoint_spin_matrices, add_single_matrix,
                     read_tibi_output, write_tibi_input)


error_template = 'Property "%s" not available. Please try running Tibi ' \
                 'first by calling e.g. atoms.get_potential_energy().'

warn_template = 'Property "%s" is None. Typically, this is because the ' \
                'required information has not been printed by Tibi ' \
                'at a low verbosity level. Please try unleashing Tibi ' \
                'with high verbosity.'


class Tibi(FileIOCalculator):
    """
    An ASE-style Calculator for the Tibi code

    The default command template assumes a Tibi executable
    named 'tibi' can be found in the :envvar:`$PATH`. To use a different
    executable, set the :envvar:`$ASE_TIBI_COMMAND` environment variable::

      export ASE_TIBI_COMMAND='/path/to/tibi PREFIX.in PREFIX.xyz > PREFIX.out'

    Parameters
    ----------
    aux_basis_giese_york : str or dict
        Dictionary indicating the Giese-York auxiliary basis set size for
        each element. To e.g. select a double-zeta auxiliary basis with s-
        and p-orbitals for Si, specify aux_basis_giese_york={'Si': '2P'}.
        If a string is provided instead of a dictionary, this value is
        applied to all elements.

        This keyword is only required for calculations in which Giese-York
        partitioning of the difference density are performed, i.e. when
        any of the 'output_giese_york_chg/mag' keywords is True or the
        'scf_aux_mapping' keyword is set to 'giese_york'. In such a case
        the following files are expected to be present in the directory
        specified by the 'tbpar_dir' keyword:

        * "<element1>-<element2>_offsiteM_<label1>.2cm" files
        * "<element1>-<element1>_onsiteM_<label1>.1cm" files

        where <label> refers to the (first) element's auxiliary basis set
        size.

    aux_basis_giese_york_eps : float
        Parameter for truncating the atom pairs that are included
        when evaluating the Giese-York mapping coefficients. Only those
        where any integral has a magnitude above this threshold will be
        considered (default: 1e-12). Use higher values to trade accuracy
        for speed.

    aux_basis_mulliken : str
        Type of Mulliken auxiliary basis to use in self-consistent
        calculations with scf_aux_mapping='mulliken'. The available options
        are:

        * 'atom_dependent': auxiliary basis with atom-dependent
          Mulliken charges and magnetizations. Atomic Hubbard values and
          spin constants are taken equal to those of the atom's lowest
          included angular momentum.
        * 'subshell_dependent': auxiliary basis with subshell-dependent
          Mulliken charges and magnetizations.

        No additional files are needed since the Mulliken mapping
        coefficients only depend on the overlap matrix. The default is
        'atom_dependent'.

    basis_set : dict
        Dictionary indicating the main basis set for each element.
        To e.g. select a double-zeta basis plus polarization functions
        for Si, specify basis_set={'Si': 'spd_sp'}. In this example,
        the parameter directory is then expected to contain (at minimum)
        files named 'Si-Si.skf', 'Si+-Si.skf', 'Si-Si+.skf' and
        'Si+-Si+.skf'. Here, 'Si' refers to the first-zeta ('spd') subset
        and 'Si+' to the second-zeta ('sp') subset.

    complex_wfn : bool
        Whether to use complex wave function coefficients (and
        Hamiltonian and overlap matrices). Defaults to True, and can
        be set to False for certain k-point grids (e.g. when only the
        Gamma-point is included), resulting in faster calculations with
        less memory usage.

    diag_algo : str
        The diagonalization algorithm. The available options are:

        * 'divide&conquer': LAPACK divide-and-conquer eigensolvers
          (either dsygvd, dsyevd, zhegvd or zheevd, depending on
          the 'diag_gen2std' and 'complex_wfn' input keywords).
          This is the default choice.
        * 'elpa': ELPA eigensolvers, which are expected to provide
          better performance when the number of basis functions grows
          large. Requires Tibi to be built with ELPA support.

    diag_elpa_blocksize : int
        Block size used by ELPA eigensolvers (default: 64).
        For a single-process, OpenMP-parallelized code such as Tibi,
        this can be seen as a cache blocking parameter which can be tuned
        for optimal ELPA performance.

    diag_gen2std : str
        Specifies how the generalized (GEVP) to standard eigenvalue problem
        (SEVP) transformations and backtransformations are handled.
        The available options are:

        * 'LAPACK': the transformations will be carried out by Tibi using
          LAPACK routines. The eigensolver (see the 'diag_algo' keyword)
          will only be tasked with solving the SEVP. This is the default.
        * 'eigensolver': the transformations get handled by the eigensolver
          (i.e. it will be tasked with solving the GEVP).

        When using LAPACK eigensolvers, there is effectively no difference
        between the two approaches. In the case of ELPA, the default
        ('LAPACK') is normally the more efficient one.

        Note: with LAPACK eigensolvers the 'diag_gen2std' value will only
        affect the first SCF iteration for each ionic step, since the
        'LAPACK' approach is always needed for subsequent SCF iterations
        because these eigensolvers do not offer the choice to
        reuse existing overlap matrix decompositions.

    diag_nband : int or float
        Parameter controlling the number of lowest eigenvectors that the
        eigensolver needs to compute (only influential for the ELPA
        eigensolvers, see the 'diag_algo' keyword). For the default value
        of 0, all eigenvectors will be calculated, i.e. as many as the
        total number of basis functions.

        Asking for fewer eigenvectors can reduce the time needed for
        diagonalization (note: the set of eigenvalues is always calculated
        in full). This can be done by setting 'diag_nband' as follows:

        * a positive integer sets the number of eigenvectors to this value,
        * a real number defines the number of eigenvectors as the given
          number times half the number of electrons (and then rounding up).

        In both cases the resulting number of eigenvectors needs to be
        equal to or larger than the bare minimum (half the number of
        electrons), otherwise an error is raised. If the resulting number
        of eigenvectors is larger than the number of basis functions,
        then it will be truncated to that value.

        Note that the number of eigenvectors should remain large enough
        to hold all fully and partially occupied bands for each k-point
        and spin channel. A warning will be issued at the end of the SCF
        cycle if this is not the case. Unsolved (and so normally
        unoccupied) eigenvectors will carry arbitrary values (see also
        the 'output_wfncoeff' keyword).

    diag_parallel_ks : bool
        When more than one k-point or spin channel is included (see the
        'kpts' and 'spin_polarized' keywords), an OpenMP-enabled Tibi
        build can be asked to distribute the Nkpt*Nspin diagonalization
        tasks over the available OpenMP threads by setting this
        'diag_parallel_ks' keyword to True. This may provide better
        performance compared to the default (False), where Tibi will
        execute the diagonalization tasks in serial (each potentially
        using multiple threads if the LAPACK library allows it).
        If Tibi has been linked to a threaded LAPACK or ELPA library,
        however, it should be verified that non- or single-threaded
        routines are used, as good performance seems difficult to achieve
        with nested parallelism.

    ewald_alpha : float
        Parameter for screening the real space Coulomb interactions
        using the complementary error function (i.e. screened by
        erfc(ewald_alpha * r), ewald_alpha in units of Angstrom^-1).
        This parameter is mostly relevant for periodic structures,
        where it determines the partitioning between the real space
        and reciprocal space terms in the Ewald summations. If the
        default value of 0.0 is used, Tibi will determine a suitable
        value (which may, however, not be optimal performance-wise).
        For non-periodic structures, the default value of 0.0 simply
        implies zero screening.

    ewald_tolerance : float
        Parameter controlling the accuracy of the electrostatics
        calculations. For the real space contributions, it defines a
        cutoff distance, beyond which a pair of unit charges will
        contribute less than the specified tolerance to the electro-
        static energy. For the reciprocal space contributions, it
        defines an analogous cutoff distance for the set of reciprocal
        lattice vectors. The default value of 0.0 will result in
        tolerances of 1e-6 eV and 1e-9 eV for periodic and non-periodic
        structures, respectively.

    include_repulsion : bool
        Whether to include repulsive interatomic interactions (default:
        True).

    include_scf : bool
        Whether to include self-consistent corrections in
        the tight-binding calculations (default: False).
        If set to True, see also the scf_* keywords.

    include_tightbinding : bool
        Whether to include tight-binding electronic interactions
        (default: True).

    kpts : str or list or tuple of 3 integers
        If 'kpts' is a list or tuple of 3 integers (e.g. the default
        [1, 1, 1]), then it is interpreted as the dimensions of a
        Monkhorst-Pack grid. Alternatively, a path to a text file
        can be provided from which the reduced k-point coordinates
        and their weights can be read (one line per k-point with
        the three coordinates followed by the weight). This mode
        allows to e.g. perform band structure calculations, possibly
        from a pre-converged density (making use of the 'scf_maxiter',
        'scf_must_converge' and 'scf_initial_guess' keywords).

    kpts_gamma_shift : bool
        Whether to shift the k-point grid so that the Gamma-point is
        included. This only applies to even Monkhorst-Pack grids
        (default: True).

    mixing_fraction : float
        The (linear) mixing fraction used by both the 'linear' and
        'pulay' mixing schemes. Must lie between 0 and 1; the default
        is 0.25.

    mixing_history : int
        Number of most recent iteration results to be stored by the
        mixer. If a linear mixing scheme is applied, the default and
        only possible value is 1. For the Pulay mixing scheme, the
        default is 6.

    mixing_pulay_precon : float
        Specifies the amount of preconditioning applied in Pulay
        mixing for increased numerical stability. Diagonal elements
        of the Pulay coefficient matrix are then scaled by (1+precon).
        The default value is 1e-6.

    mixing_pulay_restart : bool
        Whether to apply the periodically restarted ("r-Pulay") variant
        instead of the original Pulay method (default: False).

    mixing_pulay_restart_history : int
        Number of latest vectors (if any) to keep when applying a
        periodic restart in the r-Pulay method. The value must be at
        least 0 and must be less than the value of the 'mixing_history'
        keyword. Default: 1.

    mixing_scheme : str
        Name of the mixing scheme to be used to reach self-consistency
        (choose either 'linear' or 'pulay'). Pulay mixing (also known
        as DIIS) is the default.

    net_charge : float
        Total charge in units of elementary (positive) charge (default:
        0). Nonzero values are only allowed for nonperiodic structures.

    output_bands : bool
        Whether to write out the electronic bands and their occupations
        after obtaining the (converged) electronic energy
        (default: False).

    output_densmat : bool
        Whether to write out the density matrix after obtaining the
        (converged) electronic energy (default: False).

    output_ew_densmat : bool
        Whether to write out the energy-weighted density matrix after
        obtaining the (converged) electronic energy (default: False).

    output_giese_york_chg : bool
        Whether to write out the Giese-York auxiliary charges,
        after obtaining the (converged) electronic energy (default: False).

    output_giese_york_mag : bool
        Whether to write out the Giese-York auxiliary magnetizations,
        after obtaining the (converged) electronic energy (default: False).

    output_hamiltonian : bool
        Whether to write out the Hamiltonian matrix before solving
        the LCAO equations (default: False). In self-consistent tight-
        binding calculations, this is only done at the beginning of
        the SCF cycle.

    output_mulliken_orbital_mag : bool
        Whether to write out the Mulliken orbital magnetizations
        after obtaining the (converged) electronic energy in spin-
        polarized calculations (default: False).

    output_mulliken_orbital_pop : bool
        Whether to write out the Mulliken orbital populations
        after obtaining the (converged) electronic energy
        (default: False).

    output_mulliken_subshell_chg : bool
        Whether to print the Mulliken subshell and atom charges in
        the output after obtaining the (converged) electronic energy
        (default: False).

    output_mulliken_subshell_mag : bool
        Whether to print the Mulliken subshell and atom magnetizations in
        the output at after obtaining the (converged) electronic energy
        in spin-polarized calculations (default: False). The total
        magnetization always gets printed in such calculations.

    output_overlap : bool
        Whether to write out the overlap matrix before solving
        the LCAO equations (default: False). In self-consistent tight-
        binding calculations, this is only done at the beginning of
        the SCF cycle.

    output_wfncoeff : bool
        Whether to write out the wave function coefficient matrix
        after obtaining the (converged) electronic energy
        (default: False).

    overlap_2c_eps : float
        Threshold governing whether two atoms contribute to the
        Hamiltonian and overlap matrices. If the maximum absolute value
        of the diatomic overlap matrix is below this threshold, the
        atom pair will not be included, as if the Hamiltonian and
        overlap matrices were exactly zero. Default: 1e-16.

    overlap_2c_switching_degree : int
        As the two-center overlap integrals may not reach (exactly) zero
        at the last grid point, it is best to apply a switching function
        that lets the integrals smoothly go to 0 beyond the grid.
        This parameter sets the degree of the polynomial switching
        function and can be either 3 or 5. The first value is the default
        and is arguably the more prudent choice. The latter value may be
        useful to get closer agreement with the DFTB+ code.

    overlap_2c_switching_length : float
        This parameter specifies the length of switching interval for the
        two-center overlap integrals (see overlap_2c_switching_degree),
        which will be rounded up to an integer number of spline segments
        (default: 0.5 Ang).

    relax_algo : str
        Name of the relaxation algorithm to apply for 'relaxation'
        tasks. The available options are:

        * 'lbfgs': the limited-memory BFGS algorithm (default).

    relax_fmax : float
        Convergence threshold for the force magnitudes. When the force
        on each atom drops below this threshold, the relaxation is
        stopped. Default: 0.05 eV/Angstrom.

    relax_history : int
        Number of most recent iteration results to be stored in the
        (LBFGS) relaxation algorithm. Default: 50.

    relax_initial_curvature : float
        Initial curvature in all directions, used in representing
        the initial inverse Hessian in the LBFGS method. Default:
        100 eV/Angstrom^2.

    relax_maxiter : int
        Maximum number of relaxation iterations. Default: 250.

    relax_maxstep : float
        Largest allowed atom displacement in a relaxation step.
        If necessary, steps are rescaled to satisfy this requirement.
        Default: 0.5 Angstrom.

    relax_must_converge : bool
        Whether to abort in case force convergence is not reached within
        the maximum number of relaxation iterations (default: True).
        If False, only a warning gets printed in such cases and the
        run continues.

    relax_trajectory_append : bool
        Whether the trajectory file is appended with the new geometries
        during relaxation (True) or gets overwritten instead (False).
        Default: True.

    relax_trajectory_file : str
        File to which the geometry will be written during a relaxation
        task, in "extended" XYZ format (default: 'relax.xyz').

    relax_trajectory_frequency : int
        Frequency at which the relaxation trajectory file will be
        updated, expressed in number of relaxation steps. The default
        value is 1, i.e. at every step. The last geometry always gets
        written, unless the frequency is set to 0. To only write at
        the last step, choose -1.

    relax_vc : bool
        Whether to perform a variable cell ('vc') relaxation in which
        the cell vectors are allowed to vary. Default: False.
        Atoms that are marked as fixed will maintain a constant
        scaled position in the unit cell.

    relax_vc_maxstep : float
        Largest allowed change in the cell vector components in a
        vc-relaxation step. If necessary, steps are rescaled to satisfy
        this requirement. Default: 0.5 Angstrom.

    relax_vc_scale : float
        The scaled forces in a vc-relaxation are divided by this factor
        to grant them a similar scale (and unit) as the cell vector
        derivatives. With the default value of 0 Angstrom, the cube root
        of the initial cell volume is used as a scaling factor.

    relax_vc_smax : float
        Convergence threshold for the stress magnitudes in a
        vc-relaxation. When the absolute value of all stress components
        atom drops below this threshold and the force magnitudes drop
        below the relax_fmax threshold, the vc-relaxation is stopped.
        Default: 1e-3 eV/Angstrom^3.

    repulsion_2c_form : str
        The functional form of the two-center repulsive potentials
        (either 'spline' (the default) or 'polynomial').

    repulsion_3c_binary_io : bool
        Whether to try to read the parameter files in binary format
        (.3cb) instead of ASCII format (.3cf). If False (the default),
        no .3cb files will be involved (i.e. only reading from .3cf
        files). If True and a .3cf file does not exist, then the .3cf
        file is read and the corresponding .3cb file is written to the
        'tbpar_dir' directory. If True and a .3cb file exists, then this
        will be read instead of the corresponding .3cf file. This
        happens at a much faster rate, allowing to reduce the
        initialization time.

    repulsion_3c_eps_inner : float
        Parameter influencing the choice of interpolation order for
        the three-center contributions to the repulsive energy
        (default: 1e-12 eV). Those with magnitudes above this threshold
        will be evaluated with the order given by the
        'repulsion_3c_interpolation' keyword, otherwise linear
        interpolation will be applied. Must be larger than or equal to
        the value given by the 'repulsion_3c_eps_outer' keyword.

    repulsion_3c_eps_outer : float
        Parameter for truncating the atom triplets that are included
        when evaluating the three-center contributions to the repulsive
        energy. Only those with magnitudes above this threshold will be
        considered (default: 1e-12 eV). Must be smaller than or equal to
        the value given by the 'repulsion_3c_eps_inner' keyword.

    repulsion_3c_interpolation : int
        Interpolation order for the three-center repulsion evaluation.
        Allowed choices are 1 (linear) and 3 (cubic, the default).
        Note that non-linear interpolation, if chosen, only gets used
        for sufficiently large contributions (see the
        'repulsion_3c_eps_inner' keyword).

    repulsion_3c_scaling : float
        Empirical scaling factor to reduce the 3-body repulsion
        contribution. Expected to lie between 0 and 1 (default: 1).

    repulsion_3c_upsampling : list or tuple of 3 integers
        Upsampling factors for refining the three-center repulsion tables.
        Such denser grids are typically required to get accurate results
        with the linear interpolation method. Default: [1, 1, 1],
        i.e. no upsampling.

    repulsion_expansion : int
        Term at which to truncate the multicenter expansion of the
        repulsive energy. Available options are 2 and 3, the default
        being 2 (i.e. a two-center approximation). The following file
        types are expected in the directory specified by 'tbpar_dir'
        keyword:

        * 2 and 3: "<element1>-<element2>.skf" files
        * 3: "<element1>-<element2>_repulsion3c_<element3>.3cf" files
          (see also the 'repulsion_3c_binary_io' keyword)

    scf_aux_mapping : str
        Choice of mapping scheme for self-consistent calculations,
        i.e. how the second-order contributions to the Hamiltonian are
        calculated from the density matrix. The available options are:

        * 'giese_york':
          Giese-York mapping (doi:10.1063/1.3587052), see also the
          aux_basis_giese_york* keywords. Aside from the necessary mapping
          coefficient tables, one or more of the following file types are
          expected in the directory specified by the 'tbpar_dir' keyword:

          - "<element1>-<element1>_onsiteU.1ck"
            (always)
          - "<element1>-<element2>_offsiteU.2ck"
            (always)
          - "<element1>-<element1>_onsiteU_<element2>.2ck"
            (if scf_ukernel_expansion_onsite >= 2)
          - "<element1>-<element1>_onsiteW.1ck"
            (if spin_polarized=True)
          - "<element1>-<element1>_onsiteW_<element2>.2ck"
            (if spin_polarized=True and scf_wkernel_expansion_onsite >= 2)
          - "<element1>-<element2>_offsiteW.2ck"
            (if spin_polarized=True and scf_wkernel_expansion_offsite >= 2)

        * 'mulliken': (default)
          Mulliken mapping, equivalent to the so-called SCC-TB method
          (e.g. doi:10.1103/PhysRevB.58.7260 and
          doi:10.1016/j.chemphys.2004.03.034), see also the
          aux_basis_mulliken* keywords). No additional files are needed
          if scf_ukernel_expansion_onsite=1 and
          scf_ukernel_off2c_model='elstner' (and
          scf_wkernel_expansion_onsite=1 and
          scf_wkernel_expansion_offsite=0 if spin_polarized=True).
          In these cases the Hubbard parameters are obtained from the
          corresponding SKF files and the spin constants according to
          the 'scf_wkernel_on1c' keyword. Otherwise one or more of the
          following file types are expected in the directory specified
          by the 'tbpar_dir' keyword:

          - "<element1>-<element2>_offsiteU.2cl"
            (if scf_ukernel_off2c_model='tabulated')
          - "<element1>-<element2>_offsiteW.2cl"
            (if spin_polarized=True and scf_wkernel_expansion_offsite >= 2)
          - "<element1>-<element1>_onsiteU.1cl"
            (if scf_ukernel_on1c_model='tabulated')
          - "<element1>-<element1>_onsiteU_<element2>.2cl"
            (if scf_ukernel_expansion_onsite >= 2)
          - "<element1>-<element1>_onsiteW.1cl"
            (if spin_polarized=True and scf_wkernel_on1c='tabulated')
          - "<element1>-<element1>_onsiteW_<element2>.2cl"
            (if spin_polarized=True and scf_wkernel_expansion_onsite >= 2)

    scf_aux_use_delta : bool
        Whether to handle the multipole electrostatics by subtracting
        the corresponding point multipole contributions and re-add
        those separately (using e.g. Ewald summation methods).
        Setting to False (instead of the default True) only makes
        sense for verification purposes on small non-periodic structures.
        This keyword is only considered if the scf_aux_mapping keyword
        is set to 'giese_york'.

    scf_extrapolation : str
        The method for 'extrapolating' the charges (and, if spin-
        polarized, also magnetizations) between successive SCF cycles
        (e.g. between ionic steps). The available options are:

        * 'use_guess': use the same method as for the initial guess,
        * 'use_previous': use the last values from the previous SCF
                          cycle (default).

    scf_initial_guess : str
        The method for guessing the initial charge distribution (and,
        if spin-polarized, also the magnetizations) for the first SCF
        iteration. The available options are:

        * 'restart': read the initial values from a restart file
                     (see also the 'scf_restart_input_file' keyword),
        * 'zero': set all initial values to zero (default).

        Note: if needed, the initial values will be corrected so that
        their sum equals the value given by the 'net_charge' and
        'spin_nupdown' keywords for the charges and magnetizations,
        respectively.

    scf_maxiter : int
        The maximum number of iterations in the SCF cycle (default:
        250).

    scf_must_converge : bool
        Whether to abort in case SCF convergence is not reached within
        the maximum number of iterations (default: True). If False,
        only a warning gets printed in such cases and the run continues.

    scf_restart_input_file : str
        File with auxiliary basis coefficients to use as initial guesses
        when choosing 'restart' as initial guess method for the SCF cycle.
        The default file name is 'scf_restart.in'. The format of the file
        is the same as that of the restart output file (see the
        'scf_restart_output_file' keyword).

        Note that only the sum of the subshell-dependent charges/
        magnetizations will be considered (and not the individual
        components) if the 'scf_aux_mapping' and 'aux_basis_mulliken'
        keywords are set to 'mulliken' and 'atom_dependent', respectively.

    scf_restart_output_file : str
        File to which auxiliary basis coefficients will be written for
        restarts if enabled by the 'scf_write_restart_file' keyword.
        The default file name is 'scf_restart.out'.

        One line gets printed for each atom and the line contents depend
        on the choice for the 'scf_aux_mapping' keyword:

        * 'giese_york': the atom's Giese-York auxiliary charges (and, if
          spin_polarized=True, also magnetizations), in accordance with
          the atom's auxiliary basis set.
        * 'mulliken': the atom's Mulliken subshell charges (and, if
          spin_polarized=True, also subshell magnetizations)
          in the same order as the atom's entry in the 'basis_set' keyword.

    scf_tolerance : float
        Convergence criterion for the SCF cycle, which is stopped
        when the absolute changes in the (auxiliary-basis-dependent)
        induced potentials drop below this value. The default is 1e-3 eV.
        Note that the change in the total electronic energy will be much
        lower than this value.

    scf_ukernel_expansion_offsite : int
        Term at which to truncate the multicenter expansion of the
        Hartree-XC kernel in off-site 'U' integrals. The only choice
        is 2 (i.e. a two-center approximation).

    scf_ukernel_expansion_onsite : int
        Term at which to truncate the multicenter expansion of the
        Hartree-XC kernel in on-site 'U' integrals. Available options
        are 1 and 2, the default being 1 (i.e. a one-center
        approximation).

    scf_ukernel_off2c_model : str
        Model for the off-site two-center gamma functions (here
        referred to as 'U' integrals) used in the SCF energy expression.
        The available options are:

        * 'elstner': use the expressions by Elstner and coworkers
          (doi:10.1103/PhysRevB.58.7260). This is the default.
        * 'tabulated': use spline interpolation of tabulated values
          (see the 'scf_aux_mapping' keyword for more information).

    scf_ukernel_on1c_model : str
        Model for the on-site, one-center hubbard values 'U' (only used
        in the case of Mulliken mapping; see the 'scf_aux_mapping'
        keyword). The available options are:

        * 'elstner': use the diagonal U values from the 'skf' files
          and approximate the off-diagonal ones using the expression
          by Elstner and coworkers (doi:10.1103/PhysRevB.58.7260
          and the dissertation by J. Elsner (1998)). This is the default.
        * 'tabulated': read all (diagonal and off-diagonal) U values
          from '1cl' files.

    scf_wkernel_expansion_offsite : int
        Term at which to truncate the multicenter expansion of the
        spin-polarized XC kernel in off-site 'W' integrals. Available
        options are 0 and 2, the default being 0 (i.e. no off-site
        contributions).

    scf_wkernel_expansion_onsite : int
        Term at which to truncate the multicenter expansion of the
        spin-polarized XC kernel in on-site 'W' integrals. Available
        options are 1 and 2, the default being 1.

    scf_wkernel_on1c : str or dict
        Source of the on-site, one-center spin constants 'W' (only used
        in the case of Mulliken mapping; see the 'scf_aux_mapping'
        keyword). The available options are:

        * 'tabulated' (the default), indicating that the values need to be
          read from '1cl' files. See also the 'scf_aux_mapping' keyword.

        * a dictionary with the W values (in eV) for every pair of
          subshells of each element. For e.g. Si atoms with 'sp' subshells
          (see also the 'basis_set' keyword), the dictionary should be
          {'Si': [<Wss>, <Wsp>, <Wps>, <Wpp>]} or just {'Si: <W>},
          depending on the value of 'aux_basis_mulliken'.

    scf_write_restart_file : bool
        Whether to write a restart file at the end of each self-
        consistency cycle (see the 'scf_restart_output_file' for more
        information). Default: True.

    scf_write_verbose : bool
        Whether to write additional quantities to file (typically
        for debugging or testing purposes). Currently includes the
        writing of 'scf_kernel_X.dat' files (X = U/W) containing the
        kernels at the start of an SCF cycle. Default: False.

    smearing_method : str
        The method for smearing the occupation numbers. Either choose
        'none' or 'fermi-dirac'.

    smearing_width : float
        The smearing width in eV for Fermi-Dirac smearing (default:
        0.01).

    socket_inet_host : str
        Server hostname, specific to INET sockets (default: 'localhost').

    socket_inet_port : int
        Port number, specific to INET sockets (default: 31415).

    socket_type : str
        Which type of socket connection:

        * 'inet': an internet socket.
        * 'unix': a UNIX domain socket (basically a file in /tmp)
                  (default).

    socket_unix_suffix : str
        Filename suffix to use for UNIX sockets. The full filename will
        be '/tmp/ipi_<suffix>'. Default: 'tibi'.

    spin_fix_nupdown : bool
        Whether to keep 'spin_nupdown' fixed to its initial value,
        or allow it to vary so that the Fermi levels of both spin
        channels are the same. Default: False.

    spin_nupdown : float
        The difference between the number of electrons in the two spin
        channels in a spin-polarized calculation. If 'spin_fix_nupdown'
        is set to False, then the present value will only be used when
        a 'zero' SCF initial guess is demanded. Default: 0.

    spin_polarized : bool
        Whether to include spin polarization. Default: False
        (which is also the only option at the moment).

    task : str
        The kind of calculation to perform:

        * 'energy': a single-point calculation of the total energy
                    (default).
        * 'energy_forces': same as 'energy', plus the forces (and the
                    stress tensor, if the structure is periodic).
        * 'relaxation': minimization of the total energy with respect
                    to the atomic positions and/or cell vectors.
        * 'socket': run as a socket communication client via the i-PI
                    protocol.

    tbpar_dir : str
        The path to the directory where the tight-binding parameter
        files are located (default: './').

    veff_2c_switching_degree : int
        As the two-center H integrals may not reach (exactly) zero
        at the last grid point, it is best to apply a switching function
        that lets the integrals smoothly go to 0 beyond the grid.
        This parameter sets the degree of the polynomial switching
        function and can be either 3 or 5. The first value is the default
        and is arguably the more prudent choice. The latter value may be
        useful to get closer agreement with the DFTB+ code.

    veff_2c_switching_length : float
        This parameter specifies the length of switching interval for
        the two-center H integrals (see veff_2c_switching_degree),
        which will be rounded up to an integer number of spline segments
        (default: 0.5 Ang).

    veff_3c_binary_io : bool
        Whether to try to read the parameter files in binary format
        (.3cb) instead of ASCII format (.3cf). If False (the default),
        no .3cb files will be involved (i.e. only reading from .3cf
        files). If True and a .3cf file does not exist, then the .3cf
        file is read and the corresponding .3cb file is written to the
        'tbpar_dir' directory. If True and a .3cb file exists, then this
        will be read instead of the corresponding .3cf file. This
        happens at a much faster rate, allowing to reduce the
        initialization time.

    veff_3c_eps_inner : float
        Parameter influencing the choice of interpolation order for
        the three-center contributions to the Hamiltonian (default: 1e-12
        eV). Those with magnitudes above this threshold will be evaluated
        with the order given by the 'veff_3c_interpolation' keyword,
        otherwise linear interpolation will be applied. Must be larger
        than or equal to the value given by the 'veff_3c_eps_outer'
        keyword.

    veff_3c_eps_outer : float
        Parameter for truncating the atom triplets that are included
        when evaluating the three-center contributions to the Hamiltonian.
        Only those with magnitudes above this threshold will be considered
        (default: 1e-12 eV). Must be smaller than or equal to the value
        given by the 'veff_3c_eps_inner' keyword.

    veff_3c_interpolation : int
        Interpolation order for the three-center integral evaluation.
        Allowed choices are 1 (linear) and 3 (cubic, the default).
        Note that non-linear interpolation, if chosen, only gets used
        for sufficiently large contributions (see the 'veff_3c_eps_inner'
        keyword).

    veff_3c_upsampling : list or tuple of 3 integers
        Upsampling factors for refining the three-center integral tables.
        Such denser grids are typically required to get accurate results
        with the linear interpolation method. Default: [1, 1, 1],
        i.e. no upsampling.

    veff_expansion_offsite : int
        Term at which to truncate the multicenter expansion of the
        effective potential in the off-site Hamiltonian integrals.
        Available options are 2 and 3, the default being 2
        (i.e. a two-center approximation). The following file types
        are expected in the directory specified by 'tbpar_dir' keyword:

        * 2 and 3: "<element1>-<element2>.skf" files
        * 3: "<element1>-<element2>_offsite3c_<element3>.3cf" files
          (see also the 'veff_3c_binary_io' keyword)

    veff_expansion_onsite : int
        Term at which to truncate the multicenter expansion of the
        effective potential in the on-site Hamiltonian integrals.
        Available options are 1, 2, and 3, the default being 1
        (i.e. a one-center approximation). The following file types
        are expected in the directory specified by 'tbpar_dir' keyword:

        * 1, 2 and 3: "<element1>-<element1>.skf" files
        * 2 and 3: "<element1>-<element1>_onsite2c_<element2>.skf" files
        * 3: "<element1>-<element1>_onsite3c_<element2>-<element3>.3cf"
             files  (see also the 'veff_3c_binary_io' keyword)
    """
    implemented_properties = ['energy', 'free_energy', 'forces', 'stress']

    command = 'tibi PREFIX.in PREFIX.xyz > PREFIX.out'

    default_parameters = dict(
        aux_basis_giese_york={},
        aux_basis_giese_york_eps=1e-12,
        aux_basis_mulliken='atom_dependent',
        basis_set={},
        complex_wfn=True,
        diag_algo='divide&conquer',
        diag_elpa_blocksize=64,
        diag_gen2std='LAPACK',
        diag_nband=0,
        diag_parallel_ks=False,
        ewald_alpha=0.,
        ewald_tolerance=0.,
        include_repulsion=True,
        include_scf=False,
        include_tightbinding=True,
        kpts=[1, 1, 1],
        kpts_gamma_shift=True,
        mixing_fraction=0.25,
        mixing_history=6,
        mixing_pulay_precon=1e-6,
        mixing_pulay_restart=False,
        mixing_pulay_restart_history=1,
        mixing_scheme='pulay',
        net_charge=0.,
        output_bands=False,
        output_densmat=False,
        output_ew_densmat=False,
        output_giese_york_chg=False,
        output_giese_york_mag=False,
        output_hamiltonian=False,
        output_mulliken_orbital_mag=False,
        output_mulliken_orbital_pop=False,
        output_mulliken_subshell_chg=False,
        output_mulliken_subshell_mag=False,
        output_overlap=False,
        output_wfncoeff=False,
        overlap_2c_eps=1e-16,
        overlap_2c_switching_degree=3,
        overlap_2c_switching_length=0.5,
        relax_algo='lbfgs',
        relax_fmax=0.05,
        relax_history=50,
        relax_initial_curvature=100.,
        relax_maxiter=250,
        relax_maxstep=0.5,
        relax_must_converge=True,
        relax_trajectory_append=True,
        relax_trajectory_file='relax.xyz',
        relax_trajectory_frequency=1,
        relax_vc=False,
        relax_vc_maxstep=0.5,
        relax_vc_scale=0,
        relax_vc_smax=1e-3,
        repulsion_2c_form='spline',
        repulsion_3c_binary_io=False,
        repulsion_3c_eps_inner=1e-12,
        repulsion_3c_eps_outer=1e-12,
        repulsion_3c_interpolation=3,
        repulsion_3c_scaling=1.,
        repulsion_3c_upsampling=[1, 1, 1],
        repulsion_expansion=2,
        scf_aux_mapping='mulliken',
        scf_aux_use_delta=True,
        scf_extrapolation='use_previous',
        scf_initial_guess='zero',
        scf_maxiter=250,
        scf_must_converge=True,
        scf_restart_input_file='scf_restart.in',
        scf_restart_output_file='scf_restart.out',
        scf_tolerance=1e-3,
        scf_ukernel_expansion_offsite=2,
        scf_ukernel_expansion_onsite=1,
        scf_ukernel_off2c_model='elstner',
        scf_ukernel_on1c_model='elstner',
        scf_wkernel_expansion_offsite=0,
        scf_wkernel_expansion_onsite=1,
        scf_wkernel_on1c='tabulated',
        scf_write_restart_file=True,
        scf_write_verbose=False,
        smearing_method='none',
        smearing_width=0.01,
        socket_inet_host='localhost',
        socket_inet_port=31415,
        socket_type='unix',
        socket_unix_suffix='tibi',
        spin_fix_nupdown=False,
        spin_nupdown=0,
        spin_polarized=False,
        task='energy',
        tbpar_dir='./',
        veff_2c_switching_degree=3,
        veff_2c_switching_length=0.5,
        veff_3c_binary_io=False,
        veff_3c_eps_inner=1e-12,
        veff_3c_eps_outer=1e-12,
        veff_3c_interpolation=3,
        veff_3c_upsampling=[1, 1, 1],
        veff_expansion_offsite=2,
        veff_expansion_onsite=1,
    )

    def __init__(self, restart=None, label='tibi', atoms=None, command=None,
                 **kwargs):
        FileIOCalculator.__init__(self, restart=restart, label=label,
                                  atoms=atoms, command=command, **kwargs)
        self.calc = None

    def set(self, **kwargs):
        for key in kwargs:
            if key not in self.default_parameters:
                msg = '"%s" is not a known keyword for the Tibi calculator.'
                raise CalculatorSetupError(msg % key)

        changed_parameters = FileIOCalculator.set(self, **kwargs)
        if changed_parameters:
            self.reset()

    def write_input(self, atoms, properties=None, system_changes=None):
        FileIOCalculator.write_input(self, atoms, properties, system_changes)

        with open(self.label + '.in', 'w') as f:
            write_tibi_input(f, **self.parameters)

        write(self.label + '.xyz', atoms,
              columns=['symbols', 'positions', 'move_mask'],
              write_info=False, write_results=False)

    def read_results(self):
        with open(self.label + '.out', 'r') as f:
            output = next(read_tibi_output(f))

        self.calc = output.calc
        self.results = output.calc.results

        # Note: as it stands, the following (potentially large) data
        # are read in at every ionic step
        if self.parameters['include_tightbinding']:
            mtypes = ['bands', 'giese_york_chg', 'mulliken_orbital_pop']
            if self.parameters['spin_polarized']:
                mtypes.extend(['giese_york_mag', 'mulliken_orbital_mag'])

            for mtype in mtypes:
                key = 'output_%s' % mtype
                if self.parameters[key]:
                    add_single_matrix(mtype, self.calc)

            mtypes = ['densmat', 'ew_densmat', 'hamiltonian', 'overlap',
                      'wfncoeff']

            for mtype in mtypes:
                key = 'output_%s' % mtype
                if self.parameters[key]:
                    add_kpoint_spin_matrices(mtype, self.calc,
                                             self.parameters['complex_wfn'])

    def get_density_matrix(self, kpt=0, spin=0):
        return self._get_matrix('densmat', kpt=kpt, spin=spin)

    def get_eigenvalues(self, **kwargs):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Eigenvalues')
        eigenvalues = self.calc.get_eigenvalues(**kwargs)
        if eigenvalues is None:
            warnings.warn(warn_template % 'Eigenvalues')
        return eigenvalues

    def get_energy_weighted_density_matrix(self, kpt=0, spin=0):
        # Note: in atomic units
        return self._get_matrix('ew_densmat', kpt=kpt, spin=spin)

    def get_fermi_level(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Fermi level')
        return self.calc.get_fermi_level()

    def get_hamiltonian_matrix(self, kpt=0, spin=0):
        # Note: in atomic units
        return self._get_matrix('hamiltonian', kpt=kpt, spin=spin)

    def get_ibz_k_points(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'IBZ k-points')
        ibzkpts = self.calc.get_ibz_k_points()
        if ibzkpts is None:
            warnings.warn(warn_template % 'IBZ k-points')
        return ibzkpts

    def get_k_point_weights(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'K-point weights')
        k_point_weights = self.calc.get_k_point_weights()
        if k_point_weights is None:
            warnings.warn(warn_template % 'K-point weights')
        return k_point_weights

    def get_magnetization(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Magnetization')
        return self.calc.results['magnetization']

    def _get_matrix(self, mtype, kpt=0, spin=0):
        if self.calc is None:
            raise PropertyNotPresent(error_template % mtype)
        matrices = self.calc.results[mtype]
        if matrices is None:
            warnings.warn(warn_template % mtype)
            return None
        else:
            return np.copy(matrices[(kpt, spin)])

    def _get_single_matrix(self, which):
        if self.calc is None:
            raise PropertyNotPresent(error_template % which)
        properties = self.calc.results[which]
        if properties is None:
            warnings.warn(warn_template % which)
            return None
        else:
            return properties

    def get_number_of_spins(self):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Number of spins')
        nspins = self.calc.get_number_of_spins()
        if nspins is None:
            warnings.warn(warn_template % 'Number of spins')
        return nspins

    def get_occupation_numbers(self, **kwargs):
        if self.calc is None:
            raise PropertyNotPresent(error_template % 'Occupations')
        occupations = self.calc.get_occupation_numbers(**kwargs)
        if occupations is None:
            warnings.warn(warn_template % 'Occupations')
        return occupations

    def get_giese_york_chg(self):
        return self._get_single_matrix(which='giese_york_chg')

    def get_giese_york_mag(self):
        return self._get_single_matrix(which='giese_york_mag')

    def get_mulliken_atom_chg(self):
        return self._get_single_matrix(which='mulliken_atom_chg')

    def get_mulliken_atom_mag(self):
        return self._get_single_matrix(which='mulliken_atom_mag')

    def get_mulliken_orbital_mag(self):
        return self._get_single_matrix(which='mulliken_orbital_mag')

    def get_mulliken_orbital_pop(self):
        return self._get_single_matrix(which='mulliken_orbital_pop')

    def get_mulliken_subshell_chg(self):
        return self._get_single_matrix(which='mulliken_subshell_chg')

    def get_mulliken_subshell_mag(self):
        return self._get_single_matrix(which='mulliken_subshell_mag')

    def get_overlap_matrix(self, kpt=0, spin=0):
        return self._get_matrix('overlap', kpt=kpt, spin=spin)

    def get_wfncoeff_matrix(self, kpt=0, spin=0):
        return self._get_matrix('wfncoeff', kpt=kpt, spin=spin)
