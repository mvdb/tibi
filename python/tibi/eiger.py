#-----------------------------------------------------------------------------#
#   Tibi: an ab-initio tight-binding electronic structure code                #
#   Copyright 2020-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#

from ase import Atoms
from ase.calculators.calculator import FileIOCalculator
from ase.calculators.singlepoint import SinglePointCalculator
from tibi.io import write_tibi_input as write_eiger_input
from tibi.tibi import Tibi


error_template = 'Property "%s" not available. Please try running Eiger ' \
                 'first by calling e.g. atoms.get_potential_energy().'


class Eiger(FileIOCalculator):
    """
    An ASE-style Calculator for the Eiger program.

    The default command template assumes an Eiger executable
    named 'eiger' can be found in the :envvar:`$PATH`. To use a different
    executable, set the :envvar:`$ASE_EIGER_COMMAND` environment variable::

      export ASE_EIGER_COMMAND='/path/to/eiger PREFIX.in > PREFIX.out'

    Parameters
    ----------
    decompose_overlap : bool
        Whether to decompose the overlap matrices beforehand so that the
        eigensolver can already skip this decomposition at the first call
        (only influential for the ELPA eigensolvers, see the 'diag_algo'
        Tibi keyword). Default: False.

    nbasis : int
        The total number of basis functions, which sets the dimensions of
        the Hamiltonian and overlap matrices. This keyword is mandatory
        (the default value of 0 will raise an error).

    niter : int
        The number of iterations to carry out. Each iteration involves
        one eigensolver call and one band energy calculation.

    random_seed : int
        Seed for the random generator used for populating the Hamiltonian
        and overlap matrices. No seeding is done if the seed is equal to 0
        (the default value), which means that these matrices and the
        associated band energy will be different for each Eiger run.

    Other parameters
    ----------------
    In addition to the above Eiger-specific input parameters,
    a number of input parameters are shared with the Tibi program.
    Only those need to be considered which influence the number
    and type of eigenvalue problems and the eigensolver settings
    ('complex_wfn', 'diag_*', 'kpts', and 'spin_polarized').
    """
    implemented_properties = ['energy', 'free_energy']
    command = 'eiger PREFIX.in > PREFIX.out'

    def __init__(self, restart=None, label='eiger', atoms=None, command=None,
                 **kwargs):
        self.default_parameters = dict(
            decompose_overlap=False,
            nbasis=0,
            niter=1,
            random_seed=0,
        )

        for key, val in Tibi.default_parameters.items():
            if key in ['complex_wfn', 'kpts', 'spin_polarized'] or \
               key.startswith('diag_'):
                self.default_parameters[key] = val

        FileIOCalculator.__init__(self, restart=restart, label=label,
                                  atoms=atoms, command=command, **kwargs)
        self.calc = None

    def set(self, **kwargs):
        # Note: we are not checking for keywords that Eiger does not use
        # to allow using Tibi input files with added Eiger input parameters
        changed_parameters = FileIOCalculator.set(self, **kwargs)
        if changed_parameters:
            self.reset()

    def write_input(self, atoms, properties=None, system_changes=None):
        FileIOCalculator.write_input(self, atoms, properties, system_changes)

        with open(self.label + '.in', 'w') as f:
            write_eiger_input(f, **self.parameters)

    def read_results(self):
        with open(self.label + '.out', 'r') as f:
            output = next(read_eiger_output(f))

        self.calc = output.calc
        self.results = output.calc.results


def read_eiger_output(fhandle):
    """
    Returns an Atoms object with attached SinglePointCalculator
    containing results gathered from the Eiger output.

    Parameters
    ----------
    fhandle : file handle
        Handle to an open Eiger output file.

    Returns
    -------
    atoms : Atoms object
        Atoms object with attached SinglePointDFTCalculator.
    """
    def _parse_eband_section(lines):
        result = {'eband': None}

        for line in lines:
            if len(line) == 0:
                continue

            if 'Band energy [-]' in line:
                result['eband'] = float(line.split('=')[-1])

            if result['eband'] is not None:
                break
        else:
            raise IOError('Premature ending of eband section :/')

        return result

    lines = fhandle.readlines()

    identifiers = {
        'eband': 'Band energy [-]',
    }

    parsers = {
        'eband': _parse_eband_section,
    }

    # Find all the line numbers (indices) which match the above identifiers
    indices = {section: [] for section in identifiers}
    for i, line in enumerate(lines):
        for section, key in identifiers.items():
            if key in line:
                indices[section].append(i)

    # Find and parse the wanted section of the output for the identifiers
    # Dict in which we'll store the (intermediate) properties:
    results = {}

    for section in identifiers:
        nsections = len(indices[section])

        if nsections == 0:
            continue
        elif nsections == 1:
            first_index, next_index = indices[section][0], -1
        else:
            raise IOError(f'Too many hits for {section}: {nsections} > 1')

        part = lines[first_index: next_index]
        results.update(parsers[section].__call__(part))

    # Finally, store the results in an Atoms object with attached calculator
    atoms = Atoms()

    calc = SinglePointCalculator(atoms, energy=results['eband'])
    atoms.set_calculator(calc)
    yield atoms
