#-----------------------------------------------------------------------------#
#   Tibi: an ab-initio tight-binding electronic structure code                #
#   Copyright 2020-2025 Maxime Van den Bossche                                #
#   SPDX-License-Identifier: GPL-3.0-or-later                                 #
#-----------------------------------------------------------------------------#

import numpy as np
from ase import Atoms
from ase.units import Hartree, Bohr
from ase.calculators.singlepoint import (SinglePointDFTCalculator,
                                         SinglePointKPoint)


def write_tibi_input(fhandle, **kwargs):
    for key, val in kwargs.items():
        t = type(val)
        if t == list or t == tuple:
            s = ' '.join(map(str, val))
        elif t == bool:
            s = '.true.' if val else '.false.'
        elif t == str and '/' in val:
            s = '"%s"' % val
        elif t == dict:
            s = ''
            for k, v in val.items():
                if type(v) == list or type(v) == tuple:
                    s += '%s_%s ' % (k, ','.join(map(str, v)))
                else:
                    s += '%s_%s ' % (k, str(v))
            s = s.rstrip()
        else:
            s = str(val)
        fhandle.write('%s = %s\n' % (key, s))


def read_tibi_output(fhandle, index=-1):
    """
    Returns an Atoms object with attached SinglePointDFTCalculator
    which contains the results of ionic step <index>.

    Results which have been printed only once (i.e. at the first step)
    are assumed to be constant and are copied for subsequent steps.

    Parameters
    ----------
    fhandle : file handle
        Handle to an open Tibi output file.
    index : int, optional
        Index of the ionic step for which to read in the results
        (default: -1, i.e. the last step).

    Returns
    -------
    atoms : Atoms object
        Atoms object with attached SinglePointDFTCalculator.
    """
    lines = fhandle.readlines()

    identifiers = {'geometry': 'Geometry description',
                   'kpoints': 'K-points description',
                   'fermi': 'Fermi level(s) [Ha]',
                   'etot': 'Total energy [Ha]',
                   'etot_free': 'Total free energy [Ha]',
                   'forces': 'Forces [Ha / a0]',
                   'mulliken_chg': 'Mulliken subshell and atom charges',
                   'mulliken_mag': 'Mulliken subshell and atom magnetizations',
                   'magnetization': 'Total magnetization',
                   'spins': 'Spin description',
                   'stress': 'Stress tensor [Ha / a0^3]',
                   }

    parsers = {'geometry': _parse_geometry_section,
               'kpoints': _parse_kpoints_section,
               'fermi': _parse_fermi_section,
               'etot': _parse_etot_section,
               'etot_free': _parse_etot_free_section,
               'forces': _parse_forces_section,
               'mulliken_chg': _parse_mulliken_chg_section,
               'mulliken_mag': _parse_mulliken_mag_section,
               'magnetization': _parse_magnetization_section,
               'spins': _parse_spin_section,
               'stress': _parse_stress_section,
               }

    # Find all the line numbers (indices) which match the above identifiers
    indices = {section: [] for section in identifiers}
    for i, line in enumerate(lines):
        for section, key in identifiers.items():
            if key in line:
                indices[section].append(i)

    # Find and parse the wanted section of the output for the identifiers
    # Dict in which we'll store the (intermediate) properties:
    results = {
        key: None for key in [
            'atoms', 'fermi_levels', 'forces', 'kpt_coords', 'kpt_weights',
            'magnetization', 'mulliken_atom_chg', 'mulliken_atom_mag',
            'mulliken_subshell_chg', 'mulliken_subshell_mag',
            'nkpt', 'nspin', 'stress',
        ]
    }

    for section in identifiers:
        nsections = len(indices[section])

        # Determine which slice of the output to look at
        if nsections == 0:
            continue
        elif nsections == 1:
            first_index, next_index = indices[section][0], -1
        else:
            first_index = indices[section][index]
            if (index == nsections - 1) or (index == -1):
                next_index = -1
            elif index < nsections - 1:
                next_index = indices[section][index + 1]
            else:
                msg = 'Requesting output section %d, but only got %d sections' \
                      % (index, nsections)
                raise ValueError(msg)

        part = lines[first_index: next_index]
        results.update(parsers[section].__call__(part))

    # Finally, store the results in an Atoms object
    # with an attached calculator
    atoms = results['atoms']

    if results['fermi_levels'] is None:
        efermi = None
    else:
        efermi = max(results['fermi_levels'])

    calc = SinglePointDFTCalculator(atoms,
                                    energy=results['etot'],
                                    free_energy=results['etot_free'],
                                    forces=results['forces'],
                                    stress=results['stress'],
                                    efermi=efermi,
                                    ibzkpts=results['kpt_coords'],
                                    )

    if all([results[x] is not None for x in ['nspin', 'nkpt', 'kpt_weights']]):
        calc.kpts = []
        for ispin in range(results['nspin']):
            for ikpt in range(results['nkpt']):
                weight = results['kpt_weights'][ikpt]
                kpt = SinglePointKPoint(weight, ispin, ikpt)
                calc.kpts.append(kpt)

    for key in ['magnetization', 'mulliken_atom_chg', 'mulliken_atom_mag',
                'mulliken_subshell_chg', 'mulliken_subshell_mag']:
        calc.results[key] = results[key]

    atoms.set_calculator(calc)
    yield atoms


def _parse_mulliken_chg_section(lines):
    return _parse_mulliken_chgmag_section(lines, which='charges')


def _parse_mulliken_mag_section(lines):
    return _parse_mulliken_chgmag_section(lines, which='magnetizations')


def _parse_mulliken_chgmag_section(lines, which):
    assert which in ['charges', 'magnetizations']

    if which == 'charges':
        suffix = 'chg'
    elif which == 'magnetizations':
        suffix = 'mag'

    keys = ['mulliken_subshell_{0}'.format(suffix),
            'mulliken_atom_{0}'.format(suffix)]

    result = {key: None for key in keys}
    natom = None

    header = 'Mulliken subshell and atom {0}'.format(which)

    for line in lines:
        if len(line) == 0:
            continue

        if header in line:
            natom = int(line.split()[-2])

        elif 'Atom' in line and which in line:
            for key in keys:
                if result[key] is None:
                    result[key] = []

            chg = line.split('=')[-1].split('total:')
            result[keys[0]].append(list(map(float, chg[0].split())))
            result[keys[1]].append(float(chg[1]))

        if natom is not None and all([result[key] is not None for key in keys]):
            if all([len(result[key]) == natom for key in keys]):
                break
    else:
        raise IOError('Premature ending of {0} section :/'.format(which))

    result[keys[1]] = np.array(result[keys[1]])
    return result


def _parse_etot_section(lines):
    result = {'etot': None}

    for line in lines:
        if len(line) == 0:
            continue

        if 'Total energy [Ha]' in line:
            result['etot'] = float(line.split('=')[-1]) * Hartree

        if result['etot'] is not None:
            break
    else:
        raise IOError('Premature ending of etot section :/')

    return result


def _parse_etot_free_section(lines):
    result = {'etot_free': None}

    for line in lines:
        if len(line) == 0:
            continue

        if 'Total free energy [Ha]' in line:
            result['etot_free'] = float(line.split('=')[-1]) * Hartree

        if result['etot_free'] is not None:
            break
    else:
        raise IOError('Premature ending of etot_free section :/')

    return result


def _parse_fermi_section(lines):
    result = {'fermi_levels': []}

    for line in lines:
        if len(line) == 0:
            continue

        if 'Fermi level(s) [Ha]' in line:
            i = line.index("=")
            result['fermi_levels'] = list(map(float, line[i + 1:].split()))
            result['fermi_levels'] = np.array(result['fermi_levels']) * Hartree

        if len(result['fermi_levels']) > 0:
            break
    else:
        raise IOError('Premature ending of fermi section :/')

    return result


def _parse_forces_section(lines):
    result = {'forces': None}
    natom = None

    for line in lines:
        if len(line) == 0:
            continue

        if 'Forces [Ha / a0]' in line:
            natom = int(line.split()[-2])

        elif 'Atom' in line and 'forces' in line:
            if result['forces'] is None:
                result['forces'] = []

            f = list(map(float, line.split('=')[-1].split()))
            assert len(f) == 3
            result['forces'].append(f)

        if natom is not None and result['forces'] is not None:
            if len(result['forces']) == natom:
                break
    else:
        raise IOError('Premature ending of forces section :/')

    result['forces'] = np.array(result['forces']) * Hartree / Bohr

    return result


def _parse_geometry_section(lines):
    # Note: if the lines contain multiple geometry sections,
    # the parser will only read one of them (i.e. the first one)
    pbc = None
    cell = []
    positions = []
    symbols = []
    natom = None

    for line in lines:
        if len(line) == 0:
            continue

        l = line.split()

        if 'Periodic' in line:
            pbc = 'T' in line.split('=')[-1].upper()
        elif 'Cell vector' in line:
            cell.append(list(map(float, l[-3:])))
        elif 'Number of atoms' in line:
            natom = int(l[-1])
        elif ' Atom ' in line and '=' in line:
            symbols.append(l[2])
            positions.append(list(map(float, l[-3:])))

        if (natom is not None and len(positions) == natom and \
            len(symbols) == natom and pbc is not None):
            if (not pbc) or (pbc and len(cell) == 3):
                break
    else:
        raise IOError('Premature ending of geometry section :/')

    cell = np.array(cell) * Bohr if pbc else None
    positions = np.array(positions) * Bohr
    atoms = Atoms(symbols, positions=positions, pbc=pbc, cell=cell)
    result = {'atoms': atoms}
    return result


def _parse_kpoints_section(lines):
    # Note: if the lines contain multiple kpoint sections,
    # the parser will only read one of them (i.e. the first one)
    result = {'nkpt': None, 'kpt_coords': [], 'kpt_weights': []}

    for line in lines:
        if len(line) == 0:
            continue

        l = line.split()

        if 'Number of k-points' in line:
            result['nkpt'] = int(line.split('=')[-1])
        elif 'K-point' in line and 'coord' in line and 'weight' in line:
            c = list(map(float, l[4:7]))
            result['kpt_coords'].append(c)
            w = float(l[-1])
            result['kpt_weights'].append(w)

        if (result['nkpt'] is not None \
            and len(result['kpt_coords']) == result['nkpt'] \
            and len(result['kpt_weights']) == result['nkpt']):
                break
    else:
        raise IOError('Premature ending of kpoints section :/')

    return result


def _parse_magnetization_section(lines):
    result = {'magnetization': None}

    for line in lines:
        if 'Total magnetization [-|e|]' in line:
            i = line.index('=')
            result['magnetization'] = float(line[i+1:])
            break
    else:
        raise IOError('Premature ending of magnetization section :/')

    return result


def _parse_spin_section(lines):
    # Note: if the lines contain multiple spin sections,
    # the parser will only read one of them (i.e. the first one)
    result = {'nspin': None}

    for line in lines:
        if len(line) == 0:
            continue

        if 'Number of spins' in line:
            result['nspin'] = int(line.split('=')[-1])

        if result['nspin'] is not None:
            break
    else:
        raise IOError('Premature ending of spin section :/')

    return result


def _parse_stress_section(lines):
    result = {'stress': None}

    keys = ['sxx sxy sxz', 'syx syy syz', 'szx szy szz']

    for line in lines:
        if len(line) == 0:
            continue

        for key in keys:
            if line.lstrip().startswith(key):
                if result['stress'] is None:
                    result['stress'] = []

                s = list(map(float, line.split('=')[-1].split()))
                assert len(s) == 3
                result['stress'].append(s)
                break

        if result['stress'] is not None and len(result['stress']) == 3:
            break
    else:
        raise IOError('Premature ending of stress section :/')

    result['stress'] = np.array(result['stress']) * Hartree / Bohr**3

    return result


def _add_bands(calc):
    """
    Adds the eigenvalues and occupations to the
    SinglePointKPoint instances in calc.kpts.
    """
    for kpoint in calc.kpts:
        data = _read_skdat_file('bands', kpoint.k, kpoint.s)
        kpoint.eps_n = data[:, 1] * Hartree
        kpoint.f_n = data[:, 2]


def _add_giese_york_chg(calc):
    _add_datfile(calc, which='giese_york_chg')


def _add_giese_york_mag(calc):
    _add_datfile(calc, which='giese_york_mag')


def _add_mulliken_orbital_mag(calc):
    _add_datfile(calc, which='mulliken_orbital_mag')


def _add_mulliken_orbital_pop(calc):
    _add_datfile(calc, which='mulliken_orbital_pop')


def _add_datfile(calc, which):
    """Adds data from a '<which>.dat' output file."""
    calc.results[which] = []
    with open('{0}.dat'.format(which), 'r') as f:
        for line in f:
            if line.startswith('#'):
                continue
            items = list(map(float, line.split()[1:]))
            calc.results[which].append(items)


def _read_skdat_file(prefix, ikpt, ispin):
    filename = '%s_kpt%s_spin%s.dat' % (prefix, ikpt + 1, ispin + 1)
    return np.loadtxt(filename)


def add_kpoint_spin_matrices(mtype, calc, is_complex):
    """
    Reads and adds the given 'mtype' matrix for each k-point/spin
    to calc.results.
    """
    calc.results[mtype] = {}
    for kpoint in calc.kpts:
        if is_complex:
            data = _read_skdat_file(mtype + '_real', kpoint.k, kpoint.s) \
                   + 1j * _read_skdat_file(mtype + '_imag', kpoint.k, kpoint.s)
        else:
            data = _read_skdat_file(mtype, kpoint.k, kpoint.s)

        calc.results[mtype][(kpoint.k, kpoint.s)] = data[:, 1:]


def add_single_matrix(mtype, calc):
    eval('_add_%s(calc)' % mtype)
