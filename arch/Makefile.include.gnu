CC = gcc
FC = gfortran

# Optimization-related options
ifeq ($(DEBUG), 1)
    # For debugging and development
    CPPFLAGS += -D_DEBUG
    FFLAGS += -O0 -g -fPIC -Wall -Wextra -Wimplicit-interface \
              -Wno-unused-dummy-argument -fmax-errors=1 \
              -fcheck=all -fbacktrace -std=f2008
    CFLAGS += -O0 -g
else
    # For production
    CPPFLAGS += -D_NODEBUG
    FFLAGS += -O3 -march=native -funroll-loops
    CFLAGS += -O3 -march=native
endif

# OpenMP-related options
ifeq ($(OMP), 1)
    # OpenMP build
    CPPFLAGS += -D_OMP
    FFLAGS += -fopenmp
    LDFLAGS += -lgomp
else
    # Serial build
    CPPFLAGS += -D_SERIAL
endif

# LibXSMM-related options
FFLAGS += -I/opt/libxsmm/include
LDFLAGS += -L/opt/libxsmm/lib -lxsmmf -lxsmm -lxsmmext

# ELPA-related options
ifeq ($(ELPA), 1)
    CPPFLAGS += -D_ELPA
    ELPAVER = YYYY.MM.VVV

    ifeq ($(OMP), 1)
        FFLAGS += -I/opt/elpa/include/elpa_onenode_openmp-$(ELPAVER)/modules
        LDFLAGS += -L/opt/elpa/lib -lelpa_onenode_openmp
    else
        FFLAGS += -I/opt/elpa/include/elpa_onenode-$(ELPAVER)/modules
        LDFLAGS += -L/opt/elpa/lib -lelpa_onenode
    endif
endif

# BLAS/LAPACK-related options
ifeq ($(OMP), 1)
    # Intel MKL (threaded)
    LDFLAGS += -L$(MKLROOT)/lib/intel64 -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core
else
    # Intel MKL (serial)
    LDFLAGS += -L$(MKLROOT)/lib/intel64 -lmkl_gf_lp64 -lmkl_sequential -lmkl_core
endif
