import subprocess
from setuptools import find_packages, setup


def get_version():
    proc = subprocess.run(['./version.sh', 'python'], stdout=subprocess.PIPE)
    version = proc.stdout.decode().strip()
    return version


install_requires = [
    'ase>=3.22.0',
    'numpy',
    'scipy',
]


setup(install_requires=install_requires,
      license='LICENSE',
      name='tibi',
      package_dir={
          '': 'python',
      },
      packages=find_packages(where='python'),
      python_requires='>=3.6',
      url='https://gitlab.com/mvdb/tibi',
      version=get_version())
